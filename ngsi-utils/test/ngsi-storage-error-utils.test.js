/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');

const {
  errorTemplates,
  errors,
} = require('../src/utils/ngsi-storage-error-utils');

describe('ngsi-storage-error-utils', () => {

  it('we do have some errors & errorTemplates', () => {
    const numTemplates = _.size(errorTemplates);
    assert.strictEqual(numTemplates, 22);
    const numErrors = _.size(errors);
    assert.strictEqual(numErrors, 22);
  });

  it('the errors have some "matches" functionality and identifier flag', () => {
    const nonmatchingErr = new Error('this one does not match nuthin');
    _.each(errors, (error) => {
      const storageError = error();
      assert(storageError.isNgsiError);
      assert(storageError.isNgsiStorageError);
      assert(error.matches(storageError));
      assert(error.matches(storageError.message));
      assert(error.matches(errorTemplates[storageError.name]));
      assert(!error.matches(nonmatchingErr));
      assert(!error.matches(nonmatchingErr.message));
    });
  });

});
