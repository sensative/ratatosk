/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const {assertThrows} = require('../src');

const {
  fromDate,
  fromMillisecs,
  fromSeconds,
  toDate,
  toMillisecs,
  toSeconds,
  hasValidDateTimeFormat,
} = require('../src/utils/ngsi-date-time-utils');

describe('ngsi-date-time-utils', () => {

  describe('fromMillisecs', () => {

    it('a reasonable, valid working example', () => {
      const ms = 1533333333333;
      const dt = fromMillisecs(ms);
      assert.strictEqual(dt, '2018-08-03T21:55:33.333Z');
    });

    it('must be a number', () => {
      const invalids = [
        null,
        {},
        '1533333333333',
      ];
      _.each(invalids, (invalid) => {
        const message = 'Invalid numeric DateTime initializer arg';
        assertThrows(() => fromMillisecs(invalid), message);
      });
    });

  });

  describe('fromSeconds', () => {

    it('a reasonable, valid working example', () => {
      const ms = 1533333333;
      const dt = fromSeconds(ms);
      assert.strictEqual(dt, '2018-08-03T21:55:33.000Z');
    });

    it('must be a number', () => {
      const invalids = [
        null,
        {},
        '1533333333',
      ];
      _.each(invalids, (invalid) => {
        const message = 'Invalid numeric DateTime initializer arg';
        assertThrows(() => fromSeconds(invalid), message);
      });
    });

  });

  describe('toMillisecs', () => {

    it('a reasonable, valid working example', () => {
      const dt = '2018-08-03T21:55:33.333Z';
      const ms = toMillisecs(dt);
      assert.strictEqual(ms, 1533333333333);
    });

    it('orion format is also acceptable (missing the millisecond digit)', () => {
      const dt = '2018-08-03T21:55:33.33Z';
      const ms = toMillisecs(dt);
      assert.strictEqual(ms, 1533333333330);
    });

    it('if the string is formatted incorrectly, then FAIL', () => {
      const invalids = [
        '2018-08-03T21:55:33.33', // missing Z
        '2018-08-03T21:55:33.333', // missing Z
        '2018-08-03T21:55:33.3Z', // one too few digits
        '2018-08-03T21:55:33.3333Z', // one too few digits
        null,
        [],
      ];
      _.each(invalids, (invalid) => {
        const message = 'Invalid DateTime string';
        assertThrows(() => toMillisecs(invalid), message);
      });
    });

    it('even if the string nominally has valid formattin, if the numbers are out-of-bounds, then FAIL', () => {
      const invalids = [
        '2018-08-03T21:55:93.333Z', // wrong in seconds
        '2018-08-03T21:95:33.333Z', // wrong in minutes
        '2018-08-03T31:55:33.333Z', // wrong in hours
        '2018-08-43T21:55:33.333Z', // wrong in day
        '2018-28-03T21:55:33.333Z', // wrong in month
      ];
      _.each(invalids, (invalid) => {
        const message = 'Invalid DateTime string';
        assertThrows(() => toMillisecs(invalid), message);
      });
    });

  });

  describe('toSeconds', () => {

    it('a reasonable, valid working example', () => {
      const dt = '2018-08-03T21:55:33.333Z';
      const ms = toSeconds(dt);
      assert.strictEqual(ms, 1533333333);
    });

    it('orion format is also acceptable (missing the millisecond digit)', () => {
      const dt = '2018-08-03T21:55:33.33Z';
      const ms = toSeconds(dt);
      assert.strictEqual(ms, 1533333333);
    });

    it('if the string is formatted incorrectly, then FAIL', () => {
      const invalids = [
        '2018-08-03T21:55:33.33', // missing Z
        '2018-08-03T21:55:33.333', // missing Z
        '2018-08-03T21:55:33.3Z', // one too few digits
        '2018-08-03T21:55:33.3333Z', // one too few digits
        null,
        [],
      ];
      _.each(invalids, (invalid) => {
        const message = 'Invalid DateTime string';
        assertThrows(() => toSeconds(invalid), message);
      });
    });

    it('even if the string nominally has valid formattin, if the numbers are out-of-bounds, then FAIL', () => {
      const invalids = [
        '2018-08-03T21:55:93.333Z', // wrong in seconds
        '2018-08-03T21:95:33.333Z', // wrong in minutes
        '2018-08-03T31:55:33.333Z', // wrong in hours
        '2018-08-43T21:55:33.333Z', // wrong in day
        '2018-28-03T21:55:33.333Z', // wrong in month
      ];
      _.each(invalids, (invalid) => {
        const message = 'Invalid DateTime string';
        assertThrows(() => toSeconds(invalid), message);
      });
    });

  });

  describe('roundtrips should be invariant', () => {

    it('fromMillisecs then toMillisecs', () => {
      const ms = 1533333333333;
      const dt = fromMillisecs(ms);
      const res = toMillisecs(dt);
      assert.strictEqual(ms, res);
    });

    it('fromSeconds then toSeconds', () => {
      const seconds = 1533333777;
      const dt = fromSeconds(seconds);
      const res = toSeconds(dt);
      assert.strictEqual(seconds, res);
    });

  });

  describe('fromDate', () => {

    it('the supplied arg must be a date else FAIL', () => {
      const invalids = [
        '2018-08-03T21:55:93.333Z',
        1533333333333,
      ];
      _.each(invalids, (invalid) => {
        const message = 'Invalid Date object. Cannot be formatted to a DateTime string';
        assertThrows(() => fromDate(invalid), message);
      });
    });

    it('dates work', () => {
      const valids = [
        {in: new Date(1533333333333), out: '2018-08-03T21:55:33.333Z'},
      ];
      _.each(valids, (valid) => {
        const res = fromDate(valid.in);
        assert.strictEqual(res, valid.out);
      });
    });

  });

  describe('toDate (a thin cover of toMillisecs)', () => {

    it('toDate throws the same errors as toMillisecs does', () => {
      const invalids = [
        '2018-08-03T21:55:33.33', // missing Z
        '2018-08-03T21:55:33.333', // missing Z
        '2018-08-03T21:55:33.3Z', // one too few digits
        '2018-08-03T21:55:33.3333Z', // one too few digits
        null,
        [],
        '2018-08-03T21:55:93.333Z', // wrong in seconds
        '2018-08-03T21:95:33.333Z', // wrong in minutes
        '2018-08-03T31:55:33.333Z', // wrong in hours
        '2018-08-43T21:55:33.333Z', // wrong in day
        '2018-28-03T21:55:33.333Z', // wrong in month
      ];
      _.each(invalids, (invalid) => {
        const errTD = assertThrows(() => toDate(invalid));
        const errFM = assertThrows(() => toMillisecs(invalid));
        assert.deepStrictEqual(errTD, errFM);
      });
    });

    it('works with a valid DateTime formatted string (and is compatible with toMillisecs)', () => {
      const valids = [
        '2018-08-03T21:55:33.333Z',
        '2018-09-03T21:55:33.33Z',
      ];
      _.each(valids, (valid) => {
        const date = toDate(valid);
        const ms = toMillisecs(valid);
        assert.strictEqual(date.valueOf(), ms);
      });
    });

  });

  describe('hasValidDateTimeFormat (is just a cover for fromMillisecs with a catch)', () => {

    it('a reasonable, valid working example', () => {
      const ms = 1533333333333;
      const dt = fromMillisecs(ms);
      const isValid = hasValidDateTimeFormat(dt);
      assert.strictEqual(isValid, true);
    });

    it('and some invalid date-times', () => {
      const invalids = [
        null,
        {},
        '1533333333333',
      ];
      _.each(invalids, (invalid) => {
        const isValid = hasValidDateTimeFormat(invalid);
        assert.strictEqual(isValid, false);
      });
    });

  });


});
