/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');

const {
  assertThrows,
} = require('../src');

const {
  errors,
  isNgsiGeometryType,
  parseNgsiGeometry,
  serializeNgsiGeometry,
  simpleCoordPairParser: {
    parseSimpleCoordPair,
    serializeSimpleCoordPair,
    validateSimpleCoordPair,
  },
} = require('../src/utils/ngsi-location-utils');

describe('ngsi-location-utils', () => {

  describe('errors', () => {

    it('there are 22 NGSI_LOCATION_UTILS_ERROR errors', () => {
      assert.strictEqual(_.size(errors), 22);
    });

    it('the errors have some "matches" functionality and identifier flag', () => {
      const nonmatchingErr = new Error('this one does not match nuthin');
      _.each(errors, (error) => {
        const datumErr = error();
        assert(datumErr.isNgsiLocationUtilsError);
        assert(error.matches(datumErr));
        assert(error.matches(datumErr.message));
        assert(!error.matches(nonmatchingErr));
        assert(!error.matches(nonmatchingErr.message));
      });
    });

  });

  describe('parseSimpleCoordPair', () => {

    it('some valids', () => {
      const valids = [
        {in: '23,44', out: {lat: 23, lon: 44}},
        {in: '23   ,   44', out: {lat: 23, lon: 44}},
        {in: '-90,-180', out: {lat: -90, lon: -180}},
        {in: '90,180', out: {lat: 90, lon: 180}},
      ];
      _.each(valids, (valid) => {
        const pt = parseSimpleCoordPair(valid.in);
        assert.deepStrictEqual(pt, valid.out);
      });
    });

    it('some invalids', () => {
      const invalids = [
        {in: 23, err: errors.INVALID_SIMPLE_POINT_FORMAT()},
        {in: null, err: errors.INVALID_SIMPLE_POINT_FORMAT()},
        {in: '234', err: errors.INVALID_SIMPLE_POINT_DIMENSION()},
        {in: '23,23,23', err: errors.INVALID_SIMPLE_POINT_DIMENSION()},
        {in: '-91,0', err: errors.INVALID_SIMPLE_POINT_LATITUDE()},
        {in: '91,0', err: errors.INVALID_SIMPLE_POINT_LATITUDE()},
        {in: '0,-181', err: errors.INVALID_SIMPLE_POINT_LONGITUDE()},
        {in: '0,181', err: errors.INVALID_SIMPLE_POINT_LONGITUDE()},
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => parseSimpleCoordPair(invalid.in), invalid.err);
      });
    });

  });

  describe('serializeSimpleCoordPair', () => {

    it('some valids', () => {
      const valids = [
        {in: {lat: 23, lon: 44}, out: '23,44'},
        {in: {lat: -90, lon: -180}, out: '-90,-180'},
        {in: {lat: 90, lon: 180}, out: '90,180'},
      ];
      _.each(valids, (valid) => {
        const pt = serializeSimpleCoordPair(valid.in);
        assert.deepStrictEqual(pt, valid.out);
      });
    });

    it('some invalids', () => {
      const invalids = [
        {in: 23, err: errors.INVALID_SIMPLE_POINT_OBJECT()},
        {in: {lat: 23}, err: errors.INVALID_SIMPLE_POINT_OBJECT()},
        {in: {lat: -91, lon: 0}, err: errors.INVALID_SIMPLE_POINT_LATITUDE()},
        {in: {lat: 91, lon: 0}, err: errors.INVALID_SIMPLE_POINT_LATITUDE()},
        {in: {lat: 0, lon: -181}, err: errors.INVALID_SIMPLE_POINT_LONGITUDE()},
        {in: {lat: 0, lon: 181}, err: errors.INVALID_SIMPLE_POINT_LONGITUDE()},
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => serializeSimpleCoordPair(invalid.in), invalid.err);
      });
    });

  });

  describe('validateSimpleCoordPair', () => {

    it('some valids', () => {
      const valids = [
        // strings
        {in: '23,44', out: {lat: 23, lon: 44}},
        {in: '23   ,   44', out: {lat: 23, lon: 44}},
        {in: '-90,-180', out: {lat: -90, lon: -180}},
        {in: '90,180', out: {lat: 90, lon: 180}},
        // objects
        {in: {lat: 23, lon: 44}, out: '23,44'},
        {in: {lat: -90, lon: -180}, out: '-90,-180'},
        {in: {lat: 90, lon: 180}, out: '90,180'},
      ];
      _.each(valids, (valid) => {
        assert.doesNotThrow(() => validateSimpleCoordPair(valid.in));
      });
    });

    it('some invalids', () => {
      const invalids = [
        // strings
        {in: 23, err: errors.INVALID_SIMPLE_POINT_FORMAT()},
        {in: null, err: errors.INVALID_SIMPLE_POINT_FORMAT()},
        {in: '234', err: errors.INVALID_SIMPLE_POINT_DIMENSION()},
        {in: '23,23,23', err: errors.INVALID_SIMPLE_POINT_DIMENSION()},
        {in: '-91,0', err: errors.INVALID_SIMPLE_POINT_LATITUDE()},
        {in: '91,0', err: errors.INVALID_SIMPLE_POINT_LATITUDE()},
        {in: '0,-181', err: errors.INVALID_SIMPLE_POINT_LONGITUDE()},
        {in: '0,181', err: errors.INVALID_SIMPLE_POINT_LONGITUDE()},
        // objects
        {in: {lat: 23}, err: errors.INVALID_SIMPLE_POINT_OBJECT()},
        {in: {lat: -91, lon: 0}, err: errors.INVALID_SIMPLE_POINT_LATITUDE()},
        {in: {lat: 91, lon: 0}, err: errors.INVALID_SIMPLE_POINT_LATITUDE()},
        {in: {lat: 0, lon: -181}, err: errors.INVALID_SIMPLE_POINT_LONGITUDE()},
        {in: {lat: 0, lon: 181}, err: errors.INVALID_SIMPLE_POINT_LONGITUDE()},
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => validateSimpleCoordPair(invalid.in), invalid.err);
      });
    });

  });

  describe('isNgsiGeometryType', () => {

    it('some valids', () => {
      const valids = [
        'geo:point',
        'geo:box',
        'geo:line',
        'geo:polygon',
        'geo:json',
      ];
      _.each(valids, (valid) => {
        assert(isNgsiGeometryType(valid));
      });
    });

    it('some invalids', () => {
      const invalids = [
        'anything else',
        null,
      ];
      _.each(invalids, (invalid) => {
        assert(!isNgsiGeometryType(invalid));
      });
    });

  });

  describe('parseNgsiGeometry', () => {

    it('some valids', () => {
      const valids = [
        // simple geo
        {
          in: {type: 'geo:point', value: '1,2'},
          out: {
            type: 'Point',
            coordinates: [2, 1],
          },
        },
        {
          in: {type: 'geo:box', value: ['1,2', '3,4']},
          out: {
            type: 'Polygon',
            coordinates: [[[2, 1], [2, 3], [4, 3], [4, 1], [2, 1]]],
          },
        },
        {
          in: {type: 'geo:line', value: ['1,2', '3,4']},
          out: {
            type: 'LineString',
            coordinates: [[2, 1], [4, 3]],
          },
        },
        {
          in: {type: 'geo:polygon', value: ['1,2', '3,4', '5,6', '1,2']},
          out: {
            type: 'Polygon',
            coordinates: [[[2, 1], [4, 3], [6, 5], [2, 1]]],
          },
        },

        // geojson
        {
          in: {
            type: 'geo:json',
            value: {
              type: 'Point',
              coordinates: [2, 1],
            },
          },
          out: {
            type: 'Point',
            coordinates: [2, 1],
          },
        },

        {
          in: {
            type: 'geo:json',
            value: {
              type: 'MultiPoint',
              coordinates: [[2, 1], [3, 4]],
            },
          },
          out: {
            type: 'MultiPoint',
            coordinates: [[2, 1], [3, 4]],
          },
        },

        {
          in: {
            type: 'geo:json',
            value: {
              type: 'LineString',
              coordinates: [[2, 1], [3, 4]],
            },
          },
          out: {
            type: 'LineString',
            coordinates: [[2, 1], [3, 4]],
          },
        },

        {
          in: {
            type: 'geo:json',
            value: {
              type: 'MultiLineString',
              coordinates: [[[2, 1], [3, 4]]],
            },
          },
          out: {
            type: 'MultiLineString',
            coordinates: [[[2, 1], [3, 4]]],
          },
        },

        {
          in: {
            type: 'geo:json',
            value: {
              type: 'Polygon',
              coordinates: [[[2, 1], [3, 4], [5, 6], [2, 1]]],
            },
          },
          out: {
            type: 'Polygon',
            coordinates: [[[2, 1], [3, 4], [5, 6], [2, 1]]],
          },
        },

        {
          in: {
            type: 'geo:json',
            value: {
              type: 'MultiPolygon',
              coordinates: [[[[2, 1], [3, 4], [5, 6], [2, 1]]]],
            },
          },
          out: {
            type: 'MultiPolygon',
            coordinates: [[[[2, 1], [3, 4], [5, 6], [2, 1]]]],
          },
        },

      ];
      _.each(valids, (valid) => {
        const {type, value} = valid.in;
        const res = parseNgsiGeometry(type, value);
        assert.deepStrictEqual(res, valid.out);
      });
    });

    it('some invalids', () => {
      const invalids = [
        {in: {type: 'invalid', value: 'eerp'}, err: errors.INVALID_SIMPLE_GEO_TYPE()},
        // parsing a simple-geo point
        {in: {type: 'geo:point', value: 234}, err: errors.INVALID_SIMPLE_POINT_FORMAT()},
        {in: {type: 'geo:point', value: '2,3,4'}, err: errors.INVALID_SIMPLE_POINT_DIMENSION()},
        {in: {type: 'geo:point', value: '1000,0'}, err: errors.INVALID_SIMPLE_POINT_LATITUDE()},
        {in: {type: 'geo:point', value: '0,1000'}, err: errors.INVALID_SIMPLE_POINT_LONGITUDE()},
        // simple geo
        {in: {type: 'geo:box', value: '0,1'}, err: errors.INVALID_SIMPLE_BOX_FORMAT()},
        {in: {type: 'geo:box', value: ['0,1', '2,3', '4,5']}, err: errors.INVALID_SIMPLE_BOX_FORMAT()},
        {in: {type: 'geo:line', value: '0,1'}, err: errors.INVALID_SIMPLE_LINE_FORMAT()},
        {in: {type: 'geo:line', value: ['0,1']}, err: errors.INVALID_SIMPLE_LINE_FORMAT()},
        {in: {type: 'geo:polygon', value: '0,1'}, err: errors.INVALID_SIMPLE_POLYGON_FORMAT()},
        {in: {type: 'geo:polygon', value: ['0,1']}, err: errors.INVALID_SIMPLE_POLYGON_FORMAT()},
        {in: {type: 'geo:polygon', value: ['0,1', '2,3', '4,5', '6,7']}, err: errors.INVALID_SIMPLE_POLYGON_CLOSURE()},
        // geojson
        {
          in: {type: 'geo:json', value: {type: 'Point', coordinates: [0]}},
          err: errors.INVALID_GEO_JSON_COORDS(),
        },
        {
          in: {type: 'geo:json', value: {type: 'MultiPoint', coordinates: 'EERP'}},
          err: errors.INVALID_GEO_JSON_MULTI_POINT(),
        },
        {
          in: {type: 'geo:json', value: {type: 'LineString', coordinates: [[1, 2]]}},
          err: errors.INVALID_GEO_JSON_LINE_STRING(),
        },
        {
          in: {type: 'geo:json', value: {type: 'MultiLineString', coordinates: 'errrp2'}},
          err: errors.INVALID_GEO_JSON_MULTI_LINE_STRING(),
        },
        {
          in: {type: 'geo:json', value: {type: 'Polygon', coordinates: [[[1, 2]]]}},
          err: errors.INVALID_GEO_JSON_POLYGON(),
        },
        {
          in: {type: 'geo:json', value: {type: 'Polygon', coordinates: [[[1, 2], [2, 3], [3, 4], [5, 6]]]}},
          err: errors.INVALID_GEO_JSON_POLYGON(),
        },
        {
          in: {type: 'geo:json', value: {type: 'MultiPolygon', coordinates: 'eerrrapaho'}},
          err: errors.INVALID_GEO_JSON_MULTI_POLYGON(),
        },
      ];
      _.each(invalids, (invalid) => {
        const {type, value} = invalid.in;
        assertThrows(() => parseNgsiGeometry(type, value), invalid.err);
      });
    });

  });

  describe('serializeNgsiGeometry', () => {

    it('some valids', () => {
      const valids = [
        // simple geo
        {
          in: {
            type: 'geo:point',
            value: {type: 'Point', coordinates: [2, 1]},
          },
          out: '1,2',
        },
        {
          in: {
            type: 'geo:box',
            value: {type: 'Polygon', coordinates: [[[2, 1], [4, 1], [4, 3], [2, 3], [2, 1]]]},
          },
          out: ['1,2', '3,4'],
        },
        {
          in: {
            type: 'geo:line',
            value: {type: 'LineString', coordinates: [[2, 1], [4, 3]]},
          },
          out: ['1,2', '3,4'],
        },
        {
          in: {
            type: 'geo:polygon',
            value: {type: 'Polygon', coordinates: [[[2, 1], [4, 3], [6, 5], [2, 1]]]},
          },
          out: ['1,2', '3,4', '5,6', '1,2'],
        },

        // geojson
        {
          in: {
            type: 'geo:json',
            value: {
              type: 'Point',
              coordinates: [2, 1],
            },
          },
          out: {
            type: 'Point',
            coordinates: [2, 1],
          },
        },

        {
          in: {
            type: 'geo:json',
            value: {
              type: 'MultiPoint',
              coordinates: [[2, 1], [3, 4]],
            },
          },
          out: {
            type: 'MultiPoint',
            coordinates: [[2, 1], [3, 4]],
          },
        },

        {
          in: {
            type: 'geo:json',
            value: {
              type: 'LineString',
              coordinates: [[2, 1], [3, 4]],
            },
          },
          out: {
            type: 'LineString',
            coordinates: [[2, 1], [3, 4]],
          },
        },

        {
          in: {
            type: 'geo:json',
            value: {
              type: 'MultiLineString',
              coordinates: [[[2, 1], [3, 4]]],
            },
          },
          out: {
            type: 'MultiLineString',
            coordinates: [[[2, 1], [3, 4]]],
          },
        },

        {
          in: {
            type: 'geo:json',
            value: {
              type: 'Polygon',
              coordinates: [[[2, 1], [3, 4], [5, 6], [2, 1]]],
            },
          },
          out: {
            type: 'Polygon',
            coordinates: [[[2, 1], [3, 4], [5, 6], [2, 1]]],
          },
        },

        {
          in: {
            type: 'geo:json',
            value: {
              type: 'MultiPolygon',
              coordinates: [[[[2, 1], [3, 4], [5, 6], [2, 1]]]],
            },
          },
          out: {
            type: 'MultiPolygon',
            coordinates: [[[[2, 1], [3, 4], [5, 6], [2, 1]]]],
          },
        },

      ];
      _.each(valids, (valid) => {
        const {type, value} = valid.in;
        const res = serializeNgsiGeometry(type, value);
        assert.deepStrictEqual(res, valid.out);
      });
    });

    it('some invalids', () => {
      const invalids = [
        // super general
        {
          in: {
            type: 'geo:point',
            value: 'derrrrp',
          },
          err: errors.INVALID_NGSI_GEOMETRY_VALUE(),
        },
        {
          in: {
            type: 'geo:point',
            value: {type: 'Point', coordinates: 234},
          },
          err: errors.INVALID_SIMPLE_POINT_ARRAY(),
        },
        // parsing a simple-geo point
        {
          in: {
            type: 'geo:point',
            value: {type: 'Point', coordinates: 234},
          },
          err: errors.INVALID_SIMPLE_POINT_ARRAY(),
        },
        {
          in: {
            type: 'geo:point',
            value: {type: 'Point', coordinates: [2, 3, 4]},
          },
          err: errors.INVALID_SIMPLE_POINT_ARRAY(),
        },
        {
          in: {
            type: 'geo:point',
            value: {type: 'Point', coordinates: [1000, 0]},
          },
          err: errors.INVALID_SIMPLE_POINT_LONGITUDE(),
        },
        {
          in: {
            type: 'geo:point',
            value: {type: 'Point', coordinates: [0, 1000]},
          },
          err: errors.INVALID_SIMPLE_POINT_LATITUDE(),
        },

        // geojson
        {
          in: {
            type: 'geo:json',
            value: {type: 'Point', coordinates: [0]},
          },
          err: errors.INVALID_GEO_JSON_COORDS(),
        },

        {
          in: {
            type: 'geo:json',
            value: {type: 'MultiPoint', coordinates: [null]},
          },
          err: errors.INVALID_GEO_JSON_COORDS(),
        },
        {
          in: {
            type: 'geo:json',
            value: {type: 'MultiPoint', coordinates: null},
          },
          err: errors.INVALID_GEO_JSON_MULTI_POINT(),
        },

        {
          in: {
            type: 'geo:json',
            value: {type: 'LineString', coordinates: [[], []]},
          },
          err: errors.INVALID_GEO_JSON_COORDS(),
        },
        {
          in: {
            type: 'geo:json',
            value: {type: 'LineString', coordinates: null},
          },
          err: errors.INVALID_GEO_JSON_LINE_STRING(),
        },

        {
          in: {
            type: 'geo:json',
            value: {type: 'MultiLineString', coordinates: [[[], []]]},
          },
          err: errors.INVALID_GEO_JSON_COORDS(),
        },
        {
          in: {
            type: 'geo:json',
            value: {type: 'MultiLineString', coordinates: null},
          },
          err: errors.INVALID_GEO_JSON_MULTI_LINE_STRING(),
        },

        {
          in: {
            type: 'geo:json',
            value: {type: 'Polygon', coordinates: [[[], [], [], []]]},
          },
          err: errors.INVALID_GEO_JSON_COORDS(),
        },
        {
          in: {
            type: 'geo:json',
            value: {type: 'Polygon', coordinates: null},
          },
          err: errors.INVALID_GEO_JSON_POLYGON(),
        },
        {
          in: {
            type: 'geo:json',
            value: {type: 'Polygon', coordinates: [[[1, 1], [2, 2], [3, 3], [4, 4]]]},
          },
          err: errors.INVALID_GEO_JSON_POLYGON(),
        },

      ];
      _.each(invalids, (invalid) => {
        const {type, value} = invalid.in;
        assertThrows(() => serializeNgsiGeometry(type, value), invalid.err);
      });
    });

  });

});
