/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');

const {
  detailedTokenize,
  tokenize,
  errors,
} = require('../../src/libs/ngsi-tokenize');

describe('ngsi-tokenize (NOTE: is used in a couple of places)', () => {

  describe('errors', () => {

    it('there are 5 NgsiTokenizeErrors', () => {
      assert.strictEqual(_.size(errors), 5);
    });

    it('the errors have some "matches" functionality and identifier flag', () => {
      const nonmatchingErr = new Error('this one does not match nuthin');
      _.each(errors, (error) => {
        const basicErr = error();
        assert(basicErr.isNgsiTokenizeError);
        assert(error.matches(basicErr));
        assert(error.matches(basicErr.message));
        assert(!error.matches(nonmatchingErr));
        assert(!error.matches(nonmatchingErr.message));
      });
    });

  });

  describe('detailedTokenize', () => {

    it('should tokenize some q-tokens', () => {
      const valids = [
        {in: 'asdf', out: [{value: 'asdf', isGrouped: false}]},
        {in: 'a.\'b.c\'.e', out: [{value: 'a', isGrouped: false}, {value: 'b.c', isGrouped: true}, {value: 'e', isGrouped: false}]},
      ];
      _.each(valids, (valid) => {
        const res = detailedTokenize(valid.in, '.', '\'');
        assert.deepStrictEqual(res, valid.out);
      });
    });

    it('should tokenize a sql equality arg list', () => {
      const valids = [
        {in: 'asdf,\'arg,d\',\'arg..d\'', out: [{value: 'asdf', isGrouped: false}, {value: 'arg,d', isGrouped: true}, {value: 'arg..d', isGrouped: true}]},
      ];
      _.each(valids, (valid) => {
        const res = detailedTokenize(valid.in, ',', '\'');
        assert.deepStrictEqual(res, valid.out);
      });
    });

    it('should tokenize a sql range arg list', () => {
      const valids = [
        {in: 'asdf..\'arg,d\'..\'arg..d\'', out: [{value: 'asdf', isGrouped: false}, {value: 'arg,d', isGrouped: true}, {value: 'arg..d', isGrouped: true}]},
      ];
      _.each(valids, (valid) => {
        const res = detailedTokenize(valid.in, '..', '\'');
        assert.deepStrictEqual(res, valid.out);
      });
    });

  });

  describe('tokenize', () => {

    it('should tokenize some q-tokens', () => {
      const valids = [
        {in: 'asdf', out: ['asdf']},
        {in: 'a.\'b.c\'.e', out: ['a', 'b.c', 'e']},
      ];
      _.each(valids, (valid) => {
        const res = tokenize(valid.in, '.', '\'');
        assert.deepStrictEqual(res, valid.out);
      });
    });

    it('should tokenize a sql equality arg list', () => {
      const valids = [
        {in: 'asdf,\'arg,d\',\'arg..d\'', out: ['asdf', 'arg,d', 'arg..d']},
      ];
      _.each(valids, (valid) => {
        const res = tokenize(valid.in, ',', '\'');
        assert.deepStrictEqual(res, valid.out);
      });
    });

    it('should tokenize a sql range arg list', () => {
      const valids = [
        {in: 'asdf..\'arg,d\'..\'arg..d\'', out: ['asdf', 'arg,d', 'arg..d']},
      ];
      _.each(valids, (valid) => {
        const res = tokenize(valid.in, '..', '\'');
        assert.deepStrictEqual(res, valid.out);
      });
    });

  });

});
