/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const {assertThrows} = require('../../src');

const {generateErrors} = require('../../src/libs/ngsi-error-generator');


describe('ngsi-error-generator', () => {

  it('some valid errorTypes', () => {
    const valids = [
      'NGSI_WHATEVER',
    ];
    _.each(valids, (valid) => {
      assert.doesNotThrow(() => generateErrors(valid, {}));
    });
  });

  it('some invalid errorTypes', () => {
    const invalids = [
      'ngsi_whatever',
      'Ngsi_Whatever',
    ];
    _.each(invalids, (invalid) => {
      const mess = `DevErr: invalid errorType: ${invalid}`;
      assertThrows(() => generateErrors(invalid, {}), mess);
    });
  });

  it('some invalid template names', () => {
    const errorType = 'NGSI_VALID_TYPE';
    const invalidTemplates = [
      {invalidName: 'asdf'},
    ];
    _.each(invalidTemplates, (invalid) => {
      const mess = `DevErr: invalid error template name: ${_.keys(invalid).pop()}`;
      assertThrows(() => generateErrors(errorType, invalid), mess);
    });
  });

  it('the error mesages must be strings', () => {
    const errorType = 'NGSI_VALID_TYPE';
    const invalidTemplateSets = [
      {VALID_NAME: 23},
      {VALID_NAME: false},
    ];
    _.each(invalidTemplateSets, (invalid) => {
      const mess = `DevErr: invalid error template message for ${_.keys(invalid).pop()}`;
      assertThrows(() => generateErrors(errorType, invalid), mess);
    });
  });

  it('A working example', () => {
    const errorType = 'NGSI_TEST_ERROR';
    const templates = {
      ERR_ONE: 'asdf',
      ERR_TWO: '',
    };
    const errors = generateErrors(errorType, templates);
    assert.strictEqual(_.size(errors), 2);

    _.each(errors, (createError) => {
      const error = createError();
      assert(createError.matches(error));
      assert(error.isNgsiError);
      assert(error.isNgsiTestError);
      assert.strictEqual(error.message, templates[error.name]);
    });

  });

});
