/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const assertRejects = require('../../src/libs/assert-rejects');

describe('assert-rejects', () => {

  it('simple working case (as envisaged, and w/out errMatch -- returns the error)', async () => {
    const rejector = async () => Promise.reject(new Error('oopsies'));
    const err = await assertRejects(rejector());
    assert.strictEqual(err.message, 'oopsies', 'should be the oopsies');
  });

  it('same simple case, entered as an async function. works the same)', async () => {
    const rejector = async () => Promise.reject(new Error('oopsies'));
    const err = await assertRejects(async () => rejector());
    assert.strictEqual(err.message, 'oopsies', 'should be the oopsies');
  });

  it('same simple case, entered as an SYNC function. works the same)', async () => {
    const rejector = async () => Promise.reject(new Error('oopsies'));
    const err = await assertRejects(() => rejector());
    assert.strictEqual(err.message, 'oopsies', 'should be the oopsies');
  });

  it('same simple case, EXCEPT completely SYNC (NOTE: with a SYNC wrapper). works the same', async () => {
    const rejector = () => {
      throw new Error('oopsies');
    };
    const err = await assertRejects(() => rejector());
    assert.strictEqual(err.message, 'oopsies', 'should be the oopsies');
  });

  it('same simple case, but SYNC and without a wrapper. This fails. Of course.', async () => {
    let failed = false;
    const rejector = () => {
      throw new Error('oopsies');
    };
    try {
      await assertRejects(rejector());
    } catch (err) {
      failed = true;
      assert.strictEqual(err.message, 'oopsies', 'should be the oopsies');
    }
    assert.strictEqual(failed, true, 'This should have failed as expected');
  });

  it('some input args that simply fail to reject', async () => {
    const nonRejectors = [
      0,
      null,
      undefined,
      [23],
      {},
      () => 0,
      async () => 0,
      Promise.resolve(new Error()),
      () => Promise.resolve(new Error()),
      async () => Promise.resolve(new Error()),
    ];
    await Promise.all(_.map(nonRejectors, async (nonRejector) => {
      let failed = false;
      try {
        await assertRejects(nonRejector);
      } catch (err) {
        const expected = 'Error: Promise arg failed to reject as expected';
        assert.strictEqual(err.message, expected, 'wrong error..');
        failed = true;
      }
      assert.strictEqual(failed, true, 'This should have failed as expected');
    }));
  });

  it('is possible to add a "matchingErr"={message} arg (optional). If the error matches, then the rejected error is returned as usual', async () => {
    const rejector = async () => Promise.reject(new Error('oopsies'));
    const matchingErr = {message: 'oopsies'};
    const err = await assertRejects(rejector(), matchingErr);
    assert.strictEqual(err.message, 'oopsies', 'should be the oopsies');
  });

  it('if a matching error is added, but err.message does not match, then REJECTS', async () => {
    const rejector = async () => Promise.reject(new Error('oopsies'));
    const matchingErr = {message: 'not a match'};
    let failed = false;
    try {
      await assertRejects(rejector(), matchingErr);
    } catch (err) {
      const expected = 'function threw, but the error messages did not match: {expected: not a match}  {actual: oopsies}';
      assert.strictEqual(err.message, expected, 'should be the oopsies');
      failed = true;
    }
    assert.strictEqual(failed, true, 'should have failed');
  });

  it('is possible to just add a string message. If the error matches, then the rejected error is returned as usual', async () => {
    const rejector = async () => Promise.reject(new Error('oopsies'));
    const err = await assertRejects(rejector(), 'oopsies');
    assert.strictEqual(err.message, 'oopsies', 'should be the oopsies');
  });

  it('if a matching messae is added, but err.message does not match, then REJECTS', async () => {
    const rejector = async () => Promise.reject(new Error('oopsies'));
    let failed = false;
    try {
      await assertRejects(rejector(), 'not a match');
    } catch (err) {
      const expected = 'function threw, but the error messages did not match: {expected: not a match}  {actual: oopsies}';
      assert.strictEqual(err.message, expected, 'should be the oopsies');
      failed = true;
    }
    assert.strictEqual(failed, true, 'should have failed');
  });

  it('if a match arg is added, but it is not a string or a message, then FAIL', async () => {
    const invalidMatchers = [
      0,
    ];
    await Promise.all(_.map(invalidMatchers, async (invalidMatch) => {
      const rejector = async () => Promise.reject(new Error('oopsies'));
      let failed = false;
      try {
        await assertRejects(rejector(), invalidMatch);
      } catch (err) {
        const expected = 'Invalid err match item for assertThrows. need a "message" or {message}';
        assert.strictEqual(err.message, expected, 'should be the exected');
        failed = true;
      }
      assert.strictEqual(failed, true, 'should have failed');
    }));
  });

});
