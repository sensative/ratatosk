/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const assertThrows = require('../../src/libs/assert-throws');

const testFn = () => {
  throw new Error('testFn');
};

describe('assert-throws.test', () => {

  it('throws an err if the first arg is not a function', () => {
    const invalids = [
      2,
      'asdf',
      null,
      undefined,
      {a: 2},
      [1, 2],
      new Promise((resolve) => resolve()),
    ];
    _.each(invalids, (invalid) => {
      let thrownErr = null;
      try {
        assertThrows(() => 0);
      } catch (err) {
        thrownErr = err;
      }
      const expected = 'the function did not throw';
      assert.strictEqual(thrownErr.message, expected, 'wrong error message..');
    });
  });

  it('throws an err if the second arg is not parseable as an error (or error message)', () => {
    const invalids = [
      2,
      {a: 2},
      [1, 2],
      new Promise((resolve) => resolve()),
    ];
    _.each(invalids, (invalid) => {
      let thrownErr = null;
      try {
        assertThrows(() => testFn(), invalid);
      } catch (err) {
        thrownErr = err;
      }
      const expected = 'Invalid err match item for assertThrows. need a "message" or {message}';
      assert.strictEqual(thrownErr.message, expected, 'wrong error message..');
    });
  });

  it('throws an err if the input function does NOT throw', () => {
    let thrownErr = null;
    try {
      assertThrows(() => 0);
    } catch (err) {
      thrownErr = err;
    }
    const expected = 'the function did not throw';
    assert.strictEqual(thrownErr.message, expected, 'wrong error message..');
  });

  it('returns err if no matching err given', () => {
    const err = assertThrows(() => testFn());
    assert.strictEqual(err.message, 'testFn', 'should be testFn');
  });

  it('returns err even IF matching err given (upon success only of course)', () => {
    const err = assertThrows(() => testFn(), {message: 'testFn'});
    assert.strictEqual(err.message, 'testFn', 'should be testFn');
  });

  it('if given a matchingErr that does NOT match, then an error is thrown', () => {
    let thrownErr = null;
    try {
      assertThrows(() => testFn(), {message: 'not a match'});
    } catch (err) {
      thrownErr = err;
    }
    const expected = 'function threw, but the error messages did not match: {expected: not a match}  {actual: testFn}';
    assert.strictEqual(thrownErr.message, expected, 'should be testFn');
  });

  it('also works with just a string is entered as matchingErr (matches against err.message)', () => {
    const err = assertThrows(() => testFn(), 'testFn');
    assert.strictEqual(err.message, 'testFn', 'should be testFn');
  });

  it('if given a matching string does NOT match, then an error is thrown', () => {
    let thrownErr = null;
    try {
      assertThrows(() => testFn(), 'not a match');
    } catch (err) {
      thrownErr = err;
    }
    const expected = 'function threw, but the error messages did not match: {expected: not a match}  {actual: testFn}';
    assert.strictEqual(thrownErr.message, expected, 'should be testFn');
  });

});
