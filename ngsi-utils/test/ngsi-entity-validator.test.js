/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const {assertThrows} = require('../src');

const {
  errors,
  validateEntity,
  validateAttrs,
  validateAttribute,
  validateMetadatum,
} = require('../src/utils/ngsi-entity-validator');

describe('ngsi-entity-validator.test', () => {

  describe('errors', () => {

    it('there are 15 ngsiEntityValidator errors', () => {
      assert.strictEqual(_.size(errors), 15);
    });

    it('the errors have some "matches" functionality and identifier flag', () => {
      const nonmatchingErr = new Error('this one does not match nuthin');
      _.each(errors, (error) => {
        const datumErr = error();
        assert(datumErr.isNgsiEntityValidationError);
        assert(error.matches(datumErr));
        assert(error.matches(datumErr.message));
        assert(!error.matches(nonmatchingErr));
        assert(!error.matches(nonmatchingErr.message));
      });
    });

  });

  describe('basic entity', () => {

    it('an entity needs to be an object, else FAIL', () => {
      const invalids = [
        2,
        true,
        false,
        null,
        'arg',
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => validateEntity(invalid), errors.INVALID_ENTITY_SHAPE());
      });
    });

    it('an entity needs a name, else FAIL', () => {
      const invalids = [
        {},
        {type: 'argh'},
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => validateEntity(invalid), errors.MISSING_ENTITY_ID());
      });
    });

    it('an entity needs a valid id, else FAIL', () => {
      const invalids = [
        {id: undefined},
        {id: 2},
        {id: ''},
        {id: '/'},
        {id: '?'},
        {id: 'aa?aa', type: 'argh'},
      ];
      _.each(invalids, (invalid) => {
        const err = assertThrows(() => validateEntity(invalid));
        assert(err.isNgsiDatumValidationError);
      });
    });

    it('needs a type, else FAIL', () => {
      const invalids = [
        {id: 'aa'},
        {id: 'aa', invalidKey: 'argh'},
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => validateEntity(invalid), errors.MISSING_ENTITY_TYPE());
      });
    });

    it('needs a valid type, else FAIL', () => {
      const invalids = [
        {id: 'aa', type: ''},
        {id: 'aa', type: 'ass//aa'},
      ];
      _.each(invalids, (invalid) => {
        const err = assertThrows(() => validateEntity(invalid));
        assert(err.isNgsiDatumValidationError);
      });
    });

    it('with valid name and type, you have an entity', () => {
      const valids = [
        {id: 'aa', type: 'argh'},
        {id: 'aa__aa', type: 'argh'},
      ];
      _.each(valids, (valid) => {
        assert.doesNotThrow(() => validateEntity(valid));
      });
    });

  });

  describe('basic metadata', () => {

    it('metadata item must be an object', () => {
      const invalids = [
        true,
        false,
        'asdf',
        2929,
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => validateMetadatum(invalid), errors.INVALID_METADATA_ITEM_SHAPE());
      });
    });

    it('metadata can only contain props "type" and "value"', () => {
      const invalids = [
        {type: 'asdf', value: 2, extraKey: 'derp'},
        {type: 'asdf', value: 2, '': 'derp'},
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => validateMetadatum(invalid), errors.INVALID_METADATA_ITEM_PROPS());
      });
    });

    it('metadata item must have a "type" prop', () => {
      const invalids = [
        {value: 2},
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => validateMetadatum(invalid), errors.MISSING_METADATA_ITEM_TYPE());
      });
    });

    it('metadata item must have a "value" prop', () => {
      const invalids = [
        {type: 'derp'},
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => validateMetadatum(invalid), errors.MISSING_METADATA_ITEM_VALUE());
      });
    });

    it('the type and value must also be valid', () => {
      const invalids = [
        {type: 'aa ', value: 29},
        {type: '', value: null},
      ];
      _.each(invalids, (invalid) => {
        const err = assertThrows(() => validateMetadatum(invalid));
        assert(err.isNgsiDatumValidationError);
      });
    });

    it('some valid metadata items', () => {
      const valids = [
        {type: 'aa', value: null},
        {type: 'aa', value: {ab: {cd: ['ef', 'gh', 1]}}},
      ];
      _.each(valids, (valid) => {
        assert.doesNotThrow(() => validateMetadatum(valid));
      });
    });

  });

  describe('basic attributes', () => {

    it('attribute must be an object', () => {
      const invalids = [
        true,
        false,
        'asdf',
        2929,
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => validateAttribute(invalid), errors.INVALID_ATTRIBUTE_SHAPE());
      });
    });

    it('attribute can only contain props "type" and "value" and metadata', () => {
      const invalids = [
        {type: 'asdf', value: 2, metadata: {}, extraKey: 'derp'},
        {type: 'asdf', value: 2, extraKey: 'derp'},
        {type: 'asdf', value: 2, '': 'derp'},
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => validateAttribute(invalid), errors.INVALID_ATTRIBUTE_PROPS());
      });
    });

    it('attribute must have a "type" prop', () => {
      const invalids = [
        {value: 2},
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => validateAttribute(invalid), errors.MISSING_ATTRIBUTE_TYPE());
      });
    });

    it('attribute must have a "value" prop', () => {
      const invalids = [
        {type: 'derp'},
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => validateAttribute(invalid), errors.MISSING_ATTRIBUTE_VALUE());
      });
    });

    it('the attribute\'s metadata container must be an object if present', () => {
      const invalids = [
        {type: 'a', value: 'b', metadata: 10},
        {type: 'a', value: 'b', metadata: true},
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => validateAttribute(invalid), errors.INVALID_METADATA_CONTAINER_SHAPE());
      });
    });

    it('the type and value and must also be valid', () => {
      const invalids = [
        {type: 'aa ', value: 29},
        {type: '', value: null},
      ];
      _.each(invalids, (invalid) => {
        const err = assertThrows(() => validateAttribute(invalid));
        assert(err.isNgsiDatumValidationError);
      });
    });

    it('some valid attribute items', () => {
      const valids = [
        {type: 'aa', value: null, metadata: []}, // <--- this one is valid: [] is an object..
        {type: 'aa', value: null}, // <--- no metadata required
        {
          type: 'aa',
          value: {ab: {cd: ['ef', 'gh', 1]}},
          metadata: {
            mv0: {
              type: 'zzzzs',
              value: {blue: 'toto'},
            },
            skaksklaksdjlks: {
              type: 'lalal',
              value: null,
            },
          },
        },
      ];
      _.each(valids, (valid) => {
        assert.doesNotThrow(() => validateAttribute(valid));
      });
    });

  });

  describe('full entity (& attrs)', () => {

    it('a reasonably complex entity should validate', () => {
      const entity = {
        id: 'lslalla',
        type: 'doopy',
        aa: {
          type: 'aa',
          value: {ab: {cd: ['ef', 'gh', 1]}},
          metadata: {
            mv0: {
              type: 'zzzzs',
              value: {blue: 'toto'},
            },
            skaksklaksdjlks: {
              type: 'lalal',
              value: null,
            },
          },
        },
        bbblsk: {
          type: 'zzz',
          value: null,
          metadata: {},
        },
      };
      assert.doesNotThrow(() => validateEntity(entity));
      const attrs = _.omit(entity, ['id', 'type']);
      assert.doesNotThrow(() => validateAttrs(attrs));
    });

  });

});
