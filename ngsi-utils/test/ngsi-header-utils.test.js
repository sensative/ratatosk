/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');

const {
  jsonContentHeader,
  textContentHeader,
  createServicePathHeader,
  extractServicePathFromHeaders,
} = require('../src/utils/ngsi-header-utils');

describe('ngsi-header-utils', () => {

  it('jsonContentHeader returns expected header object', () => {
    const headers = jsonContentHeader();
    const expected = {'Content-Type': 'application/json'};
    assert.deepStrictEqual(headers, expected, 'jsonContentHeader not working as expected');
  });

  it('textContentHeader returns expected header object', () => {
    const headers = textContentHeader();
    const expected = {'Content-Type': 'text/plain'};
    assert.deepStrictEqual(headers, expected, 'textContentHeader not working as expected');
  });

  it('createServicePathHeader returns an empty object if no (string) argument is entered', () => {
    const emptyMultiPaths = [
      [null],
      [undefined],
      [2, false, true, {}, [], null, undefined],
    ];
    _.each(emptyMultiPaths, (paths) => {
      const headers = createServicePathHeader(...paths);
      const expected = {};
      assert.deepStrictEqual(headers, expected, 'single path createServicePathHeader not as expected');
    });
  });

  it('createServicePathHeader returns the expected header object (with a single servicePath -- NO validation is done!!)', () => {
    const singlePathSets = [
      {path: '/', res: '/'},
      {path: '', res: ''},
      {path: 'jibbety', res: 'jibbety'},
      {path: 'ill   egal', res: 'ill   egal'},
    ];
    _.each(singlePathSets, ({path, res}) => {
      const headers = createServicePathHeader(path);
      const expected = {'Fiware-ServicePath': res};
      assert.deepStrictEqual(headers, expected, 'single path createServicePathHeader not as expected');
    });
  });

  it('createServicePathHeader returns the expected header object (with multiple servicePath -- NO validation is done!!)', () => {
    const multiPathSets = [
      {paths: ['/', '/some', '/valid/and/normal/paths'], res: '/,/some,/valid/and/normal/paths'},
      {paths: ['/', 'asdf', ',', '///'], res: '/,asdf,,,///'},
    ];
    _.each(multiPathSets, ({paths, res}) => {
      const headers = createServicePathHeader(...paths);
      const expected = {'Fiware-ServicePath': res};
      assert.deepStrictEqual(headers, expected, 'multiple paths createServicePathHeader not as expected');
    });
  });

  it('extractServicePathFromHeaders just blindly unwraps & returns what is stored in the servicePath header prop', () => {
    const values = [
      'invalid service path',
      '/valid/service/path',
      '/valid,/paths',
      null,
      undefined,
      2,
      [],
      {},
    ];
    _.each(values, (val) => {
      const headers = {'Fiware-ServicePath': val};
      const res = extractServicePathFromHeaders(headers);
      assert.strictEqual(res, val);
    });
  });

});
