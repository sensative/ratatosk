/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');

const {
  getResponseCount,
  getResponseLimit,
  getResponseOffset,
  getNumDocs,
  createPaginator,
} = require('../src/utils/ngsi-query-response-utils');

describe('ngsi-query-response-utils', () => {

  describe('getResponseCount', () => {

    it('some working examples', () => {
      const valids = [
        {'fiware-total-count': 37},
        {headers: {'fiware-total-count': 37}},
      ];
      _.each(valids, (valid) => {
        const count = getResponseCount(valid);
        assert.strictEqual(count, 37, 'count should be 37');
      });
    });

    it('some examples that do not have a count', () => {
      const invalids = [
        null,
        {},
        [],
        'asdf',
      ];
      _.each(invalids, (invalid) => {
        const count = getResponseCount(invalid);
        assert.strictEqual(count, null, 'count should be "null"');
      });
    });

  });

  describe('getResponseLimit', () => {

    it('some working examples', () => {
      const valids = [
        {limit: 171},
        {params: {limit: 171}},
        {config: {params: {limit: 171}}},
      ];
      _.each(valids, (valid) => {
        const limit = getResponseLimit(valid);
        assert.strictEqual(limit, 171, 'limit should be 171');
      });
    });

    it('some examples that do not have a limit (gets set to default = 20)', () => {
      const DEFAULT_LIMIT = 20;
      const invalids = [
        null,
        {},
        [],
        'asdf',
      ];
      _.each(invalids, (invalid) => {
        const limit = getResponseLimit(invalid);
        assert.strictEqual(limit, DEFAULT_LIMIT, 'limit should be 20');
      });
    });

  });

  describe('getResponseOffset', () => {

    it('some working examples', () => {
      const valids = [
        {offset: 45},
        {params: {offset: 45}},
        {config: {params: {offset: 45}}},
      ];
      _.each(valids, (valid) => {
        const offset = getResponseOffset(valid);
        assert.strictEqual(offset, 45, 'offset should be 45');
      });
    });

    it('some examples that do not have an offset (gets set to 0)', () => {
      const invalids = [
        null,
        {},
        [],
        'asdf',
      ];
      _.each(invalids, (invalid) => {
        const offset = getResponseOffset(invalid);
        assert.strictEqual(offset, 0, 'offset should be 0');
      });
    });

  });

  describe('getNumDocs', () => {

    it('some working examples', () => {
      const valids = [
        {data: {a: 2, b: 3, c: 'asdf'}},
        {data: [1, 2, 3]},
      ];
      _.each(valids, (valid) => {
        const numDocs = getNumDocs(valid);
        assert.strictEqual(numDocs, 3, 'there should be 3 docs in all cases');
      });
    });

    it('some examples that do not have an offset (gets set to 0)', () => {
      const invalids = [
        null,
        {},
        [1, 2, 3],
        'asdf',
        {data: []}, // note that this one IS valid, but DOES have 0
      ];
      _.each(invalids, (invalid) => {
        const numDocs = getNumDocs(invalid);
        assert.strictEqual(numDocs, 0, 'numDocs should be 0 in all these cases');
      });
    });

  });

  describe('the paginator is really just an aggregation of the others, along with some derived info', () => {

    it('a fully working example', () => {
      const validRes = {
        headers: {'fiware-total-count': 333},
        config: {params: {
          offset: 25,
          limit: 5,
        }},
        data: [1, 2, 3, 4, 5],
      };
      const expected = {
        count: 333,
        offset: 25,
        limit: 5,
        docs: [1, 2, 3, 4, 5],
        numDocs: 5,
        isFull: true,
        page: 5, // page is zero-indexed
        pageOffset: 0,
        nextOffset: 30,
        hasNextPage: true,
      };
      const paginator = createPaginator(validRes);
      assert.deepStrictEqual(paginator, expected);
    });

    it('a fully broken example', () => {
      const invalidRes = null;
      const expected = {
        count: null,
        offset: 0,
        limit: 20,
        docs: undefined,
        numDocs: 0,
        isFull: false,
        page: 0,
        pageOffset: 0,
        nextOffset: 20,
        hasNextPage: false,
      };
      const paginator = createPaginator(invalidRes);
      assert.deepStrictEqual(paginator, expected);
    });

  });

});
