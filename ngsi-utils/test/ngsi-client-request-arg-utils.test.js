/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');

const {
  REST_REQUEST_HEADER_KEYS,
} = require('@ratatosk/ngsi-constants');

const {
  assertThrows,
} = require('../src');

const {
  errors,
  // bulk query
  extractBulkQueryPayload,
  // bulk update
  extractBulkUpdatePayload,
  // subscriptions
  extractCreateSubscriptionPayload,
  // registrations
  extractCreateRegistrationPayload,
  // all entities
  validateEntityRequestArgShape,
  extractRequestArgHeaders,
  extractRequestArgParams,
  extractRequestArgEntity,
  extractRequestArgEntityId,
  extractRequestArgAttrs,
  extractRequestArgAttrName,
  extractRequestArgAttr,
  extractRequestArgAttrValue,
} = require('../src/utils/ngsi-client-request-arg-utils');

describe('ngsi-client-request-arg-utils.test', () => {

  describe('errors', () => {

    it('there are 10 isNgsiClientRequestArgError errors', () => {
      assert.strictEqual(_.size(errors), 10);
    });

    it('the errors have some "matches" functionality and identifier flag', () => {
      const nonmatchingErr = new Error('this one does not match nuthin');
      _.each(errors, (error) => {
        const datumErr = error();
        assert(datumErr.isNgsiClientRequestArgError);
        assert(error.matches(datumErr));
        assert(error.matches(datumErr.message));
        assert(!error.matches(nonmatchingErr));
        assert(!error.matches(nonmatchingErr.message));
      });
    });

  });

  describe('createRegistration', () => {

    it('must be an object', () => {
      const nonObjects = [
        'asdf',
        true,
        10,
        undefined,
        null,
        false,
        0,
      ];
      _.each(nonObjects, (nonner) => {
        assertThrows(() => extractCreateRegistrationPayload(nonner), errors.INVALID_CREATE_REGISTRATION_ARG_NON_OBJECT());
      });
    });

    it('no unexpected props are allowed', () => {
      const minimalInvalid = {
        invalidKey: 'asdf',
      };
      assertThrows(() => extractCreateRegistrationPayload(minimalInvalid), errors.INVALID_REGISTRATION_ARG_KEYS());
    });

    it('if the shape looks right, then it should work like formatRegistrationPayload', () => {
      const valids = [
        {
          in: {
            payload: {
              provider: {http: {url: 'theurl'}},
              dataProvided: {entities: [{id: 'yep'}]},
            },
          },
          out: {
            provider: {http: {url: 'theurl'}},
            dataProvided: {entities: [{id: 'yep'}]},
          },
        },
      ];
      _.each(valids, (valid) => {
        const res = extractCreateRegistrationPayload(valid.in);
        assert.deepStrictEqual(res, valid.out);
      });
    });

  });

  describe('createSubscription', () => {

    it('must be an object', () => {
      const nonObjects = [
        'asdf',
        true,
        10,
        undefined,
        null,
        false,
        0,
      ];
      _.each(nonObjects, (nonner) => {
        assertThrows(() => extractCreateSubscriptionPayload(nonner), errors.INVALID_CREATE_SUBSCRIPTION_ARG_NON_OBJECT());
      });
    });

    it('no unexpected props are allowed', () => {
      const minimalInvalid = {
        invalidKey: 'asdf',
      };
      assertThrows(() => extractCreateSubscriptionPayload(minimalInvalid), errors.INVALID_SUBSCRIPTION_ARG_KEYS());
    });

    it('if the shape looks right, then it should work like formatCreateSubscriptionPayload', () => {
      const valids = [
        {
          in: {
            payload: {
              subject: {
                entities: [{id: 'green'}],
              },
              notification: {
                http: {url: 'brromp'},
              },
            },
          },
          out: {
            subject: {
              entities: [{id: 'green'}],
            },
            notification: {
              http: {url: 'brromp'},
            },
          },
        },
      ];
      _.each(valids, (valid) => {
        const res = extractCreateSubscriptionPayload(valid.in);
        assert.deepStrictEqual(res, valid.out);
      });
    });

  });

  describe('extractBulkUpdatePayload', () => {

    it('must be an object', () => {
      const nonObjects = [
        'asdf',
        true,
        10,
        undefined,
        null,
        false,
        0,
      ];
      _.each(nonObjects, (nonner) => {
        assertThrows(() => extractBulkUpdatePayload(nonner), errors.INVALID_BULK_UPDATE_ARG_NON_OBJECT());
      });
    });

    it('no unexpected props are allowed', () => {
      const noInvalidKeys = {
        headers: undefined,
        payload: undefined,
        userId: undefined,
      };
      const err = extractBulkUpdatePayload(noInvalidKeys);
      assert.notStrictEqual(err.message, errors.INVALID_BULK_UPDATE_ARG_KEYS().message);
      const maximalValidPlusOne = {
        headers: undefined,
        payload: undefined,
        invalidKey: 'bla',
      };
      assertThrows(() => extractBulkUpdatePayload(maximalValidPlusOne), errors.INVALID_BULK_UPDATE_ARG_KEYS());
      const minimalInvalid = {
        invalidKey: 'asdf',
      };
      assertThrows(() => extractBulkUpdatePayload(minimalInvalid), errors.INVALID_BULK_UPDATE_ARG_KEYS());
    });

    it('if the shape looks right, then it should work like formatBulkUpdatePayload', () => {
      const valids = [
        {
          in: {payload: {actionType: 'delete', entities: [{id: 'blue'}]}},
          out: {actionType: 'delete', entities: [{id: 'blue', type: 'Thing'}]},
        },
      ];
      _.each(valids, (valid) => {
        const res = extractBulkUpdatePayload(valid.in);
        assert.deepStrictEqual(res, valid.out);
      });
    });

  });

  describe('extractBulkQueryPayload', () => {

    it('must be an object', () => {
      const nonObjects = [
        'asdf',
        true,
        10,
        undefined,
        null,
        false,
        0,
      ];
      _.each(nonObjects, (nonner) => {
        assertThrows(() => extractBulkQueryPayload(nonner), errors.INVALID_BULK_QUERY_ARG_NON_OBJECT());
      });
    });

    it('no unexpected props are allowed', () => {
      // first the keys that ARE allowed
      const noInvalidKeys = {
        headers: undefined,
        payload: undefined,
        userId: undefined,
      };
      const err = extractBulkUpdatePayload(noInvalidKeys);
      assert.notStrictEqual(err.message, errors.INVALID_BULK_UPDATE_ARG_KEYS().message);
      // and then one that isn't
      const maximalValidPlusOne = {
        headers: undefined,
        payload: undefined,
        userId: undefined,
        invalidKey: 'bla',
      };
      assertThrows(() => extractBulkQueryPayload(maximalValidPlusOne), errors.INVALID_BULK_QUERY_ARG_KEYS());
      const minimalInvalid = {
        invalidKey: 'asdf',
      };
      assertThrows(() => extractBulkQueryPayload(minimalInvalid), errors.INVALID_BULK_QUERY_ARG_KEYS());
    });

    it('if the shape looks right, then it should work like formatBulkQueryPayload', () => {
      const valids = [
        {in: {payload: {attrs: ['asdf']}}, out: {attrs: ['asdf']}},
      ];
      _.each(valids, (valid) => {
        const res = extractBulkQueryPayload(valid.in);
        assert.deepStrictEqual(res, valid.out);
      });
    });

  });

  describe('validateEntityRequestArgShape', () => {

    it('must be an object', () => {
      const nonObjects = [
        'asdf',
        true,
        10,
      ];
      _.each(nonObjects, (nonner) => {
        assertThrows(() => validateEntityRequestArgShape(nonner), errors.INVALID_ENTITY_ARG_NON_OBJECT());
      });
    });

    it('falsey args are allowed', () => {
      const validEmpties = [
        undefined,
        null,
        false,
        0,
        '',
      ];
      _.each(validEmpties, (valid) => {
        assert.doesNotThrow(() => validateEntityRequestArgShape(valid));
      });
    });


    it('fill up all allowed values (the contents of which are NOT validated)', () => {
      const maximalValid = {
        headers: undefined,
        params: undefined,
        entity: undefined,
        entityId: undefined,
        attrName: undefined,
        attrValue: undefined,
        servicePath: undefined,
        servicePaths: undefined,
        userId: undefined,
      };
      assert.doesNotThrow(() => validateEntityRequestArgShape(maximalValid));
    });

    it('no unexpected props are allowed', () => {
      const maximalValidPlusOne = {
        headers: undefined,
        params: undefined,
        entity: undefined,
        entityId: undefined,
        attrName: undefined,
        attrValue: undefined,
        servicePath: undefined,
        servicePaths: undefined,
        invalidKey: 'bla',
        userId: 'asdf',
      };
      assertThrows(() => validateEntityRequestArgShape(maximalValidPlusOne), errors.invalid_);
      const minimalInvalid = {
        invalidKey: 'asdf',
      };
      assertThrows(() => validateEntityRequestArgShape(minimalInvalid), errors.INVALID_ENTITY_ARG_KEYS());
    });

  });

  describe('extractRequestArgHeaders', () => {

    it('if no servicePath props are included, then requestArg.headers is returned', () => {
      const headersList = [
        null,
        undefined,
        {},
        {a: 2},
      ];
      _.each(headersList, (inputHeaders) => {
        const headers = extractRequestArgHeaders({headers: inputHeaders});
        assert.deepStrictEqual(headers, inputHeaders || {});
      });
    });

    it('if servicePath is included in arg.headers, then arg.servicePath & arg.servicePaths are ignored', () => {
      const headersList = [
        {'Fiware-ServicePath': '/asdf', entity: 2},
        {'Fiware-ServicePath': '/asdf', entity: 2, servicePath: '/bb'},
        {'Fiware-ServicePath': '/asdf', entity: 2, servicePaths: '/bb,/cc'},
        {'Fiware-ServicePath': '/asdf', entity: 2, servicePath: '/bb', servicePaths: '/ignored'},
      ];
      _.each(headersList, (inputHeaders) => {
        const headers = extractRequestArgHeaders({headers: inputHeaders});
        assert.notStrictEqual(headers, inputHeaders);
        assert.deepStrictEqual(headers, inputHeaders);
      });
    });

    it('if servicePath is NOT included in arg.headers, then arg.servicePath (first) and arg.servicePaths (second) are considered', () => {
      const argsList = [
        {headers: {entity: 2}, servicePath: '/a'},
        {headers: {entity: 2}, servicePaths: '/a'},
        {headers: {entity: 2}, servicePath: '/a', servicePaths: '/b'},
      ];
      _.each(argsList, (arg) => {
        const expected = {entity: 2, 'Fiware-ServicePath': '/a'};
        const headers = extractRequestArgHeaders(arg);
        assert.deepStrictEqual(headers, expected);
      });
    });

    it('if a service path is included that is not valid, then it fails from servicePath validation', () => {
      const invalidArgsList = [
        {headers: {entity: 2, 'Fiware-ServicePath': '/b,/a a'}},
        {headers: {entity: 2}, servicePath: '/b,/a a'},
        {headers: {entity: 2}, servicePath: '/b,/a a'},
      ];
      _.each(invalidArgsList, (invalidArg) => {
        assertThrows(() => extractRequestArgHeaders(invalidArg));
      });
    });

    it('make sure that any Fiware-UserIds get preserved', () => {
      const userIds = [
        'asdfasdf10293',
        23, // this value is not getting validated
      ];
      _.each(userIds, (userId) => {
        const rawHeaders = {[REST_REQUEST_HEADER_KEYS.USER_ID]: userId};
        const headers = extractRequestArgHeaders({headers: rawHeaders});
        const expected = {'Fiware-UserId': userId};
        assert.deepStrictEqual(headers, expected);
      });
    });

    it('make sure we can set the Fiware-UserId using "userId" key', () => {
      const userId = 'whatever';
      const headers = extractRequestArgHeaders({userId});
      const expected = {'Fiware-UserId': userId};
      assert.deepStrictEqual(headers, expected);
    });

    it('Fiware-Upsert flag gets set to true when included (in headers ONLY (?))', () => {
      const valids = [true, 1, 'asdf', {}];
      _.each(valids, (valid) => {
        const rawHeaders = {[REST_REQUEST_HEADER_KEYS.UPSERT]: valid};
        const headers = extractRequestArgHeaders({headers: rawHeaders});
        const expected = {'Fiware-Upsert': true};
        assert.deepStrictEqual(headers, expected);
      });
    });

  });

  describe('extractRequestArgParams', () => {

    it('extracts and validates the params, if invalid then empty', () => {
      const invalidParamArgs = [
        {params: null},
        {params: {herpy: 'derp'}},
        null,
        {},
      ];
      _.each(invalidParamArgs, (arger) => {
        const params = extractRequestArgParams(arger);
        assert.deepStrictEqual(params, {});
      });
    });

    it('otherwise it massages and extracts the params. Here is an example', () => {
      const reqArg = {
        params: {
          q: 'a>2',
          mq: 'a.b<=3;c.d==asdfds',
          type: 'blue',
          idPattern: 'somehinge',
          count: true,
          representationType: 'values',
        },
      };
      const params = extractRequestArgParams(reqArg);
      const expected = {
        idPattern: 'somehinge',
        type: 'blue',
        options: 'count,values',
        q: 'a>2',
        mq: 'a.b<=3;c.d==asdfds',
      };
      assert.deepStrictEqual(params, expected);
    });

  });

  describe('extractRequestArgEntity', () => {

    it('returns undefined if reqArg.entity is falsy', () => {
      const falsies = [
        {entity: undefined},
        {entity: false},
        {entity: 0},
        {entity: null},
      ];
      _.each(falsies, (falsy) => {
        const entity = extractRequestArgEntity(falsy);
        assert.strictEqual(entity, undefined);
      });
    });

    it('formats the entity if there (using ngsi-entity-formatter)', () => {
      const valids = [
        {entity: {id: 'a', type: 'b', c: 'c'}},
        {entity: {id: 'a', type: 'b', c: {type: 'Text', value: 'c'}}},
        // {entity: {id: 'a', type: 'b', c: {type: 'Text', value: 'c'}}, metadata: {}},
      ];
      _.each(valids, (valid) => {
        const entity = extractRequestArgEntity(valid);
        const expected = {id: 'a', type: 'b', c: {type: 'Text', value: 'c', metadata: {}}};
        assert.deepStrictEqual(entity, expected);
      });
    });

  });

  describe('extractRequestArgEntityId', () => {

    it('will return whatever is in reqArg.entityId', () => {
      const explicitIds = [
        {entityId: 'validId'},
        {entityId: 'invalid id'},
        {entityId: 0},
        {entityId: null},
      ];
      _.each(explicitIds, (exper) => {
        const entityId = extractRequestArgEntityId(exper);
        assert.strictEqual(entityId, exper.entityId);
      });
    });

    it('will also look in req.entity. here are some equivalents', () => {
      const equivalents = [
        {entity: {id: 'derpy'}},
        {entityId: 'derpy'},
      ];
      _.each(equivalents, (equiv) => {
        const entityId = extractRequestArgEntityId(equiv);
        assert.strictEqual(entityId, 'derpy');
      });
    });

  });

  describe('extractRequestArgAttrs', () => {

    it('will return undefined if no attrs in any way specified', () => {
      const noAttrsList = [
        {},
        null,
      ];
      _.each(noAttrsList, (noAttr) => {
        const attrs = extractRequestArgAttrs(noAttr);
        assert.strictEqual(attrs, undefined);
      });
    });

    it('if reqArg.attrs is included but falsety it returns undefined', () => {
      const dodgyAttrs = [
        {attrs: null},
        {attrs: false},
        {attrs: false},
        {attrs: null, entity: {id: 'a', type: 'b', c: 'c'}},
      ];
      _.each(dodgyAttrs, (dodgy) => {
        const attrs = extractRequestArgAttrs(dodgy);
        assert.deepStrictEqual(attrs, undefined);
      });
    });

    it('if reqArg.attrs is truthy, then it should be an object', () => {
      const invalids = [
        {attrs: 10},
        {attrs: 'whatever'},
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => extractRequestArgAttrs(invalid), 'attrs should be an object');
      });
    });

    it('extractRequestArgAttrs formats the attrs, and tries to look in reqArg.entity if reqArg.attrs not set', () => {
      const equivalents = [
        {attrs: {a: 'a'}},
        {attrs: {a: {type: 'Text', value: 'a'}}},
        {entity: {id: 'a', type: 'b', a: 'a'}},
        {attrs: undefined, entity: {id: 'a', type: 'b', a: 'a'}},
        {entity: {id: 'a', type: 'b', a: {type: 'Text', value: 'a'}}},
      ];
      _.each(equivalents, (equiv) => {
        const attrs = extractRequestArgAttrs(equiv);
        const expected = {a: {type: 'Text', value: 'a', metadata: {}}};
        assert.deepStrictEqual(attrs, expected);
      });
    });

  });

  describe('extractRequestArgAttrName', () => {

    it('just returns whatever is in reqArg.attrName', () => {
      const reqArgs = [
        {},
        {attrName: null},
        {attrName: 'validName'},
        {attrName: 'invalid name'},
      ];
      _.each(reqArgs, (arg) => {
        const attrName = extractRequestArgAttrName(arg);
        assert.strictEqual(attrName, arg.attrName);
      });
    });

  });

  describe('extractRequestArgAttr', () => {

    it('will return undefined if no attr in any way specified', () => {
      const noAttrList = [
        {},
        null,
      ];
      _.each(noAttrList, (noAttr) => {
        const attr = extractRequestArgAttr(noAttr);
        assert.strictEqual(attr, undefined);
      });
    });

    it('if reqArg.attr is included but falsey it returns undefined', () => {
      const dodgyAttr = [
        {attr: null},
        {attr: false},
        {attr: false},
        {attr: null, entity: {id: 'a', type: 'b', c: 'd'}, attrName: 'c'},
      ];
      _.each(dodgyAttr, (dodgy) => {
        const attr = extractRequestArgAttr(dodgy);
        assert.deepStrictEqual(attr, undefined);
      });
    });

    it('there are several ways to specify the relevant attr', () => {
      const equivalents = [
        {attr: 10},
        {attr: {value: 10, type: 'Number'}},
        {attrName: 'derp', entity: {id: 'a', type: 'b', derp: 10}},
        {attrName: 'derp', entity: {id: 'a', type: 'b', derp: {value: 10, type: 'Number'}}},
        {attrName: 'derp', attrs: {derp: 10}},
        {attrName: 'derp', attrs: {derp: {value: 10, type: 'Number'}}},
      ];
      _.each(equivalents, (equiv) => {
        const attr = extractRequestArgAttr(equiv);
        const expected = {value: 10, type: 'Number', metadata: {}};
        assert.deepStrictEqual(attr, expected);
      });
    });

  });

  describe('extractRequestArgAttrValue', () => {

    it('if reqArg.attrValue is explicitly specified, then that is returned', () => {
      const attrValueList = [
        {},
        {attrValue: null},
        {attrValue: 'asdf'},
        {attrValue: false},
        {attrValue: {slksdlksd: 2}},
        {attrValue: [2, 3, 4]},
      ];
      _.each(attrValueList, (valer) => {
        const attrValue = extractRequestArgAttrValue(valer);
        assert.strictEqual(attrValue, valer.attrValue);
      });
    });

    it('there are several ways to specify the relevant attrValue', () => {
      const equivalents = [
        {attrValue: 10},
        {attr: 10},
        {attr: {value: 10, type: 'Number'}},
        {attrName: 'derp', entity: {id: 'a', type: 'b', derp: 10}},
        {attrName: 'derp', entity: {id: 'a', type: 'b', derp: {value: 10, type: 'Number'}}},
        {attrName: 'derp', attrs: {derp: 10}},
        {attrName: 'derp', attrs: {derp: {value: 10, type: 'Number'}}},
      ];
      _.each(equivalents, (equiv) => {
        const attr = extractRequestArgAttrValue(equiv);
        const expected = 10;
        assert.strictEqual(attr, expected);
      });
    });

  });

});
