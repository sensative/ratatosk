/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const path = require('path');
const {lint} = require('@ratatosk/ngsi-linter');

describe('ngsi-utils', () => {

  it('testing ngsi-utils main describe', () => 0);

  // ---- lint
  it('linting', async () => {
    await lint(path.join(__dirname, '..'));
  });

  // // ---- libs
  describe('ngsi generic libs', () => {
    require('./libs.test/assert-throws.test');
    require('./libs.test/assert-rejects.test');
    require('./libs.test/ngsi-tokenize.test');
    require('./libs.test/ngsi-error-generator.test');
  });

  // // ---- utils
  require('./ngsi-date-time-utils.test');
  require('./ngsi-location-utils.test');
  require('./ngsi-datum-validator.test');
  require('./ngsi-query-params-utils.test');
  require('./ngsi-entity-validator.test');
  require('./ngsi-sql.test');
  require('./ngsi-response-error-utils.test');
  require('./ngsi-header-utils.test');
  require('./ngsi-mock-storage.test');
  require('./ngsi-query-response-utils.test');
  require('./ngsi-service-path-utils.test');
  require('./ngsi-attribute-value-formatter.test');
  require('./ngsi-entity-formatter.test');
  require('./ngsi-client-request-arg-utils.test');
  require('./ngsi-storage-error-utils.test');

});
