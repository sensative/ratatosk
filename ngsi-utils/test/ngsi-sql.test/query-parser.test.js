/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const {assertThrows} = require('../../src');

const {INVALID_SERIALIZE_ARG} = require('../../src/utils/ngsi-sql/errors');

const {
  parseQueryString,
  serializeQueryList,
  validateQueryString,
  validateQueryList,
} = require('../../src/utils/ngsi-sql/query-parser');

describe('query-parser (TOP LEVEL)', () => {

  describe('parseQueryString', () => {

    it('we should be able to parse a q query of some complexity', () => {
      const query = 'sdf.sdf.sdf==20;derp.\'sldf.deo\'~=sdfsdk;!many.depp';
      const predicates = parseQueryString(query);
      assert(predicates.length === 3);
      const expected0 = {
        predicateType: 'binary',
        tokens: ['sdf', 'sdf', 'sdf'],
        op: '==',
        value: 20,
      };
      assert(_.isEqual(expected0, predicates[0]));
      const expected1 = {
        predicateType: 'binary',
        tokens: ['derp', 'sldf.deo'],
        op: '~=',
        value: 'sdfsdk',
      };
      assert(_.isEqual(expected1, predicates[1]));
      const expected2 = {
        predicateType: 'unary',
        tokens: ['many', 'depp'],
        isNegatory: true,
      };
      assert(_.isEqual(expected2, predicates[2]));
    });

    it('we should be able to parse an mq query of some complexity', () => {
      const query = `sdf.sdf.sdf==20;derp.'sldf,deo'~=sdfsdk;!many.depp`;
      const predicates = parseQueryString(query, 'mq');
      assert(predicates.length === 3, 'wrong number of predicates');
      const expected0 = {
        predicateType: 'binary',
        tokens: ['sdf', 'sdf', 'sdf'],
        op: '==',
        value: 20,
      };
      assert(_.isEqual(expected0, predicates[0]));
      const expected1 = {
        predicateType: 'binary',
        tokens: ['derp', 'sldf,deo'],
        op: '~=',
        value: 'sdfsdk',
      };
      assert(_.isEqual(expected1, predicates[1]));
      const expected2 = {
        predicateType: 'unary',
        tokens: ['many', 'depp'],
        isNegatory: true,
      };
      assert(_.isEqual(expected2, predicates[2]));
    });

    it('should fail to make an mq query with only one token', () => {
      const query = 'a==0';
      assert.throws(() => parseQueryString(query, 'mq'));
      assert.doesNotThrow(() => parseQueryString('a.b==0', 'mq'));
    });

  });

  describe('serializeQueryList', () => {

    it('the serializer argument must be an array', () => {
      assertThrows(() => serializeQueryList('mustbearray'), INVALID_SERIALIZE_ARG());
    });

    it('it should serialize a query of some complexity', () => {
      const query = [
        {
          predicateType: 'unary',
          tokens: ['asdf'],
        },
        {
          predicateType: 'binary',
          op: '<=',
          tokens: ['sdf', 'jsksk...slsl', 'sksks'],
          value: '293.24',
        },
        {
          predicateType: 'unary',
          tokens: ['dippy', 'mc.do'],
          isNegatory: true,
        },
      ];
      const res = serializeQueryList(query);
      const expected = `asdf;sdf.'jsksk...slsl'.sksks<='293.24';!dippy.'mc.do'`;
      assert.strictEqual(res, expected);
    });

  });

  describe('check IDENTITY (parseQueryString & serializeQueryList are inverses)', () => {

    it('check that x === serializeQueryList(parseQueryString(x)) for a "q" query (also validate)', () => {
      const predicateStrings = [
        `aa,bb.cc,dd.ee==23.345`,
        '!asdf',
        `alksk.s,s,s,s.kdp`,
      ];
      const queryString = predicateStrings.join(';');
      const queryList = parseQueryString(queryString, 'q');
      assert.doesNotThrow(() => validateQueryList(queryList));
      const restring = serializeQueryList(queryList);
      assert.strictEqual(restring, queryString);
      assert.doesNotThrow(() => validateQueryString(queryString, 'q'), 'should validate..');
    });

    it('check that x === serializeQueryList(parseQueryString(x)) for an "mq" query (also validate)', () => {
      const predicateStrings = [
        '!asdf.dd',
        `alksk.sks.kdp`,
      ];
      const queryString = predicateStrings.join(';');
      const queryList = parseQueryString(queryString, 'mq');
      assert.doesNotThrow(() => validateQueryList(queryList));
      const restring = serializeQueryList(queryList, 'mq');
      assert.strictEqual(restring, queryString);
      assert.doesNotThrow(() => validateQueryString(queryString, 'mq'), 'mq string should validate');
    });

    it('check that y = parseQueryString(serializeQueryList(y)) for a "q" query', () => {
      const query = [
        {
          predicateType: 'binary',
          tokens: ['aa', 'bb', 'cc.dd', 'ee'],
          op: '==',
          value: 23.345,
        },
        {
          predicateType: 'binary',
          tokens: ['a'],
          op: '==',
          value: {min: new Date(), max: new Date()},
        },
        {
          predicateType: 'binary',
          tokens: ['a'],
          op: '==',
          value: ['a', null, 2, new Date()],
        },
        {
          predicateType: 'unary',
          tokens: ['sks'],
          isNegatory: false, // autofilled default
        },
        {
          predicateType: 'unary',
          tokens: ['dkd', 'slsls.ldkdj..'],
          isNegatory: true,
        },
      ];
      const res = parseQueryString(serializeQueryList(query, 'q'));
      assert.deepStrictEqual(res, query);
    });

    it('check that y = parseQueryString(serializeQueryList(y)) for an "mq" query', () => {
      const query = [
        {
          predicateType: 'binary',
          tokens: ['aa', 'bb', 'cc.dd', 'ee'],
          op: '==',
          value: '23.345',
        },
        {
          predicateType: 'unary',
          tokens: ['sks', 'needASecondTokenHere'],
          isNegatory: false, // autofilled default
        },
        {
          predicateType: 'unary',
          tokens: ['dkd', 'slsls.ldkdj..'],
          isNegatory: true,
        },
      ];
      const res = parseQueryString(serializeQueryList(query, 'mq'));
      assert.deepStrictEqual(res, query);
    });

  });

});
