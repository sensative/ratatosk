/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const {assertThrows} = require('../../src');

const errors = require('../../src/utils/ngsi-sql/errors');
const {validateQueryType} = require('../../src/utils/ngsi-sql/query-type');
const {
  validateTokens,
  parseTokens,
  serializeTokens,
} = require('../../src/utils/ngsi-sql/token-parser');

describe('token-parser', () => {

  describe('validateTokens', () => {

    it('validateTokens requires a non-zero length array', () => {
      const invalids = [
        {a: 2},
        null,
        [],
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => validateTokens(invalid), errors.INVALID_TOKENS_SHAPE());
      });
    });

    it('validateTokens token array items must be strings', () => {
      const invalids = [
        [2],
        ['a', 'b', undefined],
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => validateTokens(invalid), errors.INVALID_TOKEN_TYPE());
      });
    });

    it('mq tokens require at least two items', () => {
      const invalidMq = ['asdf'];
      assertThrows(() => validateTokens(invalidMq, 'mq'), errors.TOO_FEW_MQ_TOKENS());
    });

    it('some valid "q" tokens', () => {
      const valids = [
        ['a'],
        ['slsk', '', 'a  a', ''],
      ];
      _.each(valids, (valid) => {
        assert.doesNotThrow(() => validateTokens(valid)); // default type is 'q'
        assert.doesNotThrow(() => validateTokens(valid, 'q'));
      });
    });

    it('some valid "mq" tokens', () => {
      const valids = [
        ['a', 'b'],
      ];
      _.each(valids, (valid) => {
        assert.doesNotThrow(() => validateTokens(valid, 'mq'));
      });
    });

    it('the first token must always validate as an attributeName', () => {
      const invalids = [
        [''],
        ['#'],
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => validateTokens(invalid), errors.INVALID_ATTRIBUTE_TOKEN());
      });
    });

    it('the second token must, for "mq" only, validate as an metadataName', () => {
      const mqInvalids = [
        ['a', ''],
        ['a', 'ha#h'],
      ];
      _.each(mqInvalids, (invalid) => {
        assertThrows(() => validateTokens(invalid, 'mq'), errors.INVALID_METADATA_TOKEN());
      });
    });
  });

  describe('parseTokens', () => {

    it('the input query type is validated', () => {
      const invalidTypes = [
        'mqq',
        'derp',
        '',
      ];
      _.each(invalidTypes, (invalid) => {
        const validationErr = assertThrows(() => validateQueryType(invalid));
        assertThrows(() => parseTokens('validToken', invalid), validationErr);
      });
    });

    it('some valid q tokens', () => {
      const valids = [
        {in: 'abc', out: ['abc']},
        {in: 'aa.\'bb.cc\'', out: ['aa', 'bb.cc']},
      ];
      _.each(valids, (valid) => {
        const res = parseTokens(valid.in);
        assert.deepStrictEqual(res, valid.out);
      });
    });

    it('some valid mq tokens', () => {
      const valids = [
        {in: 'abc.a', out: ['abc', 'a']},
        {in: 'aa.\'bb,cc\'', out: ['aa', 'bb,cc']},
        {in: 'aa.\'bb,cc\'..', out: ['aa', 'bb,cc', '', '']},
      ];
      _.each(valids, (valid) => {
        const res = parseTokens(valid.in, 'mq');
        assert.deepStrictEqual(res, valid.out);
      });
    });

    it('tokenString must be a non-empty string', () => {
      const invalids = [
        23,
        [],
        '',
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => parseTokens(invalid), errors.INVALID_TOKENS_STRING());
      });
    });

    it('some malformed tokens with groupings (token groupers MUST come in pairs)', () => {
      const invalids = [
        '\'', // '
        '\'.', // '.
        '.\'', // .'
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => parseTokens(invalid), errors.INVALID_NUM_TOKEN_GROUPS());
      });
    });

    it('the relationship between token grouper & token separator is pretty constrained', () => {
      const invalids = [
        'a\'a\'', // a'a'
        'a.a\'a\'', // a.a'a'
        '\'a\'a', // 'a'a
        '\'a\'a.a', // 'a'a
        '\'a\'\'a\'', // 'a''a'
        '\'a\'a\'a\'', // 'a'a'a'
        '\'a\'a.a\'a\'', // 'a'a.a'a'
        '\'a\'a.a.a\'a\'', // 'a'a.a.a'a'
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => parseTokens(invalid), errors.INVALID_TOKEN_GROUPING());
      });
    });

  });

  describe('serializeTokens', () => {

    it('the input tokens array is validated', () => {
      const invalids = [
        19,
        [],
        [''],
        ['a', 23, 'b'],
      ];
      _.each(invalids, (invalid) => {
        const validationErr = assertThrows(() => validateTokens(invalid));
        assertThrows(() => serializeTokens(invalid), validationErr);
      });
    });

    it('the input query type is validated', () => {
      const invalidTypes = [
        'mqq',
        'derp',
        '',
      ];
      _.each(invalidTypes, (invalid) => {
        const validationErr = assertThrows(() => validateQueryType(invalid));
        assertThrows(() => serializeTokens(['validToken'], invalid), validationErr);
      });
    });

  });

  describe('check IDENTITY (parseTokens and serializeTokens are inverses)', () => {

    it('check that x === serializeTokens(parseTokens(x))', () => {
      const tokenString = `asdf.'asdf...sdfa'.'kskks.ss..s'.asdf.dkdkd`;
      const res = serializeTokens(parseTokens(tokenString));
      assert.strictEqual(res, tokenString);
    });

    it('check that y = parseTokens(serializeTokens(y))', () => {
      const tokens = [
        'asdf',
        'asdf...sdfa',
        'kskks.ss..s',
        'asdf',
        'dkdkd',
      ];
      const res = parseTokens(serializeTokens(tokens));
      assert.deepStrictEqual(res, tokens);
    });

  });

});
