/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const {assertThrows} = require('../../src');

const {chop, glue} = require('../../src/utils/ngsi-sql/predicate-chopper');

const {
  CHOP_ERR_EMPTY_Q_PARAM,
  CHOP_ERR_EMPTY_MQ_PARAM,
  GLUE_ERR_INVALID_SHAPE,
  GLUE_ERR_INVALID_ITEM,
} = require('../../src/utils/ngsi-sql/errors');

describe('query chopper', () => {

  describe('CHOP', () => {

    it('chopping \'\' should FAIL', () => {
      const statement = '';
      assertThrows(() => chop(statement), CHOP_ERR_EMPTY_Q_PARAM()); // 'q' is default
      assertThrows(() => chop(statement, 'q'), CHOP_ERR_EMPTY_Q_PARAM());
      assertThrows(() => chop(statement, 'mq'), CHOP_ERR_EMPTY_MQ_PARAM());
    });

    it('should chop null and undefined into 0 predicates', () => {
      const choppedNull = chop(null);
      assert(choppedNull.length === 0);
      const choppedUndefined = chop(undefined);
      assert(choppedUndefined.length === 0);
    });

    it('it should throw error for non-falsey, non-string arg', () => {
      assert.throws(() => chop({}));
    });

    it('should chop into 4 empty strings', () => {
      const statement = ';;;';
      const chopped = chop(statement);
      assert(chopped.length === 4);
    });

    it('General Case: should chop into 3 expected', () => {
      const a = 'asdfkdkdf';
      const b = 'kdnksenjskn9---s99s98s8';
      const c = 'sdkkdkjfkjskdjf';
      const statement = `${a};${b};${c}`;
      const chopped = chop(statement);
      assert(chopped.length === 3);
      assert(chopped[0] === a);
      assert(chopped[1] === b);
      assert(chopped[2] === c);
    });

  });

  describe('GLUE', () => {

    it('fails if not an array', () => {
      assertThrows(() => glue(), GLUE_ERR_INVALID_SHAPE());
    });

    it('fails with an empty array', () => {
      assertThrows(() => glue([]), GLUE_ERR_INVALID_SHAPE());
    });

    it('fails with any non-string or zero-length items', () => {
      const invalids = [
        [''],
        ['a', 'b', ''],
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => glue(invalid), GLUE_ERR_INVALID_ITEM());
      });
    });

    it('works for 1 string', () => {
      const res = glue(['derp']);
      assert.strictEqual(res, 'derp', 'Can\'t even glue 1 string..');
    });

    it('works for 4 strings', () => {
      const res = glue(['a', 'b', 'c', 'd']);
      assert.strictEqual(res, 'a;b;c;d', 'Can\'t glue several strings..');
    });

  });

  describe('check IDENTITY (glue and chop are inverses)', () => {

    it('check that x === glue(chop(x))', () => {
      const glued = 'asdf;dkdkd;skskey;kkdke';
      const res = glue(chop(glued));
      assert.strictEqual(glued, res, 'x !== glue(chop(x))');
    });

    it('check that y === chop(glue(y))', () => {
      const chopped = ['skskjdj', 'dkkwj', 'a', 'bbb'];
      const res = chop(glue(chopped));
      assert.deepStrictEqual(chopped, res, 'y !== chop(glue(y))');
    });

  });

});
