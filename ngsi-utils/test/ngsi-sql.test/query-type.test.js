/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const {assertThrows} = require('../../src');

const {isMetadataQuery} = require('../../src/utils/ngsi-sql/query-type');
const {INVALID_QUERY_TYPE} = require('../../src/utils/ngsi-sql/errors');

describe('query-type', () => {

  it('isMetadataQuery("q") is false', () => {
    const isMq = isMetadataQuery('q');
    assert.strictEqual(isMq, false);
  });

  it('isMetadataQuery("mq") is true', () => {
    const isMq = isMetadataQuery('mq');
    assert.strictEqual(isMq, true);
  });

  it('any other value throws an error', () => {
    const invalids = [
      null,
      false,
      'mqqq',
    ];
    _.each(invalids, (invalid) => {
      assertThrows(() => isMetadataQuery(invalid), INVALID_QUERY_TYPE());
    });
  });

});
