/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');

const {
  parsePredicate,
  serializePredicate,
} = require('../../src/utils/ngsi-sql/predicate-parser');

describe('predicate-parser (NOTE: uses token-parser and token-validator!!)', () => {

  describe('parsePredicate', () => {

    describe('all binary predicate ops should be recognized', () => {

      it('should parse with equals "==" operator', () => {
        const predicate = 'asdf==20';
        const parts = parsePredicate(predicate);
        const expected = {
          predicateType: 'binary',
          tokens: ['asdf'],
          op: '==',
          value: 20,
        };
        assert(_.isEqual(parts, expected));
      });

      it('should parse with unequal "!=" operator', () => {
        const predicate = 'asdf!=20';
        const parts = parsePredicate(predicate);
        const expected = {
          predicateType: 'binary',
          tokens: ['asdf'],
          op: '!=',
          value: 20,
        };
        assert(_.isEqual(parts, expected));
      });

      it('should parse with greater than ">" operator', () => {
        const predicate = 'asdf>20';
        const parts = parsePredicate(predicate);
        const expected = {
          predicateType: 'binary',
          tokens: ['asdf'],
          op: '>',
          value: 20,
        };
        assert(_.isEqual(parts, expected));
      });

      it('should parse with less than "<" operator', () => {
        const predicate = 'asdf<20';
        const parts = parsePredicate(predicate);
        const expected = {
          predicateType: 'binary',
          tokens: ['asdf'],
          op: '<',
          value: 20,
        };
        assert(_.isEqual(parts, expected));
      });

      it('should parse with greater than or equal ">=" operator', () => {
        const predicate = 'asdf>=20';
        const parts = parsePredicate(predicate);
        const expected = {
          predicateType: 'binary',
          tokens: ['asdf'],
          op: '>=',
          value: 20,
        };
        assert(_.isEqual(parts, expected));
      });

      it('should parse with less than or equal "<=" operator', () => {
        const predicate = 'asdf<=20';
        const parts = parsePredicate(predicate);
        const expected = {
          predicateType: 'binary',
          tokens: ['asdf'],
          op: '<=',
          value: 20,
        };
        assert(_.isEqual(parts, expected));
      });

      it('should parse with match pattern "~=" operator', () => {
        const predicate = 'asdf~=\'20\'';
        const parts = parsePredicate(predicate);
        const expected = {
          predicateType: 'binary',
          tokens: ['asdf'],
          op: '~=',
          value: '20',
        };
        assert(_.isEqual(parts, expected));
      });

      it('should parse with colon (equals) ":" operator', () => {
        const predicate = 'asdf:20';
        const parts = parsePredicate(predicate);
        const expected = {
          predicateType: 'binary',
          tokens: ['asdf'],
          op: ':',
          value: 20,
        };
        assert(_.isEqual(parts, expected));
      });

    });

    describe('all unary predicate ops should be recognized', () => {

      it('should parse with non-existence "!" operator', () => {
        const predicate = '!asdf';
        const parts = parsePredicate(predicate);
        const expected = {
          predicateType: 'unary',
          tokens: ['asdf'],
          isNegatory: true,
        };
        assert(_.isEqual(parts, expected));
      });

      it('should parse with (implied) existence operator', () => {
        const predicate = 'asdf';
        const parts = parsePredicate(predicate);
        const expected = {
          predicateType: 'unary',
          tokens: ['asdf'],
          isNegatory: false,
        };
        assert(_.isEqual(parts, expected));
      });

    });

    describe('an mq query with binary ops needs two tokens at least', () => {

      it('an mq query with binary ops and one token should fail', () => {
        const predicate = 'asdf==20';
        assert.throws(() => parsePredicate(predicate, 'mq'));
      });

      it('an mq query with binary ops and two tokens should succeed', () => {
        const predicate = 'asdf.derp==20';
        const parts = parsePredicate(predicate, 'mq');
        const expected = {
          predicateType: 'binary',
          tokens: ['asdf', 'derp'],
          op: '==',
          value: 20,
        };
        assert(_.isEqual(parts, expected));
      });

    });

    describe('an mq query with unary ops needs two tokens at least', () => {

      it('an mq query with unary ops and one token should fail', () => {
        const predicate = 'asdf';
        assert.throws(() => parsePredicate(predicate, 'mq'));
      });

      it('an mq query with unary ops and two tokens should succeed', () => {
        const predicate = '!asdf.derp';
        const parts = parsePredicate(predicate, 'mq');
        const expected = {
          predicateType: 'unary',
          tokens: ['asdf', 'derp'],
          isNegatory: true,
        };
        assert(_.isEqual(parts, expected));
      });

    });

  });

  describe('serializePredicate', () => {

    it('throws if predicate is not an object', () => {
      assert.throws(() => serializePredicate(10));
    });

    it('succeeds with a UNARY predicate', () => {
      const predicate = {predicateType: 'unary', tokens: ['a']};
      const res = serializePredicate(predicate);
      assert.strictEqual(res, 'a');
    });

    it('throws if (otherwise valid) predicate does not have a valid predicateType', () => {
      assert.throws(() => serializePredicate());
      const invalid = {predicateType: 'invalid', tokens: ['a']};
      assert.throws(() => serializePredicate(invalid));
    });

    it('it succeeds with a BINARY predicate', () => {
      const predicate = {
        predicateType: 'binary',
        op: '==',
        value: 2,
        tokens: ['aa'],
      };
      const res = serializePredicate(predicate);
      assert.strictEqual(res, 'aa==2');
    });

    it('it succeeds with a BINARY predicate with value === null', () => {
      const predicate = {
        predicateType: 'binary',
        op: '==',
        value: null,
        tokens: ['aa'],
      };
      const res = serializePredicate(predicate);
      assert.strictEqual(res, 'aa==null');
    });

    it('it throws with a BINARY predicate with value === undefined', () => {
      const predicate = {
        predicateType: 'binary',
        op: '==',
        value: undefined,
        tokens: ['aa'],
      };
      assert.throws(() => serializePredicate(predicate));
    });

  });

  describe('check IDENTITY (parsePredicate & serializePredicate are inverses)', () => {

    it('check that x === serializePredicate(parsePredicate(x))', () => {
      const predicateString = `aa.bb.'cc.dd'.ee==23.345`;
      const res = serializePredicate(parsePredicate(predicateString));
      assert.strictEqual(res, predicateString);
    });

    it('check that y = parsePredicate(serializePredicate(y))', () => {
      const predicate = {
        predicateType: 'binary',
        tokens: ['aa', 'bb', 'cc.dd', 'ee'],
        op: '==',
        value: '23.345',
      };
      const res = parsePredicate(serializePredicate(predicate));
      assert.deepStrictEqual(res, predicate);
    });

  });

});
