/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const {
  assertThrows,
  ngsiDateTimeUtils: {toMillisecs},
} = require('../../src');

const {
  parseValueString,
  serializeValue,
  validateBinaryOperator,
  validateValueString,
  validateValue,
} = require('../../src/utils/ngsi-sql/value-parser');

const errors = require('../../src/utils/ngsi-sql/errors');

const {
  QUERY_BINARY_OPERATORS,
} = require('@ratatosk/ngsi-constants');

describe('value-parser', () => {

  describe('validateBinaryOperator (all other routes use this guy)', () => {

    it('the valid binary operators', () => {
      _.each(QUERY_BINARY_OPERATORS, (op) => {
        assert.doesNotThrow(() => validateBinaryOperator(op));
      });
    });

    it('invalid operators', () => {
      const anythingElse = [
        null,
        undefined,
        2,
        'm',
        '===',
      ];
      _.each(anythingElse, (invalid) => {
        assertThrows(() => validateBinaryOperator(invalid), errors.INVALID_BINARY_OPERATOR());
      });
    });

  });

  describe('validateValue (basically identical to the widely used internal function validateValueForOp)', () => {

    it('ALWAYS requires a valid op (uses validateBinaryOperator)', () => {
      const invalidOp = '**';
      assertThrows(() => validateValue(null, invalidOp), errors.INVALID_BINARY_OPERATOR());
    });

    it('some valid values', () => {
      const valids = [
        {value: null, op: '=='},
        {value: 8, op: '<'},
        {value: [2, 4], op: ':'},
        {value: ['a', 'b'], op: '!='},
        {value: [2, 'b'], op: '!='}, // should items be enforced to be of same interpreted type?
      ];
      _.each(valids, (valid) => {
        assert.doesNotThrow(() => validateValue(valid.value, valid.op));
      });
    });

    it('some invalid values (cannot be typed properly)', () => {
      const invalids = [
        undefined,
        {wrong: 2, shape: 3},
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => validateValue(invalid, '=='), errors.UNKNOWN_VALUE_TYPE());
      });
    });

    it('the MATCH_PATTERN op handles ONLY a single string', () => {
      assert.doesNotThrow(() => validateValue('a', '~='));
      const invalids = [
        true,
        false,
        null,
        10,
        -3.4,
        [1, 2],
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => validateValue(invalid, '~='), errors.MATCH_PATTERN_REQUIRES_REGEXP());
      });
    });

    it('only equalityOperators can handle lists and ranges', () => {
      const list = [1, 2];
      const range = {min: 1, max: 2};
      const equalityOperators = _.pick(QUERY_BINARY_OPERATORS, ['EQUALS', 'UNEQUAL', 'COLON_EQUALS']);
      const noneqOperators = _.omit(QUERY_BINARY_OPERATORS, ['EQUALS', 'UNEQUAL', 'COLON_EQUALS']);
      _.each(equalityOperators, (eqOp) => {
        assert.doesNotThrow(() => validateValue(list, eqOp));
        assert.doesNotThrow(() => validateValue(range, eqOp));
      });
      _.each(noneqOperators, (noneqOp) => {
        // even here MATCH_PATTERN is special
        if (noneqOp !== QUERY_BINARY_OPERATORS.MATCH_PATTERN) {
          assertThrows(() => validateValue(list, noneqOp), errors.INVALID_MULTI_VALUE_OPERATOR());
        }
      });
    });

    it('comparisonOperators can only handle numbers, strings, and dates', () => {
      const comparatorKeys = ['LESS_THAN', 'LESS_THAN_OR_EQUALS', 'GREATER_THAN', 'GREATER_THAN_OR_EQUALS'];
      const comparisonOperators = _.pick(QUERY_BINARY_OPERATORS, comparatorKeys);
      const compErrors = {
        LESS_THAN: errors.INVALID_COMPARISON_VALUE_LT(),
        LESS_THAN_OR_EQUALS: errors.INVALID_COMPARISON_VALUE_LTE(),
        GREATER_THAN: errors.INVALID_COMPARISON_VALUE_GT(),
        GREATER_THAN_OR_EQUALS: errors.INVALID_COMPARISON_VALUE_GTE(),
      };
      const valids = [
        1,
        'as',
        new Date(),
      ];
      const invalids = [
        null,
        false,
        true,
      ];
      _.each(comparatorKeys, (compKey) => {
        const compareOp = comparisonOperators[compKey];
        const compError = compErrors[compKey];
        _.each(valids, (valid) => {
          assert.doesNotThrow(() => validateValue(valid, compareOp));
        });
        _.each(invalids, (invalid) => {
          assertThrows(() => validateValue(invalid, compareOp), compError);
        });
      });
    });

    it('list items are only allowed to contain items of certain types', () => {
      const valids = [
        [null],
        [true, false],
        [1, 2, 3.4, -2.23],
        ['a', 'bb  bb'],
        [new Date()],
        [null, true, false, 3.14, 'a', new Date()],
      ];
      _.each(valids, (valid) => {
        assert.doesNotThrow(() => validateValue(valid, '=='));
      });
      const invalids = [
        [[1]],
        [{min: 1, max: 2}],
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => validateValue(invalid, '=='), errors.INVALID_LIST_ITEM());
      });
    });

    it('range items can only handle (PAIRS OF) numbers, strings, and dates', () => {
      const valids = [
        {min: 0, max: 1},
        {min: 1, max: 0},
        {min: 'a', max: 'b'},
        {min: new Date(), max: new Date()},
      ];
      _.each(valids, (valid) => {
        assert.doesNotThrow(() => validateValue(valid, '=='));
      });
      const invalids = [
        {min: true, max: false},
        {min: null, max: null},
        {min: [], max: []},
        {min: {min: 0, max: 1}, max: {min: 0, max: 1}},
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => validateValue(invalid, '=='), errors.INVALID_RANGE_ITEM());
      });
      const invalidPairTypes = [
        {min: 0, max: 'b'},
        {min: new Date(), max: '2'},
      ];
      _.each(invalidPairTypes, (invalid) => {
        assertThrows(() => validateValue(invalid, '=='), errors.INVALID_RANGE_INCONSISTENT_MIN_MAX_TYPES());
      });
    });

  });

  describe('serializeValue', () => {

    it('if it fails, it will throw the same error as validateValue', () => {
      const invalids = [
        {value: '2', op: 'INVALID_OP'},
        {value: {min: 2, max: 3}, op: '>'}, // and so on
      ];
      _.each(invalids, (invalid) => {
        const validateErr = assertThrows(() => validateValue(invalid.value, invalid.op));
        assertThrows(() => serializeValue(invalid.value, invalid.op), validateErr);
      });
    });

    it('some valid examples', () => {
      const valids = [
        {value: {min: 2, max: 3}, op: '==', expected: '2..3'},
        {value: ['a..b', 2, null, true, 'true'], op: '==', expected: '\'a..b\',2,null,true,\'true\''},
      ];
      _.each(valids, (valid) => {
        const res = serializeValue(valid.value, valid.op);
        assert.strictEqual(res, valid.expected);
      });
    });

  });

  describe('parseValueString (this is THE workhorse (apart from validateValue))', () => {

    it('some working examples', () => {
      const valids = [
        // string
        {in: '', op: '~=', out: ''},
        {in: '\'\'', op: '>', out: ''},
        {in: 'asdf', op: '==', out: 'asdf'},
        {in: '\'asdf\'', op: '==', out: 'asdf'},
        // null, true, false
        {in: 'null', op: ':', out: null},
        {in: '\'null\'', op: ':', out: 'null'},
        {in: 'true', op: ':', out: true},
        {in: '\'true\'', op: ':', out: 'true'},
        {in: 'false', op: ':', out: false},
        {in: '\'false\'', op: ':', out: 'false'},
        // number
        {in: '10', op: '>=', out: 10},
        {in: '-10.3465', op: '<=', out: -10.3465},
        {in: '\'-10.3465\'', op: '<=', out: '-10.3465'},
        {in: '0x455', op: '<', out: 1109},
        {in: '\'0x455\'', op: '<', out: '0x455'},
        // date
        {in: '2019-09-05T13:09:22.706Z', op: '==', out: new Date(toMillisecs('2019-09-05T13:09:22.706Z'))},
        {in: '2019-09-05T13:09:22.706Z', op: '==', out: new Date('2019-09-05T13:09:22.706Z')},
        {in: '\'2019-09-05T13:09:22.706Z\'', op: '==', out: '2019-09-05T13:09:22.706Z'},
        // range
        {in: '0..1', op: '==', out: {min: 0, max: 1}},
        {in: '2019-09-05T13:09:22.706Z..2020-09-05T13:09:22.706Z', op: '==', out: {min: new Date('2019-09-05T13:09:22.706Z'), max: new Date('2020-09-05T13:09:22.706Z')}},
        {in: '0x234..-23.4', op: '==', out: {min: 564, max: -23.4}},
        {in: 'b..\'zoo..ooz\'', op: '==', out: {min: 'b', max: 'zoo..ooz'}},
        // list
        {in: 'a,b,c', op: '==', out: ['a', 'b', 'c']},
        {in: '\'a\',\'b\'', op: '==', out: ['a', 'b']},
        {in: '-23.2,\'true\',\'dd..pp,qq\',2019-09-05T13:09:22.706Z', op: '==', out: [-23.2, 'true', 'dd..pp,qq', new Date('2019-09-05T13:09:22.706Z')]},
      ];
      _.each(valids, (valid) => {
        const res = parseValueString(valid.in, valid.op);
        assert.deepStrictEqual(res, valid.out);
      });
    });

    it('a range is effectively a list with exactly two items', () => {
      const invalid = '0..1..2';
      assertThrows(() => parseValueString(invalid, '=='), errors.INVALID_RANGE_CONSTRUCTION());
    });

    it('a range OR list must be wellformatted (with string groupers)', () => {
      const invalids = [
        '\'a..a',
        'a\'..a',
        '\'a,a',
        'a\',a',
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => parseValueString(invalid, '=='), errors.INVALID_MULTI_VALUE_SINGLE_GROUPER());
      });
    });

    it('a range OR list must also.. otherwise .. be well formatted', () => {
      const invalids = [
        '\'a\'a..a',
        'a\'a\'..a',
        '\'a\'a,a',
        'a\'a\',a',
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => parseValueString(invalid, '=='), errors.INVALID_MULTI_VALUE_GROUPING());
      });
    });

    it('some other invalid ranges', () => {
      const invalids = [
        'a..b,c',
        'a,b..c',
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => parseValueString(invalid, '=='), errors.INVALID_RANGE_ITEM());
      });
    });

  });

  describe('validateValueString (this is just an alias for parseValueString)', () => {

    it('the same valids copied from parseValueString', () => {
      const valids = [
        // string
        {in: '', op: '~=', out: ''},
        {in: '\'\'', op: '>', out: ''},
        {in: 'asdf', op: '==', out: 'asdf'},
        {in: '\'asdf\'', op: '==', out: 'asdf'},
        // null, true, false
        {in: 'null', op: ':', out: null},
        {in: '\'null\'', op: ':', out: 'null'},
        {in: 'true', op: ':', out: true},
        {in: '\'true\'', op: ':', out: 'true'},
        {in: 'false', op: ':', out: false},
        {in: '\'false\'', op: ':', out: 'false'},
        // number
        {in: '10', op: '>=', out: 10},
        {in: '-10.3465', op: '<=', out: -10.3465},
        {in: '\'-10.3465\'', op: '<=', out: '-10.3465'},
        {in: '0x455', op: '<', out: 1109},
        {in: '\'0x455\'', op: '<', out: '0x455'},
        // date
        {in: '2019-09-05T13:09:22.706Z', op: '==', out: new Date(toMillisecs('2019-09-05T13:09:22.706Z'))},
        {in: '2019-09-05T13:09:22.706Z', op: '==', out: new Date('2019-09-05T13:09:22.706Z')},
        {in: '\'2019-09-05T13:09:22.706Z\'', op: '==', out: '2019-09-05T13:09:22.706Z'},
        // range
        {in: '0..1', op: '==', out: {min: 0, max: 1}},
        {in: '2019-09-05T13:09:22.706Z..2020-09-05T13:09:22.706Z', op: '==', out: {min: new Date('2019-09-05T13:09:22.706Z'), max: new Date('2020-09-05T13:09:22.706Z')}},
        {in: '0x234..-23.4', op: '==', out: {min: 564, max: -23.4}},
        {in: 'b..\'zoo..ooz\'', op: '==', out: {min: 'b', max: 'zoo..ooz'}},
        // list
        {in: 'a,b,c', op: '==', out: ['a', 'b', 'c']},
        {in: '\'a\',\'b\'', op: '==', out: ['a', 'b']},
        {in: '-23.2,\'true\',\'dd..pp,qq\',2019-09-05T13:09:22.706Z', op: '==', out: [-23.2, 'true', 'dd..pp,qq', new Date('2019-09-05T13:09:22.706Z')]},
      ];
      _.each(valids, (valid) => {
        assert.doesNotThrow(() => parseValueString(valid.in, valid.op));
        assert.doesNotThrow(() => validateValueString(valid.in, valid.op));
      });
    });

    it('any failed validation should be the same as for the parseValueString', () => {
      const invalids = [
        '0..1..2',
        '\'a..a',
        'a\'..a',
        '\'a,a',
        'a\',a',
        '\'a\'a..a',
        'a\'a\'..a',
        '\'a\'a,a',
        'a\'a\',a',
        'a..b,c',
        'a,b..c',
        '20..ab',
        '2019-09-05T13:09:22.706Z..20',
      ];
      _.each(invalids, (invalid) => {
        const parseErr = assertThrows(() => parseValueString(invalid, '=='));
        const validateErr = assertThrows(() => validateValueString(invalid, '=='));
        assert.strictEqual(parseErr.message, validateErr.message);
      });
    });

  });

});
