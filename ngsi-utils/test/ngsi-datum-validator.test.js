/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const assert = require('assert');
const _ = require('lodash');
const {assertThrows} = require('../src');

const {
  errors,
  validateEntityId,
  validateEntityType,
  validateAttributeName,
  validateAttributeType,
  validateMetadataName,
  validateMetadataType,
  inferTypeForGeneralValue,
  validateGeneralValue,
} = require('../src/utils/ngsi-datum-validator');

// allValidators does not include inferTypeForGeneralValue
const allValidators = {
  validateEntityId,
  validateEntityType,
  validateAttributeName,
  validateAttributeType,
  validateMetadataName,
  validateMetadataType,
  validateGeneralValue,
};
const fieldValidators = _.omit(allValidators, 'validateGeneralValue');

describe('ngsi-datum-validator.test', () => {

  describe('errors', () => {

    it('there are 10 ngsiDatumValidator errors', () => {
      assert.strictEqual(_.size(errors), 10);
    });

    it('the errors have some "matches" functionality and identifier flag', () => {
      const nonmatchingErr = new Error('this one does not match nuthin');
      _.each(errors, (error) => {
        const datumErr = error();
        assert(datumErr.isNgsiDatumValidationError);
        assert(error.matches(datumErr));
        assert(error.matches(datumErr.message));
        assert(!error.matches(nonmatchingErr));
        assert(!error.matches(nonmatchingErr.message));
      });
    });

  });

  describe('1 - modes (different validation modes)', () => {

    it('some invalid modes - all validators', () => {
      const invalidModes = [
        'aaa',
      ];
      _.each(allValidators, (validator) => {
        _.each(invalidModes, (invalidMode) => {
          assertThrows(() => validator(undefined, invalidMode), errors.INVALID_MODE());
        });
      });
    });

    it('all valid modes (but undefined value) - all validators', () => {
      const validModes = [
        undefined, // <--- defaults to DEFAULT_DATA_VALIDATION_MODE
        'ORION', // <-- orion-like behavior
        'PERMISSIVE', // <-- ngsi-v2-like behavior
        'RESTRICTED', // <-- more RESERVED but NOT orion-v2-like
        'ORION_RESTRICTED', // <-- more RESERVED AND orion-v2-like
      ];
      _.each(fieldValidators, (validator) => {
        _.each(validModes, (validMode) => {
          const err = assertThrows(() => validator(undefined, validMode));
          // note that they all fail for a reason OTHER than invalid mode
          assert.notStrictEqual(err.message, errors.INVALID_MODE());
        });
      });
    });

  });

  describe('2 - validateGeneralField (most of em do this) (MODE dependent)', () => {

    it('some valid fieldNames', () => {
      const valids = [
        // shortest possible
        'a',
        // longest possible
        _.times(256, () => 'a').join(''),
        // other stuff
        'derp',
      ];
      _.each(fieldValidators, (validator) => {
        _.each(valids, (valid) => {
          assert.doesNotThrow(() => validator(valid));
        });
      });
    });

    it('some invalid fieldNames', () => {
      const invalids = [
        '&',
        'aaa&aaa',
        '?',
        'aaa?aaa',
        '/',
        'aaa/aaa',
        '#',
        'aaa#aaa',
        // illegal whitespace chars
        'de  rp',
        '\t',
        '\n',
        '\r',
        '\r\n',
        // illegal control chars
        String.fromCharCode(0),
        String.fromCharCode(25), // 0 - 31
        String.fromCharCode(31),
        String.fromCharCode(127), // & 127
      ];
      _.each(fieldValidators, (validator) => {
        _.each(invalids, (invalid) => {
          assertThrows(() => validator(invalid), errors.FIELD_CONTAINS_FORBIDDEN_CHAR());
        });
      });
    });

    it('too short - length 0', () => {
      _.each(fieldValidators, (validator) => {
        assertThrows(() => validator(''), errors.FIELD_VALUE_TOO_SHORT());
      });
    });

    it('too long - length 257', () => {
      const field = _.times(257, () => 'a').join('');
      _.each(fieldValidators, (validator) => {
        assertThrows(() => validator(field), errors.FIELD_VALUE_TOO_LONG());
      });
    });

    it('Orion restrictions -> only if mode is one of [ORION, ORION_RESTRICTED]', () => {
      const invalids = [
        '<',
        '>',
        '"',
        '=',
        ';',
        '(',
        ')',
      ];
      _.each(fieldValidators, (validator) => {
        _.each(invalids, (invalid) => {
          assert.doesNotThrow(() => validator(invalid));
          assert.doesNotThrow(() => validator(invalid, undefined));
          assert.doesNotThrow(() => validator(invalid, 'PERMISSIVE'));
          // NOTE: RESTRICTED might or might not throw, depending upon RESTRICTIONS
          assertThrows(() => validator(invalid, 'ORION'), errors.FIELD_CONTAINS_FORBIDDEN_ORION_CHAR());
          assertThrows(() => validator(invalid, 'ORION_RESTRICTED'), errors.FIELD_CONTAINS_FORBIDDEN_ORION_CHAR());
        });
      });
    });

  });

  describe('3 - attr and metadata name special exceptions (MODE DEPENDENT)', () => {

    it('attr name exceptions, (built-ins)', () => {
      const attrExceptions = [
        'dateCreated',
        'dateModified',
        'dateExpires',
        'id',
        'type',
        'geo:distance',
        '*',
      ];
      _.each(attrExceptions, (exception) => {
        assertThrows(() => validateAttributeName(exception), errors.FORBIDDEN_ATTRIBUTE_NAME());
      });
    });

    it('metadata name exceptions, (built-ins & explicit)', () => {
      const attrExceptions = [
        'dateCreated',
        'dateModified',
        'previousValue',
        'actionType',
        '*',
      ];
      _.each(attrExceptions, (exception) => {
        assertThrows(() => validateMetadataName(exception), errors.FORBIDDEN_METADATA_NAME());
      });
    });

    it('RESTRICTED exceptions -> applies only if mode is one of [RESTRICTED, ORION_RESTRICTED] (ignore ORION)', () => {
      const validators = [validateAttributeName, validateMetadataName];
      const invalids = [
        '==',
        '!-',
        '>',
        '<',
        '>=',
        '<=',
        '~=',
        ':',
        '!',
      ];
      _.each(validators, (validator) => {
        _.each(invalids, (invalid) => {
          // ORION might throw or not.. so is ignored for this one.
          // PERMISSIVE should not throw for these invalids
          assert.doesNotThrow(() => validator(invalid, 'PERMISSIVE'));
          // uncertain at this granularity which error will come
          assertThrows(() => validator(invalid, 'ORION_RESTRICTED'));
          // for RESTRICTED it is certain
          assertThrows(() => validator(invalid, 'RESTRICTED'), errors.NON_QUERYABLE_NAME());
        });
      });

    });

  });

  describe('(SPECIAL) - validateGeneralValue & inferTypeForGeneralValue', () => {

    const allModes = ['ORION', 'PERMISSIVE', 'RESTRICTED', 'ORION_RESTRICTED'];
    const orionModes = ['ORION', 'ORION_RESTRICTED'];

    it('some valids for all modes', () => {
      const valids = [
        [true, 'Boolean'],
        [false, 'Boolean'],
        [null, 'None'],
        [0, 'Number'],
        [-18.123, 'Number'],
        ['validString', 'Text'],
        ['valid_string', 'Text'],
        [{}, 'StructuredValue'],
        [{argy: 'bargy'}, 'StructuredValue'],
        [[], 'StructuredValue'],
        [[1, null, 2, 'three'], 'StructuredValue'],
      ];
      _.each(allModes, (mode) => {
        _.each(valids, (valid) => {
          assert.doesNotThrow(() => validateGeneralValue(valid[0], mode));
          const type = inferTypeForGeneralValue(valid[0], mode);
          assert.strictEqual(type, valid[1]);
        });
      });
    });

    it('some that are invalid only for Orion modes', () => {
      const orionInvalids = [
        'asdf;;asdf',
        {a: 1, b: {c: 'asdf', d: 'asdf""asdf'}},
        ['asdf()'],
      ];
      _.each(allModes, (mode) => {
        _.each(orionInvalids, (orionInvalid) => {
          if (_.includes(orionModes, mode)) {
            assertThrows(() => validateGeneralValue(orionInvalid, mode));
          } else {
            assert.doesNotThrow(() => validateGeneralValue(orionInvalid, mode));
          }
        });
      });
    });

  });

});
