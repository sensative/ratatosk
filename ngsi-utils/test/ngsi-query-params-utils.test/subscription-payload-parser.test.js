/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const {
  assertThrows,
  ngsiDateTimeUtils: {fromDate},
} = require('../../src');

const {
  errors,
  formatSubscriptionPayload,
  validateSubscriptionPayload,
} = require('../../src/utils/ngsi-query-params-utils/subscription-payload-parser');

const validSubscription = {
  id: 'getsIgnored',
  description: 'must be a string',
  subject: {
    entities: [{id: 'blue'}],
  },
  notification: {
    attrs: ['myAttr'],
    http: {url: 'http://somevalidurllookingthing'},
  },
};

describe('subscription-payload-parser', () => {

  describe('errors', () => {

    it('there are 21 isNgsiSubscriptionPayloadParserError errors', () => {
      assert.strictEqual(_.size(errors), 21);
    });

    it('the errors have some "matches" functionality and identifier flag', () => {
      const nonmatchingErr = new Error('this one does not match nuthin');
      _.each(errors, (error) => {
        const basicErr = error();
        assert(basicErr.isNgsiSubscriptionPayloadParserError, 'error type does not match');
        assert(basicErr.name);
        assert(error.matches(basicErr));
        assert(error.matches(basicErr.message));
        assert(!error.matches(nonmatchingErr));
        assert(!error.matches(nonmatchingErr.message));
      });
    });

  });

  describe('formatSubscriptionPayload', () => {

    it('validSubscription object gets formatted as expected', () => {
      const res = formatSubscriptionPayload(validSubscription);
      const expected = _.omit(validSubscription, 'id');
      assert.deepStrictEqual(res, expected); // should be unchanged
    });

    it('payload must be an object', () => {
      const invalids = [
        null,
        undefined,
        0,
        'derp',
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => formatSubscriptionPayload(invalid), errors.INVALID_SUBSCRIPTION_PAYLOAD_ARG());
      });
    });

    it('invalid payload props not allowed', () => {
      const invalids = [
        {invalidProp: 'asdfasdf'},
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => formatSubscriptionPayload(invalid), errors.EXTRANEOUS_SUBSCRIPTION_PAYLOAD_PROPS());
      });
    });

    it('description can be undefined or a string', () => {
      const descRemoved = _.omit(validSubscription, 'description');
      assert.doesNotThrow(() => formatSubscriptionPayload(descRemoved));
      const validDescriptions = [
        undefined,
        'any other string (?)',
      ];
      _.each(validDescriptions, (description) => {
        const valid = _.assign({}, validSubscription, {description});
        assert.doesNotThrow(() => formatSubscriptionPayload(valid));
      });
      const invalidDescriptions = [
        null,
        23,
        {asdf: 'asdf'},
      ];
      _.each(invalidDescriptions, (description) => {
        const invalid = _.assign({}, validSubscription, {description});
        assertThrows(() => formatSubscriptionPayload(invalid), errors.INVALID_SUBSCRIPTION_DESCRIPTION());
      });
    });

    it('status must one of the allowed values', () => {
      const invalidStatuses = [
        'invalid',
        23,
      ];
      _.each(invalidStatuses, (status) => {
        const invalid = _.assign({}, validSubscription, {status});
        assertThrows(() => formatSubscriptionPayload(invalid), errors.INVALID_SUBSCRIPTION_STATUS());
      });
      const validStatutes = [
        undefined,
        'active',
        'inactive',
        'failed',
        'expired',
      ];
      _.each(validStatutes, (status) => {
        const valid = _.assign({}, validSubscription, {status});
        assert.doesNotThrow(() => formatSubscriptionPayload(valid));
      });
    });

    it('expires must be a valid DateTime', () => {
      const invalidExpires = [
        'asdf',
        123412234,
      ];
      _.each(invalidExpires, (expires) => {
        const invalid = _.assign({}, validSubscription, {expires});
        assertThrows(() => formatSubscriptionPayload(invalid), errors.INVALID_SUBSCRIPTION_EXPIRATION());
      });
      const validExpires = [
        '2019-11-06T13:38:55.747Z',
        fromDate(new Date()),
      ];
      _.each(validExpires, (expires) => {
        const valid = _.assign({}, validSubscription, {expires});
        assert.doesNotThrow(() => formatSubscriptionPayload(valid));
      });
    });

    it('subject must be an object', () => {
      const invalidSubjects = [
        null,
        undefined,
        0,
        'derp',
      ];
      _.each(invalidSubjects, (subject) => {
        const invalid = _.assign({}, validSubscription, {subject});
        assertThrows(() => formatSubscriptionPayload(invalid), errors.INVALID_SUBSCRIPTION_SUBJECT_CONTAINER());
      });
    });

    it('subject cannot have extraneous props', () => {
      const invalidSubjects = [
        {invalidProp: 'disIsIt'},
      ];
      _.each(invalidSubjects, (subject) => {
        const invalid = _.assign({}, validSubscription, {subject});
        assertThrows(() => formatSubscriptionPayload(invalid), errors.EXTRANEOUS_SUBSCRIPTION_SUBJECT_PROPS());
      });
    });

    it('subject condition not yet implemented', () => {
      const invalidConditions = [
        'whatever',
        {attrs: ['otherwiseValidCondition']},
      ];
      _.each(invalidConditions, (condition) => {
        const subject = _.assign({}, validSubscription.subject, {condition});
        const invalid = _.assign({}, validSubscription, {subject});
        assertThrows(() => formatSubscriptionPayload(invalid), 'subscription.subject.condition not yet implemented');
      });
    });

    it('subject entities must be a non-zero array', () => {
      const invalidEntities = [
        'a string is not an array',
        [], // is empty..
      ];
      _.each(invalidEntities, (entities) => {
        const subject = _.assign({}, validSubscription.subject, {entities});
        const invalid = _.assign({}, validSubscription, {subject});
        assertThrows(() => formatSubscriptionPayload(invalid), errors.INVALID_SUBSCRIPTION_SUBJECT_ENTITIES());
      });
    });

    it('subject entities items must .. look valid', () => {
      const invalidEntityItems = [
        {invalidProp: 'asdf'},
        {id: 'asdf', idType: 'asdf'},
        {type: 'id_is_missign'},
      ];
      _.each(invalidEntityItems, (item) => {
        const subject = _.assign({}, validSubscription.subject, {entities: [item]});
        const invalid = _.assign({}, validSubscription, {subject});
        assertThrows(() => formatSubscriptionPayload(invalid), errors.INVALID_SUBJECT_ENTITY());
      });
    });

    it('notification must be an object', () => {
      const invalidNotifications = [
        null,
        'asdf',
      ];
      _.each(invalidNotifications, (notification) => {
        const invalid = _.assign({}, validSubscription, {notification});
        assertThrows(() => formatSubscriptionPayload(invalid), errors.INVALID_SUBSCRIPTION_NOTIFICATION_CONTAINER());
      });
    });

    it('notification cannot have wrong keys', () => {
      const invalidNotifications = [
        {invalidKey: true},
      ];
      _.each(invalidNotifications, (notification) => {
        const invalid = _.assign({}, validSubscription, {notification});
        assertThrows(() => formatSubscriptionPayload(invalid), errors.INVALID_SUBSCRIPTION_NOTIFICATION_PROPS());
      });
    });

    it('some other invalid notifications (highlight some errors)', () => {
      const invalidNotifications = [
        {
          notification: {},
          err: errors.INVALID_SUBSCRIPTION_NOTIFICATION_HTTP_PROPS_NEITHER(),
        },
        {
          notification: {http: null, httpCustom: null},
          err: errors.INVALID_SUBSCRIPTION_NOTIFICATION_HTTP_PROPS_BOTH(),
        },
        {
          notification: {http: 'asdf', attrs: null, exceptAttrs: null},
          err: errors.INVALID_SUBSCRIPTION_NOTIFICATION_ATTRS_PROPS(),
        },
        {
          notification: {http: 'asdf'},
          err: errors.INVALID_NOTIFICATION_HTTP_CONTAINER(),
        },
        {
          notification: {http: {extraKey: true}},
          err: errors.INVALID_NOTIFICATION_HTTP_PROPS(),
        },
        {
          notification: {http: {url: ''}},
          err: errors.INVALID_NOTIFICATION_HTTP_URL(),
        },
        {
          notification: {httpCustom: 'notSupportedYet'},
          err: 'custom http not yet supported',
        },
        {
          notification: {http: {url: 'valid'}, attrs: 23},
          err: errors.INVALID_SUBSCRIPTION_NOTIFICATION_ATTRS(),
        },
        {
          notification: {http: {url: 'valid'}, exceptAttrs: 23},
          err: errors.INVALID_SUBSCRIPTION_NOTIFICATION_ATTRS_BLACKLIST(),
        },
        {
          notification: {http: {url: 'valid'}, metadata: 23},
          err: errors.INVALID_SUBSCRIPTION_NOTIFICATION_METADATA(),
        },
      ];
      _.each(invalidNotifications, ({notification, err}) => {
        const invalid = _.assign({}, validSubscription, {notification});
        assertThrows(() => formatSubscriptionPayload(invalid), err);
      });
    });

  });

  describe('validateSubscriptionPayload', () => {

    it('valid subscriptions also validate', () => {
      assert.doesNotThrow(() => validateSubscriptionPayload(validSubscription));
      assert.doesNotThrow(() => formatSubscriptionPayload(validSubscription));
    });

  });

});
