/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

describe('ngsi-query-params-utils.test', () => {

  // // ---- the components
  require('./basic-param-parser.test');
  require('./attrs-param-parser.test');
  require('./metadata-param-parser.test');
  require('./orderby-param-parser.test');
  require('./options-param-parser.test');
  require('./geometry-params-parser.test');

  // // ---- and the MAIN tests
  require('./query-params-parser.test');
  require('./bulk-query-payload-parser.test');
  require('./bulk-update-payload-parser.test');
  require('./subscription-payload-parser.test');
  require('./registration-payload-parser.test');

});
