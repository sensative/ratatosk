/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const {assertThrows} = require('../../src');

const {
  errors,
  parseOrderbyParam,
  serializeOrderbyList,
  validateOrderbyParam,
  validateOrderbyList,
} = require('../../src/utils/ngsi-query-params-utils/orderby-param-parser');

describe('orderby-param-parser.test', () => {

  describe('errors', () => {

    it('there are 5 NgsiOrderbyParamError', () => {
      assert.strictEqual(_.size(errors), 5);
    });

    it('the errors have some "matches" functionality and identifier flag', () => {
      const nonmatchingErr = new Error('this one does not match nuthin');
      _.each(errors, (error) => {
        const basicErr = error();
        assert(basicErr.isNgsiOrderbyParamError);
        assert(basicErr.name);
        assert(error.matches(basicErr));
        assert(error.matches(basicErr.message));
        assert(!error.matches(nonmatchingErr));
        assert(!error.matches(nonmatchingErr.message));
      });
    });

  });

  describe('parseOrderbyParam', () => {

    it('an empty param is ok (returns empty list)', () => {
      const empties = [
        undefined,
        null,
      ];
      _.each(empties, (empty) => {
        const res = parseOrderbyParam(empty);
        assert.deepStrictEqual(res, null);
      });
    });

    it('some other valid orderBy params', () => {
      const valids = [
        {in: 'anyValidFieldName', out: [{name: 'anyValidFieldName', isNegatory: false}]},
        {in: '\'canBeWrapped\'', out: [{name: 'canBeWrapped', isNegatory: false}]},
        {
          in: '\'can,contain\',\'some,,,,commas\'',
          out: [{name: 'can,contain', isNegatory: false}, {name: 'some,,,,commas', isNegatory: false}],
        },
        {
          in: 'a,b,c,d,\'e\'',
          out: [{name: 'a', isNegatory: false}, {name: 'b', isNegatory: false}, {name: 'c', isNegatory: false}, {name: 'd', isNegatory: false}, {name: 'e', isNegatory: false}],
        },
        {
          in: 'aa,!bb,cc!', // and negations are allowed
          out: [{name: 'aa', isNegatory: false}, {name: 'bb', isNegatory: true}, {name: 'cc!', isNegatory: false}],
        },
      ];
      _.each(valids, (valid) => {
        const res = parseOrderbyParam(valid.in);
        assert.deepStrictEqual(res, valid.out);
      });
    });

    it('some attribute names that are normally not allowed ARE allowed here in the orderby', () => {
      const exceptions = [
        'id',
        'type',
        'geo:distance',
        'dateCreated',
      ];
      _.each(exceptions, (exception) => {
        assert.doesNotThrow(() => parseOrderbyParam(exception));
      });
    });

    it('only accepts non-empty strings', () => {
      const invalids = [
        '',
        0,
        {},
        [],
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => parseOrderbyParam(invalid), errors.EMPTY_ORDERBY_PARAM());
      });
    });

    it('cannot contain duplicates', () => {
      const invalids = [
        'aa,aa',
        '!aa,aa',
        '!aa,!aa',
        'aa,!aa',
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => parseOrderbyParam(invalid), errors.INVALID_ORDERBY_DUPLICATES());
      });
    });

    it('an orderBy param CANNOT contain the single quote (\')', () => {
      const invalid = '\'';
      assertThrows(() => parseOrderbyParam(invalid), errors.INVALID_ORDERBY_PARAM_FORMATTING());
    });

    it('If parsing the list into orderby items fails due to some missformatting of separators and groupers', () => {
      const invalids = [
        '\'',
        'a\'',
        'a\'b',
        'a\',b',
        'a\'b,',
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => parseOrderbyParam(invalid, errors.INVALID_ORDERBY_PARAM_FORMATTING()));
      });
    });

    it('all items must be valid attribute name (apart from exceptions) (no invalid chars)', () => {
      const invalids = [
        'has_a_#',
        'has_a_?',
        'has_a_&',
        'has_a_/',
        'has space',
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => parseOrderbyParam(invalid), errors.INVALID_ORDERBY_PARAM_ITEM());
      });
    });

  });

  describe('validateOrderbyParam', () => {

    it('if it is parseable, then it is validateable', () => {
      const valids = [
        {in: null, out: null},
        {in: undefined, out: null},
        {in: 'anyValidFieldName', out: [{name: 'anyValidFieldName', isNegatory: false}]},
        {in: '\'canBeWrapped\'', out: [{name: 'canBeWrapped', isNegatory: false}]},
        {
          in: '\'can,contain\',\'some,,,,commas\'',
          out: [{name: 'can,contain', isNegatory: false}, {name: 'some,,,,commas', isNegatory: false}],
        },
        {
          in: 'a,b,c,d,\'e\'',
          out: [{name: 'a', isNegatory: false}, {name: 'b', isNegatory: false}, {name: 'c', isNegatory: false}, {name: 'd', isNegatory: false}, {name: 'e', isNegatory: false}],
        },
        {
          in: 'aa,!bb,cc!', // and negations are allowed
          out: [{name: 'aa', isNegatory: false}, {name: 'bb', isNegatory: true}, {name: 'cc!', isNegatory: false}],
        },
        // exceptions
        {in: 'id', out: [{name: 'id', isNegatory: false}]},
        {in: 'type', out: [{name: 'type', isNegatory: false}]},
        {in: 'geo:distance', out: [{name: 'geo:distance', isNegatory: false}]},
        {in: 'dateCreated', out: [{name: 'dateCreated', isNegatory: false}]},
      ];
      _.each(valids, (valid) => {
        const res = parseOrderbyParam(valid.in);
        assert.deepStrictEqual(res, valid.out);
        assert.doesNotThrow(() => validateOrderbyParam(valid.in));
      });
    });

    it('any failures fail with the same error as the parser', () => {
      const invalids = [
        '',
        0,
        {},
        [],
        'a,a',
        '\'',
        'a\'',
        'a\'b',
        'a\',b',
        'a\'b,',
        'has_a_#',
        'has_a_?',
        'has_a_&',
        'has_a_/',
        'has space',
      ];
      _.each(invalids, (invalid) => {
        const parseErr = assertThrows(() => parseOrderbyParam(invalid));
        const validateErr = assertThrows(() => validateOrderbyParam(invalid));
        assert.deepStrictEqual(parseErr, validateErr);
      });
    });

  });

  describe('serializeOrderbyList', () => {

    it('can be nil or empty', () => {
      const valids = [
        null,
        undefined,
        [],
      ];
      _.each(valids, (valid) => {
        const res = serializeOrderbyList(valid);
        assert.strictEqual(res, null);
      });
    });

    it('some other valids', () => {
      const valids = [
        {in: [{name: 'aa', isNegatory: false}], out: 'aa'},
        {in: [{name: 'aa', isNegatory: true}], out: '!aa'},
        {in: [{name: 'a,,a', isNegatory: true}], out: '\'!a,,a\''},
        {
          in: [{name: 'a', isNegatory: true}, {name: 'b', isNegatory: false}, {name: 'c,c', isNegatory: false}, {name: 'd,d', isNegatory: true}],
          out: '!a,b,\'c,c\',\'!d,d\'',
        },
      ];
      _.each(valids, (valid) => {
        const res = serializeOrderbyList(valid.in);
        assert.strictEqual(res, valid.out);
      });
    });

    it('but it must be an array', () => {
      const invalids = [
        '',
        0,
        {},
        'abers',
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => serializeOrderbyList(invalid), errors.INVALID_ORDERBY_LIST());
      });
    });

    it('cannot contain dupes', () => {
      const invalids = [
        [{name: 'a', isNegatory: true}, {name: 'a', isNegatory: false}],
        [{name: 'a', isNegatory: false}, {name: 'a', isNegatory: false}],
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => serializeOrderbyList(invalid), errors.INVALID_ORDERBY_DUPLICATES());
      });
    });

    it('the items must, of course, be a validateable entityId', () => {
      const invalids = [
        [{name: 'has_a_#'}],
        [{name: 'has_a_?'}],
        [{name: 'has_a_&'}],
        [{name: 'has_a_/'}],
        [{name: 'has space'}],
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => serializeOrderbyList(invalid), errors.INVALID_ORDERBY_PARAM_ITEM());
      });
    });

  });

  describe('validateOrderbyList', () => {

    it('if it can be serialized, then the list validates ok', () => {
      const valids = [
        {in: null, out: null},
        {in: undefined, out: null},
        {in: [], out: null},
        {in: [{name: 'aa', isNegatory: false}], out: 'aa'},
        {in: [{name: 'aa', isNegatory: true}], out: '!aa'},
        {in: [{name: 'a,,a', isNegatory: true}], out: '\'!a,,a\''},
        {
          in: [{name: 'a', isNegatory: true}, {name: 'b', isNegatory: false}, {name: 'c,c', isNegatory: false}, {name: 'd,d', isNegatory: true}],
          out: '!a,b,\'c,c\',\'!d,d\'',
        },
      ];
      _.each(valids, (valid) => {
        const res = serializeOrderbyList(valid.in);
        assert.strictEqual(res, valid.out);
        assert.doesNotThrow(() => validateOrderbyList(valid.in));
      });
    });

    it('any failures fail with the same error as the serializer', () => {
      const invalids = [
        '',
        0,
        {},
        'abers',
        [{name: 'has_a_#'}],
        [{name: 'has_a_?'}],
        [{name: 'has_a_&'}],
        [{name: 'has_a_/'}],
        [{name: 'has space'}],
      ];
      _.each(invalids, (invalid) => {
        const serializeErr = assertThrows(() => serializeOrderbyList(invalid));
        const validateErr = assertThrows(() => validateOrderbyList(invalid));
        assert.deepStrictEqual(serializeErr, validateErr);
      });
    });

  });

});
