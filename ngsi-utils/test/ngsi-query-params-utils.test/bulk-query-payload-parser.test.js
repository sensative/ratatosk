/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const {assertThrows} = require('../../src');

const {
  validateAttrsParam,
  validateAttrsList,
} = require('../../src/utils/ngsi-query-params-utils/attrs-param-parser');
const {
  validateMetadataParam,
  validateMetadataList,
} = require('../../src/utils/ngsi-query-params-utils/metadata-param-parser');

const {
  errors,
  formatBulkQueryPayload,
  validateBulkQueryPayload,
} = require('../../src/utils/ngsi-query-params-utils/bulk-query-payload-parser');

describe('bulk-query-payload-parser', () => {

  describe('errors', () => {

    it('there are 8 NgsiMetadataParamErrors', () => {
      assert.strictEqual(_.size(errors), 8);
    });

    it('the errors have some "mathches" functionality and identifier flag', () => {
      const nonmatchingErr = new Error('this one does not match nuthin');
      _.each(errors, (error) => {
        const basicErr = error();
        assert(basicErr.isNgsiBulkQueryPayloadParserError);
        assert(basicErr.name);
        assert(error.matches(basicErr));
        assert(error.matches(basicErr.message));
        assert(!error.matches(nonmatchingErr));
        assert(!error.matches(nonmatchingErr.message));
      });
    });

  });

  describe('basic empty cases', () => {

    it('can be nil or empty', () => {
      const valids = [
        null,
        undefined,
        {},
        [],
      ];
      _.each(valids, (valid) => {
        const res = formatBulkQueryPayload(valid);
        assert.deepStrictEqual(res, {});
      });
    });

    it('anything else that is NOT an Object fails', () => {
      const invalids = [
        true,
        false,
        19,
        'asdf',
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => formatBulkQueryPayload(invalid), errors.INVALID_BULK_QUERY_PAYLOAD_ARG());
      });
    });

    it('having both id & idPattern is proscribed (ngsi spec)', () => {
      const entities = [{id: null, idPattern: null}];
      assertThrows(() => formatBulkQueryPayload({entities}), errors.INCOMPATIBLE_ENTITY_ID_PROPS());
    });

    it('having both type & typePattern is proscribed (ngsi spec)', () => {
      const entities = [{id: 'berop', type: null, typePattern: null}];
      assertThrows(() => formatBulkQueryPayload({entities}), errors.INCOMPATIBLE_ENTITY_TYPE_PROPS());
    });

  });

  describe('attrs prop', () => {

    it('must either survive validateAttrsParam or validateAttrsList from attrs-param-parser', () => {
      const valids = [
        {in: null, out: null},
        {in: undefined, out: undefined},
        {in: 'asdf', out: ['asdf']},
        {in: 'asdf,ksksk', out: ['asdf', 'ksksk']},
        {in: ['asdf'], out: ['asdf']},
        {in: ['asdf', 'ksksk'], out: ['asdf', 'ksksk']},
      ];
      _.each(valids, (valid) => {
        // check conformance
        try {
          validateAttrsParam(valid.in);
        } catch (err) {
          try {
            validateAttrsList(valid.in);
          } catch (err) {
            throw new Error('Both fail!!!!!');
          }
        }
        // and evaluate
        const res = formatBulkQueryPayload({attrs: valid.in});
        assert.deepStrictEqual(res.attrs, valid.out);
      });
    });

    it('and invalids fail BOTH validateAttrsList AND validateAttrsParam', () => {
      const invalids = [
        '',
        ',',
        [''],
        ['validItem', ''],
      ];
      _.each(invalids, (invalid) => {
        // check conformance
        let errParam;
        try {
          errParam = assertThrows(() => validateAttrsParam(invalid));
        } catch (err) {}
        let errList;
        try {
          errList = assertThrows(() => validateAttrsList(invalid));
        } catch (err) {}
        assert(errParam || errList);
        // and performance
        const errFormat = assertThrows(() => formatBulkQueryPayload({attrs: invalid}));
        const matches = _.isEqual(errFormat, errParam) || _.isEqual(errFormat, errList);
        assert(matches);
      });
    });

  });

  describe('metadata prop', () => {

    it('must either survive validateMetadataParam or validateMetadataList from attrs-param-parser', () => {
      const valids = [
        {in: null, out: null},
        {in: undefined, out: undefined},
        {in: 'asdf', out: ['asdf']},
        {in: 'asdf,ksksk', out: ['asdf', 'ksksk']},
        {in: ['asdf'], out: ['asdf']},
        {in: ['asdf', 'ksksk'], out: ['asdf', 'ksksk']},
      ];
      _.each(valids, (valid) => {
        // check conformance to baser functions
        try {
          validateMetadataParam(valid.in);
        } catch (err) {
          try {
            validateMetadataList(valid.in);
          } catch (err) {
            throw new Error('Both fail!!!!!');
          }
        }
        // and evaluate
        const res = formatBulkQueryPayload({metadata: valid.in});
        assert.deepStrictEqual(res.metadata, valid.out);
      });
    });

    it('and invalids fail BOTH validateAttrsList AND validateAttrsParam', () => {
      const invalids = [
        '',
        ',',
        [''],
      ];
      // check conformance
      _.each(invalids, (invalid) => {
        let errParam;
        try {
          errParam = assertThrows(() => validateAttrsParam(invalid));
        } catch (err) {}
        let errList;
        try {
          errList = assertThrows(() => validateAttrsList(invalid));
        } catch (err) {}
        assert(errParam || errList);
        // and performance
        const errFormat = assertThrows(() => formatBulkQueryPayload({attrs: invalid}));
        const matches = _.isEqual(errFormat, errParam) || _.isEqual(errFormat, errList);
        assert(matches);
      });

    });

  });

  describe('entities prop', () => {

    it('the entities prop must be an array if present', () => {
      const invalids = [
        false,
        0,
        23,
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => formatBulkQueryPayload({entities: invalid}), errors.INVALID_PAYLOAD_ENTITIES());
      });
    });

    it('can only have "id" or "idPattern, NOT both"', () => {
      const invalid = [{id: undefined, idPattern: undefined}];
      assertThrows(() => formatBulkQueryPayload({entities: invalid}), errors.INCOMPATIBLE_ENTITY_ID_PROPS());
    });

    it('MUST have either "id" or "idPattern" set', () => {
      const invalid = [{type: 'validTypeButNoIdProp'}];
      assertThrows(() => formatBulkQueryPayload({entities: invalid}), errors.MISSING_ENTITY_ID_PROP());
    });

    it('can only have "type" or "typePattern, NOT both"', () => {
      const invalid = [{type: undefined, typePattern: undefined, id: 'anIdPropIsAlwaysNeeded'}];
      assertThrows(() => formatBulkQueryPayload({entities: invalid}), errors.INCOMPATIBLE_ENTITY_TYPE_PROPS());
    });

    it('an entity works with valid "id" if either validateEntityIdList or validateEntityIdParam', () => {
      const valids = [
        {in: {id: 'asdf'}, out: [{id: 'asdf'}]},
      ];
      _.each(valids, (valid) => {
        const res = formatBulkQueryPayload({entities: [valid.in]});
        assert.deepStrictEqual(res, {entities: valid.out});
      });
    });

  });

  describe('expression prop', () => {

    it('some valids', () => {
      const valids = [
        {q: 'm>2'},
        {mq: 'a.m>2'},
        {q: 'm>2;n<2', mq: 'aa.aa~=sdfd;pp.pp>=2.234'},
      ];
      _.each(valids, (valid) => {
        const res = formatBulkQueryPayload({expression: valid});
        assert.deepStrictEqual(res.expression, valid);
      });

    });

  });

  describe('validateBulkQueryPayload (a non-returning wrapper for formatBulkQueryPayload)', () => {

    it('some invalids', () => {
      const invalids = [
        true,
        false,
        19,
        'asdf',
        {entities: {id: null, idPattern: null}},
        {id: 'berop', type: null, typePattern: null},
      ];
      _.each(invalids, (invalid) => {
        const validateErr = assertThrows(() => validateBulkQueryPayload(invalid));
        const formatErr = assertThrows(() => formatBulkQueryPayload(invalid));
        assert.deepStrictEqual(validateErr, formatErr);
      });
    });

    it('some valids', () => {
      const valids = [
        {attrs: ['asdf']},
      ];
      _.each(valids, (valid) => {
        assert.doesNotThrow(() => validateBulkQueryPayload(valid));
        assert.doesNotThrow(() => formatBulkQueryPayload(valid));
      });
    });

  });

});
