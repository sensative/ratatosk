/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const {assertThrows} = require('../../src');

const {
  validateAttrsParam,
  validateAttrsList,
} = require('../../src/utils/ngsi-query-params-utils/attrs-param-parser');
const {
  validateMetadataParam,
  validateMetadataList,
} = require('../../src/utils/ngsi-query-params-utils/metadata-param-parser');
const {
  validateEntityIdParam,
  validateEntityIdList,
  validateEntityTypeParam,
  validateEntityTypeList,
  validateIdPatternParam,
  validateTypePatternParam,
  validatePageLimitParam,
  validatePageOffsetParam,
} = require('../../src/utils/ngsi-query-params-utils/basic-param-parser');


const {
  errors,
  formatQueryParams,
  assertQueryParamsAllCompatible,
} = require('../../src/utils/ngsi-query-params-utils/query-params-parser');

const {ENTITY_REQUEST_PARAM_KEYS} = require('@ratatosk/ngsi-constants');

describe('query-params-parser', () => {

  describe('errors', () => {

    it('there are 4 isNgsiQueryParamsParserError', () => {
      assert.strictEqual(_.size(errors), 4);
    });

    it('the errors have some "mathches" functionality and identifier flag', () => {
      const nonmatchingErr = new Error('this one does not match nuthin');
      _.each(errors, (error) => {
        const basicErr = error();
        assert(basicErr.isNgsiQueryParamsParserError);
        assert(basicErr.name);
        assert(error.matches(basicErr));
        assert(error.matches(basicErr.message));
        assert(!error.matches(nonmatchingErr));
        assert(!error.matches(nonmatchingErr.message));
      });
    });

  });

  describe('assertQueryParamsAllCompatible', () => {

    it('having both id & idPattern is proscribed (ngsi spec)', () => {
      const params = {id: null, idPattern: null};
      assertThrows(() => assertQueryParamsAllCompatible(params), errors.INCOMPATIBLE_PARAMS_ID_PATTERN());
    });

    it('having both id & idPattern is proscribed (ngsi spec)', () => {
      const params = {type: null, typePattern: null};
      assertThrows(() => assertQueryParamsAllCompatible(params), errors.INCOMPATIBLE_PARAMS_TYPE_PATTERN());
    });

  });

  describe('basic empty cases', () => {

    it('can be nil or empty', () => {
      const valids = [
        null,
        undefined,
        {},
        [],
      ];
      _.each(valids, (valid) => {
        const res = formatQueryParams(valid);
        assert.deepStrictEqual(res, {});
      });
    });

    it('anything else that is NOT an Object fails', () => {
      const invalids = [
        true,
        false,
        19,
        'asdf',
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => formatQueryParams(invalid), errors.INVALID_QUERY_PARAMS_ARG());
      });
    });

    it('having both id & idPattern is proscribed (ngsi spec)', () => {
      const params = {id: null, idPattern: null};
      assertThrows(() => formatQueryParams(params), errors.INCOMPATIBLE_PARAMS_ID_PATTERN());
    });

    it('having both id & idPattern is proscribed (ngsi spec)', () => {
      const params = {type: null, typePattern: null};
      assertThrows(() => formatQueryParams(params), errors.INCOMPATIBLE_PARAMS_TYPE_PATTERN());
    });

  });

  describe('entityId input param', () => {

    const entityIdKey = ENTITY_REQUEST_PARAM_KEYS.ENTITY_ID;

    it('the entityId list can be supplied either in array or string form (i.e. parsed or serialized)', () => {
      const valids = [
        {in: {[entityIdKey]: undefined}, out: {[entityIdKey]: null}},
        {in: {[entityIdKey]: null}, out: {[entityIdKey]: null}},
        {in: {[entityIdKey]: 'asdf'}, out: {[entityIdKey]: 'asdf'}},
        {in: {[entityIdKey]: ['asdf']}, out: {[entityIdKey]: 'asdf'}},
        {in: {[entityIdKey]: 'a,b,\'c,d\''}, out: {[entityIdKey]: 'a,b,\'c,d\''}},
        {in: {[entityIdKey]: ['a', 'b', 'c,d']}, out: {[entityIdKey]: 'a,b,\'c,d\''}},
      ];
      _.each(valids, (valid) => {
        const res = formatQueryParams(valid.in);
        assert.deepStrictEqual(res, valid.out);
      });
    });

    it('an entityId string that fails, fails with the same error as validateEntityIdParam', () => {
      const invalids = [
        '',
        'a\'b',
        'a_#',
        'a,a',
      ];
      _.each(invalids, (invalid) => {
        const formatErr = assertThrows(() => formatQueryParams({[entityIdKey]: invalid}));
        const validErr = assertThrows(() => validateEntityIdParam(invalid));
        assert.deepStrictEqual(formatErr, validErr);
      });
    });

    it('an entityId  array that fails, fails with the same error as validateAttrsList', () => {
      const invalids = [
        [''],
        ['a\'b'],
        ['a_#'],
        ['a', 'a'],
      ];
      _.each(invalids, (invalid) => {
        const formatErr = assertThrows(() => formatQueryParams({[entityIdKey]: invalid}));
        const validErr = assertThrows(() => validateEntityIdList(invalid));
        assert.deepStrictEqual(formatErr, validErr);
      });
    });

  });

  describe('entityType input param', () => {

    const entityTypeKey = ENTITY_REQUEST_PARAM_KEYS.ENTITY_TYPE;

    it('the entityType param can be supplied either in array or string form (i.e. parsed or serialized)', () => {
      const valids = [
        {in: {[entityTypeKey]: undefined}, out: {[entityTypeKey]: null}},
        {in: {[entityTypeKey]: null}, out: {[entityTypeKey]: null}},
        {in: {[entityTypeKey]: 'asdf'}, out: {[entityTypeKey]: 'asdf'}},
        {in: {[entityTypeKey]: ['asdf']}, out: {[entityTypeKey]: 'asdf'}},
        {in: {[entityTypeKey]: 'a,b,\'c,d\''}, out: {[entityTypeKey]: 'a,b,\'c,d\''}},
        {in: {[entityTypeKey]: ['a', 'b', 'c,d']}, out: {[entityTypeKey]: 'a,b,\'c,d\''}},
      ];
      _.each(valids, (valid) => {
        const res = formatQueryParams(valid.in);
        assert.deepStrictEqual(res, valid.out);
      });
    });

    it('an entityType string that fails, fails with the same error as validateEntityTypeParam', () => {
      const invalids = [
        '',
        'a\'b',
        'a_#',
        'a,a',
      ];
      _.each(invalids, (invalid) => {
        const formatErr = assertThrows(() => formatQueryParams({[entityTypeKey]: invalid}));
        const validErr = assertThrows(() => validateEntityTypeParam(invalid));
        assert.deepStrictEqual(formatErr, validErr);
      });
    });

    it('an entityType array that fails, fails with the same error as validateEntityTypeList', () => {
      const invalids = [
        [''],
        ['a\'b'],
        ['a_#'],
        ['a', 'a'],
      ];
      _.each(invalids, (invalid) => {
        const formatErr = assertThrows(() => formatQueryParams({[entityTypeKey]: invalid}));
        const validErr = assertThrows(() => validateEntityTypeList(invalid));
        assert.deepStrictEqual(formatErr, validErr);
      });
    });

  });

  describe('idPattern param', () => {

    const idPatternKey = ENTITY_REQUEST_PARAM_KEYS.ENTITY_ID_PATTERN;

    it('must be a string that validates as a regex pattern', () => {
      const valids = [
        {in: {[idPatternKey]: undefined}, out: {[idPatternKey]: null}},
        {in: {[idPatternKey]: null}, out: {[idPatternKey]: null}},
        {in: {[idPatternKey]: 'asdf'}, out: {[idPatternKey]: 'asdf'}},
        {in: {[idPatternKey]: 'asdf,asdf'}, out: {[idPatternKey]: 'asdf,asdf'}},
        {in: {[idPatternKey]: '.*'}, out: {[idPatternKey]: '.*'}},
      ];
      _.each(valids, (valid) => {
        const res = formatQueryParams(valid.in);
        assert.deepStrictEqual(res, valid.out);
      });
    });

    it('if the param fails, it fails with the same error as validateIdPatternParam', () => {
      const invalids = [
        10,
        '',
        'a)a',
      ];
      _.each(invalids, (invalid) => {
        const formatErr = assertThrows(() => formatQueryParams({[idPatternKey]: invalid}));
        const validErr = assertThrows(() => validateIdPatternParam(invalid));
        assert.deepStrictEqual(formatErr, validErr);
      });
    });

  });

  describe('typePattern param', () => {

    const typePatternKey = ENTITY_REQUEST_PARAM_KEYS.ENTITY_TYPE_PATTERN;

    it('must be a string that validates as a regex pattern', () => {
      const valids = [
        {in: {[typePatternKey]: undefined}, out: {[typePatternKey]: null}},
        {in: {[typePatternKey]: null}, out: {[typePatternKey]: null}},
        {in: {[typePatternKey]: 'asdf'}, out: {[typePatternKey]: 'asdf'}},
        {in: {[typePatternKey]: 'asdf,asdf'}, out: {[typePatternKey]: 'asdf,asdf'}},
        {in: {[typePatternKey]: '.*'}, out: {[typePatternKey]: '.*'}},
      ];
      _.each(valids, (valid) => {
        const res = formatQueryParams(valid.in);
        assert.deepStrictEqual(res, valid.out);
      });
    });

    it('if the param fails, it fails with the same error as validateTypePatternParam', () => {
      const invalids = [
        10,
        '',
        'a)a',
      ];
      _.each(invalids, (invalid) => {
        const formatErr = assertThrows(() => formatQueryParams({[typePatternKey]: invalid}));
        const validErr = assertThrows(() => validateTypePatternParam(invalid));
        assert.deepStrictEqual(formatErr, validErr);
      });
    });

  });

  describe('attrs input param', () => {

    const attrsKey = ENTITY_REQUEST_PARAM_KEYS.ATTRS_LIST;

    it('the attrs list can be supplied either in array or string form (i.e. parsed or serialized)', () => {
      const valids = [
        {in: {[attrsKey]: undefined}, out: {[attrsKey]: null}},
        {in: {[attrsKey]: null}, out: {[attrsKey]: null}},
        {in: {[attrsKey]: 'asdf'}, out: {[attrsKey]: 'asdf'}},
        {in: {[attrsKey]: ['asdf']}, out: {[attrsKey]: 'asdf'}},
        {in: {[attrsKey]: 'a,b,\'c,d\''}, out: {[attrsKey]: 'a,b,\'c,d\''}},
        {in: {[attrsKey]: ['a', 'b', 'c,d']}, out: {[attrsKey]: 'a,b,\'c,d\''}},
      ];
      _.each(valids, (valid) => {
        const res = formatQueryParams(valid.in);
        assert.deepStrictEqual(res, valid.out);
      });
    });

    it('an attrsParam string that fails, fails with the same error as validateAttrsParam', () => {
      const invalids = [
        '',
        'a\'b',
        'a_#',
        'a,a',
      ];
      _.each(invalids, (invalid) => {
        const formatErr = assertThrows(() => formatQueryParams({[attrsKey]: invalid}));
        const validErr = assertThrows(() => validateAttrsParam(invalid));
        assert.deepStrictEqual(formatErr, validErr);
      });
    });

    it('an attrsList array that fails, fails with the same error as validateAttrsList', () => {
      const invalids = [
        [''],
        ['a\'b'],
        ['a_#'],
        ['a', 'a'],
      ];
      _.each(invalids, (invalid) => {
        const formatErr = assertThrows(() => formatQueryParams({[attrsKey]: invalid}));
        const validErr = assertThrows(() => validateAttrsList(invalid));
        assert.deepStrictEqual(formatErr, validErr);
      });
    });

  });

  describe('metadata input param', () => {

    const metadataKey = ENTITY_REQUEST_PARAM_KEYS.METADATA_LIST;

    it('the metadata list can be supplied either in array or string form (i.e. parsed or serialized)', () => {
      const valids = [
        {in: {[metadataKey]: undefined}, out: {[metadataKey]: null}},
        {in: {[metadataKey]: null}, out: {[metadataKey]: null}},
        {in: {[metadataKey]: 'asdf'}, out: {[metadataKey]: 'asdf'}},
        {in: {[metadataKey]: ['asdf']}, out: {[metadataKey]: 'asdf'}},
        {in: {[metadataKey]: 'a,b,\'c,d\''}, out: {[metadataKey]: 'a,b,\'c,d\''}},
        {in: {[metadataKey]: ['a', 'b', 'c,d']}, out: {[metadataKey]: 'a,b,\'c,d\''}},
      ];
      _.each(valids, (valid) => {
        const res = formatQueryParams(valid.in);
        assert.deepStrictEqual(res, valid.out);
      });
    });

    it('an metadataParam string that fails, fails with the same error as validateMetadataParam', () => {
      const invalids = [
        '',
        'a\'b',
        'a_#',
        'a,a',
      ];
      _.each(invalids, (invalid) => {
        const formatErr = assertThrows(() => formatQueryParams({[metadataKey]: invalid}));
        const validErr = assertThrows(() => validateMetadataParam(invalid));
        assert.deepStrictEqual(formatErr, validErr);
      });
    });

    it('an metadataList array that fails, fails with the same error as validateMetadataList', () => {
      const invalids = [
        [''],
        ['a\'b'],
        ['a_#'],
        ['a', 'a'],
      ];
      _.each(invalids, (invalid) => {
        const formatErr = assertThrows(() => formatQueryParams({[metadataKey]: invalid}));
        const validErr = assertThrows(() => validateMetadataList(invalid));
        assert.deepStrictEqual(formatErr, validErr);
      });
    });

  });

  describe('orderby input param', () => {

    it('some valid orderBy params', () => {
      const valids = [
        'a,!b',
        [{name: 'a', isNegatory: false}, {name: 'b', isNegatory: true}],
      ];
      _.each(valids, (valid) => {
        const params = {orderBy: valid};
        assert.doesNotThrow(() => formatQueryParams(params));
      });
    });
  });

  describe('page-limit param', () => {

    it('some valid pagelimit params', () => {
      const valids = [
        1,
        29,
        '1',
        '29',
      ];
      _.each(valids, (valid) => {
        const params = {limit: valid};
        assert.doesNotThrow(() => formatQueryParams(params));
      });
    });

    it('if invalid and fails, it fails the same way as validatePageLimitParam', () => {
      const invalids = [
        '',
        [],
        {},
        0,
        '0',
        -1,
        '-1',
        1.5,
      ];
      _.each(invalids, (invalid) => {
        const params = {limit: invalid};
        const parseErr = assertThrows(() => formatQueryParams(params));
        const validErr = assertThrows(() => validatePageLimitParam(invalid));
        assert.deepStrictEqual(parseErr, validErr);
      });
    });

  });

  describe('page-offset param', () => {

    it('some valid pageOffset params', () => {
      const valids = [
        0,
        1,
        29,
        '0',
        '1',
        '29',
      ];
      _.each(valids, (valid) => {
        const params = {offset: valid};
        assert.doesNotThrow(() => formatQueryParams(params));
      });
    });

    it('if invalid and fails, it fails the same way as validatePageOffsetParam', () => {
      const invalids = [
        '',
        [],
        {},
        -1,
        '-1',
        1.5,
      ];
      _.each(invalids, (invalid) => {
        const params = {offset: invalid};
        const parseErr = assertThrows(() => formatQueryParams(params));
        const validErr = assertThrows(() => validatePageOffsetParam(invalid));
        assert.deepStrictEqual(parseErr, validErr);
      });
    });

  });

  //
  //  BELOW HERE STILL NEEDS SOME WORK
  //


  describe('REPRESENTATION_OPTIONS (i.e. options) & count & representationTypes, gets validated with options-param-parser', () => {

    it('silly values should be left empty', () => {
      const sillies = [
        null,
        {},
        [],
        undefined,
        34,
        {any: 'object'},
      ];
      _.each(sillies, (silly) => {
        const params = formatQueryParams({options: silly});
        assert(!!params && !_.has(params, 'options'), 'options should not be filled in');
        assert.strictEqual(_.size(params), 0);
      });
    });

    it('which one wins?', () => {
      const ambigs = [
        {options: 'count', count: false}, // "count" key wins out here
        {options: 'append', append: false}, // "append" key wins out here
      ];
      _.each(ambigs, (ambi) => {
        const params = formatQueryParams(ambi);
        assert(!!params && !_.has(params, 'options'), 'options should not be filled in');
        assert.strictEqual(_.size(params), 0);
      });
    });

    it('some combos that result in {options: "count"}', () => {
      const counters = [
        {count: true},
        {options: 'count'},
        {representationType: null, count: true},
        {count: 1},
      ];
      _.each(counters, (counter) => {
        const params = formatQueryParams(counter);
        assert(_.has(params, 'options'), 'options should be filled in');
        assert.strictEqual(params.options, 'count', 'should be just "count"');
      });
    });

    it('some combos that result in {options: "append"}', () => {
      const counters = [
        {append: true},
        {options: 'append'},
        {representationType: null, append: true},
        {append: 1},
      ];
      _.each(counters, (counter) => {
        const params = formatQueryParams(counter);
        assert(_.has(params, 'options'), 'options should be filled in');
        assert.strictEqual(params.options, 'append', 'should be just "append"');
      });
    });

    it('some other valid combos', () => {
      const valids = [
        [{options: 'values', append: true}, 'append,values'],
        [{options: 'values', count: true}, 'count,values'],
        [{options: 'values', count: true, append: true}, 'append,count,values'],
        [{representationType: 'keyValues', options: 'count'}, 'count,keyValues'],
        [{representationType: 'keyValues', count: true}, 'count,keyValues'],
        [{options: 'count', representationType: 'unique', count: false}, 'unique'], // the count: false overrides
        [{options: 'append,count', representationType: 'unique', count: false, append: false}, 'unique'], // the count: false overrides
      ];
      _.each(valids, (valid) => {
        const params = formatQueryParams(valid[0]);
        assert(_.has(params, 'options'), 'options should be filled in');
        assert.strictEqual(params.options, valid[1]);
      });
    });

  });

  describe('GEOMETRY stuff', () => {

    it('some valids', () => {
      const valids = [
        {
          in: {geometry: 'point', coords: '23,23', georel: 'near;minDistance:23'},
          out: {geometry: 'point', coords: '23,23', georel: 'near;minDistance:23'},
        },
        {
          in: {geometry: 'polygon', coords: '1,2;3,4;5,6;1,2', georel: 'coveredBy'},
          out: {geometry: 'polygon', coords: '1,2;3,4;5,6;1,2', georel: 'coveredBy'},
        },
      ];
      _.each(valids, (valid) => {
        const params = valid.in;
        const formatted = formatQueryParams(params);
        assert.deepStrictEqual(formatted, valid.out);
      });
    });

    it('some invalids (that are not invalid due to)', () => {
      const invalids = [
        // need all 3
        {in: {geometry: 'point'}, err: errors.INCOMPATIBLE_GEOMETRY_PARAMS()},
        {in: {coords: 1, georel: 2}, err: errors.INCOMPATIBLE_GEOMETRY_PARAMS()},
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => formatQueryParams(invalid.in), invalid.err);
      });
    });

  });

});
