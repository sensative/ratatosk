/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');

const {
  assertThrows,
} = require('../../src');

const {
  errors,
  parseGeometryAndCoordsParam,
  parseGeorelParam,
} = require('../../src/utils/ngsi-query-params-utils/geometry-params-parser');


describe('geometry-params-parser', () => {

  describe('errors', () => {

    it('there are 13 NGSI_GEOMETRY_PARAMS_PARSER_ERROR errors', () => {
      assert.strictEqual(_.size(errors), 13);
    });

    it('the errors have some "matches" functionality and identifier flag', () => {
      const nonmatchingErr = new Error('this one does not match nuthin');
      _.each(errors, (error) => {
        const datumErr = error();
        assert(datumErr.isNgsiGeometryParamsParserError);
        assert(error.matches(datumErr));
        assert(error.matches(datumErr.message));
        assert(!error.matches(nonmatchingErr));
        assert(!error.matches(nonmatchingErr.message));
      });
    });

  });

  describe('parseGeometryAndCoordsParam', () => {

    it('some valids', () => {
      const valids = [
        {
          in: '23,24',
          geom: 'point',
          out: {type: 'geo:point', value: '23,24'},
        },
        {
          in: '23,24;25,26',
          geom: 'box',
          out: {type: 'geo:box', value: ['23,24', '25,26']},
        },
        {
          in: '23,24;25,26',
          geom: 'line',
          out: {type: 'geo:line', value: ['23,24', '25,26']},
        },
        {
          in: '23,24;25,26;2,3',
          geom: 'line',
          out: {type: 'geo:line', value: ['23,24', '25,26', '2,3']},
        },
        {
          in: '23,24;25,26;2,3;23,24',
          geom: 'polygon',
          out: {type: 'geo:polygon', value: ['23,24', '25,26', '2,3', '23,24']},
        },
      ];
      _.each(valids, (valid) => {
        const coordList = parseGeometryAndCoordsParam(valid.geom, valid.in);
        assert.deepStrictEqual(coordList, valid.out);
      });
    });

    it('some invalids', () => {
      const invalids = [
        {in: null, geom: null, err: errors.INVALID_GEOMETRY()},
        {in: null, geom: 'invalid_geom_type', err: errors.INVALID_GEOMETRY()},
        {in: null, geom: 'point', err: errors.INVALID_COORDS_ARG()},
        {in: 22, geom: 'point', err: errors.INVALID_COORDS_ARG()},
        {in: '22,23;22,24', geom: 'point', err: errors.INVALID_NUM_COORD_PAIRS_POINT()},
        {in: '22,23', geom: 'box', err: errors.INVALID_NUM_COORD_PAIRS_BOX()},
        {in: '22,23', geom: 'line', err: errors.INVALID_NUM_COORD_PAIRS_LINE()},
        {in: '22,23', geom: 'polygon', err: errors.INVALID_NUM_COORD_PAIRS_POLYGON()},
        {in: '1,2;3,4;5,6;7,8', geom: 'polygon', err: errors.INVALID_SIMPLE_POLYGON_CLOSURE()},
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => parseGeometryAndCoordsParam(invalid.geom, invalid.in), invalid.err);
      });
    });

  });

  describe('parseGeorelParam', () => {

    it('some valids', () => {
      const valids = [
        {
          in: {georel: 'coveredBy', geometry: 'polygon'},
          out: {georel: 'coveredBy'},
        },
        {
          in: {georel: 'intersects', geometry: 'polygon'},
          out: {georel: 'intersects'},
        },
        {
          in: {georel: 'equals', geometry: 'point'},
          out: {georel: 'equals'},
        },
        {
          in: {georel: 'disjoint', geometry: 'polygon'},
          out: {georel: 'disjoint'},
        },
        {
          in: {georel: 'near;minDistance:202', geometry: 'point'},
          out: {georel: 'near', minDistance: 202},
        },
        {
          in: {georel: 'near;maxDistance:443', geometry: 'point'},
          out: {georel: 'near', maxDistance: 443},
        },
      ];
      _.each(valids, (valid) => {
        const {georel, geometry} = valid.in;
        const parsed = parseGeorelParam(georel, geometry);
        assert.deepStrictEqual(parsed, valid.out);
      });
    });

    it('some invalids', () => {
      const invalids = [
        {
          in: {georel: 23, geometry: 'irrelevant'},
          err: errors.INVALID_GEOREL_ARG(),
        },
        {
          in: {georel: null, geometry: 'irrelevant'},
          err: errors.INVALID_GEOREL_ARG(),
        },
        {
          in: {georel: 'equals', geometry: 'invalid_geometry'},
          err: errors.INVALID_GEOMETRY(),
        },
        {
          in: {georel: 'invalidType', geometry: 'point'},
          err: errors.INVALID_GEOREL_TYPE(),
        },
        {
          in: {georel: 'coveredBy;minDistance:23', geometry: 'polygon'},
          err: errors.INVALID_GEOREL_MODIFIER(),
        },
        // and the 'near' stuff
        {
          in: {georel: 'near', geometry: 'point'},
          err: errors.INVALID_GEOREL_NEAR_MODIFIER(),
        },
        {
          in: {georel: 'near;derp:23', geometry: 'point'},
          err: errors.INVALID_GEOREL_NEAR_MODIFIER(),
        },
        {
          in: {georel: 'near;minDistance:abc', geometry: 'point'},
          err: errors.INVALID_GEOREL_NEAR_MODIFIER(),
        },
        {
          in: {georel: 'near;maxDistance:abc', geometry: 'point'},
          err: errors.INVALID_GEOREL_NEAR_MODIFIER(),
        },
        // near accepts only geometry = 'point'
        {
          in: {georel: 'near;maxDistance:20', geometry: 'polygon'},
          err: errors.INVALID_GEOREL_NEAR_GEOMETRY(),
        },
        {
          in: {georel: 'near;maxDistance:20', geometry: 'line'},
          err: errors.INVALID_GEOREL_NEAR_GEOMETRY(),
        },
        {
          in: {georel: 'near;maxDistance:20', geometry: 'box'},
          err: errors.INVALID_GEOREL_NEAR_GEOMETRY(),
        },
        // coveredBy accepts only geometry = 'polygon'
        {
          in: {georel: 'coveredBy', geometry: 'point'},
          err: errors.INVALID_GEOREL_COVEREDBY_GEOMETRY(),
        },
        {
          in: {georel: 'coveredBy', geometry: 'box'},
          err: errors.INVALID_GEOREL_COVEREDBY_GEOMETRY(),
        },
        {
          in: {georel: 'coveredBy', geometry: 'line'},
          err: errors.INVALID_GEOREL_COVEREDBY_GEOMETRY(),
        },
      ];
      _.each(invalids, (invalid) => {
        const {georel, geometry} = invalid.in;
        assertThrows(() => parseGeorelParam(georel, geometry), invalid.err);
      });
    });

  });

});
