/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const {assertThrows} = require('../../src');

const {
  errors,
  formatBulkUpdatePayload,
  validateBulkUpdatePayload,
} = require('../../src/utils/ngsi-query-params-utils/bulk-update-payload-parser');

describe('bulk-update-payload-parser', () => {

  describe('errors', () => {

    it('there are 5 NgsiMetadataParamErrors', () => {
      assert.strictEqual(_.size(errors), 5);
    });

    it('the errors have some "mathches" functionality and identifier flag', () => {
      const nonmatchingErr = new Error('this one does not match nuthin');
      _.each(errors, (error) => {
        const basicErr = error();
        assert(basicErr.isNgsiBulkUpdatePayloadParserError);
        assert(basicErr.name);
        assert(error.matches(basicErr));
        assert(error.matches(basicErr.message));
        assert(!error.matches(nonmatchingErr));
        assert(!error.matches(nonmatchingErr.message));
      });
    });

  });

  describe('basic empty cases', () => {

    it('can be nil or empty', () => {
      const valids = [
        null,
        undefined,
      ];
      _.each(valids, (valid) => {
        const res = formatBulkUpdatePayload(valid);
        assert.deepStrictEqual(res, {});
      });
    });

    it('some valids', () => {
      const valids = [
        {
          in: {actionType: 'update', entities: [{id: 'arp', jojo: 23}]},
          out: {actionType: 'update', entities: [{id: 'arp', type: 'Thing', jojo: {value: 23, type: 'Number', metadata: {}}}]},
        },
      ];
      _.each(valids, (valid) => {
        const res = formatBulkUpdatePayload(valid.in);
        assert.deepStrictEqual(res, valid.out);
      });
    });

    it('invalid actionTypes are not allowed', () => {
      const invalid = {actionType: 'invalidType', entities: [{id: 'bff'}]};
      assertThrows(() => formatBulkUpdatePayload(invalid), errors.INVALID_BULK_UPDATE_ACTION_TYPE());
    });

    it('entities must be non-zero length', () => {
      const invalid = {actionType: 'update', entities: []};
      assertThrows(() => formatBulkUpdatePayload(invalid), errors.INVALID_UPDATE_PAYLOAD_ENTITIES());
    });

    it('must have the correct shape', () => {
      const invalids = [
        {errrrr: 'no'},
        {actionType: 0, entities: 2, extraextra: 'noo'},
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => formatBulkUpdatePayload(invalid), errors.EXTRANEOUS_UPDATE_PAYLOAD_FIELDS());
      });
    });

    it('the entity must be formattable to an entity', () => {
      const invalids = [
        {actionType: 'update', entities: [{spud: 'nick'}]},
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => formatBulkUpdatePayload(invalid), errors.INVALID_BULK_UPDATE_ENTITY_ITEM());
      });
    });

  });

  describe('validateBulkUpdatePayload (a non-returning wrapper for formatBulkUpdatePayload)', () => {

    it('some invalids', () => {
      const invalids = [
        true,
        false,
        19,
        'asdf',
        {entities: {bub: null}},
        {actionType: 'bedropligt', entities: [{id: 'brown', type: 'stick'}]},
      ];
      _.each(invalids, (invalid) => {
        const validateErr = assertThrows(() => validateBulkUpdatePayload(invalid));
        const formatErr = assertThrows(() => formatBulkUpdatePayload(invalid));
        assert.deepStrictEqual(validateErr, formatErr);
      });
    });

    it('some valids', () => {
      const valids = [
        {actionType: 'append', entities: [{id: 'werp'}]},
      ];
      _.each(valids, (valid) => {
        assert.doesNotThrow(() => validateBulkUpdatePayload(valid));
        assert.doesNotThrow(() => formatBulkUpdatePayload(valid));
      });
    });

  });

});
