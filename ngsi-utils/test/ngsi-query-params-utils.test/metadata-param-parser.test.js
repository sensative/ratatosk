/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const {assertThrows} = require('../../src');

const {
  errors,
  parseMetadataParam,
  serializeMetadataList,
  validateMetadataParam,
  validateMetadataList,
} = require('../../src/utils/ngsi-query-params-utils/metadata-param-parser');

describe('metadata-param-parser', () => {

  describe('errors', () => {

    it('there are 5 NgsiMetadataParamErrors', () => {
      assert.strictEqual(_.size(errors), 5);
    });

    it('the errors have some "mathches" functionality and identifier flag', () => {
      const nonmatchingErr = new Error('this one does not match nuthin');
      _.each(errors, (error) => {
        const basicErr = error();
        assert(basicErr.isNgsiMetadataParamError);
        assert(basicErr.name);
        assert(error.matches(basicErr));
        assert(error.matches(basicErr.message));
        assert(!error.matches(nonmatchingErr));
        assert(!error.matches(nonmatchingErr.message));
      });
    });

  });

  describe('parseMetadataParam', () => {

    it('an empty param is ok (returns empty list)', () => {
      const empties = [
        undefined,
        null,
      ];
      _.each(empties, (empty) => {
        const res = parseMetadataParam(empty);
        assert.deepStrictEqual(res, []);
      });
    });

    it('some other valid metadata params', () => {
      const valids = [
        {in: 'anyValidFieldName', out: ['anyValidFieldName']},
        {in: '\'canBeWrapped\'', out: ['canBeWrapped']},
        {in: '\'can,contain\',\'some,,,,commas\'', out: ['can,contain', 'some,,,,commas']},
        {in: 'a,b,c,d,\'e\'', out: ['a', 'b', 'c', 'd', 'e']},
        // the builtins
        {in: 'dateCreated', out: ['dateCreated']},
        {in: 'dateModified', out: ['dateModified']},
        {in: 'previousValue', out: ['previousValue']},
        {in: 'actionType', out: ['actionType']},
        // and the wildcard
        {in: '*', out: ['*']},
      ];
      _.each(valids, (valid) => {
        const res = parseMetadataParam(valid.in);
        assert.deepStrictEqual(res, valid.out);
      });
    });

    it('only accepts non-empty strings', () => {
      const invalids = [
        '',
        0,
        {},
        [],
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => parseMetadataParam(invalid), errors.EMPTY_METADATA_PARAM());
      });
    });

    it('cannot contain duplicates', () => {
      const invalids = [
        'a,a',
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => parseMetadataParam(invalid), errors.INVALID_METADATA_DUPLICATES());
      });
    });

    it('an entity id CANNOT contain the single quote (\')', () => {
      const invalid = '\'';
      assertThrows(() => parseMetadataParam(invalid), errors.INVALID_METADATA_FORMATTING());
    });

    it('If parsing the list into metadata fails due to some missformatting of separators and groupers', () => {
      const invalids = [
        '\'',
        'a\'',
        'a\'b',
        'a\',b',
        'a\'b,',
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => parseMetadataParam(invalid, errors.INVALID_METADATA_FORMATTING()));
      });
    });

    it('all items must have a valid/allowed metadataName (no invalid chars)', () => {
      const invalids = [
        'has_a_#',
        'has_a_?',
        'has_a_&',
        'has_a_/',
        'has space',
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => parseMetadataParam(invalid), errors.INVALID_METADATA_LIST_ITEM());
      });
    });

  });

  describe('validateMetadataParam', () => {

    it('if it is parseable, then it is validateable', () => {
      const valids = [
        {in: null, out: []},
        {in: undefined, out: []},
        {in: 'anyValidFieldName', out: ['anyValidFieldName']},
        {in: '\'canBeWrapped\'', out: ['canBeWrapped']},
        {in: '\'can,contain\',\'some,,,,commas\'', out: ['can,contain', 'some,,,,commas']},
        {in: 'a,b,c,d,\'e\'', out: ['a', 'b', 'c', 'd', 'e']},
        // the builtins
        {in: 'dateCreated', out: ['dateCreated']},
        {in: 'dateModified', out: ['dateModified']},
        {in: 'previousValue', out: ['previousValue']},
        {in: 'actionType', out: ['actionType']},
        // and the wildcard
        {in: '*', out: ['*']},
      ];
      _.each(valids, (valid) => {
        const res = parseMetadataParam(valid.in);
        assert.deepStrictEqual(res, valid.out);
        assert.doesNotThrow(() => validateMetadataParam(valid.in));
      });
    });

    it('any failures fail with the same error as the parser', () => {
      const invalids = [
        '',
        0,
        {},
        [],
        'a,a',
        '\'',
        'a\'',
        'a\'b',
        'a\',b',
        'a\'b,',
        'has_a_#',
        'has_a_?',
        'has_a_&',
        'has_a_/',
        'has space',
      ];
      _.each(invalids, (invalid) => {
        const parseErr = assertThrows(() => parseMetadataParam(invalid));
        const validateErr = assertThrows(() => validateMetadataParam(invalid));
        assert.deepStrictEqual(parseErr, validateErr);
      });
    });

  });

  describe('serializeMetadataList', () => {

    it('can be nil or empty', () => {
      const valids = [
        null,
        undefined,
        [],
      ];
      _.each(valids, (valid) => {
        const res = serializeMetadataList(valid);
        assert.strictEqual(res, null);
      });
    });

    it('some other valids', () => {
      const valids = [
        {in: ['singleItem'], out: 'singleItem'},
        {in: ['a', 'b', 'c', 'd', 'e'], out: 'a,b,c,d,e'},
        {in: ['comma,containing', 'stuff,get,wrapped'], out: '\'comma,containing\',\'stuff,get,wrapped\''},
        // wildcard IS allowed
        {in: ['*'], out: '*'},
        // as are builtin metadata
        {in: ['dateCreated'], out: 'dateCreated'},
        {in: ['dateModified'], out: 'dateModified'},
        {in: ['previousValue'], out: 'previousValue'},
        {in: ['actionType'], out: 'actionType'},
      ];
      _.each(valids, (valid) => {
        const res = serializeMetadataList(valid.in);
        assert.strictEqual(res, valid.out);
      });
    });

    it('but it must be an array', () => {
      const invalids = [
        '',
        0,
        {},
        'abers',
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => serializeMetadataList(invalid), errors.INVALID_METADATA_LIST());
      });
    });

    it('cannot contain dupes', () => {
      const invalids = [
        ['a', 'a'],
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => serializeMetadataList(invalid), errors.INVALID_METADATA_DUPLICATES());
      });
    });

    it('the items must, of course, be a validateable metadata', () => {
      const invalids = [
        ['has_a_#'],
        ['has_a_?'],
        ['has_a_&'],
        ['has_a_/'],
        ['has space'],
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => serializeMetadataList(invalid), errors.INVALID_METADATA_LIST_ITEM());
      });
    });

  });

  describe('validateMetadataList', () => {

    it('if it can be serialized, then the list validates ok', () => {
      const valids = [
        {in: null, out: null},
        {in: undefined, out: null},
        {in: ['singleItem'], out: 'singleItem'},
        {in: ['a', 'b', 'c', 'd', 'e'], out: 'a,b,c,d,e'},
        {in: ['comma,containing', 'stuff,get,wrapped'], out: '\'comma,containing\',\'stuff,get,wrapped\''},
        // wildcard IS allowed
        {in: ['*'], out: '*'},
        // as are builtin metadata
        {in: ['dateCreated'], out: 'dateCreated'},
        {in: ['dateModified'], out: 'dateModified'},
        {in: ['previousValue'], out: 'previousValue'},
        {in: ['actionType'], out: 'actionType'},
      ];
      _.each(valids, (valid) => {
        const res = serializeMetadataList(valid.in);
        assert.strictEqual(res, valid.out);
        assert.doesNotThrow(() => validateMetadataList(valid.in));
      });
    });

    it('any failures fail with the same error as the serializer', () => {
      const invalids = [
        '',
        0,
        {},
        'abers',
        ['a', 'a'],
        ['has_a_#'],
        ['has_a_?'],
        ['has_a_&'],
        ['has_a_/'],
        ['has space'],
      ];
      _.each(invalids, (invalid) => {
        const serializeErr = assertThrows(() => serializeMetadataList(invalid));
        const validateErr = assertThrows(() => validateMetadataList(invalid));
        assert.deepStrictEqual(serializeErr, validateErr);
      });
    });

  });

});
