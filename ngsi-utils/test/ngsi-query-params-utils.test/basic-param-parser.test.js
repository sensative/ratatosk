/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const {assertThrows} = require('../../src');

const {
  errors,
  // id
  parseEntityIdParam,
  serializeEntityIdList,
  validateEntityIdParam,
  validateEntityIdList,
  // type
  parseEntityTypeParam,
  serializeEntityTypeList,
  validateEntityTypeParam,
  validateEntityTypeList,
  // pattern stuff
  validateIdPatternParam,
  validateTypePatternParam,
  // page limit & offset params
  validatePageLimitParam,
  validatePageOffsetParam,
  validatePageItemIdParam,
  validatePageDirectionParam,
} = require('../../src/utils/ngsi-query-params-utils/basic-param-parser');

describe('basic-param-parser', () => {

  describe('errors', () => {

    it('there are 19 NgsiBasicParamErrors', () => {
      assert.strictEqual(_.size(errors), 18);
    });

    it('the errors have some "mathches" functionality and identifier flag', () => {
      const nonmatchingErr = new Error('this one does not match nuthin');
      _.each(errors, (error) => {
        const basicErr = error();
        assert(basicErr.isNgsiBasicParamError);
        assert(basicErr.name);
        assert(error.matches(basicErr));
        assert(error.matches(basicErr.message));
        assert(!error.matches(nonmatchingErr));
        assert(!error.matches(nonmatchingErr.message));
      });
    });

  });

  describe('validateIdPatternParam', () => {

    it('some valid idPatterns', () => {
      const valids = [
        null,
        undefined,
        'derp',
        'herpymerpy',
        '.*',
      ];
      _.each(valids, (valid) => {
        assert.doesNotThrow(() => validateIdPatternParam(valid));
      });
    });

    it('some invalid idPatterns', () => {
      const invalids = [
        '',
        0,
        true,
        false,
        [],
        {},
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => validateIdPatternParam(invalid), errors.EMPTY_ENTITY_ID_PATTERN());
      });
    });

  });

  describe('validateTypePatternParam', () => {

    it('some valid typePatterns', () => {
      const valids = [
        null,
        undefined,
        'derp',
        'herpymerpy',
        '.*',
      ];
      _.each(valids, (valid) => {
        assert.doesNotThrow(() => validateTypePatternParam(valid));
      });
    });

    it('some invalid typePatterns', () => {
      const invalids = [
        '',
        0,
        true,
        false,
        [],
        {},
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => validateTypePatternParam(invalid), errors.EMPTY_ENTITY_TYPE_PATTERN());
      });
    });

  });

  describe('entityId parse & serialize', () => {

    describe('parseEntityIdParam', () => {

      it('an empty param is ok (returns empty list)', () => {
        const empties = [
          undefined,
          null,
        ];
        _.each(empties, (empty) => {
          const res = parseEntityIdParam(empty);
          assert.deepStrictEqual(res, []);
        });
      });

      it('some other valid entityId params', () => {
        const valids = [
          {in: 'anyValidFieldName', out: ['anyValidFieldName']},
          {in: '\'canBeWrapped\'', out: ['canBeWrapped']},
          {in: '\'can,contain\',\'some,,,,commas\'', out: ['can,contain', 'some,,,,commas']},
          {in: 'a,b,c,d,\'e\'', out: ['a', 'b', 'c', 'd', 'e']},
        ];
        _.each(valids, (valid) => {
          const res = parseEntityIdParam(valid.in);
          assert.deepStrictEqual(res, valid.out);
        });
      });

      it('only accepts non-empty strings', () => {
        const invalids = [
          '',
          0,
          {},
          [],
        ];
        _.each(invalids, (invalid) => {
          assertThrows(() => parseEntityIdParam(invalid), errors.EMPTY_ENTITY_ID_PARAM());
        });
      });

      it('cannot contain duplicates', () => {
        const invalid = 'a,a';
        assertThrows(() => parseEntityIdParam(invalid), errors.INVALID_ENTITY_ID_DUPLICATES());
      });

      it('an entity id CANNOT contain the single quote (\')', () => {
        const invalid = '\'';
        assertThrows(() => parseEntityIdParam(invalid), errors.INVALID_ENTITY_ID_PARAM_FORMATTING());
      });

      it('If parsing the list into ids fails due to some missformatting of separators and groupers', () => {
        const invalids = [
          '\'',
          'a\'',
          'a\'b',
          'a\',b',
          'a\'b,',
        ];
        _.each(invalids, (invalid) => {
          assertThrows(() => parseEntityIdParam(invalid, errors.INVALID_ENTITY_ID_PARAM_FORMATTING()));
        });
      });

      it('all items must be valid entityId (no invalid chars)', () => {
        const invalids = [
          'has_a_#',
          'has_a_?',
          'has_a_&',
          'has_a_/',
          'has space',
        ];
        _.each(invalids, (invalid) => {
          assertThrows(() => parseEntityIdParam(invalid), errors.INVALID_ENTITY_ID_PARAM_ITEM());
        });
      });

    });

    describe('validateEntityIdParam', () => {

      it('if it is parseable, then it is validateable', () => {
        const valids = [
          {in: null, out: []},
          {in: undefined, out: []},
          {in: 'anyValidFieldName', out: ['anyValidFieldName']},
          {in: '\'canBeWrapped\'', out: ['canBeWrapped']},
          {in: '\'can,contain\',\'some,,,,commas\'', out: ['can,contain', 'some,,,,commas']},
          {in: 'a,b,c,d,\'e\'', out: ['a', 'b', 'c', 'd', 'e']},
        ];
        _.each(valids, (valid) => {
          const res = parseEntityIdParam(valid.in);
          assert.deepStrictEqual(res, valid.out);
          assert.doesNotThrow(() => validateEntityIdParam(valid.in));
        });
      });

      it('any failures fail with the same error as the parser', () => {
        const invalids = [
          '',
          0,
          {},
          [],
          'a,a',
          '\'',
          'a\'',
          'a\'b',
          'a\',b',
          'a\'b,',
          'has_a_#',
          'has_a_?',
          'has_a_&',
          'has_a_/',
          'has space',
        ];
        _.each(invalids, (invalid) => {
          const parseErr = assertThrows(() => parseEntityIdParam(invalid));
          const validateErr = assertThrows(() => validateEntityIdParam(invalid));
          assert.deepStrictEqual(parseErr, validateErr);
        });
      });

    });

    describe('serializeEntityIdList', () => {

      it('can be nil or empty', () => {
        const valids = [
          null,
          undefined,
          [],
        ];
        _.each(valids, (valid) => {
          const res = serializeEntityIdList(valid);
          assert.strictEqual(res, null);
        });
      });

      it('some other valids', () => {
        const valids = [
          {in: ['singleItem'], out: 'singleItem'},
          {in: ['a', 'b', 'c', 'd', 'e'], out: 'a,b,c,d,e'},
          {in: ['comma,containing', 'stuff,get,wrapped'], out: '\'comma,containing\',\'stuff,get,wrapped\''},
        ];
        _.each(valids, (valid) => {
          const res = serializeEntityIdList(valid.in);
          assert.strictEqual(res, valid.out);
        });
      });

      it('but it must be an array', () => {
        const invalids = [
          '',
          0,
          {},
          'abers',
        ];
        _.each(invalids, (invalid) => {
          assertThrows(() => serializeEntityIdList(invalid), errors.INVALID_ENTITY_ID_LIST());
        });
      });

      it('cannot contain dupes', () => {
        const invalids = [
          ['a', 'a'],
        ];
        _.each(invalids, (invalid) => {
          assertThrows(() => serializeEntityIdList(invalid), errors.INVALID_ENTITY_ID_DUPLICATES());
        });
      });

      it('the items must, of course, be a validateable entityId', () => {
        const invalids = [
          ['has_a_#'],
          ['has_a_?'],
          ['has_a_&'],
          ['has_a_/'],
          ['has space'],
        ];
        _.each(invalids, (invalid) => {
          assertThrows(() => serializeEntityIdList(invalid), errors.INVALID_ENTITY_ID_PARAM_ITEM());
        });
      });

    });

    describe('validateEntityIdList', () => {

      it('if it can be serialized, then the list validates ok', () => {
        const valids = [
          {in: null, out: null},
          {in: undefined, out: null},
          {in: [], out: null},
          {in: ['singleItem'], out: 'singleItem'},
          {in: ['a', 'b', 'c', 'd', 'e'], out: 'a,b,c,d,e'},
          {in: ['comma,containing', 'stuff,get,wrapped'], out: '\'comma,containing\',\'stuff,get,wrapped\''},
        ];
        _.each(valids, (valid) => {
          const res = serializeEntityIdList(valid.in);
          assert.strictEqual(res, valid.out);
          assert.doesNotThrow(() => validateEntityIdList(valid.in));
        });
      });

      it('any failures fail with the same error as the serializer', () => {
        const invalids = [
          '',
          0,
          {},
          'abers',
          ['a', 'a'],
          ['has_a_#'],
          ['has_a_?'],
          ['has_a_&'],
          ['has_a_/'],
          ['has space'],
        ];
        _.each(invalids, (invalid) => {
          const serializeErr = assertThrows(() => serializeEntityIdList(invalid));
          const validateErr = assertThrows(() => validateEntityIdList(invalid));
          assert.deepStrictEqual(serializeErr, validateErr);
        });
      });

    });

  });

  describe('entityType parse & serialize', () => {

    describe('parseEntityTypeParam', () => {

      it('an empty param is ok (returns empty list)', () => {
        const empties = [
          undefined,
          null,
        ];
        _.each(empties, (empty) => {
          const res = parseEntityTypeParam(empty);
          assert.deepStrictEqual(res, []);
        });
      });

      it('some other valid entityType params', () => {
        const valids = [
          {in: 'anyValidFieldName', out: ['anyValidFieldName']},
          {in: '\'canBeWrapped\'', out: ['canBeWrapped']},
          {in: '\'can,contain\',\'some,,,,commas\'', out: ['can,contain', 'some,,,,commas']},
          {in: 'a,b,c,d,\'e\'', out: ['a', 'b', 'c', 'd', 'e']},
        ];
        _.each(valids, (valid) => {
          const res = parseEntityTypeParam(valid.in);
          assert.deepStrictEqual(res, valid.out);
        });
      });

      it('only accepts non-empty strings', () => {
        const invalids = [
          '',
          0,
          {},
          [],
        ];
        _.each(invalids, (invalid) => {
          assertThrows(() => parseEntityTypeParam(invalid), errors.EMPTY_ENTITY_TYPE_PARAM());
        });
      });

      it('cannot contain duplicates', () => {
        const invalids = [
          'a,a',
        ];
        _.each(invalids, (invalid) => {
          assertThrows(() => parseEntityTypeParam(invalid), errors.INVALID_ENTITY_TYPE_DUPLICATES());
        });
      });

      it('an entity id CANNOT contain the single quote (\')', () => {
        const invalid = '\'';
        assertThrows(() => parseEntityTypeParam(invalid), errors.INVALID_ENTITY_TYPE_PARAM_FORMATTING());
      });

      it('If parsing the list into ids fails due to some missformatting of separators and groupers', () => {
        const invalids = [
          '\'',
          'a\'',
          'a\'b',
          'a\',b',
          'a\'b,',
        ];
        _.each(invalids, (invalid) => {
          assertThrows(() => parseEntityTypeParam(invalid, errors.INVALID_ENTITY_TYPE_PARAM_FORMATTING()));
        });
      });

      it('all items must be valid entityType (no invalid chars)', () => {
        const invalids = [
          'has_a_#',
          'has_a_?',
          'has_a_&',
          'has_a_/',
          'has space',
        ];
        _.each(invalids, (invalid) => {
          assertThrows(() => parseEntityTypeParam(invalid), errors.INVALID_ENTITY_TYPE_PARAM_ITEM());
        });
      });

    });

    describe('validateEntityTypeParam', () => {

      it('if it is parseable, then it is validateable', () => {
        const valids = [
          {in: null, out: []},
          {in: undefined, out: []},
          {in: 'anyValidFieldName', out: ['anyValidFieldName']},
          {in: '\'canBeWrapped\'', out: ['canBeWrapped']},
          {in: '\'can,contain\',\'some,,,,commas\'', out: ['can,contain', 'some,,,,commas']},
          {in: 'a,b,c,d,\'e\'', out: ['a', 'b', 'c', 'd', 'e']},
        ];
        _.each(valids, (valid) => {
          const res = parseEntityTypeParam(valid.in);
          assert.deepStrictEqual(res, valid.out);
          assert.doesNotThrow(() => validateEntityTypeParam(valid.in));
        });
      });

      it('any failures fail with the same error as the parser', () => {
        const invalids = [
          '',
          0,
          {},
          [],
          'a,a',
          '\'',
          'a\'',
          'a\'b',
          'a\',b',
          'a\'b,',
          'has_a_#',
          'has_a_?',
          'has_a_&',
          'has_a_/',
          'has space',
        ];
        _.each(invalids, (invalid) => {
          const parseErr = assertThrows(() => parseEntityTypeParam(invalid));
          const validateErr = assertThrows(() => validateEntityTypeParam(invalid));
          assert.deepStrictEqual(parseErr, validateErr);
        });
      });

    });

    describe('serializeEntityTypeList', () => {

      it('can be nil or empty', () => {
        const valids = [
          null,
          undefined,
          [],
        ];
        _.each(valids, (valid) => {
          const res = serializeEntityTypeList(valid);
          assert.strictEqual(res, null);
        });
      });

      it('some other valids', () => {
        const valids = [
          {in: ['singleItem'], out: 'singleItem'},
          {in: ['a', 'b', 'c', 'd', 'e'], out: 'a,b,c,d,e'},
          {in: ['comma,containing', 'stuff,get,wrapped'], out: '\'comma,containing\',\'stuff,get,wrapped\''},
        ];
        _.each(valids, (valid) => {
          const res = serializeEntityTypeList(valid.in);
          assert.strictEqual(res, valid.out);
        });
      });

      it('but it must be an array', () => {
        const invalids = [
          '',
          0,
          {},
          'abers',
        ];
        _.each(invalids, (invalid) => {
          assertThrows(() => serializeEntityTypeList(invalid), errors.INVALID_ENTITY_TYPE_LIST());
        });
      });

      it('cannot contain dupes', () => {
        const invalids = [
          ['a', 'a'],
        ];
        _.each(invalids, (invalid) => {
          assertThrows(() => serializeEntityTypeList(invalid), errors.INVALID_ENTITY_TYPE_DUPLICATES());
        });
      });

      it('the items must, of course, be a validateable entityType', () => {
        const invalids = [
          ['has_a_#'],
          ['has_a_?'],
          ['has_a_&'],
          ['has_a_/'],
          ['has space'],
        ];
        _.each(invalids, (invalid) => {
          assertThrows(() => serializeEntityTypeList(invalid), errors.INVALID_ENTITY_TYPE_PARAM_ITEM());
        });
      });

    });

    describe('validateEntityTypeList', () => {

      it('if it can be serialized, then the list validates ok', () => {
        const valids = [
          {in: null, out: null},
          {in: undefined, out: null},
          {in: [], out: null},
          {in: ['singleItem'], out: 'singleItem'},
          {in: ['a', 'b', 'c', 'd', 'e'], out: 'a,b,c,d,e'},
          {in: ['comma,containing', 'stuff,get,wrapped'], out: '\'comma,containing\',\'stuff,get,wrapped\''},
        ];
        _.each(valids, (valid) => {
          const res = serializeEntityIdList(valid.in);
          assert.strictEqual(res, valid.out);
          assert.doesNotThrow(() => validateEntityTypeList(valid.in));
        });
      });

      it('any failures fail with the same error as the serializer', () => {
        const invalids = [
          '',
          0,
          {},
          'abers',
          ['a', 'a'],
          ['has_a_#'],
          ['has_a_?'],
          ['has_a_&'],
          ['has_a_/'],
          ['has space'],
        ];
        _.each(invalids, (invalid) => {
          const serializeErr = assertThrows(() => serializeEntityTypeList(invalid));
          const validateErr = assertThrows(() => validateEntityTypeList(invalid));
          assert.deepStrictEqual(serializeErr, validateErr);
        });
      });

    });


  });

  describe('validatePageLimitParam', () => {

    it('null & undefined are ok', () => {
      const nils = [
        null,
        undefined,
      ];
      _.each(nils, (nil) => {
        assert.doesNotThrow(() => validatePageLimitParam(nil));
      });
    });

    it('cannot be empty', () => {
      const empties = [
        '',
        {},
        [],
      ];
      _.each(empties, (empty) => {
        assertThrows(() => validatePageLimitParam(empty), errors.EMPTY_PAGE_LIMIT_PARAM());
      });
    });

    it('must be a non-zero positive integer', () => {
      const valids = [
        1,
        234,
        '12',
        '345',
      ];
      _.each(valids, (valid) => {
        assert.doesNotThrow(() => validatePageLimitParam(valid));
      });
    });

    it('some invalids that are wrong-looking numbers, effectively', () => {
      const invalids = [
        0,
        -1,
        1.2,
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => validatePageLimitParam(invalid), errors.INVALID_PAGE_LIMIT_PARAM());
      });
    });

  });

  describe('validatePageOffsetParam', () => {

    it('null & undefined are ok', () => {
      const nils = [
        null,
        undefined,
      ];
      _.each(nils, (nil) => {
        assert.doesNotThrow(() => validatePageOffsetParam(nil));
      });
    });

    it('cannot be empty', () => {
      const empties = [
        '',
        {},
        [],
      ];
      _.each(empties, (empty) => {
        assertThrows(() => validatePageOffsetParam(empty), errors.EMPTY_PAGE_OFFSET_PARAM());
      });
    });

    it('must be a non-zero positive integer', () => {
      const valids = [
        0,
        1,
        234,
        '12',
        '345',
      ];
      _.each(valids, (valid) => {
        assert.doesNotThrow(() => validatePageOffsetParam(valid));
      });
    });

    it('some invalids that are wrong-looking numbers, effectively', () => {
      const invalids = [
        -1,
        1.2,
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => validatePageOffsetParam(invalid), errors.INVALID_PAGE_OFFSET_PARAM());
      });
    });

  });

  describe('validatePageItemIdParam', () => {

    it('some valids', () => {
      const valids = [
        'asdf',
      ];
      _.each(valids, (valid) => {
        assert.doesNotThrow(() => validatePageItemIdParam(valid));
      });
    });

    it('some invalids', () => {
      const invalids = [
        undefined,
        null,
        {},
        23,
      ];
      const mess = /The paging item id has to be a non-empty string/;
      _.each(invalids, (invalid) => {
        assert.throws(() => validatePageItemIdParam(invalid), mess);
      });
    });

  });

  describe('validatePageDirectionParam', () => {

    it('some valids', () => {
      const valids = [
        'next',
        'prev',
      ];
      _.each(valids, (valid) => {
        assert.doesNotThrow(() => validatePageDirectionParam(valid));
      });
    });

    it('some invalids', () => {
      const invalids = [
        undefined,
        null,
        {},
        'anything_but_next_or_prev',
      ];
      const mess = /The paging direction must be either "prev" or "next"/;
      _.each(invalids, (invalid) => {
        assert.throws(() => validatePageDirectionParam(invalid), mess);
      });
    });

  });

});
