/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const {assertThrows} = require('../../src');

const {
  errors,
  parseAttrsParam,
  serializeAttrsList,
  validateAttrsParam,
  validateAttrsList,
} = require('../../src/utils/ngsi-query-params-utils/attrs-param-parser');

describe('attrs-param-parser', () => {

  describe('errors', () => {

    it('there are 5 NgsiAttrsParamErrors', () => {
      assert.strictEqual(_.size(errors), 5);
    });

    it('the errors have some "mathches" functionality and identifier flag', () => {
      const nonmatchingErr = new Error('this one does not match nuthin');
      _.each(errors, (error) => {
        const basicErr = error();
        assert(basicErr.isNgsiAttrsParamError);
        assert(basicErr.name);
        assert(error.matches(basicErr));
        assert(error.matches(basicErr.message));
        assert(!error.matches(nonmatchingErr));
        assert(!error.matches(nonmatchingErr.message));
      });
    });

  });

  describe('parseAttrsParam', () => {

    it('an empty param is ok (returns empty list)', () => {
      const empties = [
        undefined,
        null,
      ];
      _.each(empties, (empty) => {
        const res = parseAttrsParam(empty);
        assert.deepStrictEqual(res, []);
      });
    });

    it('some other valid attrs params', () => {
      const valids = [
        {in: 'anyValidFieldName', out: ['anyValidFieldName']},
        {in: '\'canBeWrapped\'', out: ['canBeWrapped']},
        {in: '\'can,contain\',\'some,,,,commas\'', out: ['can,contain', 'some,,,,commas']},
        {in: 'a,b,c,d,\'e\'', out: ['a', 'b', 'c', 'd', 'e']},
        // the builtins
        {in: 'dateCreated', out: ['dateCreated']},
        {in: 'dateModified', out: ['dateModified']},
        {in: 'dateExpires', out: ['dateExpires']},
        // and the wildcard
        {in: '*', out: ['*']},
      ];
      _.each(valids, (valid) => {
        const res = parseAttrsParam(valid.in);
        assert.deepStrictEqual(res, valid.out);
      });
    });

    it('only accepts non-empty strings', () => {
      const invalids = [
        '',
        0,
        {},
        [],
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => parseAttrsParam(invalid), errors.EMPTY_ATTRS_PARAM());
      });
    });

    it('cannot contain duplicates', () => {
      const invalids = [
        'a,a',
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => parseAttrsParam(invalid), errors.INVALID_ATTRS_DUPLICATES());
      });
    });

    it('an entity id CANNOT contain the single quote (\')', () => {
      const invalid = '\'';
      assertThrows(() => parseAttrsParam(invalid), errors.INVALID_ATTRS_FORMATTING());
    });

    it('If parsing the list into attrs fails due to some missformatting of separators and groupers', () => {
      const invalids = [
        '\'',
        'a\'',
        'a\'b',
        'a\',b',
        'a\'b,',
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => parseAttrsParam(invalid, errors.INVALID_ATTRS_FORMATTING()));
      });
    });

    it('all items must have a valid/allowed attributeName (no invalid chars)', () => {
      const invalids = [
        'has_a_#',
        'has_a_?',
        'has_a_&',
        'has_a_/',
        'has space',
        'id',
        'type',
        'geo:distance',
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => parseAttrsParam(invalid), errors.INVALID_ATTRS_LIST_ITEM());
      });
    });

  });

  describe('validateAttrsParam', () => {

    it('if it is parseable, then it is validateable', () => {
      const valids = [
        {in: null, out: []},
        {in: undefined, out: []},
        {in: 'anyValidFieldName', out: ['anyValidFieldName']},
        {in: '\'canBeWrapped\'', out: ['canBeWrapped']},
        {in: '\'can,contain\',\'some,,,,commas\'', out: ['can,contain', 'some,,,,commas']},
        {in: 'a,b,c,d,\'e\'', out: ['a', 'b', 'c', 'd', 'e']},
        // the builtins
        {in: 'dateCreated', out: ['dateCreated']},
        {in: 'dateModified', out: ['dateModified']},
        {in: 'dateExpires', out: ['dateExpires']},
        // and the wildcard
        {in: '*', out: ['*']},
      ];
      _.each(valids, (valid) => {
        const res = parseAttrsParam(valid.in);
        assert.deepStrictEqual(res, valid.out);
        assert.doesNotThrow(() => validateAttrsParam(valid.in));
      });
    });

    it('any failures fail with the same error as the parser', () => {
      const invalids = [
        '',
        0,
        {},
        [],
        'a,a',
        '\'',
        'a\'',
        'a\'b',
        'a\',b',
        'a\'b,',
        'has_a_#',
        'has_a_?',
        'has_a_&',
        'has_a_/',
        'has space',
        'id',
        'type',
        'geo:distance',
      ];
      _.each(invalids, (invalid) => {
        const parseErr = assertThrows(() => parseAttrsParam(invalid));
        const validateErr = assertThrows(() => validateAttrsParam(invalid));
        assert.deepStrictEqual(parseErr, validateErr);
      });
    });

  });

  describe('serializeAttrsList', () => {

    it('can be nil or empty', () => {
      const valids = [
        null,
        undefined,
        [],
      ];
      _.each(valids, (valid) => {
        const res = serializeAttrsList(valid);
        assert.strictEqual(res, null);
      });
    });

    it('some other valids', () => {
      const valids = [
        {in: ['singleItem'], out: 'singleItem'},
        {in: ['a', 'b', 'c', 'd', 'e'], out: 'a,b,c,d,e'},
        {in: ['comma,containing', 'stuff,get,wrapped'], out: '\'comma,containing\',\'stuff,get,wrapped\''},
        // wildcard IS allowed
        {in: ['*'], out: '*'},
        // as are builtin attributes
        {in: ['dateCreated'], out: 'dateCreated'},
        {in: ['dateModified'], out: 'dateModified'},
        {in: ['dateExpires'], out: 'dateExpires'},
      ];
      _.each(valids, (valid) => {
        const res = serializeAttrsList(valid.in);
        assert.strictEqual(res, valid.out);
      });
    });

    it('but it must be an array', () => {
      const invalids = [
        '',
        0,
        {},
        'abers',
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => serializeAttrsList(invalid), errors.INVALID_ATTRS_LIST());
      });
    });

    it('cannot contain dupes', () => {
      const invalids = [
        ['a', 'a'],
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => serializeAttrsList(invalid), errors.INVALID_ATTRS_DUPLICATES());
      });
    });

    it('the items must, of course, be a validateable attrs', () => {
      const invalids = [
        ['has_a_#'],
        ['has_a_?'],
        ['has_a_&'],
        ['has_a_/'],
        ['has space'],
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => serializeAttrsList(invalid), errors.INVALID_ATTRS_LIST_ITEM());
      });
    });

  });

  describe('validateAttrsList', () => {

    it('if it can be serialized, then the list validates ok', () => {
      const valids = [
        {in: null, out: null},
        {in: undefined, out: null},
        {in: ['singleItem'], out: 'singleItem'},
        {in: ['a', 'b', 'c', 'd', 'e'], out: 'a,b,c,d,e'},
        {in: ['comma,containing', 'stuff,get,wrapped'], out: '\'comma,containing\',\'stuff,get,wrapped\''},
        // wildcard IS allowed
        {in: ['*'], out: '*'},
        // as are builtin attributes
        {in: ['dateCreated'], out: 'dateCreated'},
        {in: ['dateModified'], out: 'dateModified'},
        {in: ['dateExpires'], out: 'dateExpires'},
      ];
      _.each(valids, (valid) => {
        const res = serializeAttrsList(valid.in);
        assert.strictEqual(res, valid.out);
        assert.doesNotThrow(() => validateAttrsList(valid.in));
      });
    });

    it('any failures fail with the same error as the serializer', () => {
      const invalids = [
        '',
        0,
        {},
        'abers',
        ['a', 'a'],
        ['has_a_#'],
        ['has_a_?'],
        ['has_a_&'],
        ['has_a_/'],
        ['has space'],
      ];
      _.each(invalids, (invalid) => {
        const serializeErr = assertThrows(() => serializeAttrsList(invalid));
        const validateErr = assertThrows(() => validateAttrsList(invalid));
        assert.deepStrictEqual(serializeErr, validateErr);
      });
    });

  });

});
