/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const {
  assertThrows,
  ngsiDateTimeUtils: {fromDate},
} = require('../../src');

const {
  errors,
  formatRegistrationPayload,
  validateRegistrationPayload,
} = require('../../src/utils/ngsi-query-params-utils/registration-payload-parser');

const validRegistration = {
  id: 'getsIgnored',
  description: 'must be a string',
  dataProvided: {
    entities: [
      {
        id: 'boo',
        type: 'per',
      },
    ],
    attrs: [
      'relativeHumidity',
    ],
  },
  provider: {
    legacyForwarding: true,
    supportedForwardingMode: 'all',
    http: {
      url: 'http://localhost:1234',
    },
  },
};

describe('registration-payload-parser', () => {

  describe('errors', () => {

    it('there are 17 isNgsiRegistrationPayloadParserError errors', () => {
      assert.strictEqual(_.size(errors), 17);
    });

    it('the errors have some "matches" functionality and identifier flag', () => {
      const nonmatchingErr = new Error('this one does not match nuthin');
      _.each(errors, (error) => {
        const basicErr = error();
        assert(basicErr.isNgsiRegistrationPayloadParserError, 'error type does not match');
        assert(basicErr.name);
        assert(error.matches(basicErr));
        assert(error.matches(basicErr.message));
        assert(!error.matches(nonmatchingErr));
        assert(!error.matches(nonmatchingErr.message));
      });
    });

  });

  describe('formatRegistrationPayload', () => {

    it('validRegistration object gets formatted as expected', () => {
      const res = formatRegistrationPayload(validRegistration);
      const expected = _.omit(validRegistration, 'id');
      assert.deepStrictEqual(res, expected); // should be unchanged
    });

    it('payload must be an object', () => {
      const invalids = [
        null,
        undefined,
        0,
        'derp',
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => formatRegistrationPayload(invalid), errors.INVALID_REGISTRATION_PAYLOAD_ARG());
      });
    });

    it('invalid payload props not allowed', () => {
      const invalids = [
        {invalidProp: 'asdfasdf'},
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => formatRegistrationPayload(invalid), errors.EXTRANEOUS_REGISTRATION_PAYLOAD_PROPS());
      });
    });

    it('description can be undefined or a string', () => {
      const descRemoved = _.omit(validRegistration, 'description');
      assert.doesNotThrow(() => formatRegistrationPayload(descRemoved));
      const validDescriptions = [
        undefined,
        'any other string (?)',
      ];
      _.each(validDescriptions, (description) => {
        const valid = _.assign({}, validRegistration, {description});
        assert.doesNotThrow(() => formatRegistrationPayload(valid));
      });
      const invalidDescriptions = [
        null,
        23,
        {asdf: 'asdf'},
      ];
      _.each(invalidDescriptions, (description) => {
        const invalid = _.assign({}, validRegistration, {description});
        assertThrows(() => formatRegistrationPayload(invalid), errors.INVALID_REGISTRATION_DESCRIPTION());
      });
    });

    it('status must one of the allowed values', () => {
      const invalidStatuses = [
        'invalid',
        23,
      ];
      _.each(invalidStatuses, (status) => {
        const invalid = _.assign({}, validRegistration, {status});
        assertThrows(() => formatRegistrationPayload(invalid), errors.INVALID_REGISTRATION_STATUS());
      });
      const validStatutes = [
        undefined,
        'active',
        'inactive',
        'failed',
        'expired',
      ];
      _.each(validStatutes, (status) => {
        const valid = _.assign({}, validRegistration, {status});
        assert.doesNotThrow(() => formatRegistrationPayload(valid));
      });
    });

    it('expires must be a valid DateTime', () => {
      const invalidExpires = [
        'asdf',
        123412234,
      ];
      _.each(invalidExpires, (expires) => {
        const invalid = _.assign({}, validRegistration, {expires});
        assertThrows(() => formatRegistrationPayload(invalid), errors.INVALID_REGISTRATION_EXPIRATION());
      });
      const validExpires = [
        '2019-11-06T13:38:55.747Z',
        fromDate(new Date()),
      ];
      _.each(validExpires, (expires) => {
        const valid = _.assign({}, validRegistration, {expires});
        assert.doesNotThrow(() => formatRegistrationPayload(valid));
      });
    });

    it('some other invalid registrations (highlight some errors)', () => {
      const invalidRegistrations = [
        {
          registration: {provider: null},
          err: errors.INVALID_REGISTRATION_PROVIDER_CONTAINER(),
        },
        {
          registration: {provider: {invalidKey: true}},
          err: errors.EXTRANEOUS_REGISTRATION_PROVIDER_PROPS(),
        },
        {
          registration: {provider: {http: null}},
          err: errors.INVALID_PROVIDER_HTTP_CONTAINER(),
        },
        {
          registration: {provider: {http: {invalidKey: true}}},
          err: errors.INVALID_PROVIDER_HTTP_PROPS(),
        },
        {
          registration: {provider: {http: {url: 234}}},
          err: errors.INVALID_REGISTRATION_HTTP_URL(),
        },
        {
          registration: {provider: {http: {url: 'validUrl'}, supportedForwardingMode: 234}},
          err: errors.INVALID_REGISTRATION_FORWARDING_MODE(),
        },
        {
          registration: {provider: {http: {url: 'validUrl'}, legacyForwarding: 234}},
          err: errors.INVALID_PROVIDER_LEGACY_FORWARDING(),
        },
        // dataProvided
        {
          registration: {dataProvided: null},
          err: errors.INVALID_REGISTRATION_DATA_PROVIDED_CONTAINER(),
        },
        {
          registration: {dataProvided: {invaidKey: true}},
          err: errors.EXTRANEOUS_REGISTRATION_DATA_PROVIDED_PROPS(),
        },
        {
          registration: {dataProvided: {invaidKey: true}},
          err: errors.EXTRANEOUS_REGISTRATION_DATA_PROVIDED_PROPS(),
        },
        {
          registration: {dataProvided: {entities: 'notAnArray'}},
          err: errors.INVALID_DATA_PROVIDED_ENTITIES(),
        },
        {
          registration: {dataProvided: {entities: ['invalidEntity']}},
          err: errors.INVALID_DATA_PROVIDED_ENTITY(),
        },
        {
          registration: {dataProvided: {entities: [{id: 'valid'}], attrs: 234}},
          err: errors.INVALID_DATA_PROVIDED_ATTRS(),
        },
        {
          registration: {dataProvided: {entities: [{id: 'valid'}], expression: 234}},
          err: 'REGISTRATION dataProvided "expression" not yet implemented',
        },
      ];
      _.each(invalidRegistrations, ({registration, err}) => {
        const invalid = {...validRegistration, ...registration};
        assertThrows(() => formatRegistrationPayload(invalid), err);
      });
    });

  });

  describe('validateRegistrationPayload', () => {

    it('valid registrations also validate', () => {
      assert.doesNotThrow(() => validateRegistrationPayload(validRegistration));
      assert.doesNotThrow(() => formatRegistrationPayload(validRegistration));
    });

  });

});
