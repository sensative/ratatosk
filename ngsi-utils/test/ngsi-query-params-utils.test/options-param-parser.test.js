/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const {assertThrows} = require('../../src');

const {
  errors,
  parseOptionsParamOrionStyle,
  parseOptionsParam,
  serializeOptionsParam,
  combineMultipleOptionsArgs,
} = require('../../src/utils/ngsi-query-params-utils/options-param-parser');

describe('options-param-parser.test', () => {

  describe('errors (do this first, as they will be used in all other tests)', () => {

    it('there are 6 errors', () => {
      assert.strictEqual(_.size(errors), 6);
    });

    it('all errors isNgsiOptionsParamError and matches itself but not others', () => {
      const nonMatchingErr = new Error('this error does not match any options param error');
      _.each(errors, (createError) => {
        const optionsErr = createError();
        assert(optionsErr.isNgsiOptionsParamError);
        assert(createError.matches(optionsErr));
        assert(createError.matches(optionsErr.message));
        assert(!createError.matches(nonMatchingErr));
        assert(!createError.matches(nonMatchingErr.message));
      });
    });

  });

  describe('parseOptionsParamOrionStyle', () => {

    it('some values that result in a default options', () => {
      const defaultValids = [
        null,
        undefined,
      ];
      _.each(defaultValids, (valid) => {
        const parsed = parseOptionsParamOrionStyle(valid);
        const expected = {append: false, count: false, representationType: null, upsert: false};
        assert.deepStrictEqual(parsed, expected);
      });
    });

    it('some values that have invalid shape', () => {
      const invalids = [
        0,
        [],
        {},
        '',
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => parseOptionsParamOrionStyle(invalid), errors.INVALID_PARAM_SHAPE());
      });
    });

    it('one or more preceding commas are fine', () => {
      const valids = [
        ',',
        ',,,,,,,',
        ',count',
        ',,,,,,,,,,,count',
        ',,,append',
      ];
      _.each(valids, (valid) => {
        assert.doesNotThrow(() => parseOptionsParamOrionStyle(valid));
      });
    });

    it('empty options param values are not allowed', () => {
      const invalids = [
        'count,,',
        'count,,values',
        ',,,,,count,values,',
        'append,,',
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => parseOptionsParamOrionStyle(invalid, errors.EMPTY_PARAM_ITEM()));
      });
    });

    it('duplicates ARE allowed', () => {
      const valids = [
        'count,count',
        ',,,,,values,count,values',
        ',,,,append,append',
      ];
      _.each(valids, (valid) => {
        assert.doesNotThrow(() => parseOptionsParamOrionStyle(valid));
      });
    });

    it('invalid options string values are not allowed', () => {
      const invalids = [
        'derp',
        'count,derp',
        ',,,,derp',
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => parseOptionsParamOrionStyle(invalid), errors.INVALID_PARAM_ITEM());
      });
    });

    it('multiple different values of representationTypes are not allowed', () => {
      const invalids = [
        'values,keyValues',
        'unique,values',
        'keyValues,unique',
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => parseOptionsParamOrionStyle(invalid), errors.MULTIPLE_REPRESENTATION_TYPE_ITEMS());
      });
    });

    it('some valid varieties', () => {
      const corrects = [
        // the basic set
        ['append', {append: true, count: false, representationType: null, upsert: false}],
        ['count', {append: false, count: true, representationType: null, upsert: false}],
        ['values', {append: false, count: false, representationType: 'values', upsert: false}],
        ['upsert', {append: false, count: false, representationType: null, upsert: true}],
        ['keyValues', {append: false, count: false, representationType: 'keyValues', upsert: false}],
        ['unique', {append: false, count: false, representationType: 'unique', upsert: false}],
        ['count,unique', {append: false, count: true, representationType: 'unique', upsert: false}],
        ['unique,count', {append: false, count: true, representationType: 'unique', upsert: false}],
        ['count,values', {append: false, count: true, representationType: 'values', upsert: false}],
        ['values,count', {append: false, count: true, representationType: 'values', upsert: false}],
        ['count,keyValues', {append: false, count: true, representationType: 'keyValues', upsert: false}],
        ['keyValues,count', {append: false, count: true, representationType: 'keyValues', upsert: false}],
        ['append,keyValues,count,upsert', {append: true, count: true, representationType: 'keyValues', upsert: true}],
        // some also corrects
        [',,,,,,,,,,,,,,,unique,count', {append: false, count: true, representationType: 'unique', upsert: false}],
        [',,,,,,,,,,', {append: false, count: false, representationType: null, upsert: false}],
        ['count,count,count,count,count,count,count,count', {append: false, count: true, representationType: null, upsert: false}],
        [',,,,,,,,,,,,,,count,count,unique,unique,unique,count,count,count,count,unique,unique,count,count', {append: false, count: true, representationType: 'unique', upsert: false}],
      ];
      _.each(corrects, (correct) => {
        const res = parseOptionsParamOrionStyle(correct[0]);
        assert.deepStrictEqual(correct[1], res, 'parsed looks wrong');
      });
    });

  });

  describe('parseOptionsParam', () => {

    it('some values that result in a default options', () => {
      const defaultValids = [
        null,
        undefined,
      ];
      _.each(defaultValids, (valid) => {
        const parsed = parseOptionsParam(valid);
        const expected = {append: false, count: false, representationType: null, upsert: false};
        assert.deepStrictEqual(parsed, expected);
      });
    });

    it('some values that have invalid shape', () => {
      const invalids = [
        0,
        [],
        {},
        '',
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => parseOptionsParam(invalid), errors.INVALID_PARAM_SHAPE());
      });
    });

    it('empty options param values are not allowed', () => {
      const invalids = [
        ',', // simple comma definitely NOT allowed
        ',,,count', // and preceding commas NOT allowed at all
        'count,,',
        'count,,values',
        ',,,,,count,values,',
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => parseOptionsParam(invalid, errors.EMPTY_PARAM_ITEM()));
      });
    });

    it('duplicates are NOT allowed', () => {
      const invalids = [
        'count,count',
        'values,count,values',
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => parseOptionsParam(invalid), errors.DUPLICATE_PARAM_ITEM());
      });
    });

    it('invalid options string values are not allowed', () => {
      const invalids = [
        'derp',
        'count,derp',
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => parseOptionsParam(invalid), errors.INVALID_PARAM_ITEM());
      });
    });

    it('multiple different values of representationTypes are not allowed', () => {
      const invalids = [
        'values,keyValues',
        'unique,values',
        'keyValues,unique',
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => parseOptionsParam(invalid), errors.MULTIPLE_REPRESENTATION_TYPE_ITEMS());
      });
    });

    it('and most of the valid varieties (for now at least..)', () => {
      const valids = [
        // the basic set
        ['append', {append: true, count: false, representationType: null, upsert: false}],
        ['count', {append: false, count: true, representationType: null, upsert: false}],
        ['values', {append: false, count: false, representationType: 'values', upsert: false}],
        ['upsert', {append: false, count: false, representationType: null, upsert: true}],
        ['keyValues', {append: false, count: false, representationType: 'keyValues', upsert: false}],
        ['unique', {append: false, count: false, representationType: 'unique', upsert: false}],
        ['count,unique', {append: false, count: true, representationType: 'unique', upsert: false}],
        ['unique,count', {append: false, count: true, representationType: 'unique', upsert: false}],
        ['count,values', {append: false, count: true, representationType: 'values', upsert: false}],
        ['values,count', {append: false, count: true, representationType: 'values', upsert: false}],
        ['count,keyValues', {append: false, count: true, representationType: 'keyValues', upsert: false}],
        ['keyValues,count', {append: false, count: true, representationType: 'keyValues', upsert: false}],
        ['append,keyValues,count,upsert', {append: true, count: true, representationType: 'keyValues', upsert: true}],
        ['upsert,append,count,unique', {append: true, count: true, representationType: 'unique', upsert: true}],
      ];
      _.each(valids, (valid) => {
        const res = parseOptionsParam(valid[0]);
        assert.deepStrictEqual(valid[1], res, 'parsed looks wrong');
      });
    });

  });

  describe('serializeOptionsParam', () => {

    it('some values that return null', () => {
      const nullyValids = [
        null,
        undefined,
        'whichever string you want',
        'count',
        {johnny: 2, therdy: 'werdy'}, // additional keys are ignored
        {count: false},
        {count: false, representationType: null},
        {count: 0, representationType: null},
        {count: 0, representationType: undefined},
        {count: undefined, representationType: undefined},
      ];
      _.each(nullyValids, (valid) => {
        const res = serializeOptionsParam(valid);
        assert.strictEqual(res, null);
      });
    });

    it('if included (and not null or undefined), then representationType must be valid', () => {
      const invalidRepTypes = [
        {representationType: 0},
        {representationType: true},
        {representationType: false},
        {representationType: 'derp'},
      ];
      _.each(invalidRepTypes, (invalid) => {
        assertThrows(() => serializeOptionsParam(invalid), errors.INVALID_REPRESENTATION_TYPE());
      });
    });

    it('the non-empty ones', () => {
      const valids = [
        [{count: false, representationType: 'values'}, 'values'],
        [{count: false, representationType: 'keyValues'}, 'keyValues'],
        [{count: false, representationType: 'unique'}, 'unique'],
        [{count: true, representationType: 'values'}, 'count,values'],
        [{count: true, representationType: 'keyValues'}, 'count,keyValues'],
        [{count: true, representationType: 'unique'}, 'count,unique'],
        [{count: true}, 'count'],
        // and count just needs to be truthy or falsey
        [{count: 1.34}, 'count'],
        [{count: {}}, 'count'],
        [{count: []}, 'count'],
        [{count: 'spoon'}, 'count'],
        [{count: 0, representationType: 'values'}, 'values'],
        [{count: null, representationType: 'values'}, 'values'],
        [{count: undefined, representationType: 'values'}, 'values'],
        [{count: '', representationType: 'values'}, 'values'],
        // some appends
        [{append: true}, 'append'],
        [{append: true, count: true}, 'append,count'],
        [{append: true, count: true, representationType: 'values'}, 'append,count,values'],
        // append just needs to be truthy or falsey
        [{append: 1.34}, 'append'],
        [{append: {}}, 'append'],
        [{append: []}, 'append'],
        [{append: 'spoon'}, 'append'],
        [{append: 0, representationType: 'values'}, 'values'],
        [{append: null, representationType: 'values'}, 'values'],
        [{append: undefined, representationType: 'values'}, 'values'],
        [{append: '', representationType: 'values'}, 'values'],
      ];
      _.each(valids, (valid) => {
        const res = serializeOptionsParam(valid[0]);
        assert.strictEqual(res, valid[1], 'should be as valid[1]');
      });
    });

  });

  describe('combineMultipleOptionsArgs', () => {

    it('an example combination', () => {
      const optionsArgs = [
        'unique,count', // handles both strings and objects
        {count: false}, // this overwrites first value for count
        {representationType: 'unique'}, // this overwrites first value for representationType
        {append: true}, // sets append to true
      ];
      const res = combineMultipleOptionsArgs(...optionsArgs);
      const expected = 'append,unique';
      assert.strictEqual(res, expected);
    });

    it('another example ', () => {
      const optionsArgs = [
        'values', // handles both strings and objects
        {count: true}, // this overwrites first value for count
        {representationType: 'keyValues'}, // this overwrites first value for representationType
        {append: true}, // sets append to true
      ];
      const res = combineMultipleOptionsArgs(...optionsArgs);
      const expected = 'append,count,keyValues';
      assert.strictEqual(res, expected);
    });

  });

});
