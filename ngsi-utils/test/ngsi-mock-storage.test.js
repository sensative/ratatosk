/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const {createMockStorage} = require('../src/utils/ngsi-mock-storage');

describe('ngsi-mock-storage', () => {

  it('has all 23 api keys', () => {
    const storage = createMockStorage();
    assert.strictEqual(_.size(storage), 23, 'there should be 23 keys');
    assert(_.has(storage, 'explain'), 'explain is missing');
    assert(_.has(storage, 'getEntities'), 'getEntities is missing');
    assert(_.has(storage, 'createEntity'), 'createEntity is missing');
    assert(_.has(storage, 'findEntity'), 'findEntity is missing');
    assert(_.has(storage, 'deleteEntity'), 'deleteEntity is missing');
    assert(_.has(storage, 'getEntityAttrs'), 'getEntityAttrs is missing');
    assert(_.has(storage, 'replaceEntityAttrs'), 'replaceEntityAttrs is missing');
    assert(_.has(storage, 'upsertEntityAttrs'), 'upsertEntityAttrs is missing');
    assert(_.has(storage, 'patchEntityAttrs'), 'patchEntityAttrs is missing');
    assert(_.has(storage, 'findEntityAttr'), 'findEntityAttr is missing');
    assert(_.has(storage, 'updateEntityAttr'), 'updateEntityAttr is missing');
    assert(_.has(storage, 'deleteEntityAttr'), 'deleteEntityAttr is missing');
    assert(_.has(storage, 'findEntityAttrValue'), 'findEntityAttrValue is missing');
    assert(_.has(storage, 'updateEntityAttrValue'), 'updateEntityAttrValue is missing');
    assert(_.has(storage, 'bulkQuery'), 'bulkQuery is missing');
    assert(_.has(storage, 'bulkUpdate'), 'bulkUpdate is missing');
    // subscriptions
    assert(_.has(storage, 'createSubscription'), 'createSubscription is missing');
    assert(_.has(storage, 'getSubscriptions'), 'getSubscriptions is missing');
    assert(_.has(storage, 'findSubscription'), 'findSubscription is missing');
    assert(_.has(storage, 'deleteSubscription'), 'deleteSubscription is missing');
    assert(_.has(storage, 'updateSubscription'), 'updateSubscription is missing');
    // notifications
    assert(_.has(storage, 'generateNotifications'), 'generateNotifications is missing');
    assert(_.has(storage, 'acknowledgeNotification'), 'acknowledgeNotification is missing');
  });

  it('all keys resolve "mock-ok"', async () => {
    const storage = createMockStorage();
    await Promise.all(_.map(storage, async (fn) => {
      const res = await fn();
      assert.strictEqual(res, 'mock-ok', 'invalid mock res');
    }));
  });

});
