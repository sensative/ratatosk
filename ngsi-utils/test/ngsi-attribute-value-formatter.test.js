/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const {assertThrows} = require('../src');
const {
  formatAttributeValue,
  unpackFormattedAttrValue,
} = require('../src/utils/ngsi-attribute-value-formatter');

const JSON_CONTENT = {'Content-Type': 'application/json'};
const TEXT_CONTENT = {'Content-Type': 'text/plain'};

describe('ngsi-attribute-value-formatter', () => {

  describe('undefined should should throw an error', () => {
    it('value: undefined, type: TEXT', () => {
      assert.throws(() => formatAttributeValue(undefined));
    });
  });

  describe('valid values are processed as expected', () => {

    it('value: 32', () => {
      const update = formatAttributeValue(32);
      const expected = {
        headers: TEXT_CONTENT,
        attrValue: '32',
      };
      assert.deepStrictEqual(update, expected, 'INVALID for 32');
    });

    it('value: true', () => {
      const update = formatAttributeValue(true);
      const expected = {
        headers: TEXT_CONTENT,
        attrValue: 'true',
      };
      assert.deepStrictEqual(update, expected, 'INVALID for true');
    });

    it('value: false', () => {
      const update = formatAttributeValue(false);
      const expected = {
        headers: TEXT_CONTENT,
        attrValue: 'false',
      };
      assert.deepStrictEqual(update, expected, 'INVALID for false');
    });

    it('value: null', () => {
      const update = formatAttributeValue(null);
      const expected = {
        headers: TEXT_CONTENT,
        attrValue: 'null',
      };
      assert.deepStrictEqual(update, expected, 'INVALID for null');
    });

    it('value: \'asdf\'', () => {
      const update = formatAttributeValue('asdf');
      const expected = {
        headers: TEXT_CONTENT,
        attrValue: '"asdf"',
      };
      assert.deepStrictEqual(update, expected, 'INVALID for \'asdf\'');
    });

    it('value: {a: 0}', () => {
      const update = formatAttributeValue({a: 0});
      const expected = {
        headers: JSON_CONTENT,
        attrValue: {a: 0},
      };
      assert.deepStrictEqual(update, expected, 'INVALID for {a: 0}');
    });

    it('value: [1, 2, 3, "asdf"]', () => {
      const update = formatAttributeValue([1, 2, 3, 'asdf']);
      const expected = {
        headers: JSON_CONTENT,
        attrValue: [1, 2, 3, 'asdf'],
      };
      assert.deepStrictEqual(update, expected, 'INVALID for [1, 2, 3, "asdf"]');
    });

  });

  describe('unpackFormattedAttrValue', () => {

    it('unpacks null', () => {
      const res = unpackFormattedAttrValue('null');
      assert.strictEqual(res, null);
    });

    it('unpacks true', () => {
      const res = unpackFormattedAttrValue('true');
      assert.strictEqual(res, true);
    });

    it('unpacks false', () => {
      const res = unpackFormattedAttrValue('false');
      assert.strictEqual(res, false);
    });

    it('unpacks a "string"', () => {
      const vals = [
        ['""', ''],
        ['"asdf"', 'asdf'],
      ];
      _.each(vals, ([val, unpacked]) => {
        const res = unpackFormattedAttrValue(val);
        assert.strictEqual(res, unpacked);
      });
    });

    it('returns objects and arrays as is', () => {
      const vals = [
        [],
        {},
        {a: 2},
        [2, 3, 4, 5],
      ];
      _.each(vals, (val) => {
        const res = unpackFormattedAttrValue(val);
        assert.deepStrictEqual(res, val);
      });
    });

    it('some invalid formatted attrValue that do not work', () => {
      const invalids = [
        undefined,
        '"',
      ];
      _.each(invalids, (invalid) => {
        const errMessage = 'invalid attrValue';
        assertThrows(() => unpackFormattedAttrValue(invalid), errMessage);
      });
    });

  });

});
