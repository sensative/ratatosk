/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const {assertThrows} = require('../src');
const {
  validateServicePath,
  validateMultipleServicePaths,
  parseServicePathsLoose,
  errors,
} = require('../src/utils/ngsi-service-path-utils');

describe('ngsi-service-path-utils', () => {

  describe('validateServicePath', () => {

    it('should validate undefined', () => {
      validateServicePath(undefined);
    });

    it('should validate null', () => {
      validateServicePath(null);
    });

    it('should validate "" (empty string)', () => {
      validateServicePath('');
    });

    it('in strict mode, "", null, and undefined DO throw', () => {
      const options = {isStrict: true};
      const mess = 'Only explicit valid servicePaths allowed in strict mode';
      assertThrows(() => validateServicePath(undefined, options), mess);
      assertThrows(() => validateServicePath(null, options), mess);
      assertThrows(() => validateServicePath('', options), mess);
    });

    it('in update mode, the wildcard character is not allowed', () => {
      const pathWithWildcard = '/#';
      assert.doesNotThrow(() => validateServicePath(pathWithWildcard));
      const mess = 'illegal char in servicePath component';
      assertThrows(() => validateServicePath(pathWithWildcard, {isUpdate: true}), mess);
    });

    it('should NOT validate false', () => {
      assert.throws(() => validateServicePath(false));
    });

    it('should NOT validate 0', () => {
      assert.throws(() => validateServicePath(0));
    });

    it('should NOT validate 9', () => {
      assert.throws(() => validateServicePath(9));
    });

    it('should NOT validate {}', () => {
      assert.throws(() => validateServicePath({}));
    });

    it('should validate "/"', () => {
      validateServicePath('/');
    });

    it('should validate "/asdf"', () => {
      validateServicePath('/asdf');
    });

    it('should validate "/abcABC_123"', () => {
      validateServicePath('/abcABC_123');
    });

    it('should NOT validate "/aa*aa"', () => {
      assert.throws(() => validateServicePath('/aa*aa'));
    });

    it('should validate "/asdf/derp/stupor"', () => {
      validateServicePath('/asdf/derp/stupor');
    });

    it('should NOT validate "////"', () => {
      assert.throws(() => validateServicePath('////'));
    });

    it('should NOT validate "/aaa////////////////////"', () => {
      assert.throws(() => validateServicePath('/aaa////////////////////'));
    });

    it('should validate "/a/b/c/d/e"', () => {
      validateServicePath('/a/b/c/d/e');
    });

    it('should NOT validate "/aa//aa"', () => {
      assert.throws(() => validateServicePath('/aa//aa'));
    });

    it('should validate, with 10 levels, 50 chars at each level', () => {
      const chars = 'abcdefghij'.split('');
      const levels = _.map(chars, (char) => {
        return _.times(50, () => char).join('');
      });
      const path = `/${levels.join('/')}`;
      validateServicePath(path);
    });

    it('should NOT validate, with 10 levels, 51 chars at each level', () => {
      const chars = 'abcdefghij'.split('');
      const levels = _.map(chars, (char) => {
        return _.times(51, () => char).join('');
      });
      const path = `/${levels.join('/')}`;
      assert.throws(() => validateServicePath(path));
    });

    it('should NOT validate, with 11 levels, 50 chars at each level', () => {
      const chars = 'abcdefghijk'.split(''); // <---- k was added
      const levels = _.map(chars, (char) => {
        return _.times(50, () => char).join('');
      });
      const path = `/${levels.join('/')}`;
      assert.throws(() => validateServicePath(path));
    });

  });

  describe('validateMultipleServicePaths', () => {

    it('must be a string or an array', () => {
      const invalids = [
        null,
        10,
        false,
        true,
        {0: '/asdf'},
      ];
      _.each(invalids, (invalid) => {
        const message = 'validateMultipleServicePaths argument must be a string or array';
        assertThrows(() => validateMultipleServicePaths(invalid), message);
      });
    });


    it('a bunch of valids', () => {
      const valids = [
        '/',
        '/a',
        '/aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
        '/aa/b/aa',
        '/a,/b,/c',
        ['/'],
        ['/a/s', '/asdf/sdf', '/asdf/asdf'],
      ];
      _.each(valids, (valid) => {
        assert.doesNotThrow(() => validateMultipleServicePaths(valid));
      });
    });

    it('if this is an update operation, then max 1 path', () => {
      const isUpdate = true;
      const pathValid = '/a';
      assert.doesNotThrow(() => validateServicePath(pathValid, isUpdate));
      const pathInvalid = '/a,/b';
      const message = 'Update operations can specify maximum 1 servicePaths';
      assertThrows(() => validateMultipleServicePaths(pathInvalid, isUpdate), message);
    });

    it('if this is an update operation, then wildcard is not allowed', () => {
      const isUpdate = true;
      const pathWithWildcard = '/#';
      const message = 'illegal char in servicePath component';
      assertThrows(() => validateMultipleServicePaths(pathWithWildcard, isUpdate), message);
    });

    it('a non-update operation (i.e. query) can have max 10 disjoint paths', () => {
      const pathValid = '/a,/b,/c,/d,/e,/f,/g,/h,/i,/j';
      assert.doesNotThrow(() => validateMultipleServicePaths(pathValid));
      const pathInvalid = '/a,/b,/c,/d,/e,/f,/g,/h,/i,/j,/k';
      const message = 'Too many servicePaths for this operation (max 10 allowed)';
      assertThrows(() => validateMultipleServicePaths(pathInvalid), message);
    });

    it('If the supplied argument is an, array, then each item must be a single valid path', () => {
      const validArr = ['/asdf', '/a/b/c', '/def'];
      assert.doesNotThrow(() => validateMultipleServicePaths(validArr));
      const invalidArr = ['/a', '/a,/b'];
      const message = 'illegal char in servicePath component';
      assertThrows(() => validateMultipleServicePaths(invalidArr), message);
    });

    it('an invalid servicePath item will get caught in "validateServicePath"', () => {
      const invalidPaths = [
        '',
        null,
        'noRoot',
        '/invalid char',
        ['/also invalid char'],
      ];
      _.each(invalidPaths, (invalid) => {
        assertThrows(() => validateMultipleServicePaths(invalid));
      });
    });

  });

  describe('parseServicePathsLoose', () => {

    it('some valid examples', () => {
      const valids = [
        {arg: null, res: ['/#']},
        {arg: '/aasdf', res: ['/aasdf']},
      ];
      _.each(valids, (valid) => {
        const paths = parseServicePathsLoose(valid.arg);
        assert.deepStrictEqual(paths, valid.res);
      });
    });

    it('also provides defaults for both "isUpdate" or not', () => {
      // not an update
      const pathsQuery = parseServicePathsLoose(null, false);
      assert.deepStrictEqual(pathsQuery, ['/#']);
      // is an update
      const pathsUpdate = parseServicePathsLoose(null, true);
      assert.deepStrictEqual(pathsUpdate, ['/']);
    });

  });

  describe('servicePath errors', () => {

    it('there are 11 servicePath errors possible', () => {
      assert.strictEqual(_.size(errors), 11);
    });

    it('they are all isNgsiServicePathErrors', () => {
      _.each(errors, (error) => {
        const err = error();
        assert(err.isNgsiServicePathError, 'it should be an isNgsiServicePathError');
      });
    });

    it('they all match instances of themselves', () => {
      _.each(errors, (error) => {
        const err = error();
        assert(error.matches(err), 'it should match an instance');
      });
    });

    it('they all match instance message of themselves', () => {
      _.each(errors, (error) => {
        const errMessage = error().message;
        assert(error.matches(errMessage), 'it should match a matching error message');
      });
    });

    it('they fail to match non-matches', () => {
      const invalidMessage = 'invalid error message';
      const invalidError = new Error(invalidMessage);
      _.each(errors, (error) => {
        assert(!error.matches(invalidMessage), 'it should fail to match');
        assert(!error.matches(invalidError), 'it should fail to match');
      });
    });

  });

});
