/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const {
  assertThrows,
  ngsiDatumValidator: {validateEntityId},
} = require('../src');

const {
  errors,
  formatProp,
  formatAttribute,
  formatAttrs,
  formatEntity,
} = require('../src/utils/ngsi-entity-formatter');

describe('entity-formatter', () => {

  describe('errors', () => {

    it('there are 8 isNgsiEntityFormatterError errors', () => {
      assert.strictEqual(_.size(errors), 8);
    });

    it('the errors have some "matches" functionality and identifier flag', () => {
      const nonmatchingErr = new Error('this one does not match nuthin');
      _.each(errors, (error) => {
        const datumErr = error();
        assert(datumErr.isNgsiEntityFormatterError);
        assert(error.matches(datumErr));
        assert(error.matches(datumErr.message));
        assert(!error.matches(nonmatchingErr));
        assert(!error.matches(nonmatchingErr.message));
      });
    });

  });

  describe('formatProp', () => {

    it('prop cannot be undefined', () => {
      assertThrows(() => formatProp(undefined), errors.INVALID_PROPERTY_VALUE());
      assert.deepStrictEqual(formatProp({type: 'Number', value: null}), {type: 'Number', value: null}, 'control test');
    });

    it('prop value cannot be undefined', () => {
      const invalids = [
        {type: 'Number', value: undefined},
        {type: 'Number'},
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => formatProp(invalid), errors.INVALID_PROPERTY_VALUE());
      });
    });

    it('explicit types should be respected (except 0 and other falsy stuff)', () => {
      assert.deepStrictEqual(formatProp({type: undefined, value: null}), {type: 'None', value: null});
      assert.deepStrictEqual(formatProp({type: 0, value: null}), {type: 'None', value: null});
      assert.deepStrictEqual(formatProp({type: 0, value: 2}), {type: 'Number', value: 2});
      assert.deepStrictEqual(formatProp({type: 'Splerp', value: 2}), {type: 'Splerp', value: 2});
    });

    it('fails when type is truthy, but not a string', () => {
      const invalidTypes = [3, [], {}];
      _.each(invalidTypes, (invalidType) => {
        const invalid = {value: 2, type: invalidType};
        assertThrows(() => formatProp(invalid), errors.INVALID_VALUE_TYPE());
      });
    });

    it('implicit simple types should match expected', () => {
      const valids = [
        {in: 0, out: {type: 'Number', value: 0}},
        {in: -3.14, out: {type: 'Number', value: -3.14}},
        {in: true, out: {type: 'Boolean', value: true}},
        {in: false, out: {type: 'Boolean', value: false}},
        {in: [2, 3], out: {type: 'StructuredValue', value: [2, 3]}},
        {in: {a: 2, b: 3}, out: {type: 'StructuredValue', value: {a: 2, b: 3}}},
        {in: null, out: {type: 'None', value: null}},
      ];
      _.each(valids, (valid) => {
        assert.deepStrictEqual(formatProp(valid.in), valid.out);
      });
    });

  });

  describe('formatAttribute', () => {

    it('a trivial working example', () => {
      const valid = {type: 'Number', value: null, metadata: {}};
      assert.deepStrictEqual(formatAttribute(valid), valid);
    });

    it('should throw for undefined values', () => {
      const invalids = [
        undefined,
        {type: 'Number'},
        {value: undefined, type: 'Number'},
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => formatAttribute(invalid), errors.INVALID_PROPERTY_VALUE());
      });
    });

    it('explicit types should be respected (except 0 and other falsy stuff)', () => {
      const valids = [
        {in: {type: 0, value: null}, out: {type: 'None', value: null, metadata: {}}},
        {in: {type: 0, value: 2}, out: {type: 'Number', value: 2, metadata: {}}},
        {in: {type: 'Splerp', value: 2}, out: {type: 'Splerp', value: 2, metadata: {}}},
      ];
      _.each(valids, (valid) => {
        assert.deepStrictEqual(formatAttribute(valid.in), valid.out);
      });
    });

    it('should format simple attributes', () => {
      const valids = [
        {in: 0, out: {value: 0, type: 'Number', metadata: {}}},
        {in: -3.14, out: {value: -3.14, type: 'Number', metadata: {}}},
        {in: true, out: {value: true, type: 'Boolean', metadata: {}}},
        {in: false, out: {value: false, type: 'Boolean', metadata: {}}},
        {in: [2, 3], out: {value: [2, 3], type: 'StructuredValue', metadata: {}}},
        {in: {a: 2, b: 3}, out: {value: {a: 2, b: 3}, type: 'StructuredValue', metadata: {}}},
        {in: null, out: {value: null, type: 'None', metadata: {}}},
      ];
      _.each(valids, (valid) => {
        assert.deepStrictEqual(formatAttribute(valid.in), valid.out);
      });
    });

    it('should format metadata as expected', () => {
      const simpleMeta = {
        value: 9,
        metadata: {
          derp: {type: 'Spok', value: 3},
          splirk: 'arp',
          dunk: null,
        },
      };
      const expectedSimpleMeta = {
        type: 'Number',
        value: 9,
        metadata: {
          derp: {type: 'Spok', value: 3},
          splirk: {type: 'Text', value: 'arp'},
          dunk: {type: 'None', value: null},
        },
      };
      assert.deepStrictEqual(formatAttribute(simpleMeta), expectedSimpleMeta);
    });

  });


  describe('formatAttrs', () => {

    it('attrs must be an object', () => {
      const invalids = [
        null,
        undefined,
        10,
        'asdf',
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => formatAttrs(invalid), errors.INVALID_ATTRS_CONTAINER());
      });
    });

    it('attrs cannot have an id prop', () => {
      const invalids = [
        {id: 10},
        {id: 'asdf'},
        {id: 'asdf', otherProp: '1234'},
        {id: 'asdf', type: '1234'}, // id is checked before type
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => formatAttrs(invalid), errors.INVALID_ATTRS_HAS_ID());
      });
    });

    it('attrs cannot have a type prop', () => {
      const invalids = [
        {type: 10},
        {type: 'asdf'},
        {type: 'asdf', otherProp: '1234'},
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => formatAttrs(invalid), errors.INVALID_ATTRS_HAS_TYPE());
      });
    });

    it('should handle a general example entity', () => {
      const rawAttrs = {
        twine: 3,
        dirk: {value: 2},
        happy: {type: 'Knee', value: true},
        metazto: {
          value: null,
          metadata: {
            ecto: {value: null},
            plasm: 'Very good stuff',
          },
        },
      };
      const expectedAttrs = {
        twine: {type: 'Number', value: 3, metadata: {}},
        dirk: {type: 'Number', value: 2, metadata: {}},
        happy: {type: 'Knee', value: true, metadata: {}},
        metazto: {
          type: 'None',
          value: null,
          metadata: {
            ecto: {type: 'None', value: null},
            plasm: {type: 'Text', value: 'Very good stuff'},
          },
        },
      };
      const attrs = formatAttrs(rawAttrs);
      assert.deepStrictEqual(attrs, expectedAttrs);
    });

  });


  describe('formatEntity', () => {

    it('entity must be an object', () => {
      const invalids = [
        null,
        undefined,
        'asdf',
        23,
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => formatEntity(invalid), errors.INVALID_ENTITY_CONTAINER());
      });
    });

    it('should format a simple entity w/out attrs', () => {
      const simpleValid = {type: 'Vroom', id: 'shmoze'};
      const res = formatEntity(simpleValid);
      assert.deepStrictEqual(res, simpleValid);
    });

    it('should fail without valid id (uses validateEntityId)', () => {
      const invalids = [
        '',
        [2, 3],
        null,
      ];
      _.each(invalids, (invalid) => {
        const formatErr = assertThrows(() => formatEntity({type: 'Irrelevant', id: invalid}));
        const validateErr = assertThrows(() => validateEntityId(invalid));
        assert.deepStrictEqual(formatErr, validateErr);
      });
    });

    it('should handle simple entity w/out explicit type', () => {
      const res = formatEntity({id: 'brah'});
      const expected = {id: 'brah', type: 'Thing'};
      assert.deepStrictEqual(res, expected);
    });

    it('should handle a general example entity', () => {
      const rawEnt = {
        type: 'Derp',
        id: 'ssshmo',
        twine: 3,
        dirk: {value: 2},
        happy: {type: 'Knee', value: true},
        metazto: {
          value: null,
          metadata: {
            ecto: {value: null},
            plasm: 'Very good stuff',
          },
        },
      };
      const expEnt = {
        type: 'Derp',
        id: 'ssshmo',
        twine: {type: 'Number', value: 3, metadata: {}},
        dirk: {type: 'Number', value: 2, metadata: {}},
        happy: {type: 'Knee', value: true, metadata: {}},
        metazto: {
          type: 'None',
          value: null,
          metadata: {
            ecto: {type: 'None', value: null},
            plasm: {type: 'Text', value: 'Very good stuff'},
          },
        },
      };
      assert.deepStrictEqual(formatEntity(rawEnt), expEnt);
    });

  });

});
