/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const {assertThrows} = require('../src');

const {
  assertErrorsMatch,
  assertErrorsMatchish,
  assertErrorsMatchesColonTruncated,
  assertErrorsMatchFails,
  NgsiError,
  createError,
} = require('../src/utils/ngsi-response-error-utils');

const invalidNgsiErr = () => ({message: 'invalid ngsi-error template'});

describe('ngsi-error', () => {

  describe('the basic NgsiError object', () => {

    it('needs the "new" keyword', () => {
      const message = 'Class constructor NgsiError cannot be invoked without \'new\'';
      assertThrows(() => NgsiError(), {message});
    });

    it('the arg needs "name", "message" and "status" of correct type', () => {
      const invalids = [
        null,
        undefined,
        0,
        1,
        false,
        true,
        'bob',
        {name: 'ab', message: 'ba'},
        {name: 'ab', status: 400},
        {message: 'ba', status: 400},
        {name: 'ab', message: 'ba', status: 399},
        {name: 'ab', message: 'ba', status: 600},
        {name: 2, message: 'ba', status: 400},
        {name: 'ab', message: 2, status: 400},
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => new NgsiError(invalid), invalidNgsiErr());
      });
    });

    it('the arg needs "name", "message" and "status" of correct type', () => {
      const valids = [
        {name: '', message: '', status: 400},
        {name: '', message: '', status: 599},
        {name: 'ab', message: 'ba', status: 437},
      ];
      _.each(valids, (valid) => {
        assert.doesNotThrow(() => new NgsiError(valid));
      });
    });

    it('a minimal valid error', () => {
      const err = new NgsiError({name: 'bob', message: 'mary', status: 400});
      assert.strictEqual(err.name, 'bob');
      assert.strictEqual(err.message, 'mary');
      assert.strictEqual(err.status, 400);
      assert.strictEqual(err.constructor.name, 'NgsiError');
      assert.strictEqual(err.isNgsiError, true);
    });

  });

  describe('createError (with a template = {name, message, status} object) -- this is just an alias for "new NgsiError(template)"', () => {

    it('some invalids', () => {
      const invalids = [
        null,
        undefined,
        0,
        1,
        false,
        true,
        'bob',
        {name: 'ab', message: 'ba'},
        {name: 'ab', status: 400},
        {message: 'ba', status: 400},
        {name: 'ab', message: 'ba', status: 399},
        {name: 2, message: 'ba', status: 400},
        {name: 'ab', message: 2, status: 400},
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => createError(invalid), invalidNgsiErr());
      });
    });

    it('a minimal valid error', () => {
      const err = createError({name: 'bob', message: 'mary', status: 400});
      assert.strictEqual(err.name, 'bob');
      assert.strictEqual(err.message, 'mary');
      assert.strictEqual(err.status, 400);
      assert.strictEqual(err.constructor.name, 'NgsiError');
      assert.strictEqual(err.isNgsiError, true);
      const rawErr = new NgsiError({name: 'bob', message: 'mary', status: 400});
      assert.deepStrictEqual(err, rawErr, 'createError is just a wrapper');
    });

  });

  describe('assertErrorsMatch', () => {

    it('some errors that succeed', () => {
      const matchers = [
        // from the template
        {name: 'asdf', message: 'klk', status: 453},
        new NgsiError({name: 'asdf', message: 'klk', status: 453}),
        createError({name: 'asdf', message: 'klk', status: 453}),
        // from the responses
        {response: {status: 453, data: {error: 'asdf', description: 'klk'}}},
        {status: 453, data: {error: 'asdf', description: 'klk'}},
      ];
      _.each(matchers, (m1) => {
        _.each(matchers, (m2) => {
          assertErrorsMatch(m1, m2);
        });
      });
    });

    it('some errors that fail to match with each other', () => {
      const nonMatchers = [
        // the basic error
        {name: 'asdf', message: 'klk', status: 453},
        // single mods
        {name: 'asd', message: 'klk', status: 453},
        {name: 'asdf', message: 'kl', status: 453},
        {name: 'asdf', message: 'klk', status: 452},
        // and 2 that should later matchish
        {name: 'asdf', message: 'klk /AA/ klk', status: 453},
        {name: 'asdf', message: 'klk /BB/ klk', status: 453},
      ];
      _.each(nonMatchers, (m1) => {
        _.each(nonMatchers, (m2) => {
          if (m1 !== m2) {
            assertThrows(() => assertErrorsMatch(m1, m2));
          }
        });
      });
    });

  });

  describe('assertErrorsMatchFails', () => {

    it('some errors that succeed in failing to match with each other', () => {
      const nonMatchers = [
        // the basic error
        {name: 'asdf', message: 'klk', status: 453},
        // single mods
        {name: 'asd', message: 'klk', status: 453},
        {name: 'asdf', message: 'kl', status: 453},
        {name: 'asdf', message: 'klk', status: 452},
        // and 2 that should later matchish
        {name: 'asdf', message: 'klk /AA/ klk', status: 453},
        {name: 'asdf', message: 'klk /BB/ klk', status: 453},
      ];
      _.each(nonMatchers, (m1) => {
        _.each(nonMatchers, (m2) => {
          if (m1 !== m2) {
            assert.doesNotThrow(() => assertErrorsMatchFails(m1, m2));
          }
        });
      });
    });

    it('some errors that fail in failing to match with each other', () => {
      const matchers = [
        // from the template
        {name: 'asdf', message: 'klk', status: 453},
        new NgsiError({name: 'asdf', message: 'klk', status: 453}),
        createError({name: 'asdf', message: 'klk', status: 453}),
        // from the responses
        {response: {status: 453, data: {error: 'asdf', description: 'klk'}}},
        {status: 453, data: {error: 'asdf', description: 'klk'}},
      ];
      _.each(matchers, (m1) => {
        _.each(matchers, (m2) => {
          assertThrows(() => assertErrorsMatchFails(m1, m2));
        });
      });
    });

  });

  describe('assertErrorsMatchish', () => {

    it('some pairs that matchish', () => {
      const name = 'derp';
      const status = 489;
      const messagePairs = [
        ['asdf', 'asdf'], // if it matches, then it matchish
        ['asdf/bbbb/asdf', 'asdf/z/asdf'], // after removint stuff in //, these match
      ];
      _.each(messagePairs, ([m1, m2]) => {
        const err1 = {name, status, message: m1};
        const err2 = {name, status, message: m2};
        assertErrorsMatchish(err1, err2);
      });
    });

  });


  describe('assertErrorsMatchesColonTruncated', () => {

    it('some pairs that colon matches', () => {
      const name = 'derp';
      const status = 489;
      const messagePairs = [
        ['asdf', 'asdf'], // if it matches, then it matchish
        ['asdf: 2q3', 'asdf: [ ccccc ]'], // colon with stuff after works
        ['asdf', 'asdf: [2]'], // can mix
      ];
      _.each(messagePairs, ([m1, m2]) => {
        const err1 = {name, status, message: m1};
        const err2 = {name, status, message: m2};
        assertErrorsMatchesColonTruncated(err1, err2);
      });
    });

    it('some pairs that DO NOT colon match', () => {
      const name = 'derp';
      const status = 489;
      const messagePairs = [
        ['asdf', 'asdfff'], // if it matches, then it matchish
        ['asdfg: 2q3', 'asdfh: [ ccccc ]'], // colon with stuff after works
        ['asdf', 'asdf : [2]'], // can mix
      ];
      _.each(messagePairs, ([m1, m2]) => {
        const err1 = {name, status, message: m1};
        const err2 = {name, status, message: m2};
        assertThrows(() => assertErrorsMatchesColonTruncated(err1, err2));
      });
    });

  });

});
