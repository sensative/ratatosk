/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {
  PARAM_LIST_ITEM_GROUPER,
  PARAM_LIST_ITEM_SEPARATOR,
  REST_PAGINATION_LIMIT_MAXIMUM,
  PAGING_DIRECTIONS,
} = require('@ratatosk/ngsi-constants');

const {tokenize} = require('../../libs/ngsi-tokenize');
const {generateErrors} = require('../../libs/ngsi-error-generator');
const {
  validateEntityId,
  validateEntityIdPattern,
  validateEntityType,
  validateEntityTypePattern,
} = require('../ngsi-datum-validator');

const errorTemplates = {
  // parser specific errors
  EMPTY_ENTITY_ID_PARAM: 'The entity id param must be a non-empty string if included',
  INVALID_ENTITY_ID_PARAM_ITEM: 'The id param contains an invalid id item',
  INVALID_ENTITY_ID_DUPLICATES: 'Duplicates are not allowed in the entity id list',
  INVALID_ENTITY_ID_PARAM_FORMATTING: 'The entity id param is formatted incorrectly',
  INVALID_ENTITY_ID_LIST: 'The entityId list must be an array',

  EMPTY_ENTITY_TYPE_PARAM: 'The entity type param must be a non-empty string if included',
  INVALID_ENTITY_TYPE_PARAM_ITEM: 'The ityped param contains an invalid type item',
  INVALID_ENTITY_TYPE_DUPLICATES: 'Duplicates are not allowed in the entity type list',
  INVALID_ENTITY_TYPE_PARAM_FORMATTING: 'The entity type param is formatted incorrectly',
  INVALID_ENTITY_TYPE_LIST: 'The entityType list must be an array',

  // pattern errors
  EMPTY_ENTITY_ID_PATTERN: 'The idPattern param must be a non-empty string if included',
  EMPTY_ENTITY_TYPE_PATTERN: 'The typePattern param must be a non-empty string if included',

  // pageLimit and pageOffset errors
  EMPTY_PAGE_OFFSET_PARAM: 'The page offset param must be a non-empty string (or a number) if included',
  INVALID_PAGE_OFFSET_PARAM: 'Invalid page offset: must be a positive integer (or zero), or a corresponding string',
  EMPTY_PAGE_LIMIT_PARAM: 'The page limit param must be a non-empty string (or a number) if included',
  INVALID_PAGE_LIMIT_PARAM: 'Invalid page limit: must be a positive integer (strictly non-zero), or a corresponding string',

  // keyset paging errors
  INVALID_PAGE_ITEM_ID_PARAM: 'The paging item id has to be a non-empty string',
  INVALID_PAGE_DIRECTION_PARAM: 'The paging direction must be either "prev" or "next"',
};
const errors = generateErrors('NGSI_BASIC_PARAM_ERROR', errorTemplates);

// /////////
// some tools for parsing the different comma-separated lists
// /////////

const wrapListItemInGroupers = (item) => {
  const requiresGrouping = item.indexOf(PARAM_LIST_ITEM_SEPARATOR) > -1;
  if (requiresGrouping) {
    return `${PARAM_LIST_ITEM_GROUPER}${item}${PARAM_LIST_ITEM_GROUPER}`;
  }
  return item;
};

const validateEntityIdItem = (item) => {
  try {
    validateEntityId(item);
  } catch (err) {
    throw errors.INVALID_ENTITY_ID_PARAM_ITEM();
  }
};

const validateEntityTypeItem = (item) => {
  try {
    validateEntityType(item);
  } catch (err) {
    throw errors.INVALID_ENTITY_TYPE_PARAM_ITEM();
  }
};

//
// entity id stuff
//

const parseEntityIdParam = (entityIdParam) => {
  if (_.isNil(entityIdParam)) {
    return [];
  }
  if (!_.isString(entityIdParam) || !entityIdParam.length) {
    throw errors.EMPTY_ENTITY_ID_PARAM();
  }
  let entityIdList;
  try {
    entityIdList = tokenize(entityIdParam, PARAM_LIST_ITEM_SEPARATOR, PARAM_LIST_ITEM_GROUPER);
  } catch (err) {
    throw errors.INVALID_ENTITY_ID_PARAM_FORMATTING();
  }
  if (_.uniq(entityIdList).length !== entityIdList.length) {
    throw errors.INVALID_ENTITY_ID_DUPLICATES();
  }
  _.each(entityIdList, (item) => validateEntityIdItem(item));
  return entityIdList; // DONE
};

const serializeEntityIdList = (entityIdList) => {
  if (_.isNil(entityIdList)) {
    return null;
  }
  if (!_.isArray(entityIdList)) {
    throw errors.INVALID_ENTITY_ID_LIST();
  }
  if (!entityIdList.length) {
    return null;
  }
  if (_.uniq(entityIdList).length !== entityIdList.length) {
    throw errors.INVALID_ENTITY_ID_DUPLICATES();
  }
  _.each(entityIdList, (item) => validateEntityIdItem(item));
  const groupedList = _.map(entityIdList, (item) => wrapListItemInGroupers(item));
  const entityIdParam = groupedList.join(PARAM_LIST_ITEM_SEPARATOR);
  return entityIdParam;
};

const validateEntityIdParam = (entityIdParam) => {
  parseEntityIdParam(entityIdParam);
};

const validateEntityIdList = (entityIdList) => {
  serializeEntityIdList(entityIdList);
};

//
// entity type param
//

const parseEntityTypeParam = (entityTypeParam) => {
  if (_.isNil(entityTypeParam)) {
    return [];
  }
  if (!_.isString(entityTypeParam) || !entityTypeParam.length) {
    throw errors.EMPTY_ENTITY_TYPE_PARAM();
  }
  let entityTypeList;
  try {
    entityTypeList = tokenize(entityTypeParam, PARAM_LIST_ITEM_SEPARATOR, PARAM_LIST_ITEM_GROUPER);
  } catch (err) {
    throw errors.INVALID_ENTITY_TYPE_PARAM_FORMATTING();
  }
  if (_.uniq(entityTypeList).length !== entityTypeList.length) {
    throw errors.INVALID_ENTITY_TYPE_DUPLICATES();
  }
  _.each(entityTypeList, (item) => validateEntityTypeItem(item));
  return entityTypeList; // DONE
};

const serializeEntityTypeList = (entityTypeList) => {
  if (_.isNil(entityTypeList)) {
    return null;
  }
  if (!_.isArray(entityTypeList)) {
    throw errors.INVALID_ENTITY_TYPE_LIST();
  }
  if (!entityTypeList.length) {
    return null;
  }
  if (_.uniq(entityTypeList).length !== entityTypeList.length) {
    throw errors.INVALID_ENTITY_TYPE_DUPLICATES();
  }
  _.each(entityTypeList, (item) => validateEntityTypeItem(item));
  const groupedList = _.map(entityTypeList, (item) => wrapListItemInGroupers(item));
  const entityTypeParam = groupedList.join(PARAM_LIST_ITEM_SEPARATOR);
  return entityTypeParam;
};

const validateEntityTypeParam = (entityTypeParam) => {
  parseEntityTypeParam(entityTypeParam);
};

const validateEntityTypeList = (entityTypeList) => {
  serializeEntityTypeList(entityTypeList);
};

const validateIdPatternParam = (idPattern) => {
  if (_.isNil(idPattern)) {
    return; // is valid
  }
  if (!_.isString(idPattern) || !idPattern.length) {
    throw errors.EMPTY_ENTITY_ID_PATTERN();
  }
  validateEntityIdPattern(idPattern);
};

const validateTypePatternParam = (typePattern) => {
  if (_.isNil(typePattern)) {
    return; // is valid
  }
  if (!_.isString(typePattern) || !typePattern.length) {
    throw errors.EMPTY_ENTITY_TYPE_PATTERN();
  }
  validateEntityTypePattern(typePattern);
};

// the page limit stuff

const validatePageLimitParam = (pageLimit) => {
  if (_.isNil(pageLimit)) {
    return; // isValid
  }
  const hasValidType = _.isString(pageLimit) || _.isNumber(pageLimit);
  if (pageLimit === '' || !hasValidType || isNaN(pageLimit)) {
    throw errors.EMPTY_PAGE_LIMIT_PARAM();
  }
  const num = _.toNumber(pageLimit);
  if (num <= 0 || num > REST_PAGINATION_LIMIT_MAXIMUM || !_.isSafeInteger(num)) {
    throw errors.INVALID_PAGE_LIMIT_PARAM();
  }
};

const validatePageOffsetParam = (pageOffset) => {
  if (_.isNil(pageOffset)) {
    return; // isValid
  }
  const hasValidType = _.isString(pageOffset) || _.isNumber(pageOffset);
  if (pageOffset === '' || !hasValidType || isNaN(pageOffset)) {
    throw errors.EMPTY_PAGE_OFFSET_PARAM();
  }
  const num = _.toNumber(pageOffset);
  if (num < 0 || !_.isSafeInteger(num)) {
    throw errors.INVALID_PAGE_OFFSET_PARAM();
  }
};

const validatePageItemIdParam = (pageItemId) => {
  // should this be more sophisticated? Probably.. but probably should not be mongo-specific
  if (!_.isString(pageItemId)) {
    throw errors.INVALID_PAGE_ITEM_ID_PARAM();
  }
};

const validatePageDirectionParam = (pageDirection) => {
  if (!_.includes(_.values(PAGING_DIRECTIONS), pageDirection)) {
    throw errors.INVALID_PAGE_DIRECTION_PARAM();
  }
};

module.exports = {
  errors,
  // id
  parseEntityIdParam,
  serializeEntityIdList,
  validateEntityIdParam,
  validateEntityIdList,
  // type
  parseEntityTypeParam,
  serializeEntityTypeList,
  validateEntityTypeParam,
  validateEntityTypeList,
  // pattern stuff
  validateIdPatternParam,
  validateTypePatternParam,
  // page limit & offset params
  validatePageLimitParam,
  validatePageOffsetParam,
  validatePageItemIdParam,
  validatePageDirectionParam,
};
