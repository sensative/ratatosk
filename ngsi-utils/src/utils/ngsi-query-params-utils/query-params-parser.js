/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {
  ENTITY_REQUEST_PARAM_KEYS: {
    ENTITY_TYPE, // 'type'
    ENTITY_TYPE_PATTERN, // 'typePattern'
    ENTITY_ID, // 'id'
    ENTITY_ID_PATTERN, // 'idPattern'
    ATTRIBUTE_QUERY, // 'q'
    METADATA_QUERY, // 'mq'

    ATTRS_LIST, // 'attrs'
    METADATA_LIST, // 'metadata'
    ORDER_BY, // 'orderBy'

    OPTIONS_PARAM, // 'options'

    PAGE_LIMIT, // 'limit'
    PAGE_OFFSET, // 'offset'
    PAGE_ITEM_ID, // 'pageItemId'
    PAGE_DIRECTION, // 'pageDirection'

    GEOMETRY, // geometry
    COORDS, // coords
    GEOREL, // georel
  },
} = require('@ratatosk/ngsi-constants');

const {
  ADDED_QUERY_PARAM_KEYS: {
    TOTAL_COUNT,
    REPRESENTATION_TYPE,
    STRICT_APPEND,
  },
  NGSI_SQL_QUERY_TYPES,
} = require('../../constants');

const {generateErrors} = require('../../libs/ngsi-error-generator');

const {

  queryParser: {
    serializeQueryList,
    validateQueryString,
  },
} = require('../ngsi-sql');

const {
  serializeAttrsList,
  validateAttrsParam,
} = require('./attrs-param-parser');
const {
  serializeMetadataList,
  validateMetadataParam,
} = require('./metadata-param-parser');
const {
  validateOrderbyParam,
  serializeOrderbyList,
} = require('./orderby-param-parser');
const {
  combineMultipleOptionsArgs,
} = require('./options-param-parser');
const {
  validateEntityIdParam,
  serializeEntityIdList,
  validateEntityTypeParam,
  serializeEntityTypeList,
  validateIdPatternParam,
  validateTypePatternParam,
  validatePageLimitParam,
  validatePageOffsetParam,
  validatePageItemIdParam,
  validatePageDirectionParam,
} = require('./basic-param-parser');
const {
  parseGeometryAndCoordsParam,
  parseGeorelParam,
} = require('./geometry-params-parser');

const errorTemplates = {
  INVALID_QUERY_PARAMS_ARG: 'The query params arg must be either nil or an object',
  INCOMPATIBLE_PARAMS_ID_PATTERN: 'Only one of "id" and "idPattern" params can be used at once',
  INCOMPATIBLE_PARAMS_TYPE_PATTERN: 'Only one of "type" and "typePattern" params can be used at once',
  INCOMPATIBLE_GEOMETRY_PARAMS: 'If any geometry params are included (georel, geometry, coords), then they all need to be included',
};
const errors = generateErrors('NGSI_QUERY_PARAMS_PARSER_ERROR', errorTemplates);


const assertQueryParamsAllCompatible = (queryParams) => {
  // check param compatibility
  if (_.has(queryParams, ENTITY_ID) && _.has(queryParams, ENTITY_ID_PATTERN)) {
    throw errors.INCOMPATIBLE_PARAMS_ID_PATTERN();
  }
  if (_.has(queryParams, ENTITY_TYPE) && _.has(queryParams, ENTITY_TYPE_PATTERN)) {
    throw errors.INCOMPATIBLE_PARAMS_TYPE_PATTERN();
  }
  // check geometry stuff
  const hasGeometry = _.has(queryParams, GEOMETRY);
  const hasCoords = _.has(queryParams, COORDS);
  const hasGeorel = _.has(queryParams, GEOREL);
  if (hasGeometry || hasCoords || hasGeorel) {
    if (!hasGeometry || !hasCoords || !hasGeorel) {
      throw errors.INCOMPATIBLE_GEOMETRY_PARAMS();
    }
  }
};

// MAIN EXPORT
const formatQueryParams = (inputParams) => {
  // preamble
  if (_.isNil(inputParams) || _.isEqual(inputParams, {})) {
    return {};
  }
  if (!_.isObject(inputParams)) {
    throw errors.INVALID_QUERY_PARAMS_ARG();
  }
  assertQueryParamsAllCompatible(inputParams);

  // constructing & validating the outputParams object is the main exercise here
  const outputParams = {}; // this guy gets mutated throughout

  if (_.has(inputParams, ENTITY_ID)) {
    const entityId = inputParams[ENTITY_ID];
    if (_.isString(entityId)) {
      validateEntityIdParam(entityId);
      outputParams[ENTITY_ID] = entityId;
    } else {
      outputParams[ENTITY_ID] = serializeEntityIdList(entityId);
    }
  }

  if (_.has(inputParams, ENTITY_TYPE)) {
    const entityType = inputParams[ENTITY_TYPE];
    if (_.isString(entityType)) {
      validateEntityTypeParam(entityType);
      outputParams[ENTITY_TYPE] = entityType;
    } else {
      outputParams[ENTITY_TYPE] = serializeEntityTypeList(entityType);
    }
  }

  if (_.has(inputParams, ENTITY_ID_PATTERN)) {
    const entityIdPattern = inputParams[ENTITY_ID_PATTERN];
    validateIdPatternParam(entityIdPattern);
    outputParams[ENTITY_ID_PATTERN] = entityIdPattern || null;
  }

  if (_.has(inputParams, ENTITY_TYPE_PATTERN)) {
    const entityTypePattern = inputParams[ENTITY_TYPE_PATTERN];
    validateTypePatternParam(entityTypePattern);
    outputParams[ENTITY_TYPE_PATTERN] = entityTypePattern || null;
  }

  if (_.has(inputParams, ATTRS_LIST)) {
    const attrs = inputParams[ATTRS_LIST];
    if (_.isString(attrs)) {
      validateAttrsParam(attrs);
      outputParams[ATTRS_LIST] = attrs;
    } else {
      outputParams[ATTRS_LIST] = serializeAttrsList(attrs);
    }
  }

  if (_.has(inputParams, METADATA_LIST)) {
    const metadata = inputParams[METADATA_LIST];
    if (_.isString(metadata)) {
      validateMetadataParam(metadata);
      outputParams[METADATA_LIST] = metadata;
    } else {
      outputParams[METADATA_LIST] = serializeMetadataList(metadata);
    }
  }

  if (_.has(inputParams, ORDER_BY)) {
    const orderBy = inputParams[ORDER_BY];
    if (_.isString(orderBy)) {
      validateOrderbyParam(orderBy);
      outputParams[ORDER_BY] = orderBy;
    } else {
      outputParams[ORDER_BY] = serializeOrderbyList(orderBy);
    }
  }

  // This guy is kind of chained at the hip to TOTAL_COUNT
  if (_.has(inputParams, OPTIONS_PARAM) || _.has(inputParams, REPRESENTATION_TYPE) || _.has(inputParams, TOTAL_COUNT) || _.has(inputParams, STRICT_APPEND)) {
    // extract
    const optionsArg = inputParams[OPTIONS_PARAM];
    const appendArg = inputParams[STRICT_APPEND];
    const countArg = inputParams[TOTAL_COUNT];
    const repTypeArg = inputParams[REPRESENTATION_TYPE];
    // combine
    const optionsParam = combineMultipleOptionsArgs(
      optionsArg,
      _.isNil(appendArg) ? {} : {append: appendArg},
      _.isNil(countArg) ? {} : {count: countArg},
      _.isNil(repTypeArg) ? {} : {representationType: repTypeArg},
    );
    if (optionsParam) {
      outputParams[OPTIONS_PARAM] = optionsParam;
    }
  }

  if (_.has(inputParams, ATTRIBUTE_QUERY)) {
    const attrQuery = inputParams[ATTRIBUTE_QUERY];
    if (_.isString(attrQuery)) {
      validateQueryString(attrQuery, NGSI_SQL_QUERY_TYPES.ATTRIBUTE_QUERY);
      outputParams[ATTRIBUTE_QUERY] = attrQuery;
    } else {
      outputParams[ATTRIBUTE_QUERY] = serializeQueryList(attrQuery, NGSI_SQL_QUERY_TYPES.ATTRIBUTE_QUERY);
    }
  }

  if (_.has(inputParams, METADATA_QUERY)) {
    const metadataQuery = inputParams[METADATA_QUERY];
    if (_.isString(metadataQuery)) {
      validateQueryString(metadataQuery, NGSI_SQL_QUERY_TYPES.METADATA_QUERY);
      outputParams[METADATA_QUERY] = metadataQuery;
    } else {
      outputParams[METADATA_QUERY] = serializeQueryList(metadataQuery, NGSI_SQL_QUERY_TYPES.METADATA_QUERY);
    }
  }

  if (_.has(inputParams, PAGE_LIMIT)) {
    const limit = _.get(inputParams, PAGE_LIMIT);
    validatePageLimitParam(limit);
    outputParams[PAGE_LIMIT] = _.toSafeInteger(limit);
  }

  if (_.has(inputParams, PAGE_OFFSET)) {
    const offset = _.get(inputParams, PAGE_OFFSET);
    validatePageOffsetParam(offset);
    outputParams[PAGE_OFFSET] = _.toSafeInteger(offset);
  }

  if (_.has(inputParams, PAGE_ITEM_ID)) {
    const pageItemId = _.get(inputParams, PAGE_ITEM_ID);
    validatePageItemIdParam(pageItemId);
    outputParams[PAGE_ITEM_ID] = pageItemId;
  }

  if (_.has(inputParams, PAGE_DIRECTION)) {
    const pageDirection = _.get(inputParams, PAGE_DIRECTION);
    validatePageDirectionParam(pageDirection);
    outputParams[PAGE_DIRECTION] = pageDirection;
  }

  if (_.has(inputParams, GEOMETRY)) {
    const georel = _.get(inputParams, GEOREL);
    const geometry = _.get(inputParams, GEOMETRY);
    const coords = _.get(inputParams, COORDS);
    parseGeometryAndCoordsParam(geometry, coords);
    parseGeorelParam(georel, geometry);
    outputParams[GEOREL] = georel;
    outputParams[GEOMETRY] = geometry;
    outputParams[COORDS] = coords;
  }

  return outputParams;
};

module.exports = {
  errors,
  formatQueryParams,
  assertQueryParamsAllCompatible,
};
