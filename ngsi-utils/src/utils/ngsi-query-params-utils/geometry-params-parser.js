/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {
  SPECIAL_VALUE_TYPES,
  GEOMETRY_QUERY_GEOMETRY_TYPES,
  GEOMETRY_QUERY_COORDS_MULTI_SEP,
  GEOMETRY_QUERY_GEOREL_TYPES,
  GEOMETRY_QUERY_GEOREL_NEAR_MODIFIERS,
  GEOMETRY_QUERY_GEOREL_MODIFIER_SEP,
  GEOMETRY_QUERY_GEOREL_MODIFIER_ASSIGNMENT_SEP,
} = require('@ratatosk/ngsi-constants');

const {generateErrors} = require('../../libs/ngsi-error-generator');
const {
  simpleCoordPairParser: {parseSimpleCoordPair, serializeSimpleCoordPair},
} = require('../ngsi-location-utils');

const errorTemplates = {
  INVALID_GEOMETRY: 'Invalid geometry value. Must be one of [point, box, line, polygon]',
  INVALID_COORDS_ARG: 'Invalid coords arg. Must be a non-zero string',
  INVALID_NUM_COORD_PAIRS_POINT: 'Invalid number of coordinates for a "point" - must be exactly 1',
  INVALID_NUM_COORD_PAIRS_BOX: 'Invalid number of coordinates for a "box" - must be exactly 2',
  INVALID_NUM_COORD_PAIRS_LINE: 'Invalid number of coordinates for a "line" - must be at least 2',
  INVALID_NUM_COORD_PAIRS_POLYGON: 'Invalid number of coordinates for a "polygon" - must be at least 4',
  INVALID_SIMPLE_POLYGON_CLOSURE: 'A polygon must have the same coordinates at the start and end',
  INVALID_GEOREL_ARG: 'georel must be a string',
  INVALID_GEOREL_TYPE: 'the georel type must be one allowed by specification (e.g. "near" or "coveredBy")',
  INVALID_GEOREL_MODIFIER: 'only the georel "near" type can have a modifier',
  INVALID_GEOREL_NEAR_MODIFIER: 'the georel "near" type must have exactly one modifier (with modifier type and distance)',
  INVALID_GEOREL_NEAR_GEOMETRY: 'invalid geometry for georel "near" (must be a point)',
  INVALID_GEOREL_COVEREDBY_GEOMETRY: 'invalid geometry for georel "coveredBy" (must be a polygon)',
};
const errors = generateErrors('NGSI_GEOMETRY_PARAMS_PARSER_ERROR', errorTemplates);

const parseGeometryAndCoordsParam = (geometry, coords) => {
  // check the basic args
  if (!_.includes(GEOMETRY_QUERY_GEOMETRY_TYPES, geometry)) {
    throw errors.INVALID_GEOMETRY();
  }
  if (!_.isString(coords) || !coords.length) {
    throw errors.INVALID_COORDS_ARG();
  }
  // make sure that we can parse the coords
  const coordPairs = coords.split(GEOMETRY_QUERY_COORDS_MULTI_SEP);
  const validatedCoords = _.map(coordPairs, (pair) => {
    return serializeSimpleCoordPair(parseSimpleCoordPair(pair));
  });
  // make sure we have a valid simple geometry
  switch (geometry) {
    case GEOMETRY_QUERY_GEOMETRY_TYPES.POINT: {
      if (validatedCoords.length !== 1) {
        throw errors.INVALID_NUM_COORD_PAIRS_POINT();
      }
      return {
        type: SPECIAL_VALUE_TYPES.GEO_POINT,
        value: validatedCoords[0],
      };
    }
    case GEOMETRY_QUERY_GEOMETRY_TYPES.BOX: {
      if (validatedCoords.length !== 2) {
        throw errors.INVALID_NUM_COORD_PAIRS_BOX();
      }
      return {
        type: SPECIAL_VALUE_TYPES.GEO_BOX,
        value: validatedCoords,
      };
    }
    case GEOMETRY_QUERY_GEOMETRY_TYPES.LINE: {
      if (validatedCoords.length < 2) {
        throw errors.INVALID_NUM_COORD_PAIRS_LINE();
      }
      return {
        type: SPECIAL_VALUE_TYPES.GEO_LINE,
        value: validatedCoords,
      };
    }
    case GEOMETRY_QUERY_GEOMETRY_TYPES.POLYGON: {
      if (validatedCoords.length < 4) {
        throw errors.INVALID_NUM_COORD_PAIRS_POLYGON();
      }
      const first = validatedCoords[0];
      const last = validatedCoords[validatedCoords.length - 1];
      if (!_.isEqual(first, last)) {
        throw errors.INVALID_SIMPLE_POLYGON_CLOSURE();
      }
      return {
        type: SPECIAL_VALUE_TYPES.GEO_POLYGON,
        value: validatedCoords,
      };
    }
    default: {
      throw new Error('DevErr: this geometry list should be exhaustive');
    }
  }
};

const parseGeorelParam = (georelArg, geometry) => {
  if (!_.isString(georelArg)) {
    throw errors.INVALID_GEOREL_ARG();
  }
  if (!_.includes(GEOMETRY_QUERY_GEOMETRY_TYPES, geometry)) {
    throw errors.INVALID_GEOMETRY();
  }
  const parts = georelArg.split(GEOMETRY_QUERY_GEOREL_MODIFIER_SEP);
  const georel = parts[0];
  if (georel === GEOMETRY_QUERY_GEOREL_TYPES.NEAR && geometry !== GEOMETRY_QUERY_GEOMETRY_TYPES.POINT) {
    throw errors.INVALID_GEOREL_NEAR_GEOMETRY();
  }
  if (georel === GEOMETRY_QUERY_GEOREL_TYPES.COVERED_BY && geometry !== GEOMETRY_QUERY_GEOMETRY_TYPES.POLYGON) {
    throw errors.INVALID_GEOREL_COVEREDBY_GEOMETRY();
  }
  if (!_.includes(GEOMETRY_QUERY_GEOREL_TYPES, georel)) {
    throw errors.INVALID_GEOREL_TYPE();
  }
  if (georel !== GEOMETRY_QUERY_GEOREL_TYPES.NEAR) {
    if (parts.length !== 1) {
      throw errors.INVALID_GEOREL_MODIFIER();
    }
    return {georel};
  }
  // otherwise, for "near", parse the modifier
  if (parts.length !== 2) {
    throw errors.INVALID_GEOREL_NEAR_MODIFIER();
  }
  const modifierParts = parts[1].split(GEOMETRY_QUERY_GEOREL_MODIFIER_ASSIGNMENT_SEP);
  if (modifierParts.length !== 2) {
    throw errors.INVALID_GEOREL_NEAR_MODIFIER();
  }
  const modifierType = modifierParts[0];
  const modifierDistance = modifierParts[1];
  const hasValidModifierType = _.includes(GEOMETRY_QUERY_GEOREL_NEAR_MODIFIERS, modifierType);
  const hasValidDistance = !isNaN(modifierDistance) && _.toNumber(modifierDistance) >= 0;
  if (!hasValidModifierType || !hasValidDistance) {
    throw errors.INVALID_GEOREL_NEAR_MODIFIER();
  }
  return {georel, [modifierType]: _.toNumber(modifierDistance)};
};

module.exports = {
  errors,
  parseGeometryAndCoordsParam,
  parseGeorelParam,
};
