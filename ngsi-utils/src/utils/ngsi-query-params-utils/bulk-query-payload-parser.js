/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {
  BULK_QUERY_PAYLOAD_KEYS,
  BULK_QUERY_PAYLOAD_ENTITIES_KEYS,
  BULK_QUERY_PAYLOAD_EXPRESSION_KEYS,
} = require('@ratatosk/ngsi-constants');

const {
  NGSI_SQL_QUERY_TYPES,
} = require('../../constants');

const {generateErrors} = require('../../libs/ngsi-error-generator');

const {
  queryParser: {
    serializeQueryList,
    validateQueryString,
  },
} = require('../ngsi-sql');

const {
  validateAttrsList,
  parseAttrsParam,
} = require('./attrs-param-parser');
const {
  validateMetadataList,
  parseMetadataParam,
} = require('./metadata-param-parser');
const {
  validateEntityIdList,
  validateEntityIdParam,
  validateEntityTypeList,
  validateEntityTypeParam,
  validateIdPatternParam,
  validateTypePatternParam,
} = require('./basic-param-parser');

const errorTemplates = {
  INVALID_BULK_QUERY_PAYLOAD_ARG: 'The bulk query payload arg must be an object',
  EXTRANEOUS_PAYLOAD_FIELDS: 'Invalid extraneous payload field(s) present',
  INVALID_PAYLOAD_ENTITIES: 'The payload entities must be an array if present',
  INVALID_PAYLOAD_ENTITIES_ITEM: 'The payload entities item must be an object',
  INCOMPATIBLE_ENTITY_ID_PROPS: 'An entity cannot contain both "id" and "idPattern"',
  MISSING_ENTITY_ID_PROP: 'An entity MUST have either an "id" or "idPattern" property set',
  INCOMPATIBLE_ENTITY_TYPE_PROPS: 'An entity cannot contain both "type" and "typePattern"',
  INVALID_PAYLOAD_EXPRESSION: 'The expression must an object if present',
};
const errors = generateErrors('NGSI_BULK_QUERY_PAYLOAD_PARSER_ERROR', errorTemplates);


const formatBulkQueryEntity = (inputEntity) => {
  if (!_.isObject(inputEntity)) {
    throw errors.INVALID_PAYLOAD_ENTITIES_ITEM();
  }
  const keys = _.keys(inputEntity);
  const allowedKeys = _.values(BULK_QUERY_PAYLOAD_ENTITIES_KEYS);
  if (_.size(_.without(keys, ...allowedKeys))) {
    throw errors.INVALID_PAYLOAD_ENTITIES_ITEM();
  }


  const hasEntityId = _.has(inputEntity, BULK_QUERY_PAYLOAD_ENTITIES_KEYS.ENTITY_ID);
  const hasIdPattern = _.has(inputEntity, BULK_QUERY_PAYLOAD_ENTITIES_KEYS.ENTITY_ID_PATTERN);
  const hasEntityType = _.has(inputEntity, BULK_QUERY_PAYLOAD_ENTITIES_KEYS.ENTITY_TYPE);
  const hasTypePattern = _.has(inputEntity, BULK_QUERY_PAYLOAD_ENTITIES_KEYS.ENTITY_TYPE_PATTERN);

  // check param compatibility
  if (hasEntityId && hasIdPattern) {
    throw errors.INCOMPATIBLE_ENTITY_ID_PROPS();
  }
  if (!hasEntityId && !hasIdPattern) {
    throw errors.MISSING_ENTITY_ID_PROP();
  }
  if (hasEntityType && hasTypePattern) {
    throw errors.INCOMPATIBLE_ENTITY_TYPE_PROPS();
  }

  const outputEntity = {}; // this guy gets mutated throughout the rest
  // and format
  if (hasEntityId) {
    const entityId = inputEntity[BULK_QUERY_PAYLOAD_ENTITIES_KEYS.ENTITY_ID];
    if (_.isString(entityId)) {
      validateEntityIdParam(entityId);
    } else {
      validateEntityIdList(entityId);
    }
    outputEntity[BULK_QUERY_PAYLOAD_ENTITIES_KEYS.ENTITY_ID] = entityId;
  }

  if (hasEntityType) {
    const entityType = inputEntity[BULK_QUERY_PAYLOAD_ENTITIES_KEYS.ENTITY_TYPE];
    if (_.isString(entityType)) {
      validateEntityTypeParam(entityType);
    } else {
      validateEntityTypeList(entityType);
    }
    outputEntity[BULK_QUERY_PAYLOAD_ENTITIES_KEYS.ENTITY_TYPE] = entityType;
  }

  if (hasIdPattern) {
    const entityIdPattern = inputEntity[BULK_QUERY_PAYLOAD_ENTITIES_KEYS.ENTITY_ID_PATTERN];
    validateIdPatternParam(entityIdPattern);
    outputEntity[BULK_QUERY_PAYLOAD_ENTITIES_KEYS.ENTITY_ID_PATTERN] = entityIdPattern || null;
  }

  if (hasTypePattern) {
    const entityTypePattern = inputEntity[BULK_QUERY_PAYLOAD_ENTITIES_KEYS.ENTITY_TYPE_PATTERN];
    validateTypePatternParam(entityTypePattern);
    outputEntity[BULK_QUERY_PAYLOAD_ENTITIES_KEYS.ENTITY_TYPE_PATTERN] = entityTypePattern || null;
  }

  return outputEntity;
};

const formatBulkQueryExpression = (inputExpression) => {
  if (!_.isObject(inputExpression)) {
    throw errors.INVALID_PAYLOAD_EXPRESSION();
  }

  const outputExpression = {}; // this guy gets mutated throuhout

  if (_.has(inputExpression, BULK_QUERY_PAYLOAD_EXPRESSION_KEYS.ATTRIBUTE_QUERY)) {
    const attrQuery = inputExpression[BULK_QUERY_PAYLOAD_EXPRESSION_KEYS.ATTRIBUTE_QUERY];
    if (_.isString(attrQuery)) {
      validateQueryString(attrQuery, NGSI_SQL_QUERY_TYPES.ATTRIBUTE_QUERY);
      outputExpression[BULK_QUERY_PAYLOAD_EXPRESSION_KEYS.ATTRIBUTE_QUERY] = attrQuery;
    } else {
      outputExpression[BULK_QUERY_PAYLOAD_EXPRESSION_KEYS.ATTRIBUTE_QUERY] = serializeQueryList(attrQuery, NGSI_SQL_QUERY_TYPES.ATTRIBUTE_QUERY);
    }
  }

  if (_.has(inputExpression, BULK_QUERY_PAYLOAD_EXPRESSION_KEYS.METADATA_QUERY)) {
    const metadataQuery = inputExpression[BULK_QUERY_PAYLOAD_EXPRESSION_KEYS.METADATA_QUERY];
    if (_.isString(metadataQuery)) {
      validateQueryString(metadataQuery, NGSI_SQL_QUERY_TYPES.METADATA_QUERY);
      outputExpression[BULK_QUERY_PAYLOAD_EXPRESSION_KEYS.METADATA_QUERY] = metadataQuery;
    } else {
      outputExpression[BULK_QUERY_PAYLOAD_EXPRESSION_KEYS.METADATA_QUERY] = serializeQueryList(metadataQuery, NGSI_SQL_QUERY_TYPES.METADATA_QUERY);
    }
  }

  return _.size(outputExpression) ? outputExpression : null;
};

// MAIN EXPORT
const formatBulkQueryPayload = (inputPayload) => {
  // preamble
  if (_.isNil(inputPayload) || _.isEqual(inputPayload, {})) {
    return {};
  }
  if (!_.isObject(inputPayload)) {
    throw errors.INVALID_BULK_QUERY_PAYLOAD_ARG();
  }
  // check the keys
  const keys = _.keys(inputPayload);
  const allowedKeys = _.values(BULK_QUERY_PAYLOAD_KEYS);
  if (_.size(_.without(keys, ...allowedKeys))) {
    throw errors.EXTRANEOUS_PAYLOAD_FIELDS();
  }

  // constructing & validating the outputPayload object is the main exercise here
  const outputPayload = {}; // this guy gets mutated throughout

  if (_.has(inputPayload, BULK_QUERY_PAYLOAD_KEYS.ENTITIES)) {
    const entities = inputPayload[BULK_QUERY_PAYLOAD_KEYS.ENTITIES];
    if (!_.isArray(entities)) {
      throw errors.INVALID_PAYLOAD_ENTITIES();
    }
    const outputEntities = _.map(entities, (entity) => formatBulkQueryEntity(entity));
    outputPayload[BULK_QUERY_PAYLOAD_KEYS.ENTITIES] = outputEntities;
  }

  if (_.has(inputPayload, BULK_QUERY_PAYLOAD_KEYS.EXPRESSION)) {
    const expression = inputPayload[BULK_QUERY_PAYLOAD_KEYS.EXPRESSION];
    const outputExpression = formatBulkQueryExpression(expression);
    outputPayload[BULK_QUERY_PAYLOAD_KEYS.EXPRESSION] = outputExpression;
  }

  if (_.has(inputPayload, BULK_QUERY_PAYLOAD_KEYS.ATTRS_LIST)) {
    const attrs = inputPayload[BULK_QUERY_PAYLOAD_KEYS.ATTRS_LIST];
    if (_.isString(attrs)) {
      outputPayload[BULK_QUERY_PAYLOAD_KEYS.ATTRS_LIST] = parseAttrsParam(attrs);
    } else {
      validateAttrsList(attrs);
      outputPayload[BULK_QUERY_PAYLOAD_KEYS.ATTRS_LIST] = attrs;
    }
  }

  if (_.has(inputPayload, BULK_QUERY_PAYLOAD_KEYS.METADATA_LIST)) {
    const metadata = inputPayload[BULK_QUERY_PAYLOAD_KEYS.METADATA_LIST];
    if (_.isString(metadata)) {
      outputPayload[BULK_QUERY_PAYLOAD_KEYS.METADATA_LIST] = parseMetadataParam(metadata);
    } else {
      validateMetadataList(metadata);
      outputPayload[BULK_QUERY_PAYLOAD_KEYS.METADATA_LIST] = metadata;
    }
  }

  return outputPayload;
};

const validateBulkQueryPayload = (payload) => {
  formatBulkQueryPayload(payload); // just doesn't return anything
};

module.exports = {
  errors,
  formatBulkQueryPayload,
  validateBulkQueryPayload,
  // used elsewhere.. these should move??
  formatBulkQueryEntity,
};
