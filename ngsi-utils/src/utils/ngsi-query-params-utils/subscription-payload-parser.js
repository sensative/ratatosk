/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {
  ENTITY_REPRESENTATION_OPTIONS,
  SUBSCRIPTION_PAYLOAD_KEYS,
  SUBSCRIPTION_PAYLOAD_SUBJECT_KEYS,
  // SUBSCRIPTION_PAYLOAD_SUBJECT_CONDITION_KEYS,
  SUBSCRIPTION_PAYLOAD_NOTIFICATION_KEYS,
  SUBSCRIPTION_PAYLOAD_NOTIFICATION_HTTP_KEYS,
  // SUBSCRIPTION_PAYLOAD_NOTIFICATION_HTTP_CUSTOM_KEYS,
  SUBSCRIPTION_STATUS_VALUES,
} = require('@ratatosk/ngsi-constants');

const {generateErrors} = require('../../libs/ngsi-error-generator');

const {
  parseAttrsParam,
  validateAttrsList,
} = require('./attrs-param-parser');

const {
  parseMetadataParam,
  validateMetadataList,
} = require('./metadata-param-parser');

const {
  formatBulkQueryEntity, // THIS NEEDS REORGANIZING!!
} = require('./bulk-query-payload-parser');

const {
  hasValidDateTimeFormat,
} = require('../ngsi-date-time-utils');

const errorTemplates = {
  INVALID_SUBSCRIPTION_PAYLOAD_ARG: 'The subscription payload arg must be an object',
  EXTRANEOUS_SUBSCRIPTION_PAYLOAD_PROPS: 'Invalid extraneous subscription payload field(s) present',
  INVALID_SUBSCRIPTION_DESCRIPTION: 'The subscription description must be a string',
  INVALID_SUBSCRIPTION_STATUS: 'Invalid subscription status',
  INVALID_SUBSCRIPTION_EXPIRATION: 'Invalid subscription expiration (must be ISO8601 if included)',
  INVALID_SUBSCRIPTION_SUBJECT_CONTAINER: 'The subscription subject must be an object',
  EXTRANEOUS_SUBSCRIPTION_SUBJECT_PROPS: 'Extraneous props found in subscription subject',
  INVALID_SUBSCRIPTION_SUBJECT_ENTITIES: 'The subscription.subject.entities must be a non-zero length array',
  INVALID_SUBJECT_ENTITY: 'Invalid subscription entity item (must follow specification)',
  INVALID_SUBSCRIPTION_NOTIFICATION_CONTAINER: 'The subscription payload arg must be an object',
  INVALID_SUBSCRIPTION_NOTIFICATION_PROPS: 'Extraneous props found in subscription nofification',
  INVALID_SUBSCRIPTION_NOTIFICATION_ATTRS_PROPS: 'Notification cannot contain both "attrs" and "exceptAttrs" props',
  INVALID_SUBSCRIPTION_NOTIFICATION_ATTRS: 'Notification attrs supplied are invalid',
  INVALID_SUBSCRIPTION_NOTIFICATION_ATTRS_BLACKLIST: 'Notification exeptAttrs supplied are invalid',
  INVALID_SUBSCRIPTION_NOTIFICATION_METADATA: 'Notification metadata supplied are invalid',
  INVALID_SUBSCRIPTION_NOTIFICATION_HTTP_PROPS_BOTH: 'Notification cannot contain both "http" and "httpCustom" props',
  INVALID_SUBSCRIPTION_NOTIFICATION_HTTP_PROPS_NEITHER: 'Notification must contain one of "http" or "httpCustom" props',
  INVALID_NOTIFICATION_HTTP_CONTAINER: 'the subscription.notification.http must be an object',
  INVALID_NOTIFICATION_HTTP_PROPS: 'invalid extraneous subscription notification http url props',
  INVALID_NOTIFICATION_HTTP_URL: 'invalid subscription notifaction http url',
  INVALID_SUBSCRIPTION_NOTIFICATION_ATTRS_FORMAT: 'Invalid notification attrs format',
};
const errors = generateErrors('NGSI_SUBSCRIPTION_PAYLOAD_PARSER_ERROR', errorTemplates);


const formatSubscriptionSubjectEntities = (inputEntities) => {
  if (!_.isArray(inputEntities) || !_.size(inputEntities)) {
    throw errors.INVALID_SUBSCRIPTION_SUBJECT_ENTITIES();
  }
  let outputEntities;
  try {
    outputEntities = _.map(inputEntities, (entity) => formatBulkQueryEntity(entity));
  } catch (err) {
    throw errors.INVALID_SUBJECT_ENTITY();
  }
  return outputEntities;
};

const formatSubscriptionSubject = (inputSubject) => {
  if (!_.isObject(inputSubject)) {
    throw errors.INVALID_SUBSCRIPTION_SUBJECT_CONTAINER();
  }
  const keys = _.keys(inputSubject);
  const allowedKeys = _.values(SUBSCRIPTION_PAYLOAD_SUBJECT_KEYS);
  if (_.size(_.without(keys, ...allowedKeys))) {
    throw errors.EXTRANEOUS_SUBSCRIPTION_SUBJECT_PROPS();
  }

  const outputSubject = {}; // this guy gets mutated throughout

  const inputEntities = inputSubject[SUBSCRIPTION_PAYLOAD_SUBJECT_KEYS.ENTITIES];
  const outputEntities = formatSubscriptionSubjectEntities(inputEntities);
  outputSubject[SUBSCRIPTION_PAYLOAD_SUBJECT_KEYS.ENTITIES] = outputEntities;

  const inputCondition = inputSubject[SUBSCRIPTION_PAYLOAD_SUBJECT_KEYS.CONDITION];
  if (!_.isUndefined(inputCondition)) {
    throw new Error('subscription.subject.condition not yet implemented');
  }

  return outputSubject;
};

const formatNotificationHttp = (inputHttp) => {
  if (!_.isObject(inputHttp)) {
    throw errors.INVALID_NOTIFICATION_HTTP_CONTAINER();
  }
  // check the keys
  const keys = _.keys(inputHttp);
  const allowedKeys = _.values(SUBSCRIPTION_PAYLOAD_NOTIFICATION_HTTP_KEYS);
  if (_.size(_.without(keys, ...allowedKeys))) {
    throw errors.INVALID_NOTIFICATION_HTTP_PROPS();
  }

  const outputHttp = {}; // this guy gets mutated throughout

  const url = inputHttp[SUBSCRIPTION_PAYLOAD_NOTIFICATION_HTTP_KEYS.URL];
  if (!_.isString(url) || !url.length) {
    throw errors.INVALID_NOTIFICATION_HTTP_URL();
  }
  outputHttp[SUBSCRIPTION_PAYLOAD_NOTIFICATION_HTTP_KEYS.URL] = url;

  return outputHttp;
};

const formatSubscriptionNotification = (inputNotification) => {
  if (!_.isObject(inputNotification)) {
    throw errors.INVALID_SUBSCRIPTION_NOTIFICATION_CONTAINER();
  }
  // check the keys
  const keys = _.keys(inputNotification);
  const allowedKeys = _.values(SUBSCRIPTION_PAYLOAD_NOTIFICATION_KEYS);
  if (_.size(_.without(keys, ...allowedKeys))) {
    throw errors.INVALID_SUBSCRIPTION_NOTIFICATION_PROPS();
  }

  // attrs and exceptAttrs cannot both be there..
  const hasAttrs = _.has(inputNotification, SUBSCRIPTION_PAYLOAD_NOTIFICATION_KEYS.ATTRS);
  const hasExceptAttrs = _.has(inputNotification, SUBSCRIPTION_PAYLOAD_NOTIFICATION_KEYS.EXCEPT_ATTRS);
  if (hasAttrs && hasExceptAttrs) {
    throw errors.INVALID_SUBSCRIPTION_NOTIFICATION_ATTRS_PROPS();
  }

  // http and httpCustom cannot both be there, but one of them must
  const hasHttp = _.has(inputNotification, SUBSCRIPTION_PAYLOAD_NOTIFICATION_KEYS.HTTP);
  const hasHttpCustom = _.has(inputNotification, SUBSCRIPTION_PAYLOAD_NOTIFICATION_KEYS.HTTP_CUSTOM);
  if (hasHttp && hasHttpCustom) {
    throw errors.INVALID_SUBSCRIPTION_NOTIFICATION_HTTP_PROPS_BOTH();
  }
  if (!hasHttp && !hasHttpCustom) {
    throw errors.INVALID_SUBSCRIPTION_NOTIFICATION_HTTP_PROPS_NEITHER();
  }

  const outputNotification = {}; // this guy gets mutated throughout

  if (_.has(inputNotification, SUBSCRIPTION_PAYLOAD_NOTIFICATION_KEYS.ATTRS)) {
    const inputAttrs = inputNotification[SUBSCRIPTION_PAYLOAD_NOTIFICATION_KEYS.ATTRS];
    let outputAttrs;
    try {
      if (_.isString(inputAttrs)) {
        outputAttrs = parseAttrsParam(inputAttrs);
      } else {
        validateAttrsList(inputAttrs);
        outputAttrs = inputAttrs;
      }
    } catch (err) {
      throw errors.INVALID_SUBSCRIPTION_NOTIFICATION_ATTRS();
    }
    outputNotification[SUBSCRIPTION_PAYLOAD_NOTIFICATION_KEYS.ATTRS] = outputAttrs;
  }

  if (_.has(inputNotification, SUBSCRIPTION_PAYLOAD_NOTIFICATION_KEYS.EXCEPT_ATTRS)) {
    const inputExceptAttrs = inputNotification[SUBSCRIPTION_PAYLOAD_NOTIFICATION_KEYS.EXCEPT_ATTRS];
    let outputExceptAttrs;
    try {
      if (_.isString(inputExceptAttrs)) {
        outputExceptAttrs = parseAttrsParam(inputExceptAttrs);
      } else {
        validateAttrsList(inputExceptAttrs);
        outputExceptAttrs = inputExceptAttrs;
      }
    } catch (err) {
      throw errors.INVALID_SUBSCRIPTION_NOTIFICATION_ATTRS_BLACKLIST();
    }
    outputNotification[SUBSCRIPTION_PAYLOAD_NOTIFICATION_KEYS.EXCEPT_ATTRS] = outputExceptAttrs;
  }

  if (_.has(inputNotification, SUBSCRIPTION_PAYLOAD_NOTIFICATION_KEYS.METADATA)) {
    const inputMetadata = inputNotification[SUBSCRIPTION_PAYLOAD_NOTIFICATION_KEYS.METADATA];
    let outputMetadata;
    try {
      if (_.isString(inputMetadata)) {
        outputMetadata = parseMetadataParam(inputMetadata);
      } else {
        validateMetadataList(inputMetadata);
        outputMetadata = inputMetadata;
      }
    } catch (err) {
      throw errors.INVALID_SUBSCRIPTION_NOTIFICATION_METADATA();
    }
    outputNotification[SUBSCRIPTION_PAYLOAD_NOTIFICATION_KEYS.METADATA] = outputMetadata;
  }

  if (_.has(inputNotification, SUBSCRIPTION_PAYLOAD_NOTIFICATION_KEYS.HTTP)) {
    const inputHttp = inputNotification[SUBSCRIPTION_PAYLOAD_NOTIFICATION_KEYS.HTTP];
    const outputHttp = formatNotificationHttp(inputHttp);
    outputNotification[SUBSCRIPTION_PAYLOAD_NOTIFICATION_KEYS.HTTP] = outputHttp;
  }

  if (_.has(inputNotification, SUBSCRIPTION_PAYLOAD_NOTIFICATION_KEYS.HTTP_CUSTOM)) {
    throw new Error('custom http not yet supported');
  }

  if (_.has(inputNotification, SUBSCRIPTION_PAYLOAD_NOTIFICATION_KEYS.ATTRS_FORMAT)) {
    const attrsFormat = inputNotification[SUBSCRIPTION_PAYLOAD_NOTIFICATION_KEYS.ATTRS_FORMAT];
    if (!_.includes(ENTITY_REPRESENTATION_OPTIONS, attrsFormat)) {
      throw errors.INVALID_SUBSCRIPTION_NOTIFICATION_ATTRS_FORMAT();
    }
    outputNotification[SUBSCRIPTION_PAYLOAD_NOTIFICATION_KEYS.ATTRS_FORMAT] = attrsFormat;
  }

  return outputNotification;
};

const formatSubscriptionPayload = (inputPayload) => {
  if (!_.isObject(inputPayload)) {
    throw errors.INVALID_SUBSCRIPTION_PAYLOAD_ARG();
  }
  // check the keys
  const keys = _.keys(inputPayload);
  const allowedKeys = _.values(SUBSCRIPTION_PAYLOAD_KEYS);
  if (_.size(_.without(keys, ...allowedKeys))) {
    throw errors.EXTRANEOUS_SUBSCRIPTION_PAYLOAD_PROPS();
  }

  const outputPayload = {}; // this guy gets mutated throughout

  const description = inputPayload[SUBSCRIPTION_PAYLOAD_KEYS.DESCRIPTION];
  if (!_.isUndefined(description)) {
    if (!_.isString(description)) {
      throw errors.INVALID_SUBSCRIPTION_DESCRIPTION();
    } else {
      outputPayload[SUBSCRIPTION_PAYLOAD_KEYS.DESCRIPTION] = description;
    }
  }

  const inputSubject = inputPayload[SUBSCRIPTION_PAYLOAD_KEYS.SUBJECT];
  const outputSubject = formatSubscriptionSubject(inputSubject);
  outputPayload[SUBSCRIPTION_PAYLOAD_KEYS.SUBJECT] = outputSubject;

  const inputNotification = inputPayload[SUBSCRIPTION_PAYLOAD_KEYS.NOTIFICATION];
  const outputNotification = formatSubscriptionNotification(inputNotification);
  outputPayload[SUBSCRIPTION_PAYLOAD_KEYS.NOTIFICATION] = outputNotification;

  const status = inputPayload[SUBSCRIPTION_PAYLOAD_KEYS.STATUS];
  if (!_.isUndefined(status)) {
    if (!_.includes(SUBSCRIPTION_STATUS_VALUES, status)) {
      throw errors.INVALID_SUBSCRIPTION_STATUS();
    } else {
      outputPayload[SUBSCRIPTION_PAYLOAD_KEYS.STATUS] = status;
    }
  }

  const expires = inputPayload[SUBSCRIPTION_PAYLOAD_KEYS.EXPIRES];
  if (!_.isUndefined(expires)) {
    if (!hasValidDateTimeFormat(expires)) {
      throw errors.INVALID_SUBSCRIPTION_EXPIRATION();
    } else {
      outputPayload[SUBSCRIPTION_PAYLOAD_KEYS.EXPIRES] = expires;
    }
  }

  const throttling = inputPayload[SUBSCRIPTION_PAYLOAD_KEYS.THROTTLING];
  if (!_.isUndefined(throttling)) {
    if (!_.isNumber(throttling) || throttling < 0) {
      throw errors.INVALID_SUBSCRIPTION_THROTTLING();
    } else {
      outputPayload[SUBSCRIPTION_PAYLOAD_KEYS.THROTTLING] = throttling;
    }
  }

  return outputPayload;
};

const validateSubscriptionPayload = (payload) => {
  formatSubscriptionPayload(payload); // just doesn't return anything
};

module.exports = {
  errors,
  formatSubscriptionPayload,
  validateSubscriptionPayload,
};
