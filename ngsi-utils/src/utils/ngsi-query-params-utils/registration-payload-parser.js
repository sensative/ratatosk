/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {
  REGISTRATION_PAYLOAD_KEYS,
  REGISTRATION_PROVIDER_KEYS,
  REGISTRATION_DATA_PROVIDED_KEYS,
  REGISTRATION_PROVIDER_HTTP_KEYS,
  REGISTRATION_FORWARDING_MODE_VALUES,
  REGISTRATION_STATUS_VALUES,
} = require('@ratatosk/ngsi-constants');

const {generateErrors} = require('../../libs/ngsi-error-generator');

const {
  parseAttrsParam,
  validateAttrsList,
} = require('./attrs-param-parser');

const {
  formatBulkQueryEntity, // THIS NEEDS REORGANIZING!!
} = require('./bulk-query-payload-parser');

const {
  hasValidDateTimeFormat,
} = require('../ngsi-date-time-utils');

const errorTemplates = {
  INVALID_REGISTRATION_PAYLOAD_ARG: 'The registration payload arg must be an object',
  EXTRANEOUS_REGISTRATION_PAYLOAD_PROPS: 'Invalid extraneous registration payload field(s) present',
  INVALID_REGISTRATION_DESCRIPTION: 'The registration description must be a string',
  INVALID_REGISTRATION_STATUS: 'Invalid registration status',
  INVALID_REGISTRATION_EXPIRATION: 'Invalid registration expiration (must be ISO8601 if included)',
  INVALID_REGISTRATION_PROVIDER_CONTAINER: 'The registration provider must be an object',
  EXTRANEOUS_REGISTRATION_PROVIDER_PROPS: 'Extraneous props found in registration provider',
  INVALID_PROVIDER_LEGACY_FORWARDING: 'legacyForwarding should be a boolean if included',
  INVALID_DATA_PROVIDED_ENTITIES: 'The registration dataProvided entities must be a non-zero length array',
  INVALID_DATA_PROVIDED_ENTITY: 'Invalid registration dataProvided entities item (must follow specification)',
  INVALID_DATA_PROVIDED_ATTRS: 'registration dataProvided attrs supplied are invalid',
  INVALID_REGISTRATION_DATA_PROVIDED_CONTAINER: 'The registration dataProvided must be an object',
  EXTRANEOUS_REGISTRATION_DATA_PROVIDED_PROPS: 'Extraneous props found in registration dataProvided',
  INVALID_PROVIDER_HTTP_CONTAINER: 'the registration.provider.http must be an object',
  INVALID_PROVIDER_HTTP_PROPS: 'invalid extraneous registration provider http url props',
  INVALID_REGISTRATION_HTTP_URL: 'invalid registration provider http url',
  INVALID_REGISTRATION_FORWARDING_MODE: 'invalid supportedForwardingMode value',
};
const errors = generateErrors('NGSI_REGISTRATION_PAYLOAD_PARSER_ERROR', errorTemplates);


const formatRegistrationProviderHttp = (inputHttp) => {
  if (!_.isObject(inputHttp)) {
    throw errors.INVALID_PROVIDER_HTTP_CONTAINER();
  }
  // check the keys
  const keys = _.keys(inputHttp);
  const allowedKeys = _.values(REGISTRATION_PROVIDER_HTTP_KEYS);
  if (_.size(_.without(keys, ...allowedKeys))) {
    throw errors.INVALID_PROVIDER_HTTP_PROPS();
  }

  const outputHttp = {}; // this guy gets mutated throughout

  const url = inputHttp[REGISTRATION_PROVIDER_HTTP_KEYS.URL];
  if (!_.isString(url) || !url.length) {
    throw errors.INVALID_REGISTRATION_HTTP_URL();
  }
  outputHttp[REGISTRATION_PROVIDER_HTTP_KEYS.URL] = url;

  return outputHttp;
};


const formatRegistrationProvider = (inputProvider) => {
  if (!_.isObject(inputProvider)) {
    throw errors.INVALID_REGISTRATION_PROVIDER_CONTAINER();
  }
  const keys = _.keys(inputProvider);
  const allowedKeys = _.values(REGISTRATION_PROVIDER_KEYS);
  if (_.size(_.without(keys, ...allowedKeys))) {
    throw errors.EXTRANEOUS_REGISTRATION_PROVIDER_PROPS();
  }

  const outputProvider = {}; // this guy gets mutated throughout

  const inputHttp = inputProvider[REGISTRATION_PROVIDER_KEYS.HTTP];
  const outputHttp = formatRegistrationProviderHttp(inputHttp);
  outputProvider[REGISTRATION_PROVIDER_KEYS.HTTP] = outputHttp;

  if (_.has(inputProvider, REGISTRATION_PROVIDER_KEYS.LEGACY_FORWARDING)) {
    const legacyForwarding = _.get(inputProvider, REGISTRATION_PROVIDER_KEYS.LEGACY_FORWARDING);
    if (!_.isBoolean(legacyForwarding)) {
      throw errors.INVALID_PROVIDER_LEGACY_FORWARDING();
    }
    outputProvider[REGISTRATION_PROVIDER_KEYS.LEGACY_FORWARDING] = legacyForwarding;
  }

  if (_.has(inputProvider, REGISTRATION_PROVIDER_KEYS.SUPPORTED_FORWARDING_MODES)) {
    const mode = _.get(inputProvider, REGISTRATION_PROVIDER_KEYS.SUPPORTED_FORWARDING_MODES);
    if (!_.includes(REGISTRATION_FORWARDING_MODE_VALUES, mode)) {
      throw errors.INVALID_REGISTRATION_FORWARDING_MODE();
    }
    outputProvider[REGISTRATION_PROVIDER_KEYS.SUPPORTED_FORWARDING_MODES] = mode;
  }

  return outputProvider;
};


const formatDataProvidedEntities = (inputEntities) => {
  if (!_.isArray(inputEntities) || !_.size(inputEntities)) {
    throw errors.INVALID_DATA_PROVIDED_ENTITIES();
  }
  let outputEntities;
  try {
    outputEntities = _.map(inputEntities, (entity) => formatBulkQueryEntity(entity));
  } catch (err) {
    throw errors.INVALID_DATA_PROVIDED_ENTITY();
  }
  return outputEntities;
};


const formatRegistrationDataProvided = (inputDataProvided) => {

  if (!_.isObject(inputDataProvided)) {
    throw errors.INVALID_REGISTRATION_DATA_PROVIDED_CONTAINER();
  }
  const keys = _.keys(inputDataProvided);
  const allowedKeys = _.values(REGISTRATION_DATA_PROVIDED_KEYS);
  if (_.size(_.without(keys, ...allowedKeys))) {
    throw errors.EXTRANEOUS_REGISTRATION_DATA_PROVIDED_PROPS();
  }

  const outputDataProvided = {}; // this guy gets mutated throughout

  const inputEntities = inputDataProvided[REGISTRATION_DATA_PROVIDED_KEYS.ENTITIES];
  const outputEntities = formatDataProvidedEntities(inputEntities);
  outputDataProvided[REGISTRATION_DATA_PROVIDED_KEYS.ENTITIES] = outputEntities;

  if (_.has(inputDataProvided, REGISTRATION_DATA_PROVIDED_KEYS.ATTRS)) {
    const inputAttrs = inputDataProvided[REGISTRATION_DATA_PROVIDED_KEYS.ATTRS];
    let outputAttrs;
    try {
      if (_.isString(inputAttrs)) {
        outputAttrs = parseAttrsParam(inputAttrs);
      } else {
        validateAttrsList(inputAttrs);
        outputAttrs = inputAttrs;
      }
    } catch (err) {
      throw errors.INVALID_DATA_PROVIDED_ATTRS();
    }
    outputDataProvided[REGISTRATION_DATA_PROVIDED_KEYS.ATTRS] = outputAttrs;
  }

  if (_.has(inputDataProvided, REGISTRATION_DATA_PROVIDED_KEYS.EXPRESSION)) {
    throw new Error('REGISTRATION dataProvided "expression" not yet implemented');
  }

  return outputDataProvided;
};


const formatRegistrationPayload = (inputPayload) => {
  if (!_.isObject(inputPayload)) {
    throw errors.INVALID_REGISTRATION_PAYLOAD_ARG();
  }
  // check the keys
  const keys = _.keys(inputPayload);
  const allowedKeys = _.values(REGISTRATION_PAYLOAD_KEYS);
  if (_.size(_.without(keys, ...allowedKeys))) {
    throw errors.EXTRANEOUS_REGISTRATION_PAYLOAD_PROPS();
  }

  const outputPayload = {}; // this guy gets mutated throughout

  const inputProvider = inputPayload[REGISTRATION_PAYLOAD_KEYS.PROVIDER];
  const outputProvider = formatRegistrationProvider(inputProvider);
  outputPayload[REGISTRATION_PAYLOAD_KEYS.PROVIDER] = outputProvider;

  const inputDataProvided = inputPayload[REGISTRATION_PAYLOAD_KEYS.DATA_PROVIDED];
  const outputDataProvided = formatRegistrationDataProvided(inputDataProvided);
  outputPayload[REGISTRATION_PAYLOAD_KEYS.DATA_PROVIDED] = outputDataProvided;

  const description = inputPayload[REGISTRATION_PAYLOAD_KEYS.DESCRIPTION];
  if (!_.isUndefined(description)) {
    if (!_.isString(description)) {
      throw errors.INVALID_REGISTRATION_DESCRIPTION();
    } else {
      outputPayload[REGISTRATION_PAYLOAD_KEYS.DESCRIPTION] = description;
    }
  }

  const status = inputPayload[REGISTRATION_PAYLOAD_KEYS.STATUS];
  if (!_.isUndefined(status)) {
    if (!_.includes(REGISTRATION_STATUS_VALUES, status)) {
      throw errors.INVALID_REGISTRATION_STATUS();
    } else {
      outputPayload[REGISTRATION_PAYLOAD_KEYS.STATUS] = status;
    }
  }

  const expires = inputPayload[REGISTRATION_PAYLOAD_KEYS.EXPIRES];
  if (!_.isUndefined(expires)) {
    if (!hasValidDateTimeFormat(expires)) {
      throw errors.INVALID_REGISTRATION_EXPIRATION();
    } else {
      outputPayload[REGISTRATION_PAYLOAD_KEYS.EXPIRES] = expires;
    }
  }

  return outputPayload;
};


const validateRegistrationPayload = (payload) => {
  formatRegistrationPayload(payload); // just doesn't return anything
};

module.exports = {
  errors,
  formatRegistrationPayload,
  validateRegistrationPayload,
};
