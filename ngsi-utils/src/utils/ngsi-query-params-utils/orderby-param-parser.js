/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {generateErrors} = require('../../libs/ngsi-error-generator');

const {
  validateAttributeName,
} = require('../ngsi-datum-validator');

const {
  PARAM_LIST_ITEM_GROUPER, // "'"
  PARAM_LIST_ITEM_SEPARATOR,
  ORDERBY_PARAM_NEGATOR,
  BUILTIN_ATTRIBUTES,
  ENTITY_ID_KEY,
  ENTITY_TYPE_KEY,
  GEO_DISTANCE_KEY,
} = require('@ratatosk/ngsi-constants');

const {
  tokenize,
} = require('../../libs/ngsi-tokenize');

// NgsiOrderbyParamError

const errorTemplates = {
  EMPTY_ORDERBY_PARAM: 'The orderby param must be a non-empty string if included',
  INVALID_ORDERBY_PARAM_ITEM: 'The orderby param contains an invalid item',
  INVALID_ORDERBY_DUPLICATES: 'Duplicates are not allowed in the orderby list',
  INVALID_ORDERBY_PARAM_FORMATTING: 'The orderby param is formatted incorrectly',
  INVALID_ORDERBY_LIST: 'The orderby list must be an array',
};
const errors = generateErrors('NGSI_ORDERBY_PARAM_ERROR', errorTemplates);

// local utils

const serializeOrderbyItem = (item) => {
  const preamble = item.isNegatory ? ORDERBY_PARAM_NEGATOR : '';
  const itemStr = `${preamble}${item.name}`;
  const requiresGrouping = itemStr.indexOf(PARAM_LIST_ITEM_SEPARATOR) > -1;
  if (requiresGrouping) {
    return `${PARAM_LIST_ITEM_GROUPER}${itemStr}${PARAM_LIST_ITEM_GROUPER}`;
  }
  return itemStr;
};

const validateOrderbyItem = (item) => {
  const builtins = _.map(BUILTIN_ATTRIBUTES, (attr) => attr.name);
  const exceptions = [
    BUILTIN_ATTRIBUTES,
    ENTITY_ID_KEY,
    ENTITY_TYPE_KEY,
    GEO_DISTANCE_KEY,
  ].concat(builtins);
  if (_.includes(exceptions, item.name)) {
    return; // these guys are ok
  }
  try {
    validateAttributeName(item.name);
  } catch (err) {
    throw errors.INVALID_ORDERBY_PARAM_ITEM();
  }
};

const assertNoDuplicates = (items) => {
  const names = _.map(items, (item) => item.name);
  if (_.uniq(names).length !== names.length) {
    throw errors.INVALID_ORDERBY_DUPLICATES();
  }
};

// and the exports

const parseOrderbyParam = (orderbyParam) => {
  // preliminary checks
  if (_.isNil(orderbyParam)) {
    return null;
  }
  if (!_.isString(orderbyParam) || !orderbyParam.length) {
    throw errors.EMPTY_ORDERBY_PARAM();
  }
  // parse the list
  let rawOrderbyList;
  try {
    rawOrderbyList = tokenize(orderbyParam, PARAM_LIST_ITEM_SEPARATOR, PARAM_LIST_ITEM_GROUPER);
  } catch (err) {
    throw errors.INVALID_ORDERBY_PARAM_FORMATTING();
  }
  // interpret as orderby items
  const items = _.map(rawOrderbyList, (rawItem) => {
    const isNegatory = rawItem.indexOf(ORDERBY_PARAM_NEGATOR) === 0;
    const name = isNegatory ? rawItem.substring(1) : rawItem;
    return {name, isNegatory};
  });
  // check for dupes
  assertNoDuplicates(items);
  // and the individual items
  _.each(items, (item) => validateOrderbyItem(item));
  return items;
};

const serializeOrderbyList = (orderbyList) => {
  // teh basics
  if (_.isNil(orderbyList)) {
    return null;
  }
  if (!_.isArray(orderbyList)) {
    throw errors.INVALID_ORDERBY_LIST();
  }
  if (!orderbyList.length) {
    return null;
  }
  // check for invalids and dupes
  _.each(orderbyList, (item) => validateOrderbyItem(item));
  assertNoDuplicates(orderbyList);
  // and serialize
  const serializedItems = _.map(orderbyList, (item) => serializeOrderbyItem(item));
  const orderbyParam = serializedItems.join(PARAM_LIST_ITEM_SEPARATOR);
  return orderbyParam;
};

const validateOrderbyParam = (orderbyParam) => {
  parseOrderbyParam(orderbyParam);
};

const validateOrderbyList = (orderbyList) => {
  serializeOrderbyList(orderbyList);
};

module.exports = {
  errors,
  parseOrderbyParam,
  serializeOrderbyList,
  validateOrderbyParam,
  validateOrderbyList,
};
