/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

// the individual param parsers
const basicParamParser = require('./basic-param-parser');
const attrsParamParser = require('./attrs-param-parser');
const metadataParamParser = require('./metadata-param-parser');
const optionsParamParser = require('./options-param-parser');
const orderbyParamParser = require('./orderby-param-parser');
const geometryParamsParser = require('./geometry-params-parser');
// and the agglomerates that pulls them together
const queryParamsParser = require('./query-params-parser');
const bulkQueryPayloadParser = require('./bulk-query-payload-parser');
const bulkUpdatePayloadParser = require('./bulk-update-payload-parser');
const subscriptionPayloadParser = require('./subscription-payload-parser');
const registrationPayloadParser = require('./registration-payload-parser');

module.exports = {
  basicParamParser,
  attrsParamParser,
  metadataParamParser,
  optionsParamParser,
  orderbyParamParser,
  geometryParamsParser,
  // main exports
  queryParamsParser,
  bulkQueryPayloadParser,
  bulkUpdatePayloadParser,
  subscriptionPayloadParser,
  registrationPayloadParser,
};
