/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {
  BUILTIN_ATTRIBUTES,
  QUERY_ATTRIBUTE_LIST_WILDCARD,
  PARAM_LIST_ITEM_GROUPER,
  PARAM_LIST_ITEM_SEPARATOR,
} = require('@ratatosk/ngsi-constants');

const {
  tokenize,
} = require('../../libs/ngsi-tokenize');

const {generateErrors} = require('../../libs/ngsi-error-generator');

const {
  validateAttributeName,
} = require('../ngsi-datum-validator');

const errorTemplates = {
  // parser specific errors
  EMPTY_ATTRS_PARAM: 'The attrs param must be a non-empty string if included',
  INVALID_ATTRS_LIST_ITEM: 'Attrs list items must be either "*", a builtin attr, or generally a valid attribute name',
  INVALID_ATTRS_DUPLICATES: 'Duplicates are not allowed in attrs list',
  INVALID_ATTRS_FORMATTING: 'Attrs param is formatted incorrectly',
  INVALID_ATTRS_LIST: 'The attrsList must be an array',
};
const errors = generateErrors('NGSI_ATTRS_PARAM_ERROR', errorTemplates);

// /////////
// some tools for parsing the different comma-separated lists
// /////////

const wrapListItemInGroupers = (item) => {
  const requiresGrouping = item.indexOf(PARAM_LIST_ITEM_SEPARATOR) > -1;
  if (requiresGrouping) {
    return `${PARAM_LIST_ITEM_GROUPER}${item}${PARAM_LIST_ITEM_GROUPER}`;
  }
  return item;
};

const validateAttrsItem = (item) => {
  if (item === QUERY_ATTRIBUTE_LIST_WILDCARD || _.find(BUILTIN_ATTRIBUTES, {name: item})) {
    return; // wildcard and builtins allowed
  }

  try {
    validateAttributeName(item); // but they must be valid
  } catch (err) {
    throw errors.INVALID_ATTRS_LIST_ITEM();
  }
};

//
// attrs param stuff
//

const parseAttrsParam = (attrsParam) => {
  if (_.isNil(attrsParam)) {
    return [];
  }
  if (!_.isString(attrsParam) || !attrsParam.length) {
    throw errors.EMPTY_ATTRS_PARAM();
  }
  let attrsList;
  try {
    attrsList = tokenize(attrsParam, PARAM_LIST_ITEM_SEPARATOR, PARAM_LIST_ITEM_GROUPER);
  } catch (err) {
    throw errors.INVALID_ATTRS_FORMATTING();
  }
  if (_.uniq(attrsList).length !== attrsList.length) {
    throw errors.INVALID_ATTRS_DUPLICATES();
  }
  _.each(attrsList, (item) => validateAttrsItem(item));
  return attrsList; // DONE
};

const serializeAttrsList = (attrsList) => {
  if (_.isNil(attrsList)) {
    return null;
  }
  if (!_.isArray(attrsList)) {
    throw errors.INVALID_ATTRS_LIST();
  }
  if (!attrsList.length) {
    return null;
  }
  if (_.uniq(attrsList).length !== attrsList.length) {
    throw errors.INVALID_ATTRS_DUPLICATES();
  }
  _.each(attrsList, (item) => validateAttrsItem(item));
  const groupedList = _.map(attrsList, (item) => wrapListItemInGroupers(item));
  const attrsParam = groupedList.join(PARAM_LIST_ITEM_SEPARATOR);
  return attrsParam;
};

const validateAttrsParam = (attrs) => {
  parseAttrsParam(attrs);
};

const validateAttrsList = (attrsList) => {
  serializeAttrsList(attrsList);
};


module.exports = {
  errors,
  parseAttrsParam,
  serializeAttrsList,
  validateAttrsParam,
  validateAttrsList,
};
