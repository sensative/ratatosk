/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {
  BULK_UPDATE_PAYLOAD_KEYS,
  BULK_UPDATE_ACTION_TYPES,
} = require('@ratatosk/ngsi-constants');

const {formatEntity} = require('../ngsi-entity-formatter');
const {generateErrors} = require('../../libs/ngsi-error-generator');

const errorTemplates = {
  INVALID_BULK_UPDATE_PAYLOAD_ARG: 'The bulk update payload arg must be an object',
  EXTRANEOUS_UPDATE_PAYLOAD_FIELDS: 'Invalid extraneous bulk update payload field(s) present',
  INVALID_UPDATE_PAYLOAD_ENTITIES: 'The bulk update payload entities must be a non-zero array',
  INVALID_BULK_UPDATE_ENTITY_ITEM: 'Invalid bulk update entity was found',
  INVALID_BULK_UPDATE_ACTION_TYPE: 'Invalid bulk update actionType',
};
const errors = generateErrors('NGSI_BULK_UPDATE_PAYLOAD_PARSER_ERROR', errorTemplates);


const formatBulkUpdatePayload = (inputPayload) => {
  // preamble
  if (_.isNil(inputPayload) || _.isEqual(inputPayload, {})) {
    return {};
  }
  if (!_.isObject(inputPayload)) {
    throw errors.INVALID_BULK_UPDATE_PAYLOAD_ARG();
  }
  // check the keys
  const keys = _.keys(inputPayload);
  const allowedKeys = _.values(BULK_UPDATE_PAYLOAD_KEYS);
  if (_.size(_.without(keys, ...allowedKeys))) {
    throw errors.EXTRANEOUS_UPDATE_PAYLOAD_FIELDS();
  }

  // constructing & validating the outputPayload object is the main exercise here
  const outputPayload = {}; // this guy gets mutated throughout

  // ENTITIES
  const entities = inputPayload[BULK_UPDATE_PAYLOAD_KEYS.ENTITIES];
  if (!_.isArray(entities) || !entities.length) {
    throw errors.INVALID_UPDATE_PAYLOAD_ENTITIES();
  }
  let outputEntities;
  try {
    outputEntities = _.map(entities, (entity) => formatEntity(entity));
  } catch (err) {
    throw errors.INVALID_BULK_UPDATE_ENTITY_ITEM();
  }
  outputPayload[BULK_UPDATE_PAYLOAD_KEYS.ENTITIES] = outputEntities;

  // ACTION_TYPE
  const actionType = inputPayload[BULK_UPDATE_PAYLOAD_KEYS.ACTION_TYPE];
  if (!_.includes(BULK_UPDATE_ACTION_TYPES, actionType)) {
    throw errors.INVALID_BULK_UPDATE_ACTION_TYPE();
  }
  outputPayload[BULK_UPDATE_PAYLOAD_KEYS.ACTION_TYPE] = actionType;

  // DONE
  return outputPayload;
};

const validateBulkUpdatePayload = (payload) => {
  formatBulkUpdatePayload(payload); // just doesn't return anything
};

module.exports = {
  errors,
  formatBulkUpdatePayload,
  validateBulkUpdatePayload,
};
