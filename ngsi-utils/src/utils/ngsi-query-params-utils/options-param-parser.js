/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {generateErrors} = require('../../libs/ngsi-error-generator');

const {
  PARAM_LIST_ITEM_SEPARATOR, // ","
  OPTIONS_PARAM_APPEND, // "append"
  OPTIONS_PARAM_COUNT, // "count"
  OPTIONS_PARAM_UPSERT, // "upsert"
  ENTITY_REPRESENTATION_OPTIONS, // values, keyValues, unique
} = require('@ratatosk/ngsi-constants');

// the values [values, unique, keyValues] in some order
const representationTypes = _.values(ENTITY_REPRESENTATION_OPTIONS);
// the values [values, unique, keyValues count] in some order
const allPossibleOptionsValues = _.concat(
  OPTIONS_PARAM_APPEND,
  OPTIONS_PARAM_COUNT,
  OPTIONS_PARAM_UPSERT,
  representationTypes,
);

// some local constants
const defaultOptions = () => ({
  append: false,
  count: false,
  representationType: null,
  upsert: false,
});

const errorTemplates = {
  // parser specific errors
  INVALID_PARAM_SHAPE: 'The options param must be a non-empty string if included',
  EMPTY_PARAM_ITEM: 'Empty options param items are not allowed',
  DUPLICATE_PARAM_ITEM: 'Duplicate options param items are not allowed',
  INVALID_PARAM_ITEM: 'Invalid options param items are not allowed',
  MULTIPLE_REPRESENTATION_TYPE_ITEMS: 'Multiple representationType items are not allowed in the option param',
  // serializer specific errors
  INVALID_REPRESENTATION_TYPE: 'Cannot serialize options object: invalid representationType',
};
const errors = generateErrors('NGSI_OPTIONS_PARAM_ERROR', errorTemplates);

// I'm going to keep this for the sake of completeness & curiosity
// It can be useful to have something that emulates Orion behavior
const parseOptionsParamOrionStyle = (param) => {
  if (_.isNil(param)) {
    return defaultOptions();
  }
  // it must be a string of at least length 1
  if (!_.isString(param) || !param.length) {
    throw errors.INVALID_PARAM_SHAPE();
  }
  // BEGINNING OF NON-COMMON PART
  // first orion strips any preceding ','
  const strippedF = _.trimStart(param, PARAM_LIST_ITEM_SEPARATOR);
  // if ','s was all we had, then we're done
  if (strippedF.length === 0) {
    return defaultOptions();
  }
  // however, any trailing ',' throws an error
  const stripped = _.trimEnd(strippedF, PARAM_LIST_ITEM_SEPARATOR);
  if (stripped.length < strippedF.length) {
    throw errors.EMPTY_PARAM_ITEM();
  }
  // separate the strings and remove duplicates
  const parts = _.uniq(stripped.split(PARAM_LIST_ITEM_SEPARATOR));
  // the common parser
  return parseParts(parts);
};

const parseOptionsParam = (param) => {
  if (_.isNil(param)) {
    return defaultOptions();
  }
  // it must be a string of at least length 1
  if (!_.isString(param) || !param.length) {
    throw errors.INVALID_PARAM_SHAPE();
  }
  // split up the string
  const parts = param.split(PARAM_LIST_ITEM_SEPARATOR);
  // the common parser
  return parseParts(parts);
};

// NOT for export
const parseParts = (parts) => {
  // make sure we have no empties
  if (_.includes(parts, '')) {
    throw errors.EMPTY_PARAM_ITEM();
  }
  // make sure we have no duplicates
  if (parts.length !== _.uniq(parts).length) {
    throw errors.DUPLICATE_PARAM_ITEM();
  }
  // make sure each part is allowed/valid
  const invalidValue = _.find(parts, (part) => !_.includes(allPossibleOptionsValues, part));
  if (invalidValue) {
    throw errors.INVALID_PARAM_ITEM();
  }
  // make sure we do not have multiple displayOption values
  const representationTypeValues = _.filter(parts, (part) => _.includes(representationTypes, part));
  if (representationTypeValues.length > 1) {
    throw errors.MULTIPLE_REPRESENTATION_TYPE_ITEMS();
  }

  const hasAppend = _.includes(parts, OPTIONS_PARAM_APPEND);
  const hasCount = _.includes(parts, OPTIONS_PARAM_COUNT);
  const hasUpsert = _.includes(parts, OPTIONS_PARAM_UPSERT);

  // and construct the options object
  const parsedOptionsObj = {
    append: hasAppend,
    count: hasCount,
    upsert: hasUpsert,
    representationType: representationTypeValues.length ? representationTypeValues[0] : null,
  };
  return parsedOptionsObj;
};

const serializeOptionsParam = (param) => {
  // nil begets null..
  if (_.isNil(param)) {
    return null;
  }
  // extract the values
  const append = !!_.get(param, 'append');
  const count = !!_.get(param, 'count');
  const representationType = _.get(param, 'representationType');
  // if everything is falsy, then send back null as well
  if (!append && !count && _.isNil(representationType)) {
    return null;
  }
  // check if the rep type is valid
  if (!_.isNil(representationType) && !_.includes(representationTypes, representationType)) {
    throw errors.INVALID_REPRESENTATION_TYPE();
  }
  // and generate the return value
  const items = [];
  if (append) {
    items.push(OPTIONS_PARAM_APPEND);
  }
  if (count) {
    items.push(OPTIONS_PARAM_COUNT);
  }
  if (representationType) {
    items.push(representationType);
  }
  const serializedOptionsParam = items.join(PARAM_LIST_ITEM_SEPARATOR);
  return serializedOptionsParam;
};

// this guy can combine multiple versions of both serialized and parsed options params into one
// if multiple values are supplied through different channels, then the last one 'survives', no
// error gets thrown
const combineMultipleOptionsArgs = (...args) => {
  // make sure they all become objecty with an appropriate shape
  const massagedArgs = _.map(args, (arg) => {
    if (_.isString(arg)) {
      const parsedArg = parseOptionsParam(arg);
      const appendPart = parsedArg.append ? _.pick(parsedArg, 'append') : {};
      const countPart = parsedArg.count ? _.pick(parsedArg, 'count') : {};
      const repTypePart = !_.isNil(parsedArg.representationType) ? _.pick(parsedArg, 'representationType') : {};
      const massagedArg = _.assign({}, appendPart, countPart, repTypePart);
      return massagedArg;
    } else {
      const parsedArg = parseOptionsParam(serializeOptionsParam(arg));
      const appendPart = !_.isNil(_.get(arg, 'append')) ? _.pick(parsedArg, 'append') : {};
      const countPart = !_.isNil(_.get(arg, 'count')) ? _.pick(parsedArg, 'count') : {};
      const repTypePart = !_.isNil(parsedArg.representationType) ? _.pick(parsedArg, 'representationType') : {};
      const massagedArg = _.assign({}, appendPart, countPart, repTypePart);
      return massagedArg;
    }
  });
  // combine
  const combinedParsedParam = _.assign(defaultOptions(), ...massagedArgs);
  // and serialize the result
  return serializeOptionsParam(combinedParsedParam);
};

module.exports = {
  errors,
  parseOptionsParamOrionStyle,
  parseOptionsParam,
  serializeOptionsParam,
  combineMultipleOptionsArgs,
};
