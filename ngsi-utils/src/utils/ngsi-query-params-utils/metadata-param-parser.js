/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {
  BUILTIN_METADATA,
  QUERY_ATTRIBUTE_LIST_WILDCARD,
  PARAM_LIST_ITEM_GROUPER,
  PARAM_LIST_ITEM_SEPARATOR,
} = require('@ratatosk/ngsi-constants');

const {tokenize} = require('../../libs/ngsi-tokenize');
const {generateErrors} = require('../../libs/ngsi-error-generator');

const {
  validateMetadataName,
} = require('../ngsi-datum-validator');

const errorTemplates = {
  // parser specific errors
  EMPTY_METADATA_PARAM: 'The metadata param must be a non-empty string if included',
  INVALID_METADATA_LIST_ITEM: 'Metadata list items must be either "*", a builtin metadatum, or generally a valid metadata name',
  INVALID_METADATA_DUPLICATES: 'Duplicates are not allowed in metadata list',
  INVALID_METADATA_FORMATTING: 'Metadata param is formatted incorrectly',
  INVALID_METADATA_LIST: 'The metadataList must be an array',
};
const errors = generateErrors('NGSI_METADATA_PARAM_ERROR', errorTemplates);

// /////////
// some tools for parsing the different comma-separated lists
// /////////

const wrapListItemInGroupers = (item) => {
  const requiresGrouping = item.indexOf(PARAM_LIST_ITEM_SEPARATOR) > -1;
  if (requiresGrouping) {
    return `${PARAM_LIST_ITEM_GROUPER}${item}${PARAM_LIST_ITEM_GROUPER}`;
  }
  return item;
};

const validateMetadataItem = (item) => {
  if (item === QUERY_ATTRIBUTE_LIST_WILDCARD || _.find(BUILTIN_METADATA, {name: item})) {
    return; // wildcard and builtins allowed
  }
  try {
    validateMetadataName(item); // but they must be valid
  } catch (err) {
    throw errors.INVALID_METADATA_LIST_ITEM();
  }
};

//
// metadata param stuff
//

const parseMetadataParam = (metadataParam) => {
  if (_.isNil(metadataParam)) {
    return [];
  }
  if (!_.isString(metadataParam) || !metadataParam.length) {
    throw errors.EMPTY_METADATA_PARAM();
  }
  let metadataList;
  try {
    metadataList = tokenize(metadataParam, PARAM_LIST_ITEM_SEPARATOR, PARAM_LIST_ITEM_GROUPER);
  } catch (err) {
    throw errors.INVALID_METADATA_FORMATTING();
  }
  if (_.uniq(metadataList).length !== metadataList.length) {
    throw errors.INVALID_METADATA_DUPLICATES();
  }
  _.each(metadataList, (item) => validateMetadataItem(item));
  return metadataList; // DONE
};

const serializeMetadataList = (metadataList) => {
  if (_.isNil(metadataList)) {
    return null;
  }
  if (!_.isArray(metadataList)) {
    throw errors.INVALID_METADATA_LIST();
  }
  if (!metadataList.length) {
    return null;
  }
  if (_.uniq(metadataList).length !== metadataList.length) {
    throw errors.INVALID_METADATA_DUPLICATES();
  }
  _.each(metadataList, (item) => validateMetadataItem(item));
  const groupedList = _.map(metadataList, (item) => wrapListItemInGroupers(item));
  const metadataParam = groupedList.join(PARAM_LIST_ITEM_SEPARATOR);
  return metadataParam;
};

const validateMetadataParam = (metadata) => {
  parseMetadataParam(metadata);
};

const validateMetadataList = (metadataList) => {
  serializeMetadataList(metadataList);
};


module.exports = {
  errors,
  parseMetadataParam,
  serializeMetadataList,
  validateMetadataParam,
  validateMetadataList,
};
