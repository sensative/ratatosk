/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {generateErrors} = require('../libs/ngsi-error-generator');

const {
  validateEntityId,
  inferTypeForGeneralValue,
  validateGeneralValue,
} = require('./ngsi-datum-validator');

const {
  DEFAULT_ENTITY_TYPE,
  ENTITY_REPRESENTATION_OPTIONS,
} = require('@ratatosk/ngsi-constants');

const errorTemplates = {
  INVALID_VALUE_TYPE: 'type must be a string (or falsy)',
  INVALID_METADATA_CONTAINER: 'If metadata is included, it must be an object',
  INVALID_PROPERTY_VALUE: 'Invalid general value: cannot infer type',
  INVALID_ATTRS_CONTAINER: 'attrs should be an object',
  INVALID_ATTRS_HAS_ID: 'attrs object should not have an "id" prop',
  INVALID_ATTRS_HAS_TYPE: 'attrs object should not have a "type" prop',
  INVALID_ENTITY_CONTAINER: 'entity must be an object',
  INVALID_ATTRIBUTE_SHAPE: 'attribute must be a JSON object, unless keyValues option is used',
};
const errors = generateErrors('NGSI_ENTITY_FORMATTER_ERROR', errorTemplates);


const inferTrueType = (value, explicitType) => {
  if (_.isUndefined(value)) {
    throw errors.INVALID_PROPERTY_VALUE();
  }
  if (explicitType && !_.isString(explicitType)) {
    throw errors.INVALID_VALUE_TYPE();
  }
  if (explicitType) {
    return explicitType;
  }
  try {
    validateGeneralValue(value); // from ngsi-datum-validator
  } catch (err) {
    throw errors.INVALID_PROPERTY_VALUE();
  }

  return inferTypeForGeneralValue(value);
};

const formatProp = (rawProp, inputRepresentation) => {
  switch (inputRepresentation) {
    case ENTITY_REPRESENTATION_OPTIONS.KEY_VALUES: {
      // infer the type
      return {
        type: inferTrueType(rawProp),
        value: rawProp,
      };
    }
    case ENTITY_REPRESENTATION_OPTIONS.NORMALIZED: {
      if (!_.has(rawProp, 'value')) {
        throw errors.INVALID_ATTRIBUTE_SHAPE();
      }
      return {
        type: inferTrueType(rawProp.value, rawProp.type),
        value: rawProp.value,
      };
    }
    default: {
      // if it looks like a formatted prop, then validate it (metadata is irrelevant)
      const isPreFormatted = _.has(rawProp, 'value') || _.has(rawProp, 'type');
      if (isPreFormatted) {
        return {
          type: inferTrueType(rawProp.value, rawProp.type),
          value: rawProp.value,
        };
      }
      // otherwise infer the type
      return {
        type: inferTrueType(rawProp),
        value: rawProp,
      };
    }
  }
};

const formatAttribute = (raw, inputRepresentation) => {
  validateGeneralValue(raw);

  if (_.has(raw, 'metadata') && !_.isObject(raw.metadata)) {
    throw errors.INVALID_METADATA_CONTAINER();
  }
  // create a metadata object
  const metadata = _.mapValues(_.get(raw, 'metadata'), (md) => formatProp(md));
  // compose the parts
  const attr = _.assign(
    formatProp(raw, inputRepresentation),
    {metadata},
  );
  return attr;
};

const formatAttrs = (rawAttrs, inputRepresentation) => {
  if (!_.isObject(rawAttrs)) {
    throw errors.INVALID_ATTRS_CONTAINER();
  }
  if (_.has(rawAttrs, 'id')) {
    throw errors.INVALID_ATTRS_HAS_ID();
  }
  if (_.has(rawAttrs, 'type')) {
    throw errors.INVALID_ATTRS_HAS_TYPE();
  }
  return _.mapValues(rawAttrs, (attr) => formatAttribute(attr, inputRepresentation));
};

/**
 * Performs formatting into entity and user input validation
 */
const formatEntity = (rawEntity, inputRepresentation) => {
  if (!_.isObject(rawEntity)) {
    throw errors.INVALID_ENTITY_CONTAINER();
  }
  validateEntityId(rawEntity.id);
  const basicEntity = {
    id: rawEntity.id,
    type: _.isNil(rawEntity.type) ? DEFAULT_ENTITY_TYPE : rawEntity.type,
  };
  const rawAttributes = _.omit(rawEntity, ['id', 'type']);
  const formattedAttrs = formatAttrs(rawAttributes, inputRepresentation);
  return _.assign(
    basicEntity,
    formattedAttrs,
  );
};

module.exports = {
  errors,
  formatProp,
  formatAttribute,
  formatAttrs,
  formatEntity,
};
