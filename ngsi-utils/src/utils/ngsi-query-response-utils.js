/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {
  DEFAULT_PAGE_SIZE,
  REST_RESPONSE_HEADER_KEYS: {
    TOTAL_COUNT,
  },
  ENTITY_REQUEST_PARAM_KEYS: {
    PAGE_OFFSET,
    PAGE_LIMIT,
  },
} = require('@ratatosk/ngsi-constants');

const getResponseCount = (resObj) => {
  // We expect something like: {'fiware-total-count': 203}
  //             OR: {headers: {'fiware-total-count': 203}}
  const count = _.get(
    resObj,
    ['headers', TOTAL_COUNT],
    _.get(resObj, TOTAL_COUNT, null), // null is the final default
  );
  if (_.isNil(count)) {
    return null;
  }
  return _.toSafeInteger(count);
};

const getResponseOffset = (resObj) => {
  // We expect something like: {'offset': 27}
  //              OR: {params: {'offset': 27}}
  //     OR: {config: {params: {'offset': 27}}}
  const offset = _.get(
    resObj,
    ['config', 'params', PAGE_OFFSET],
    _.get(
      resObj,
      ['params', PAGE_OFFSET],
      _.get(resObj, PAGE_OFFSET, null), // null is the final default
    ),
  );
  return _.toSafeInteger(offset || 0);
};

const getResponseLimit = (resObj) => {

  // We expect something like: {'limit': 23}
  //              OR: {params: {'limit': 23}}
  //     OR: {config: {params: {'limit': 23}}}
  const limit = _.get(
    resObj,
    ['config', 'params', PAGE_LIMIT],
    _.get(
      resObj,
      ['params', PAGE_LIMIT],
      _.get(resObj, PAGE_LIMIT, null), // null is the final default
    ),
  );
  return _.toSafeInteger(limit) || DEFAULT_PAGE_SIZE;
};

const getNumDocs = (restResponse) => {
  return _.size(_.get(restResponse, 'data'));
};

const createPaginator = (restResponse) => {
  // input
  const headers = _.get(restResponse, 'headers');
  const config = _.get(restResponse, 'config');
  const docs = _.get(restResponse, 'data');
  // output
  const count = getResponseCount(headers);
  const offset = getResponseOffset(config);
  const limit = getResponseLimit(config);
  const numDocs = getNumDocs(restResponse);
  const page = Math.floor(offset / limit);
  const pageOffset = offset - page * limit;
  const isFull = _.size(docs) === limit;
  const nextOffset = offset + limit;
  const hasNextPage = isFull && (nextOffset < count);

  return {
    count,
    offset,
    limit,
    docs,
    numDocs,
    isFull,
    page,
    pageOffset,
    nextOffset,
    hasNextPage,
  };

};

module.exports = {
  getResponseCount,
  getResponseOffset,
  getResponseLimit,
  getNumDocs,
  createPaginator,
};
