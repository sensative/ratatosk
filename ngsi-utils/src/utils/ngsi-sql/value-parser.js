/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {
  INVALID_BINARY_OPERATOR,
  UNKNOWN_VALUE_TYPE,
  MATCH_PATTERN_REQUIRES_REGEXP,
  INVALID_MULTI_VALUE_OPERATOR,
  INVALID_COMPARISON_VALUE_LT,
  INVALID_COMPARISON_VALUE_LTE,
  INVALID_COMPARISON_VALUE_GT,
  INVALID_COMPARISON_VALUE_GTE,
  INVALID_LIST_ITEM,
  INVALID_RANGE_ITEM,
  INVALID_RANGE_INCONSISTENT_MIN_MAX_TYPES,
  INVALID_RANGE_CONSTRUCTION,
  INVALID_MULTI_VALUE_SINGLE_GROUPER,
  INVALID_MULTI_VALUE_GROUPING,
} = require('./errors');

const {
  detailedTokenize,
  errors: tokenizeErrors,
} = require('../../libs/ngsi-tokenize.js');

const {
  hasValidDateTimeFormat,
  toDate,
  fromDate,
} = require('../ngsi-date-time-utils');

const {
  QUERY_BINARY_OPERATORS,
  QUERY_VALUE_LIST_GROUPER_CHAR,
  QUERY_VALUE_LIST_SEPARATOR,
  QUERY_VALUE_RANGE_SEPARATOR,
} = require('@ratatosk/ngsi-constants');

const equalityOperators = [
  QUERY_BINARY_OPERATORS.EQUALS,
  QUERY_BINARY_OPERATORS.COLON_EQUALS,
  QUERY_BINARY_OPERATORS.UNEQUAL,
];
const comparisonOperators = [
  QUERY_BINARY_OPERATORS.LESS_THAN,
  QUERY_BINARY_OPERATORS.LESS_THAN_OR_EQUALS,
  QUERY_BINARY_OPERATORS.GREATER_THAN,
  QUERY_BINARY_OPERATORS.GREATER_THAN_OR_EQUALS,
];

const valueTypes = {
  NULL: 'NULL',
  BOOLEAN: 'BOOLEAN',
  NUMBER: 'NUMBER',
  DATE: 'DATE',
  STRING: 'STRING',
  RANGE: 'RANGE',
  LIST: 'LIST',
};

const determineValueType = (value) => {
  if (_.isNull(value)) {
    return valueTypes.NULL;
  }
  if (_.isBoolean(value)) {
    return valueTypes.BOOLEAN;
  }
  if (_.isNumber(value)) {
    return valueTypes.NUMBER;
  }
  if (_.isString(value)) {
    return valueTypes.STRING;
  }
  if (_.isDate(value)) {
    return valueTypes.DATE;
  }
  if (_.isArray(value)) {
    return valueTypes.LIST;
  }
  if (_.isObject(value) && _.has(value, 'min') && _.has(value, 'max')) {
    return valueTypes.RANGE;
  }
  throw UNKNOWN_VALUE_TYPE();
};

const hasOuterQuotes = (str) => {
  if (str.length < 2) {
    return false;
  }
  return str === `${QUERY_VALUE_LIST_GROUPER_CHAR}${str.slice(1, str.length - 1)}${QUERY_VALUE_LIST_GROUPER_CHAR}`;
};

const hasInnerQuotes = (str) => {
  const unwrapped = hasOuterQuotes(str) ? str.slice(1, str.length - 1) : str;
  return unwrapped.indexOf(QUERY_VALUE_LIST_GROUPER_CHAR) > -1;
};

const stringRequiresWrapping = (str) => {
  const isSpecial = _.includes(['null', 'true', 'false'], str);
  const isNumeric = (str !== '') && !isNaN(str);
  const isDate = hasValidDateTimeFormat(str);
  const hasQuotes = str.indexOf(QUERY_VALUE_LIST_GROUPER_CHAR) > -1;
  const hasRanges = str.indexOf(QUERY_VALUE_RANGE_SEPARATOR) > -1;
  return isSpecial || isNumeric || isDate || hasQuotes || hasRanges;
};

const validateList = (items) => {
  if (determineValueType(items) !== valueTypes.LIST) {
    throw new Error('DevErr: only lists (i.e. arrays) should get in here');
  }
  const allowedTypes = _.pick(valueTypes, ['NULL', 'BOOLEAN', 'NUMBER', 'STRING', 'DATE']);
  _.each(items, (item) => {
    const itemType = determineValueType(item);
    if (!_.includes(allowedTypes, itemType)) {
      throw INVALID_LIST_ITEM();
    }
  });
};

const validateRange = (range) => {
  if (determineValueType(range) !== valueTypes.RANGE) {
    throw new Error('DevErr: only range items (i.e. {min, max}) should get in here');
  }
  const allowedTypes = _.pick(valueTypes, ['NUMBER', 'STRING', 'DATE']);
  const minType = determineValueType(range.min);
  const maxType = determineValueType(range.max);
  if (!_.includes(allowedTypes, minType)) {
    throw INVALID_RANGE_ITEM();
  }
  if (!_.includes(allowedTypes, maxType)) {
    throw INVALID_RANGE_ITEM();
  }
  if (minType !== maxType) {
    throw INVALID_RANGE_INCONSISTENT_MIN_MAX_TYPES();
  }
};

const validateFullValue = (value) => {
  const type = determineValueType(value);
  if (type === valueTypes.LIST) {
    validateList(value);
  }
  if (type === valueTypes.RANGE) {
    validateRange(value);
  }
};

const validateValueForOp = (value, op) => {
  validateFullValue(value);
  const valueType = determineValueType(value);
  // match pattern is easy to eliminate
  if (op === QUERY_BINARY_OPERATORS.MATCH_PATTERN) {
    if (valueType !== valueTypes.STRING) {
      throw MATCH_PATTERN_REQUIRES_REGEXP();
    }
    return;
  }
  // only the equalityOperators can handle arrays & ranges
  if (valueType === valueTypes.LIST || valueType === valueTypes.RANGE) {
    if (!_.includes(equalityOperators, op)) {
      throw INVALID_MULTI_VALUE_OPERATOR();
    }
    return;
  }
  // comparison operators can only handle numbers, strings, and dates
  if (_.includes(comparisonOperators, op)) {
    const allowedTypes = ['NUMBER', 'STRING', 'DATE'];
    if (!_.includes(allowedTypes, valueType)) {
      if (op === QUERY_BINARY_OPERATORS.LESS_THAN) {
        throw INVALID_COMPARISON_VALUE_LT();
      }
      if (op === QUERY_BINARY_OPERATORS.LESS_THAN_OR_EQUALS) {
        throw INVALID_COMPARISON_VALUE_LTE();
      }
      if (op === QUERY_BINARY_OPERATORS.GREATER_THAN) {
        throw INVALID_COMPARISON_VALUE_GT();
      }
      if (op === QUERY_BINARY_OPERATORS.GREATER_THAN_OR_EQUALS) {
        throw INVALID_COMPARISON_VALUE_GTE();
      }
      throw new Error('DevErr: there should be no other comparison operators');
    }
  }
};


const parseRawValueString = (valString) => {
  // try the simple ones suspects
  if (valString === '') {
    // should this throw an error for MATCH_PATTERN op?
    return ''; // isNaN('') === false
  }
  if (!isNaN(valString)) {
    return _.toNumber(valString);
  }
  if (valString === 'null') {
    return null;
  }
  if (valString === 'true') {
    return true;
  }
  if (valString === 'false') {
    return false;
  }
  // check to see if it's a forced string
  if (hasOuterQuotes(valString) && !hasInnerQuotes(valString)) {
    return valString.substring(1, valString.length - 1);
  }

  //
  // INTERPRETED OBJECTS & strings (as a defacto default)
  //

  // first check to see if it's a date
  if (hasValidDateTimeFormat(valString)) {
    return toDate(valString);
  }

  // attempt to parse as a range
  try {
    const rangeStack = detailedTokenize(valString, QUERY_VALUE_RANGE_SEPARATOR, QUERY_VALUE_LIST_GROUPER_CHAR);
    if (rangeStack.length > 2) {
      throw INVALID_RANGE_CONSTRUCTION();
    }
    if (rangeStack.length === 2) {
      const minItem = rangeStack[0];
      const maxItem = rangeStack[1];
      const min = minItem.isGrouped ? minItem.value : parseRawValueString(minItem.value);
      const max = maxItem.isGrouped ? maxItem.value : parseRawValueString(maxItem.value);
      return {min, max};
    }
  } catch (err) {
    if (err.isNgsiTokenizeError) {
      if (tokenizeErrors.INVALID_NUM_TOKENIZER_GROUPS.matches(err)) {
        // do nothing - it can be a list (and gets handled there)
      } else if (tokenizeErrors.INVALID_TOKENIZER_GROUPING.matches(err)) {
        // do nothing - it can be a list
      } else {
        throw new Error(`DevErr: Unhandled tokenizer error: `, err.name);
      }
    } else if (err.isNgsiSqlError) {
      throw err; // just pass it on
    } else {
      throw new Error(`DevErr: Invalid range-parsing error: ${err.message}`);
    }
  }

  // attempt to parse as a list
  try {
    const listStack = detailedTokenize(valString, QUERY_VALUE_LIST_SEPARATOR, QUERY_VALUE_LIST_GROUPER_CHAR);
    if (listStack.length > 1) {
      const list = _.map(listStack, (listItem) => {
        return listItem.isGrouped ? listItem.value : parseRawValueString(listItem.value);
      });
      return list;
    }
  } catch (err) {
    if (err.isNgsiTokenizeError) {
      if (tokenizeErrors.INVALID_NUM_TOKENIZER_GROUPS.matches(err)) {
        throw INVALID_MULTI_VALUE_SINGLE_GROUPER();
      } else if (tokenizeErrors.INVALID_TOKENIZER_GROUPING.matches(err)) {
        throw INVALID_MULTI_VALUE_GROUPING();
      } else {
        throw new Error(`DevErr: Unhandled tokenizer error: `, err.name);
      }
    } else {
      throw new Error(`DevErr: Invalid list-parsing error: ${err.message}`);
    }
  }

  // otherwise it's just a string
  return valString;
};

// //////////
//
//  Exports
//
// //////////


const validateBinaryOperator = (op) => {
  if (!_.includes(QUERY_BINARY_OPERATORS, op)) {
    throw INVALID_BINARY_OPERATOR();
  }
};


const parseValueString = (valString, op) => {
  validateBinaryOperator(op);
  // the default is to just return the raw string
  const value = parseRawValueString(valString, op);
  // do the rest of the validation
  validateValueForOp(value, op);
  // and done
  return value;
};

const serializeValue = (value, op) => {
  validateBinaryOperator(op);
  validateValueForOp(value, op);
  const valueType = determineValueType(value);
  if (valueType === valueTypes.NULL) {
    return 'null';
  }
  if (valueType === valueTypes.BOOLEAN) {
    return value ? 'true' : 'false';
  }
  if (valueType === valueTypes.NUMBER) {
    return `${value}`;
  }
  if (valueType === valueTypes.STRING) {
    return stringRequiresWrapping(value) ? `'${value}'` : value;
  }
  if (valueType === valueTypes.DATE) {
    return fromDate(value);
  }
  if (valueType === valueTypes.LIST) {
    const items = _.map(value, (item) => {
      const itemType = determineValueType(item);
      if (itemType === valueTypes.NULL) {
        return 'null';
      }
      if (itemType === valueTypes.BOOLEAN) {
        return item ? 'true' : 'false';
      }
      if (itemType === valueTypes.NUMBER) {
        return `${item}`;
      }
      if (itemType === valueTypes.STRING) {
        return stringRequiresWrapping(item) ? `'${item}'` : item;
      }
      if (itemType === valueTypes.DATE) {
        return fromDate(item);
      }
      throw new Error('DevErr: list validation is broken');
    });
    return items.join(QUERY_VALUE_LIST_SEPARATOR);
  }
  if (valueType === valueTypes.RANGE) {
    const parseExtremum = (extremum) => {
      const extremumType = determineValueType(extremum);
      if (extremumType === valueTypes.NUMBER) {
        return `${extremum}`;
      }
      if (extremumType === valueTypes.STRING) {
        return stringRequiresWrapping(extremum) ? `'${extremum}'` : extremum;
      }
      if (extremumType === valueTypes.DATE) {
        return fromDate(extremum);
      }
      throw new Error('DevErr: range validation is broken');
    };
    const range = _.mapValues(value, parseExtremum);
    return `${range.min}${QUERY_VALUE_RANGE_SEPARATOR}${range.max}`;
  }
  throw new Error('DevErr: serialization validation is broken');
};


const validateValueString = (valueString, op) => {
  parseValueString(valueString, op);
};

const validateValue = (value, op) => {
  validateBinaryOperator(op);
  validateValueForOp(value, op);
};

module.exports = {
  parseValueString,
  serializeValue,
  validateBinaryOperator,
  validateValueString,
  validateValue,
};
