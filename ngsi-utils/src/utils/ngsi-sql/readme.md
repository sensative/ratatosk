## ngsi v2: Simple Query Language

You can find the specification at: http://fiware.github.io/specifications/ngsiv2/stable/

This is a set of tools to parse sql (fiware Simple Query Language) and is composed of
 - **queryParser** (top guy)
 - **queryChopper** (used by queryParser)
 - **predicateParser** (used by queryParser)
 - **tokenParser** (used by predicateParser)
