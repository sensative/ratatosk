/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const {generateErrors} = require('../../libs/ngsi-error-generator');


const errorTemplates = {
  INVALID_QUERY_TYPE: 'invalid query type, (must be "q" or "mq")',
  CHOP_ERR_EMPTY_Q_PARAM: 'if included, a q-query param must be a non-zero string',
  CHOP_ERR_EMPTY_MQ_PARAM: 'if included, an mq-query param must be a non-zero string',
  GLUE_ERR_INVALID_SHAPE: 'predicateStatements must be an non-zero length array',
  GLUE_ERR_INVALID_ITEM: 'predicateStatements item must be a non-zero string',
  INVALID_SERIALIZE_ARG: 'the query serializer argument should be an array',
  INVALID_QUERY_OPERATOR_TYPE: 'invalid query operator type (should be "unary" or "binary")',
  INVALID_BINARY_SQL_OPERATOR: 'invalid binary sql operator',
  INVALID_TOKENS_SHAPE: 'tokens must be an array with length > 0',
  TOO_FEW_MQ_TOKENS: 'metadata queries must have at least 2 tokens',
  INVALID_TOKEN_TYPE: 'token item must be a string',
  INVALID_TOKENS_STRING: 'tokenString must a non-empty string',
  INVALID_NUM_TOKEN_GROUPS: 'token groupings (which can contain a non-pathy ".") must always come in pairs',
  INVALID_TOKEN_GROUPING: 'token groupings are malformed and token separators "." are missplaced',
  INVALID_ATTRIBUTE_TOKEN: 'invalid attribute token found in sql query',
  INVALID_METADATA_TOKEN: 'invalid metadata token found in sql metadata query',
  EMPTY_PREDICATE_STATEMENT: 'sql statement cannot be empty',
  // value-parser
  INVALID_BINARY_OPERATOR: 'Invalid binary operator',
  UNKNOWN_VALUE_TYPE: 'Unknown sql value type',
  MATCH_PATTERN_REQUIRES_REGEXP: 'The sql binary operator matchPattern takes ONLY string values to create a RegExp',
  INVALID_MULTI_VALUE_OPERATOR: 'Invalid operator for value - only equalityOperators (==, :, !=) can handle list and range values',
  INVALID_COMPARISON_VALUE_LT: 'Invalid value for "<" (less-than operator)',
  INVALID_COMPARISON_VALUE_LTE: 'Invalid value for "<=" (less-than-or-equal operator)',
  INVALID_COMPARISON_VALUE_GT: 'Invalid value for ">" (greater-than operator)',
  INVALID_COMPARISON_VALUE_GTE: 'Invalid value for ">=" (greater-than-or-equal operator)',
  INVALID_LIST_ITEM: 'Invalid list item. Only (numbers, strings, dates, booleans, null) are allowed',
  INVALID_RANGE_ITEM: 'Invalid range item. Only (numbers, strings, dates) are allowed',
  INVALID_RANGE_INCONSISTENT_MIN_MAX_TYPES: 'A range value must have {min, max} of same allowed type',
  INVALID_MATCH_PATTERN_VALUE: 'the value does not evaluate to a string, which is needed for the MatchPattern RegExp',
  INVALID_RANGE_CONSTRUCTION: 'a range must have the form XX..YY where XX and YY are individual values',
  INVALID_MULTI_VALUE_SINGLE_GROUPER: 'list-token grouping chars (i.e. the single-quote) must always come in pairs',
  INVALID_MULTI_VALUE_GROUPING: 'the formatting of groupers wrt separators for the multi-value is malformed',
};
const errors = generateErrors('NGSI_SQL_ERROR', errorTemplates);

module.exports = errors;
