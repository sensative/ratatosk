/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {validateQueryType} = require('./query-type');
const {parseTokens, serializeTokens} = require('./token-parser');
const {parseValueString, serializeValue} = require('./value-parser');

const {
  QUERY_BINARY_OPERATORS,
  QUERY_UNARY_OPERATORS: {NON_EXISTENCE},
} = require('@ratatosk/ngsi-constants');

const {
  NGSI_SQL_QUERY_TYPES,
  QUERY_PREDICATE_TYPES,
} = require('../../constants');

const {
  INVALID_QUERY_OPERATOR_TYPE,
  INVALID_BINARY_SQL_OPERATOR,
  EMPTY_PREDICATE_STATEMENT,
} = require('./errors');

// >= and <= need to be checked PRIOR TO > and <
const ORDERED_BINARY_OPERATORS = _.sortBy(QUERY_BINARY_OPERATORS, (op) => -op.length);
const extractBinaryPredicateParts = (statement) => {
  let parts = null;
  _.each(ORDERED_BINARY_OPERATORS, (op) => {
    const index = statement.indexOf(op);
    if (index > -1) {
      const [tokensString, valueString] = statement.split(op);
      parts = {op, tokensString, valueString};
      return false; // exit loop
    }
  });
  return parts;
};

const parseBinaryPredicate = (statement, queryType) => {
  const parts = extractBinaryPredicateParts(statement);
  if (!parts) {
    return null; // must be a unary operator
  }
  const tokens = parseTokens(parts.tokensString, queryType);
  const value = parseValueString(parts.valueString, parts.op);
  return {
    predicateType: QUERY_PREDICATE_TYPES.BINARY,
    tokens,
    op: parts.op,
    value,
  };
};

const parseUnaryPredicate = (statement, queryType) => {
  const isNegatory = statement.charAt(0) === NON_EXISTENCE;
  const tokensString = isNegatory ? statement.substring(1) : statement;
  const tokens = parseTokens(tokensString, queryType);
  return {
    predicateType: QUERY_PREDICATE_TYPES.UNARY,
    tokens,
    isNegatory,
  };
};

const parsePredicate = (statement, queryType = NGSI_SQL_QUERY_TYPES.ATTRIBUTE_QUERY) => {
  validateQueryType(queryType);
  if (!_.isString(statement) || !statement.length) {
    throw EMPTY_PREDICATE_STATEMENT();
  }
  const binaryPredicate = parseBinaryPredicate(statement, queryType);
  if (binaryPredicate) {
    return binaryPredicate;
  }
  const unaryPredicate = parseUnaryPredicate(statement, queryType);
  return unaryPredicate;
};

const serializePredicate = (predicate, queryType = NGSI_SQL_QUERY_TYPES.ATTRIBUTE_QUERY) => {
  validateQueryType(queryType);
  const tokenString = serializeTokens(_.get(predicate, 'tokens'), queryType);
  const predicateType = _.get(predicate, 'predicateType');
  switch (predicateType) {
    case QUERY_PREDICATE_TYPES.BINARY: {
      const op = _.get(predicate, 'op');
      if (!_.includes(QUERY_BINARY_OPERATORS, op)) {
        throw INVALID_BINARY_SQL_OPERATOR();
      }
      const value = _.get(predicate, 'value');
      const valueString = serializeValue(value, op);
      return `${tokenString}${op}${valueString}`;
    }
    case QUERY_PREDICATE_TYPES.UNARY: {
      if (predicate.isNegatory) {
        return `${NON_EXISTENCE}${tokenString}`;
      }
      return tokenString;
    }
    default: {
      throw INVALID_QUERY_OPERATOR_TYPE();
    }
  }
};

module.exports = {
  parsePredicate,
  serializePredicate,
};
