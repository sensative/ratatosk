/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {validateQueryType} = require('./query-type');
const {chop, glue} = require('./predicate-chopper');
const {parsePredicate, serializePredicate} = require('./predicate-parser');

const {NGSI_SQL_QUERY_TYPES} = require('../../constants');
const {
  INVALID_SERIALIZE_ARG,
} = require('./errors');

const parseQueryString = (queryString, queryType = NGSI_SQL_QUERY_TYPES.ATTRIBUTE_QUERY) => {
  validateQueryType(queryType);
  const statements = chop(queryString, queryType);
  const predicates = _.map(statements, (statement) => parsePredicate(statement, queryType));
  return predicates;
};

const serializeQueryList = (predicates, queryType = NGSI_SQL_QUERY_TYPES.ATTRIBUTE_QUERY) => {
  validateQueryType(queryType);
  if (!_.isArray(predicates)) {
    throw INVALID_SERIALIZE_ARG();
  }
  const statements = _.map(predicates, (predicate) => serializePredicate(predicate, queryType));
  const queryString = glue(statements);
  return queryString;
};

const validateQueryList = (queryList, queryType = NGSI_SQL_QUERY_TYPES.ATTRIBUTE_QUERY) => {
  const copy = parseQueryString(serializeQueryList(queryList, queryType), queryType);
  if (!_.isEqual(queryList, copy)) {
    throw new Error('DevErr: this round trip should be the identity op');
  }
};

const validateQueryString = (queryString, queryType = NGSI_SQL_QUERY_TYPES.ATTRIBUTE_QUERY) => {
  const queryList = parseQueryString(queryString, queryType);
  validateQueryList(queryList, queryType);
};

module.exports = {
  parseQueryString,
  serializeQueryList,
  validateQueryString,
  validateQueryList,
};
