/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const {
  tokenize,
  errors: tokenizeErrors,
} = require('../../libs/ngsi-tokenize');

const {
  QUERY_TOKEN_GROUPER_CHAR,
  QUERY_TOKEN_SEPARATOR,
} = require('@ratatosk/ngsi-constants');

const {NGSI_SQL_QUERY_TYPES} = require('../../constants');

const {
  INVALID_TOKENS_SHAPE,
  TOO_FEW_MQ_TOKENS,
  INVALID_TOKEN_TYPE,
  INVALID_TOKENS_STRING,
  INVALID_NUM_TOKEN_GROUPS,
  INVALID_TOKEN_GROUPING,
  INVALID_ATTRIBUTE_TOKEN,
  INVALID_METADATA_TOKEN,
} = require('./errors');
const {validateQueryType, isMetadataQuery} = require('./query-type');

const {
  // errors: datumValidationErrors,
  validateAttributeName,
  validateMetadataName,
} = require('../ngsi-datum-validator');


const validateTokens = (tokens, queryType = NGSI_SQL_QUERY_TYPES.ATTRIBUTE_QUERY) => {
  validateQueryType(queryType);
  // just check that tokens is an array of strings
  if (!_.isArray(tokens) || !tokens.length) {
    throw INVALID_TOKENS_SHAPE();
  }
  if (isMetadataQuery(queryType) && tokens.length < 2) {
    throw TOO_FEW_MQ_TOKENS();
  }
  _.each(tokens, (token) => {
    if (!_.isString(token)) {
      throw INVALID_TOKEN_TYPE();
    }
  });
  // validate the attribute token
  try {
    validateAttributeName(tokens[0]); // either case
  } catch (err) {
    throw INVALID_ATTRIBUTE_TOKEN();
  }
  // validate the metadata token
  if (isMetadataQuery(queryType)) {
    try {
      validateMetadataName(tokens[1]);
    } catch (err) {
      throw INVALID_METADATA_TOKEN();
    }
  }
};

const extractTokens = (tokenString) => {
  try {
    const tokens = tokenize(tokenString, QUERY_TOKEN_SEPARATOR, QUERY_TOKEN_GROUPER_CHAR);
    return tokens;
  } catch (err) {
    if (err.isNgsiTokenizeError) {
      if (tokenizeErrors.INVALID_TOKENIZE_STRING.matches(err)) {
        throw INVALID_TOKENS_STRING();
      }
      if (tokenizeErrors.INVALID_NUM_TOKENIZER_GROUPS.matches(err)) {
        throw INVALID_NUM_TOKEN_GROUPS();
      }
      if (tokenizeErrors.INVALID_TOKENIZER_GROUPING.matches(err)) {
        throw INVALID_TOKEN_GROUPING();
      }
    }
    throw new Error(`DevErr: Invalid token-parsing error: ${err.message}`);
  }
};

const parseTokens = (tokenString, queryType = NGSI_SQL_QUERY_TYPES.ATTRIBUTE_QUERY) => {
  validateQueryType(queryType);
  const tokens = extractTokens(tokenString);
  validateTokens(tokens, queryType);
  return tokens;
};

const serializeTokens = (tokens, queryType = NGSI_SQL_QUERY_TYPES.ATTRIBUTE_QUERY) => {
  validateQueryType(queryType);
  validateTokens(tokens);
  // if tokens have internal "."s then we need to wrap it in single quotes
  const wrappedTokens = _.map(tokens, (token) => {
    if (token.indexOf(QUERY_TOKEN_SEPARATOR) > -1) {
      return `${QUERY_TOKEN_GROUPER_CHAR}${token}${QUERY_TOKEN_GROUPER_CHAR}`;
    }
    return token;
  });
  // and join em up
  return wrappedTokens.join(QUERY_TOKEN_SEPARATOR);
};

module.exports = {
  validateTokens,
  parseTokens,
  serializeTokens,
};
