/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {NGSI_SQL_QUERY_TYPES} = require('../../constants');
const {INVALID_QUERY_TYPE} = require('./errors');

const isMetadataQuery = (queryType) => {
  validateQueryType(queryType);
  return queryType === NGSI_SQL_QUERY_TYPES.METADATA_QUERY;
};

const validateQueryType = (queryType) => {
  if (!_.includes(NGSI_SQL_QUERY_TYPES, queryType)) {
    throw INVALID_QUERY_TYPE();
  }
};

module.exports = {
  isMetadataQuery,
  validateQueryType,
};
