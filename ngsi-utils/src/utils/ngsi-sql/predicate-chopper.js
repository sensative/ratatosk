/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const {QUERY_PREDICATE_SEPARATOR} = require('@ratatosk/ngsi-constants');

const {validateQueryType, isMetadataQuery} = require('./query-type');
const {
  CHOP_ERR_EMPTY_Q_PARAM,
  CHOP_ERR_EMPTY_MQ_PARAM,
  GLUE_ERR_INVALID_SHAPE,
  GLUE_ERR_INVALID_ITEM,
} = require('./errors');

const {NGSI_SQL_QUERY_TYPES} = require('../../constants');

const chop = (queryArg, queryType = NGSI_SQL_QUERY_TYPES.ATTRIBUTE_QUERY) => {
  validateQueryType(queryType);
  if (_.isNil(queryArg)) {
    return [];
  }
  if (!_.isString(queryArg) || !queryArg.length) {
    const err = isMetadataQuery(queryType) ? CHOP_ERR_EMPTY_MQ_PARAM : CHOP_ERR_EMPTY_Q_PARAM;
    throw err();
  }
  const statements = queryArg.split(QUERY_PREDICATE_SEPARATOR);
  return statements;
};

const glue = (predicateStatements) => {
  // just check that predicateStatements is an array of strings
  if (!_.isArray(predicateStatements) || !predicateStatements.length) {
    throw GLUE_ERR_INVALID_SHAPE();
  }
  _.each(predicateStatements, (statement) => {
    if (!_.isString(statement) || !statement.length) {
      throw GLUE_ERR_INVALID_ITEM();
    }
  });
  return predicateStatements.join(QUERY_PREDICATE_SEPARATOR);
};

module.exports = {
  chop,
  glue,
};
