/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {
  SPECIAL_VALUE_TYPES,
  GEO_JSON_TYPES,
} = require('@ratatosk/ngsi-constants');

const {generateErrors} = require('../../libs/ngsi-error-generator');

const {
  errors: simpleCoordPairParserErrors,
  parseSimpleCoordPair,
  serializeSimpleCoordPair,
  validateSimpleCoordPair,
} = require('./simple-coord-pair-parser');

const errorTemplates = {
  INVALID_SIMPLE_POINT_ARRAY: 'A geojson point.coords has the format [lon, lat]',
  INVALID_SIMPLE_BOX_FORMAT: 'A simple box must be an array with exactly two coordinate pairs',
  INVALID_SIMPLE_BOX_ARRAY: 'A parsed simple box must be an array with exactly two coord pairs',
  INVALID_SIMPLE_LINE_FORMAT: 'A simple line must be an array with at least two coordinate pairs',
  INVALID_SIMPLE_LINE_ARRAY: 'A parsed simple line must be an array with at least two coordinate pairs',
  INVALID_SIMPLE_POLYGON_FORMAT: 'A simple polygon must be an array with at least 4 coord pairs',
  INVALID_SIMPLE_POLYGON_ARRAY: 'A parsed simple polygon must be an array with at least 4 coord pairs',
  INVALID_SIMPLE_POLYGON_CLOSURE: 'A polygon must repeat the first coord pair as the final pair',
  INVALID_SIMPLE_GEO_TYPE: 'The attribute type is not a known ngsi geo:<xxx> type',
  INVALID_GEO_JSON_TYPE: 'Invalid geojson type (must be "Point", "LineString", "Polygon" or "Multi"-versions)',
  INVALID_GEO_JSON_COORDS: 'Invalid geojson coords (must have format [lon, lat])',
  INVALID_GEO_JSON_LINE_STRING: 'Invalid geojson LineString',
  INVALID_GEO_JSON_POLYGON: 'Invalid geojson Polygon',
  INVALID_GEO_JSON_MULTI_POINT: 'Invalid geojson MultiPoint',
  INVALID_GEO_JSON_MULTI_LINE_STRING: 'Invalid geojson MultiLineString',
  INVALID_GEO_JSON_MULTI_POLYGON: 'Invalid geojson MultiPolygon',
  INVALID_NGSI_GEOMETRY_VALUE: 'Invalid geometry value (must look like a geojson object)',
};
const errors = generateErrors('NGSI_LOCATION_UTILS_ERROR', errorTemplates);

//
// POINT
//

const parseSimplePoint = (value) => {
  const coords = parseSimpleCoordPair(value);
  return {
    type: GEO_JSON_TYPES.POINT,
    coordinates: [coords.lon, coords.lat],
  };
};

const serializeSimplePoint = (value) => {
  if (!_.isArray(value) || value.length !== 2) {
    throw errors.INVALID_SIMPLE_POINT_ARRAY();
  }
  const [lon, lat] = value;
  const coords = serializeSimpleCoordPair({lon, lat});
  return coords;
};

//
// BOX
//

const parseSimpleBox = (value) => {
  if (!_.isArray(value) || value.length !== 2) {
    throw errors.INVALID_SIMPLE_BOX_FORMAT();
  }
  const [first, last] = _.map(value, (pair) => parseSimpleCoordPair(pair));
  const coordinates = [[
    [first.lon, first.lat],
    [first.lon, last.lat],
    [last.lon, last.lat],
    [last.lon, first.lat],
    [first.lon, first.lat],
  ]];
  return {
    type: GEO_JSON_TYPES.POLYGON,
    coordinates,
  };
};

const serializeSimpleBox = (value) => {
  if (!_.isArray(value) || value.length !== 1) {
    throw errors.INVALID_SIMPLE_BOX_ARRAY();
  }
  if (!_.isArray(value[0]) || value[0].length !== 5) {
    throw errors.INVALID_SIMPLE_BOX_ARRAY();
  }
  const first = value[0][0];
  const mid = value[0][2];
  const last = value[0][4];
  if (!_.isEqual(first, last)) {
    throw errors.INVALID_SIMPLE_BOX_ARRAY();
  }
  const coords = _.map([first, mid], (point) => {
    if (!_.isArray(point) || point.length !== 2) {
      throw errors.INVALID_SIMPLE_BOX_ARRAY();
    }
    const [lon, lat] = point;
    return serializeSimpleCoordPair({lon, lat});
  });
  return coords;
};

//
// LINE
//

const parseSimpleLine = (value) => {
  if (!_.isArray(value) || value.length < 2) {
    throw errors.INVALID_SIMPLE_LINE_FORMAT();
  }
  const coordinates = _.map(value, (pair) => {
    const point = parseSimpleCoordPair(pair);
    return [point.lon, point.lat];
  });
  return {
    type: GEO_JSON_TYPES.LINE_STRING,
    coordinates,
  };
};

const serializeSimpleLine = (value) => {
  if (!_.isArray(value) || value.length < 2) {
    throw errors.INVALID_SIMPLE_LINE_ARRAY();
  }
  const coords = _.map(value, (point) => {
    if (!_.isArray(point) || point.length !== 2) {
      throw errors.INVALID_SIMPLE_LINE_ARRAY();
    }
    const [lon, lat] = point;
    return serializeSimpleCoordPair({lon, lat});
  });
  return coords;
};

//
// POLYGON
//

const parseSimplePolygon = (value) => {
  if (!_.isArray(value) || value.length < 4) {
    throw errors.INVALID_SIMPLE_POLYGON_FORMAT();
  }
  const coordinates = _.map(value, (pair) => {
    const point = parseSimpleCoordPair(pair);
    return [point.lon, point.lat];
  });
  const first = coordinates[0];
  const last = coordinates[coordinates.length - 1];
  if (!_.isEqual(first, last)) {
    throw errors.INVALID_SIMPLE_POLYGON_CLOSURE();
  }
  return {
    type: GEO_JSON_TYPES.POLYGON,
    coordinates: [coordinates],
  };
};

const serializeSimplePolygon = (value) => {
  if (!_.isArray(value) || value.length !== 1) {
    throw errors.INVALID_SIMPLE_POLYGON_ARRAY();
  }
  if (!_.isArray(value[0]) || value[0].length < 4) {
    throw errors.INVALID_SIMPLE_POLYGON_ARRAY();
  }
  const coords = _.map(value[0], (point) => {
    if (!_.isArray(point) || point.length !== 2) {
      throw errors.INVALID_SIMPLE_POLYGON_ARRAY();
    }
    const [lon, lat] = point;
    return serializeSimpleCoordPair({lon, lat});
  });
  const first = coords[0];
  const last = coords[coords.length - 1];
  if (!_.isEqual(first, last)) {
    throw errors.INVALID_SIMPLE_POLYGON_CLOSURE();
  }
  return coords;
};

//
// GEO-JSON
//

// utility
const validateGeoJsonPointArray = (points) => {
  _.each(points, (point) => {
    if (!_.isArray(point) || point.length < 2) {
      throw errors.INVALID_GEO_JSON_COORDS();
    }
    const [lon, lat] = point;
    serializeSimpleCoordPair({lon, lat});
  });
};

// core validators

const validateGeoJsonPoint = (value) => {
  validateGeoJsonPointArray([value]);
};

const validateGeoJsonLineString = (value) => {
  if (!_.isArray(value) || value.length < 2) {
    throw errors.INVALID_GEO_JSON_LINE_STRING();
  }
  validateGeoJsonPointArray(value);
};

const validateGeoJsonPolygon = (value) => {
  if (!_.isArray(value) || value.length < 1) {
    throw errors.INVALID_GEO_JSON_POLYGON();
  }
  _.each(value, (ring) => {
    if (!_.isArray(ring) || ring.length < 4) {
      throw errors.INVALID_GEO_JSON_POLYGON();
    }
    const first = ring[0];
    const last = ring[ring.length - 1];
    if (!_.isEqual(first, last)) {
      throw errors.INVALID_GEO_JSON_POLYGON();
    }
    validateGeoJsonPointArray(ring);
  });
};

const validateGeoJson = (geoType, value) => {
  switch (geoType) {
    case GEO_JSON_TYPES.POINT: {
      validateGeoJsonPoint(value);
      return;
    }
    case GEO_JSON_TYPES.MULTI_POINT: {
      if (!_.isArray(value)) {
        throw errors.INVALID_GEO_JSON_MULTI_POINT();
      }
      _.each(value, (point) => validateGeoJsonPoint(point));
      return;
    }
    case GEO_JSON_TYPES.LINE_STRING: {
      validateGeoJsonLineString(value);
      return;
    }
    case GEO_JSON_TYPES.MULTI_LINE_STRING: {
      if (!_.isArray(value)) {
        throw errors.INVALID_GEO_JSON_MULTI_LINE_STRING();
      }
      _.each(value, (line) => validateGeoJsonLineString(line));
      return;
    }
    case GEO_JSON_TYPES.POLYGON: {
      validateGeoJsonPolygon(value);
      return;
    }
    case GEO_JSON_TYPES.MULTI_POLYGON: {
      if (!_.isArray(value)) {
        throw errors.INVALID_GEO_JSON_MULTI_POLYGON();
      }
      _.each(value, (polygon) => validateGeoJsonPolygon(polygon));
      return;
    }
    default: {
      throw errors.INVALID_GEO_JSON_TYPE();
    }
  }
};

const parseGeoJson = (geoType, coords) => {
  validateGeoJson(geoType, coords);
  return {
    type: geoType,
    coordinates: coords,
  };
};

const serializeGeoJson = (geoType, coords) => {
  validateGeoJson(geoType, coords);
  return {
    type: geoType,
    coordinates: coords,
  };
};


//
// aggregate
//

const isNgsiGeometryType = (type) => {
  const geoTypes = _.pick(SPECIAL_VALUE_TYPES, [
    'GEO_POINT',
    'GEO_BOX',
    'GEO_LINE',
    'GEO_POLYGON',
    'GEO_JSON',
  ]);
  return _.includes(geoTypes, type);
};

const parseNgsiGeometry = (type, value) => {
  switch (type) {
    case SPECIAL_VALUE_TYPES.GEO_POINT:
      return parseSimplePoint(value);
    case SPECIAL_VALUE_TYPES.GEO_BOX:
      return parseSimpleBox(value);
    case SPECIAL_VALUE_TYPES.GEO_LINE:
      return parseSimpleLine(value);
    case SPECIAL_VALUE_TYPES.GEO_POLYGON:
      return parseSimplePolygon(value);
    case SPECIAL_VALUE_TYPES.GEO_JSON:
      const geoType = _.get(value, 'type');
      const coords = _.get(value, 'coordinates');
      return parseGeoJson(geoType, coords);
    default:
      throw errors.INVALID_SIMPLE_GEO_TYPE();
  }
};


const serializeNgsiGeometry = (type, value) => {
  if (!_.isObject(value) || !_.has(value, 'type' || !_.has(value, 'coordinates'))) {
    throw errors.INVALID_NGSI_GEOMETRY_VALUE();
  }
  const {type: geojsonType, coordinates} = value;
  switch (type) {
    case SPECIAL_VALUE_TYPES.GEO_POINT:
      return serializeSimplePoint(coordinates);
    case SPECIAL_VALUE_TYPES.GEO_BOX:
      return serializeSimpleBox(coordinates);
    case SPECIAL_VALUE_TYPES.GEO_LINE:
      return serializeSimpleLine(coordinates);
    case SPECIAL_VALUE_TYPES.GEO_POLYGON:
      return serializeSimplePolygon(coordinates);
    case SPECIAL_VALUE_TYPES.GEO_JSON:
      return serializeGeoJson(geojsonType, coordinates);
    default:
      throw errors.INVALID_SIMPLE_GEO_TYPE();
  }
};


module.exports = {
  errors: {
    ...errors,
    ...simpleCoordPairParserErrors,
  },
  simpleCoordPairParser: {
    parseSimpleCoordPair,
    serializeSimpleCoordPair,
    validateSimpleCoordPair,
  },
  parseNgsiGeometry,
  serializeNgsiGeometry,
  isNgsiGeometryType,
};
