/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {
  GEOMETRY_QUERY_COORDS_LAT_LON_SEP,
} = require('@ratatosk/ngsi-constants');

const {generateErrors} = require('../../libs/ngsi-error-generator');

const errorTemplates = {
  INVALID_SIMPLE_POINT_FORMAT: 'A simple point must be a string',
  INVALID_SIMPLE_POINT_DIMENSION: 'A simple point must have 2 coordinates "<lat>,<lon>"',
  INVALID_SIMPLE_POINT_LATITUDE: 'Latitude must be between -90 and 90',
  INVALID_SIMPLE_POINT_LONGITUDE: 'Longitude must be between -180 and 180',
  INVALID_SIMPLE_POINT_OBJECT: 'A simple point parsed object must be an object with {lon, lat} coords',
};
const errors = generateErrors('NGSI_LOCATION_UTILS_ERROR', errorTemplates);

const parseSimpleCoordPair = (point) => {
  if (!_.isString(point)) {
    throw errors.INVALID_SIMPLE_POINT_FORMAT();
  }
  const coords = point.split(GEOMETRY_QUERY_COORDS_LAT_LON_SEP);
  if (coords.length !== 2) {
    throw errors.INVALID_SIMPLE_POINT_DIMENSION();
  }
  const lat = _.toNumber(coords[0]);
  const lon = _.toNumber(coords[1]);
  if (lat < -90 || lat > 90) {
    throw errors.INVALID_SIMPLE_POINT_LATITUDE();
  }
  if (lon < -180 || lon > 180) {
    throw errors.INVALID_SIMPLE_POINT_LONGITUDE();
  }
  return {lat, lon};
};

const serializeSimpleCoordPair = (point) => {
  if (!_.isObject(point) || !_.isNumber(point.lon) || !_.isNumber(point.lat)) {
    throw errors.INVALID_SIMPLE_POINT_OBJECT();
  }
  if (point.lat < -90 || point.lat > 90) {
    throw errors.INVALID_SIMPLE_POINT_LATITUDE();
  }
  if (point.lon < -180 || point.lon > 180) {
    throw errors.INVALID_SIMPLE_POINT_LONGITUDE();
  }
  return `${point.lat}${GEOMETRY_QUERY_COORDS_LAT_LON_SEP}${point.lon}`;
};

const validateSimpleCoordPair = (point) => {
  if (_.isObject(point)) {
    serializeSimpleCoordPair(point);
  } else {
    parseSimpleCoordPair(point);
  }
};


module.exports = {
  errors,
  parseSimpleCoordPair,
  serializeSimpleCoordPair,
  validateSimpleCoordPair,
};
