/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const moment = require('moment');

const {
  NGSI_DATETIME_FORMAT,
  NGSI_DATETIME_REGEX,
  ORION_DATETIME_REGEX,
} = require('@ratatosk/ngsi-constants');

const invalidDateTimeInitializer = () => new Error('Invalid numeric DateTime initializer arg');
const invalidDateTimeString = () => new Error('Invalid DateTime string');
const invalidDateObject = () => new Error('Invalid Date object. Cannot be formatted to a DateTime string');

const fromMillisecs = (ms1970) => {
  if (!_.isNumber(ms1970)) {
    throw invalidDateTimeInitializer();
  }
  return moment(ms1970).utc().format(NGSI_DATETIME_FORMAT);
};

const fromSeconds = (seconds1970) => {
  if (!_.isNumber(seconds1970)) {
    throw invalidDateTimeInitializer();
  }
  return fromMillisecs(seconds1970 * 1000);
};

const toMillisecs = (dtString) => {
  if (!NGSI_DATETIME_REGEX.test(dtString) && !ORION_DATETIME_REGEX.test(dtString)) {
    throw invalidDateTimeString();
  }
  const dateTime = moment(dtString, moment.ISO_8601);
  if (!dateTime.isValid()) {
    throw invalidDateTimeString();
  }
  return dateTime.valueOf();
};

const toSeconds = (dtString) => {
  const millisecs = toMillisecs(dtString);
  return Math.floor(millisecs / 1000);
};

const fromDate = (date) => {
  if (!_.isDate(date)) {
    throw invalidDateObject();
  }
  const ms = date.valueOf();
  const dateTimeString = fromMillisecs(ms);
  return dateTimeString;
};

const toDate = (dtString) => {
  const ms = toMillisecs(dtString);
  const date = new Date(ms);
  return date;
};

const hasValidDateTimeFormat = (dtString) => {
  try {
    toMillisecs(dtString);
  } catch (err) {
    return false;
  }
  return true;
};

module.exports = {
  fromMillisecs,
  toMillisecs,
  fromSeconds,
  toSeconds,
  fromDate,
  toDate,
  hasValidDateTimeFormat,
};
