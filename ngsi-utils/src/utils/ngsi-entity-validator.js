/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {
  DEFAULT_DATA_VALIDATION_MODE,
} = require('../constants');

const {generateErrors} = require('../libs/ngsi-error-generator');

const {
  validateEntityId,
  validateEntityType,
  validateAttributeName,
  validateAttributeType,
  validateMetadataName,
  validateMetadataType,
  validateGeneralValue,
} = require('./ngsi-datum-validator');

const errorTemplates = {
  INVALID_METADATA_ITEM_SHAPE: 'metadata item must be an object',
  INVALID_METADATA_ITEM_PROPS: 'metadata item has additional, invalid props',
  MISSING_METADATA_ITEM_TYPE: 'metadata item type is missing',
  MISSING_METADATA_ITEM_VALUE: 'metadata item value is missing',
  INVALID_ATTRIBUTE_SHAPE: 'attribute must be an object',
  INVALID_ATTRIBUTE_PROPS: 'attribute item has additional, invalid props',
  MISSING_ATTRIBUTE_TYPE: 'attribute item type is missing',
  MISSING_ATTRIBUTE_VALUE: 'attribute item value is missing',
  INVALID_METADATA_CONTAINER_SHAPE: 'attribute.metata must be an object if present',
  INVALID_ATTRIBUTES_CONTAINER_SHAPE: 'attributes container must be an object',
  INVALID_ATTRIBUTES_CONTAINER_ID_PROP: 'attributes container cannot have "id" prop',
  INVALID_ATTRIBUTES_CONTAINER_TYPE_PROP: 'attributes container cannot have "type" prop',
  INVALID_ENTITY_SHAPE: 'entity must be an object',
  MISSING_ENTITY_ID: 'entity.id is missing',
  MISSING_ENTITY_TYPE: 'entity.type is missing',
};
const errors = generateErrors('NGSI_ENTITY_VALIDATION_ERROR', errorTemplates);


const validateMetadatum = (mdItem, mode = DEFAULT_DATA_VALIDATION_MODE) => {
  // shape
  if (!_.isObject(mdItem)) {
    throw errors.INVALID_METADATA_ITEM_SHAPE();
  }
  if (_.size(_.omit(mdItem, ['type', 'value'])) > 0) {
    throw errors.INVALID_METADATA_ITEM_PROPS();
  }
  // type, and value
  if (!_.has(mdItem, 'type')) {
    throw errors.MISSING_METADATA_ITEM_TYPE();
  }
  validateMetadataType(mdItem.type, mode);
  if (!_.has(mdItem, 'value')) {
    throw errors.MISSING_METADATA_ITEM_VALUE();
  }
  validateGeneralValue(mdItem.value, mode);
};

const validateAttribute = (attr, mode = DEFAULT_DATA_VALIDATION_MODE) => {
  // shape
  if (!_.isObject(attr)) {
    throw errors.INVALID_ATTRIBUTE_SHAPE();
  }
  if (_.size(_.omit(attr, ['type', 'value', 'metadata'])) > 0) {
    throw errors.INVALID_ATTRIBUTE_PROPS();
  }
  // type, and value
  if (!_.has(attr, 'type')) {
    throw errors.MISSING_ATTRIBUTE_TYPE();
  }
  validateAttributeType(attr.type, mode);
  if (!_.has(attr, 'value')) {
    throw errors.MISSING_ATTRIBUTE_VALUE();
  }
  validateGeneralValue(attr.value, mode);
  // metadata
  if (_.has(attr, 'metadata') && !_.isObject(attr.metadata)) {
    throw errors.INVALID_METADATA_CONTAINER_SHAPE();
  }
  _.each(attr.metadata, (mdItem, mdName) => {
    validateMetadataName(mdName, mode);
    validateMetadatum(mdItem, mode);
  });
};

const validateAttrs = (attrs, mode = DEFAULT_DATA_VALIDATION_MODE) => {
  // shape
  if (!_.isObject(attrs)) {
    throw errors.INVALID_ATTRIBUTES_CONTAINER_SHAPE();
  }
  // id, type may NOT be present
  if (_.has(attrs, 'id')) {
    throw errors.INVALID_ATTRIBUTES_CONTAINER_ID_PROP();
  }
  if (_.has(attrs, 'type')) {
    throw errors.INVALID_ATTRIBUTES_CONTAINER_TYPE_PROP();
  }
  // check the attributes
  _.each(attrs, (attr, attrName) => {
    validateAttributeName(attrName, mode);
    validateAttribute(attr, mode);
  });
};

// TODO mode can be removed
const validateEntity = (entity, mode = DEFAULT_DATA_VALIDATION_MODE) => {
  // shape
  if (!_.isObject(entity)) {
    throw errors.INVALID_ENTITY_SHAPE();
  }
  // id, type
  if (!_.has(entity, 'id')) {
    throw errors.MISSING_ENTITY_ID();
  }
  validateEntityId(entity.id);
  if (!_.has(entity, 'type')) {
    throw errors.MISSING_ENTITY_TYPE();
  }
  validateEntityType(entity.type);
  // attributes
  const attributes = _.omit(entity, ['id', 'type']);
  validateAttrs(attributes);
};

module.exports = {
  errors,
  validateMetadatum,
  validateAttribute,
  validateAttrs,
  validateEntity,
};
