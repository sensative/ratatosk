/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');

const {jsonContentHeader, textContentHeader} = require('./ngsi-header-utils');

const TEXT_CONTENT = textContentHeader();
const JSON_CONTENT = jsonContentHeader();

const determineContentType = (value) => {
  assert(!_.isUndefined(value), 'value cannot be undefined');
  // the following is specified by ngsi-v2
  if (_.isString(value) || _.isNumber(value) || _.isNull(value) || _.isBoolean(value)) {
    return TEXT_CONTENT;
  } else if (_.isObject(value)) {
    return JSON_CONTENT;
  } else {
    throw new Error(`Invalid value: ${value}`);
  }
};

const formatValue = (value, type) => {
  assert(!_.isUndefined(value), 'value cannot be undefined');
  if (_.isEqual(type, JSON_CONTENT)) {
    // we should at the very least be able to JSON.stringify it
    // NOTE: we should also check for invalid chars? or somewhere else?
    try {
      JSON.stringify(value);
    } catch (e) {
      throw new Error('value is not a valid JSON object');
    }
    return value;
  } else if (_.isEqual(type, TEXT_CONTENT)) {
    if (_.isNull(value)) {
      return 'null';
    } else if (_.isBoolean(value)) {
      return value ? 'true' : 'false';
    } else if (_.isNumber(value)) {
      return value.toString();
    } else if (_.isString(value)) {
      return JSON.stringify(value); // check for invalid chars?
    } else {
      throw new Error('invalid value for text type: ', value);
    }
  } else {
    throw new Error(`invalid type ${type}: must be text or json`);
  }
};

const formatAttributeValue = (attrValueOrig, headersOrig) => {
  const contentTypeHeaders = determineContentType(attrValueOrig);
  const headers = _.assign({}, contentTypeHeaders, headersOrig);
  const attrValue = formatValue(attrValueOrig, contentTypeHeaders);
  return {attrValue, headers};
};

const unpackFormattedAttrValue = (formattedValue) => {
  const unpackErr = new Error('invalid attrValue');
  if (_.isString(formattedValue)) {
    try {
      return JSON.parse(formattedValue); // takes care of '"stringVal"', 'true', 'false', 'null', '10.2'
    } catch (err) {
      throw unpackErr;
    }
  } else if (_.isObject(formattedValue)) {
    return formattedValue;
  } else {
    throw unpackErr;
  }
};

module.exports = {
  formatAttributeValue,
  unpackFormattedAttrValue,
};
