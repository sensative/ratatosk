/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {
  SERVICE_PATH_DISJOINT_SEP,
  REST_REQUEST_HEADER_KEYS,
  REST_REQUEST_HEADER_VALUES,
} = require('@ratatosk/ngsi-constants');

const jsonContentHeader = () => ({
  [REST_REQUEST_HEADER_KEYS.CONTENT_TYPE]: REST_REQUEST_HEADER_VALUES.JSON,
});

const textContentHeader = () => ({
  [REST_REQUEST_HEADER_KEYS.CONTENT_TYPE]: REST_REQUEST_HEADER_VALUES.TEXT,
});

// this function blindly assumes the path is valid (i.e. no validation)
const createServicePathHeader = (...paths) => {
  const stringPaths = _.filter(paths, (path) => _.isString(path));
  if (!stringPaths.length) {
    return {};
  }
  const fullPath = stringPaths.join(SERVICE_PATH_DISJOINT_SEP);
  return {[REST_REQUEST_HEADER_KEYS.SERVICE_PATH]: fullPath};
};

// this just unwraps blindly what's in the header
const extractServicePathFromHeaders = (headers) => {
  return _.get(headers, REST_REQUEST_HEADER_KEYS.SERVICE_PATH);
};

module.exports = {
  jsonContentHeader,
  textContentHeader,
  createServicePathHeader,
  extractServicePathFromHeaders,
};
