/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');


const invalidErrorTemplateErr = () => new Error('invalid ngsi-error template');
const isValidErrorTemplate = (template) => {
  if (!_.isObject(template)) {
    return false;
  }
  if (!_.isString(template.name) || !_.isString(template.message) || !_.isNumber(template.status) || template.status < 400 || template.status >= 600) {
    return false;
  }
  return true;
};

class NgsiError extends Error {
  constructor (template) {
    if (!isValidErrorTemplate(template)) {
      throw invalidErrorTemplateErr();
    }
    super(template.message);
    Error.captureStackTrace(this, this.constructor);
    this.name = template.name;
    this.status = template.status;
    this.isNgsiError = true;
  }
}

const normalizedError = (err) => {
  const directErr = {
    name: _.get(err, 'name'),
    message: _.get(err, 'message'),
    status: _.get(err, 'status'),
  };
  if (isValidErrorTemplate(directErr)) {
    return directErr;
  }
  const fullRes = {
    name: _.get(err, 'response.data.error'),
    message: _.get(err, 'response.data.description'),
    status: _.get(err, 'response.status'),
  };
  if (isValidErrorTemplate(fullRes)) {
    return fullRes;
  }
  const partRes = {
    name: _.get(err, 'data.error'),
    message: _.get(err, 'data.description'),
    status: _.get(err, 'status'),
  };
  if (isValidErrorTemplate(partRes)) {
    return partRes;
  }
  throw new Error(`The error object is not identifiable as an ngsi-error. {name: ${err.name}, message: ${err.message}}`);
};

const removeInterpolatedContents = (desc) => {
  const parts = desc.split('/');
  let acc = '';
  _.each(parts, (part, index) => {
    if (index % 2 === 0) {
      acc = acc + part;
    }
  });
  return acc;
};

const MATCH_TYPES = {
  STRICT: 'STRICT',
  INTERPOLATE_TEMPLATE: 'INTERPOLATE_TEMPLATE',
  COLON_OPTIONS: 'COLON_OPTIONS',
};

const stringifyErrors = (actualError, expectedError) => {
  const actualStr = `actualError: {name: ${actualError.name}, desc: ${actualError.message}}`;
  const expectedStr = `expectedError: {name: ${expectedError.name}, desc: ${expectedError.message}}`;
  const fullStr = `[${actualStr}, ${expectedStr}]`;
  return fullStr;
};

// NOTE: the first 2 args are interchangeable
const match = (rawErr1, rawErr2, matchType = MATCH_TYPES.STRICT) => {
  // normalize
  const err1 = normalizedError(rawErr1);
  const err2 = normalizedError(rawErr2);
  // status
  assert.strictEqual(err1.status, err2.status, `status disagrees. ${stringifyErrors(err1, err2)}`);
  // error "name"
  assert.strictEqual(err1.name, err2.name, `error type disagrees. ${stringifyErrors(err1, err2)}`);
  // and the "description"
  switch (matchType) {
    case MATCH_TYPES.STRICT: {
      assert.strictEqual(
        err1.message,
        err2.message,
        `error template message did not match as EXACT REPLICA. ${stringifyErrors(err1, err2)}`,
      );
      break;
    }
    case MATCH_TYPES.INTERPOLATE_TEMPLATE: {
      assert.strictEqual(
        removeInterpolatedContents(err1.message),
        removeInterpolatedContents(err2.message),
        `error template message did not match as TEMPLATE. ${stringifyErrors(err1, err2)}`,
      );
      break;
    }
    case MATCH_TYPES.COLON_OPTIONS: {
      assert.strictEqual(
        err1.message.split(':')[0],
        err2.message.split(':')[0],
        `error messages did not match when truncated to the first COLON. ${stringifyErrors(err1, err2)}`,
      );
      break;
    }
    default: {
      throw new Error('DevErr: invalid matchType (internal usage only)');
    }

  }

};

const assertErrorsMatchFails = (err1, err2) => {
  let failed = false;
  try {
    match(err1, err2, MATCH_TYPES.STRICT);
  } catch (err) {
    failed = true;
  }
  if (!failed) {
    throw new Error('Errors should not have matched');
  }
};

const assertErrorsMatch = (err1, err2) => {
  match(err1, err2, MATCH_TYPES.STRICT);
};

const assertErrorsMatchish = (err1, err2) => {
  match(err1, err2, MATCH_TYPES.INTERPOLATE_TEMPLATE);
};

const assertErrorsMatchesColonTruncated = (err1, err2) => {
  match(err1, err2, MATCH_TYPES.COLON_OPTIONS);
};

const createError = (template) => new NgsiError(template);

module.exports = {
  NgsiError,
  createError,
  assertErrorsMatch,
  assertErrorsMatchish,
  assertErrorsMatchesColonTruncated,
  assertErrorsMatchFails,
};
