/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {generateErrors} = require('../libs/ngsi-error-generator');

const {
  validateMultipleServicePaths,
} = require('./ngsi-service-path-utils');

const {
  createServicePathHeader,
  extractServicePathFromHeaders,
} = require('./ngsi-header-utils');

const {
  formatEntity,
  formatAttrs,
  formatAttribute,
} = require('./ngsi-entity-formatter');

const {
  registrationPayloadParser: {
    formatRegistrationPayload,
  },
  subscriptionPayloadParser: {
    formatSubscriptionPayload,
  },
  bulkUpdatePayloadParser: {
    formatBulkUpdatePayload,
  },
  bulkQueryPayloadParser: {
    formatBulkQueryPayload,
  },
  queryParamsParser: {
    formatQueryParams,
  },
} = require('./ngsi-query-params-utils');

const {
  REST_REQUEST_HEADER_KEYS,
} = require('@ratatosk/ngsi-constants');

const {
  ENTITY_REQUEST_ARG_KEYS,
  BULK_QUERY_ARG_KEYS,
  BULK_UPDATE_ARG_KEYS,
  SUBSCRIPTION_ARG_KEYS,
  REGISTRATION_ARG_KEYS,
} = require('../constants');

const errorTemplates = {
  INVALID_ENTITY_ARG_NON_OBJECT: 'Invalid entity requestArg. If present, it must be an object',
  INVALID_ENTITY_ARG_KEYS: 'Invalid entity requestArg. Unrecognized keys present',
  INVALID_BULK_QUERY_ARG_NON_OBJECT: 'Invalid bulk query requestArg. It must be an object',
  INVALID_BULK_QUERY_ARG_KEYS: 'Invalid bulk query requestArg. Unrecognized keys present',
  INVALID_BULK_UPDATE_ARG_NON_OBJECT: 'Invalid bulk update requestArg. It must be an object',
  INVALID_BULK_UPDATE_ARG_KEYS: 'Invalid bulk update requestArg. Unrecognized keys present',
  INVALID_CREATE_SUBSCRIPTION_ARG_NON_OBJECT: 'Invalid createSubscription requestArg. It must be an object',
  INVALID_SUBSCRIPTION_ARG_KEYS: 'Invalid createSubscription requestArg. Unrecognized keys present',
  INVALID_CREATE_REGISTRATION_ARG_NON_OBJECT: 'Invalid createRegistration requestArg. It must be an object',
  INVALID_REGISTRATION_ARG_KEYS: 'Invalid createRegistration requestArg. Unrecognized keys present',
};
const errors = generateErrors('NGSI_CLIENT_REQUEST_ARG_ERROR', errorTemplates);

const hasValidKeyCoverage = (obj, allowedKeys) => {
  const keys = _.keys(obj);
  const validKeys = _.intersection(keys, allowedKeys);
  const isCovered = _.isEqual(keys, _.intersection(keys, validKeys));
  return isCovered;
};

//
// bulk query
//

const extractBulkQueryPayload = (bulkQueryArg) => {
  // validate the shape
  if (!_.isObject(bulkQueryArg)) {
    throw errors.INVALID_BULK_QUERY_ARG_NON_OBJECT();
  }
  // check for acceptable keys
  const allowedKeys = _.values(BULK_QUERY_ARG_KEYS);
  if (!hasValidKeyCoverage(bulkQueryArg, allowedKeys)) {
    throw errors.INVALID_BULK_QUERY_ARG_KEYS();
  }
  // and evaluate
  const payloadOrig = _.get(bulkQueryArg, BULK_QUERY_ARG_KEYS.PAYLOAD);
  const payload = formatBulkQueryPayload(payloadOrig);
  return payload;
};

//
// bulk update
//

const extractBulkUpdatePayload = (bulkUpdateArg) => {
  // validate the shape
  if (!_.isObject(bulkUpdateArg)) {
    throw errors.INVALID_BULK_UPDATE_ARG_NON_OBJECT();
  }
  // check for acceptable keys
  const allowedKeys = _.values(BULK_UPDATE_ARG_KEYS);
  if (!hasValidKeyCoverage(bulkUpdateArg, allowedKeys)) {
    throw errors.INVALID_BULK_UPDATE_ARG_KEYS();
  }
  // and evaluate
  const payloadOrig = _.get(bulkUpdateArg, BULK_UPDATE_ARG_KEYS.PAYLOAD);
  const payload = formatBulkUpdatePayload(payloadOrig);
  return payload;
};

//
// subscriptions
//

const extractCreateSubscriptionPayload = (subscriptionArg) => {
  // validate the shape
  if (!_.isObject(subscriptionArg)) {
    throw errors.INVALID_CREATE_SUBSCRIPTION_ARG_NON_OBJECT();
  }
  // check for acceptable keys
  const allowedKeys = _.values(SUBSCRIPTION_ARG_KEYS);
  if (!hasValidKeyCoverage(subscriptionArg, allowedKeys)) {
    throw errors.INVALID_SUBSCRIPTION_ARG_KEYS();
  }
  // and evaluate
  const payloadOrig = _.get(subscriptionArg, SUBSCRIPTION_ARG_KEYS.PAYLOAD);
  const payload = formatSubscriptionPayload(payloadOrig);
  return payload;
};

const extractSubscriptionId = (subscriptionArg) => {
  const subscriptionId = _.get(subscriptionArg, SUBSCRIPTION_ARG_KEYS.SUBSCRIPTION_ID);
  return subscriptionId;
};

//
// registrations
//

const extractCreateRegistrationPayload = (registrationArg) => {
  // validate the shape
  if (!_.isObject(registrationArg)) {
    throw errors.INVALID_CREATE_REGISTRATION_ARG_NON_OBJECT();
  }
  // check for acceptable keys
  const allowedKeys = _.values(REGISTRATION_ARG_KEYS);
  if (!hasValidKeyCoverage(registrationArg, allowedKeys)) {
    throw errors.INVALID_REGISTRATION_ARG_KEYS();
  }
  // and evaluate
  const payloadOrig = _.get(registrationArg, REGISTRATION_ARG_KEYS.PAYLOAD);
  const payload = formatRegistrationPayload(payloadOrig);
  return payload;
};

const extractRegistrationId = (registrationArg) => {
  const registrationId = _.get(registrationArg, REGISTRATION_ARG_KEYS.REGISTRATION_ID);
  return registrationId;
};


//
// entities stuff
//

const validateEntityRequestArgShape = (requestArg) => {
  if (!requestArg) {
    return; // we only validate when arg is present
  }
  if (!_.isObject(requestArg)) {
    throw errors.INVALID_ENTITY_ARG_NON_OBJECT();
  }
  // check for acceptable keys
  const allowedKeys = _.values(ENTITY_REQUEST_ARG_KEYS);
  if (!hasValidKeyCoverage(requestArg, allowedKeys)) {
    throw errors.INVALID_ENTITY_ARG_KEYS();
  }
};

const extractRequestArgHeaders = (requestArg) => {
  const origHeaders = _.get(requestArg, ENTITY_REQUEST_ARG_KEYS.HEADERS);

  // userId
  let userIdHeaders = {};
  const headersUserId = _.get(origHeaders, REST_REQUEST_HEADER_KEYS.USER_ID);
  const propsUserId = _.get(requestArg, ENTITY_REQUEST_ARG_KEYS.USER_ID);
  if (headersUserId || propsUserId) {
    userIdHeaders = {
      [REST_REQUEST_HEADER_KEYS.USER_ID]: headersUserId || propsUserId,
    };
  }

  // upsert (modification to create) -- keep this one restricted to
  let upsertHeaders = {};
  const isUpsert = !!_.get(origHeaders, REST_REQUEST_HEADER_KEYS.UPSERT);
  if (isUpsert) {
    upsertHeaders = {
      [REST_REQUEST_HEADER_KEYS.UPSERT]: true,
    };
  }

  // servicePath headers
  const headersServicePaths = extractServicePathFromHeaders(origHeaders);
  const propsServicePaths = _.get(
    requestArg,
    ENTITY_REQUEST_ARG_KEYS.SERVICE_PATH,
    _.get(requestArg, ENTITY_REQUEST_ARG_KEYS.SERVICE_PATHS),
  );
  // hierarchy: 1) explicit header 2) SERVICE_PATH, 3) SERVICE_PATHS
  let servicePathHeaders = {};
  const servicePaths = headersServicePaths || propsServicePaths;
  if (servicePaths) {
    validateMultipleServicePaths(servicePaths);
    const pathsArr = _.isArray(servicePaths) ? servicePaths : [servicePaths];
    servicePathHeaders = createServicePathHeader(...pathsArr);
  }
  // and combine
  const headers = _.assign(
    {},
    origHeaders,
    userIdHeaders,
    upsertHeaders,
    servicePathHeaders,
  );
  return headers;
};

const extractRequestArgParams = (requestArg) => {
  const paramsOrig = _.get(requestArg, ENTITY_REQUEST_ARG_KEYS.PARAMS);
  const params = formatQueryParams(paramsOrig);
  return params;
};

const extractRequestArgEntity = (requestArg) => {
  const entity = _.get(requestArg, ENTITY_REQUEST_ARG_KEYS.ENTITY);
  return entity ? formatEntity(entity) : undefined;
};

const extractRequestArgEntityId = (requestArg) => {
  return _.get(
    requestArg,
    ENTITY_REQUEST_ARG_KEYS.ENTITY_ID,
    _.get(requestArg, [ENTITY_REQUEST_ARG_KEYS.ENTITY, 'id']),
  );
};

const extractRequestArgAttrName = (requestArg) => {
  return _.get(requestArg, ENTITY_REQUEST_ARG_KEYS.ATTR_NAME);
};

const extractRequestArgAttrs = (requestArg) => {
  const entity = extractRequestArgEntity(requestArg);
  const attrs = _.get(
    requestArg,
    ENTITY_REQUEST_ARG_KEYS.ATTRS,
    entity ? _.omit(entity, ['id', 'type']) : undefined,
  );
  return attrs ? formatAttrs(attrs) : undefined;
};

const extractRequestArgAttr = (requestArg) => {
  const attrName = extractRequestArgAttrName(requestArg);
  const attrs = extractRequestArgAttrs(requestArg);
  const attr = _.get(
    requestArg,
    ENTITY_REQUEST_ARG_KEYS.ATTR,
    _.get(attrs, attrName),
  );
  return attr ? formatAttribute(attr) : undefined;
};

const extractRequestArgAttrValue = (requestArg) => {
  const attr = extractRequestArgAttr(requestArg);
  const attrValue = _.get(
    requestArg,
    ENTITY_REQUEST_ARG_KEYS.ATTR_VALUE,
    _.get(attr, 'value'),
  );
  return attrValue;
};


module.exports = {
  errors,
  // bulk query
  extractBulkQueryPayload,
  // bulk update
  extractBulkUpdatePayload,
  // subscriptions
  extractCreateSubscriptionPayload,
  extractSubscriptionId,
  // registrations
  extractCreateRegistrationPayload,
  extractRegistrationId,
  // all entities
  validateEntityRequestArgShape,
  extractRequestArgHeaders,
  extractRequestArgParams,
  extractRequestArgEntity,
  extractRequestArgEntityId,
  extractRequestArgAttrs,
  extractRequestArgAttr,
  extractRequestArgAttrName,
  extractRequestArgAttrValue,
};
