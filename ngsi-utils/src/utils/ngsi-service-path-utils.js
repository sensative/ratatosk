/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {generateErrors} = require('../libs/ngsi-error-generator');

const {
  SERVICE_PATH_SEP,
  SERVICE_PATH_WILDCARD,
  SERVICE_PATH_DEFAULT_QUERY_PATH,
  SERVICE_PATH_DEFAULT_UPDATE_PATH,
  SERVICE_PATH_DISJOINT_SEP,
  SERVICE_PATH_MAX_DISJOINT_ALLOWED,
  SERVICE_PATH_MAX_DISJOINT_ALLOWED_FOR_UPDATES,
  SERVICE_PATH_MAX_LEVELS,
  SERVICE_PATH_MIN_LEVEL_CHARS,
  SERVICE_PATH_MAX_LEVEL_CHARS,
  SERVICE_PATH_LEVEL_REGEX,
} = require('@ratatosk/ngsi-constants');

const errorTemplates = {
  EMPTY_SERVICE_PATH: 'Only explicit valid servicePaths allowed in strict mode',
  NON_STRING_SERVICE_PATH: 'servicePath must be a string if included',
  NON_ABSOLUTE_SERVICE_PATH: 'servicePath must be absolute (i.e. begin with "/")',
  TOO_MANY_COMPONENTS: `too many components in ServicePath`,
  TOO_SHORT_COMPONENT: `servicePath level must be a minimum ${SERVICE_PATH_MIN_LEVEL_CHARS} chars`,
  TOO_LONG_COMPONENT: `servicePath level must be a maximum ${SERVICE_PATH_MAX_LEVEL_CHARS} chars`,
  ILLEGAL_CHAR_IN_COMPONENT: 'illegal char in servicePath component',
  INVALID_SERVICE_PATHS_ARG: 'validateMultipleServicePaths argument must be a string or array',
  INVALID_SERVICE_PATHS_ARG_STRICT: 'validateMultipleServicePathsStrict argument must be an array with at least 1 servicePath',
  TOO_MANY_UPDATE_SERVICE_PATHS: `Update operations can specify maximum ${SERVICE_PATH_MAX_DISJOINT_ALLOWED_FOR_UPDATES} servicePaths`,
  TOO_MANY_SERVICE_PATHS: `Too many servicePaths for this operation (max ${SERVICE_PATH_MAX_DISJOINT_ALLOWED} allowed)`,
};
const errors = generateErrors('NGSI_SERVICE_PATH_ERROR', errorTemplates);

const validateServicePath = (path, inputOptions) => {
  const defaultOptions = {isStrict: false, isUpdate: false};
  const options = _.assign({}, defaultOptions, inputOptions);

  // This should be rewritten as if(_.isUndefined()) return true; if(!path) isStrict etc)
  if (_.isNil(path) || path === '') {
    if (options.isStrict) {
      throw errors.EMPTY_SERVICE_PATH();
    }
    return; // DONE
  }
  let mutablePath = path;
  if (!_.isString(mutablePath)) {
    throw errors.NON_STRING_SERVICE_PATH();
  }
  if (mutablePath.charAt(0) !== SERVICE_PATH_SEP) {
    throw errors.NON_ABSOLUTE_SERVICE_PATH();
  }
  // get rid of beginning "/"
  mutablePath = mutablePath.substring(1);
  if (mutablePath.length === 0) {
    // this is just the root path. It's ok.
    return; // DONE
  }

  // now break into parts]
  const parts = mutablePath.split(SERVICE_PATH_SEP);
  if (parts.length > SERVICE_PATH_MAX_LEVELS) {
    throw errors.TOO_MANY_COMPONENTS();
  }
  _.each(parts, (part, index) => {
    if (part.length < SERVICE_PATH_MIN_LEVEL_CHARS) {
      throw errors.TOO_SHORT_COMPONENT();
    }
    if (part.length > SERVICE_PATH_MAX_LEVEL_CHARS) {
      throw errors.TOO_LONG_COMPONENT();
    }
    const isWildCard = part === SERVICE_PATH_WILDCARD;
    const isLastPart = index === parts.length - 1;
    const skipRegexCheck = isLastPart && isWildCard && !options.isUpdate;
    if (!skipRegexCheck && !SERVICE_PATH_LEVEL_REGEX.test(part)) {
      throw errors.ILLEGAL_CHAR_IN_COMPONENT();
    }
  });
};

// paths here can be a string (either singular or compoun)
// or an array (of exclusively singular path items)
const validateMultipleServicePaths = (pathsArg, isUpdate) => {
  if (!_.isString(pathsArg) && !_.isArray(pathsArg)) {
    throw errors.INVALID_SERVICE_PATHS_ARG();
  }
  let paths = pathsArg;
  if (_.isString(pathsArg)) {
    paths = pathsArg.split(SERVICE_PATH_DISJOINT_SEP);
  }
  if (isUpdate && paths.length > SERVICE_PATH_MAX_DISJOINT_ALLOWED_FOR_UPDATES) {
    throw errors.TOO_MANY_UPDATE_SERVICE_PATHS();
  } else if (!isUpdate && paths.length > SERVICE_PATH_MAX_DISJOINT_ALLOWED) {
    throw errors.TOO_MANY_SERVICE_PATHS();
  }
  _.each(paths, (path) => {
    validateServicePath(path, {isStrict: true, isUpdate});
  });
};

const validateMultipleServicePathsStrict = (pathsArg, isUpdate) => {
  if (!_.isArray(pathsArg) || !pathsArg.length) {
    throw errors.INVALID_SERVICE_PATHS_ARG_STRICT();
  }
  validateMultipleServicePaths(pathsArg, isUpdate);
};

const parseServicePathsLoose = (pathsArg, isUpdate) => {
  if (!pathsArg) {
    const servicePath = isUpdate ? SERVICE_PATH_DEFAULT_UPDATE_PATH : SERVICE_PATH_DEFAULT_QUERY_PATH;
    return [servicePath];
  }
  if (!_.isString(pathsArg)) {
    throw errors.NON_STRING_SERVICE_PATH();
  }
  const paths = pathsArg.split(SERVICE_PATH_DISJOINT_SEP);
  return paths;
};

module.exports = {
  validateServicePath,
  validateMultipleServicePaths,
  validateMultipleServicePathsStrict,
  parseServicePathsLoose,
  errors,
};
