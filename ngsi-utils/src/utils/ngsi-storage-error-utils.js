/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

// some errors, originating from the db storage, need to be obviously recognizable

const {generateErrors} = require('../libs/ngsi-error-generator');

const errorTemplates = {
  // subscriptions dbArg errors
  INVALID_SUBSCRIPTION_DBARG_FORMAT: 'dbSubscriptionArg must be an object if included',
  INVALID_SUBSCRIPTION_DBARG_KEYS: 'Invalid dbSubscriptionArg keys',

  // bulk dbArg errors
  INVALID_BULK_QUERY_DBARG_FORMAT: 'dbBulkQueryArg must be an object if included',
  INVALID_BULK_QUERY_DBARG_KEYS: 'Invalid dbBulkQueryArg keys',
  INVALID_BULK_UPDATE_DBARG_FORMAT: 'dbBulkUpdateArg must be an object if included',
  INVALID_BULK_UPDATE_DBARG_KEYS: 'Invalid dbBulkUpdateArg keys',

  // entity dbArg assertions
  INVALID_ENTITY_DBARG_ENTITY_MISSING: 'Entity is missing from entity dbArg',
  INVALID_ENTITY_DBARG_ENTITY_ID_MISSING: 'Entity id is missing from entity dbArg',
  INVALID_ENTITY_DBARG_FAULTY_ID_INFO: 'Entity id and idPattern params forbidden in entity dbArg for this route',
  INVALID_ENTITY_DBARG_ATTRS_MISSING: 'Entity attrs are missing from entity dbArg',
  INVALID_ENTITY_DBARG_ATTR_NAME_MISSING: 'attrName is required but missing from entity dbArg',
  INVALID_ENTITY_DBARG_ATTR_MISSING: 'Entity attr is required but missing from entity dbArg',
  INVALID_ENTITY_DBARG_ATTR_VALUE_MISSING: 'Entity attrValue is required but missing from entity dbArg',

  // others
  CANNOT_CREATE_ALREADY_EXISTS: 'Cannot create entity as it already exists for the given {id, type, servicePath} combination',
  TOO_MANY_MATCHES: 'More than 1 entity was found that matches the query',
  MATCH_NOT_FOUND: 'No matching entity to the query was found',
  ATTR_DOES_NOT_EXIST: 'Desired attribute does not exist',
  CANNOT_CREATE_ATTR_ALREADY_EXISTS: 'Cannot create attr as it already exists',
  CANNOT_CREATE_ATTR_COLLIDES_WITH_REGISTRATION: 'Cannot create attr as a registration with the same name already exists',
  MULTIPLE_GEOATTRS_BUT_NO_DEFAULT: 'When multiple geo attrs exist, exactly one of them must have the defaultLocation metadatum (none found)',
  MULTIPLE_GEOATTRS_AND_MULTIPLE_DEFAULTS: 'When multiple geo attrs exist, exactly one of them must have the defaultLocation metadatum (multiple found)',

  // custom access stuff and query
  INVALID_CUSTOM_ARG: 'Invalid custom query argument. This is storage implementation specific',
};
const errors = generateErrors('NGSI_STORAGE_ERROR', errorTemplates);

module.exports = {
  errorTemplates,
  errors,
};
