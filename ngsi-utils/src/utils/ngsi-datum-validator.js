/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {
  // general value types
  DEFAULT_VALUE_TYPES,
  SPECIAL_VALUE_TYPES,
  // ngsi field syntax
  FIELD_NAME_MIN_LENGTH,
  FIELD_NAME_MAX_LENGTH,
  FIELD_NAME_INVALID_CHARS,
  // builtins
  ILLEGAL_ATTRIBUTE_NAMES,
  ILLEGAL_METADATA_NAMES,
  // orion
  ORION_FORBIDDEN_QUERY_CHARS,
  // sql stuff
  QUERY_PREDICATE_SEPARATOR,
  QUERY_BINARY_OPERATORS,
  QUERY_UNARY_OPERATORS,
  QUERY_TOKEN_GROUPER_CHAR,
  QUERY_TOKEN_SEPARATOR,
} = require('@ratatosk/ngsi-constants');

const {
  DEFAULT_DATA_VALIDATION_MODE,
  DATA_VALIDATION_MODES: {
    ORION,
    PERMISSIVE,
    RESTRICTED,
    ORION_RESTRICTED,
  },
} = require('../constants');

const {hasValidDateTimeFormat} = require('./ngsi-date-time-utils');
const {generateErrors} = require('../libs/ngsi-error-generator');


const restrictedSubstrings = _.concat(
  _.values(QUERY_BINARY_OPERATORS),
  _.values(QUERY_UNARY_OPERATORS),
  QUERY_PREDICATE_SEPARATOR,
  QUERY_TOKEN_GROUPER_CHAR,
  QUERY_TOKEN_SEPARATOR,
);

const validModes = [
  ORION,
  PERMISSIVE,
  RESTRICTED,
  ORION_RESTRICTED,
];

const errorTemplates = {
  INVALID_MODE: 'Invalid validation mode',
  FIELD_MUST_BE_STRING: 'field must be a string',
  FIELD_VALUE_TOO_SHORT: `field must be at least length ${FIELD_NAME_MIN_LENGTH}`,
  FIELD_VALUE_TOO_LONG: `field must be at most length ${FIELD_NAME_MAX_LENGTH}`,
  FIELD_CONTAINS_FORBIDDEN_CHAR: `field contains an illegal char`,
  FIELD_CONTAINS_FORBIDDEN_ORION_CHAR: 'field contains a char forbidden by Orion syntax rules',
  FORBIDDEN_METADATA_NAME: 'forbidden metadata name was used (check builtins)',
  FORBIDDEN_ATTRIBUTE_NAME: 'forbidden attribute name was used (check builtins)',
  NON_QUERYABLE_NAME: 'attr or md name contains a substring that is SQL incompatible',
  INVALID_GENERAL_VALUE: 'Invalid general value: cannot infer type',
};
const errors = generateErrors('NGSI_DATUM_VALIDATION_ERROR', errorTemplates);

// /////
//  local utilities
// /////

const needsOrionTreatment = (mode) => {
  return (mode === ORION || mode === ORION_RESTRICTED);
};

const needsRestrictedTreatment = (mode) => {
  return (mode === RESTRICTED || mode === ORION_RESTRICTED);
};

const validateFiwareFieldLength = (field) => {
  if (field.length < FIELD_NAME_MIN_LENGTH) {
    throw errors.FIELD_VALUE_TOO_SHORT();
  }
  if (field.length > FIELD_NAME_MAX_LENGTH) {
    throw errors.FIELD_VALUE_TOO_LONG();
  }
};

const validateFiwareField = (field) => {
  validateFiwareFieldLength(field);
  // validate the chars
  _.each(FIELD_NAME_INVALID_CHARS, (char) => {
    if (field.indexOf(char) > -1) {
      throw errors.FIELD_CONTAINS_FORBIDDEN_CHAR();
    }
  });
};

const checkForIllegalMetadataNames = (name) => {
  _.each(ILLEGAL_METADATA_NAMES, (metadataName) => {
    if (metadataName === name) {
      throw errors.FORBIDDEN_METADATA_NAME();
    }
  });
};

const checkForIllegalAttributeNames = (name) => {
  _.each(ILLEGAL_ATTRIBUTE_NAMES, (attrName) => {
    if (attrName === name) {
      throw errors.FORBIDDEN_ATTRIBUTE_NAME();
    }
  });
};

const validateOrionField = (field) => {
  // validate the chars
  _.each(ORION_FORBIDDEN_QUERY_CHARS, (char) => {
    if (field.indexOf(char) > -1) {
      throw errors.FIELD_CONTAINS_FORBIDDEN_ORION_CHAR();
    }
  });
};

const validateGeneralField = (val, mode) => {
  if (!_.isString(val)) {
    throw errors.FIELD_MUST_BE_STRING();
  }
  validateFiwareField(val);
  if (needsOrionTreatment(mode)) {
    validateOrionField(val);
  }
};

const validatePatternField = (val) => {
  if (!_.isString(val)) {
    throw errors.FIELD_MUST_BE_STRING();
  }
  validateFiwareFieldLength(val);
  RegExp(val); // invalid regexps will throw SyntaxError with a descriptive message
  // What error to throw?
};

const validateQueryableName = (queryableName, mode) => {
  if (!needsRestrictedTreatment(mode)) {
    return; // done
  }
  _.each(restrictedSubstrings, (rs) => {
    if (queryableName.indexOf(rs) > -1) {
      throw errors.NON_QUERYABLE_NAME();
    }
  });
};

// This guy gets exported (should probably be stored elsewhere)
const inferTypeForGeneralValue = (value, ignoreSpecialTypes) => {
  // otherwise do some more work
  if (_.isString(value)) {
    if (hasValidDateTimeFormat(value) && !ignoreSpecialTypes) {
      return SPECIAL_VALUE_TYPES.DATE_TIME;
    }
    return DEFAULT_VALUE_TYPES.TEXT;
  } else if (_.isNumber(value)) {
    return DEFAULT_VALUE_TYPES.NUMBER;
  } else if (_.isBoolean(value)) {
    return DEFAULT_VALUE_TYPES.BOOLEAN;
  } else if (_.isNull(value)) {
    return DEFAULT_VALUE_TYPES.NONE;
  } else if (_.isArray(value)) {
    return DEFAULT_VALUE_TYPES.STRUCTURED_VALUE;
  } else if (_.isObject(value)) {
    return DEFAULT_VALUE_TYPES.STRUCTURED_VALUE;
  } else {
    throw errors.INVALID_GENERAL_VALUE();
  }
};

const validateGeneralValueRecursive = (val, mode, isOutermost) => {
  if (!needsOrionTreatment(mode)) {
    return; // done
  }
  const type = inferTypeForGeneralValue(val, !isOutermost);
  switch (type) {
    case DEFAULT_VALUE_TYPES.NUMBER:
    case DEFAULT_VALUE_TYPES.BOOLEAN:
    case SPECIAL_VALUE_TYPES.DATE_TIME:
    case DEFAULT_VALUE_TYPES.NONE: {
      return; // these are automatically valid
    }
    case DEFAULT_VALUE_TYPES.TEXT: {
      return validateOrionField(val);
    }
    case DEFAULT_VALUE_TYPES.STRUCTURED_VALUE: {
      const vals = _.values(val);
      return _.each(vals, (v) => validateGeneralValueRecursive(v, mode, false)); // recursion!!
    }
    default: {
      throw new Error('DevErr: Invalid type. this should be impossible');
    }
  }
};

const validateMode = (mode) => {
  if (!_.includes(validModes, mode)) {
    throw errors.INVALID_MODE();
  }
};

// /////
// And all the exports
// /////

const validateGeneralValue = (val, mode = DEFAULT_DATA_VALIDATION_MODE) => {
  validateMode(mode);
  validateGeneralValueRecursive(val, mode, true);
};

const validateEntityId = (val, mode = DEFAULT_DATA_VALIDATION_MODE) => {
  validateMode(mode);
  validateGeneralField(val, mode);
};

const validateEntityType = (val, mode = DEFAULT_DATA_VALIDATION_MODE) => {
  validateMode(mode);
  validateGeneralField(val, mode);
};

const validateEntityIdPattern = (val, mode = DEFAULT_DATA_VALIDATION_MODE) => {
  validateMode(mode);
  validatePatternField(val);
};

const validateEntityTypePattern = (val, mode = DEFAULT_DATA_VALIDATION_MODE) => {
  validateMode(mode);
  validatePatternField(val);
};

const validateAttributeName = (val, mode = DEFAULT_DATA_VALIDATION_MODE) => {
  validateMode(mode);
  validateGeneralField(val, mode);
  checkForIllegalAttributeNames(val);
  validateQueryableName(val, mode);
};

const validateAttributeType = (val, mode = DEFAULT_DATA_VALIDATION_MODE) => {
  validateMode(mode);
  validateGeneralField(val, mode);
};

const validateMetadataName = (val, mode = DEFAULT_DATA_VALIDATION_MODE) => {
  validateMode(mode);
  validateGeneralField(val, mode);
  checkForIllegalMetadataNames(val);
  validateQueryableName(val, mode);
};

const validateMetadataType = (val, mode = DEFAULT_DATA_VALIDATION_MODE) => {
  validateMode(mode);
  validateGeneralField(val, mode);
};

module.exports = {
  errors,
  validateEntityId,
  validateEntityType,
  validateEntityIdPattern,
  validateEntityTypePattern,
  validateAttributeName,
  validateAttributeType,
  validateMetadataName,
  validateMetadataType,
  // generalValue can be either metadata or attr value
  inferTypeForGeneralValue,
  validateGeneralValue,
};
