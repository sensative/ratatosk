/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const FAILED_REJECTION_ERR = 'Error: Promise arg failed to reject as expected';
const INVALID_MATCH_ERR = 'Invalid err match item for assertThrows. need a "message" or {message}';
const failedMatchErr = (expected, actual) => `function threw, but the error messages did not match: {expected: ${expected}}  {actual: ${actual}}`;

const run = async (arg) => {
  if (_.isFunction(arg)) {
    await arg();
  } else {
    await arg;
  }
};

const assertRejects = async (promise, matchArg) => {
  const hasMatcher = !_.isNil(matchArg);
  const messageMatch = _.isString(matchArg) ? matchArg : _.get(matchArg, 'message');
  if (hasMatcher && !_.isString(messageMatch)) {
    // kill any possible uncaught rejections
    await Promise.resolve().then(() => promise).catch(() => 0);
    // then reject
    throw new Error(INVALID_MATCH_ERR);
  }
  let failedToReject = false;
  return run(promise)
    .then(() => {
      failedToReject = true;
    })
    .catch((err) => err)
    .then((err) => {
      if (failedToReject) {
        throw new Error(FAILED_REJECTION_ERR);
      }
      const message = _.get(err, 'message', null);
      if (hasMatcher && messageMatch !== message) {
        // we have an errMatch to match against, but it doesn't match
        throw new Error(failedMatchErr(messageMatch, message));
      }
      return err;
    });
};

module.exports = assertRejects;
