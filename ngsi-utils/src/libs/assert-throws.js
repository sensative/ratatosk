/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const INVALID_FN_ERR = 'The first arg must be a function';
const FAILED_THROW_ERR = 'the function did not throw';

const INVALID_MATCH_ERR = 'Invalid err match item for assertThrows. need a "message" or {message}';
const failedMatchErr = (expected, actual) => `function threw, but the error messages did not match: {expected: ${expected}}  {actual: ${actual}}`;

const assertThrows = (fn, matchArg) => {
  if (!_.isFunction(fn)) {
    throw new Error(INVALID_FN_ERR);
  }
  const hasMatcher = !_.isNil(matchArg);
  const messageMatch = _.isString(matchArg) ? matchArg : _.get(matchArg, 'message');
  if (hasMatcher && !_.isString(messageMatch)) {
    throw new Error(INVALID_MATCH_ERR);
  }
  try {
    fn();
  } catch (err) {
    const message = _.get(err, 'message', null);
    if (hasMatcher && messageMatch !== message) {
      // we have an errMatch to match against, but it doesn't match
      throw new Error(failedMatchErr(messageMatch, message));
    }
    // everything went as expected. return the actually thrown err
    return err;
  }
  throw new Error(FAILED_THROW_ERR);
};

module.exports = assertThrows;
