/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const regex = /^[_A-Z]+$/;

const validateErrorType = (errorType) => {
  if (!_.isString(errorType) || !errorType.match(regex)) {
    throw new Error(`DevErr: invalid errorType: ${errorType}`);
  }
};

const validateTemplates = (templates) => {
  _.each(templates, (value, key) => {
    if (!key.match(regex)) {
      throw new Error(`DevErr: invalid error template name: ${key}`);
    }
    if (!_.isString(value)) {
      throw new Error(`DevErr: invalid error template message for ${key}`);
    }
  });
};

const createErrorTypeFlag = (errorType) => {
  const rawParts = errorType.split('_');
  const parts = _.map(rawParts, (rawPart) => {
    if (!rawPart) {
      return rawPart;
    }
    return rawPart.charAt(0) + rawPart.substring(1).toLowerCase();
  });
  return `is${parts.join('')}`;
};

const generateErrors = (errorType, templates) => {
  validateErrorType(errorType);
  validateTemplates(templates);

  const errors = _.mapValues(templates, (message, name) => {
    const flag = createErrorTypeFlag(errorType);
    const create = () => {
      const err = new Error(message);
      err.isNgsiError = true;
      err[flag] = true;
      err.name = name;
      return err;
    };
    create.matches = (err) => {
      const isMatchingError = _.isError(err) && message === err.message;
      const hasMatchingMessage = _.isString(err) && message === err;
      return isMatchingError || hasMatchingMessage;
    };
    return create;
  });

  return errors;
};

module.exports = {
  generateErrors,
};
