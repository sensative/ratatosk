/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

// this guy is used in a couple places.
const _ = require('lodash');
const {generateErrors} = require('./ngsi-error-generator');


const errorTemplates = {
  INVALID_TOKENIZE_STRING: 'A nonempty string is required for the ngsi-tokenizer input',
  INVALID_TOKENIZE_GROUPER: 'A nonempty string is required to "group" tokens',
  INVALID_TOKENIZE_SEPARATOR: 'A nonempty string is required to "separate" tokens',
  INVALID_NUM_TOKENIZER_GROUPS: 'The tokenize groupers must come in pairs, ALWAYS leaving an odd number of groups',
  INVALID_TOKENIZER_GROUPING: 'Invalid tokenizer: the grouper and separators are invalidly composed',
};
const errors = generateErrors('NGSI_TOKENIZE_ERROR', errorTemplates);


const detailedTokenize = (tokenString, separator, grouper) => {
  if (!_.isString(tokenString) || !tokenString.length) {
    throw errors.INVALID_TOKENIZE_STRING();
  }
  if (!_.isString(separator) || !separator.length) {
    throw errors.INVALID_TOKENIZE_SEPARATOR();
  }
  if (!_.isString(grouper) || !grouper.length) {
    throw errors.INVALID_TOKENIZE_GROUPER();
  }
  // this all is about if paths are defined with groupings
  // eg: aa.'bb.cc.dd'.ee.'ff' ---> ['aa', 'bb.cc.dd', 'ee', 'ff']
  const isGrouped = tokenString.indexOf(grouper) > -1;
  // The usual and reasonable case would be to just not do/use this 'feature' at all:
  if (!isGrouped) {
    const tokens = tokenString.split(separator);
    return _.map(tokens, (token) => ({value: token, isGrouped: false}));
  }
  // And all the rest of this is the edge case..
  // N.B.: this code all looks kinda hokey until you look at the ngsi-v2
  // documentation for this stuff, which EXPLICITLY makes a sort of escape
  // character which unfortunately has to be the single quote ('). Derp.
  const tokenGroups = tokenString.split(grouper);
  if (tokenGroups.length % 2 === 0) {
    // We must always have paired grouping chars which always leads to an odd number of groups
    throw errors.INVALID_NUM_TOKENIZER_GROUPS();
  }
  // extract individual tokens, from the tokenGroups
  const detailedTokens = _.flatten(_.map(tokenGroups, (group, index) => {
    // the odd token groups will ALWAYS be the grouped tokens, so these should be done
    if (index % 2 === 1) {
      return [{value: group, isGrouped: true}]; // for the flatten
    }
    // first edge-case: index === 0 (i.e. the start of the overall tokensString)
    if (index === 0) {
      const groupTokens = _.split(group, separator);
      // CASE: no tokens here at all: the tokenString effectively STARTED with the grouped token groups[1]
      if (groupTokens.length === 1 && !groupTokens[0].length) {
        return [];
      }
      // CASE: there is no separator between the group string and the first grouped token
      if (groupTokens.length === 1) {
        throw errors.INVALID_TOKENIZER_GROUPING();
      }
      // CASE: more than one groupToken & LAST one is NOT empty
      // (which if would have to be for the '.' to be next to the grouping char)
      if (groupTokens[groupTokens.length - 1] !== '') {
        throw errors.INVALID_TOKENIZER_GROUPING();
      }
      // otherwise just return the groupTokens (minus the last, empty, groupToken)
      const actualTokens = groupTokens.slice(0, groupTokens.length - 1);
      return _.map(actualTokens, (token) => ({value: token, isGrouped: false}));
    }
    // second edge-case: index === lastIndex
    // (i.e. the end of the overall tokensString)
    if (index === tokenGroups.length - 1) {
      const groupTokens = _.split(group, separator);
      // CASE: no tokens here at all: the tokenString effectively ENDED with the grouped token groups[lastIndex]
      if (groupTokens.length === 1 && !groupTokens[0].length) {
        return [];
      }
      // CASE: there is no separator between the last grouped token and this group string
      if (groupTokens.length === 1) {
        throw errors.INVALID_TOKENIZER_GROUPING();
      }
      // CASE: more than one groupToken & FIRST one is NOT empty
      // (which if would have to be for the '.' to be next to the grouping char)
      if (groupTokens[0] !== '') {
        throw errors.INVALID_TOKENIZER_GROUPING();
      }
      // otherwise just return the groupTokens (minus the first, empty, groupToken)
      const actualTokens = groupTokens.slice(1, groupTokens.length);
      return _.map(actualTokens, (token) => ({value: token, isGrouped: false}));
    }
    // DONE WITH EDGE CASES
    // the normal case: a group in the middle, sandwiched between two grouped tokens
    const groupTokens = _.split(group, separator);
    // the first AND last groups MUST be empty for these squished guys
    if (groupTokens.length < 2 || groupTokens[0].length || groupTokens[groupTokens.length - 1].length) {
      throw errors.INVALID_TOKENIZER_GROUPING();
    }
    // and done (just remove the edges)
    const actualTokens = groupTokens.slice(1, groupTokens.length - 1);
    return _.map(actualTokens, (token) => ({value: token, isGrouped: false}));
  }));
  return detailedTokens;
};

const tokenize = (tokenString, separator, grouper) => {
  const detailedTokens = detailedTokenize(tokenString, separator, grouper);
  const tokens = _.map(detailedTokens, (detailedToken) => detailedToken.value);
  return tokens;
};

module.exports = {
  detailedTokenize,
  tokenize,
  errors,
};
