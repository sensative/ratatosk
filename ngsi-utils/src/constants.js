/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const QUERY_PREDICATE_TYPES = {
  BINARY: 'binary',
  UNARY: 'unary',
};

// how should attr.value and md.value data be validated
// fiware-orion - validates all fields
// permissive - does not validate chars in value fields
const DATA_VALIDATION_MODES = {
  // strings have to follow special fiware rules
  ORION: 'ORION',
  // only ngsi spec rules enforced (attr and md values are not (very) validated)
  PERMISSIVE: 'PERMISSIVE',
  // attribute and md names restricted from using substrings that interfere with sql ops
  RESTRICTED: 'RESTRICTED',
  // both ORION and RESTRICTED
  ORION_RESTRICTED: 'ORION_RESTRICTED',
};
const DEFAULT_DATA_VALIDATION_MODE = DATA_VALIDATION_MODES.PERMISSIVE;

// additional query params recognized by utils
const ADDED_QUERY_PARAM_KEYS = {
  TOTAL_COUNT: 'count',
  REPRESENTATION_TYPE: 'representationType',
  STRICT_APPEND: 'append',
};

// additional requestArg props recognized by utils
const ENTITY_REQUEST_ARG_KEYS = {
  HEADERS: 'headers',
  SERVICE_PATH: 'servicePath',
  SERVICE_PATHS: 'servicePaths',
  PARAMS: 'params',
  ENTITY: 'entity',
  ENTITY_ID: 'entityId',
  ATTR_NAME: 'attrName',
  ATTR: 'attr',
  ATTR_VALUE: 'attrValue',
  ATTRS: 'attrs',
  USER_ID: 'userId',
};

const DB_ENTITY_ARG_KEYS = {
  SERVICE_PATHS: 'servicePaths',
  ENTITY: 'entity',
  ENTITY_ID: 'entityId',
  ATTRS: 'attrs',
  ATTR: 'attr',
  ATTR_VALUE: 'attrValue',
  ATTR_NAME: 'attrName',
  PARAMS: 'params',
  UPSERT: 'upsert',
  CUSTOM: 'custom',
};

const DB_ARG_PARAMS_KEYS = {
  ENTITY_TYPE: 'type',
  ENTITY_TYPE_PATTERN: 'typePattern',
  ENTITY_ID: 'id',
  ENTITY_ID_PATTERN: 'idPattern',
  ORDER_BY: 'orderBy',
  ATTRIBUTE_QUERY: 'q',
  METADATA_QUERY: 'mq',
  ATTRS_LIST: 'attrs',
  METADATA_LIST: 'metadata',
  // instead of 'options'
  TOTAL_COUNT: 'count',
  STRICT_APPEND: 'append',
  REPRESENTATION_TYPE: 'representationType',
  // and geo stuffs
  GEOMETRY: 'geometry',
  COORDS: 'coords',
  GEOREL: 'georel',
  // standard paging options
  PAGE_LIMIT: 'limit',
  PAGE_OFFSET: 'offset',
  // Non-standard paging options
  // needed for performant paging via keyset-paging
  PAGE_ITEM_ID: 'pageItemId',
  PAGE_DIRECTION: 'pageDirection',
};

// these are the keys allowed for utils (some extras in there..)
const BULK_QUERY_ARG_KEYS = {
  HEADERS: 'headers',
  SERVICE_PATH: 'servicePath',
  SERVICE_PATHS: 'servicePaths',
  PAYLOAD: 'payload',
  USER_ID: 'userId',
};

// the keys allowed in the dbArg for bulk query
const DB_BULK_QUERY_ARG_KEYS = {
  SERVICE_PATHS: 'servicePaths',
  PAYLOAD: 'payload',
};

// these are the keys allowed for utils (some extras in there..)
const BULK_UPDATE_ARG_KEYS = {
  HEADERS: 'headers',
  SERVICE_PATH: 'servicePath',
  SERVICE_PATHS: 'servicePaths',
  PAYLOAD: 'payload',
  USER_ID: 'userId',
};

// the keys allowed in the dbArg for bulk query
const DB_BULK_UPDATE_ARG_KEYS = {
  SERVICE_PATHS: 'servicePaths',
  PAYLOAD: 'payload',
};

// SUBSCRIPTIONS

const SUBSCRIPTION_ARG_KEYS = {
  HEADERS: 'headers',
  SERVICE_PATH: 'servicePath',
  SERVICE_PATHS: 'servicePaths',
  PAYLOAD: 'payload',
  SUBSCRIPTION_ID: 'subscriptionId',
};
const DB_SUBSCRIPTION_ARG_KEYS = {
  SERVICE_PATHS: 'servicePaths',
  PAYLOAD: 'payload',
  SUBSCRIPTION_ID: 'subscriptionId',
};

// REGISTRATIONS
const REGISTRATION_ARG_KEYS = {
  HEADERS: 'headers',
  SERVICE_PATH: 'servicePath',
  SERVICE_PATHS: 'servicePaths',
  PAYLOAD: 'payload',
  REGISTRATION_ID: 'registrationId',
};
const DB_REGISTRATION_ARG_KEYS = {
  SERVICE_PATHS: 'servicePaths',
  PAYLOAD: 'payload',
  REGISTRATION_ID: 'registrationId',
};

const STORAGE_API_KEYS = {
  // root queries
  EXPLAIN_ENTITIES_QUERY: 'explain',
  GET_ENTITIES: 'getEntities',
  CREATE_ENTITY: 'createEntity',
  // /entities/:entityId
  FIND_ENTITY: 'findEntity',
  DELETE_ENTITY: 'deleteEntity',
  // /entities/:entityId/attrs
  GET_ENTITY_ATTRS: 'getEntityAttrs',
  UPSERT_ENTITY_ATTRS: 'upsertEntityAttrs',
  REPLACE_ENTITY_ATTRS: 'replaceEntityAttrs',
  PATCH_ENTITY_ATTRS: 'patchEntityAttrs',
  // /entities/:entityId/attrs/:attrName
  FIND_ENTITY_ATTR: 'findEntityAttr',
  UPDATE_ENTITY_ATTR: 'updateEntityAttr',
  DELETE_ENTITY_ATTR: 'deleteEntityAttr',
  // /entities/:entityId/attrs/:attrName/value
  FIND_ENTITY_ATTR_VALUE: 'findEntityAttrValue',
  UPDATE_ENTITY_ATTR_VALUE: 'updateEntityAttrValue',
  // bulk ops
  BULK_QUERY: 'bulkQuery',
  BULK_UPDATE: 'bulkUpdate',
  // subscriptions
  CREATE_SUBSCRIPTION: 'createSubscription',
  GET_SUBSCRIPTIONS: 'getSubscriptions',
  FIND_SUBSCRIPTION: 'findSubscription',
  DELETE_SUBSCRIPTION: 'deleteSubscription',
  UPDATE_SUBSCRIPTION: 'updateSubscription',
  // notifications
  GENERATE_NOTIFICATIONS: 'generateNotifications',
  ACKNOWLEDGE_NOTIFICATION: 'acknowledgeNotification',
};

const NGSI_SQL_QUERY_TYPES = {
  ATTRIBUTE_QUERY: 'q',
  METADATA_QUERY: 'mq',
};


module.exports = {
  QUERY_PREDICATE_TYPES,
  DATA_VALIDATION_MODES,
  DEFAULT_DATA_VALIDATION_MODE,
  ADDED_QUERY_PARAM_KEYS,
  ENTITY_REQUEST_ARG_KEYS,
  DB_ENTITY_ARG_KEYS,
  DB_ARG_PARAMS_KEYS,
  BULK_QUERY_ARG_KEYS,
  DB_BULK_QUERY_ARG_KEYS,
  BULK_UPDATE_ARG_KEYS,
  DB_BULK_UPDATE_ARG_KEYS,
  SUBSCRIPTION_ARG_KEYS,
  DB_SUBSCRIPTION_ARG_KEYS,
  REGISTRATION_ARG_KEYS,
  DB_REGISTRATION_ARG_KEYS,
  STORAGE_API_KEYS,
  NGSI_SQL_QUERY_TYPES,
};
