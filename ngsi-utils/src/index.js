/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

// constants used in the tools (some might move to ngsi-constants?)
const constants = require('./constants');

// I just like these so I'm gonna keep em
const assertThrows = require('./libs/assert-throws');
const assertRejects = require('./libs/assert-rejects');
// and these ones are used by other utils pretty freely
const ngsiErrorGenerator = require('./libs/ngsi-error-generator');
const ngsiTokenize = require('./libs/ngsi-tokenize');

// and all the utils
const ngsiSql = require('./utils/ngsi-sql');
const ngsiLocationUtils = require('./utils/ngsi-location-utils');
const ngsiQueryParamsUtils = require('./utils/ngsi-query-params-utils');
const ngsiDatumValidator = require('./utils/ngsi-datum-validator');
const ngsiDateTimeUtils = require('./utils/ngsi-date-time-utils');
const ngsiEntityValidator = require('./utils/ngsi-entity-validator');
const ngsiHeaderUtils = require('./utils/ngsi-header-utils');
const ngsiMockStorage = require('./utils/ngsi-mock-storage');
const ngsiQueryResponseUtils = require('./utils/ngsi-query-response-utils');
const ngsiServicePathUtils = require('./utils/ngsi-service-path-utils');
const ngsiAttributeValueFormatter = require('./utils/ngsi-attribute-value-formatter');
const ngsiEntityFormatter = require('./utils/ngsi-entity-formatter');
const ngsiClientRequestArgUtils = require('./utils/ngsi-client-request-arg-utils');

const ngsiResponseErrorUtils = require('./utils/ngsi-response-error-utils');
const ngsiStorageErrorUtils = require('./utils/ngsi-storage-error-utils');


module.exports = {
  constants,
  // i just like developing with these..
  assertThrows,
  assertRejects,
  // used by a number of utils. why not make it public?..
  ngsiTokenize,
  ngsiErrorGenerator,
  // utils
  ngsiSql,
  ngsiLocationUtils,
  ngsiQueryParamsUtils,
  ngsiDatumValidator,
  ngsiDateTimeUtils,
  ngsiEntityValidator,
  ngsiHeaderUtils,
  ngsiMockStorage,
  ngsiQueryResponseUtils,
  ngsiServicePathUtils,
  ngsiAttributeValueFormatter,
  ngsiEntityFormatter,
  ngsiClientRequestArgUtils,


  ngsiResponseErrorUtils,
  ngsiStorageErrorUtils,
};
