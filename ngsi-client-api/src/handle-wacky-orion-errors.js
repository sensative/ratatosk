/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const {ngsiResponseErrorUtils: {assertErrorsMatch}} = require('@ratatosk/ngsi-utils');

const {ORION_ERRORS} = require('@ratatosk/ngsi-constants');

const wackyOrionErrors = [
  ORION_ERRORS.BAD_REQ_TOO_MANY_SERVICE_PATHS,
];

// OK, so this is a little sick.. sometimes errors are returned as successes..
const handleWackyOrionErrors = async (res) => {
  const orionError = _.get(res, 'data.orionError');
  if (orionError) {
    // if doing the following throws an error, then let it be thrown.
    const convertedErr = {
      name: orionError.reasonPhrase.split(' ').join(''),
      message: orionError.details,
      status: _.toSafeInteger(orionError.code),
    };
    const matchingWackyErr = _.find(wackyOrionErrors, (wackyErr) => {
      try {
        assertErrorsMatch(convertedErr, wackyErr);
      } catch (err) {
        return false;
      }
      return true;
    });
    if (matchingWackyErr) {
      // make it look "like" a normal network error..
      return Promise.reject(matchingWackyErr);
    }
    throw new Error(`Got an unregistered orionError that came back as a success.. ${orionError.code}   ${orionError.reasonPhrase}   ${orionError.details}`);
  } else {
    return res;
  }
};

module.exports = handleWackyOrionErrors;
