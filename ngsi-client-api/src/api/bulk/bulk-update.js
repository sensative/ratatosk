/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const {
  ngsiClientRequestArgUtils: {
    extractRequestArgHeaders,
    extractBulkUpdatePayload,
  },
} = require('@ratatosk/ngsi-utils');

const createRequestConfig = (bulkUpdateArg) => {
  const headers = extractRequestArgHeaders(bulkUpdateArg);
  const payload = extractBulkUpdatePayload(bulkUpdateArg);
  return {
    url: '/op/update',
    method: 'post',
    headers,
    data: payload,
  };
};


// this guy gets validated
const bulkUpdate = (request) => {
  const req = async (bulkUpdateArg) => {
    const config = createRequestConfig(bulkUpdateArg);
    return request(config);
  };
  req.createRequestConfig = createRequestConfig;
  return req;
};

module.exports = bulkUpdate;
