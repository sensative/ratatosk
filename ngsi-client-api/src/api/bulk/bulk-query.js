/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const {
  ngsiClientRequestArgUtils: {
    extractRequestArgHeaders,
    extractBulkQueryPayload,
  },
} = require('@ratatosk/ngsi-utils');

const createRequestConfig = (bulkQueryArg) => {
  const headers = extractRequestArgHeaders(bulkQueryArg);
  const payload = extractBulkQueryPayload(bulkQueryArg);
  return {
    url: '/op/query',
    method: 'post',
    headers,
    data: payload,
  };
};


// this guy gets validated
const bulkQuery = (request) => {
  const req = async (bulkQueryArg) => {
    const config = createRequestConfig(bulkQueryArg);
    return request(config);
  };
  req.createRequestConfig = createRequestConfig;
  return req;
};

module.exports = bulkQuery;
