/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const {
  ngsiDatumValidator: {validateEntityId, validateAttributeName},
  ngsiClientRequestArgUtils: {
    validateEntityRequestArgShape,
    extractRequestArgHeaders,
    extractRequestArgAttrName,
    extractRequestArgAttr,
    extractRequestArgEntityId,
    extractRequestArgParams,
  },
} = require('@ratatosk/ngsi-utils');

const createRequestConfig = (requestArg) => {
  validateEntityRequestArgShape(requestArg);
  const headers = extractRequestArgHeaders(requestArg);
  const attrName = extractRequestArgAttrName(requestArg);
  validateAttributeName(attrName);
  const entityId = extractRequestArgEntityId(requestArg);
  validateEntityId(entityId);
  const attr = extractRequestArgAttr(requestArg);
  const params = extractRequestArgParams(requestArg);
  return {
    url: `/entities/${entityId}/attrs/${attrName}`,
    method: 'put',
    headers,
    params,
    data: attr,
  };
};

// this guy gets validated
const updateEntityAttr = (request) => {
  const req = async (requestArg) => {
    const config = createRequestConfig(requestArg);
    return request(config);
  };
  req.createRequestConfig = createRequestConfig;
  return req;
};

module.exports = updateEntityAttr;
