/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const axios = require('axios');

// entities routes
const getEntities = require('./api/entities/get-entities'); // updated
const createEntity = require('./api/entities/create-entity'); // updated
const findEntity = require('./api/entities/find-entity'); // updated
const deleteEntity = require('./api/entities/delete-entity'); // updated
const getEntityAttrs = require('./api/entities/get-entity-attrs'); // updated
const replaceEntityAttrs = require('./api/entities/replace-entity-attrs');
const upsertEntityAttrs = require('./api/entities/upsert-entity-attrs');
const patchEntityAttrs = require('./api/entities/patch-entity-attrs');
const findEntityAttr = require('./api/entities/find-entity-attr');
const updateEntityAttr = require('./api/entities/update-entity-attr');
const deleteEntityAttr = require('./api/entities/delete-entity-attr');
const findEntityAttrValue = require('./api/entities/find-entity-attr-value');
const updateEntityAttrValue = require('./api/entities/update-entity-attr-value');
// bulk op routes
const bulkQuery = require('./api/bulk/bulk-query');
const bulkUpdate = require('./api/bulk/bulk-update');
// subscriptions
const getSubscriptions = require('./api/subscriptions/get-subscriptions');
const findSubscription = require('./api/subscriptions/find-subscription');
const createSubscription = require('./api/subscriptions/create-subscription');
const deleteSubscription = require('./api/subscriptions/delete-subscription');
const updateSubscription = require('./api/subscriptions/update-subscription');
// registrations
const getRegistrations = require('./api/registrations/get-registrations');
const findRegistration = require('./api/registrations/find-registration');
const createRegistration = require('./api/registrations/create-registration');
const deleteRegistration = require('./api/registrations/delete-registration');
const updateRegistration = require('./api/registrations/update-registration');

const handleWackyOrionErrors = require('./handle-wacky-orion-errors');

const createApi = (config) => {

  const {request: ngsiRequest} = axios.create({
    baseURL: `http://${config.host}:${config.port}/v2`,
    timeout: 1000,
  });


  const api = {
    // the raw request object that is used by all other routes
    rawRequest: ngsiRequest,
    // entities routes
    getEntities: getEntities(ngsiRequest),
    findEntity: findEntity(ngsiRequest),
    createEntity: createEntity(ngsiRequest),
    deleteEntity: deleteEntity(ngsiRequest),
    getEntityAttrs: getEntityAttrs(ngsiRequest),
    patchEntityAttrs: patchEntityAttrs(ngsiRequest),
    replaceEntityAttrs: replaceEntityAttrs(ngsiRequest),
    upsertEntityAttrs: upsertEntityAttrs(ngsiRequest),
    findEntityAttr: findEntityAttr(ngsiRequest),
    updateEntityAttr: updateEntityAttr(ngsiRequest),
    deleteEntityAttr: deleteEntityAttr(ngsiRequest),
    findEntityAttrValue: findEntityAttrValue(ngsiRequest),
    updateEntityAttrValue: updateEntityAttrValue(ngsiRequest),
    // bulk op routes
    bulkQuery: bulkQuery(ngsiRequest),
    bulkUpdate: bulkUpdate(ngsiRequest),
    // subscriptions
    createSubscription: createSubscription(ngsiRequest),
    getSubscriptions: getSubscriptions(ngsiRequest),
    findSubscription: findSubscription(ngsiRequest),
    deleteSubscription: deleteSubscription(ngsiRequest),
    updateSubscription: updateSubscription(ngsiRequest),
    // subscriptions
    createRegistration: createRegistration(ngsiRequest),
    getRegistrations: getRegistrations(ngsiRequest),
    findRegistration: findRegistration(ngsiRequest),
    deleteRegistration: deleteRegistration(ngsiRequest),
    updateRegistration: updateRegistration(ngsiRequest),
  };
  // check for any wierdnesses
  return _.mapValues(api, (req, name) => {
    // rawRequest should be kept as raw as possible
    if (name === 'rawRequest') {
      return req;
    }
    const wrappedReq = async (...args) => {
      return req(...args)
        .then(handleWackyOrionErrors);
    };
    wrappedReq.createRequestConfig = req.createRequestConfig;
    return wrappedReq;
  });
};

module.exports = {
  createApi,
  handleWackyOrionErrors,
};
