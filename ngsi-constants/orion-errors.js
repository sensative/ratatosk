/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const orionErrors = {
  MULTIPLE_LOCATIONS_FORBIDDEN: {
    name: 'NoResourcesAvailable',
    message: 'You cannot use more than one geo location attribute when creating an entity [see Orion user manual]',
    status: 413,
  },
  CONTENT_LENGTH_REQUIRED: {
    name: 'ContentLengthRequired',
    message: 'Zero/No Content-Length in PUT/POST/PATCH request',
    status: 411,
  },
  UNSUPPORTED_MEDIA_TYPE_TEXT: {
    name: 'UnsupportedMediaType',
    message: 'not supported content type: text/plain',
    status: 415,
  },
  UNPROCESSABLE_ENTITY_ALREADY_EXISTS: {
    name: 'Unprocessable',
    message: 'Already Exists',
    status: 422,
  },
  UNPROCESSABLE_ATTRIBUTE_ALREADY_EXISTS: {
    name: 'Unprocessable',
    message: 'one or more of the attributes in the request already exist: [ ARRAY_LIST ]',
    status: 422,
  },
  BAD_REQ_FORBIDDEN_CHARS: {
    name: 'BadRequest',
    message: 'forbidden characters in String Filter',
    status: 400,
  },
  BAD_REQ_INVALID_DATE_FORMAT: {
    name: 'BadRequest',
    message: 'date has invalid format',
    status: 400,
  },
  BAD_REQ_INCOMPATIBLE_PARAMS_ID_PATTERN: {
    name: 'BadRequest',
    message: 'Incompatible parameters: id, IdPattern',
    status: 400,
  },
  BAD_REQ_INCOMPATIBLE_PARAMS_TYPE_PATTERN: {
    name: 'BadRequest',
    message: 'Incompatible parameters: type, typePattern',
    status: 400,
  },
  BAD_REQ_SERVICE_PATH_UPDATE_MULTI_PATH: {
    name: 'BadRequest',
    message: 'more than one service path in context update request',
    status: 400,
  },
  BAD_REQ_TOO_MANY_SERVICE_PATHS: {
    name: 'BadRequest',
    message: 'too many service paths - a maximum of ten service paths is allowed',
    status: 400,
  },
  BAD_REQ_SERVICE_PATH_BAD_CHAR: {
    name: 'BadRequest',
    message: 'Bad Character in Service-Path',
    status: 400,
  },
  BAD_REQ_SERVICE_PATH_TOO_MANY_COMPONENTS: {
    name: 'BadRequest',
    message: 'too many components in ServicePath',
    status: 400,
  },
  BAD_REQ_SERVICE_PATH_COMPONENT_NAME_TOO_LONG: {
    name: 'BadRequest',
    message: 'component-name too long in ServicePath',
    status: 400,
  },
  BAD_REQ_SERVICE_PATH_EMPTY_COMPONENT: {
    name: 'BadRequest',
    message: 'empty component in ServicePath',
    status: 400,
  },
  BAD_REQ_SERVICE_PATH_ILLEGAL_CHAR: {
    name: 'BadRequest',
    message: 'a component of ServicePath contains an illegal character',
    status: 400,
  },
  BAD_REQ_SERVICE_PATH_NOT_ABSOLUTE: {
    name: 'BadRequest',
    message: 'Only /absolute/ Service Paths allowed [a service path must begin with /]',
    status: 400,
  },
  BAD_REQ_TOO_SHORT_ENTITY_ID: {
    name: 'BadRequest',
    message: 'entity id length: 0, min length supported: 1',
    status: 400,
  },
  BAD_REQ_TOO_SHORT_ATTR_NAME: {
    name: 'BadRequest',
    message: 'attribute name length: 0, min length supported: 1',
    status: 400,
  },
  BAD_REQ_TOO_SHORT_ATTR_TYPE: {
    name: 'BadRequest',
    message: 'attribute type length: 0, min length supported: 1',
    status: 400,
  },
  BAD_REQ_TOO_SHORT_METADATA_TYPE: {
    name: 'BadRequest',
    message: 'metadata type length: 0, min length supported: 1',
    status: 400,
  },
  BAD_REQ_TOO_SHORT_ENTITY_TYPE: {
    name: 'BadRequest',
    message: 'entity type length: 0, min length supported: 1',
    status: 400,
  },
  BAD_REQ_INVALID_VALUE: {
    name: 'BadRequest',
    message: 'attribute value type not recognized',
    status: 400,
  },
  BAD_REQ_INVALID_JSON: {
    name: 'BadRequest',
    message: 'Neither JSON Object nor JSON Array for attribute::value',
    status: 400,
  },
  BAD_REQ_BAD_PAGINATION_OFFSET_GENERAL: {
    name: 'BadRequest',
    message: 'Bad pagination offset: /-1/ [must be a decimal number]',
    status: 400,
  },
  BAD_REQ_BAD_PAGINATION_LIMIT_NEG_ONE: {
    name: 'BadRequest',
    message: 'Bad pagination limit: /-1/ [must be a decimal number]',
    status: 400,
  },
  BAD_REQ_BAD_PAGINATION_LIMIT_ZERO: {
    name: 'BadRequest',
    message: 'Bad pagination limit: /0/ [a value of ZERO is unacceptable]',
    status: 400,
  },
  BAD_REQ_BAD_PAGINATION_LIMIT_MAX_ONE_K: {
    name: 'BadRequest',
    message: 'Bad pagination limit: /XXX/ [max: 1000]',
    status: 400,
  },
  BAD_REQ_MISSING_ID_OR_PATTERN: {
    name: 'BadRequest',
    message: 'nor /id/ nor /idPattern/ present',
    status: 400,
  },
  BAD_REQ_EMPTY_ID_PARAM: {
    name: 'BadRequest',
    message: 'Empty right-hand-side for URI param /id/',
    status: 400,
  },
  BAD_REQ_EMPTY_IDPATTERN_PARAM: {
    name: 'BadRequest',
    message: 'Empty right-hand-side for URI param /idPattern/',
    status: 400,
  },
  BAD_REQ_EMPTY_TYPE_PARAM: {
    name: 'BadRequest',
    message: 'Empty right-hand-side for URI param /type/',
    status: 400,
  },
  BAD_REQ_EMPTY_TYPEPATTERN_PARAM: {
    name: 'BadRequest',
    message: 'Empty right-hand-side for URI param /typePattern/',
    status: 400,
  },
  BAD_REQ_EMPTY_ATTRS_PARAM: {
    name: 'BadRequest',
    message: 'Empty right-hand-side for URI param /attrs/',
    status: 400,
  },
  BAD_REQ_EMPTY_METADATA_PARAM: {
    name: 'BadRequest',
    message: 'Empty right-hand-side for URI param /metadata/',
    status: 400,
  },
  BAD_REQ_EMPTY_Q_PARAM: {
    name: 'BadRequest',
    message: 'Empty right-hand-side for URI param /q/',
    status: 400,
  },
  BAD_REQ_EMPTY_MQ_PARAM: {
    name: 'BadRequest',
    message: 'Empty right-hand-side for URI param /mq/',
    status: 400,
  },
  BAD_REQ_EMPTY_ORDERBY_PARAM: {
    name: 'BadRequest',
    message: 'Empty right-hand-side for URI param /orderBy/',
    status: 400,
  },
  BAD_REQ_EMPTY_OFFSET_PARAM: {
    name: 'BadRequest',
    message: 'Empty right-hand-side for URI param /offset/',
    status: 400,
  },
  BAD_REQ_EMPTY_LIMIT_PARAM: {
    name: 'BadRequest',
    message: 'Empty right-hand-side for URI param /limit/',
    status: 400,
  },
  BAD_REQ_EMPTY_OPTIONS_PARAM: {
    name: 'BadRequest',
    message: 'Empty right-hand-side for URI param /options/',
    status: 400,
  },
  BAD_REQ_INVALID_OPTIONS_PARAM: {
    name: 'BadRequest',
    message: 'Invalid value for URI param /options/',
    status: 400,
  },
  BAD_REQ_INVALID_CHAR_IN_PARAM: {
    name: 'BadRequest',
    message: 'invalid character in URI parameter',
    status: 400,
  },
  BAD_REQ_INVALID_CHAR_IN_PARAM_Q: {
    name: 'BadRequest',
    message: 'invalid character found in URI param /q/',
    status: 400,
  },
  BAD_REQ_INVALID_CHAR_IN_PARAM_MQ: {
    name: 'BadRequest',
    message: 'invalid character found in URI param /mq/',
    status: 400,
  },
  BAD_REQ_INVALID_CHAR_IN_ATTR_VALUE: {
    name: 'BadRequest',
    message: 'Invalid characters in attribute value',
    status: 400,
  },
  BAD_REQ_INVALID_CHAR_IN_ENTITY_ID: {
    name: 'BadRequest',
    message: 'Invalid characters in entity id',
    status: 400,
  },
  BAD_REQ_INVALID_CHAR_IN_ENTITY_TYPE: {
    name: 'BadRequest',
    message: 'Invalid characters in entity type',
    status: 400,
  },
  BAD_REQ_OP_LESSTHAN_ON_BOOL: {
    name: 'BadRequest',
    message: 'values of type /Bool/ not supported for operator /LessThan/',
    status: 400,
  },
  BAD_REQ_OP_LESSTHANOREQUAL_ON_BOOL: {
    name: 'BadRequest',
    message: 'values of type /Bool/ not supported for operator /LessThanOrEqual/',
    status: 400,
  },
  BAD_REQ_OP_GREATERTHAN_ON_BOOL: {
    name: 'BadRequest',
    message: 'values of type /Bool/ not supported for operator /GreaterThan/',
    status: 400,
  },
  BAD_REQ_OP_GREATERTHANOREQUAL_ON_BOOL: {
    name: 'BadRequest',
    message: 'values of type /Bool/ not supported for operator /GreaterThanOrEqual/',
    status: 400,
  },
  BAD_REQ_OP_LESSTHAN_ON_NULL: {
    name: 'BadRequest',
    message: 'values of type /Null/ not supported for operator /LessThan/',
    status: 400,
  },
  BAD_REQ_OP_LESSTHANOREQUAL_ON_NULL: {
    name: 'BadRequest',
    message: 'values of type /Null/ not supported for operator /LessThanOrEqual/',
    status: 400,
  },
  BAD_REQ_OP_GREATERTHAN_ON_NULL: {
    name: 'BadRequest',
    message: 'values of type /Null/ not supported for operator /GreaterThan/',
    status: 400,
  },
  BAD_REQ_OP_GREATERTHANOREQUAL_ON_NULL: {
    name: 'BadRequest',
    message: 'values of type /Null/ not supported for operator /GreaterThanOrEqual/',
    status: 400,
  },
  BAD_REQ_EMPTY_Q_ITEM: {
    name: 'BadRequest',
    message: 'empty q-item',
    status: 400,
  },
  BAD_REQ_EMPTY_Q_ITEM_WHITESPACE: {
    name: 'BadRequest',
    message: 'empty q-item &#40;only whitespace&#41;',
    status: 400,
  },
  BAD_REQ_NO_METADATA_AT_ALL: {
    name: 'BadRequest',
    message: 'no metadata in right-hand-side of q-item',
    status: 400,
  },
  BAD_REQ_NO_METADATA_NAME: {
    // This one is wierd. It showed up but after messing around I cannot recreate it..
    name: 'BadRequest',
    message: 'no metadata name - not valid for metadata filters',
    status: 400,
  },
  BAD_REQ_ENTITY_ID_IN_PAYLOAD: {
    name: 'BadRequest',
    message: 'entity id specified in payload',
    status: 400,
  },
  BAD_REQ_ENTITY_TYPE_IN_PAYLOAD: {
    name: 'BadRequest',
    message: 'entity type specified in payload',
    status: 400,
  },
  BAD_REQ_INVALID_ENTITY_ID_TYPE: {
    name: 'BadRequest',
    message: 'Invalid JSON type for entity id',
    status: 400,
  },
  BAD_REQ_INVALID_ENTITY_TYPE_TYPE: {
    name: 'BadRequest',
    message: 'Invalid JSON type for entity type',
    status: 400,
  },
  BAD_REQ_EMPTY_PAYLOAD: {
    name: 'BadRequest',
    message: 'empty payload',
    status: 400,
  },
  BAD_REQ_UNKNOWN_PAYLOAD_KEY: {
    name: 'BadRequest',
    message: 'Unrecognized field in JSON payload: /uknownKey/',
    status: 400,
  },
  BAD_REQ_NO_RELEVANT_FIELDS: {
    name: 'BadRequest',
    message: 'Invalid JSON payload, no relevant fields found',
    status: 400,
  },
  BAD_REQ_ATTR_MUST_BE_JSON: {
    name: 'BadRequest',
    message: 'attribute must be a JSON object, unless keyValues option is used',
    status: 400,
  },
  BAD_REQ_UNRECOGNIZED_ATTR_PROP: {
    name: 'BadRequest',
    message: 'unrecognized property for context attribute',
    status: 400,
  },
  BAD_REQ_INVALID_GEO_COORDS: {
    name: 'BadRequest',
    message: 'error parsing location attribute: geo coordinates format error [see Orion user manual]: -91,0',
    status: 400,
  },
  BAD_REQ_INVALID_GEOMETRY_WITH_GEOREL_NEAR: {
    name: 'BadRequest',
    message: 'Invalid query: georel /near/ used with geometry different than point',
    status: 400,
  },
  BAD_REQ_INVALID_POINT_GEOMETRY_WITH_GEOREL_COVEREDBY: {
    name: 'BadRequest',
    message: 'Invalid query: point geometry cannot be used with coveredBy georel',
    status: 400,
  },
  BAD_REQ_INVALID_LINE_GEOMETRY_WITH_GEOREL_COVEREDBY: {
    name: 'BadRequest',
    message: 'Invalid query: line geometry cannot be used with coveredBy georel',
    status: 400,
  },
  PARSE_ERROR_INVALID_JSON: {
    name: 'ParseError',
    message: 'Errors found in incoming JSON buffer',
    status: 400,
  },
  FIND_ERROR_TOO_MANY: {
    name: 'TooManyResults',
    message: 'More than one matching entity. Please refine your query',
    status: 409,
  },
  FIND_ERROR_ENTITY_MISSING: {
    message: 'The requested entity has not been found. Check type and id',
    name: 'NotFound',
    status: 404,
  },
  FIND_ERROR_ATTR_MISSING: {
    message: 'The entity does not have such an attribute',
    name: 'NotFound',
    status: 404,
  },
  INTERNAL_SERVER_ERROR_BAD_MONGO_QUERY: {
    message: 'Error at querying MongoDB',
    name: 'InternalServerError',
    status: 500,
  },
};


const additionalErrors = {
  UNPROCESSABLE_MULTIPLE_GEO_WITHOUT_DEFAULT: {
    name: 'Unprocessable',
    message: 'Multiple geo attrs exist without any "defaultLocation" set',
    status: 422,
  },
  BAD_REQ_OP_LESSTHAN_NGSI: {
    name: 'BadRequest',
    message: 'Invalid value for operator "<" (i.e. LessThan)',
    status: 400,
  },
  BAD_REQ_OP_LESSTHANOREQUAL_NGSI: {
    name: 'BadRequest',
    message: 'Invalid value for operator "<=" (i.e. LessThanOrEqual)',
    status: 400,
  },
  BAD_REQ_OP_GREATERTHAN_NGSI: {
    name: 'BadRequest',
    message: 'Invalid value for operator "<" (i.e. GreaterThan)',
    status: 400,
  },
  BAD_REQ_OP_GREATERTHANOREQUAL_NGSI: {
    name: 'BadRequest',
    message: 'Invalid value for operator "<=" (i.e. GreaterThanOrEqual)',
    status: 400,
  },
  BAD_REQ_OP_MATCHPATTERN_NGSI: {
    name: 'BadRequest',
    message: 'Invalid value for operator "~=" (i.e. MatchPattern) - only strings allowed',
    status: 400,
  },
  BAD_REQ_ATTRS_PARAM_DUPLICATE: {
    name: 'BadRequest',
    message: 'Duplicate attr entries are not allowed in the attrs param',
    status: 400,
  },
  BAD_REQ_ATTRS_PARAM_INVALID_ATTRNAME: {
    name: 'BadRequest',
    message: 'The attrs param items must be either "*", a builtin attr, or generally a valid attribute name',
    status: 400,
  },
  BAD_REQ_METADATA_PARAM_DUPLICATE: {
    name: 'BadRequest',
    message: 'Duplicate metadata entries are not allowed in the metadata param',
    status: 400,
  },
  BAD_REQ_METADATA_PARAM_INVALID_MDNAME: {
    name: 'BadRequest',
    message: 'The metadata param items must be either "*", a builtin metadatum, or generally a valid metadata name',
    status: 400,
  },
  BAD_REQ_ORDERBY_PARAM_DUPLICATE: {
    name: 'BadRequest',
    message: 'Duplicate orderBy items (regardless of negation) are not allowed in the orderBy param',
    status: 400,
  },
  BAD_REQ_ORDERBY_PARAM_INVALID_ITEM: {
    name: 'BadRequest',
    message: 'The orderBy param item must be either id, type, , a builtin attr, or generally a valid attribute name',
    status: 400,
  },
  BAD_REQ_INVALID_SQL_ATTRIBUTE_TOKEN: {
    name: 'BadRequest',
    message: 'Invalid attribute token in q (or mq) query',
    status: 400,
  },
  BAD_REQ_INVALID_SQL_METADATA_TOKEN: {
    name: 'BadRequest',
    message: 'Invalid metadata token in mq query',
    status: 400,
  },
  BAD_REQ_SERVICE_PATH_EMPTY: {
    name: 'BadRequest',
    message: 'Invalid servicePath: cannot be empty',
    status: 400,
  },
  BAD_REQ_INVALID_PAGE_LIMIT_PARAM: {
    name: 'BadRequest',
    message: 'Invalid limit param value. Must be an integer > 1 if included (or string variant)',
    status: 400,
  },
  BAD_REQ_INVALID_PAGE_OFFSET_PARAM: {
    name: 'BadRequest',
    message: 'Invalid limit offset value. Must be an integer >= 1 if included (or string variant)',
    status: 400,
  },
  BAD_REQ_INVALID_PAGING_ID_PARAM: {
    name: 'BadRequest',
    message: 'Invalid keyset paging id param. Must be a string (that is castable to an appropriate id)',
    status: 400,
  },
  UNPROCESSABLE_ATTRIBUTE_COLLIDES_WITH_REGISTRATION: {
    name: 'Unprocessable',
    message: 'one or more of the attributes in the request collides with a registration',
    status: 422,
  },
  BAD_REQ_INVALID_GEOMETRY_WITH_GEOREL_COVEREDBY: {
    name: 'BadRequest',
    message: 'Invalid geometry, only polygons may be used with coveredBy georel',
    status: 400,
  },
  BAD_REQ_INVALID_ACCESS_TYPE: {
    name: 'BadRequest',
    message: 'Invalid accessType (must be included and validatable when userAccess is enabled)',
    status: 400,
  },
  BAD_REQ_INVALID_USER_ID: {
    name: 'BadRequest',
    message: 'Invalid userId (must be included and validatable when userAccess is enabled)',
    status: 401,
  },
  BAD_REQ_INVALID_USER_TOKEN: {
    name: 'BadRequest',
    message: 'Fiware-UserToken is invalid.',
    status: 401,
  },
};


module.exports = _.assign({}, orionErrors, additionalErrors);
