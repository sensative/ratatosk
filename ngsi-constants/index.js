/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const ORION_ERRORS = require('./orion-errors');

const DEFAULT_ENTITY_TYPE = 'Thing';
const DEFAULT_VALUE_TYPES = {
  TEXT: 'Text',
  NUMBER: 'Number',
  BOOLEAN: 'Boolean',
  NONE: 'None',
  STRUCTURED_VALUE: 'StructuredValue',
};

const SPECIAL_VALUE_TYPES = {
  DATE_TIME: 'DateTime',
  GEO_POINT: 'geo:point',
  GEO_BOX: 'geo:box',
  GEO_LINE: 'geo:line',
  GEO_POLYGON: 'geo:polygon',
  GEO_JSON: 'geo:json',
};

const GEO_JSON_TYPES = {
  POINT: 'Point',
  MULTI_POINT: 'MultiPoint',
  LINE_STRING: 'LineString',
  MULTI_LINE_STRING: 'MultiLineString',
  POLYGON: 'Polygon',
  MULTI_POLYGON: 'MultiPolygon',
};


const FIELD_NAME_MIN_LENGTH = 1;
const FIELD_NAME_MAX_LENGTH = 256;

// FIELD_NAME_EXPLICITLY_INVALID_CHARS should according to spec include
// 'control characters' as well as 'white space', AS WELL AS a few specific ones
const ASCII_CONTROL_CHAR_CODES = _.times(32, (i) => i).concat([127]);
const ASCII_CONTROL_CHARS = _.map(ASCII_CONTROL_CHAR_CODES, (code) => String.fromCharCode(code));
const ASCII_NONCONTROL_CHAR_CODES = _.times(127 - 32, (i) => i + 32);
const WHITESPACE_CHARS = (() => {
  const chars = [];
  _.each(ASCII_NONCONTROL_CHAR_CODES, (code) => {
    const isWS = String.fromCharCode(code).replace(/\s+/, '') === '';
    if (isWS) {
      chars.push(String.fromCharCode(code));
    }
  });
  return chars;
})();
const FIELD_NAME_EXPLICITLY_INVALID_CHARS = ['&', '?', '/', '#'];
const FIELD_NAME_ADDITIONAL_EXPLICITLY_INVALID_CHARS = [
  '\'', // generally a good idea considering param list usages
  '.', // just plays really poorly with Mongo w.r.t. ngsi sql property paths (would need to encode the periods)
];
const FIELD_NAME_INVALID_CHARS = _.uniq(_.concat(
  FIELD_NAME_EXPLICITLY_INVALID_CHARS,
  FIELD_NAME_ADDITIONAL_EXPLICITLY_INVALID_CHARS,
  ASCII_CONTROL_CHARS,
  WHITESPACE_CHARS,
));

const QUERY_PREDICATE_SEPARATOR = ';';

const QUERY_BINARY_OPERATORS = {
  EQUALS: '==',
  UNEQUAL: '!=',
  GREATER_THAN: '>',
  LESS_THAN: '<',
  GREATER_THAN_OR_EQUALS: '>=',
  LESS_THAN_OR_EQUALS: '<=',
  MATCH_PATTERN: '~=',
  COLON_EQUALS: ':',
};

const QUERY_UNARY_OPERATORS = {
  NON_EXISTENCE: '!',
};

const BUILTIN_ATTRIBUTES = {
  CREATED_AT: {
    name: 'dateCreated',
    type: SPECIAL_VALUE_TYPES.DATE_TIME,
  },
  MODIFIED_AT: {
    name: 'dateModified',
    type: SPECIAL_VALUE_TYPES.DATE_TIME,
  },
  EXPIRES_AT: {
    name: 'dateExpires',
    type: SPECIAL_VALUE_TYPES.DATE_TIME,
  },
  FIWARE_ID: {
    name: 'fiwareId',
    type: DEFAULT_VALUE_TYPES.TEXT,
  },
};
const ILLEGAL_ATTRIBUTE_NAMES_BUILTIN = _.map(BUILTIN_ATTRIBUTES, (attr) => attr.name);
const QUERY_ATTRIBUTE_LIST_WILDCARD = '*';
const ENTITY_ID_KEY = 'id';
const ENTITY_TYPE_KEY = 'type';
const GEO_DISTANCE_KEY = 'geo:distance';
const ILLEGAL_ATTRIBUTE_NAMES_EXPLICIT = [
  ENTITY_ID_KEY,
  ENTITY_TYPE_KEY,
  GEO_DISTANCE_KEY,
  QUERY_ATTRIBUTE_LIST_WILDCARD,
];
const ILLEGAL_ATTRIBUTE_NAMES = _.uniq(_.concat(
  ILLEGAL_ATTRIBUTE_NAMES_BUILTIN,
  ILLEGAL_ATTRIBUTE_NAMES_EXPLICIT,
));

const BUILTIN_METADATA = {
  CREATED_AT: {
    name: 'dateCreated',
    type: SPECIAL_VALUE_TYPES.DATE_TIME,
  },
  MODIFIED_AT: {
    name: 'dateModified',
    type: SPECIAL_VALUE_TYPES.DATE_TIME,
  },
  PREVIOUS_VALUE: {
    name: 'previousValue',
    type: 'ANY',
  },
  ACTION_TYPE: {
    name: 'actionType',
    type: 'Text',
  },
};

const DEFAULT_LOCATION_METADATUM_NAME = 'defaultLocation';
const DEFAULT_LOCATION_METADATUM = {
  type: DEFAULT_VALUE_TYPES.BOOLEAN,
  value: true,
};

const ILLEGAL_METADATA_NAMES_BUILTIN = _.map(BUILTIN_METADATA, (attr) => attr.name);
const ILLEGAL_METADATA_NAMES_EXPLICIT = ['*'];
const ILLEGAL_METADATA_NAMES = _.uniq(_.concat(
  ILLEGAL_METADATA_NAMES_BUILTIN,
  ILLEGAL_METADATA_NAMES_EXPLICIT,
));

const QUERY_TOKEN_GROUPER_CHAR = '\'';
const QUERY_TOKEN_SEPARATOR = '.';
const QUERY_VALUE_LIST_GROUPER_CHAR = '\'';
const QUERY_VALUE_LIST_SEPARATOR = ',';
const QUERY_VALUE_RANGE_SEPARATOR = '..';

const ORION_FORBIDDEN_QUERY_CHARS = ['<', '>', '"', '\'', '=', ';', '(', ')'];
const ORION_ENTITITIES_DB_COLLECTION = 'entities';
const ORION_SUBSCRIPTIONS_DB_COLLECTION = 'csubs';
const ORION_REGISTRATIONS_DB_COLLECTION = 'registrations';

const REST_REQUEST_HEADER_KEYS = {
  ACCEPT: 'Accept',
  CONTENT_TYPE: 'Content-Type',
  SERVICE_PATH: 'Fiware-ServicePath',
  USER_ID: 'Fiware-UserId',
  USER_TOKEN: 'Fiware-UserToken',
  UPSERT: 'Fiware-Upsert',
};

const REST_RESPONSE_HEADER_KEYS = {
  CORRELATOR: 'fiware-correlator',
  TOTAL_COUNT: 'fiware-total-count',
};

const REST_PAGINATION_LIMIT_MAXIMUM = 3000;

const DEFAULT_PAGE_SIZE = 20;

const PAGING_DIRECTIONS = {
  NEXT: 'next',
  PREVIOUS: 'prev',
};

const REST_REQUEST_HEADER_VALUES = {
  JSON: 'application/json',
  TEXT: 'text/plain',
  ANY: 'application/json, text/plain, */*',
};

const ENTITY_REQUEST_PARAM_KEYS = {
  ENTITY_TYPE: 'type',
  ENTITY_TYPE_PATTERN: 'typePattern',
  ENTITY_ID: 'id',
  ENTITY_ID_PATTERN: 'idPattern',
  ORDER_BY: 'orderBy',
  ATTRIBUTE_QUERY: 'q',
  METADATA_QUERY: 'mq',
  ATTRS_LIST: 'attrs',
  METADATA_LIST: 'metadata',
  OPTIONS_PARAM: 'options',
  PAGE_LIMIT: 'limit',
  PAGE_OFFSET: 'offset',
  PAGE_ITEM_ID: 'pageItemId',
  PAGE_DIRECTION: 'pageDirection',
  // geometry
  GEOREL: 'georel',
  GEOMETRY: 'geometry',
  COORDS: 'coords',
};

const GEOMETRY_QUERY_GEOMETRY_TYPES = {
  POINT: 'point',
  BOX: 'box',
  LINE: 'line',
  POLYGON: 'polygon',
};

const GEOMETRY_QUERY_GEOREL_TYPES = {
  NEAR: 'near',
  COVERED_BY: 'coveredBy',
  INTERSECTS: 'intersects',
  EQUALS: 'equals',
  DISJOINT: 'disjoint',
};

const GEOMETRY_QUERY_GEOREL_NEAR_MODIFIERS = {
  MIN_DISTANCE: 'minDistance',
  MAX_DISTANCE: 'maxDistance',
};
const GEOMETRY_QUERY_GEOREL_MODIFIER_SEP = ';';
const GEOMETRY_QUERY_GEOREL_MODIFIER_ASSIGNMENT_SEP = ':';

const GEOMETRY_QUERY_COORDS_LAT_LON_SEP = ',';
const GEOMETRY_QUERY_COORDS_MULTI_SEP = ';';

const BULK_QUERY_PAYLOAD_KEYS = {
  ENTITIES: 'entities',
  EXPRESSION: 'expression',
  ATTRS_LIST: 'attrs',
  METADATA_LIST: 'metadata',
};

const BULK_QUERY_PAYLOAD_ENTITIES_KEYS = {
  ENTITY_TYPE: 'type',
  ENTITY_TYPE_PATTERN: 'typePattern',
  ENTITY_ID: 'id',
  ENTITY_ID_PATTERN: 'idPattern',
};

const BULK_QUERY_PAYLOAD_EXPRESSION_KEYS = {
  ATTRIBUTE_QUERY: 'q',
  METADATA_QUERY: 'mq',
  // these have not been implemented yet
  // GEOREL: 'georel',
  // GEOMETRY: 'geometry',
  // COORDS: 'coords',
};

const BULK_UPDATE_PAYLOAD_KEYS = {
  ENTITIES: 'entities',
  ACTION_TYPE: 'actionType',
};

// append, appendStrict, update, delete, or replace
const BULK_UPDATE_ACTION_TYPES = {
  APPEND: 'append',
  APPEND_STRICT: 'appendStrict',
  UPDATE: 'update',
  DELETE: 'delete',
  REPLACE: 'replace',
};

const SUBSCRIPTION_PAYLOAD_KEYS = {
  SUBSCRIPTION_ID: 'id',
  DESCRIPTION: 'description',
  SUBJECT: 'subject',
  NOTIFICATION: 'notification',
  STATUS: 'status', // this is a wierd one..
  EXPIRES: 'expires', // optional ISO8601 (if not included: permanent)
  THROTTLING: 'throttling', // optional (seconds)
};

const SUBSCRIPTION_PAYLOAD_SUBJECT_KEYS = {
  ENTITIES: 'entities',
  CONDITION: 'condition',
};

const SUBSCRIPTION_PAYLOAD_SUBJECT_CONDITION_KEYS = {
  ATTRS: 'attrs',
  EXPRESSION: 'expression',
};

const SUBSCRIPTION_PAYLOAD_NOTIFICATION_KEYS = {
  ATTRS: 'attrs',
  EXCEPT_ATTRS: 'exceptAttrs',
  HTTP: 'http',
  HTTP_CUSTOM: 'httpCustom',
  ATTRS_FORMAT: 'attrsFormat',
  METADATA: 'metadata',
};

const SUBSCRIPTION_PAYLOAD_NOTIFICATION_HTTP_KEYS = {
  URL: 'url',
};

const SUBSCRIPTION_PAYLOAD_NOTIFICATION_HTTP_CUSTOM_KEYS = {
  URL: 'url',
  HEADERS: 'headers',
  QUERIES: 'qs',
  METHOD: 'method',
  PAYLOAD: 'payload',
};

const SUBSCRIPTION_NOTIFICATION_STATE_PROPS = {
  TIMES_SENT: 'timesSent',
  LAST_NOTIFICATION: 'lastNotification',
  LAST_FAILURE: 'lastFailure',
  LAST_SUCCESS: 'lastSuccess',
};

const SUBSCRIPTION_STATUS_VALUES = {
  ACTIVE: 'active',
  INACTIVE: 'inactive',
  EXPIRED: 'expired',
  FAILED: 'failed',
};

const REGISTRATION_PAYLOAD_KEYS = {
  REGISTRATION_ID: 'id',
  DESCRIPTION: 'description',
  PROVIDER: 'provider',
  DATA_PROVIDED: 'dataProvided',
  STATUS: 'status',
  EXPIRES: 'expires',
  FORWARDING_INFORMATION: 'forwardingInformation',
};

const REGISTRATION_PROVIDER_KEYS = {
  HTTP: 'http',
  SUPPORTED_FORWARDING_MODES: 'supportedForwardingMode',
  LEGACY_FORWARDING: 'legacyForwarding', // man. orion-made-up stuff
};

const REGISTRATION_PROVIDER_HTTP_KEYS = {
  URL: 'url',
};

const REGISTRATION_DATA_PROVIDED_KEYS = {
  ENTITIES: 'entities',
  ATTRS: 'attrs',
  EXPRESSION: 'expression',
};

const REGISTRATION_FORWARDING_MODE_VALUES = {
  NONE: 'none', // not implemented yet in Orion
  QUERY: 'query', // not implemented yet in Orion
  UPDATE: 'update', // not implemented yet in Orion
  ALL: 'all', // this is the only value allowed in Orion (and is the default..)
};

const DEFAULT_REGISTRATION_FORWARDING_MODE = REGISTRATION_FORWARDING_MODE_VALUES.ALL;

const REGISTRATION_STATUS_VALUES = {
  ACTIVE: 'active',
  INACTIVE: 'inactive',
  EXPIRED: 'expired',
  FAILED: 'failed',
};

const PARAM_LIST_ITEM_SEPARATOR = ',';
const PARAM_LIST_ITEM_GROUPER = '\'';
const ORDERBY_PARAM_NEGATOR = '!';
const OPTIONS_PARAM_COUNT = 'count';
const OPTIONS_PARAM_UPSERT = 'upsert';
const OPTIONS_PARAM_APPEND = 'append';

const ENTITY_REPRESENTATION_OPTIONS = {
  EXTENDED: 'extended',
  NORMALIZED: 'normalized',
  VALUES: 'values',
  UNIQUE: 'unique',
  KEY_VALUES: 'keyValues',
  PEEK: 'peek',
};
const DEFAULT_ENTITY_REPRESENTATION_TYPE = ENTITY_REPRESENTATION_OPTIONS.NORMALIZED;
const SERVICE_PATH_SEP = '/';
const SERVICE_PATH_WILDCARD = '#';
const SERVICE_PATH_DEFAULT_UPDATE_PATH = SERVICE_PATH_SEP; // i.e. "/"
const SERVICE_PATH_DEFAULT_QUERY_PATH = `${SERVICE_PATH_SEP}${SERVICE_PATH_WILDCARD}`; // i.e. "/#"
const SERVICE_PATH_DISJOINT_SEP = ',';
const SERVICE_PATH_MAX_LEVELS = 10;
const SERVICE_PATH_MAX_LEVEL_CHARS = 50;
const SERVICE_PATH_MIN_LEVEL_CHARS = 1;
const SERVICE_PATH_MAX_DISJOINT_ALLOWED = 10;
const SERVICE_PATH_MAX_DISJOINT_ALLOWED_FOR_UPDATES = 1;
const SERVICE_PATH_LEVEL_REGEX = /^[_a-z0-9]+$/i; // same info as allowed_chars
const SERVICE_PATH_ALLOWED_CHARS = _.concat(
  ['_'], // underscore was explicitly allowed
  '0123456789'.split(''), // numerals
  'abcdefghijklmnopqrstuvwxyz'.split(''), // lower-case
  'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split(''), // upper-case
);

const NGSI_DATETIME_FORMAT = 'YYYY-MM-DD[T]HH:mm:ss.SSS[Z]';
const NGSI_DATETIME_REGEX = /(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}).(\d{3})Z/;
const ORION_DATETIME_REGEX = /(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}).(\d{2})Z/;

module.exports = {
  DEFAULT_ENTITY_TYPE,
  DEFAULT_VALUE_TYPES,
  SPECIAL_VALUE_TYPES,
  GEO_JSON_TYPES,
  BUILTIN_ATTRIBUTES,
  BUILTIN_METADATA,
  DEFAULT_LOCATION_METADATUM_NAME,
  DEFAULT_LOCATION_METADATUM,
  FIELD_NAME_MIN_LENGTH,
  FIELD_NAME_MAX_LENGTH,
  FIELD_NAME_INVALID_CHARS,
  QUERY_ATTRIBUTE_LIST_WILDCARD,
  ENTITY_ID_KEY,
  ENTITY_TYPE_KEY,
  GEO_DISTANCE_KEY,
  ILLEGAL_ATTRIBUTE_NAMES,
  ILLEGAL_METADATA_NAMES,
  QUERY_PREDICATE_SEPARATOR,
  QUERY_BINARY_OPERATORS,
  QUERY_UNARY_OPERATORS,
  QUERY_TOKEN_GROUPER_CHAR,
  QUERY_TOKEN_SEPARATOR,
  QUERY_VALUE_LIST_GROUPER_CHAR,
  QUERY_VALUE_LIST_SEPARATOR,
  QUERY_VALUE_RANGE_SEPARATOR,
  ORION_FORBIDDEN_QUERY_CHARS,
  ORION_ENTITITIES_DB_COLLECTION,
  ORION_SUBSCRIPTIONS_DB_COLLECTION,
  ORION_REGISTRATIONS_DB_COLLECTION,
  ORION_ERRORS,
  REST_REQUEST_HEADER_KEYS,
  REST_RESPONSE_HEADER_KEYS,
  REST_PAGINATION_LIMIT_MAXIMUM,
  DEFAULT_PAGE_SIZE,
  PAGING_DIRECTIONS,
  REST_REQUEST_HEADER_VALUES,
  ENTITY_REQUEST_PARAM_KEYS,
  GEOMETRY_QUERY_GEOMETRY_TYPES,
  GEOMETRY_QUERY_COORDS_LAT_LON_SEP,
  GEOMETRY_QUERY_COORDS_MULTI_SEP,
  GEOMETRY_QUERY_GEOREL_TYPES,
  GEOMETRY_QUERY_GEOREL_NEAR_MODIFIERS,
  GEOMETRY_QUERY_GEOREL_MODIFIER_SEP,
  GEOMETRY_QUERY_GEOREL_MODIFIER_ASSIGNMENT_SEP,
  BULK_QUERY_PAYLOAD_KEYS,
  BULK_QUERY_PAYLOAD_ENTITIES_KEYS,
  BULK_QUERY_PAYLOAD_EXPRESSION_KEYS,
  BULK_UPDATE_PAYLOAD_KEYS,
  BULK_UPDATE_ACTION_TYPES,
  SUBSCRIPTION_PAYLOAD_KEYS,
  SUBSCRIPTION_PAYLOAD_SUBJECT_KEYS,
  SUBSCRIPTION_PAYLOAD_SUBJECT_CONDITION_KEYS,
  SUBSCRIPTION_PAYLOAD_NOTIFICATION_KEYS,
  SUBSCRIPTION_PAYLOAD_NOTIFICATION_HTTP_KEYS,
  SUBSCRIPTION_PAYLOAD_NOTIFICATION_HTTP_CUSTOM_KEYS,
  SUBSCRIPTION_NOTIFICATION_STATE_PROPS,
  SUBSCRIPTION_STATUS_VALUES,
  REGISTRATION_PAYLOAD_KEYS,
  REGISTRATION_PROVIDER_KEYS,
  REGISTRATION_PROVIDER_HTTP_KEYS,
  REGISTRATION_DATA_PROVIDED_KEYS,
  REGISTRATION_FORWARDING_MODE_VALUES,
  DEFAULT_REGISTRATION_FORWARDING_MODE,
  REGISTRATION_STATUS_VALUES,
  PARAM_LIST_ITEM_SEPARATOR,
  PARAM_LIST_ITEM_GROUPER,
  ORDERBY_PARAM_NEGATOR,
  OPTIONS_PARAM_COUNT,
  OPTIONS_PARAM_UPSERT,
  OPTIONS_PARAM_APPEND,
  ENTITY_REPRESENTATION_OPTIONS,
  DEFAULT_ENTITY_REPRESENTATION_TYPE,
  SERVICE_PATH_SEP,
  SERVICE_PATH_WILDCARD,
  SERVICE_PATH_DEFAULT_UPDATE_PATH,
  SERVICE_PATH_DEFAULT_QUERY_PATH,
  SERVICE_PATH_DISJOINT_SEP,
  SERVICE_PATH_MAX_LEVELS,
  SERVICE_PATH_MAX_LEVEL_CHARS,
  SERVICE_PATH_MIN_LEVEL_CHARS,
  SERVICE_PATH_MAX_DISJOINT_ALLOWED,
  SERVICE_PATH_MAX_DISJOINT_ALLOWED_FOR_UPDATES,
  SERVICE_PATH_LEVEL_REGEX,
  SERVICE_PATH_ALLOWED_CHARS,
  NGSI_DATETIME_FORMAT,
  NGSI_DATETIME_REGEX,
  ORION_DATETIME_REGEX,
};
