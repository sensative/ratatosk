/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const {MongoClient} = require('mongodb');
const {generateApi} = require('./api-generator');

// MUTABLE STATE!!
const state = {
  current: null,
  isStopping: null,
  isStarting: null,
};

// this is just used as a sanity check
const assertValidState = () => {
  // at most 1 can be non-null at a time
  const hasValidSize = _.size(_.compact(state)) < 2;
  if (!hasValidSize) {
    throw new Error('DevErr: invalid mongo connection state');
  }
};

// local utility function
const connect = async (url, opts) => {
  const options = _.assign(
    {
      useNewUrlParser: true, // due to deprecation warning
      useUnifiedTopology: true, // due to deprecation warning
    },
    opts,
  );
  return new Promise((resolve, reject) => {
    // Use connect method to connect to the server
    MongoClient.connect(url, options, (err, cli) => {
      if (err) {
        reject(err);
      } else if (!cli) {
        reject(new Error('mongo client is missing'));
      } else {
        resolve(cli);
      }
    });
  });
};


// EXPORT - closes any existing (or about-to-exist) mongo connection
const stop = async () => {
  assertValidState();
  if (state.isStopping) {
    return state.isStopping;
  }
  if (state.isStarting) {
    return state.isStarting
      .then(stop);
  }
  if (!state.current) {
    return; // already stopped
  }
  // and stop the client
  const client = state.current.client;
  state.current = null;
  state.isStopping = client.close()
    .then(() => {
      state.isStopping = null;
      assertValidState();
    });
  // and done
  assertValidState();
  return state.isStopping;
};


// EXPORT - creates a new connection if one does not exist
const start = async ({url, options, dbName}) => {

  assertValidState();
  if (state.isStopping) {
    return state.isStopping
      .then(() => start({url, options, dbName}));
  }
  if (state.isStarting) {
    return state.isStarting;
  }
  if (state.current) {
    return state.current.api;
  }
  // start the connection and return the api
  state.isStarting = connect(url, options, dbName)
    .then((client) => {
      const connErr = () => new Error('Connection has been closed');
      const isAlive = async () => {
        if (client !== _.get(state, 'current.client')) {
          throw connErr();
        }
      };
      const db = client.db(dbName);
      const api = generateApi(db, isAlive);
      state.current = {client, db, api};
      state.isStarting = null;
      assertValidState();
      return state.current.api;
    });
  // and done
  assertValidState();
  return state.isStarting;
};

// EXPORT - stops any existing mongo connection, then starts a new one
const restart = async ({url, options, dbName}) => {
  await stop();
  return start({url, options, dbName});
};

const getCurrentDatabase = () => {
  return _.get(state, 'current.db');
};

module.exports = {
  start,
  stop,
  restart,
  getCurrentDatabase,
};
