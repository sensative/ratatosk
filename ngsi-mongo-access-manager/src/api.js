/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {
  getValidatedToken,
  getValidatedFilter,
} = require('./validators');

const createToken = (collection) => async (token) => {
  const validatedToken = getValidatedToken(token);
  const res = await collection.insertOne(validatedToken);
  return _.get(res, 'ops.0');
};

const getTokens = (collection) => async (filter) => {
  const validatedFilter = getValidatedFilter(filter);
  const docs = await collection.find(validatedFilter).toArray();
  return docs;
};

const deleteTokens = (collection) => async (filter) => {
  const validatedFilter = getValidatedFilter(filter);
  const res = await collection.deleteMany(validatedFilter);
  return res.deletedCount;
};

const countTokens = (collection) => async (filter) => {
  const validatedFilter = getValidatedFilter(filter);
  const num = await collection.countDocuments(validatedFilter);
  return num;
};

module.exports = {
  createToken,
  getTokens,
  deleteTokens,
  countTokens,
};
