/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const {PropTypes, validateProps} = require('vanilla-prop-types');
const {ObjectId} = require('mongodb');

const {
  PAGING_DIRECTIONS,
} = require('@ratatosk/ngsi-constants');

const {
  ACCESS_TYPES,
  ACCESS_TOKEN_COLLECTION,
  ACCESS_TOKEN_UNWIND_KEY,
  ACCESS_TOKEN_KEYS,
} = require('./constants');

const errors = require('./errors');

const {
  constants: {STORAGE_API_KEYS},
} = require('@ratatosk/ngsi-utils');

const matcherKeys = _.mapValues(ACCESS_TOKEN_KEYS, (key) => `$${key}`);


const mapApiKeyToAccessType = (storageApiKey) => {
  switch (storageApiKey) {

    // not applicable
    case STORAGE_API_KEYS.CREATE_ENTITY: {
      return ACCESS_TYPES.NOT_APPLICABLE;
    }

    // reads
    case STORAGE_API_KEYS.EXPLAIN_ENTITIES_QUERY:
    case STORAGE_API_KEYS.GET_ENTITIES:
    case STORAGE_API_KEYS.FIND_ENTITY:
    case STORAGE_API_KEYS.GET_ENTITY_ATTRS:
    case STORAGE_API_KEYS.FIND_ENTITY_ATTR:
    case STORAGE_API_KEYS.FIND_ENTITY_ATTR_VALUE:
    case STORAGE_API_KEYS.BULK_QUERY: {
      return ACCESS_TYPES.READ;
    }

    // writes
    case STORAGE_API_KEYS.DELETE_ENTITY:
    case STORAGE_API_KEYS.UPSERT_ENTITY_ATTRS:
    case STORAGE_API_KEYS.REPLACE_ENTITY_ATTRS:
    case STORAGE_API_KEYS.PATCH_ENTITY_ATTRS:
    case STORAGE_API_KEYS.UPDATE_ENTITY_ATTR:
    case STORAGE_API_KEYS.DELETE_ENTITY_ATTR:
    case STORAGE_API_KEYS.UPDATE_ENTITY_ATTR_VALUE:
    case STORAGE_API_KEYS.BULK_UPDATE: {
      return ACCESS_TYPES.WRITE;
    }

    // subscriptions not implemented (?)
    case STORAGE_API_KEYS.CREATE_SUBSCRIPTION:
    case STORAGE_API_KEYS.GET_SUBSCRIPTIONS:
    case STORAGE_API_KEYS.FIND_SUBSCRIPTION:
    case STORAGE_API_KEYS.DELETE_SUBSCRIPTION:
    case STORAGE_API_KEYS.UPDATE_SUBSCRIPTION: {
      throw errors.INVALID_AGG_API_KEY_SUBSCRIPTIONS();
    }
    // nor nofications
    case STORAGE_API_KEYS.GENERATE_NOTIFICATIONS:
    case STORAGE_API_KEYS.ACKNOWLEDGE_NOTIFICATION: {
      throw errors.INVALID_AGG_API_KEY_NOTIFICATIONS();
    }
    // unknown route
    default: {
      throw errors.INVALID_AGG_API_KEY_NOT_FOUND();
    }
  }
};

const getValidatedUserId = (userId) => {
  if (!ObjectId.isValid(userId)) {
    throw errors.INVALID_AGG_STAGE_USER_ID();
  }
  return new ObjectId(userId);
};

const validatePaging = validateProps({
  itemId: PropTypes.string.isRequired,
  itemValue: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  direction: PropTypes.oneOf(_.values(PAGING_DIRECTIONS)).isRequired,
}, {
  isNilable: true,
});

const generateAggregationStages = (userId, storageApiKey, representationType, pipelinePaging) => {
  const validatedUserId = getValidatedUserId(userId);
  const accessType = mapApiKeyToAccessType(storageApiKey);
  validatePaging(pipelinePaging);
  if (accessType === ACCESS_TYPES.NOT_APPLICABLE) {
    return []; // just do not do anything
  }

  // otherwise go all out
  if (!pipelinePaging) {
    const stages = [
      {
        $lookup: {
          from: ACCESS_TOKEN_COLLECTION,
          let: {fiwareId: '$_id'},
          pipeline: [{
            $match: {
              $expr: {
                $and: [
                  {$eq: [matcherKeys.ENTITY_ID, '$$fiwareId']},
                  {$eq: [matcherKeys.USER_ID, validatedUserId]},
                  {$eq: [matcherKeys.ACCESS_TYPE, accessType]},
                ],
              },
            },
          }],
          as: ACCESS_TOKEN_UNWIND_KEY,
        },
      },
      {
        $unwind: {path: '$token'},
      },
    ];
    return stages;
  } else {
    const stages = [
      {
        $lookup: {
          from: ACCESS_TOKEN_COLLECTION,
          let: {fiwareId: '$_id'},
          pipeline: [{
            $match: {
              $expr: {
                $and: [
                  {$eq: [matcherKeys.ENTITY_ID, '$$fiwareId']},
                  {$eq: [matcherKeys.USER_ID, validatedUserId]},
                  {$eq: [matcherKeys.ACCESS_TYPE, accessType]},
                ],
              },
            },
          }],
          as: ACCESS_TOKEN_UNWIND_KEY,
        },
      },
      {
        $unwind: {path: '$token'},
      },
    ];
    return stages;
  }
};

module.exports = {
  generateAggregationStages,
};
