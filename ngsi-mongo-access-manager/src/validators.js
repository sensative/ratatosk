/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const {ObjectId} = require('mongodb');

const {
  ACCESS_TOKEN_KEYS,
  ACCESS_TYPES,
} = require('./constants');

const errors = require('./errors');

const getValidatedFilter = (filter) => {
  if (_.isNil(filter)) {
    return null;
  }
  if (!_.isObject(filter)) {
    throw errors.INVALID_FILTER_OBJECT();
  }
  const keys = _.keys(filter);
  const allowedKeys = _.values(ACCESS_TOKEN_KEYS);
  const extraKeys = _.without(keys, ...allowedKeys);
  if (_.size(extraKeys)) {
    throw errors.EXTRANEOUS_FILTER_PROPS();
  }
  // check items individually
  const validatedFilter = {};
  if (_.has(filter, ACCESS_TOKEN_KEYS.TOKEN_ID)) {
    const tokenId = filter[ACCESS_TOKEN_KEYS.TOKEN_ID];
    if (!ObjectId.isValid(tokenId)) {
      throw errors.INVALID_FILTER_TOKEN_ID();
    }
    validatedFilter[ACCESS_TOKEN_KEYS.TOKEN_ID] = new ObjectId(tokenId);
  }
  if (_.has(filter, ACCESS_TOKEN_KEYS.USER_ID)) {
    const userId = filter[ACCESS_TOKEN_KEYS.USER_ID];
    if (!ObjectId.isValid(userId)) {
      throw errors.INVALID_FILTER_USER_ID();
    }
    validatedFilter[ACCESS_TOKEN_KEYS.USER_ID] = new ObjectId(userId);
  }
  if (_.has(filter, ACCESS_TOKEN_KEYS.ENTITY_ID)) {
    const entityId = filter[ACCESS_TOKEN_KEYS.ENTITY_ID];
    if (!ObjectId.isValid(entityId)) {
      throw errors.INVALID_FILTER_ENTITY_ID();
    }
    validatedFilter[ACCESS_TOKEN_KEYS.ENTITY_ID] = new ObjectId(entityId);
  }
  if (_.has(filter, ACCESS_TOKEN_KEYS.ACCESS_TYPE)) {
    const accessType = filter[ACCESS_TOKEN_KEYS.ACCESS_TYPE];
    if (!_.includes(ACCESS_TYPES, accessType)) {
      throw errors.INVALID_FILTER_ACCESS_TYPE();
    }
    validatedFilter[ACCESS_TOKEN_KEYS.ACCESS_TYPE] = accessType;
  }
  // and done
  return validatedFilter;
};

const getValidatedToken = (token) => {
  if (!_.isObject(token)) {
    throw errors.INVALID_TOKEN_OBJECT();
  }
  const keys = _.keys(token);
  const allowedKeys = [ACCESS_TOKEN_KEYS.USER_ID, ACCESS_TOKEN_KEYS.ENTITY_ID, ACCESS_TOKEN_KEYS.ACCESS_TYPE];
  const extraKeys = _.without(keys, ...allowedKeys);
  if (_.size(extraKeys)) {
    throw errors.EXTRANEOUS_TOKEN_PROPS();
  }
  // check items individually
  const userId = token[ACCESS_TOKEN_KEYS.USER_ID];
  if (!ObjectId.isValid(userId)) {
    throw errors.INVALID_TOKEN_USER_ID();
  }
  const entityId = token[ACCESS_TOKEN_KEYS.ENTITY_ID];
  if (!ObjectId.isValid(entityId)) {
    throw errors.INVALID_TOKEN_ENTITY_ID();
  }
  const accessType = token[ACCESS_TOKEN_KEYS.ACCESS_TYPE];
  if (!_.includes(ACCESS_TYPES, accessType)) {
    throw errors.INVALID_TOKEN_ACCESS_TYPE();
  }
  // and done
  const validatedFilter = {
    [ACCESS_TOKEN_KEYS.USER_ID]: new ObjectId(userId),
    [ACCESS_TOKEN_KEYS.ENTITY_ID]: new ObjectId(entityId),
    [ACCESS_TOKEN_KEYS.ACCESS_TYPE]: accessType,
  };
  return validatedFilter;
};

module.exports = {
  getValidatedToken,
  getValidatedFilter,
};
