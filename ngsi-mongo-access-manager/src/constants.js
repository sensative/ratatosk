/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const ACCESS_TOKEN_COLLECTION = 'tokens';
const ACCESS_TOKEN_UNWIND_KEY = 'token';

// this is the shape an accessToken is expected to have
const ACCESS_TOKEN_KEYS = {
  TOKEN_ID: '_id',
  USER_ID: 'userId',
  ENTITY_ID: 'entityId',
  ACCESS_TYPE: 'accessType',
};

const ACCESS_TYPES = {
  READ: 'READ',
  WRITE: 'WRITE',
  ADMIN: 'ADMIN',
  NOT_APPLICABLE: 'NOT_APPLICABLE',
};

module.exports = {
  ACCESS_TOKEN_COLLECTION,
  ACCESS_TOKEN_UNWIND_KEY,
  ACCESS_TOKEN_KEYS,
  ACCESS_TYPES,
};
