/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const api = require('./api');

const {
  ACCESS_TOKEN_COLLECTION,
} = require('./constants');


const generateApi = async (db, isAlive) => {
  // get the collection
  const tokensCollection = db.collection(ACCESS_TOKEN_COLLECTION);
  // and inject collection to activate the api
  const activeApi = _.mapValues(api, (endpoint) => {
    // we want to activate the endpoint just once
    const activeEndpoint = endpoint(tokensCollection);
    // and return a function that evaluates each time (including an isAlive check)
    if (isAlive) {
      return async (dbArg) => isAlive()
        .then(() => activeEndpoint(dbArg));
    } else {
      return async (dbArg) => activeEndpoint(dbArg);
    }
  });

  // and done
  return activeApi;
};

module.exports = {
  generateApi,
};
