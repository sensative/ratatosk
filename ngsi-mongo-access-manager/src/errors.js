/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const {
  ngsiErrorGenerator: {generateErrors},
} = require('@ratatosk/ngsi-utils');

const errorTemplates = {
  // filter
  INVALID_FILTER_OBJECT: 'the filter must be a properly formatted object if included',
  EXTRANEOUS_FILTER_PROPS: 'invalid properties included in filter (_id, userId, and entityId allowed)',
  INVALID_FILTER_TOKEN_ID: 'invalid tokenId in filter',
  INVALID_FILTER_USER_ID: 'invalid userId in filter',
  INVALID_FILTER_ENTITY_ID: 'invalid entityId in filter',
  INVALID_FILTER_ACCESS_TYPE: 'invalid accessType included in filter',
  // token
  INVALID_TOKEN_OBJECT: 'the token must be a properly formatted object',
  EXTRANEOUS_TOKEN_PROPS: 'invalid properties included in token (only userId, entityId and accessType are allowed, and all are required)',
  INVALID_TOKEN_USER_ID: 'invalid userId in token',
  INVALID_TOKEN_ENTITY_ID: 'invalid entityId in token',
  INVALID_TOKEN_ACCESS_TYPE: 'invalid accessType in token',
  // aggregation
  INVALID_AGG_STAGE_USER_ID: 'invalid userId used to generate aggregation stage',
  INVALID_AGG_API_KEY_SUBSCRIPTIONS: 'access management to subscriptions routes has not been implemented',
  INVALID_AGG_API_KEY_NOTIFICATIONS: 'access management to notification callbacks is not supported',
  INVALID_AGG_API_KEY_NOT_FOUND: 'Invalid storageApiKey. Not found in STORAGE_API_KEYS',
};
const errors = generateErrors('NGSI_MONGO_ACCESS_MANAGER_ERROR', errorTemplates);

module.exports = errors;
