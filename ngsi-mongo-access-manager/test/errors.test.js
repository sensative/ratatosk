/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');

const errors = require('../src/errors');

describe('errors', () => {

  it('there are 16 ngsiDatumValidator errors', () => {
    assert.strictEqual(_.size(errors), 15);
  });

  it('the errors have some "matches" functionality and identifier flag', () => {
    const nonmatchingErr = new Error('this one does not match nuthin');
    _.each(errors, (generator) => {
      const err = generator();
      assert(err.isNgsiMongoAccessManagerError);
      assert(generator.matches(err));
      assert(generator.matches(err.message));
      assert(!generator.matches(nonmatchingErr));
      assert(!generator.matches(nonmatchingErr.message));
    });
  });

});
