/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const {MongoMemoryServer} = require('mongodb-memory-server');
const path = require('path');
const {lint} = require('@ratatosk/ngsi-linter');

const {setMongoUrl} = require('./test-config');

let mongoServer;
before(() => {
  mongoServer = new MongoMemoryServer();
  return mongoServer.getConnectionString()
    .then((mongoUrl) => setMongoUrl(mongoUrl));
});
after(async () => {
  await mongoServer.stop();
});

describe('testing components in ngsi-mongo-storage', () => {

  it('inside the main describe', () => 0);

  // // ---- lint
  it('linting', async () => {
    await lint(path.join(__dirname, '..'));
  });

  // // peripherals
  require('./errors.test');
  require('./validators.test');
  require('./aggregation.test');
  // // connectionManager and apiGenerator
  require('./start-stop.test');
  require('./api.test');
  require('./api-generator.test');

});
