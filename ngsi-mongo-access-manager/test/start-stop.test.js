/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const assert = require('assert');
const {ObjectId} = require('mongodb');

const {assertRejects} = require('@ratatosk/ngsi-utils');

const {connectionManager} = require('../src');

const {getMongoConfig} = require('./test-config');

const connClosedMessage = 'Connection has been closed';


describe('start-stop tests', () => {

  after(async () => {
    const api = await connectionManager.restart(getMongoConfig());
    await api.deleteTokens();
    await connectionManager.stop();
  });

  it('tester', () => 0);

  it('can start and stop', async () => {
    const token = {
      userId: new ObjectId(),
      entityId: new ObjectId(),
      accessType: 'READ',
    };
    const api = await connectionManager.start(getMongoConfig());
    const createdToken = await api.createToken(token);
    assert.strictEqual(createdToken.entityId.toString(), token.entityId.toString());
    await connectionManager.stop();
    const apiN = await connectionManager.start(getMongoConfig());
    const tokensN = await apiN.getTokens();
    assert.strictEqual(tokensN.length, 1);
    assert.strictEqual(tokensN[0].entityId.toString(), token.entityId.toString());
    const apiNN = await connectionManager.restart(getMongoConfig());
    const res0 = await apiNN.deleteTokens({_id: createdToken._id});
    assert.strictEqual(res0, 1); // deletedCount
    const res1 = await apiNN.deleteTokens({entityId: token.entityId});
    assert.strictEqual(res1, 0); // none should have been found here
    await connectionManager.stop();
    const apiNNN = await connectionManager.start(getMongoConfig());
    const tokensNNN = await apiNNN.getTokens();
    assert.strictEqual(tokensNNN.length, 0);
    const numTokensNNN = await apiNNN.countTokens();
    assert.strictEqual(numTokensNNN, 0);
    await connectionManager.stop();
    assertRejects(api.createToken(token), connClosedMessage);
    assertRejects(api.countTokens(), connClosedMessage);
  });

  it('can start, start, restart, and stop', async () => {
    const api0 = await connectionManager.start(getMongoConfig());
    const restarter = connectionManager.restart(getMongoConfig());
    // NOTE that next reject happens SYNC (is before the "await");
    assertRejects(api0.getTokens(), connClosedMessage);
    const api1 = await restarter;
    const stopper = connectionManager.stop();
    // and the same here
    assertRejects(api1.getTokens(), connClosedMessage);
    await stopper;
  });

});
