/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const {ObjectId} = require('mongodb');

const {connectionManager: {restart, stop}} = require('../src');

const {getMongoConfig} = require('./test-config');

const users = {
  joe: new ObjectId(),
  sandy: new ObjectId(),
  steve: new ObjectId(),
};

const entities = {
  barb: new ObjectId(),
  deeb: new ObjectId(),
};

const rights = ['READ', 'WRITE', 'ADMIN'];

const tokens = [];
_.each(users, (user) => {
  _.each(entities, (ent) => {
    _.each(rights, (right) => {
      const token = {userId: user, entityId: ent, accessType: right};
      tokens.push(token);
    });
  });
});

describe('api test', () => {

  let api;
  before(async () => {
    api = await restart(getMongoConfig());
  });
  after(async () => {
    const numDel = await api.deleteTokens();
    assert.strictEqual(numDel, 9);
    await stop();
  });

  it('create a bunch of tokens', async () => {
    await Promise.all(_.map(tokens, async (token) => {
      await api.createToken(token);
    }));
    const num = await api.countTokens();
    assert.strictEqual(num, 18);
  });

  it('delete all steve tokens', async () => {
    const tobeDel = await api.countTokens({userId: users.steve});
    assert.strictEqual(tobeDel, 6);
    const numDel = await api.deleteTokens({userId: users.steve});
    assert.strictEqual(numDel, 6);
    const num = await api.countTokens();
    assert.strictEqual(num, 12);
  });

  it('delete all joe tokens that are ADMIN', async () => {
    const tobeDel = await api.countTokens({userId: users.joe, accessType: 'ADMIN'});
    assert.strictEqual(tobeDel, 2);
    const numDel = await api.deleteTokens({userId: users.joe, accessType: 'ADMIN'});
    assert.strictEqual(numDel, 2);
    const numJoes = await api.countTokens({userId: users.joe});
    assert.strictEqual(numJoes, 4);
    const num = await api.countTokens();
    assert.strictEqual(num, 10);
  });

  it('delete a single sandy token by _id', async () => {
    const filter = {userId: users.sandy, accessType: 'WRITE', entityId: entities.deeb};
    const tobeDel = await api.countTokens(filter);
    assert.strictEqual(tobeDel, 1);
    const [token] = await api.getTokens(filter);
    // delete using the id this time
    const numDel = await api.deleteTokens({_id: token._id});
    assert.strictEqual(numDel, 1);
    const numSandy = await api.countTokens({userId: users.sandy});
    assert.strictEqual(numSandy, 5);
    const num = await api.countTokens();
    assert.strictEqual(num, 9);
  });

});
