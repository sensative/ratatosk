/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const {ObjectId} = require('mongodb');
const assert = require('assert');
const {assertThrows} = require('@ratatosk/ngsi-utils');

const {
  generateAggregationStages,
} = require('../src/aggregation');


describe('aggregation', () => {

  describe('generateAggregationStages', () => {

    it('create a 357 items in database', () => {
      return 0;
    });

    it('a working example', () => {
      const userId = new ObjectId().toString();
      const storageApiKey = 'getEntities';
      const stages = generateAggregationStages(userId, storageApiKey);
      const expected = [
        {
          $lookup: {
            as: 'token',
            from: 'tokens',
            let: {fiwareId: '$_id'},
            pipeline: [
              {
                $match: {
                  $expr: {
                    $and: [
                      {
                        $eq: ['$entityId', '$$fiwareId'],
                      },
                      {
                        $eq: ['$userId', new ObjectId(userId)],
                      },
                      {
                        $eq: ['$accessType', 'READ'],
                      },
                    ],
                  },
                },
              },
            ],
          },
        },
        {
          $unwind: {path: '$token'},
        },
      ];
      assert.deepStrictEqual(stages, expected);
    });

    it('a working example (WITH paging stuff)', () => {
      const userId = new ObjectId().toString();
      const storageApiKey = 'getEntities';
      const stages = generateAggregationStages(userId, storageApiKey, null, {
        itemId: 'id', itemValue: 'blaf-1123', direction: 'next',
      });

      const expected = [
        {
          $lookup: {
            as: 'token',
            from: 'tokens',
            let: {fiwareId: '$_id'},
            pipeline: [
              {
                $match: {
                  $expr: {
                    $and: [
                      {
                        $eq: ['$entityId', '$$fiwareId'],
                      },
                      {
                        $eq: ['$userId', new ObjectId(userId)],
                      },
                      {
                        $eq: ['$accessType', 'READ'],
                      },
                    ],
                  },
                },
              },
            ],
          },
        },
        {
          $unwind: {path: '$token'},
        },
      ];
      assert.deepStrictEqual(stages, expected);
    });

    it('createEntity does not use agg', () => {
      const userId = new ObjectId().toString();
      const storageApiKey = 'createEntity';
      const stages = generateAggregationStages(userId, storageApiKey);
      const expected = [];
      assert.deepStrictEqual(stages, expected);
    });

    it('some NON-working examples', () => {
      const validUserId = new ObjectId();
      const invalids = [
        // invalid userId
        {
          userId: 'derp',
          err: 'invalid userId used to generate aggregation stage',
        },
        // invalid storageApiKey
        {
          userId: validUserId,
          storageApiKey: 'derp',
          err: 'Invalid storageApiKey. Not found in STORAGE_API_KEYS',
        },
        {
          userId: validUserId,
          storageApiKey: 'createSubscription',
          err: 'access management to subscriptions routes has not been implemented',
        },
        {
          userId: validUserId,
          storageApiKey: 'generateNotifications',
          err: 'access management to notification callbacks is not supported',
        },
      ];
      _.each(invalids, ({userId, storageApiKey, err}) => {
        assertThrows(() => generateAggregationStages(userId, storageApiKey), err);
      });
    });

  });

});
