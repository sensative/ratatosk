/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const {ObjectId} = require('mongodb');

const assert = require('assert');
const {assertThrows} = require('@ratatosk/ngsi-utils');

const {
  getValidatedToken,
  getValidatedFilter,
} = require('../src/validators');

describe('validators', () => {

  describe('getValidatedToken', () => {

    it('some valid tokens', () => {
      const userId = (new ObjectId()).toString();
      const entityId = (new ObjectId()).toString();
      const valids = [
        {userId: new ObjectId(), entityId: new ObjectId(), accessType: 'READ'},
        {userId: new ObjectId(), entityId, accessType: 'READ'},
        {userId, entityId: new ObjectId(), accessType: 'READ'},
        {userId, entityId, accessType: 'READ'},
        {userId, entityId, accessType: 'WRITE'},
        {userId, entityId, accessType: 'ADMIN'},
      ];
      _.each(valids, (valid) => {
        const res = getValidatedToken(valid);
        assert(res.userId instanceof ObjectId);
        assert(res.entityId instanceof ObjectId);
      });
    });

    it('some invalid tokens', () => {
      const userId = (new ObjectId()).toString();
      const entityId = (new ObjectId()).toString();
      const invalids = [
        {
          token: null,
          err: 'the token must be a properly formatted object',
        },
        {
          token: 'asdf',
          err: 'the token must be a properly formatted object',
        },
        {
          token: {derp: 23},
          err: 'invalid properties included in token (only userId, entityId and accessType are allowed, and all are required)',
        },
        {
          token: {entityId},
          err: 'invalid userId in token',
        },
        {
          token: {entityId, userId: '1234aaaa'},
          err: 'invalid userId in token',
        },
        {
          token: {userId},
          err: 'invalid entityId in token',
        },
        {
          token: {userId, entityId: '1234aaaa'},
          err: 'invalid entityId in token',
        },
        {
          token: {userId, entityId},
          err: 'invalid accessType in token',
        },
        {
          token: {userId, entityId, accessType: 'wert'},
          err: 'invalid accessType in token',
        },
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => getValidatedToken(invalid.token), invalid.err);
      });
    });

  });

  describe('getValidatedFilter', () => {

    it('some valid filters', () => {
      const tokenId = (new ObjectId()).toString();
      const userId = (new ObjectId()).toString();
      const entityId = (new ObjectId()).toString();
      const valids = [
        null,
        undefined,
        {},
        {userId: new ObjectId(), entityId: new ObjectId(), _id: new ObjectId(), accessType: 'READ'},
        {userId, entityId, _id: tokenId, accessType: 'READ'},
        {userId},
        {entityId},
        {_id: tokenId},
        {accessType: 'READ'},
        {accessType: 'WRITE'},
        {accessType: 'ADMIN'},
      ];
      _.each(valids, (valid) => {
        const res = getValidatedFilter(valid);
        const userId = _.get(res, 'userId');
        const entityId = _.get(res, 'entityId');
        assert(_.isNil(userId) || (userId instanceof ObjectId));
        assert(_.isNil(entityId) || (entityId instanceof ObjectId));
      });
    });

    it('some invalid filters', () => {
      const invalids = [
        {
          filter: 'asdf',
          err: 'the filter must be a properly formatted object if included',
        },
        {
          filter: {derp: 23},
          err: 'invalid properties included in filter (_id, userId, and entityId allowed)',
        },
        {
          filter: {userId: '1234aaaa'},
          err: 'invalid userId in filter',
        },
        {
          filter: {entityId: '1234aaaa'},
          err: 'invalid entityId in filter',
        },
        {
          filter: {_id: '1234aaaa'},
          err: 'invalid tokenId in filter',
        },
        {
          filter: {accessType: 'asdfasdf'},
          err: 'invalid accessType included in filter',
        },
      ];
      _.each(invalids, (invalid) => {
        assertThrows(() => getValidatedFilter(invalid.filter), invalid.err);
      });
    });

  });

});
