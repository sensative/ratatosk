![Ratatosk Logo](assets/ratatosk-logo-square.png "Ratatosk")

# Ratatosk

Ratatosk is a drop in replacement for the [Fiware Orion](https://github.com/telefonicaid/fiware-orion)  context broker. 

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)

## General info
The Ratatosk project is an implementation of the Publish/Subscribe Context Broker GE. The motivation
behind the project is to provide a modern and well structured implementation the can be easily understood 
and expanded by anyone who understands Node JS.

## Technologies
Ratatosk is written entierly in Node JS.

## Setup
In order to run the  project locally on your computer you need to install:
* Node JS
* Yarn 2

### Starting up Ratatosk
The entry point for ratatosk can be found in the ngsi-folder:
```cd ngsi-server && yarn start```

