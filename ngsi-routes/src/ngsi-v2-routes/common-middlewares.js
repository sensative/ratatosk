/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {
  ngsiResponseErrorUtils: {createError},
  ngsiServicePathUtils: {
    parseServicePathsLoose,
  },
  constants: {
    DB_ARG_PARAMS_KEYS,
    DB_ENTITY_ARG_KEYS,
  },
} = require('@ratatosk/ngsi-utils');

const {
  generateAggregationStages: defaultGenerateAggregate, // this is the default
} = require('@ratatosk/ngsi-mongo-access-manager');

const {
  REST_REQUEST_HEADER_KEYS,
  ORION_ERRORS,
} = require('@ratatosk/ngsi-constants');

const {
  DB_ARG_PRIMARY_KEY,
  NGSI_API_OPTIONS_KEYS,
  USER_ID_KEY,
} = require('../constants');

const setServicePaths = (isUpdate) => (req, res, next) => {
  const rawServicePaths = _.get(req.headers, REST_REQUEST_HEADER_KEYS.SERVICE_PATH.toLowerCase());
  const servicePaths = parseServicePathsLoose(rawServicePaths, isUpdate);
  _.set(req, [DB_ARG_PRIMARY_KEY, DB_ENTITY_ARG_KEYS.SERVICE_PATHS], servicePaths);
  next();
};

// This function seems to belong here - this file seems to have become a
// header parser utility
// I can't find upsert as header in the ngsi spec, this should probably be removed
const setUpsertFlag = (req, res, next) => {
  const upsertHeader = !!_.get(req.headers, REST_REQUEST_HEADER_KEYS.UPSERT.toLowerCase());
  // upsert may be set as param (actually in the spec)
  const upsertParam = _.get(req, [DB_ARG_PRIMARY_KEY, DB_ENTITY_ARG_KEYS.UPSERT], false);
  const isUpsert = upsertHeader || upsertParam;
  if (isUpsert) {
    _.set(req, [DB_ARG_PRIMARY_KEY, DB_ENTITY_ARG_KEYS.UPSERT], true);
  }
  next();
};


const setUserId = (options, userAccessRequired) => async (req, res, next) => {
  // decompose the options
  const userAccessRequired = options[NGSI_API_OPTIONS_KEYS.USER_ACCESS];
  const userTokenParser = options[NGSI_API_OPTIONS_KEYS.USER_TOKEN_PARSER];
  // decompose the req
  const reqUserId = _.get(req, ['headers', _.toLower(REST_REQUEST_HEADER_KEYS.USER_ID)]);
  const reqUserToken = _.get(req, ['headers', _.toLower(REST_REQUEST_HEADER_KEYS.USER_TOKEN)]);
  // case 0: no access required
  if (!userAccessRequired) {
    next();
    return;
  }
  // Case 1: simple userId is used
  if (!userTokenParser && reqUserId) {
    _.set(req, USER_ID_KEY, reqUserId); // SETS THE USER_ID
    next(); // exit
    return;
  }
  // Case 2: simple userId is used, but id is missing
  if (!userTokenParser && !reqUserId) {
    next(createError(ORION_ERRORS.BAD_REQ_INVALID_USER_ID));
    return;
  }
  // case 3: token userId is used, but token is missing
  if (userTokenParser && !reqUserToken) {
    next(createError(ORION_ERRORS.BAD_REQ_INVALID_USER_TOKEN));
    return;
  }
  // case 4: parse the token
  try {
    const userId = await userTokenParser(reqUserToken);
    if (!userId) {
      next(createError(ORION_ERRORS.BAD_REQ_INVALID_USER_TOKEN));
    } else {
      _.set(req, USER_ID_KEY, userId); // SETS THE USER_ID
      next(); // exit
    }
    return;
  } catch (err) {
    next(createError(ORION_ERRORS.BAD_REQ_INVALID_USER_TOKEN));
  }
};

// rename: evaluateUserAccess
const setUserAccess = (options, storageApiKey) => async (req, res, next) => {
  // decompose the options
  const userAccessRequired = options[NGSI_API_OPTIONS_KEYS.USER_ACCESS];
  const generatePipelineStages = options[NGSI_API_OPTIONS_KEYS.CUSTOM_AGGREGATION_GENERATOR] || defaultGenerateAggregate;
  // decompose the req
  const userId = _.get(req, USER_ID_KEY);
  const representationType = _.get(req, [
    DB_ARG_PRIMARY_KEY,
    DB_ENTITY_ARG_KEYS.PARAMS,
    DB_ARG_PARAMS_KEYS.REPRESENTATION_TYPE,
  ]);
  // test if we can skip the rest of this middleware
  if (!userAccessRequired) {
    next();
    return;
  }
  // sanity check for the id
  if (!userId) {
    next(createError(ORION_ERRORS.BAD_REQ_INVALID_USER_ID));
    return;
  }
  // and generate the mongo pipeline stages
  try {
    // await in case customAggregationGenerator is asynchronous
    const stages = await generatePipelineStages(
      userId,
      storageApiKey,
      representationType,
    );
    _.set(req, [DB_ARG_PRIMARY_KEY, DB_ENTITY_ARG_KEYS.CUSTOM], stages);
    next();
  } catch (err) {
    next(err);
  }
};

module.exports = {
  setServicePaths,
  setUpsertFlag,
  setUserId,
  setUserAccess,
};
