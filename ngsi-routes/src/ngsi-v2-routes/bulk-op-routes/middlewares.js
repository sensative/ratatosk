/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {errors: composeErrors} = require('compose-middleware');

const {
  ngsiQueryParamsUtils: {
    basicParamParser: {errors: basicParamErrors},
    bulkUpdatePayloadParser: {
      formatBulkUpdatePayload,
      errors: bulkUpdatePayloadParserErrors,
    },
    bulkQueryPayloadParser: {
      formatBulkQueryPayload,
      errors: bulkQueryPayloadParserErrors,
    },
  },
  ngsiStorageErrorUtils: {errors: ngsiStorageErrors},
  ngsiResponseErrorUtils: {createError},
  constants: {
    DB_BULK_QUERY_ARG_KEYS,
    DB_BULK_UPDATE_ARG_KEYS,
  },
} = require('@ratatosk/ngsi-utils');

const {
  ORION_ERRORS,
} = require('@ratatosk/ngsi-constants');

const {DB_ARG_PRIMARY_KEY} = require('../../constants');


const setQueryPayload = (req, res, next) => {
  const inputPayload = _.get(req, 'body', {});
  const dbPayload = formatBulkQueryPayload(inputPayload);
  const paramPath = [DB_ARG_PRIMARY_KEY, DB_BULK_QUERY_ARG_KEYS.PAYLOAD];
  _.set(req, paramPath, dbPayload);
  return next();
};

const setUpdatePayload = (req, res, next) => {
  const inputPayload = _.get(req, 'body', {});
  const dbPayload = formatBulkUpdatePayload(inputPayload);
  const paramPath = [DB_ARG_PRIMARY_KEY, DB_BULK_UPDATE_ARG_KEYS.PAYLOAD];
  _.set(req, paramPath, dbPayload);
  return next();
};

const convertNgsiBasicParamErrors = (err, req, res, next) => {
  // EMPTY_ENTITY_ID_PARAM: 'The entity id param must be a non-empty string if included',
  // INVALID_ENTITY_ID_PARAM_ITEM: 'The id param contains an invalid id item',
  // INVALID_ENTITY_ID_DUPLICATES: 'Duplicates are not allowed in the entity id list',
  // INVALID_ENTITY_ID_PARAM_FORMATTING: 'The entity id param is formatted incorrectly',
  // INVALID_ENTITY_ID_LIST: 'The entityId list must be an array',
  // EMPTY_ENTITY_TYPE_PARAM: 'The entity type param must be a non-empty string if included',
  // INVALID_ENTITY_TYPE_PARAM_ITEM: 'The ityped param contains an invalid type item',
  // INVALID_ENTITY_TYPE_DUPLICATES: 'Duplicates are not allowed in the entity type list',
  // INVALID_ENTITY_TYPE_PARAM_FORMATTING: 'The entity type param is formatted incorrectly',
  // INVALID_ENTITY_TYPE_LIST: 'The entityType list must be an array',
  // EMPTY_ENTITY_ID_PATTERN: 'The idPattern param must be a non-empty string if included',
  // EMPTY_ENTITY_TYPE_PATTERN: 'The typePattern param must be a non-empty string if included',
  // EMPTY_PAGE_OFFSET_PARAM: 'The page offset param must be a non-empty string (or a number) if included',
  // INVALID_PAGE_OFFSET_PARAM: 'Invalid page offset: must be a positive integer (or zero), or a corresponding string',
  // EMPTY_PAGE_LIMIT_PARAM: 'The page limit param must be a non-empty string (or a number) if included',
  // INVALID_PAGE_LIMIT_PARAM: 'Invalid page limit: must be a positive integer (strictly non-zero), or a corresponding string',
  if (err.isNgsiBasicParamError) {
    if (basicParamErrors.EMPTY_ENTITY_ID_PARAM.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_TOO_SHORT_ENTITY_ID);
      return next(ngsiErr);
    }
    if (basicParamErrors.EMPTY_ENTITY_TYPE_PARAM.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_TOO_SHORT_ENTITY_TYPE);
      return next(ngsiErr);
    }
    // if (basicParamErrors.EMPTY_ENTITY_ID_PATTERN.matches(err)) {
    //   const ngsiErr = createError(ORION_ERRORS.);
    //   return next(ngsiErr);
    // }
    // if (basicParamErrors.EMPTY_ENTITY_TYPE_PATTERN.matches(err)) {
    //   const ngsiErr = createError(ORION_ERRORS.);
    //   return next(ngsiErr);
    // }
    if (basicParamErrors.INVALID_ENTITY_ID_PARAM_ITEM.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_INVALID_CHAR_IN_ENTITY_ID);
      return next(ngsiErr);
    }
    if (basicParamErrors.INVALID_ENTITY_ID_PARAM_FORMATTING.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_INVALID_CHAR_IN_ENTITY_ID);
      return next(ngsiErr);
    }
    if (basicParamErrors.INVALID_ENTITY_TYPE_PARAM_ITEM.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_INVALID_CHAR_IN_ENTITY_TYPE);
      return next(ngsiErr);
    }
    if (basicParamErrors.INVALID_ENTITY_TYPE_PARAM_FORMATTING.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_INVALID_CHAR_IN_ENTITY_TYPE);
      return next(ngsiErr);
    }
    // if (basicParamErrors.EMPTY_PAGE_OFFSET_PARAM.matches(err)) {
    //   const ngsiErr = createError(ORION_ERRORS.);
    //   return next(ngsiErr);
    // }
    // if (basicParamErrors.EMPTY_PAGE_LIMIT_PARAM.matches(err)) {
    //   const ngsiErr = createError(ORION_ERRORS.);
    //   return next(ngsiErr);
    // }
    // if (basicParamErrors.INVALID_PAGE_OFFSET_PARAM.matches(err)) {
    //   const ngsiErr = createError(ORION_ERRORS.);
    //   return next(ngsiErr);
    // }
    // if (basicParamErrors.INVALID_PAGE_LIMIT_PARAM.matches(err)) {
    //   const ngsiErr = createError(ORION_ERRORS.);
    //   return next(ngsiErr);
    // }

    // this list should be exhaustive (?)
    const devErr = new Error(`Unaccounted for NgsiBasicParamError (${err.name}): ${err.message}`);
    return next(devErr);
  }
  return next(err);
};

const convertUpdatePayloadParserErrors = (err, req, res, next) => {
  // INVALID_BULK_UPDATE_PAYLOAD_ARG: 'The bulk update payload arg must be an object',
  // EXTRANEOUS_UPDATE_PAYLOAD_FIELDS: 'Invalid extraneous bulk update payload field(s) present',
  // INVALID_UPDATE_PAYLOAD_ENTITIES: 'The bulk update payload entities must be a non-zero array',
  // INVALID_BULK_UPDATE_ENTITY_ITEM: 'Invalid bulk update entity was found',
  // INVALID_BULK_UPDATE_ACTION_TYPE: 'Invalid bulk update actionType',
  if (err.isNgsiBulkUpdatePayloadParserError) {
    if (bulkUpdatePayloadParserErrors.EXTRANEOUS_UPDATE_PAYLOAD_FIELDS.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_NO_RELEVANT_FIELDS);
      return next(ngsiErr);
    }
    const devErr = new Error(`DevErr: unaccounted for isNgsiBulkUpdatePayloadParserError: {name: ${err.name}, message: ${err.message}}`);
    return next(devErr);
  }
  return next(err);
};

const convertQueryPayloadParserErrors = (err, req, res, next) => {
  // INVALID_BULK_QUERY_PAYLOAD_ARG: 'The bulk query payload arg must be an object',
  // EXTRANEOUS_PAYLOAD_FIELDS: 'Invalid extraneous payload field(s) present',
  // INVALID_PAYLOAD_ENTITIES: 'The payload entities must be an array if present',
  // INVALID_PAYLOAD_ENTITIES_ITEM: 'The payload entities item must be an object',
  // INCOMPATIBLE_ENTITY_ID_PROPS: 'An entity cannot contain both "id" and "idPattern"',
  // MISSING_ENTITY_ID_PROP: 'An entity MUST have either an "id" or "idPattern" property set',
  // INCOMPATIBLE_ENTITY_TYPE_PROPS: 'An entity cannot contain both "type" and "typePattern"',
  // INVALID_PAYLOAD_EXPRESSION: 'The expression must an object if present',
  if (err.isNgsiBulkQueryPayloadParserError) {
    if (bulkQueryPayloadParserErrors.INVALID_BULK_QUERY_PAYLOAD_ARG.matches(err)) {
      // THIS GUY SEEMS IRRELEVANT DUE TO BODYPARSER (?)
      const ngsiErr = createError(ORION_ERRORS.CONTENT_LENGTH_REQUIRED);
      return next(ngsiErr);
    }
    if (bulkQueryPayloadParserErrors.EXTRANEOUS_PAYLOAD_FIELDS.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_NO_RELEVANT_FIELDS);
      return next(ngsiErr);
    }
    if (bulkQueryPayloadParserErrors.MISSING_ENTITY_ID_PROP.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_MISSING_ID_OR_PATTERN);
      return next(ngsiErr);
    }
    // missed it
    const devErr = new Error(`DevErr: unaccounted for isNgsiBulkQueryPayloadParserError: {name: ${err.name}, message: ${err.message}}`);
    return next(devErr);
  }
  return next(err);
};

const convertNgsiStorageErrors = (err, req, res, next) => {
  if (err.isNgsiStorageError) {
    if (ngsiStorageErrors.CANNOT_CREATE_ALREADY_EXISTS.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.UNPROCESSABLE_ENTITY_ALREADY_EXISTS);
      return next(ngsiErr);
    }
    if (ngsiStorageErrors.TOO_MANY_MATCHES.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.FIND_ERROR_TOO_MANY);
      return next(ngsiErr);
    }
    if (ngsiStorageErrors.MATCH_NOT_FOUND.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.FIND_ERROR_ENTITY_MISSING);
      return next(ngsiErr);
    }
    if (ngsiStorageErrors.ATTR_DOES_NOT_EXIST.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.FIND_ERROR_ATTR_MISSING);
      return next(ngsiErr);
    }
    if (ngsiStorageErrors.CANNOT_CREATE_ATTR_ALREADY_EXISTS.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.UNPROCESSABLE_ATTRIBUTE_ALREADY_EXISTS);
      return next(ngsiErr);
    }
    // missed it
    const devErr = new Error(`Unaccounted for NgsiStorageError: ${err.name}`);
    return next(devErr);
  }
  return next(err);
};

const handleErrors = composeErrors([
  convertQueryPayloadParserErrors,
  convertUpdatePayloadParserErrors,
  convertNgsiBasicParamErrors,
  convertNgsiStorageErrors,
]);

module.exports = {
  setQueryPayload,
  setUpdatePayload,
  handleErrors,
};
