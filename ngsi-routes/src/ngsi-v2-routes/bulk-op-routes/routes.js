/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const {compose} = require('compose-middleware');

const {
  setServicePaths,
  setUpsertFlag,
  setUserId,
  setUserAccess,
} = require('../common-middlewares');

const {
  setQueryPayload,
  setUpdatePayload,
  handleErrors,
} = require('./middlewares');

const {
  DB_ARG_PRIMARY_KEY,
} = require('../../constants');

const {
  constants: {STORAGE_API_KEYS},
} = require('@ratatosk/ngsi-utils');

const bulkQuery = (storage, options) => compose([
  setQueryPayload,
  setUserId(options),
  setUserAccess(options, STORAGE_API_KEYS.BULK_QUERY),
  setServicePaths(false),
  (req, res, next) => {
    const dbBulkQueryArg = _.get(req, DB_ARG_PRIMARY_KEY);
    return storage.bulkQuery(dbBulkQueryArg)
      .then((data) => res.json(data).status(200))
      .catch(next);
  },
  handleErrors,
]);

const bulkUpdate = (storage, options) => compose([
  setUpdatePayload,
  setUserId(options),
  setUserAccess(options, STORAGE_API_KEYS.BULK_UPDATE),
  setServicePaths(true),
  setUpsertFlag,
  (req, res, next) => {
    const dbBulkUpdateArg = _.get(req, DB_ARG_PRIMARY_KEY);
    return storage.bulkUpdate(dbBulkUpdateArg)
      .then((data) => res.sendStatus(203))
      .catch(next);
  },
  handleErrors,
]);

module.exports = {
  bulkQuery,
  bulkUpdate,
};
