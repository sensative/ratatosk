/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const {Router: createRouter} = require('express');

const {
  // /entities
  getEntities,
  createEntity,
  // /entities/:entityId
  findEntity,
  deleteEntity,
  // /entities/:entityId/attrs
  getEntityAttrs,
  replaceEntityAttrs,
  upsertEntityAttrs,
  patchEntityAttrs,
  // /entities/:entityId/attrs/:attrName
  findEntityAttr,
  updateEntityAttr,
  deleteEntityAttr,
  // /entities/:entityId/attrs/:attrName/value
  findEntityAttrValue,
  updateEntityAttrValue,
} = require('./routes');

const createRoutes = (storage, options) => {

  const router = createRouter();

  // add the endpoints
  router.get('/', getEntities(storage, options));
  router.post('/', createEntity(storage, options));

  router.get('/:entityId', findEntity(storage, options));
  router.delete('/:entityId', deleteEntity(storage, options));

  router.get('/:entityId/attrs', getEntityAttrs(storage, options));
  router.put('/:entityId/attrs', replaceEntityAttrs(storage, options));
  router.post('/:entityId/attrs', upsertEntityAttrs(storage, options));
  router.patch('/:entityId/attrs', patchEntityAttrs(storage, options));

  router.get('/:entityId/attrs/:attrName', findEntityAttr(storage, options));
  router.put('/:entityId/attrs/:attrName', updateEntityAttr(storage, options));
  router.delete('/:entityId/attrs/:attrName', deleteEntityAttr(storage, options));

  router.get('/:entityId/attrs/:attrName/value', findEntityAttrValue(storage, options));
  router.put('/:entityId/attrs/:attrName/value', updateEntityAttrValue(storage, options));

  // and done
  return router;
};


module.exports = {createRoutes};
