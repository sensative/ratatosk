/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const {compose} = require('compose-middleware');

const {
  setServicePaths,
  setUpsertFlag,
  setUserId,
  setUserAccess,
} = require('../common-middlewares');

const {
  triggerSubscriptions,
} = require('../../subscriptions-manager');

const {
  handleGlobalEntityUpdate,
} = require('../../global-updates-manager');

const {
  handleGlobalRegistrations,
} = require('../../global-registrations-manager');

const {
  setters: {
    setQueryParams,
    setEntityId,
    setAttrName,
    setEntity,
    setAttrs,
    setAttr,
    setAttrValue,
    setResponseLocation,
  },
  assertions: {
    assertNonTextContent,
    assertContentHasLength,
    assertNonZeroDataEntityId,
    assertNonZeroDataEntityType,
    assertDataEntityWasExcluded,
    assertNonZeroDataEntityAttrNames,
    assertNonZeroDataEntityAttrTypes,
    assertNonZeroDataEntityMetadataTypes,
  },
  handleErrors,
} = require('./middlewares');

const {
  REST_RESPONSE_HEADER_KEYS: {TOTAL_COUNT},
} = require('@ratatosk/ngsi-constants');

const {
  DB_ARG_PRIMARY_KEY,
  USER_ID_KEY,
} = require('../../constants');

const {
  constants: {STORAGE_API_KEYS},
} = require('@ratatosk/ngsi-utils');

const handleSideEffects = (storage, options, userId) => async (dbRes) => {
  // fire n forget
  await handleGlobalEntityUpdate(options, dbRes, userId)
    .catch((err) => {
      console.error(`createEntity handleGlobalEntityUpdate error: ${err.message}`);
    });
  // fire n forget
  await handleGlobalRegistrations(options, dbRes)
    .catch((err) => {
      console.error(`createEntity handleGlobalRegistrations error: ${err.message}`);
    });
  // fire n forget
  await triggerSubscriptions(storage, dbRes)
    .catch((err) => {
      console.error(`createEntity triggerSubscriptions error: ${err.message}`);
    });
  // and be done (chain it)
  return dbRes;
};


const getEntities = (storage, options) => compose([
  setQueryParams,
  setUserId(options),
  setUserAccess(options, STORAGE_API_KEYS.GET_ENTITIES),
  setServicePaths(false),
  (req, res, next) => {
    const dbArg = _.get(req, DB_ARG_PRIMARY_KEY);
    return storage.getEntities(dbArg)
      .then((data) => {
        if (!_.isNil(data.count)) {
          res.set(TOTAL_COUNT, data.count);
        }
        return res.json(data.entities).status(200);
      })
      .catch(next);
  },
  handleErrors,
]);

const createEntity = (storage, options) => compose([
  setQueryParams,
  setUserId(options),
  setUserAccess(options, STORAGE_API_KEYS.CREATE_ENTITY),
  setServicePaths(true),
  setUpsertFlag,
  assertContentHasLength,
  assertNonTextContent,
  assertNonZeroDataEntityId,
  assertNonZeroDataEntityType,
  assertNonZeroDataEntityAttrNames,
  assertNonZeroDataEntityAttrTypes,
  assertNonZeroDataEntityMetadataTypes,
  setEntity,
  setResponseLocation,
  async (req, res, next) => {
    const dbArg = _.get(req, DB_ARG_PRIMARY_KEY);
    return storage.createEntity(dbArg)
      .then(handleSideEffects(storage, options, _.get(req, USER_ID_KEY)))
      .then((dbRes) => {
        if (dbArg.upsert) {
          res.status(204).send();
        } else {
          res.status(201).send();
        }
      })
      .catch(next);
  },
  handleErrors,
]);

const findEntity = (storage, options) => compose([
  setQueryParams,
  setUserId(options),
  setUserAccess(options, STORAGE_API_KEYS.FIND_ENTITY),
  setServicePaths(false),
  setEntityId,
  (req, res, next) => {
    const dbArg = _.get(req, DB_ARG_PRIMARY_KEY);
    return storage.findEntity(dbArg)
      .then((entity) => res.json(entity).status(200))
      .catch(next);
  },
  handleErrors,
]);

const deleteEntity = (storage, options) => compose([
  setQueryParams,
  setUserId(options),
  setUserAccess(options, STORAGE_API_KEYS.DELETE_ENTITY),
  setServicePaths(true),
  assertDataEntityWasExcluded,
  setEntityId,
  (req, res, next) => {
    const dbArg = _.get(req, DB_ARG_PRIMARY_KEY);
    return storage.deleteEntity(dbArg)
      .then(handleSideEffects(storage, options, _.get(req, USER_ID_KEY)))
      .then((dbRes) => {
        res.status(204).send();
      })
      .catch(next);
  },
  handleErrors,
]);

const getEntityAttrs = (storage, options) => compose([
  setQueryParams,
  setUserId(options),
  setUserAccess(options, STORAGE_API_KEYS.GET_ENTITY_ATTRS),
  setServicePaths(false),
  setEntityId,
  (req, res, next) => {
    const dbArg = _.get(req, DB_ARG_PRIMARY_KEY);
    return storage.getEntityAttrs(dbArg)
      .then((attrs) => res.json(attrs).status(200))
      .catch(next);
  },
  handleErrors,
]);

const replaceEntityAttrs = (storage, options) => compose([
  setQueryParams,
  setUserId(options),
  setUserAccess(options, STORAGE_API_KEYS.REPLACE_ENTITY_ATTRS),
  setServicePaths(true),
  setEntityId,
  setAttrs,
  (req, res, next) => {
    const dbArg = _.get(req, DB_ARG_PRIMARY_KEY);
    return storage.replaceEntityAttrs(dbArg)
      .then(handleSideEffects(storage, options, _.get(req, USER_ID_KEY)))
      .then((dbRes) => {
        res.status(204).send();
      })
      .catch(next);
  },
  handleErrors,
]);

const upsertEntityAttrs = (storage, options) => compose([
  setQueryParams,
  setUserId(options),
  setUserAccess(options, STORAGE_API_KEYS.UPSERT_ENTITY_ATTRS),
  setServicePaths(true),
  setEntityId,
  setAttrs,
  (req, res, next) => {
    const dbArg = _.get(req, DB_ARG_PRIMARY_KEY);
    return storage.upsertEntityAttrs(dbArg)
      .then(handleSideEffects(storage, options, _.get(req, USER_ID_KEY)))
      .then((dbRes) => {
        res.status(204).send();
      })
      .catch(next);
  },
  handleErrors,
]);

const patchEntityAttrs = (storage, options) => compose([
  setQueryParams,
  setUserId(options),
  setUserAccess(options, STORAGE_API_KEYS.PATCH_ENTITY_ATTRS),
  setServicePaths(true),
  setEntityId,
  setAttrs,
  (req, res, next) => {
    const dbArg = _.get(req, DB_ARG_PRIMARY_KEY);
    return storage.patchEntityAttrs(dbArg)
      .then(handleSideEffects(storage, options, _.get(req, USER_ID_KEY)))
      .then((dbRes) => {
        res.status(204).send();
      })
      .catch(next);
  },
  handleErrors,
]);

const findEntityAttr = (storage, options) => compose([
  setQueryParams,
  setUserId(options),
  setUserAccess(options, STORAGE_API_KEYS.FIND_ENTITY_ATTR),
  setServicePaths(false),
  setEntityId,
  setAttrName,
  (req, res, next) => {
    const dbArg = _.get(req, DB_ARG_PRIMARY_KEY);
    return storage.findEntityAttr(dbArg)
      .then((attr) => {
        res.json(attr).status(200);
      })
      .catch(next);
  },
  handleErrors,
]);

const updateEntityAttr = (storage, options) => compose([
  setQueryParams,
  setUserId(options),
  setUserAccess(options, STORAGE_API_KEYS.UPDATE_ENTITY_ATTR),
  setServicePaths(true),
  setEntityId,
  setAttrName,
  setAttr,
  (req, res, next) => {
    const dbArg = _.get(req, DB_ARG_PRIMARY_KEY);
    return storage.updateEntityAttr(dbArg)
      .then(handleSideEffects(storage, options, _.get(req, USER_ID_KEY)))
      .then((dbRes) => {
        res.status(204).send();
      })
      .catch(next);
  },
  handleErrors,
]);

const deleteEntityAttr = (storage, options) => compose([
  setQueryParams,
  setUserId(options),
  setUserAccess(options, STORAGE_API_KEYS.DELETE_ENTITY_ATTR),
  setServicePaths(true),
  setEntityId,
  setAttrName,
  (req, res, next) => {
    const dbArg = _.get(req, DB_ARG_PRIMARY_KEY);
    return storage.deleteEntityAttr(dbArg)
      .then(handleSideEffects(storage, options, _.get(req, USER_ID_KEY)))
      .then((dbRes) => {
        res.status(204).send();
      })
      .catch(next);
  },
  handleErrors,
]);

const findEntityAttrValue = (storage, options) => compose([
  setQueryParams,
  setUserId(options),
  setUserAccess(options, STORAGE_API_KEYS.FIND_ENTITY_ATTR_VALUE),
  setServicePaths(false),
  setEntityId,
  setAttrName,
  (req, res, next) => {
    const dbArg = _.get(req, DB_ARG_PRIMARY_KEY);
    return storage.findEntityAttrValue(dbArg)
      .then((attrValue) => res.json(attrValue).status(200))
      .catch(next);
  },
  handleErrors,
]);

const updateEntityAttrValue = (storage, options) => compose([
  setQueryParams,
  setUserId(options),
  setUserAccess(options, STORAGE_API_KEYS.UPDATE_ENTITY_ATTR_VALUE),
  setServicePaths(true),
  setEntityId,
  setAttrName,
  setAttrValue,
  (req, res, next) => {
    const dbArg = _.get(req, DB_ARG_PRIMARY_KEY);
    return storage.updateEntityAttrValue(dbArg)
      .then(handleSideEffects(storage, options, _.get(req, USER_ID_KEY)))
      .then((dbRes) => {
        res.status(204).send();
      })
      .catch(next);
  },
  handleErrors,
]);

module.exports = {
  // /entities
  getEntities,
  createEntity,
  // /entities/:entityId
  findEntity,
  deleteEntity,
  // /entities/:entityId/attrs
  getEntityAttrs,
  replaceEntityAttrs,
  upsertEntityAttrs,
  patchEntityAttrs,
  // /entities/:entityId/attrs/:attrName
  findEntityAttr,
  updateEntityAttr,
  deleteEntityAttr,
  // /entities/:entityId/attrs/:attrName/value
  findEntityAttrValue,
  updateEntityAttrValue,
};
