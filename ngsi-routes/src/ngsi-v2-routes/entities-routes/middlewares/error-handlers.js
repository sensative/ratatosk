/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const {errors: composeErrors} = require('compose-middleware');

const {
  ngsiSql: {errors: sqlErrors},
  ngsiQueryParamsUtils: {
    basicParamParser: {errors: basicParamErrors},
    attrsParamParser: {errors: attrsParamErrors},
    metadataParamParser: {errors: metadataParamErrors},
    optionsParamParser: {errors: optionsParamErrors},
    orderbyParamParser: {errors: orderbyParamErrors},
    queryParamsParser: {errors: queryParamsParserErrors},
    geometryParamsParser: {errors: geometryParamsParserErrors},
  },
  ngsiDatumValidator: {errors: datumValidationErrors},
  ngsiServicePathUtils: {errors: servicePathErrors},
  ngsiStorageErrorUtils: {errors: ngsiStorageErrors},
  ngsiResponseErrorUtils: {createError},
} = require('@ratatosk/ngsi-utils');

const {
  errors: accessManagerErrors,
} = require('@ratatosk/ngsi-mongo-access-manager');

const {
  ORION_ERRORS,
} = require('@ratatosk/ngsi-constants');

const convertNgsiSqlErrors = (err, req, res, next) => {
  if (err.isNgsiSqlError) {
    if (sqlErrors.CHOP_ERR_EMPTY_Q_PARAM.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_EMPTY_Q_PARAM);
      return next(ngsiErr);
    }
    if (sqlErrors.CHOP_ERR_EMPTY_MQ_PARAM.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_EMPTY_MQ_PARAM);
      return next(ngsiErr);
    }
    if (sqlErrors.INVALID_COMPARISON_VALUE_LT.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_OP_LESSTHAN_NGSI);
      return next(ngsiErr);
    }
    if (sqlErrors.INVALID_COMPARISON_VALUE_LTE.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_OP_LESSTHANOREQUAL_NGSI);
      return next(ngsiErr);
    }
    if (sqlErrors.INVALID_COMPARISON_VALUE_GT.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_OP_GREATERTHAN_NGSI);
      return next(ngsiErr);
    }
    if (sqlErrors.INVALID_COMPARISON_VALUE_GTE.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_OP_GREATERTHANOREQUAL_NGSI);
      return next(ngsiErr);
    }
    if (sqlErrors.MATCH_PATTERN_REQUIRES_REGEXP.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_OP_MATCHPATTERN_NGSI);
      return next(ngsiErr);
    }
    if (sqlErrors.EMPTY_PREDICATE_STATEMENT.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_EMPTY_Q_ITEM);
      return next(ngsiErr);
    }
    if (sqlErrors.TOO_FEW_MQ_TOKENS.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_NO_METADATA_AT_ALL);
      return next(ngsiErr);
    }
    if (sqlErrors.INVALID_ATTRIBUTE_TOKEN.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_INVALID_SQL_ATTRIBUTE_TOKEN);
      return next(ngsiErr);
    }
    if (sqlErrors.INVALID_METADATA_TOKEN.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_INVALID_SQL_METADATA_TOKEN);
      return next(ngsiErr);
    }
    // this list will NOT be exhaustive
    const devErr = new Error(`Unaccounted for NgsiSqlError: ${err.message}`);
    return next(devErr);
  }
  return next(err);
};

const convertNgsiQueryParamsParserErrors = (err, req, res, next) => {
  if (err.isNgsiQueryParamsParserError) {
    if (queryParamsParserErrors.INCOMPATIBLE_PARAMS_ID_PATTERN.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_INCOMPATIBLE_PARAMS_ID_PATTERN);
      return next(ngsiErr);
    }
    if (queryParamsParserErrors.INCOMPATIBLE_PARAMS_TYPE_PATTERN.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_INCOMPATIBLE_PARAMS_TYPE_PATTERN);
      return next(ngsiErr);
    }
  }
  return next(err);
};

const convertNgsiDatumValidationErrors = (err, req, res, next) => {
  if (err.isNgsiDatumValidationError) {
    if (datumValidationErrors.FIELD_CONTAINS_FORBIDDEN_CHAR.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_INVALID_CHAR_IN_PARAM);
      return next(ngsiErr);
    }
    // this list will probably not be exhaustive (?)
    const devErr = new Error(`Unaccounted for NgsiDatumValidationError: ${err.message}`);
    return next(devErr);
  }
  return next(err);
};

const convertNgsiAttrsParamErrors = (err, req, res, next) => {
  // EMPTY_ATTRS_PARAM: 'The attrs param must be a non-empty string if included',
  // INVALID_ATTRS_LIST_ITEM: 'Attrs list items must be either "*" or a valid attribute name',
  // INVALID_ATTRS_DUPLICATES: 'Duplicates are not allowed in attrs list',
  // INVALID_ATTRS_FORMATTING: 'Attrs param is formatted incorrectly',
  // INVALID_ATTRS_LIST: 'The attrsList must be an array',
  if (err.isNgsiAttrsParamError) {
    if (attrsParamErrors.EMPTY_ATTRS_PARAM.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_EMPTY_ATTRS_PARAM);
      return next(ngsiErr);
    }
    if (attrsParamErrors.INVALID_ATTRS_DUPLICATES.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_ATTRS_PARAM_DUPLICATE);
      return next(ngsiErr);
    }
    if (attrsParamErrors.INVALID_ATTRS_LIST_ITEM.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_ATTRS_PARAM_INVALID_ATTRNAME);
      return next(ngsiErr);
    }
  }
  return next(err);
};

const convertNgsiMetadataParamErrors = (err, req, res, next) => {
  // EMPTY_METADATA_PARAM: 'The metadata param must be a non-empty string if included',
  // INVALID_METADATA_LIST_ITEM: 'Metadata list items must be either "*", a builtin metadatum, or generally a valid metadata name',
  // INVALID_METADATA_DUPLICATES: 'Duplicates are not allowed in metadata list',
  // INVALID_METADATA_FORMATTING: 'Metadata param is formatted incorrectly',
  // INVALID_METADATA_LIST: 'The metadataList must be an array',
  if (err.isNgsiMetadataParamError) {
    if (metadataParamErrors.EMPTY_METADATA_PARAM.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_EMPTY_METADATA_PARAM);
      return next(ngsiErr);
    }
    if (metadataParamErrors.INVALID_METADATA_DUPLICATES.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_METADATA_PARAM_DUPLICATE);
      return next(ngsiErr);
    }
    if (metadataParamErrors.INVALID_METADATA_LIST_ITEM.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_METADATA_PARAM_INVALID_MDNAME);
      return next(ngsiErr);
    }
  }
  return next(err);
};

const convertNgsiOrderbyParamErrors = (err, req, res, next) => {
  // EMPTY_ORDERBY_PARAM: 'The orderby param must be a non-empty string if included',
  // INVALID_ORDERBY_PARAM_ITEM: 'The orderby param contains an invalid item',
  // INVALID_ORDERBY_DUPLICATES: 'Duplicates are not allowed in the orderby list',
  // INVALID_ORDERBY_PARAM_FORMATTING: 'The orderby param is formatted incorrectly',
  // INVALID_ORDERBY_LIST: 'The orderby list must be an array',
  if (err.isNgsiOrderbyParamError) {
    if (orderbyParamErrors.EMPTY_ORDERBY_PARAM.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_EMPTY_ORDERBY_PARAM);
      return next(ngsiErr);
    }
    if (orderbyParamErrors.INVALID_ORDERBY_DUPLICATES.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_ORDERBY_PARAM_DUPLICATE);
      return next(ngsiErr);
    }
    if (orderbyParamErrors.INVALID_ORDERBY_PARAM_ITEM.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_ORDERBY_PARAM_INVALID_ITEM);
      return next(ngsiErr);
    }
  }
  return next(err);
};

const convertNgsiBasicParamErrors = (err, req, res, next) => {
  // EMPTY_ENTITY_ID_PARAM: 'The entity id param must be a non-empty string if included',
  // INVALID_ENTITY_ID_PARAM_ITEM: 'The id param contains an invalid id item',
  // INVALID_ENTITY_ID_DUPLICATES: 'Duplicates are not allowed in the entity id list',
  // INVALID_ENTITY_ID_PARAM_FORMATTING: 'The entity id param is formatted incorrectly',
  // INVALID_ENTITY_ID_LIST: 'The entityId list must be an array',
  // EMPTY_ENTITY_TYPE_PARAM: 'The entity type param must be a non-empty string if included',
  // INVALID_ENTITY_TYPE_PARAM_ITEM: 'The ityped param contains an invalid type item',
  // INVALID_ENTITY_TYPE_DUPLICATES: 'Duplicates are not allowed in the entity type list',
  // INVALID_ENTITY_TYPE_PARAM_FORMATTING: 'The entity type param is formatted incorrectly',
  // INVALID_ENTITY_TYPE_LIST: 'The entityType list must be an array',
  // EMPTY_ENTITY_ID_PATTERN: 'The idPattern param must be a non-empty string if included',
  // EMPTY_ENTITY_TYPE_PATTERN: 'The typePattern param must be a non-empty string if included',
  // EMPTY_PAGE_OFFSET_PARAM: 'The page offset param must be a non-empty string (or a number) if included',
  // INVALID_PAGE_OFFSET_PARAM: 'Invalid page offset: must be a positive integer (or zero), or a corresponding string',
  // EMPTY_PAGE_LIMIT_PARAM: 'The page limit param must be a non-empty string (or a number) if included',
  // INVALID_PAGE_LIMIT_PARAM: 'Invalid page limit: must be a positive integer (strictly non-zero), or a corresponding string',
  if (err.isNgsiBasicParamError) {
    if (basicParamErrors.EMPTY_ENTITY_ID_PARAM.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_EMPTY_ID_PARAM);
      return next(ngsiErr);
    }
    if (basicParamErrors.EMPTY_ENTITY_ID_PATTERN.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_EMPTY_IDPATTERN_PARAM);
      return next(ngsiErr);
    }
    if (basicParamErrors.EMPTY_ENTITY_TYPE_PARAM.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_EMPTY_TYPE_PARAM);
      return next(ngsiErr);
    }
    if (basicParamErrors.EMPTY_ENTITY_TYPE_PATTERN.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_EMPTY_TYPEPATTERN_PARAM);
      return next(ngsiErr);
    }
    if (basicParamErrors.INVALID_ENTITY_ID_PARAM_ITEM.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_INVALID_CHAR_IN_PARAM);
      return next(ngsiErr);
    }
    if (basicParamErrors.INVALID_ENTITY_TYPE_PARAM_ITEM.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_INVALID_CHAR_IN_PARAM);
      return next(ngsiErr);
    }

    if (basicParamErrors.EMPTY_PAGE_OFFSET_PARAM.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_EMPTY_OFFSET_PARAM);
      return next(ngsiErr);
    }
    if (basicParamErrors.EMPTY_PAGE_LIMIT_PARAM.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_EMPTY_LIMIT_PARAM);
      return next(ngsiErr);
    }
    if (basicParamErrors.INVALID_PAGE_OFFSET_PARAM.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_INVALID_PAGE_OFFSET_PARAM);
      return next(ngsiErr);
    }
    if (basicParamErrors.INVALID_PAGE_LIMIT_PARAM.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_INVALID_PAGE_LIMIT_PARAM);
      return next(ngsiErr);
    }

    if (basicParamErrors.INVALID_PAGING_ID_PARAM.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_INVALID_PAGING_ID_PARAM);
      return next(ngsiErr);
    }

    // this list should be exhaustive (?)
    const devErr = new Error(`Unaccounted for NgsiBasicParamError: ${err.message}`);
    return next(devErr);
  }
  return next(err);
};

const convertNgsiOptionsParamErrors = (err, req, res, next) => {
  if (err.isNgsiOptionsParamError) {
    if (optionsParamErrors.INVALID_PARAM_SHAPE.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_EMPTY_OPTIONS_PARAM);
      return next(ngsiErr);
    }
    // otherwise use the default error
    const ngsiErr = createError(ORION_ERRORS.BAD_REQ_INVALID_OPTIONS_PARAM);
    return next(ngsiErr);
  }
  return next(err);
};

const convertNgsiGeometryParamsErrors = (err, req, res, next) => {
  // INVALID_GEOMETRY: 'Invalid geometry value. Must be one of [point, box, line, polygon]',
  // INVALID_COORDS_ARG: 'Invalid coords arg. Must be a non-zero string',
  // INVALID_NUM_COORD_PAIRS_POINT: 'Invalid number of coordinates for a "point" - must be exactly 1',
  // INVALID_NUM_COORD_PAIRS_BOX: 'Invalid number of coordinates for a "box" - must be exactly 2',
  // INVALID_NUM_COORD_PAIRS_LINE: 'Invalid number of coordinates for a "line" - must be at least 2',
  // INVALID_NUM_COORD_PAIRS_POLYGON: 'Invalid number of coordinates for a "polygon" - must be at least 4',
  // INVALID_SIMPLE_POLYGON_CLOSURE: 'A polygon must have the same coordinates at the start and end',
  // INVALID_GEOREL_ARG: 'georel must be a string',
  // INVALID_GEOREL_TYPE: 'the georel type must be one allowed by specification (e.g. "near" or "coveredBy")',
  // INVALID_GEOREL_MODIFIER: 'only the georel "near" type can have a modifier',
  // INVALID_GEOREL_NEAR_MODIFIER: 'the georel "near" type must have exactly one modifier (with modifier type and distance)',
  // INVALID_GEOREL_NEAR_GEOMETRY: 'invalid geometry for georel "near" (must be a point)',
  // INVALID_GEOREL_COVEREDBY_GEOMETRY: 'invalid geometry for georel "coveredBy" (must be a polygon)',
  if (err.isNgsiGeometryParamsParserError) {

    if (geometryParamsParserErrors.INVALID_GEOREL_NEAR_GEOMETRY.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_INVALID_GEOMETRY_WITH_GEOREL_NEAR);
      return next(ngsiErr);
    }
    if (geometryParamsParserErrors.INVALID_GEOREL_COVEREDBY_GEOMETRY.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_INVALID_GEOMETRY_WITH_GEOREL_COVEREDBY);
      return next(ngsiErr);
    }

    // this list should be exhaustive
    const devErr = new Error(`Unaccounted for NgsiGeometryParamsParserError: ${err.message}`);
    return next(devErr);
  }
  return next(err);
};

const convertServicePathErrors = (err, req, res, next) => {
  if (err.isNgsiServicePathError) {
    if (servicePathErrors.NON_ABSOLUTE_SERVICE_PATH.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_SERVICE_PATH_NOT_ABSOLUTE);
      return next(ngsiErr);
    }
    if (servicePathErrors.ILLEGAL_CHAR_IN_COMPONENT.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_SERVICE_PATH_ILLEGAL_CHAR);
      return next(ngsiErr);
    }
    if (servicePathErrors.TOO_SHORT_COMPONENT.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_SERVICE_PATH_EMPTY_COMPONENT);
      return next(ngsiErr);
    }
    if (servicePathErrors.TOO_LONG_COMPONENT.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_SERVICE_PATH_COMPONENT_NAME_TOO_LONG);
      return next(ngsiErr);
    }
    if (servicePathErrors.TOO_MANY_COMPONENTS.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_SERVICE_PATH_TOO_MANY_COMPONENTS);
      return next(ngsiErr);
    }
    if (servicePathErrors.TOO_MANY_SERVICE_PATHS.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_TOO_MANY_SERVICE_PATHS);
      return next(ngsiErr);
    }
    if (servicePathErrors.TOO_MANY_UPDATE_SERVICE_PATHS.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_SERVICE_PATH_UPDATE_MULTI_PATH);
      return next(ngsiErr);
    }
    if (servicePathErrors.EMPTY_SERVICE_PATH.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_SERVICE_PATH_EMPTY);
      return next(ngsiErr);
    }

    // this list should be exhaustive
    const devErr = new Error(`Unaccounted for NgsiServicePathError: ${err.message}`);
    return next(devErr);
  }
  return next(err);
};

const convertNgsiStorageErrors = (err, req, res, next) => {
  if (err.isNgsiStorageError) {
    if (ngsiStorageErrors.CANNOT_CREATE_ALREADY_EXISTS.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.UNPROCESSABLE_ENTITY_ALREADY_EXISTS);
      return next(ngsiErr);
    }
    if (ngsiStorageErrors.TOO_MANY_MATCHES.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.FIND_ERROR_TOO_MANY);
      return next(ngsiErr);
    }
    if (ngsiStorageErrors.MATCH_NOT_FOUND.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.FIND_ERROR_ENTITY_MISSING);
      return next(ngsiErr);
    }
    if (ngsiStorageErrors.ATTR_DOES_NOT_EXIST.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.FIND_ERROR_ATTR_MISSING);
      return next(ngsiErr);
    }
    if (ngsiStorageErrors.CANNOT_CREATE_ATTR_ALREADY_EXISTS.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.UNPROCESSABLE_ATTRIBUTE_ALREADY_EXISTS);
      return next(ngsiErr);
    }
    if (ngsiStorageErrors.CANNOT_CREATE_ATTR_COLLIDES_WITH_REGISTRATION.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.UNPROCESSABLE_ATTRIBUTE_COLLIDES_WITH_REGISTRATION);
      return next(ngsiErr);
    }
    if (ngsiStorageErrors.MULTIPLE_GEOATTRS_BUT_NO_DEFAULT.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.UNPROCESSABLE_MULTIPLE_GEO_WITHOUT_DEFAULT);
      return next(ngsiErr);
    }
    // missed it
    const devErr = new Error(`Unaccounted for NgsiStorageError: ${err.name}`);
    return next(devErr);
  }
  return next(err);
};

const convertAccessManagerErrors = (err, req, res, next) => {
  // INVALID_AGG_STAGE_USER_ID: 'invalid userId used to generate aggregation stage',
  if (err.isNgsiMongoAccessManagerError) {
    if (accessManagerErrors.INVALID_AGG_STAGE_USER_ID.matches(err)) {
      const ngsiErr = createError(ORION_ERRORS.BAD_REQ_INVALID_USER_ID);
      return next(ngsiErr);
    }
    // missed it
    const devErr = new Error(`Unaccounted for NgsiMongoAccessManagerError: ${err.name}`);
    return next(devErr);
  }
  return next(err);
};

// pack em all together
const handleErrors = composeErrors([
  convertNgsiQueryParamsParserErrors,
  convertNgsiSqlErrors,
  convertNgsiDatumValidationErrors,
  convertNgsiAttrsParamErrors,
  convertNgsiMetadataParamErrors,
  convertNgsiOrderbyParamErrors,
  convertNgsiBasicParamErrors,
  convertNgsiOptionsParamErrors,
  convertNgsiGeometryParamsErrors,
  convertServicePathErrors,
  convertNgsiStorageErrors,
  convertAccessManagerErrors,
]);

module.exports = {
  handleErrors,
};
