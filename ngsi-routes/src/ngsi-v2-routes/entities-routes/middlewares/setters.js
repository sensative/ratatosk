/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {
  ngsiSql: {
    queryParser: {
      parseQueryString,
    },
  },
  ngsiQueryParamsUtils: {
    basicParamParser: {
      parseEntityIdParam,
      parseEntityTypeParam,
      validateIdPatternParam,
      validateTypePatternParam,
      validatePageLimitParam,
      validatePageOffsetParam,
      validatePageItemIdParam,
      validatePageDirectionParam,
    },
    attrsParamParser: {
      parseAttrsParam,
    },
    metadataParamParser: {
      parseMetadataParam,
    },
    orderbyParamParser: {
      parseOrderbyParam,
    },
    optionsParamParser: {
      parseOptionsParam,
    },
    queryParamsParser: {
      assertQueryParamsAllCompatible,
    },
    geometryParamsParser: {
      parseGeorelParam,
      parseGeometryAndCoordsParam,
    },
  },
  ngsiDatumValidator: {
    validateEntityId,
    validateAttributeName,
    validateGeneralValue,
  },
  ngsiEntityValidator: {
    validateEntity,
    validateAttrs,
    validateAttribute,
  },
  ngsiEntityFormatter: {
    formatEntity,
    formatAttrs,
    formatAttribute,
  },
  ngsiAttributeValueFormatter: {
    unpackFormattedAttrValue,
  },
  ngsiResponseErrorUtils: {
    createError,
  },
  constants: {
    DB_ENTITY_ARG_KEYS,
    DB_ARG_PARAMS_KEYS,
    NGSI_SQL_QUERY_TYPES,
  },
} = require('@ratatosk/ngsi-utils');
const {DB_ARG_PRIMARY_KEY} = require('../../../constants');

const {
  DEFAULT_ENTITY_REPRESENTATION_TYPE,
  DEFAULT_ENTITY_TYPE,
  ENTITY_REPRESENTATION_OPTIONS,
  ENTITY_REQUEST_PARAM_KEYS,
  ORION_ERRORS,
} = require('@ratatosk/ngsi-constants');

const setQueryParams = (req, res, next) => {
  const query = _.get(req, 'query', {});
  // check for compatibility
  assertQueryParamsAllCompatible(query);
  // check entity id
  if (_.has(query, ENTITY_REQUEST_PARAM_KEYS.ENTITY_ID)) {
    const entityId = query[ENTITY_REQUEST_PARAM_KEYS.ENTITY_ID];
    try {
      const parsedEntityId = parseEntityIdParam(entityId);
      const paramPath = [DB_ARG_PRIMARY_KEY, DB_ENTITY_ARG_KEYS.PARAMS, DB_ARG_PARAMS_KEYS.ENTITY_ID];
      _.set(req, paramPath, parsedEntityId);
    } catch (err) {
      return next(err);
    }
  }
  // check entity idPattern
  if (_.has(query, ENTITY_REQUEST_PARAM_KEYS.ENTITY_ID_PATTERN)) {
    const entityIdPattern = query[ENTITY_REQUEST_PARAM_KEYS.ENTITY_ID_PATTERN];
    try {
      validateIdPatternParam(entityIdPattern);
      const paramPath = [DB_ARG_PRIMARY_KEY, DB_ENTITY_ARG_KEYS.PARAMS, DB_ARG_PARAMS_KEYS.ENTITY_ID_PATTERN];
      _.set(req, paramPath, entityIdPattern);
    } catch (err) {
      return next(err);
    }
  }
  // check entity type
  if (_.has(query, ENTITY_REQUEST_PARAM_KEYS.ENTITY_TYPE)) {
    const entityType = query[ENTITY_REQUEST_PARAM_KEYS.ENTITY_TYPE];
    try {
      const parsedEntityType = parseEntityTypeParam(entityType);
      const paramPath = [DB_ARG_PRIMARY_KEY, DB_ENTITY_ARG_KEYS.PARAMS, DB_ARG_PARAMS_KEYS.ENTITY_TYPE];
      _.set(req, paramPath, parsedEntityType);
    } catch (err) {
      return next(err);
    }
  }
  // check entity typePattern
  if (_.has(query, ENTITY_REQUEST_PARAM_KEYS.ENTITY_TYPE_PATTERN)) {
    const entityTypePattern = query[ENTITY_REQUEST_PARAM_KEYS.ENTITY_TYPE_PATTERN];
    try {
      validateTypePatternParam(entityTypePattern);
      const paramPath = [DB_ARG_PRIMARY_KEY, DB_ENTITY_ARG_KEYS.PARAMS, DB_ARG_PARAMS_KEYS.ENTITY_TYPE_PATTERN];
      _.set(req, paramPath, entityTypePattern);
    } catch (err) {
      return next(err);
    }
  }
  // check attrs query
  if (_.has(query, ENTITY_REQUEST_PARAM_KEYS.ATTRIBUTE_QUERY)) {
    const attrsQueryStr = query[ENTITY_REQUEST_PARAM_KEYS.ATTRIBUTE_QUERY];
    try {
      const attrsQuery = parseQueryString(attrsQueryStr, NGSI_SQL_QUERY_TYPES.ATTRIBUTE_QUERY);
      const paramPath = [DB_ARG_PRIMARY_KEY, DB_ENTITY_ARG_KEYS.PARAMS, NGSI_SQL_QUERY_TYPES.ATTRIBUTE_QUERY];
      _.set(req, paramPath, attrsQuery);
    } catch (err) {
      return next(err);
    }
  }

  // check attrs query
  if (_.has(query, ENTITY_REQUEST_PARAM_KEYS.ORDER_BY)) {
    const orderbyStr = query[ENTITY_REQUEST_PARAM_KEYS.ORDER_BY];
    try {
      const orderByList = parseOrderbyParam(orderbyStr);
      const paramPath = [DB_ARG_PRIMARY_KEY, DB_ENTITY_ARG_KEYS.PARAMS, DB_ARG_PARAMS_KEYS.ORDER_BY];
      _.set(req, paramPath, orderByList);
    } catch (err) {
      return next(err);
    }
  }

  // check md query
  if (_.has(query, ENTITY_REQUEST_PARAM_KEYS.METADATA_QUERY)) {
    const mdQueryStr = query[ENTITY_REQUEST_PARAM_KEYS.METADATA_QUERY];
    try {
      const mdQuery = parseQueryString(mdQueryStr, NGSI_SQL_QUERY_TYPES.METADATA_QUERY);
      const paramPath = [DB_ARG_PRIMARY_KEY, DB_ENTITY_ARG_KEYS.PARAMS, NGSI_SQL_QUERY_TYPES.METADATA_QUERY];
      _.set(req, paramPath, mdQuery);
    } catch (err) {
      return next(err);
    }
  }

  // check attrs list (projection)
  if (_.has(query, ENTITY_REQUEST_PARAM_KEYS.ATTRS_LIST)) {
    const attrs = query[ENTITY_REQUEST_PARAM_KEYS.ATTRS_LIST];
    try {
      const attrsArr = parseAttrsParam(attrs); // validates
      const paramPath = [DB_ARG_PRIMARY_KEY, DB_ENTITY_ARG_KEYS.PARAMS, DB_ARG_PARAMS_KEYS.ATTRS_LIST];
      _.set(req, paramPath, attrsArr);
    } catch (err) {
      return next(err);
    }
  }

  // check attrs list (metadata sub-projection)
  if (_.has(query, ENTITY_REQUEST_PARAM_KEYS.METADATA_LIST)) {
    const metadata = query[ENTITY_REQUEST_PARAM_KEYS.METADATA_LIST];
    try {
      const metadataArr = parseMetadataParam(metadata); // validates
      const paramPath = [DB_ARG_PRIMARY_KEY, DB_ENTITY_ARG_KEYS.PARAMS, DB_ARG_PARAMS_KEYS.METADATA_LIST];
      _.set(req, paramPath, metadataArr);
    } catch (err) {
      return next(err);
    }
  }

  // handle the options object (i.e. count & representationType etc.)
  if (_.has(query, ENTITY_REQUEST_PARAM_KEYS.OPTIONS_PARAM)) {
    const optionsStr = query[ENTITY_REQUEST_PARAM_KEYS.OPTIONS_PARAM];
    try {
      const optionsObj = parseOptionsParam(optionsStr);

      const appendParamPath = [DB_ARG_PRIMARY_KEY, DB_ENTITY_ARG_KEYS.PARAMS, DB_ARG_PARAMS_KEYS.STRICT_APPEND];
      _.set(req, appendParamPath, optionsObj.append);

      const countParamPath = [DB_ARG_PRIMARY_KEY, DB_ENTITY_ARG_KEYS.PARAMS, DB_ARG_PARAMS_KEYS.TOTAL_COUNT];
      _.set(req, countParamPath, optionsObj.count);

      const upsertParamPath = [DB_ARG_PRIMARY_KEY, DB_ENTITY_ARG_KEYS.UPSERT];
      _.set(req, upsertParamPath, optionsObj.upsert);

      if (optionsObj.representationType) {
        const repTypeParamPath = [DB_ARG_PRIMARY_KEY, DB_ENTITY_ARG_KEYS.PARAMS, DB_ARG_PARAMS_KEYS.REPRESENTATION_TYPE];
        _.set(req, repTypeParamPath, optionsObj.representationType);
      }
    } catch (err) {
      return next(err);
    }
  }

  if (_.has(query, ENTITY_REQUEST_PARAM_KEYS.PAGE_LIMIT)) {
    const limit = query[ENTITY_REQUEST_PARAM_KEYS.PAGE_LIMIT];
    try {
      validatePageLimitParam(limit);
      const pageLimitParamPath = [DB_ARG_PRIMARY_KEY, DB_ENTITY_ARG_KEYS.PARAMS, DB_ARG_PARAMS_KEYS.PAGE_LIMIT];
      _.set(req, pageLimitParamPath, _.toSafeInteger(limit));
    } catch (err) {
      return next(err);
    }
  }

  if (_.has(query, ENTITY_REQUEST_PARAM_KEYS.PAGE_OFFSET)) {
    const offset = query[ENTITY_REQUEST_PARAM_KEYS.PAGE_OFFSET];
    try {
      validatePageOffsetParam(offset);
      const pageOffsetParamPath = [DB_ARG_PRIMARY_KEY, DB_ENTITY_ARG_KEYS.PARAMS, DB_ARG_PARAMS_KEYS.PAGE_OFFSET];
      _.set(req, pageOffsetParamPath, _.toSafeInteger(offset));
    } catch (err) {
      return next(err);
    }
  }

  if (_.has(query, ENTITY_REQUEST_PARAM_KEYS.PAGE_ITEM_ID)) {
    const pageItemId = query[ENTITY_REQUEST_PARAM_KEYS.PAGE_ITEM_ID];
    try {
      validatePageItemIdParam(pageItemId);
      const pageItemIdParamPath = [DB_ARG_PRIMARY_KEY, DB_ENTITY_ARG_KEYS.PARAMS, DB_ARG_PARAMS_KEYS.PAGE_ITEM_ID];
      _.set(req, pageItemIdParamPath, pageItemId);
    } catch (err) {
      return next(err);
    }
  }

  if (_.has(query, ENTITY_REQUEST_PARAM_KEYS.PAGE_DIRECTION)) {
    const pageDirection = query[ENTITY_REQUEST_PARAM_KEYS.PAGE_DIRECTION];
    try {
      validatePageDirectionParam(pageDirection);
      const pageDirectionParamPath = [DB_ARG_PRIMARY_KEY, DB_ENTITY_ARG_KEYS.PARAMS, DB_ARG_PARAMS_KEYS.PAGE_DIRECTION];
      _.set(req, pageDirectionParamPath, pageDirection);
    } catch (err) {
      return next(err);
    }
  }

  if (_.has(query, ENTITY_REQUEST_PARAM_KEYS.GEOMETRY)) {
    const geometry = _.get(query, ENTITY_REQUEST_PARAM_KEYS.GEOMETRY);
    const coords = _.get(query, ENTITY_REQUEST_PARAM_KEYS.COORDS);
    const georel = _.get(query, ENTITY_REQUEST_PARAM_KEYS.GEOREL);
    const basePath = [DB_ARG_PRIMARY_KEY, DB_ENTITY_ARG_KEYS.PARAMS];
    const geometryPath = _.concat(basePath, DB_ARG_PARAMS_KEYS.GEOMETRY);
    const coordsPath = _.concat(basePath, DB_ARG_PARAMS_KEYS.COORDS);
    const georelPath = _.concat(basePath, DB_ARG_PARAMS_KEYS.GEOREL);
    try {
      parseGeometryAndCoordsParam(geometry, coords);
      parseGeorelParam(georel, geometry);
      _.set(req, geometryPath, geometry);
      _.set(req, coordsPath, coords);
      _.set(req, georelPath, georel);
    } catch (err) {
      return next(err);
    }
  }

  // and done
  next();
};

const setEntityId = (req, res, next) => {
  const params = _.get(req, 'params', {});
  if (params.entityId) {
    try {
      validateEntityId(params.entityId);
    } catch (err) {
      return next(err);
    }
    _.set(req, [DB_ARG_PRIMARY_KEY, DB_ENTITY_ARG_KEYS.ENTITY_ID], params.entityId);
  } else {
    const err = new Error('entity id is missing');
    return next(err);
  }
  next();
};

const setAttrName = (req, res, next) => {
  const params = _.get(req, 'params', {});
  if (params.attrName) {
    try {
      validateAttributeName(params.attrName);
    } catch (err) {
      return next(err);
    }
    _.set(req, [DB_ARG_PRIMARY_KEY, DB_ENTITY_ARG_KEYS.ATTR_NAME], params.attrName);
  } else {
    const err = new Error('attribute name is missing');
    return next(err);
  }
  next();
};

const setEntity = (req, res, next) => {
  const repTypeParamPath = [DB_ARG_PRIMARY_KEY, DB_ENTITY_ARG_KEYS.PARAMS, DB_ARG_PARAMS_KEYS.REPRESENTATION_TYPE];
  const representationType = _.get(req, repTypeParamPath);
  const body = _.get(req, 'body');

  let entity;
  if (representationType === ENTITY_REPRESENTATION_OPTIONS.KEY_VALUES) {
    entity = formatEntity(body, ENTITY_REPRESENTATION_OPTIONS.KEY_VALUES);
  } else {
    entity = formatEntity(body, DEFAULT_ENTITY_REPRESENTATION_TYPE);
  }

  if (!_.isObject(entity)) {
    throw new Error('Whoa nelly, this guy needs an orion error');
  }

  try {
    // assign the default entity type if it is missing
    validateEntity(_.assign({type: DEFAULT_ENTITY_TYPE}, entity));
  } catch (err) {
    return next(err);
  }
  _.set(req, [DB_ARG_PRIMARY_KEY, DB_ENTITY_ARG_KEYS.ENTITY], entity);
  next();
};

const setAttrs = (req, res, next) => {
  const attrs = formatAttrs(_.get(req, 'body'));
  try {
    validateAttrs(attrs);
  } catch (err) {
    return next(err);
  }
  _.set(req, [DB_ARG_PRIMARY_KEY, DB_ENTITY_ARG_KEYS.ATTRS], attrs);
  next();
};

const setAttr = (req, res, next) => {
  const attr = formatAttribute(_.get(req, 'body'));
  try {
    validateAttribute(attr);
  } catch (err) {
    return next(err);
  }
  _.set(req, [DB_ARG_PRIMARY_KEY, DB_ENTITY_ARG_KEYS.ATTR], attr);
  next();
};

const setAttrValue = (req, res, next) => {
  let attrValue = _.get(req, 'body');
  try {
    attrValue = unpackFormattedAttrValue(attrValue); // this can throw
    validateGeneralValue(attrValue); // and this
  } catch (err) {
    const ngsiErr = createError(ORION_ERRORS.BAD_REQ_INVALID_VALUE);
    return next(ngsiErr);
  }
  _.set(req, [DB_ARG_PRIMARY_KEY, DB_ENTITY_ARG_KEYS.ATTR_VALUE], attrValue);
  next();
};

const setResponseLocation = (req, res, next) => {
  const entity = _.get(req, [DB_ARG_PRIMARY_KEY, DB_ENTITY_ARG_KEYS.ENTITY]);
  if (!_.isObject(entity)) {
    return next(new Error('Dev: entity should already have been set'));
  }
  const location = `/v2/entities/${entity.id}?type=${entity.type}`;
  res.set('location', location);
  next();
};


module.exports = {
  setQueryParams,
  setEntity,
  setEntityId,
  setAttrName,
  setAttrs,
  setAttr,
  setAttrValue,
  setResponseLocation,
};
