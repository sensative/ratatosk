/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {
  ngsiResponseErrorUtils: {createError},
} = require('@ratatosk/ngsi-utils');

const {
  ORION_ERRORS,
} = require('@ratatosk/ngsi-constants');

const assertNonTextContent = (req, res, next) => {
  const contentType = _.get(req, ['headers', 'content-type']);
  const textContent = 'text/plain';
  if (_.isString(contentType) && contentType.match(textContent)) {
    const err = createError(ORION_ERRORS.UNSUPPORTED_MEDIA_TYPE_TEXT);
    return next(err);
  }
  next();
};

const assertContentHasLength = (req, res, next) => {
  const contentLength = _.toSafeInteger(_.get(req, ['headers', 'content-length']));
  if (contentLength <= 0) {
    const err = createError(ORION_ERRORS.CONTENT_LENGTH_REQUIRED);
    return next(err);
  }
  next();
};

const assertNonZeroDataEntityId = (req, res, next) => {
  const entityId = _.get(req, 'body.id');
  if (!entityId) {
    const err = createError(ORION_ERRORS.BAD_REQ_TOO_SHORT_ENTITY_ID);
    return next(err);
  }
  next();
};

const assertNonZeroDataEntityType = (req, res, next) => {
  const entityType = _.get(req, 'body.type');
  if (!_.isNil(entityType) && !entityType) { // default is used when nil
    const err = createError(ORION_ERRORS.BAD_REQ_TOO_SHORT_ENTITY_TYPE);
    return next(err);
  }
  next();
};

const assertDataEntityWasExcluded = (req, res, next) => {
  const entityId = _.get(req, 'body.id');
  if (!_.isNil(entityId)) {
    const err = createError(ORION_ERRORS.BAD_REQ_ENTITY_ID_IN_PAYLOAD);
    return next(err);
  }
  const entityType = _.get(req, 'body.type');
  if (!_.isNil(entityType)) {
    const err = createError(ORION_ERRORS.BAD_REQ_ENTITY_TYPE_IN_PAYLOAD);
    return next(err);
  }
  next();
};


const assertNonZeroDataEntityAttrTypes = (req, res, next) => {
  const attrs = _.omit(req.body, ['id', 'type']);
  const hasZeroLengthAttrType = _.some(attrs, (attr) => {
    const attrType = _.get(attr, 'type');
    return attrType === '';
  });
  if (hasZeroLengthAttrType) {
    const err = createError(ORION_ERRORS.BAD_REQ_TOO_SHORT_ATTR_TYPE);
    return next(err);
  }
  next();
};

const assertNonZeroDataEntityAttrNames = (req, res, next) => {
  const attrNames = _.keys(_.omit(req.body, ['id', 'type']));
  const hasZeroLengthAttrName = _.some(attrNames, (name) => !name);
  if (hasZeroLengthAttrName) {
    const err = createError(ORION_ERRORS.BAD_REQ_TOO_SHORT_ATTR_NAME);
    return next(err);
  }
  next();
};

const assertNonZeroDataEntityMetadataTypes = (req, res, next) => {
  const attrs = _.omit(req.body, ['id', 'type']);

  const mds = [];

  _.forEach(attrs, (attr) => {
    if (attr && attr.metadata) {
      _.forEach(attr.metadata, (md) => {
        mds.push(md);
      });
    }
  });

  const hasZeroLengthMetadataType = _.some(mds, (md) => {
    const mdType = _.get(md, 'type');
    return !mdType;
  });

  if (hasZeroLengthMetadataType) {
    const err = createError(ORION_ERRORS.BAD_REQ_TOO_SHORT_METADATA_TYPE);
    return next(err);
  }
  next();
};

module.exports = {
  assertNonTextContent,
  assertContentHasLength,
  assertNonZeroDataEntityId,
  assertNonZeroDataEntityType,
  assertDataEntityWasExcluded,
  assertNonZeroDataEntityAttrTypes,
  assertNonZeroDataEntityAttrNames,
  assertNonZeroDataEntityMetadataTypes,
};
