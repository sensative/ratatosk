/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const bodyParser = require('body-parser');
const {compose} = require('compose-middleware');

const {ORION_ERRORS} = require('@ratatosk/ngsi-constants');
const {ngsiResponseErrorUtils: {createError}} = require('@ratatosk/ngsi-utils');

const ngsiBodyParser = () => compose([
  bodyParser.json(),
  bodyParser.text(),
  (err, req, res, next) => { // eslint-disable-line handle-callback-err
    const ngsiErr = createError(ORION_ERRORS.BAD_REQ_INVALID_JSON);
    return next(ngsiErr);
  },
]);

const ngsiErrorHandler = () => (err, req, res, next) => {
  if (err.isNgsiError) {
    return res.status(err.status || 500).send({
      error: err.name,
      description: err.message,
    });
  }
  console.error({UNHANDLED_ERR: {name: err.name, message: err.message, keys: _.keys(err)}});
  console.error(err);
  return next(err);
};

module.exports = {
  ngsiBodyParser,
  ngsiErrorHandler,
};
