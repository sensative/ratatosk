/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const {Router: createRouter} = require('express');

const {ngsiBodyParser, ngsiErrorHandler} = require('./root-middlewares');
const {createRoutes: entityRoutes} = require('./entities-routes');
const {createRoutes: bulkOpRoutes} = require('./bulk-op-routes');
const {createRoutes: subscriptionRoutes} = require('./subscriptions-routes');


const createRoutes = (storage, options = {}) => {
  const router = createRouter();
  // parse body
  router.use(ngsiBodyParser());
  // add the routes
  router.use('/entities', entityRoutes(storage, options));
  router.use('/op', bulkOpRoutes(storage, options));
  router.use('/subscriptions', subscriptionRoutes(storage, options));
  // catch some errors
  router.use(ngsiErrorHandler());
  // and done
  return router;
};

module.exports = {createRoutes};
