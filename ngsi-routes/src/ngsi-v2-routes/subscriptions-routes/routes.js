/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const {compose} = require('compose-middleware');

const {
  setServicePaths,
} = require('../common-middlewares');

const {
  setCreationPayload,
  setSubscriptionId,
  handleErrors,
} = require('./middlewares');

const {
  DB_ARG_PRIMARY_KEY,
} = require('../../constants');


const createSubscription = (storage, options) => compose([
  setCreationPayload,
  setServicePaths(true),
  (req, res, next) => {
    const dbSubscriptionArg = _.get(req, DB_ARG_PRIMARY_KEY);
    return storage.createSubscription(dbSubscriptionArg)
      .then(() => res.status(201).send())
      .catch(next);
  },
  handleErrors,
]);

const getSubscriptions = (storage, options) => compose([
  setServicePaths(false),
  (req, res, next) => {
    const dbSubscriptionArg = _.get(req, DB_ARG_PRIMARY_KEY);
    return storage.getSubscriptions(dbSubscriptionArg)
      .then((subscriptions) => {
        return res.json(subscriptions).status(200);
      })
      .catch(next);
  },
  handleErrors,
]);


const findSubscription = (storage, options) => compose([
  setSubscriptionId,
  setServicePaths(false),
  (req, res, next) => {
    const dbSubscriptionArg = _.get(req, DB_ARG_PRIMARY_KEY);
    return storage.findSubscription(dbSubscriptionArg)
      .then((subscription) => {
        return res.json(subscription).status(200);
      })
      .catch(next);
  },
  handleErrors,
]);

const deleteSubscription = (storage, options) => compose([
  setSubscriptionId,
  setServicePaths(true),
  (req, res, next) => {
    const dbSubscriptionArg = _.get(req, DB_ARG_PRIMARY_KEY);
    return storage.deleteSubscription(dbSubscriptionArg)
      .then(() => res.status(204).send())
      .catch(next);
  },
  handleErrors,
]);

const updateSubscription = (storage, options) => compose([
  setSubscriptionId,
  setServicePaths(true),
  (req, res, next) => {
    const err = new Error('updateSubscription not yet implemented (ngsi-routes)');
    return next(err);
  },
  handleErrors,
]);

module.exports = {
  getSubscriptions,
  createSubscription,
  findSubscription,
  deleteSubscription,
  updateSubscription,
};
