/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const {Router: createRouter} = require('express');

const {
  // subscriptions
  getSubscriptions,
  createSubscription,
  // subscription/:subscriptionId
  findSubscription,
  deleteSubscription,
  updateSubscription,
} = require('./routes');

const createRoutes = (storage, options) => {

  const router = createRouter();

  // add the endpoints
  router.get('/', getSubscriptions(storage, options));
  router.post('/', createSubscription(storage, options));

  router.get('/:subscriptionId', findSubscription(storage, options));
  router.delete('/:subscriptionId', deleteSubscription(storage, options));
  router.patch('/:subscriptionId', updateSubscription(storage, options));

  // and done
  return router;
};


module.exports = {createRoutes};
