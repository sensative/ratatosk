/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {errors: composeErrors} = require('compose-middleware');

const {
  ngsiQueryParamsUtils: {
    subscriptionPayloadParser: {
      formatSubscriptionPayload,
    },
  },
  constants: {
    DB_SUBSCRIPTION_ARG_KEYS,
  },
} = require('@ratatosk/ngsi-utils');

// const {
//   ORION_ERRORS,
// } = require('@ratatosk/ngsi-constants');

const {DB_ARG_PRIMARY_KEY} = require('../../constants');


const setCreationPayload = (req, res, next) => {
  const inputPayload = _.get(req, 'body', {});
  const dbPayload = formatSubscriptionPayload(inputPayload);
  const paramPath = [DB_ARG_PRIMARY_KEY, DB_SUBSCRIPTION_ARG_KEYS.PAYLOAD];
  _.set(req, paramPath, dbPayload);
  return next();
};

const setSubscriptionId = (req, res, next) => {
  const params = _.get(req, 'params', {});
  _.set(req, [DB_ARG_PRIMARY_KEY, DB_SUBSCRIPTION_ARG_KEYS.SUBSCRIPTION_ID], params.subscriptionId);
  next();
};

const handleErrors = composeErrors([
]);

module.exports = {
  setCreationPayload,
  setSubscriptionId,
  handleErrors,
};
