/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const {sendNotification} = require('./notification-publisher');

const triggerSubscriptions = async (storage, dbRes) => {
  const {originalEntity, modifiedEntity} = dbRes;
  const payloads = await storage.generateNotifications({originalEntity, modifiedEntity});
  // these are fire and forget
  await Promise.all(_.map(payloads, async ({subscription, notification}) => {
    const url = _.get(subscription, 'notification.http.url', '');
    let succeeded = true;
    // send notification
    try {
      await sendNotification(url, notification);
    } catch (err) {
      succeeded = false;
    }
    // send acknowledgement
    try {
      await storage.acknowledgeNotification({subscription, succeeded});
    } catch (err) {
      console.error('failure to acknowledgeNotification: ', err.message);
    }
  }));

};

module.exports = {
  triggerSubscriptions,
};
