/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const NGSI_API_OPTIONS_KEYS = {
  GLOBAL_UPDATE_CALLBACK: 'globalUpdateCallback',
  GLOBAL_REGISTRATION_CALLBACK: 'globalRegistrationCallback',
  GLOBAL_REGISTRATION_ATTRS: 'globalRegistrationAttrs',
  USER_ACCESS: 'userAccess',
  USER_TOKEN_PARSER: 'userTokenParser',
  CUSTOM_AGGREGATION_GENERATOR: 'customAggregationGenerator',
};

const DB_ARG_PRIMARY_KEY = 'dbArg';
const CALLBACK_URL_KEY = 'url';
const USER_ID_KEY = 'updateUserId';

module.exports = {
  NGSI_API_OPTIONS_KEYS,
  DB_ARG_PRIMARY_KEY,
  CALLBACK_URL_KEY,
  USER_ID_KEY,
};
