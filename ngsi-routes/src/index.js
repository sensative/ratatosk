/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const {Router: createRouter} = require('express');

const {createRoutes: createNgsiRoutes} = require('./ngsi-v2-routes');
const {
  constants: {STORAGE_API_KEYS},
} = require('@ratatosk/ngsi-utils');
const {
  NGSI_API_OPTIONS_KEYS,
  CALLBACK_URL_KEY,
} = require('./constants');

const validateConfig = (options = {}) => {
  // only valid options keys are allowed
  if (!_.isObject(options)) {
    throw new Error('Invalid options object');
  }
  if (_.size(_.omit(options, _.values(NGSI_API_OPTIONS_KEYS)))) {
    throw new Error('Invalid keys included in ngsi-api-options object');
  }

  if (!_.isNil(options[NGSI_API_OPTIONS_KEYS.GLOBAL_UPDATE_CALLBACK])) {
    const callback = _.get(options, NGSI_API_OPTIONS_KEYS.GLOBAL_UPDATE_CALLBACK);
    if (!_.isObject(callback) && !_.isFunction(callback)) {
      throw new Error('global update callback needs to be an object or a function');
    }
    if (!_.isFunction(callback) && _.isObject(callback) && !_.isString(callback[CALLBACK_URL_KEY])) {
      throw new Error('url is missing from global update callback object');
    }
  }

  if (!_.isNil(options[NGSI_API_OPTIONS_KEYS.GLOBAL_REGISTRATION_CALLBACK])) {
    const callback = _.get(options, NGSI_API_OPTIONS_KEYS.GLOBAL_REGISTRATION_CALLBACK);
    if (!_.isObject(callback) && !_.isFunction(callback)) {
      throw new Error('global registration callback needs to be an object or a function');
    }
    if (!_.isFunction(callback) && _.isObject(callback) && !_.isString(callback[CALLBACK_URL_KEY])) {
      throw new Error('url is missing from global registration callback object');
    }
  }

  if (!_.isNil(options[NGSI_API_OPTIONS_KEYS.USER_ACCESS])) {
    const userAccess = _.get(options, NGSI_API_OPTIONS_KEYS.USER_ACCESS);
    if (!_.isBoolean(userAccess)) {
      throw new Error('userAccess must be a boolean if included');
    }
  }

  if (!_.isNil(options[NGSI_API_OPTIONS_KEYS.USER_TOKEN_PARSER])) {
    const userTokenParser = _.get(options, NGSI_API_OPTIONS_KEYS.USER_TOKEN_PARSER);
    if (!_.isFunction(userTokenParser)) {
      throw new Error('userTokenParser must be a function when included');
    }
    if (!options[NGSI_API_OPTIONS_KEYS.USER_ACCESS]) {
      throw new Error('userTokenParser cannot be included if userAccess is not required');
    }
  }

  if (!_.isNil(options[NGSI_API_OPTIONS_KEYS.CUSTOM_AGGREGATION_GENERATOR])) {
    const customAggregationGenerator = _.get(options, NGSI_API_OPTIONS_KEYS.CUSTOM_AGGREGATION_GENERATOR);
    if (!_.isFunction(customAggregationGenerator)) {
      throw new Error('customAggregationGenerator must be a function when included');
    }
  }

};

const validateStorage = (storage) => {
  // check for valid storage
  if (!_.isObject(storage)) {
    throw new Error('Invalid ngsi storage object');
  }
  if (_.size(_.omit(storage, _.values(STORAGE_API_KEYS)))) {
    throw new Error('Invalid keys included in ngsi storage object');
  }
  _.each(storage, (val) => {
    if (!_.isFunction(val)) {
      throw new Error('Invalid ngsi storage: props must be functions');
    }
  });

};

const createRoutes = (storage, options, serverOptions) => {
  validateStorage(storage);
  validateConfig(options);
  const router = createRouter();
  const pathPrefix = _.get(serverOptions, 'pathPrefix', '');
  router.use(`${pathPrefix}/v2`, createNgsiRoutes(storage, options));
  return router;
};

module.exports = {
  createRoutes,
};
