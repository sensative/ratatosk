/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {
  publishUpdates,
} = require('./http-publisher');

const {
  NGSI_API_OPTIONS_KEYS,
  CALLBACK_URL_KEY,
} = require('../constants');

const handleGlobalEntityUpdate = async (options, dbRes, userId) => {
  const {originalEntity, modifiedEntity} = dbRes;
  const updatePayload = {
    originalEntity,
    modifiedEntity,
    ...(userId ? {userId} : {}),
  };
  if (_.isEqual(originalEntity, modifiedEntity)) {
    return; // no changes were made. nothing to update
  }
  const updateHook = _.get(options, NGSI_API_OPTIONS_KEYS.GLOBAL_UPDATE_CALLBACK);
  if (_.isFunction(updateHook)) {
    try {
      await updateHook(updatePayload);
    } catch (err) {
      console.error('failure to publish global updates with custom callback function: ', err.message);
    }
    return; // done
  }
  const url = _.get(updateHook, CALLBACK_URL_KEY);
  if (url) {
    try {
      await publishUpdates(url, updatePayload);
    } catch (err) {
      console.error('failure to http-publish global updates: ', err.message);
    }
    return; // done
  }
  if (updateHook) {
    console.error('DevErr: invalid handleGlobalEntityUpdate - should have been caught with validation already');
  }
};

module.exports = {
  handleGlobalEntityUpdate,
};
