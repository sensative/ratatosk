/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {
  publishRegistration,
} = require('./http-publisher');

const {
  NGSI_API_OPTIONS_KEYS,
  CALLBACK_URL_KEY,
} = require('../constants');


const triggerSingleRegistration = async (callback, registration, data, entity) => {

  if (_.isFunction(callback)) {
    try {
      await callback(registration, data, entity);
    } catch (err) {
      console.error('failure to publish global updates with custom callback function: ', err.message);
    }
    return; // done
  }
  const url = _.get(callback, CALLBACK_URL_KEY);
  if (url) {
    try {
      await publishRegistration(url, registration, data, entity);
    } catch (err) {
      console.error('failure to http-publish global registrations: ', err.message);
    }
    return; // done
  }
  if (callback) {
    console.error('DevErr: invalid triggerSingleRegistration callback - should have been caught with validation already');
  }

};


const handleGlobalRegistrations = async (options, dbRes) => {
  const {registrations, modifiedEntity} = dbRes;
  const registrationCallback = _.get(options, NGSI_API_OPTIONS_KEYS.GLOBAL_REGISTRATION_CALLBACK);
  // if nothing registered, or no callback, then exit
  if (!_.size(registrations) || !registrationCallback) {
    return;
  }
  await Promise.all(_.map(registrations, async (data, registration) => {
    await triggerSingleRegistration(registrationCallback, registration, data, modifiedEntity);
  }));

};

module.exports = {
  handleGlobalRegistrations,
};
