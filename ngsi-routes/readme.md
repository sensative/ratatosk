## Exposes middleware that implements ngsi-v2

---------
#### the roadmap to the entities api
```js
const orionRest = [
  // find-entities
  {url: '/v2/entities', method: 'get'},
  // create-entity
  {url: '/v2/entities', method: 'post'},

  // find-entity
  {url: '/v2/entities/:id', method: 'get'},
  // delete-entity
  {url: '/v2/entities/:id', method: 'delete'},

  // find-entity-attrs
  {url: '/v2/entities/:id/attrs', method: 'get'},
  // update-entity-attrs
  {url: '/v2/entities/:id/attrs', method: 'put'},
  // replace-entity-attrs
  {url: '/v2/entities/:id/attrs', method: 'post'},
  // patch-entity-attrs
  {url: '/v2/entities/:id/attrs', method: 'patch'},

  // find-entity-attr
  {url: '/v2/entities/:id/attrs/:param_1', method: 'get'},
  // update-entity-attr
  {url: '/v2/entities/:id/attrs/:param_1', method: 'put'},
  // delete-entity-attr
  {url: '/v2/entities/:id/attrs/:param_1', method: 'delete'},

  // find-entity-attr-value
  {url: '/v2/entities/:id/attrs/:param_1/value', method: 'get'},
  // update-entity-attr-value
  {url: '/v2/entities/:id/attrs/:param_1/value', method: 'put'},

];
```
