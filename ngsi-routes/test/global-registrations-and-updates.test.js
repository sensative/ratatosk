/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

// const _ = require('lodash');
const assert = require('assert');
const sinon = require('sinon');

const {
  assertRejects,
  ngsiResponseErrorUtils: {assertErrorsMatch},
} = require('@ratatosk/ngsi-utils');

const {
  ORION_ERRORS,
} = require('@ratatosk/ngsi-constants');

// test setup info (local stuff)
const {getNgsiConfig, getMongoConfig} = require('./test-config');
const {restart: startServer, stop: stopServer} = require('./test-server');
const {createRoutes} = require('../src');
const {
  connectionManager: {
    restart: startMongo,
    stop: stopMongo,
    mongoRaw,
  },
} = require('@ratatosk/ngsi-mongo-storage');

const {createApi: createClientApi} = require('@ratatosk/ngsi-client-api');
const clientApi = createClientApi(getNgsiConfig());


const handleUpdates = sinon.spy((...args) => {
  return args;
});

const handleRegistrations = sinon.spy((...args) => {
  return args;
});


describe('global-registrations-and-updates', () => {

  before(async () => {
    const storageConfig = {
      ...getMongoConfig(),
      globalRegistrationAttrs: ['cmd'],
    };
    const mongoStorage = await startMongo(storageConfig);
    await mongoRaw.wipeEntities();
    const routerOptions = {
      globalUpdateCallback: handleUpdates,
      globalRegistrationCallback: handleRegistrations,
    };
    const router = createRoutes(mongoStorage, routerOptions);
    await startServer(router, getNgsiConfig());
  });

  after(async () => {
    await mongoRaw.wipeEntities();
    await stopMongo();
    await stopServer();
  });

  it('creating an (emtpy) entity should trigger (only) globalUpdateCallback', async () => {
    assert.strictEqual(handleUpdates.callCount, 0);
    assert.strictEqual(handleRegistrations.callCount, 0);
    const entity = {id: 'asdfff', type: 'flippidy'};
    await clientApi.createEntity({entity});
    // check the updates
    assert.strictEqual(handleUpdates.callCount, 1);
    const {originalEntity, modifiedEntity} = handleUpdates.args[0][0];
    assert.strictEqual(originalEntity, null);
    assert.strictEqual(modifiedEntity.id, 'asdfff');
    assert.deepStrictEqual(modifiedEntity.attrs, {});
    handleUpdates.resetHistory();
    // and check the registrations
    assert.strictEqual(handleRegistrations.callCount, 0);
  });

  it('creating an entity with "cmd" attr should FAIL', async () => {
    assert.strictEqual(handleUpdates.callCount, 0);
    assert.strictEqual(handleRegistrations.callCount, 0);
    const entity = {id: 'spoons', type: 'flippidy', cmd: 234, larps: 'moob'};
    const err = await assertRejects(clientApi.createEntity({entity}));
    assertErrorsMatch(err, ORION_ERRORS.UNPROCESSABLE_ATTRIBUTE_COLLIDES_WITH_REGISTRATION);
    assert.strictEqual(handleUpdates.callCount, 0);
    assert.strictEqual(handleRegistrations.callCount, 0);
  });

  it('replacing some attrs with ONLY "cmd" should ONLY trigger globalRegistrationCallback', async () => {
    assert.strictEqual(handleUpdates.callCount, 0);
    assert.strictEqual(handleRegistrations.callCount, 0);
    const reqArg = {entityId: 'asdfff', attrs: {cmd: 23}};
    await clientApi.replaceEntityAttrs(reqArg);
    assert.strictEqual(handleUpdates.callCount, 0);
    assert.strictEqual(handleRegistrations.callCount, 1);
    handleRegistrations.resetHistory();
  });

  it('replacing some attrs with "cmd" AND other attr should trigger both', async () => {
    assert.strictEqual(handleUpdates.callCount, 0);
    assert.strictEqual(handleRegistrations.callCount, 0);
    const reqArg = {entityId: 'asdfff', attrs: {cmd: 23, burb: 'nibs'}};
    await clientApi.replaceEntityAttrs(reqArg);
    assert.strictEqual(handleUpdates.callCount, 1);
    assert.strictEqual(handleRegistrations.callCount, 1);
    handleRegistrations.resetHistory();
  });

});
