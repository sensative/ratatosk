/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');

const {ngsiEntityFormatter: {formatEntity, formatAttrs, formatAttribute}} = require('@ratatosk/ngsi-utils');

// test setup info (local stuff)
const {getNgsiConfig, getMongoConfig} = require('./test-config');
const {restart: startServer, stop: stopServer} = require('./test-server');
const {createRoutes} = require('../src');
const {
  connectionManager: {
    restart: startMongo,
    stop: stopMongo,
    mongoRaw,
  },
} = require('@ratatosk/ngsi-mongo-storage');

const {createApi: createClientApi} = require('@ratatosk/ngsi-client-api');
const clientApi = createClientApi(getNgsiConfig());

describe('crud flowthrough', () => {

  before(async () => {
    const mongoStorage = await startMongo(getMongoConfig());
    await mongoRaw.wipeEntities();
    const router = createRoutes(mongoStorage);
    await startServer(router, getNgsiConfig());
  });

  after(async () => {
    // await mongoRaw.wipeEntities();
    await stopMongo();
    await stopServer();
  });

  it('should be possible to create entities', async () => {
    const entity = formatEntity({
      id: 'spiggy',
      type: 'Erp',
      a: 2,
      b: 3,
      eff: {value: 2, metadata: {arp: 2.3}},
    });
    const res = await clientApi.createEntity({entity});
    assert.strictEqual(res.status, 201);
    const entityX = formatEntity({
      id: 'miggy',
      type: 'Derp',
      a: 0,
      dee: 'spo',
      gee: {value: 58, metadata: {googoo: 'twelve'}},
    });
    const resX = await clientApi.createEntity({entity: entityX});
    assert.strictEqual(resX.status, 201);
  });

  it('should be possible to get entities', async () => {
    const {data: ents0} = await clientApi.getEntities({params: {id: 'spiggy'}});
    assert.strictEqual(ents0.length, 1);
    const {data: ents1} = await clientApi.getEntities({params: {idPattern: 'pig'}});
    assert.strictEqual(ents1.length, 1);
    const {data: ents2} = await clientApi.getEntities({params: {type: 'Erp'}});
    assert.strictEqual(ents2.length, 1);
    const {data: ents3} = await clientApi.getEntities({params: {typePattern: 'Er'}});
    assert.strictEqual(ents3.length, 1);
    const {data: ents4} = await clientApi.getEntities({params: {q: 'a>1'}});
    assert.strictEqual(ents4.length, 1);
    const {data: ents5} = await clientApi.getEntities({params: {q: '!dee'}});
    assert.strictEqual(ents5.length, 1);
    const {data: ents6} = await clientApi.getEntities({params: {mq: 'eff.arp==2.3'}});
    assert.strictEqual(ents6.length, 1);
    const {data: ents7} = await clientApi.getEntities({params: {mq: '!gee.googoo'}});
    assert.strictEqual(ents7.length, 1);
  });

  it('attrs list projection works', async () => {
    const {data: ents0} = await clientApi.getEntities({params: {id: 'spiggy', attrs: 'a'}});
    assert.strictEqual(ents0.length, 1);
    const expected = {
      id: 'spiggy',
      type: 'Erp',
      a: {value: 2, type: 'Number', metadata: {}},
    };
    assert.deepStrictEqual(ents0[0], expected);
  });

  it('should find a single entity', async () => {
    const {data: ent0} = await clientApi.findEntity({entityId: 'spiggy'});
    assert.strictEqual(ent0.id, 'spiggy');
  });

  it('should delete a single entity', async () => {
    // first create an extra & verify
    const entity = {id: 'theSpare', type: 'Bluefish'};
    await clientApi.createEntity({entity});
    const {data: entsS} = await clientApi.getEntities();
    assert.strictEqual(entsS.length, 3);
    // then delete & verify
    await clientApi.deleteEntity({entityId: 'theSpare'});
    const {data: entsF} = await clientApi.getEntities();
    assert.strictEqual(entsF.length, 2);
  });

  it('should getEntityAttrs', async () => {
    const {data: attrs} = await clientApi.getEntityAttrs({entityId: 'spiggy'});
    const expected = ['a', 'b', 'eff'];
    assert.deepStrictEqual(_.keys(attrs).sort(), expected.sort());
  });

  it('should replaceEntityAttrs', async () => {
    const attrs = formatAttrs({asdf: 3.148, ksks: 'axxt'});
    await clientApi.replaceEntityAttrs({entityId: 'miggy', attrs});
    const {data: attrsF} = await clientApi.getEntityAttrs({entityId: 'miggy'});
    assert.strictEqual(attrsF.ksks.value, 'axxt');
    assert.strictEqual(attrsF.asdf.value, 3.148);
    assert.strictEqual(_.size(attrsF), 2);
  });

  it('should upsertEntityAttrs', async () => {
    const attrs = formatAttrs({asdf: 8.413, skati: 'blubb'});
    await clientApi.upsertEntityAttrs({entityId: 'miggy', attrs});
    const {data: attrsF} = await clientApi.getEntityAttrs({entityId: 'miggy'});
    assert.strictEqual(attrsF.ksks.value, 'axxt');
    assert.strictEqual(attrsF.asdf.value, 8.413);
    assert.strictEqual(attrsF.skati.value, 'blubb');
    assert.strictEqual(_.size(attrsF), 3);
  });

  it('should patchEntityAttrs', async () => {
    const attrs = formatAttrs({asdf: 13, skati: 'ubb'});
    await clientApi.patchEntityAttrs({entityId: 'miggy', attrs});
    const {data: attrsF} = await clientApi.getEntityAttrs({entityId: 'miggy'});
    assert.strictEqual(attrsF.ksks.value, 'axxt');
    assert.strictEqual(attrsF.asdf.value, 13);
    assert.strictEqual(attrsF.skati.value, 'ubb');
    assert.strictEqual(_.size(attrsF), 3);
  });

  it('should findEntityAttr', async () => {
    const {data: attr} = await clientApi.findEntityAttr({entityId: 'spiggy', attrName: 'a'});
    assert.deepStrictEqual(attr, {value: 2, type: 'Number', metadata: {}});
  });

  it('should updateEntityAttr', async () => {
    const attr = formatAttribute(2.8989);
    await clientApi.updateEntityAttr({entityId: 'spiggy', attrName: 'a', attr});
    const {data: attrF} = await clientApi.findEntityAttr({entityId: 'spiggy', attrName: 'a'});
    assert.deepStrictEqual(attrF, {value: 2.8989, type: 'Number', metadata: {}});
  });

  it('should deleteEntityAttr', async () => {
    // validate that 'a' does in fact exist
    const {data: attrsS} = await clientApi.getEntityAttrs({entityId: 'spiggy'});
    assert.strictEqual(_.size(attrsS), 3);
    assert(_.has(attrsS, 'a'));
    assert.strictEqual(attrsS.a.value, 2.8989);
    // delete 'a' & validate
    await clientApi.deleteEntityAttr({entityId: 'spiggy', attrName: 'a'});
    const {data: attrsF} = await clientApi.getEntityAttrs({entityId: 'spiggy'});
    assert.strictEqual(_.size(attrsF), 2);
    assert(!_.has(attrsF, 'a'));
  });

  it('should findEntityAttrValue', async () => {
    const {data: attrValue} = await clientApi.findEntityAttrValue({entityId: 'spiggy', attrName: 'b'});
    assert.strictEqual(attrValue, 3);
  });

  it('should updateEntityAttrValue', async () => {
    // validate prev value
    const {data: attrValueS} = await clientApi.findEntityAttrValue({entityId: 'spiggy', attrName: 'b'});
    assert.strictEqual(attrValueS, 3);
    // update and recheck (string)
    await clientApi.updateEntityAttrValue({entityId: 'spiggy', attrName: 'b', attrValue: 'yntio'});
    const {data: attrValue0} = await clientApi.findEntityAttrValue({entityId: 'spiggy', attrName: 'b'});
    assert.strictEqual(attrValue0, 'yntio');
    // update and recheck (null)
    await clientApi.updateEntityAttrValue({entityId: 'spiggy', attrName: 'b', attrValue: null});
    const {data: attrValue1} = await clientApi.findEntityAttrValue({entityId: 'spiggy', attrName: 'b'});
    assert.strictEqual(attrValue1, null);
    // update and recheck (false)
    await clientApi.updateEntityAttrValue({entityId: 'spiggy', attrName: 'b', attrValue: false});
    const {data: attrValue2} = await clientApi.findEntityAttrValue({entityId: 'spiggy', attrName: 'b'});
    assert.strictEqual(attrValue2, false);
    // update and recheck (true)
    await clientApi.updateEntityAttrValue({entityId: 'spiggy', attrName: 'b', attrValue: true});
    const {data: attrValue3} = await clientApi.findEntityAttrValue({entityId: 'spiggy', attrName: 'b'});
    assert.strictEqual(attrValue3, true);
    // update and recheck (0)
    await clientApi.updateEntityAttrValue({entityId: 'spiggy', attrName: 'b', attrValue: 0});
    const {data: attrValue4} = await clientApi.findEntityAttrValue({entityId: 'spiggy', attrName: 'b'});
    assert.strictEqual(attrValue4, 0);
    // update and recheck (-1002.82828)
    await clientApi.updateEntityAttrValue({entityId: 'spiggy', attrName: 'b', attrValue: -1002.82828});
    const {data: attrValue5} = await clientApi.findEntityAttrValue({entityId: 'spiggy', attrName: 'b'});
    assert.strictEqual(attrValue5, -1002.82828);
    // update and recheck {isObject: true}
    await clientApi.updateEntityAttrValue({entityId: 'spiggy', attrName: 'b', attrValue: {isObject: true}});
    const {data: attrValue6} = await clientApi.findEntityAttrValue({entityId: 'spiggy', attrName: 'b'});
    assert.deepStrictEqual(attrValue6, {isObject: true});
    // update and recheck ['isArray', true]
    await clientApi.updateEntityAttrValue({entityId: 'spiggy', attrName: 'b', attrValue: ['isArray', true]});
    const {data: attrValue7} = await clientApi.findEntityAttrValue({entityId: 'spiggy', attrName: 'b'});
    assert.deepStrictEqual(attrValue7, ['isArray', true]);
  });

});
