/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');

// test setup info (local stuff)
const {getNgsiConfig, getMongoConfig} = require('./test-config');
const {restart: startServer, stop: stopServer} = require('./test-server');
const {createRoutes} = require('../src');
const {
  connectionManager: {
    restart: startMongo,
    stop: stopMongo,
    mongoRaw,
  },
} = require('@ratatosk/ngsi-mongo-storage');

const {createApi: createClientApi} = require('@ratatosk/ngsi-client-api');
const clientApi = createClientApi(getNgsiConfig());

const generateId = (index) => {
  const numPad = 4 - String(index).length;
  const padding = _.times(numPad, () => '0').join('');
  const entityId = `borky_${padding}${index}`;
  return entityId;
};

describe('keyset-paging ', () => {

  before(async () => {
    const mongoStorage = await startMongo(getMongoConfig());
    await mongoRaw.wipeEntities();
    const router = createRoutes(mongoStorage);
    await startServer(router, getNgsiConfig());
  });

  after(async () => {
    await mongoRaw.wipeEntities();
    await stopMongo();
    await stopServer();
  });

  it('create some entities', async () => {
    const entities = _.times(120, (index) => ({
      id: generateId(index),
      val1: index % 20,
      val2: Math.floor(index / 30),
      val3: index % 3,
    }));
    await Promise.all(_.map(entities, async (entity) => {
      await clientApi.createEntity({entity});
    }));
    const num = await mongoRaw.countEntities();
    assert.strictEqual(num, 120);
  });

  describe('no sorting', () => {

    let firstForty;
    it('get firstForty', async () => {
      // get firstForty
      const params = {limit: 40};
      const {data} = await clientApi.getEntities({params});
      assert.strictEqual(data.length, 40);
      firstForty = data;
    });

    it('Get third page with offset 14 & limit 7', async () => {
      const params = {
        limit: 7,
        offset: 14,
      };
      const {data: entities} = await clientApi.getEntities({params});
      assert.deepStrictEqual(entities, firstForty.slice(14, 21));
    });

    it('Get third page with pageItemId borky_0013 & pageDirection next & limit 7', async () => {
      const params = {
        limit: 7,
        pageItemId: firstForty[13].id,
        pageDirection: 'next',
      };
      const {data: entities} = await clientApi.getEntities({params});
      assert.deepStrictEqual(entities, firstForty.slice(14, 21));
    });

    it('Get third page with pageItemId borky_0021 & pageDirection prev & limit 7', async () => {
      const params = {
        limit: 7,
        pageItemId: firstForty[21].id,
        pageDirection: 'prev',
      };
      const {data: entities} = await clientApi.getEntities({params});
      assert.deepStrictEqual(entities, _.reverse(firstForty.slice(14, 21)));
    });

  });

  describe('with multi-sorting', () => {

    let thirdPageIds;
    let beforeThirdPageId;
    let afterThirdPageId;
    it('get thirdPageIds', async () => {
      // get firstForty
      const params = {limit: 40, orderBy: 'val1,!val3,val2'};
      const {data: firstForty} = await clientApi.getEntities({params});
      assert.strictEqual(firstForty.length, 40);
      thirdPageIds = _.times(7, (index) => {
        const entityId = firstForty[14 + index].id;
        return entityId;
      });
      assert.deepStrictEqual(thirdPageIds, [
        'borky_0022',
        'borky_0082',
        'borky_0042',
        'borky_0102',
        'borky_0023',
        'borky_0083',
        'borky_0043',
      ]);
      beforeThirdPageId = firstForty[13].id;
      assert.strictEqual(beforeThirdPageId, 'borky_0062');
      afterThirdPageId = firstForty[21].id;
      assert.strictEqual(afterThirdPageId, 'borky_0103');
    });

    it('Get third page with offset 14 & limit 7', async () => {
      const params = {
        limit: 7,
        offset: 14,
        orderBy: 'val1,!val3,val2',
      };
      const {data: entities} = await clientApi.getEntities({params});
      const entityIds = _.map(entities, (ent) => ent.id);
      assert.deepStrictEqual(entityIds, thirdPageIds);
    });

    it('Get third page with pageItemId borky_0013 & pageDirection next & limit 7', async () => {
      const params = {
        limit: 7,
        orderBy: 'val1,!val3,val2',
        pageItemId: beforeThirdPageId,
        pageDirection: 'next',
      };
      const {data: entities} = await clientApi.getEntities({params});
      const entityIds = _.map(entities, (ent) => ent.id);
      assert.deepStrictEqual(entityIds, thirdPageIds);
    });

    it('Get third page with pageItemId borky_0021 & pageDirection prev & limit 7', async () => {
      const params = {
        limit: 7,
        orderBy: 'val1,!val3,val2',
        pageItemId: afterThirdPageId,
        pageDirection: 'prev',
      };
      const {data: entities} = await clientApi.getEntities({params});
      const entityIds = _.map(entities, (ent) => ent.id);
      assert.deepStrictEqual(entityIds, _.reverse(thirdPageIds));
    });

  });

});
