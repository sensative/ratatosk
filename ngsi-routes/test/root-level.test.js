/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const proxyquire = require('proxyquire');

const {
  assertThrows,
  ngsiMockStorage: {createMockStorage},
} = require('@ratatosk/ngsi-utils');

const {createRoutes} = proxyquire('../src', {
  './ngsi-v2-routes': {createRoutes: (config) => (req, res) => res.json({did: 'it'})},
});

const storage = createMockStorage();

describe('root-level tests', () => {

  describe('storage validity', () => {

    it('throws error if invalid ngsi-api storage', () => {
      const invalidStorages = [
        null,
        1,
        false,
        true,
        'storage',
      ];
      _.each(invalidStorages, (invalid) => {
        const message = 'Invalid ngsi storage object';
        assertThrows(() => createRoutes(invalid), message);
      });
    });

    it('extraneous keys are NOT allowed in ngsi-api storage-options object', () => {
      const invalids = [
        {invalidOptionsKey: true},
        {getEntities: undefined, invalidOptionsKey: true},
      ];
      _.each(invalids, (invalidOptions) => {
        const message = 'Invalid keys included in ngsi storage object';
        assertThrows(() => createRoutes(invalidOptions), message);
      });
    });

    it('storage params need to be functions', () => {
      const invalids = [
        {getEntities: null},
      ];
      _.each(invalids, (invalidOptions) => {
        const message = 'Invalid ngsi storage: props must be functions';
        assertThrows(() => createRoutes(invalidOptions), message);
      });
    });

  });

  describe('check valid options object', () => {

    it('throws error if invalid options param type', () => {
      const invalids = [
        null,
        1,
        false,
        true,
      ];
      _.each(invalids, (invalidOptions) => {
        const message = 'Invalid options object';
        assertThrows(() => createRoutes(storage, invalidOptions), message);
      });
    });

    it('extraneous keys are NOT allowed in ngsi-api-options object', () => {
      const invalids = [
        {invalidOptionsKey: true},
      ];
      _.each(invalids, (invalidOptions) => {
        const message = 'Invalid keys included in ngsi-api-options object';
        assertThrows(() => createRoutes(storage, invalidOptions), message);
      });
    });


    it('should be able to create default routes with valid options', () => {
      const valids = [
        undefined,
        {},
      ];
      _.each(valids, (validOptions) => {
        assert.doesNotThrow(() => createRoutes(storage, validOptions));
      });
    });

  });

});
