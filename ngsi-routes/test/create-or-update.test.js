/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

// const _ = require('lodash');
const assert = require('assert');

const {
  assertRejects,
  ngsiEntityFormatter: {formatEntity},
  ngsiResponseErrorUtils: {assertErrorsMatch},
} = require('@ratatosk/ngsi-utils');

const {
  ORION_ERRORS,
  REST_REQUEST_HEADER_KEYS,
} = require('@ratatosk/ngsi-constants');


// test setup info (local stuff)
const {getNgsiConfig, getMongoConfig} = require('./test-config');
const {restart: startServer, stop: stopServer} = require('./test-server');
const {createRoutes} = require('../src');
const {
  connectionManager: {
    restart: startMongo,
    stop: stopMongo,
    mongoRaw,
  },
} = require('@ratatosk/ngsi-mongo-storage');

const {createApi: createClientApi} = require('@ratatosk/ngsi-client-api');
const clientApi = createClientApi(getNgsiConfig());

describe('crud flowthrough', () => {

  before(async () => {
    const mongoStorage = await startMongo(getMongoConfig());
    await mongoRaw.wipeEntities();
    const router = createRoutes(mongoStorage);
    await startServer(router, getNgsiConfig());
  });

  after(async () => {
    // await mongoRaw.wipeEntities();
    await stopMongo();
    await stopServer();
  });

  it('should be possible to create entities', async () => {
    const entity = formatEntity({
      id: 'asdf',
      type: 'bob',
      arp: 2,
    });
    const res = await clientApi.createEntity({entity});
    assert.strictEqual(res.status, 201);
  });

  it('normally, creating another entity with same(ish) info FAILS', async () => {
    const entity = formatEntity({
      id: 'asdf',
      type: 'bob',
      deep: 'deep',
    });
    const err = await assertRejects(clientApi.createEntity({entity}));
    assertErrorsMatch(err, ORION_ERRORS.UNPROCESSABLE_ENTITY_ALREADY_EXISTS);
  });

  it('with "upserts" flag, the create succeeds (with an update)', async () => {
    const entity = formatEntity({
      id: 'asdf',
      type: 'bob',
      deep: 'deep',
    });
    const headers = {
      [REST_REQUEST_HEADER_KEYS.UPSERT]: true,
    };
    const res = await clientApi.createEntity({entity, headers});
    assert.strictEqual(res.status, 204);
    // and check that entities contents looks correct
    const {data: entities} = await clientApi.getEntities();
    assert.strictEqual(entities.length, 1);
    const savedEnt = entities[0];
    assert.strictEqual(savedEnt.arp.value, 2);
    assert.strictEqual(savedEnt.deep.value, 'deep');
  });

});
