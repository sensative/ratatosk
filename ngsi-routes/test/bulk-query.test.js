/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');

const {
  assertThrows,
} = require('@ratatosk/ngsi-utils');

// test setup info (local stuff)
const {getNgsiConfig, getMongoConfig} = require('./test-config');
const {restart: startServer, stop: stopServer} = require('./test-server');
const {
  connectionManager: {
    restart: startMongo,
    stop: stopMongo,
    mongoRaw,
  },
} = require('@ratatosk/ngsi-mongo-storage');
const {createRoutes} = require('../src');

const {createApi: createClientApi} = require('@ratatosk/ngsi-client-api');
const clientApi = createClientApi(getNgsiConfig());


const entities = [
  {id: 'aa', type: 'aaa'},
  {id: 'bb', type: 'bbb'},
  {id: 'aa', type: 'bbb'},
];

describe('bulk-query', () => {

  before(async () => {
    const mongoStorage = await startMongo(getMongoConfig());
    await mongoRaw.wipeEntities();
    const router = createRoutes(mongoStorage);
    await startServer(router, getNgsiConfig());
    await Promise.all(_.map(entities, (entity) => clientApi.createEntity({entity})));
  });

  after(async () => {
    await mongoRaw.wipeEntities();
    await stopMongo();
    await stopServer();
  });


  it('bulk-query empty payload', async () => {
    const payload = {};
    const {data: entities} = await clientApi.bulkQuery({payload});
    assert.strictEqual(entities.length, 3);
  });

  it('bulk-query non-object payload', async () => {
    const payload = false;
    assertThrows(() => clientApi.bulkQuery.createRequestConfig({payload}));
    const config = clientApi.bulkQuery.createRequestConfig({payload: {}});
    _.set(config, 'data', false);
    // but this guy DOES NOT throw. the body parser strikes again
    const {data: docs} = await clientApi.rawRequest(config);
    assert.strictEqual(docs.length, 3);
  });

});
