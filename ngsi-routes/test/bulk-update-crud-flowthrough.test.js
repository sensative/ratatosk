/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');

const {
  assertRejects,
  ngsiResponseErrorUtils: {assertErrorsMatch, assertErrorsMatchesColonTruncated},
} = require('@ratatosk/ngsi-utils');

const {
  ORION_ERRORS,
} = require('@ratatosk/ngsi-constants');

// test setup info (local stuff)
const {getNgsiConfig, getMongoConfig} = require('./test-config');
const {restart: startServer, stop: stopServer} = require('./test-server');
const {
  connectionManager: {
    restart: startMongo,
    stop: stopMongo,
    mongoRaw,
  },
} = require('@ratatosk/ngsi-mongo-storage');
const {createRoutes} = require('../src');

const {createApi: createClientApi} = require('@ratatosk/ngsi-client-api');
const clientApi = createClientApi(getNgsiConfig());


describe('bulk-update-crud-flowthrough', () => {

  before(async () => {
    const mongoStorage = await startMongo(getMongoConfig());
    await mongoRaw.wipeEntities();
    const router = createRoutes(mongoStorage);
    await startServer(router, getNgsiConfig());
  });

  after(async () => {
    await mongoRaw.wipeEntities();
    await stopMongo();
    await stopServer();
  });

  it('create some fresh entities (append)', async () => {
    const entities = [
      {id: 'aa', type: 'AA', derp: true},
      {id: 'bb', type: 'BB', derp: {value: 3.4, metadata: {merp: 'erp'}}},
    ];
    const actionType = 'append';
    await clientApi.bulkUpdate({payload: {actionType, entities}});
    const {data: docs} = await clientApi.getEntities();
    assert.strictEqual(docs.length, 2);
    const trueDerp = _.find(docs, {id: 'aa'}).derp.value;
    assert.strictEqual(trueDerp, true);
    const merpDerp = _.find(docs, {id: 'bb'}).derp.metadata.merp.value;
    assert.strictEqual(merpDerp, 'erp');
  });

  it('create one fresh and update one old (append)', async () => {
    const entities = [
      {id: 'aa', type: 'AA', derp: false, lerp: 23}, // changes old attr, adds new attr
      {id: 'cc', type: 'CC', derp: null},
    ];
    const actionType = 'append';
    await clientApi.bulkUpdate({payload: {actionType, entities}});
    const {data: docs} = await clientApi.getEntities();
    assert.strictEqual(docs.length, 3);
    const trueDerp = _.find(docs, {id: 'aa'}).derp.value;
    assert.strictEqual(trueDerp, false);
    const numLerp = _.find(docs, {id: 'aa'}).lerp.value;
    assert.strictEqual(numLerp, 23);
    const merpDerp = _.find(docs, {id: 'cc'}).derp.value;
    assert.strictEqual(merpDerp, null);
  });

  it('create a fresh entity and add a fresh attr to one old entity (appendStrict)', async () => {
    const entities = [
      {id: 'aa', type: 'AA', sherp: 'dibbs'}, // changes old attr, adds new attr
      {id: 'dd', type: 'DD', derp: 'asdf'},
    ];
    const actionType = 'appendStrict';
    await clientApi.bulkUpdate({payload: {actionType, entities}});
    const {data: docs} = await clientApi.getEntities();
    assert.strictEqual(docs.length, 4);
    const trueDerp = _.find(docs, {id: 'aa'}).derp.value;
    assert.strictEqual(trueDerp, false); // is left unchanged
    const numLerp = _.find(docs, {id: 'aa'}).sherp.value;
    assert.strictEqual(numLerp, 'dibbs'); // was just added
    const merpDerp = _.find(docs, {id: 'dd'}).derp.value;
    assert.strictEqual(merpDerp, 'asdf');
  });

  it('cannot update existing attr on old entity (appendStrict)', async () => {
    const entities = [
      {id: 'aa', type: 'AA', derp: 'thiswillnotwork'},
    ];
    const actionType = 'appendStrict';
    const err = await assertRejects(clientApi.bulkUpdate({payload: {actionType, entities}}));
    assertErrorsMatchesColonTruncated(err, ORION_ERRORS.UNPROCESSABLE_ATTRIBUTE_ALREADY_EXISTS);
  });

  it('update existing attrs (update)', async () => {
    const entities = [
      {id: 'aa', type: 'AA', derp: 'thisshouldwork'},
    ];
    const actionType = 'update';
    await clientApi.bulkUpdate({payload: {actionType, entities}});
    const {data: docs} = await clientApi.getEntities();
    assert.strictEqual(docs.length, 4);
    const newDerp = _.find(docs, {id: 'aa'}).derp.value;
    assert.strictEqual(newDerp, 'thisshouldwork'); // is left unchanged
    const numAttrs = _.size(_.find(docs, {id: 'aa'}));
    assert.strictEqual(numAttrs, 5); // other attrs not affected
  });

  it('cannot add new attrs with update (update)', async () => {
    const entities = [
      {id: 'aa', type: 'AA', croderp: 'thiswillnotwork'},

    ];
    const actionType = 'update';
    const err = await assertRejects(clientApi.bulkUpdate({payload: {actionType, entities}}));
    assertErrorsMatch(err, ORION_ERRORS.FIND_ERROR_ATTR_MISSING);
  });

  it('cannot create fresh entity with update (update)', async () => {
    const entities = [
      {id: 'mmmmmuppet', type: 'AA', croderp: 'thiswillnotwork'},
    ];
    const actionType = 'update';
    const err = await assertRejects(clientApi.bulkUpdate({payload: {actionType, entities}}));
    assertErrorsMatch(err, ORION_ERRORS.FIND_ERROR_ENTITY_MISSING);
  });

  it('replace all attrs for existing entity (replace)', async () => {
    const entities = [
      {id: 'aa', type: 'AA', derp: 'shouldbeonlysurvivingattr'},
    ];
    const actionType = 'replace';
    await clientApi.bulkUpdate({payload: {actionType, entities}});
    const {data: docs} = await clientApi.getEntities();
    assert.strictEqual(docs.length, 4);
    const newDerp = _.find(docs, {id: 'aa'}).derp.value;
    assert.strictEqual(newDerp, 'shouldbeonlysurvivingattr'); // is left unchanged
    const numAttrs = _.size(_.find(docs, {id: 'aa'}));
    assert.strictEqual(numAttrs, 3); // other attrs are gone (id, type, derp)
  });

  it('cannot create fresh entity with replace (replace)', async () => {
    const entities = [
      {id: 'mmmmmuppet', type: 'AA', croderp: 'thiswillnotwork'},
    ];
    const actionType = 'replace';
    const err = await assertRejects(clientApi.bulkUpdate({payload: {actionType, entities}}));
    assertErrorsMatch(err, ORION_ERRORS.FIND_ERROR_ENTITY_MISSING);
  });

  it('delete an attr from an existing entity (delete)', async () => {
    const entities = [
      {id: 'aa', type: 'AA', derp: 'thiswilldisappear'},
    ];
    const actionType = 'delete';
    await clientApi.bulkUpdate({payload: {actionType, entities}});
    const {data: docs} = await clientApi.getEntities();
    assert.strictEqual(docs.length, 4);
    const numAttrs = _.size(_.find(docs, {id: 'aa'}));
    assert.strictEqual(numAttrs, 2); // other attrs are gone (id, type)
  });

  it('delete a non-existing attr from an existing entity fails (delete)', async () => {
    const entities = [
      {id: 'aa', type: 'AA', croderp: 'thiswillnotwork'},
    ];
    const actionType = 'delete';
    const err = await assertRejects(clientApi.bulkUpdate({payload: {actionType, entities}}));
    assertErrorsMatch(err, ORION_ERRORS.FIND_ERROR_ATTR_MISSING);
  });

  it('delete an existing entity (delete)', async () => {
    const entities = [
      {id: 'aa', type: 'AA'},
    ];
    const actionType = 'delete';
    await clientApi.bulkUpdate({payload: {actionType, entities}});
    const {data: docs} = await clientApi.getEntities();
    assert.strictEqual(docs.length, 3);
    const aa = _.find(docs, {id: 'aa'});
    assert.strictEqual(aa, undefined); // was disappeared
  });

  it('delete a non-existing attr from a non-existing entity does not work (delete)', async () => {
    const entities = [
      {id: 'mmmmmuppet', type: 'AA', croderp: 'thiswillnotwork'},
    ];
    const actionType = 'delete';
    const err = await assertRejects(clientApi.bulkUpdate({payload: {actionType, entities}}));
    assertErrorsMatch(err, ORION_ERRORS.FIND_ERROR_ENTITY_MISSING);
  });

  it('delete a non-existing attr from a non-existing entity does not work (delete)', async () => {
    const entities = [
      {id: 'mmmmmuppet', type: 'AA'},
    ];
    const actionType = 'delete';
    const err = await assertRejects(clientApi.bulkUpdate({payload: {actionType, entities}}));
    assertErrorsMatch(err, ORION_ERRORS.FIND_ERROR_ENTITY_MISSING);
  });

});
