/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const express = require('express');

let server = null;

const restart = async (router, config) => {
  await stop();
  const app = express();
  return new Promise((resolve) => {
    server = require('http')
      .createServer(app)
      .listen(config.port, () => {
        app.use(router);
        console.info(`test-server STARTED listening to port ${config.port}`);
        resolve(app);
      });
  });
};

const stop = async () => {
  if (!server) {
    return;
  }
  const copy = server;
  server = null;
  await copy.close();
  console.info('test-server STOPPED listening');
};

module.exports = {
  restart,
  stop,
};
