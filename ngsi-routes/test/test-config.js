/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const config = {
  ngsiServer: {
    host: 'localhost',
    port: '9773',
  },
  mongo: {
    url: null,
    dbName: 'chibbity',
    options: {},
  },
};

// This mutable stuff is to accomodate MongoMemoryServer
// which does not have a static ip address prior to start
let currentMongoUrl = null;
const setMongoUrl = (url) => {
  currentMongoUrl = url;
};

const getNgsiConfig = () => _.assign({}, config.ngsiServer);

const getMongoConfig = () => _.assign({}, config.mongo, {url: currentMongoUrl});


module.exports = {
  getNgsiConfig,
  setMongoUrl,
  getMongoConfig,
};
