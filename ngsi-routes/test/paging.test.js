/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');

const {
  ngsiEntityFormatter: {formatEntity},
} = require('@ratatosk/ngsi-utils');


// test setup info (local stuff)
const {getNgsiConfig, getMongoConfig} = require('./test-config');
const {restart: startServer, stop: stopServer} = require('./test-server');
const {createRoutes} = require('../src');
const {
  connectionManager: {
    restart: startMongo,
    stop: stopMongo,
    mongoRaw,
  },
} = require('@ratatosk/ngsi-mongo-storage');

const {createApi: createClientApi} = require('@ratatosk/ngsi-client-api');
const clientApi = createClientApi(getNgsiConfig());

describe('crud flowthrough', () => {

  before(async () => {
    const mongoStorage = await startMongo(getMongoConfig());
    await mongoRaw.wipeEntities();
    const router = createRoutes(mongoStorage);
    await startServer(router, getNgsiConfig());
  });

  after(async () => {
    await mongoRaw.wipeEntities();
    await stopMongo();
    await stopServer();
  });

  it('should be possible to create 352 entities', async () => {
    const allRes = await Promise.all(_.times(352, async (index) => {
      const zeroPad = (num) => `00${num}`.slice(-3);
      const entity = formatEntity({
        id: `fabby_${zeroPad(index)}`,
        type: 'bob',
        arp: 2,
      });
      const res = await clientApi.createEntity({entity});
      assert.strictEqual(res.status, 201);
    }));
    assert.strictEqual(allRes.length, 352);
  });

  it('should be possible to get entities without limit and offset or order', async () => {
    const res = await clientApi.getEntities();
    assert.strictEqual(res.status, 200);
    assert.strictEqual(res.data.length, 20);
  });

  it('should be possible to get entities without limit and offset WITH order', async () => {
    const res = await clientApi.getEntities({params: {orderBy: 'id'}});
    assert.strictEqual(res.status, 200);
    assert.strictEqual(res.data.length, 20);
    assert.strictEqual(res.data[19].id, 'fabby_019'); // yup.. is alphabetical
  });

  it('should be possible to get entities WITH limit and offset and order', async () => {
    const res = await clientApi.getEntities({params: {orderBy: 'id', limit: 25, offset: 32}});
    assert.strictEqual(res.status, 200);
    assert.strictEqual(res.data.length, 25);
    assert.strictEqual(res.data[24].id, 'fabby_056'); // yup.. is alphabetical
  });

  it('should be possible to get entities WITH itemId, itemValue, direction and order', async () => {
    const res = await clientApi.getEntities({params: {orderBy: 'id', limit: 25, offset: 32}});
    assert.strictEqual(res.status, 200);
    assert.strictEqual(res.data.length, 25);
    assert.strictEqual(res.data[24].id, 'fabby_056'); // yup.. is alphabetical
  });

});
