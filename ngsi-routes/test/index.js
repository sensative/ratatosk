/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const path = require('path');
const {lint} = require('@ratatosk/ngsi-linter');

const {MongoMemoryServer} = require('mongodb-memory-server');
const {setMongoUrl} = require('./test-config');

let mongoServer;
before(() => {
  mongoServer = new MongoMemoryServer();
  return mongoServer.getConnectionString()
    .then((mongoUrl) => setMongoUrl(mongoUrl));
});
after(async () => {
  await mongoServer.stop();
});

describe('testing components in orion-test-utility', () => {

  it('got into main describe', () => 0);

  // ---- lint
  it('linting', async () => {
    await lint(path.join(__dirname, '..'));
  });

  require('./root-level.test');
  require('./crud-flowthrough.test');
  require('./bulk-query.test');
  require('./bulk-update-crud-flowthrough.test');
  require('./subscriptions-manager-indy.test');
  require('./global-registrations-and-updates.test');

  require('./inject-aggregation-generator.test');
  require('./user-access-token.test');
  require('./create-or-update.test');

  require('./paging.test');
  require('./pageItem-paging.test');

});
