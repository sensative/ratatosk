/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const sinon = require('sinon');

// test setup info (local stuff)
const {getNgsiConfig, getMongoConfig} = require('./test-config');
const {restart: startServer, stop: stopServer} = require('./test-server');
const {createRoutes} = require('../src');
const {
  connectionManager: {
    restart: startMongo,
    stop: stopMongo,
    mongoRaw,
  },
} = require('@ratatosk/ngsi-mongo-storage');

const {createApi: createClientApi} = require('@ratatosk/ngsi-client-api');
const clientApi = createClientApi(getNgsiConfig());

//
//
// This is the guy that is being flexed
const customAggregationGenerator = sinon.spy(() => [
  {$match: {'attrs.joh.value': {$lt: 10}}},
]);


describe('inject-aggregation-generator', () => {

  before(async () => {
    const mongoStorage = await startMongo(getMongoConfig());
    await mongoRaw.wipeEntities();
    const routerOptions = {
      userAccess: true,
      customAggregationGenerator,
    };
    const router = createRoutes(mongoStorage, routerOptions);
    await startServer(router, getNgsiConfig());
  });

  after(async () => {
    await mongoRaw.wipeEntities();
    await stopMongo();
    await stopServer();
  });

  it('create some entities', async () => {
    const headers = {'Fiware-UserId': 'whatever'};
    const entities = [
      {id: 'aarb', joh: 2},
      {id: 'barb', joh: 3},
      {id: 'carb', joh: 12},
      {id: 'darb', joh: 13},
    ];
    await Promise.all(_.map(entities, async (entity) => {
      await clientApi.createEntity({entity, headers});
    }));
    const num = await mongoRaw.countEntities();
    assert.strictEqual(num, 4);
    assert.strictEqual(customAggregationGenerator.callCount, 4);
    customAggregationGenerator.resetHistory();
  });

  it('get some entities (using the customAggregationGenerator)', async () => {
    const userId = 'whatever';
    const {data: docs} = await clientApi.getEntities({userId});
    assert.strictEqual(docs.length, 2);
    _.each(docs, (doc) => {
      assert(doc.joh.value < 10);
    });
    assert.strictEqual(customAggregationGenerator.callCount, 1);
    customAggregationGenerator.resetHistory();
  });

});
