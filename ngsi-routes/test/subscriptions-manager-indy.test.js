/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const assert = require('assert');
const proxyquire = require('proxyquire');
const {ngsiMockStorage: {createMockStorage}} = require('@ratatosk/ngsi-utils');

const notificationPublisher = proxyquire('../src/subscriptions-manager/notification-publisher', {
  axios: async (payload) => payload,
});

const {triggerSubscriptions} = proxyquire('../src/subscriptions-manager', {
  './notification-publisher': notificationPublisher,
});

const payloadItem = {
  subscription: {
    id: 'subId',
    notification: {http: {url: 'someUrl'}},
  },
  notification: 'The notification payload',
};

const mockStorage = createMockStorage();
mockStorage.generateNotifications.resolves([payloadItem]);

describe('subscriptions-manager-indy.test', () => {

  it('things should work as expected', async () => {
    await triggerSubscriptions(mockStorage, {});
    assert(mockStorage.generateNotifications.calledOnce);
    const numCalls = mockStorage.acknowledgeNotification.callCount;
    assert.strictEqual(numCalls, 1);
  });

});
