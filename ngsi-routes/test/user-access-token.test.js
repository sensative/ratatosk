/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');

const {createApi: createClientApi} = require('@ratatosk/ngsi-client-api');

const {
  connectionManager: {
    restart: startMongo,
    stop: stopMongo,
    mongoRaw,
  },
} = require('@ratatosk/ngsi-mongo-storage');

const {
  assertRejects,
} = require('@ratatosk/ngsi-utils');

// test setup info (local stuff)
const {getNgsiConfig, getMongoConfig} = require('./test-config');
const {restart: startServer, stop: stopServer} = require('./test-server');
const {createRoutes} = require('../src');

const clientApi = createClientApi(getNgsiConfig());

// our custom tokenParser that MUST (i.e. can only) be supplied through options
const userTokenParser = _.trim;

describe('user-access-token', () => {

  // describe('valid options params', () => {
  //
  //   it('userTokenParser: userAccess must be true', async () => {
  //     const mongoStorage = await startMongo(getMongoConfig());
  //     await mongoRaw.wipeEntities();
  //     const invalids = [
  //       {
  //         routerOptions: {
  //           userAccess: false,
  //           userTokenParser,
  //         },
  //         mess: 'userTokenParser cannot be included if userAccess is not required',
  //       },
  //       {
  //         routerOptions: {
  //           userAccess: 'Anything that is not boolean',
  //           userTokenParser,
  //         },
  //         mess: 'userAccess must be a boolean if included',
  //       },
  //       {
  //         routerOptions: {
  //           userAccess: true,
  //           userTokenParser: 'Must be a function',
  //         },
  //         mess: 'userTokenParser must be a function when included',
  //       },
  //     ];
  //     _.each(invalids, (invalid) => {
  //       assertThrows(() => createRoutes(mongoStorage, invalid.routerOptions), invalid.mess);
  //     });
  //
  //     // and the valid case
  //     const validRouterOptions = {
  //       userAccess: true,
  //       userTokenParser,
  //     };
  //     const router = createRoutes(mongoStorage, validRouterOptions);
  //     await startServer(router, getNgsiConfig());
  //     // and shut it down
  //     await mongoRaw.wipeEntities();
  //     await stopMongo();
  //     await stopServer();
  //   });
  //
  // });
  //
  describe('different headers, test userAccess functionality', () => {

    before(async () => {
      const mongoStorage = await startMongo(getMongoConfig());
      await mongoRaw.wipeEntities();
      const routerOptions = {
        userAccess: true,
        userTokenParser,
      };
      const router = createRoutes(mongoStorage, routerOptions);
      await startServer(router, getNgsiConfig());
    });

    after(async () => {
      await mongoRaw.wipeEntities();
      await stopMongo();
      await stopServer();
    });

    it('some invalids', async () => {
      const invalids = [
        {
          userId: undefined,
          userToken: undefined,
          mess: 'Fiware-UserToken is invalid.',
        },
        {
          userId: 'abcdef',
          userToken: undefined,
          mess: 'Fiware-UserToken is invalid.',
        },
      ];
      await Promise.all(_.map(invalids, async (invalid) => {
        // since we use the test-server with axios, we get the error through
        // the rest response
        const headers = _.pickBy({
          'Fiware-UserId': invalid.userId,
          'Fiware-UserToken': invalid.userToken,
        });
        const err = await assertRejects(clientApi.getEntities({headers}));
        const errDesc = _.get(err, 'response.data.description');
        assert.strictEqual(errDesc, invalid.mess);
      }));
    });

    it('some valids', async () => {
      const valids = [
        '0123456789ab', // must be ObjectId length
        '        0123456789ab          ', // our custom trim() parser
      ];
      await Promise.all(_.map(valids, async (valid) => {
        const headers = {'Fiware-UserToken': valid};
        const res = await clientApi.getEntities({headers});
        const entities = res.data;
        assert.deepStrictEqual(entities, []);
      }));
    });


  });

  describe('valid config, test tokenParser functionality', () => {

    before(async () => {
      const mongoStorage = await startMongo(getMongoConfig());
      await mongoRaw.wipeEntities();
      const routerOptions = {
        userAccess: true,
        userTokenParser: () => {
          throw new Error('Could not grab userId');
        },
      };
      const router = createRoutes(mongoStorage, routerOptions);
      await startServer(router, getNgsiConfig());
    });

    after(async () => {
      await mongoRaw.wipeEntities();
      await stopMongo();
      await stopServer();
    });

    it('some invalids', async () => {
      const invalids = [
        {
          userId: undefined,
          userToken: undefined,
          mess: 'Fiware-UserToken is invalid.',
        },
        {
          userId: undefined,
          userToken: 'asdf',
          mess: 'Fiware-UserToken is invalid.',
        },
        {
          userId: 'abcdef',
          userToken: undefined,
          mess: 'Fiware-UserToken is invalid.',
        },
      ];
      await Promise.all(_.map(invalids, async (invalid) => {
        // since we use the test-server with axios, we get the error through
        // the rest response
        const headers = _.pickBy({
          'Fiware-UserId': invalid.userId,
          'Fiware-UserToken': invalid.userToken,
        });
        const err = await assertRejects(clientApi.getEntities({headers}));
        const errDesc = _.get(err, 'response.data.description');
        assert.strictEqual(errDesc, invalid.mess);
      }));
    });
  });

});
