/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const assert = require('assert');
const _ = require('lodash');
const delay = require('delay');

// const {assertRejects} = require('@ratatosk/ngsi-utils');

const {
  orionApi,
  ngsiApi,
} = require('../../client-api-util');
const mongoApi = require('../../mongo-api');
const notificationsApi = require('../../notification-buffer-api');

const {notificationBuffer: {host, port}} = require('../../config');

const entityRaw = {
  id: 'dredd',
  type: 'smerp',
};

const subscriptionRaw = {
  description: 'scroompy',
  subject: {
    entities: [{id: 'dredd'}],
  },
  notification: {
    http: {url: `http://${host}:${port}/notify`},
  },
};

describe('check-crud-notifications.test', () => {

  const fiwareApis = {
    orionApi,
    // ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      let subscriptionId;

      before(async () => {
        await mongoApi.assertEntitiesEmpty();
        await mongoApi.assertSubscriptionsEmpty();
        await notificationsApi.getNotifications();
      });

      after(async () => {
        await mongoApi.wipeEntities();
        await mongoApi.wipeSubscriptions();
      });

      it('create a working subscription', async () => {
        // as of yet we should have intercepted no notifications
        const notificationsInit = await notificationsApi.getNotifications();
        // DIVERGE
        if (fiwareApi === orionApi) {
          // assert.strictEqual(notificationsInit.length, 0, 'at notificationsInit'); // WHOA... STICKY!!
        }
        if (fiwareApi === ngsiApi) {
          assert.strictEqual(notificationsInit.length, 0, 'ngsiApi notificationsInit');
        }

        await fiwareApi.createSubscription({payload: subscriptionRaw});
        const [subscription] = await mongoApi.findSubscriptions();
        subscriptionId = subscription._id.toString(); // save!!

        // DIVERGE
        await delay(250);
        const notificationsFinal = await notificationsApi.getNotifications();
        if (fiwareApi === orionApi) {
          assert.strictEqual(notificationsFinal.length, 0, 'ngsiApi notificationsFinal');
          // assert.strictEqual(notificationsInit.length, 0, 'orionApi notificationsInit'); // WHOA... STICKY!!
        }
        if (fiwareApi === ngsiApi) {
          assert.strictEqual(notificationsFinal.length, 0, 'ngsiApi notificationsFinal');
        }

      });

      it('create a matching entity', async () => {
        // as of yet we should have intercepted no notifications
        const notificationsInit = await notificationsApi.getNotifications();
        // DIVERGE
        if (fiwareApi === orionApi) {
          assert.strictEqual(notificationsInit.length, 0, 'at notificationsInit'); // WHOA... STICKY!!
        }
        if (fiwareApi === ngsiApi) {
          assert.strictEqual(notificationsInit.length, 0, 'ngsiApi notificationsInit');
        }

        await fiwareApi.createEntity({entity: entityRaw});

        // DIVERGE
        await delay(250);
        const notificationsFinal = await notificationsApi.waitForNextNotification();
        let finalNote;
        if (fiwareApi === orionApi) {
          assert(notificationsFinal.length >= 1, 'ngsiApi notificationsFinal'); // so sticky
          finalNote = _.find(notificationsFinal, {subscriptionId});
        }
        if (fiwareApi === ngsiApi) {
          assert.strictEqual(notificationsFinal.length, 1, 'ngsiApi notificationsFinal');
          finalNote = notificationsFinal.pop();
        }
        const expectedNotification = {
          subscriptionId,
          data: [{
            id: 'dredd',
            type: 'smerp',
          }],
        };
        assert.deepStrictEqual(finalNote, expectedNotification);
      });

      it('updating a matching entity', async () => {
        // as of yet we should have intercepted no notifications
        const notificationsInit = await notificationsApi.getNotifications();
        // DIVERGE
        if (fiwareApi === orionApi) {
          assert.strictEqual(notificationsInit.length, 0, 'at notificationsInit'); // WHOA... STICKY!!
        }
        if (fiwareApi === ngsiApi) {
          assert.strictEqual(notificationsInit.length, 0, 'ngsiApi notificationsInit');
        }

        await fiwareApi.upsertEntityAttrs({entity: entityRaw, attrs: {doop: 'noop'}});

        // DIVERGE
        await delay(250);
        const notificationsFinal = await notificationsApi.waitForNextNotification();
        let finalNote;
        if (fiwareApi === orionApi) {
          assert(notificationsFinal.length >= 1, 'ngsiApi notificationsFinal'); // so sticky
          finalNote = _.find(notificationsFinal, {subscriptionId});
        }
        if (fiwareApi === ngsiApi) {
          assert.strictEqual(notificationsFinal.length, 1, 'ngsiApi notificationsFinal');
          finalNote = notificationsFinal.pop();
        }
        const expectedNotification = {
          subscriptionId,
          data: [{
            id: 'dredd',
            type: 'smerp',
            doop: {type: 'Text', value: 'noop', metadata: {}},
          }],
        };
        assert.deepStrictEqual(finalNote, expectedNotification);
      });

      it('delete the matching entity', async () => {
        // as of yet we should have intercepted no notifications
        const notificationsInit = await notificationsApi.getNotifications();
        // DIVERGE
        if (fiwareApi === orionApi) {
          assert.strictEqual(notificationsInit.length, 0, 'at notificationsInit'); // WHOA... STICKY!!
        }
        if (fiwareApi === ngsiApi) {
          assert.strictEqual(notificationsInit.length, 0, 'ngsiApi notificationsInit');
        }

        await fiwareApi.deleteEntity({entityId: 'dredd'});

        // DIVERGE
        await delay(250);
        const notificationsFinal = await notificationsApi.waitForNextNotification();
        if (fiwareApi === orionApi) {
          // Apparently no notification is sent upon deletion
          assert(notificationsFinal.length >= 0, 'ngsiApi notificationsFinal'); // so sticky
        }
        if (fiwareApi === ngsiApi) {
          assert.strictEqual(notificationsFinal.length, 0, 'ngsiApi notificationsFinal');
        }
      });

    });

  });

});
