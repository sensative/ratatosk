/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const assert = require('assert');
const _ = require('lodash');
const delay = require('delay');

const {
  orionApi,
  ngsiApi,
} = require('../../client-api-util');
const mongoApi = require('../../mongo-api');
const notificationsApi = require('../../notification-buffer-api');

const {notificationBuffer: {host, port}} = require('../../config');

const validEntity = {
  id: 'blue',
  type: 'merp',
};

const fullyValidSubscription = {
  description: 'scroompy',
  subject: {
    entities: [{id: 'blue'}],
  },
  notification: {
    http: {url: `http://${host}:${port}/notify`},
  },
};

const validSubscriptionWithInvalidUrl = {
  description: 'broompy',
  subject: {
    entities: [{id: 'blue'}],
  },
  notification: {
    http: {url: 'http://12.12.12.12:234/asdf/asdf'},
  },
};

const nonMatchingSubscription = {
  description: 'nonmatcher',
  subject: {
    entities: [{id: 'missing'}],
  },
  notification: {
    http: {url: `http://${host}:${port}/notify`},
  },
};

describe('trigger-subscription-notifications', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      beforeEach(async () => {
        await mongoApi.assertEntitiesEmpty();
        await mongoApi.assertSubscriptionsEmpty();
        await notificationsApi.getNotifications();
      });

      afterEach(async () => {
        await mongoApi.wipeEntities();
        await mongoApi.wipeSubscriptions();
      });

      it('create a subscription for an entity with FAILING url', async () => {
        await fiwareApi.createEntity({entity: validEntity});
        // as of yet we should have intercepted no notifications
        const notificationsPre = await notificationsApi.getNotifications();
        // DIVERGE
        if (fiwareApi === orionApi) {
          // assert.strictEqual(notificationsPre.length, 0, 'fiwareApi notificationsPre'); // WHOA... STICKY!!
        }
        if (fiwareApi === ngsiApi) {
          assert.strictEqual(notificationsPre.length, 0, 'ngsiApi notificationsPre');
        }

        // create a subscription and check it
        const subscriptionRes = await fiwareApi.createSubscription({payload: validSubscriptionWithInvalidUrl});
        assert.strictEqual(subscriptionRes.data, ''); // why not just return the id???
        const [mongoSubscription] = await mongoApi.findSubscriptions();
        const subscriptionId = mongoSubscription._id.toString();
        const {data: subInit} = await fiwareApi.findSubscription({subscriptionId});
        assert.strictEqual(subInit.description, 'broompy');

        // DIVERGE!! (no initial notification for ngsi yet)
        if (fiwareApi === orionApi) {
          assert.strictEqual(subInit.notification.timesSent, 1);
        }
        if (fiwareApi === ngsiApi) {
          assert.strictEqual(subInit.notification.timesSent, 0);
        }

        assert.strictEqual(subInit.status, 'active');
        // create an entity and check it
        const {data: entInit} = await fiwareApi.findEntity({entityId: 'blue'});
        assert.deepStrictEqual(_.omit(entInit, ['id', 'type']), {});
        // insert an attribute and check entity again
        await fiwareApi.upsertEntityAttrs({entityId: 'blue', attrs: {bb: 23}});
        const {data: entMid} = await fiwareApi.findEntity({entityId: 'blue'});
        const expectedMidAttrs = {
          bb: {type: 'Number', value: 23, metadata: {}},
        };
        assert.deepStrictEqual(_.omit(entMid, ['id', 'type']), expectedMidAttrs);

        // DIVERGE!! (no initial notification for ngsi yet)
        if (fiwareApi === orionApi) {
          // the subscription should have been triggered
          await delay(20); // just to see if anything changes
          const {data: midInit} = await fiwareApi.findSubscription({subscriptionId});
          assert.strictEqual(midInit.description, 'broompy');
          assert.strictEqual(midInit.notification.timesSent, 2);
          assert.strictEqual(midInit.status, 'active'); // seems like it should have failed (?)
        }
        if (fiwareApi === ngsiApi) {
          await delay(300);
          const {data: midInit} = await fiwareApi.findSubscription({subscriptionId});
          assert.strictEqual(midInit.description, 'broompy');
          assert.strictEqual(midInit.notification.timesSent, 1, 'midInit ngsi');
          assert.strictEqual(midInit.status, 'failed'); // is this right?
        }

        // remove the subscription & verify
        await fiwareApi.deleteSubscription({subscriptionId});
        const {data: midSubs} = await fiwareApi.getSubscriptions();
        assert.strictEqual(midSubs.length, 0, 'at midSubs.length');
      });

      it('create a subscription for an entity with WORKING url', async () => {
        await fiwareApi.createEntity({entity: validEntity});
        // as of yet we should have intercepted no notifications
        const notificationsInit = await notificationsApi.getNotifications();
        // DIVERGE
        if (fiwareApi === orionApi) {
          // assert.strictEqual(notificationsInit.length, 0, 'at notificationsInit'); // WHOA... STICKY!!
        }
        if (fiwareApi === ngsiApi) {
          assert.strictEqual(notificationsInit.length, 0, 'ngsiApi notificationsInit');
        }

        // create a fully valid subscription
        await fiwareApi.createSubscription({payload: fullyValidSubscription});
        const [validSubscription] = await mongoApi.findSubscriptions();
        const validSubscriptionId = validSubscription._id.toString();

        const {data: subFinal} = await fiwareApi.findSubscription({subscriptionId: validSubscriptionId});
        assert.strictEqual(subFinal.description, 'scroompy');
        assert.strictEqual(subFinal.status, 'active');

        // DIVERGE!! (no initial notification for ngsi yet)
        if (fiwareApi === orionApi) {
          assert.strictEqual(subFinal.notification.timesSent, 1, 'subFinal orion');
        }
        if (fiwareApi === ngsiApi) {
          assert.strictEqual(subFinal.notification.timesSent, 0, 'subFinal ngsi');
        }

        // DIVERGE!! only Orion has initial notification yet
        if (fiwareApi === orionApi) {
          // check what has happened to notifications
          await delay(30);
          const notificationsWaiter = await notificationsApi.waitForNextNotification();
          assert(notificationsWaiter.length >= 1, 'pre-update'); // some wackyness
          const expectedNotification = {
            subscriptionId: validSubscriptionId,
            data: [{
              id: 'blue',
              type: 'merp',
            }],
          };
          // CANNOT DEPEND UPON ORION NOTIFICATIONS!!
          const relevantNotification = _.find(notificationsWaiter, {subscriptionId: validSubscriptionId});
          assert.deepStrictEqual(relevantNotification, expectedNotification);
        }

        // insert an attribute and check entity again
        await fiwareApi.upsertEntityAttrs({entityId: 'blue', attrs: {juicy: 'loosy'}});
        const {data: entFinal} = await fiwareApi.findEntity({entityId: 'blue'});
        const expectedFinalAttrs = {
          juicy: {type: 'Text', value: 'loosy', metadata: {}},
        };
        assert.deepStrictEqual(_.omit(entFinal, ['id', 'type']), expectedFinalAttrs);

        // DIVERGE!!
        let finalNotification;
        if (fiwareApi === orionApi) {
          // check what has happened to notifications
          const notificationsWaiterFinal = await notificationsApi.waitForNextNotification();
          assert(notificationsWaiterFinal.length >= 1, 'notificationsWaiterFinal.length');
          finalNotification = _.find(notificationsWaiterFinal, {subscriptionId: validSubscriptionId});
        }
        if (fiwareApi === ngsiApi) {
          // check what has happened to notifications
          const notificationsWaiterFinal = await notificationsApi.waitForNextNotification();
          assert.strictEqual(notificationsWaiterFinal.length, 1, 'notificationsWaiterFinal.length');
          finalNotification = notificationsWaiterFinal.pop();
        }
        const expectedData = [{
          id: 'blue',
          type: 'merp',
          juicy: {
            type: 'Text',
            value: 'loosy',
            metadata: {},
          },
        }];
        assert.deepStrictEqual(finalNotification.data, expectedData);
      });

      it('create a subscription that does not match an entity', async () => {
        // as of yet we should have intercepted no notifications
        const notificationsPre = await notificationsApi.getNotifications();
        // DIVERGE
        if (fiwareApi === orionApi) {
          // assert.strictEqual(notificationsPre.length, 0, 'at notificationsPre'); // WHOA... STICKY!!
        }
        if (fiwareApi === ngsiApi) {
          assert.strictEqual(notificationsPre.length, 0, 'ngsiApi notificationsPre');
        }

        // create a subscription and check it
        const subscriptionRes = await fiwareApi.createSubscription({payload: nonMatchingSubscription});
        assert.strictEqual(subscriptionRes.data, ''); // why not just return the id???
        const [mongoSubscription] = await mongoApi.findSubscriptions();
        const subscriptionId = mongoSubscription._id.toString();
        const {data: subInit} = await fiwareApi.findSubscription({subscriptionId});
        assert.strictEqual(subInit.description, 'nonmatcher');
        assert.strictEqual(subInit.status, 'active');
        // DIVERGE!!
        if (fiwareApi === orionApi) {
          assert.strictEqual(subInit.notification.timesSent, undefined);
        }
        if (fiwareApi === ngsiApi) {
          assert.strictEqual(subInit.notification.timesSent, 0);
        }

        // remove the subscription & verify
        await fiwareApi.deleteSubscription({subscriptionId});
        const {data: midSubs} = await fiwareApi.getSubscriptions();
        assert.strictEqual(midSubs.length, 0, 'at midSubs.length');

        // as of yet we should have intercepted no notifications
        const notificationsPost = await notificationsApi.getNotifications();
        // DIVERGE
        if (fiwareApi === orionApi) {
          // assert.strictEqual(notificationsPost.length, 0, 'at notificationsPost'); // WHOA... STICKY!!
        }
        if (fiwareApi === ngsiApi) {
          assert.strictEqual(notificationsPost.length, 0, 'ngsiApi notificationsPost');
        }
      });

    });

  });

});
