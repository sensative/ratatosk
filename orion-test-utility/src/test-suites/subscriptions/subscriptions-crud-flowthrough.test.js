/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const assert = require('assert');
const _ = require('lodash');

// const {assertRejects} = require('@ratatosk/ngsi-utils');

const {
  orionApi,
  ngsiApi,
} = require('../../client-api-util');
const mongoApi = require('../../mongo-api');

const validSubscription = {
  id: 'getsIgnored',
  description: 'broompy',
  subject: {
    entities: [{id: 'blue'}],
  },
  notification: {
    attrs: ['myAttr'],
    http: {url: 'http://12.12.12.12:234/asdf/asdf'},
  },
};

describe('subscriptions-crud-flowthrough', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      let subscriptionId;

      before(async () => {
        await mongoApi.assertSubscriptionsEmpty();
      });

      after(async () => {
        await mongoApi.wipeSubscriptions();
      });

      it('create a raw, minimal, subscription', async () => {
        await fiwareApi.createSubscription({payload: validSubscription});
        const [sub] = await mongoApi.findSubscriptions();
        const expectedEntities = [{id: 'blue', isPattern: 'false'}];
        assert.deepStrictEqual(sub.entities, expectedEntities);
        // and set the id that will be used henceforth
        subscriptionId = sub._id.valueOf();
      });

      it('retrieve all subscriptions', async () => {
        const {data: docs} = await fiwareApi.getSubscriptions();
        assert.strictEqual(docs.length, 1);
        const [sub] = docs;
        assert.strictEqual(sub.description, 'broompy');
      });

      it('find a subscription', async () => {
        const {data: sub} = await fiwareApi.findSubscription({subscriptionId});
        assert.strictEqual(sub.description, 'broompy');
      });

      it('delete a subscription', async () => {
        await fiwareApi.deleteSubscription({subscriptionId});
        const {data: docs} = await fiwareApi.getSubscriptions();
        assert.strictEqual(docs.length, 0);
      });

    });

  });

});
