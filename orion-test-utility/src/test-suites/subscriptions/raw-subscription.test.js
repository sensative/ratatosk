/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

// This essentially tests that a simple case CRUD cycle works as expected

const assert = require('assert');
const _ = require('lodash');

// const {assertRejects} = require('@ratatosk/ngsi-utils');

const {
  orionApi,
  ngsiApi,
} = require('../../client-api-util');
const mongoApi = require('../../mongo-api');

const validSubscription = {
  id: 'getsIgnored',
  description: 'must be a string',
  subject: {
    entities: [{id: 'blue'}],
  },
  notification: {
    attrs: ['myAttr'],
    http: {url: 'http://12.12.12.12:234/asdf/asdf'},
  },
};

describe('create subscripition', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      before(async () => {
        await mongoApi.assertSubscriptionsEmpty();
      });

      afterEach(async () => {
        await mongoApi.wipeSubscriptions();
      });

      it('create a raw, minimal, subscription', async () => {
        const raw = {
          url: '/subscriptions',
          method: 'post',
          data: validSubscription,
        };
        await fiwareApi.rawRequest(raw);
        const [sub] = await mongoApi.findSubscriptions();
        const expectedEntities = [{id: 'blue', isPattern: 'false'}];
        assert.deepStrictEqual(sub.entities, expectedEntities);
      });

      it('same subscription should be makeable without using raw', async () => {
        await fiwareApi.createSubscription({payload: validSubscription});
        const [sub] = await mongoApi.findSubscriptions();
        const expectedEntities = [{id: 'blue', isPattern: 'false'}];
        assert.deepStrictEqual(sub.entities, expectedEntities);
      });

      it('use an idPattern', async () => {
        const subject = {entities: [{idPattern: 'blue'}]};
        const raw = {
          url: '/subscriptions',
          method: 'post',
          data: {...validSubscription, subject},
        };
        await fiwareApi.rawRequest(raw);
        const [sub] = await mongoApi.findSubscriptions();
        const expectedEntities = [{
          id: 'blue',
          isPattern: 'true',
        }];
        assert.deepStrictEqual(sub.entities, expectedEntities);
      });

      it('use a type', async () => {
        const subject = {entities: [{id: 'blue', type: 'special'}]};
        const raw = {
          url: '/subscriptions',
          method: 'post',
          data: {...validSubscription, subject},
        };
        await fiwareApi.rawRequest(raw);
        const [sub] = await mongoApi.findSubscriptions();
        const expectedEntities = [{
          id: 'blue',
          isPattern: 'false', // WHAT ?!
          type: 'special',
          isTypePattern: false,
        }];
        assert.deepStrictEqual(sub.entities, expectedEntities);
      });

      it('use a typePattern', async () => {
        const subject = {entities: [{id: 'blue', typePattern: 'special'}]};
        const raw = {
          url: '/subscriptions',
          method: 'post',
          data: {...validSubscription, subject},
        };
        await fiwareApi.rawRequest(raw);
        const [sub] = await mongoApi.findSubscriptions();
        const expectedEntities = [{
          id: 'blue',
          isPattern: 'false', // WHAT ?!
          type: 'special',
          isTypePattern: true,
        }];
        assert.deepStrictEqual(sub.entities, expectedEntities);
      });


    });

  });

});
