/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

// This is very similar to the the other find-entity-XX
// I'm just gonna keep this as a place to park code if necessary

const _ = require('lodash');
const assert = require('assert');

const {orionApi, ngsiApi} = require('../../client-api-util');
const mongoApi = require('../../mongo-api');

const {
  assertRejects,
  ngsiResponseErrorUtils: {assertErrorsMatch},
} = require('@ratatosk/ngsi-utils');

const {
  ORION_ERRORS,
} = require('@ratatosk/ngsi-constants');

describe('find-entity-attr-value', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      before(() => mongoApi.assertEntitiesEmpty());
      after(() => mongoApi.wipeEntities());

      describe('the basics', () => {

        before(async () => {
          const entity = {
            id: 'sploder',
            type: 'gerpy',
            agda: 'gerbil',
            bo: 73,
            jerks: {type: 'Norbos', value: 73},
            niblets: {type: 'pork', value: null},
            trinkets: 'gerbil',
          };
          await fiwareApi.createEntity({entity});
          const entityX = {
            id: 'sploder',
            type: 'keet',
          };
          await fiwareApi.createEntity({entity: entityX});
        });
        after(() => mongoApi.wipeEntities());

        it('findEntityAttrValue works when disambiguating with type', async () => {
          const reqArg = {entityId: 'sploder', attrName: 'jerks', params: {type: 'gerpy'}};
          const {data: attr} = await fiwareApi.findEntityAttrValue(reqArg);
          const expected = 73;
          assert.deepStrictEqual(attr, expected);
        });

        it('representation types: "values" does NOT have any effect', async () => {
          const reqArg = {entityId: 'sploder', attrName: 'jerks', params: {type: 'gerpy', options: 'values'}};
          const {data: attr} = await fiwareApi.findEntityAttrValue(reqArg);
          const expected = 73;
          assert.deepStrictEqual(attr, expected);
        });

        it('find-entity-attr-value CANNOT be used with orion to disambiguate. works for ngsi-server', async () => {
          const reqArg = {entityId: 'sploder', attrName: 'jerks', params: {q: 'bo>49'}};

          // DIVERGE!!
          if (fiwareApi === orionApi) {
            const err = await assertRejects(fiwareApi.findEntityAttrValue(reqArg));
            assertErrorsMatch(err, ORION_ERRORS.FIND_ERROR_TOO_MANY);
          }
          if (fiwareApi === ngsiApi) {
            const {data: attrValue} = await fiwareApi.findEntityAttrValue(reqArg);
            assert.strictEqual(attrValue, 73);
          }

        });

      });

    });

  });

});
