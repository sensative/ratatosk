/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');

const {orionApi, ngsiApi} = require('../../client-api-util');
const mongoApi = require('../../mongo-api');

const {
  assertRejects,
  ngsiResponseErrorUtils: {assertErrorsMatch},
} = require('@ratatosk/ngsi-utils');

const {
  ORION_ERRORS,
} = require('@ratatosk/ngsi-constants');

const entity = {
  id: 'sploder',
  type: 'gerpy',
  agda: 'gerbil',
  bo: 73,
  jerks: {type: 'Norbos', value: 73},
};

describe('update-entity-attr', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      beforeEach(async () => {
        await mongoApi.assertEntitiesEmpty();
        await fiwareApi.createEntity({entity});
      });
      afterEach(() => mongoApi.wipeEntities());

      it('updateEntityAttr: works in simplest case', async () => {
        const reqArg = {entityId: 'sploder', attrName: 'jerks', attr: 909};
        const res = await fiwareApi.updateEntityAttr(reqArg);
        assert.strictEqual(res.status, 204, 'status should be 204');
        const {data: doc} = await fiwareApi.findEntity(_.omit(reqArg, 'attrName'));
        const expected = {
          id: 'sploder',
          type: 'gerpy',
          agda: {type: 'Text', value: 'gerbil', metadata: {}},
          bo: {type: 'Number', value: 73, metadata: {}},
          jerks: {type: 'Number', value: 909, metadata: {}},
        };
        assert.deepStrictEqual(doc, expected);
      });

      it('updateEntityAttr: fails if there are 2 entities with same name (as expected - no attempt at disambuation)', async () => {
        // create a second arg
        const entityX = {
          id: 'sploder',
          type: 'keet',
        };
        await fiwareApi.createEntity({entity: entityX});
        // we should not be able to identify this guy
        const reqArg = {entityId: 'sploder', attrName: 'jerks', attr: 909};
        const err = await assertRejects(fiwareApi.updateEntityAttr(reqArg));
        assertErrorsMatch(err, ORION_ERRORS.FIND_ERROR_TOO_MANY);
      });

      it('what happens if we add some params to this sucker? PARAMS WORK (at least type..)', async () => {
        // create a second arg
        const entityX = {id: 'sploder', type: 'keet'};
        await fiwareApi.createEntity({entity: entityX});
        // update attr
        const reqArg = {
          entityId: 'sploder',
          attrName: 'jerks',
          attr: 907,
          params: {type: 'gerpy'},
        };
        await fiwareApi.updateEntityAttr(reqArg);
        // and check that it worked
        const {data: attr} = await fiwareApi.findEntityAttr(reqArg);
        const expected = {type: 'Number', value: 907, metadata: {}};
        assert.deepStrictEqual(attr, expected);
      });

      it('updateEntityAttr FAILS when disambiguating with q-query, but NOT ngsi-server', async () => {
        // create a second arg
        const entityX = {id: 'sploder', type: 'keet', bo: 37};
        await fiwareApi.createEntity({entity: entityX});
        // update attr
        const reqArg = {
          entityId: 'sploder',
          attrName: 'jerks',
          attr: 933,
          params: {q: 'bo>49'},
        };

        // DIVERGE!!
        if (fiwareApi === orionApi) {
          const err = await assertRejects(fiwareApi.updateEntityAttr(reqArg));
          assertErrorsMatch(err, ORION_ERRORS.FIND_ERROR_TOO_MANY);
        }
        if (fiwareApi === ngsiApi) {
          await fiwareApi.updateEntityAttr(reqArg);
          const {data: attr} = await fiwareApi.findEntityAttr({
            entityId: 'sploder',
            attrName: 'jerks',
            params: {type: 'gerpy'},
          });
          const expected = {
            value: 933,
            type: 'Number',
            metadata: {},
          };
          assert.deepStrictEqual(attr, expected);
        }

      });

    });

  });

});
