/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');

const {orionApi, ngsiApi} = require('../../client-api-util');
const mongoApi = require('../../mongo-api');

const entity = {
  id: 'sploder',
  type: 'gerpy',
  agda: 'gerbil',
  bo: 73,
  jerks: {type: 'Norbos', value: 74},
};

describe('replace-entity-attrs', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      beforeEach(async () => {
        await mongoApi.assertEntitiesEmpty();
        await fiwareApi.createEntity({entity});
      });
      afterEach(() => mongoApi.wipeEntities());

      it('replaceEntityAttrs: works in simplest case (1 completely new attr)', async () => {
        const attrs = {what: 'dat'};
        const reqArg = {entityId: 'sploder', attrs};
        const res = await fiwareApi.replaceEntityAttrs(reqArg);
        assert.strictEqual(res.status, 204, 'status should be 204');
        const {data: doc} = await fiwareApi.findEntity(reqArg);
        const expected = {
          id: 'sploder',
          type: 'gerpy',
          what: {type: 'Text', value: 'dat', metadata: {}},
        };
        assert.deepStrictEqual(doc, expected);
      });

      it('replaceEntityAttrs: same simple case but with "append" option (NO EFFECT)', async () => {
        const attrs = {what: 'dat'};
        const reqArg = {entityId: 'sploder', attrs, params: {append: true}};
        const config = fiwareApi.replaceEntityAttrs.createRequestConfig(reqArg);
        assert.strictEqual(config.params.options, 'append');
        await fiwareApi.replaceEntityAttrs(reqArg);
        const {data: doc} = await fiwareApi.findEntity(reqArg);
        const expected = {
          id: 'sploder',
          type: 'gerpy',
          what: {type: 'Text', value: 'dat', metadata: {}},
        };
        assert.deepStrictEqual(doc, expected);
      });

    });

  });

});
