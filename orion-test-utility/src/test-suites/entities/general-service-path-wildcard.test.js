/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const serialPromise = require('promise-serial-exec');

const {
  assertRejects,
  ngsiResponseErrorUtils: {assertErrorsMatch},
} = require('@ratatosk/ngsi-utils');

const {ORION_ERRORS} = require('@ratatosk/ngsi-constants');

const {
  ngsiApi,
  orionApi,
} = require('../../client-api-util');

const mongoApi = require('../../mongo-api');

const entityTemplates = [
  {entity: {id: 'spiggy0', type: 'Jam'}, servicePath: '/'},
  {entity: {id: 'spiggy1', type: 'Jam'}, servicePath: '/a'},
  {entity: {id: 'spiggy2', type: 'Jam'}, servicePath: '/b'},
  {entity: {id: 'spiggy3', type: 'Jam'}, servicePath: '/c'},
  {entity: {id: 'spiggy4', type: 'Jam'}, servicePath: '/d'},
  {entity: {id: 'spiggy5', type: 'Jam'}, servicePath: '/a/a'},
  {entity: {id: 'spiggy6', type: 'Jam'}, servicePath: '/a/b'},
  {entity: {id: 'spiggy7', type: 'Jam'}, servicePath: '/a/c'},
  {entity: {id: 'spiggy8', type: 'Jam'}, servicePath: '/a/d'},
  {entity: {id: 'spiggy9', type: 'Jam'}, servicePath: '/a/a/a'},
  {entity: {id: 'spiggyA', type: 'Jam'}, servicePath: '/a/a/b'},
  {entity: {id: 'spiggyB', type: 'Jam'}, servicePath: '/a/a/c'},
  {entity: {id: 'spiggyC', type: 'Jam'}, servicePath: '/a/a/d'},
];

describe('general-service-path-wildcard', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      before(async () => {
        await mongoApi.assertEntitiesEmpty();
        await serialPromise(_.map(entityTemplates, (template) => () => {
          const {entity, servicePath} = template;
          return fiwareApi.createEntity({entity, servicePath});
        }));
      });
      after(async () => mongoApi.wipeEntities());

      it('cannot use a wildcard (OR Multiple paths) in createEntity', async () => {
        const entity = {id: 'willNotBeSaved', type: 'irrelevantType'};
        const servicePath = '/will/be/overwritten';
        const config = fiwareApi.createEntity.createRequestConfig({entity, servicePath});

        _.set(config, 'headers.Fiware-ServicePath', '/a/#'); // mutate
        const err = await assertRejects(fiwareApi.rawRequest(config));
        if (fiwareApi === orionApi) {
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_SERVICE_PATH_BAD_CHAR);
        }
        if (fiwareApi === ngsiApi) {
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_SERVICE_PATH_ILLEGAL_CHAR);
        }

        _.set(config, 'headers.Fiware-ServicePath', '/a,/c'); // mutate
        const err2 = await assertRejects(fiwareApi.rawRequest(config));
        assertErrorsMatch(err2, ORION_ERRORS.BAD_REQ_SERVICE_PATH_UPDATE_MULTI_PATH);
      });

      it('getEntities: "/#" finds all entities', async () => {
        const servicePath = '/#';
        const {data: docs} = await fiwareApi.getEntities({servicePath});
        const numTotal = await mongoApi.countEntities();
        assert.strictEqual(docs.length, numTotal);
      });

      it('getEntities: find all entities if servicePath is left out', async () => {
        const {data: docs} = await fiwareApi.getEntities();
        const numTotal = await mongoApi.countEntities();
        assert.strictEqual(docs.length, numTotal);
      });

      it('getEntities: "/d/#,/c/#" finds two (i.e. those with "/d" and "/c" -- kinda strange)', async () => {
        const servicePath = '/d/#,/c/#';
        const {data: docs} = await fiwareApi.getEntities({servicePath});
        assert.strictEqual(docs.length, 2);
      });

      it('getEntities: "/a/#" should find most of them, including the one at "/a"', async () => {
        const servicePath = '/a/#';
        const {data: docs} = await fiwareApi.getEntities({servicePath});
        assert.strictEqual(docs.length, 9);
      });

      it('should be able to handle combination', async () => {
        const servicePath = '/a/#,/d/#,/c';
        const {data: docs} = await fiwareApi.getEntities({servicePath});
        assert.strictEqual(docs.length, 11);
      });

      it('findEntity: "/d/#" should find one', async () => {
        const entityId = 'spiggy4';
        const servicePath = '/d/#';
        const {data: doc} = await fiwareApi.findEntity({entityId, servicePath});
        assert.strictEqual(doc.id, 'spiggy4');
      });

      it('upsertEntityAttrs: what works here?? - some wierdnesses abound', async () => {
        const entityId = 'spiggy4';
        const servicePath = '/will/get/overwritten';
        const attrs = {billy: 'joe'};
        const updateConfig = fiwareApi.upsertEntityAttrs.createRequestConfig({entityId, attrs, servicePath});
        const unchanged = {id: entityId, type: 'Jam'};

        // "/d/#" does not ERROR, but does not work anyways..
        _.set(updateConfig, 'headers.Fiware-ServicePath', '/d/#');
        // DIVERGE!!
        if (fiwareApi === orionApi) {
          await fiwareApi.rawRequest(updateConfig);
          const {data: updatedDoc} = await fiwareApi.findEntity({entityId});
          assert.deepStrictEqual(updatedDoc, unchanged);
        }
        if (fiwareApi === ngsiApi) {
          const err = await assertRejects(fiwareApi.rawRequest(updateConfig));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_SERVICE_PATH_ILLEGAL_CHAR);
        }

        // "/#" does not ERROR, but does not work anyways..
        _.set(updateConfig, 'headers.Fiware-ServicePath', '/#');
        // DIVERGE!!
        if (fiwareApi === orionApi) {
          await fiwareApi.rawRequest(updateConfig);
          const {data: updatedDoc} = await fiwareApi.findEntity({entityId});
          assert.deepStrictEqual(updatedDoc, unchanged);
        }
        if (fiwareApi === ngsiApi) {
          const err = await assertRejects(fiwareApi.rawRequest(updateConfig));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_SERVICE_PATH_ILLEGAL_CHAR);
        }

        // "/d,/c" does not ERROR, but does not work anyways..
        _.set(updateConfig, 'headers.Fiware-ServicePath', '/d,/c');
        // DIVERGE!!
        if (fiwareApi === orionApi) {
          await fiwareApi.rawRequest(updateConfig);
          const {data: updatedDoc} = await fiwareApi.findEntity({entityId});
          assert.deepStrictEqual(updatedDoc, unchanged);
        }
        if (fiwareApi === ngsiApi) {
          const err = await assertRejects(fiwareApi.rawRequest(updateConfig));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_SERVICE_PATH_UPDATE_MULTI_PATH);
        }

        // just as a quick check, "/ddd" FAILS as expected .. ish
        _.set(updateConfig, 'headers.Fiware-ServicePath', '/ddd');
        const err = await assertRejects(fiwareApi.rawRequest(updateConfig));
        assertErrorsMatch(err, ORION_ERRORS.FIND_ERROR_ENTITY_MISSING);

        // "/d" WORKS
        _.set(updateConfig, 'headers.Fiware-ServicePath', '/d');
        await fiwareApi.rawRequest(updateConfig);
        const {data: updatedDoc3} = await fiwareApi.findEntity({entityId});
        const expected = _.assign({}, unchanged, {billy: {type: 'Text', value: 'joe', metadata: {}}});
        assert.deepStrictEqual(updatedDoc3, expected);
      });

      it('updateEntityAttrValue: what works here as well?? - more wierdnesses abound', async () => {
        const entityId = 'spiggy4';
        const servicePath = '/will/get/overwritten';
        const attrName = 'billy';
        const attrValue = 'skull';
        const updateConfig = fiwareApi.updateEntityAttrValue.createRequestConfig({entityId, attrName, attrValue, servicePath});
        const unchanged = {id: entityId, type: 'Jam', billy: {type: 'Text', value: 'joe', metadata: {}}};

        // "/d/#" does not ERROR, but does not work anyways..
        _.set(updateConfig, 'headers.Fiware-ServicePath', '/d/#');
        // DIVERGE!!
        if (fiwareApi === orionApi) {
          await fiwareApi.rawRequest(updateConfig);
          const {data: updatedDoc} = await fiwareApi.findEntity({entityId});
          assert.deepStrictEqual(updatedDoc, unchanged);
        }
        if (fiwareApi === ngsiApi) {
          const err = await assertRejects(fiwareApi.rawRequest(updateConfig));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_SERVICE_PATH_ILLEGAL_CHAR);
        }


        // "/#" does not ERROR, but does not work anyways..
        _.set(updateConfig, 'headers.Fiware-ServicePath', '/#');
        // DIVERGE!!
        if (fiwareApi === orionApi) {
          await fiwareApi.rawRequest(updateConfig);
          const {data: updatedDoc} = await fiwareApi.findEntity({entityId});
          assert.deepStrictEqual(updatedDoc, unchanged);
        }
        if (fiwareApi === ngsiApi) {
          const err = await assertRejects(fiwareApi.rawRequest(updateConfig));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_SERVICE_PATH_ILLEGAL_CHAR);
        }

        // "/d,/c" does not ERROR, but does not work anyways..
        _.set(updateConfig, 'headers.Fiware-ServicePath', '/d,/c');
        // DIVERGE!!
        if (fiwareApi === orionApi) {
          await fiwareApi.rawRequest(updateConfig);
          const {data: updatedDoc} = await fiwareApi.findEntity({entityId});
          assert.deepStrictEqual(updatedDoc, unchanged);
        }
        if (fiwareApi === ngsiApi) {
          const err = await assertRejects(fiwareApi.rawRequest(updateConfig));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_SERVICE_PATH_UPDATE_MULTI_PATH);
        }

        // just as a quick check, "/ddd" FAILS as expected .. ish
        _.set(updateConfig, 'headers.Fiware-ServicePath', '/ddd');
        const err = await assertRejects(fiwareApi.rawRequest(updateConfig));
        assertErrorsMatch(err, ORION_ERRORS.FIND_ERROR_ENTITY_MISSING);

        // "/d" WORKS
        _.set(updateConfig, 'headers.Fiware-ServicePath', '/d');
        await fiwareApi.rawRequest(updateConfig);
        const {data: updatedDoc3} = await fiwareApi.findEntity({entityId});
        const expected = _.assign({}, unchanged, {billy: {type: 'Text', value: 'skull', metadata: {}}});
        assert.deepStrictEqual(updatedDoc3, expected);
      });

      it('deleteEntity: what works here as well?? - more wierdnesses abound', async () => {
        const entityId = 'spiggy4';
        const servicePath = '/will/get/overwritten';
        const updateConfig = fiwareApi.deleteEntity.createRequestConfig({entityId, servicePath});
        const unchanged = {id: entityId, type: 'Jam', billy: {type: 'Text', value: 'skull', metadata: {}}};

        // "/d/#" does not ERROR, but does not work anyways..
        _.set(updateConfig, 'headers.Fiware-ServicePath', '/d/#');
        // DIVERGE!!
        if (fiwareApi === orionApi) {
          await fiwareApi.rawRequest(updateConfig);
          const {data: updatedDoc} = await fiwareApi.findEntity({entityId});
          assert.deepStrictEqual(updatedDoc, unchanged);
        }
        if (fiwareApi === ngsiApi) {
          const err = await assertRejects(fiwareApi.rawRequest(updateConfig));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_SERVICE_PATH_ILLEGAL_CHAR);
        }

        // "/#" does not ERROR, but does not work anyways..
        _.set(updateConfig, 'headers.Fiware-ServicePath', '/#');
        // DIVERGE!!
        if (fiwareApi === orionApi) {
          await fiwareApi.rawRequest(updateConfig);
          const {data: updatedDoc} = await fiwareApi.findEntity({entityId});
          assert.deepStrictEqual(updatedDoc, unchanged);
        }
        if (fiwareApi === ngsiApi) {
          const err = await assertRejects(fiwareApi.rawRequest(updateConfig));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_SERVICE_PATH_ILLEGAL_CHAR);
        }

        // "/d,/c" does not ERROR, but does not work anyways..
        _.set(updateConfig, 'headers.Fiware-ServicePath', '/d,/c');
        // DIVERGE!!
        if (fiwareApi === orionApi) {
          await fiwareApi.rawRequest(updateConfig);
          const {data: updatedDoc} = await fiwareApi.findEntity({entityId});
          assert.deepStrictEqual(updatedDoc, unchanged);
        }
        if (fiwareApi === ngsiApi) {
          const err = await assertRejects(fiwareApi.rawRequest(updateConfig));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_SERVICE_PATH_UPDATE_MULTI_PATH);
        }

        // "/d" WORKS (i.e. find fails)
        _.set(updateConfig, 'headers.Fiware-ServicePath', '/d');
        await fiwareApi.rawRequest(updateConfig);
        // and now should be gone
        const err = await assertRejects(fiwareApi.findEntity({entityId}));
        assertErrorsMatch(err, ORION_ERRORS.FIND_ERROR_ENTITY_MISSING);
      });

    });

  });

});
