/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

// Test all the different versions of patch value

const _ = require('lodash');
const assert = require('assert');

const {orionApi, ngsiApi} = require('../../../client-api-util');
const mongoApi = require('../../../mongo-api');

const keyValuesEntity = {
  id: 'StarPlatinum',
  type: 'stand',
  quote: 'ORA ORA ORA ORA!',
};

const regularEntity = {
  id: 'StarPlatinum',
  type: 'stand',
  quote: {
    value: 'ORA ORA ORA ORA!',
    metadata: {},
    type: 'Text',
  },
  number: {
    value: 1,
    metadata: {},
    type: 'Number',
  },
};

describe('check-creation-with-options', () => {
  const fiwareApis = {
    orionApi,
    ngsiApi,
  };

  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      before(async () => mongoApi.assertEntitiesEmpty());
      after(async () => mongoApi.wipeEntities());
      afterEach(async () => mongoApi.wipeEntities());

      it('should be possible to create an entity with keyValues option', async () => {
        const postRes = await fiwareApi.rawRequest({
          url: '/entities/?options=keyValues',
          method: 'post',
          data: keyValuesEntity,
        });
        assert.deepStrictEqual(postRes.status, 201);

        const getRes = await fiwareApi.rawRequest({
          url: '/entities/StarPlatinum?options=keyValues',
          method: 'get',
        });
        assert.deepStrictEqual(getRes.status, 200);
        assert.deepStrictEqual(getRes.data, keyValuesEntity);
      });

      it('should NOT be possible to create an entity in keyValues format without keyValue option', async () => {
        let errStatus;
        let errData;
        try {
          await fiwareApi.rawRequest({
            url: '/entities/',
            method: 'post',
            data: keyValuesEntity,
          });
        } catch (err) {
          errStatus = err.response.status;
          errData = err.response.data;
        }
        assert.deepStrictEqual(errStatus, 400);
        assert.deepStrictEqual(errData, {
          error: 'BadRequest',
          description: 'attribute must be a JSON object, unless keyValues option is used',
        });
      });

      it('should be possible to post a normal entity as keyValue and interpret it correctly (for KV)', async () => {
        const postRes = await fiwareApi.rawRequest({
          url: '/entities/?options=keyValues',
          method: 'post',
          data: regularEntity,
        });
        assert.deepStrictEqual(postRes.status, 201);

        const getRes = await fiwareApi.rawRequest({
          url: '/entities/StarPlatinum?options=keyValues',
          method: 'get',
        });
        assert.deepStrictEqual(getRes.status, 200);
        assert.deepStrictEqual(getRes.data, regularEntity);
      });

      it('should be possible to create and update with upsert option', async () => {
        const postRes = await fiwareApi.rawRequest({
          url: '/entities/?options=upsert',
          method: 'post',
          data: regularEntity,
        });
        assert.deepStrictEqual(postRes.status, 204);

        const modifiedEntity = _.cloneDeep(regularEntity);
        modifiedEntity.number.value = 2;
        delete modifiedEntity.quote;

        const postModifiedRes = await fiwareApi.rawRequest({
          url: '/entities/?options=upsert',
          method: 'post',
          data: modifiedEntity,
        });
        assert.deepStrictEqual(postModifiedRes.status, 204);

        const getRes = await fiwareApi.rawRequest({
          url: '/entities/StarPlatinum',
          method: 'get',
        });
        assert.deepStrictEqual(getRes.status, 200);
        assert.deepStrictEqual(getRes.data, {...regularEntity, ...modifiedEntity});
      });

      it('should be possible to create and update with upsert and keyValue', async () => {
        const postRes = await fiwareApi.rawRequest({
          url: '/entities/?options=upsert,keyValues',
          method: 'post',
          data: keyValuesEntity,
        });
        assert.deepStrictEqual(postRes.status, 204);

        const modifiedKVEntity = _.cloneDeep(keyValuesEntity);
        modifiedKVEntity.number = 2;
        delete modifiedKVEntity.quote;

        const postModifiedRes = await fiwareApi.rawRequest({
          url: '/entities/?options=keyValues,upsert',
          method: 'post',
          data: modifiedKVEntity,
        });
        assert.deepStrictEqual(postModifiedRes.status, 204);

        const getRes = await fiwareApi.rawRequest({
          url: '/entities/StarPlatinum?options=keyValues',
          method: 'get',
        });
        assert.deepStrictEqual(getRes.status, 200);
        assert.deepStrictEqual(getRes.data, {...keyValuesEntity, ...modifiedKVEntity});
      });
    });
  });
});
