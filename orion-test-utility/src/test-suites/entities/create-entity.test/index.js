/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

// Test all the different versions of patch value

const mongoApi = require('../../../mongo-api');

describe('create-entity', () => {

  before(() => mongoApi.assertEntitiesEmpty());
  after(() => mongoApi.assertEntitiesEmpty());

  require('./check-basic-create.test');
  require('./check-entity-uniqueness.test');
  require('./check-service-path-limits.test');
  require('./check-creation-with-options.test');
  require('./bug-hunts.test');

});
