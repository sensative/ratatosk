/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');

// const {assertRejects} = require('@ratatosk/ngsi-utils');
// const {ORION_ERRORS} = require('@ratatosk/ngsi-constants');

const {orionApi, ngsiApi} = require('../../../client-api-util');
const mongoApi = require('../../../mongo-api');

const failingReq = {
  method: 'POST',
  uri: 'http://orion:1026/v2/entities',
  json: true,
  headers: {
    host: 'api-authz.yggio-rise.sensative.net',
    connection: 'close',
    'x-real-ip': '94.254.94.78',
    'x-forwarded-for': '94.254.94.78',
    'x-forwarded-proto': 'https',
    'x-forwarded-ssl': 'on',
    'x-forwarded-port': '443',
    'content-length': '148',
    'user-agent': 'curl/7.47.0',
    accept: '*/*',
    'content-type': 'application/json',
    authorization: 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJYSEpqNlRVWXo4OHBBeXpQZDYyX2RVTnJ2NU9JU2lyb2hGb2pOTXFURDhRIn0.eyJqdGkiOiJhNzExZDFkYS05MDMxLTQ5YmMtYWE0MS1mNWFlMmIwMzQyNDciLCJleHAiOjE1NzI2MTIyMTgsIm5iZiI6MCwiaWF0IjoxNTcyNjA4NjE4LCJpc3MiOiJodHRwczovL2tleWNsb2FrLnlnZ2lvLXJpc2Uuc2Vuc2F0aXZlLm5ldC9hdXRoL3JlYWxtcy95Z2dpbyIsImF1ZCI6WyJ5Z2dpby1jb3JlLXNlcnZpY2VzIiwiYnJva2VyIiwiYWNjb3VudCJdLCJzdWIiOiI3OWFlOWE1YS0wMzA0LTQxZWMtYjcyMS1kNTdhMDlkNDE5Y2IiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJ5Z2dpby1zZXJ2aWNlcyIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6ImEyZDk1MGZjLWZlZjctNDg5Ny1iNTE5LWZlNjYxNTU5ZmU3ZSIsImFjciI6IjEiLCJhbGxvd2VkLW9yaWdpbnMiOlsiKiJdLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsib2ZmbGluZV9hY2Nlc3MiLCJ1bWFfYXV0aG9yaXphdGlvbiJdfSwicmVzb3VyY2VfYWNjZXNzIjp7InlnZ2lvLWNvcmUtc2VydmljZXMiOnsicm9sZXMiOlsidW1hX3Byb3RlY3Rpb24iXX0sImJyb2tlciI6eyJyb2xlcyI6WyJyZWFkLXRva2VuIl19LCJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6InByb2ZpbGUgZW1haWwiLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwicHJlZmVycmVkX3VzZXJuYW1lIjoiYWxpY2UifQ.fzjgADPQJQPgCFj1_gWprsdqty-_hiHzo_5Q5BcIw0brVO_1JFIT7b3KoM6AjCg5WpkTpL03EvXJITacTIcnkulywyZLQDaqCNJSWjtMxUlmHIfHxko4ZI13dUbKXwdHH-elU_0DsHjg3rnmtdPvc-Sxz-azC_7G9hvtvkrrdKrgLOtwylSd6DfY2Z6CySv_Oe2YGJkthQJSEy8LKEVjtvX3N0870-FGreSB_rf4aCT8oIyW2D35YdYGScCVuQvvqmoQU4JLoFeP4MQL0rn2iaktvrXgWph3ZikkY6AMZRkWqOXVPxCUzEVtY3Z1qj6qGk8hP-65FoODgUAaLI0k6Q',
  },
  resolveWithFullResponse: true,
  body: {
    id: 'Room3',
    type: 'Room',
    temperature: {value: 24, type: 'Float'},
    pressure: {value: 720, type: 'Integer'},
  },
};

describe('check-basic-create', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      before(async () => mongoApi.assertEntitiesEmpty());
      afterEach(async () => mongoApi.wipeEntities());

      it('just the entity', async () => {
        await fiwareApi.createEntity({entity: failingReq.body});
        const {data: docs} = await fiwareApi.getEntities();
        assert.strictEqual(docs.length, 1);
      });

      it('just the entity & resolveWithFullResponse', async () => {
        const config = fiwareApi.createEntity.createRequestConfig({entity: failingReq.body});
        config.resolveWithFullResponse = true; // mutate!!
        await fiwareApi.rawRequest(config);
        const {data: docs} = await fiwareApi.getEntities();
        assert.strictEqual(docs.length, 1);
      });

      it('try all the headers', async () => {
        const config = fiwareApi.createEntity.createRequestConfig({entity: failingReq.body});
        config.headers = failingReq.headers; // mutate!!
        await fiwareApi.rawRequest(config);
        const {data: docs} = await fiwareApi.getEntities();
        assert.strictEqual(docs.length, 1);
      });

      it('add all of it', async () => {
        const config = fiwareApi.createEntity.createRequestConfig({entity: failingReq.body});
        config.headers = failingReq.headers; // mutate!!
        config.resolveWithFullResponse = true; // mutate!!
        config.json = true; // mutate!!
        await fiwareApi.rawRequest(config);
        const {data: docs} = await fiwareApi.getEntities();
        assert.strictEqual(docs.length, 1);
      });

    });

  });

});
