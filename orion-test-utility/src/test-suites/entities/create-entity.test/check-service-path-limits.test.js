/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

// Test all the different versions of patch value

const _ = require('lodash');
const assert = require('assert');
const serialPromise = require('promise-serial-exec');
const {assertThrows} = require('@ratatosk/ngsi-utils');

const {
  assertRejects,
  ngsiResponseErrorUtils: {assertErrorsMatch},
  ngsiHeaderUtils: {createServicePathHeader},
  ngsiServicePathUtils: {validateServicePath},
} = require('@ratatosk/ngsi-utils');

const {ORION_ERRORS} = require('@ratatosk/ngsi-constants');

const {orionApi, ngsiApi} = require('../../../client-api-util');
const mongoApi = require('../../../mongo-api');

// a dirty little utility function to check what servicePath is stored
// in the (ONLY) stored entity in mongodb. assumes fastidious order is held elsewhere
const getCurrentServicePath = async (fiwareApi) => {
  const idPath = fiwareApi.isOrionApi ? '_id' : 'orionId';
  const docs = await mongoApi.findEntities();
  assert.strictEqual(docs.length, 1, 'there is more than one doc');
  const path = docs[0][idPath].servicePath;
  return path;
};

describe('check-service-path-limits', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {
    fiwareApi.isOrionApi = fiwareApi === orionApi;

    describe(`USING ${apiName}`, () => {

      beforeEach(async () => mongoApi.assertEntitiesEmpty());
      afterEach(async () => mongoApi.wipeEntities());

      it('servicePath: explicit "/" should work', async () => {
        const entity = {id: 'dolt', type: 'Doltish'};
        const headers = createServicePathHeader('/');
        validateServicePath('/');
        const res = await fiwareApi.createEntity({entity, headers});
        assert.strictEqual(res.status, 201, 'should have succeeded');
        const path = await getCurrentServicePath(fiwareApi);
        assert.strictEqual(path, '/', 'incorrect service path');
      });

      it('servicePath: explicit "" (empty string) should work', async () => {
        const entity = {id: 'dolt', type: 'Doltish'};
        const headers = createServicePathHeader('');
        const assertMess = 'Only explicit valid servicePaths allowed in strict mode';
        assertThrows(() => validateServicePath('', {isStrict: true}), assertMess);
        assert.doesNotThrow(() => validateServicePath('', {isStrict: false}));
        const res = await fiwareApi.createEntity({entity, headers});
        assert.strictEqual(res.status, 201, 'should have succeeded');
        const path = await getCurrentServicePath(fiwareApi);
        assert.strictEqual(path, '/', 'incorrect service path');
      });

      it('servicePath: false (the boolean) should NOT work', async () => {
        const entity = {id: 'dolt', type: 'Doltish'};
        const headers = {'Fiware-ServicePath': false};
        assert.throws(() => validateServicePath(false));
        const err = await assertRejects(fiwareApi.createEntity({entity, headers}));
        assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_SERVICE_PATH_NOT_ABSOLUTE);
      });

      it('servicePath: a string that is not an absolute path should NOT work', async () => {
        const entity = {id: 'dolt', type: 'Doltish'};
        const reqArg = {entity, headers: createServicePathHeader('/placeholder')};
        const config = fiwareApi.createEntity.createRequestConfig(reqArg);
        const invalidServicePath = 'invalidservicepath';
        _.set(config, 'headers.Fiware-ServicePath', invalidServicePath); // mutate!!
        assert.throws(() => validateServicePath(invalidServicePath));
        const err = await assertRejects(fiwareApi.rawRequest(config));
        assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_SERVICE_PATH_NOT_ABSOLUTE);
      });

      it('servicePath needs to be an absolute path (some examples that break the same "RULE")', async () => {
        const entity = {id: 'dolt', type: 'Doltish'};
        const reqArg = {entity, headers: createServicePathHeader('/placeholder')};
        const config = fiwareApi.createEntity.createRequestConfig(reqArg);
        const nonAbsoluteServicePaths = [
          false,
          'notanabsolutepath',
          'asdf asdf',
          true,
          {},
          [],
          'asdf/asdf/asfd',
        ];
        await serialPromise(_.map(nonAbsoluteServicePaths, (invalidServicePath) => async () => {
          _.set(config, 'headers.Fiware-ServicePath', invalidServicePath); // mutate!!
          assert.throws(() => validateServicePath(invalidServicePath));
          const err = await assertRejects(fiwareApi.rawRequest(config));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_SERVICE_PATH_NOT_ABSOLUTE);
        }));

      });

      it('servicePath: explicit "/asdf" should work', async () => {
        const entity = {id: 'dolt', type: 'Doltish'};
        const headers = createServicePathHeader('/asdf');
        validateServicePath('/asdf');
        const res = await fiwareApi.createEntity({entity, headers});
        assert.strictEqual(res.status, 201, 'should have succeeded');
        const path = await getCurrentServicePath(fiwareApi);
        assert.strictEqual(path, '/asdf', 'incorrect service path');
      });

      it('servicePath: explicit "/abcABC_123" should work', async () => {
        const entity = {id: 'dolt', type: 'Doltish'};
        const headers = createServicePathHeader('/abcABC_123');
        validateServicePath('/abcABC_123');
        const res = await fiwareApi.createEntity({entity, headers});
        assert.strictEqual(res.status, 201, 'should have succeeded');
        const path = await getCurrentServicePath(fiwareApi);
        assert.strictEqual(path, '/abcABC_123', 'incorrect service path');
      });

      it('servicePath: explicit "/aa*aa" should NOT work', async () => {
        const path = '/aa*aa'; // illegal char "*"
        const entity = {id: 'dolt', type: 'Doltish'};
        assert.throws(() => validateServicePath(path));
        const reqArg = {entity, headers: createServicePathHeader('/placeholder')};
        const config = fiwareApi.createEntity.createRequestConfig(reqArg);
        _.set(config, 'headers.Fiware-ServicePath', path); // mutate!!
        const err = await assertRejects(fiwareApi.rawRequest(config));
        assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_SERVICE_PATH_ILLEGAL_CHAR);
      });

      it('servicePath: explicit "/asdf/derp/stupor" should work', async () => {
        const entity = {id: 'dolt', type: 'Doltish'};
        const headers = createServicePathHeader('/asdf/derp/stupor');
        validateServicePath('/asdf/derp/stupor');
        const res = await fiwareApi.createEntity({entity, headers});
        assert.strictEqual(res.status, 201, 'should have succeeded');
        const path = await getCurrentServicePath(fiwareApi);
        assert.strictEqual(path, '/asdf/derp/stupor', 'incorrect service path');
      });

      it('servicePath: explicit "////" should work for orionApi but NOT ngsiApi', async () => {
        const entity = {id: 'dolt', type: 'Doltish'};
        const headers = createServicePathHeader('/placeholder');
        const config = fiwareApi.createEntity.createRequestConfig({entity, headers});
        const invalidServicePath = '////';
        assert.throws(() => validateServicePath(invalidServicePath));
        _.set(config, 'headers.Fiware-ServicePath', invalidServicePath); // MUTATE!!
        if (fiwareApi === orionApi) {
          const res = await fiwareApi.rawRequest(config);
          assert.strictEqual(res.status, 201, 'should have succeeded');
          const path = await getCurrentServicePath(fiwareApi);
          assert.strictEqual(path, '/', 'incorrect service path');
        }
        if (fiwareApi === ngsiApi) {
          const err = await assertRejects(fiwareApi.rawRequest(config));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_SERVICE_PATH_EMPTY_COMPONENT);
        }
      });

      it('servicePath: explicit "/aaa////////" should work for orionApi but NOT ngsiApi', async () => {
        const entity = {id: 'dolt', type: 'Doltish'};
        const headers = createServicePathHeader('/placeholder');
        const config = fiwareApi.createEntity.createRequestConfig({entity, headers});
        const invalidServicePath = '/aaa////////';
        assert.throws(() => validateServicePath(invalidServicePath));
        _.set(config, 'headers.Fiware-ServicePath', invalidServicePath); // MUTATE!!
        if (fiwareApi === orionApi) {
          const res = await fiwareApi.rawRequest(config);
          assert.strictEqual(res.status, 201, 'should have succeeded');
          const path = await getCurrentServicePath(fiwareApi);
          assert.strictEqual(path, '/aaa', 'incorrect service path');
        }
        if (fiwareApi === ngsiApi) {
          const err = await assertRejects(fiwareApi.rawRequest(config));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_SERVICE_PATH_EMPTY_COMPONENT);
        }
      });

      it('servicePath: explicit "/a/b/c/d/e" should work', async () => {
        const entity = {id: 'dolt', type: 'Doltish'};
        const headers = createServicePathHeader('/a/b/c/d/e');
        validateServicePath('/a/b/c/d/e');
        const res = await fiwareApi.createEntity({entity, headers});
        assert.strictEqual(res.status, 201, 'should have succeeded');
        const path = await getCurrentServicePath(fiwareApi);
        assert.strictEqual(path, '/a/b/c/d/e', 'incorrect service path');
      });

      it('servicePath: explicit "/aa//aa" should NOT work', async () => {
        const path = '/aa//aa';
        const entity = {id: 'dolt', type: 'Doltish'};
        assert.throws(() => validateServicePath(path));
        const reqArg = {entity, headers: createServicePathHeader('/placeholder')};
        const config = fiwareApi.createEntity.createRequestConfig(reqArg);
        _.set(config, 'headers.Fiware-ServicePath', path); // mutate!!
        const err = await assertRejects(fiwareApi.rawRequest(config));
        assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_SERVICE_PATH_EMPTY_COMPONENT);
      });

      it('servicePath: create the longest one possible (10 levels, 50chars/level)', async () => {
        const entity = {id: 'dolt', type: 'Doltish'};
        const chars = 'abcdefghij'.split('');
        const levels = _.map(chars, (char) => {
          return _.times(50, () => char).join('');
        });
        const path = `/${levels.join('/')}`;
        assert.strictEqual(path.length, 510, 'path should be 510 chars long');
        const headers = createServicePathHeader(path);
        validateServicePath(path);
        const res = await fiwareApi.createEntity({entity, headers});
        assert.strictEqual(res.status, 201, 'should have succeeded');
        const savedPath = await getCurrentServicePath(fiwareApi);
        assert.strictEqual(savedPath, path, 'incorrect service path');
      });

      it('servicePath: servicePath: create the longest one possible (10 levels, 50chars/level) - then add single char "z"', async () => {
        const entity = {id: 'dolt', type: 'Doltish'};
        const chars = 'abcdefghij'.split('');
        const levels = _.map(chars, (char) => {
          return _.times(50, () => char).join('');
        });
        const path = `/${levels.join('/')}`;
        const modifiedPath = `${path.slice(0, 333)}z${path.slice(333)}`;
        assert.strictEqual(modifiedPath.length, 511, 'path should be 511 chars long');
        const reqArg = {entity, headers: createServicePathHeader('/placeholder')};
        const config = fiwareApi.createEntity.createRequestConfig(reqArg);
        _.set(config, 'headers.Fiware-ServicePath', modifiedPath); // mutate!!
        const err = await assertRejects(fiwareApi.rawRequest(config));
        assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_SERVICE_PATH_COMPONENT_NAME_TOO_LONG);
      });

      it('servicePath: explicit path with 11 levels, 50 chars at each level should NOT work', async () => {
        const entity = {id: 'dolt', type: 'Doltish'};
        const chars = 'abcdefghijk'.split(''); // <----- 'k' was added
        const levels = _.map(chars, (char) => {
          return _.times(50, () => char).join('');
        });
        const path = `/${levels.join('/')}`;
        assert.strictEqual(path.length, 561, 'path should be 561 chars long');
        const reqArg = {entity, headers: createServicePathHeader('/placeholder')};
        const config = fiwareApi.createEntity.createRequestConfig(reqArg);
        _.set(config, 'headers.Fiware-ServicePath', path); // mutate!!
        const err = await assertRejects(fiwareApi.rawRequest(config));
        assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_SERVICE_PATH_TOO_MANY_COMPONENTS);
      });

      it('servicePath: two disjoint (simple) servicePaths should not work', async () => {
        const entity = {id: 'dolt', type: 'Doltish'};
        const headers = createServicePathHeader('/placeholder');
        const config = fiwareApi.createEntity.createRequestConfig({entity, headers});
        const path = '/a,/h';
        _.set(config, 'headers.Fiware-ServicePath', path); // mutate!!
        const err = await assertRejects(fiwareApi.rawRequest(config));
        assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_SERVICE_PATH_UPDATE_MULTI_PATH);
      });

      it('servicePath: sending "," ought not crash the server (but DOES for orionApi!!)', async () => {
        const entity = {id: 'dolt', type: 'Doltish'};
        const headers = createServicePathHeader('/placeholder');
        const config = fiwareApi.createEntity.createRequestConfig({entity, headers});
        const invalidServicePath = ',';
        assert.throws(() => validateServicePath(invalidServicePath));
        _.set(config, 'headers.Fiware-ServicePath', invalidServicePath); // MUTATE!!


        if (fiwareApi === ngsiApi) {
          const err = await assertRejects(fiwareApi.rawRequest(config));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_SERVICE_PATH_UPDATE_MULTI_PATH);
        }
        if (fiwareApi === orionApi) {
          // DO NOT RUN (i.e. uncomment) THIS GUY!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
          // const err = await assertRejects(fiwareApi.rawRequest(config));
          // assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_SERVICE_PATH_UPDATE_MULTI_PATH);
        }
      });

    });

  });

});
