/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

// Test all the different versions of patch value

const _ = require('lodash');
const assert = require('assert');

const {
  assertRejects,
  ngsiResponseErrorUtils: {assertErrorsMatch},
  ngsiHeaderUtils: {textContentHeader, createServicePathHeader},
} = require('@ratatosk/ngsi-utils');

const {ORION_ERRORS} = require('@ratatosk/ngsi-constants');

const {orionApi, ngsiApi} = require('../../../client-api-util');
const mongoApi = require('../../../mongo-api');

const removeMongoTrash = (mongoDoc) => _.omit(mongoDoc, [
  'creDate',
  'modDate',
  'lastCorrelator',
  'attrNames',
]);

describe('check-basic-create', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      before(async () => mongoApi.assertEntitiesEmpty());
      after(async () => mongoApi.wipeEntities());
      afterEach(async () => mongoApi.wipeEntities());

      it('should be possible to create a single, simple entity', async () => {
        const entity = {id: 'supanu', type: 'Kerchew'};
        const res = await fiwareApi.createEntity({entity});
        const mongoDocs = await mongoApi.findEntities();
        assert.strictEqual(mongoDocs.length, 1, 'there should be 1 entity');
        const mongoDoc = removeMongoTrash(mongoDocs[0]);

        // DIVERGE!!
        let expected;
        if (fiwareApi === orionApi) {
          expected = {
            _id: {
              id: 'supanu',
              type: 'Kerchew',
              servicePath: '/',
            },
            attrs: {},
          };
        }
        if (fiwareApi === ngsiApi) {
          assert.strictEqual(mongoDoc._id.constructor.name, 'ObjectID');
          expected = {
            _id: mongoDoc._id,
            orionId: {
              id: 'supanu',
              type: 'Kerchew',
              servicePath: '/',
            },
            attrs: {},
          };
        }

        assert.deepStrictEqual(mongoDoc, expected, 'mongodoc looks incorrect');
        //
        // Also check the response headers
        const responseHeaders = res.headers;
        assert.strictEqual(responseHeaders['content-length'], '0', 'createEntity should not return any conent');
        assert.strictEqual(responseHeaders.location, '/v2/entities/supanu?type=Kerchew', 'wrong res header.location');

        // DEVIATION!!
        if (fiwareApi === orionApi) {
          assert(_.has(responseHeaders, 'fiware-correlator'), 'response header should have a fiware-correlator key with a buncha stuff in it..');
        }

        // And check the input headers
        const requestHeaders = res.config.headers;
        assert.strictEqual(requestHeaders['Content-Type'], 'application/json;charset=utf-8', 'wrong content-type in req header');
        assert.strictEqual(requestHeaders['Content-Length'], 32, 'Wrong content length');
      });

      it('If json content headers are not included w/ createEntity, then axios autofills that header regardless. I am going to leave this be for now', async () => {
        const entity = {id: 'supanu', type: 'Kerchew'};
        const headers = undefined; // <--- empty, but gets autofilled by axios in background...
        const res = await fiwareApi.createEntity({entity, headers});
        const mongoDocs = await mongoApi.findEntities();
        assert.strictEqual(mongoDocs.length, 1, 'there should be 1 entity');
        const mongoDoc = removeMongoTrash(mongoDocs[0]);

        // DIVERGE!!
        let expected;
        if (fiwareApi === orionApi) {
          expected = {
            _id: {
              id: 'supanu',
              type: 'Kerchew',
              servicePath: '/',
            },
            attrs: {},
          };
        }
        if (fiwareApi === ngsiApi) {
          assert.strictEqual(mongoDoc._id.constructor.name, 'ObjectID');
          expected = {
            _id: mongoDoc._id,
            orionId: {
              id: 'supanu',
              type: 'Kerchew',
              servicePath: '/',
            },
            attrs: {},
          };
        }
        assert.deepStrictEqual(mongoDoc, expected, 'mongodoc looks incorrect');
        //
        // Also check the response headers
        const responseHeaders = res.headers;
        assert.strictEqual(responseHeaders['content-length'], '0', 'createEntity should not return any conent');
        assert.strictEqual(responseHeaders.location, '/v2/entities/supanu?type=Kerchew', 'wrong res header.location');

        // DEVIATION!!
        if (fiwareApi === orionApi) {
          assert(_.has(responseHeaders, 'fiware-correlator'), 'response header should have a fiware-correlator key with a buncha stuff in it..');
        }

        // And check the input headers
        const requestHeaders = res.config.headers;
        // NOTE THIS ONE: the "application/json;charset=utf-8" gets autofilled by axios:
        assert.strictEqual(requestHeaders['Content-Type'], 'application/json;charset=utf-8', 'wrong content-type in req header');
        assert.strictEqual(requestHeaders['Content-Length'], 32, 'Wrong content length');
      });

      it('createEntity FAILS when textContentHeader header is used', async () => {
        const entity = {id: 'supanu', type: 'Kerchew'};
        const headers = textContentHeader();
        const invalidConfig = _.assign(
          fiwareApi.createEntity.createRequestConfig({entity}),
          {headers},
        );
        const err = await assertRejects(fiwareApi.rawRequest(invalidConfig));
        assertErrorsMatch(err, ORION_ERRORS.UNSUPPORTED_MEDIA_TYPE_TEXT);
      });

      it('createEntity fails when no entity is included', async () => {
        const validConfig = fiwareApi.createEntity.createRequestConfig({entity: {id: 'willbeRemoved'}});
        const invalidConfig = _.omit(validConfig, 'data');
        const err = await assertRejects(fiwareApi.rawRequest(invalidConfig));
        assertErrorsMatch(err, ORION_ERRORS.CONTENT_LENGTH_REQUIRED);
      });

      it('createEntity just ignores any extra/irrelevant/invalid headers that might be included', async () => {
        const entity = {id: 'supanu', type: 'Kerchew'};
        const headers = {'Fiware-InvalidHeaderThatShouldBeIgnored': 'derp'};
        const res = await fiwareApi.createEntity({entity, headers});
        assert.strictEqual(res.status, 201, 'this should have succeeded');
        assert.strictEqual(res.config.headers['Fiware-InvalidHeaderThatShouldBeIgnored'], 'derp', 'The header should be included in the request');
      });

      it('createEntity needs "id"', async () => {
        const validConfig = fiwareApi.createEntity.createRequestConfig({entity: {id: 'idwillbeRemoved'}});
        const invalidConfig = {
          ...validConfig,
          data: _.omit(validConfig.data, 'id'),
        };
        const err = await assertRejects(fiwareApi.rawRequest(invalidConfig));
        assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_TOO_SHORT_ENTITY_ID);
      });

      it('createEntity CAN omit entity type (default type "Thing" is used)', async () => {
        const entity = {id: 'supanu', type: 'Kerchew'};
        const validConfig = fiwareApi.createEntity.createRequestConfig({entity});
        const stillValidConfig = {
          ...validConfig,
          data: _.omit(validConfig.data, 'type'),
        };

        await fiwareApi.rawRequest(stillValidConfig);
        const mongoDocs = await mongoApi.findEntities();
        assert.strictEqual(mongoDocs.length, 1, 'there should be 1 entity');
        const mongoDoc = removeMongoTrash(mongoDocs[0]);

        // DIVERGE!!
        let expected;
        if (fiwareApi === orionApi) {
          expected = {
            _id: {
              id: 'supanu',
              type: 'Thing', //  <--------------- Fiware specification default
              servicePath: '/',
            },
            attrs: {},
          };
        }
        if (fiwareApi === ngsiApi) {
          assert.strictEqual(mongoDoc._id.constructor.name, 'ObjectID');
          expected = {
            _id: mongoDoc._id,
            orionId: {
              id: 'supanu',
              type: 'Thing', //  <--------------- Fiware specification default
              servicePath: '/',
            },
            attrs: {},
          };
        }
        assert.deepStrictEqual(mongoDoc, expected, 'mongodoc looks incorrect');
      });

      it('createEntity CAN omit type of attributes (typing is inferred)', async () => {
        const entity = {
          id: 'Thingamabob',
          type: 'Foo',
          mylist: {
            value: ['SNA', 'FU'],
          },
          mynum: {
            value: 1337,
          },
          mynothing: {
            value: null,
          },
          mybool: {
            value: true,
          },
          myname: {
            value: 'manbat',
          },
          myobj: {
            value: {
              herp: 'derp',
            },
          },
        };
        const res = await fiwareApi.createEntity({entity});

        const mongoDocs = await mongoApi.findEntities();
        assert.strictEqual(mongoDocs.length, 1, 'there should be 1 entity');
        const mongoDoc = removeMongoTrash(mongoDocs[0]);

        assert.strictEqual(mongoDoc.attrs.mylist.type, 'StructuredValue');
        assert.strictEqual(mongoDoc.attrs.mynum.type, 'Number');
        assert.strictEqual(mongoDoc.attrs.mynothing.type, 'None');
        assert.strictEqual(mongoDoc.attrs.mybool.type, 'Boolean');
        assert.strictEqual(mongoDoc.attrs.myname.type, 'Text');
        assert.strictEqual(mongoDoc.attrs.myobj.type, 'StructuredValue');
      });

      it('should be possible to create a simple entity with an explicit service path', async () => {
        const entity = {id: 'spoonypoo', type: 'Kerchew'};
        const headers = createServicePathHeader('/supany/stule');
        await fiwareApi.createEntity({entity, headers});
        const mongoDocs = await mongoApi.findEntities();
        assert.strictEqual(mongoDocs.length, 1, 'there should be 1 entity');
        const mongoDoc = removeMongoTrash(mongoDocs[0]);

        // DIVERGE!!
        let expected;
        if (fiwareApi === orionApi) {
          expected = {
            _id: {
              id: 'spoonypoo',
              type: 'Kerchew',
              servicePath: '/supany/stule', //  <-------------
            },
            attrs: {},
          };
        }
        if (fiwareApi === ngsiApi) {
          assert.strictEqual(mongoDoc._id.constructor.name, 'ObjectID');
          expected = {
            _id: mongoDoc._id,
            orionId: {
              id: 'spoonypoo',
              type: 'Kerchew',
              servicePath: '/supany/stule', //  <-------------
            },
            attrs: {},
          };
        }
        assert.deepStrictEqual(mongoDoc, expected, 'mongodoc looks incorrect');
      });

      it('is NOT possible to create an entity with an "empty string" ENTITY type (field syntax restrictions)', async () => {
        // In general: type must be at the least a string with length > 0
        const validConfig = fiwareApi.createEntity.createRequestConfig({entity: {id: 'supanu', type: 'temp'}});
        const invalidConfig = {
          ...validConfig,
          data: {...validConfig.data, type: ''},
        };
        const err = await assertRejects(fiwareApi.rawRequest(invalidConfig));
        assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_TOO_SHORT_ENTITY_TYPE);
      });

      it('is NOT possible to create an entity with an "empty string" ATTR type (field syntax restrictions)', async () => {
        // In general: type must be at the least a string with length > 0
        const validConfig = fiwareApi.createEntity.createRequestConfig({entity: {id: 'supanu', spatter: 2}});
        const invalidConfig = {
          ...validConfig,
          data: {
            ...validConfig.data,
            spatter: {
              ...validConfig.data.spatter,
              type: '', // <--------------------- empty!!!
            },
          },
        };
        const err = await assertRejects(fiwareApi.rawRequest(invalidConfig));
        assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_TOO_SHORT_ATTR_TYPE);
      });

      it('is NOT possible to create an entity with an "empty string" METADATA type (field syntax restrictions)', async () => {
        // In general: type must be at the least a string with length > 0
        const entity = {
          id: 'supanu',
          tatter: {
            value: 2,
            metadata: {
              johnny: 4,
              type: 'WillBeChangedToNothing',
            },
          },
        };
        const validConfig = fiwareApi.createEntity.createRequestConfig({entity});
        // MAKE it invalid
        _.set(validConfig, 'data.tatter.metadata.johnny.type', '');
        const err = await assertRejects(fiwareApi.rawRequest(validConfig));
        assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_TOO_SHORT_METADATA_TYPE);
      });

      it('it is NOT possible to create an entity with an attr WITHOUT a name', async () => {
        // This test sets a lower bound on 'type' prop in general
        // In general: type must be at the least a string with length > 0
        const validConfig = fiwareApi.createEntity.createRequestConfig({entity: {id: 'supachuu', type: 'temp', joe: 2}});
        const invalidConfig = {
          ...validConfig,
          // we want to move "joe" to ""
          data: _.assign(
            {'': validConfig.data.joe},
            _.omit(validConfig.data, 'joe'),
          ),
        };
        const err = await assertRejects(fiwareApi.rawRequest(invalidConfig));
        assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_TOO_SHORT_ATTR_NAME);
      });

      it('IS possible to save a "null" value', async () => {
        const entity = {id: 'supanu', type: 'Kerchew', nully: {type: 'Nullz', value: null}};
        await fiwareApi.createEntity({entity});
        const mongoDocs = await mongoApi.findEntities();
        assert(mongoDocs.length === 1, 'there should only be the one');
        assert(_.get(mongoDocs[0], 'attrs.nully.value') === null, 'nully should be null');
      });

      it('should be possible to create an entity with "significant structure"', async () => {
        const entity = {
          id: 'spoooner',
          type: 'kadinkydink',
          powys: 23,
          shper: {
            value: 'asdlkfj',
            metadata: {
              derps: true,
            },
          },
          turky: {
            type: 'Qudirk',
            value: {stuff: {in: {da: {box: 3}}}},
          },
          customLocation: {
            type: 'geo:point',
            value: '23, 34',
          },
        };
        await fiwareApi.createEntity({entity});
        const mongoDocs = await mongoApi.findEntities();
        assert.strictEqual(mongoDocs.length, 1, 'there should be 1 entity');
        const doc = mongoDocs[0];
        // make sure that we have the stuff we expect to have in the doc

        // DEVIATION!!
        if (fiwareApi === orionApi) {
          assert(_.isString(doc.lastCorrelator), 'entity should have a lastCorrelator');
        }

        // DEVIATION!! dates-handling will be PERMANENTLY different
        if (fiwareApi === orionApi) {
          assert(_.isNumber(doc.creDate), 'entity should have a creDate');
          assert(_.isNumber(doc.modDate), 'entity should have a modDate');
          assert.strictEqual(_.size(doc.attrs), 4, 'entity should have 4 attrs');
          assert.strictEqual(_.size(doc.attrs), doc.attrNames.length, 'attrs and attrNames do not match in size');
          _.each(doc.attrs, (attr, attrKey) => {
            assert(_.isNumber(doc.creDate), 'entity should have a creDate');
            assert(_.isNumber(doc.modDate), 'entity should have a modDate');
            assert.strictEqual(_.size(attr.md), attr.mdNames.length, 'metadata and mdNames do not match in size');
            assert(_.includes(doc.attrNames, attrKey), 'missing attrName');
          });
        }
        if (fiwareApi === ngsiApi) {
          assert(_.isDate(doc.creDate), 'entity should have a creDate');
          assert(_.isDate(doc.modDate), 'entity should have a modDate');
          assert(!_.has(doc, 'attrNames'));
          assert.strictEqual(_.size(doc.attrs), 4, 'entity should have 4 attrs');
          _.each(doc.attrs, (attr, attrKey) => {
            assert(_.isDate(doc.creDate), 'entity should have a creDate');
            assert(_.isDate(doc.modDate), 'entity should have a modDate');
            assert(!_.has(attr, 'mdNames'));
          });
        }

        // DEVIATION!!
        if (fiwareApi === orionApi) {
          // inspect the "special" location thingy
          const expectedLocation = {
            attrName: 'customLocation',
            coords: {
              type: 'Point',
              coordinates: [34, 23],
            },
          };
          assert.deepStrictEqual(expectedLocation, doc.location, 'Invalid location prop');
        }

        // and go through the attrs one by one
        const expectedPowys = {
          type: 'Number',
          value: 23,
          md: {},
        };
        const expectedShper = {
          type: 'Text',
          value: 'asdlkfj',
          md: {
            derps: {type: 'Boolean', value: true},
          },
        };
        const expectedTurky = {
          type: 'Qudirk',
          value: {stuff: {in: {da: {box: 3}}}},
          md: {},
        };
        const omitteds = ['creDate', 'modDate', 'mdNames'];
        assert.deepStrictEqual(
          expectedPowys,
          _.assign({md: {}}, _.omit(doc.attrs.powys, omitteds)),
          'Invalid attr powys',
        );
        assert.deepStrictEqual(
          expectedShper,
          _.omit(doc.attrs.shper, omitteds),
          'Invalid attr shper',
        );
        assert.deepStrictEqual(
          expectedTurky,
          _.assign({md: {}}, _.omit(doc.attrs.turky, omitteds)),
          'Invalid attr turky',
        );

        // DIVERGE!!
        if (fiwareApi === orionApi) {
          const expectedCustomLocation = {
            type: 'geo:point',
            value: '23, 34',
          };
          const actualLocation = _.omit(doc.attrs.customLocation, omitteds);
          assert.deepStrictEqual(actualLocation, expectedCustomLocation);
        }
        if (fiwareApi === ngsiApi) {
          const expectedCustomLocation = {
            type: 'geo:point',
            value: {
              type: 'Point',
              coordinates: [34, 23],
            },
            md: {},
          };
          const actualLocation = _.omit(doc.attrs.customLocation, omitteds);
          assert.deepStrictEqual(actualLocation, expectedCustomLocation);
        }
      });

    });

  });

});
