/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');

const {
  assertRejects,
  ngsiResponseErrorUtils: {assertErrorsMatch},
  ngsiHeaderUtils: {createServicePathHeader},
} = require('@ratatosk/ngsi-utils');

const {ORION_ERRORS} = require('@ratatosk/ngsi-constants');

const {orionApi, ngsiApi} = require('../../../client-api-util');
const mongoApi = require('../../../mongo-api');

describe('checking the uniqueness of the database _id object', () => {


  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      beforeEach(async () => mongoApi.assertEntitiesEmpty());
      afterEach(async () => mongoApi.wipeEntities());

      it('should not be possible to create two entities with the same id\'s and types (and same service paths)', async () => {
        const entity = {id: 'supanu', type: 'Kerchew'};
        await fiwareApi.createEntity({entity});
        const err = await assertRejects(fiwareApi.createEntity({entity}));
        assertErrorsMatch(err, ORION_ERRORS.UNPROCESSABLE_ENTITY_ALREADY_EXISTS);
      });

      it('should be possible to create two entities with the same id\'s but different types (same explicit service paths)', async () => {
        const ent1 = {id: 'supanu', type: 'Kerchew'};
        const ent2 = {id: 'supanu', type: 'Wakkow'};
        const headers = createServicePathHeader('/derpy/lederp/yerp');
        await fiwareApi.createEntity({entity: ent1, headers});
        await fiwareApi.createEntity({entity: ent2, headers});
        const mongoDocs = await mongoApi.findEntities();
        assert.strictEqual(mongoDocs.length, 2, 'there should be 2 entities');

        // DIVERGE!!
        if (fiwareApi === orionApi) {
          const id0 = mongoDocs[0]._id;
          const id1 = mongoDocs[1]._id;
          assert.strictEqual(id0.id, id1.id, 'should have the same id');
          assert.strictEqual(id0.servicePath, id1.servicePath, 'should have the same servicePath');
          assert.notStrictEqual(id0.type, id1.type, 'should have different types');
        }
        if (fiwareApi === ngsiApi) {
          const id0 = mongoDocs[0].orionId;
          const id1 = mongoDocs[1].orionId;
          assert.strictEqual(id0.id, id1.id, 'should have the same id');
          assert.strictEqual(id0.servicePath, id1.servicePath, 'should have the same servicePath');
          assert.notStrictEqual(id0.type, id1.type, 'should have different types');
        }
      });

      it('should be possible to create two entities with the same id\'s and same types (but different service paths)', async () => {
        const ent1 = {id: 'supanu', type: 'Kerchew'};
        const ent2 = {id: 'supanu', type: 'Kerchew'};
        const path1 = createServicePathHeader('/same/beginning');
        const path2 = createServicePathHeader('/same/beginning/but/different/end');
        await fiwareApi.createEntity({entity: ent1, headers: path1});
        await fiwareApi.createEntity({entity: ent2, headers: path2});
        const mongoDocs = await mongoApi.findEntities();
        assert.strictEqual(mongoDocs.length, 2, 'there should be 2 entities');

        // DIVERGE!!
        if (fiwareApi === orionApi) {
          const id0 = mongoDocs[0]._id;
          const id1 = mongoDocs[1]._id;
          assert.strictEqual(id0.id, id1.id, 'should have the same id');
          assert.strictEqual(id0.type, id1.type, 'should have the same type');
          assert.notStrictEqual(id0.servicePath, id1.servicePath, 'should have different servicePaths');
        }
        if (fiwareApi === ngsiApi) {
          const id0 = mongoDocs[0].orionId;
          const id1 = mongoDocs[1].orionId;
          assert.strictEqual(id0.id, id1.id, 'should have the same id');
          assert.strictEqual(id0.type, id1.type, 'should have the same type');
          assert.notStrictEqual(id0.servicePath, id1.servicePath, 'should have different servicePaths');
        }
      });

      it('and of course, it should be possible to create two entities with different id\'s but same types (and same service paths)', async () => {
        const ent1 = {id: 'supanu', type: 'Kerchew'};
        const ent2 = {id: 'pointy', type: 'Kerchew'};
        const headers = createServicePathHeader('/same/path');
        await fiwareApi.createEntity({entity: ent1, headers});
        await fiwareApi.createEntity({entity: ent2, headers});
        const mongoDocs = await mongoApi.findEntities();
        assert.strictEqual(mongoDocs.length, 2, 'there should be 2 entities');

        // DIVERGE!!
        if (fiwareApi === orionApi) {
          const id0 = mongoDocs[0]._id;
          const id1 = mongoDocs[1]._id;
          assert.notStrictEqual(id0.id, id1.id, 'should have different ids');
          assert.strictEqual(id0.type, id1.type, 'should have the same type');
          assert.strictEqual(id0.servicePath, id1.servicePath, 'should have the same servicePath');
        }
        if (fiwareApi === ngsiApi) {
          const id0 = mongoDocs[0].orionId;
          const id1 = mongoDocs[1].orionId;
          assert.notStrictEqual(id0.id, id1.id, 'should have different ids');
          assert.strictEqual(id0.type, id1.type, 'should have the same type');
          assert.strictEqual(id0.servicePath, id1.servicePath, 'should have the same servicePath');
        }
      });

    });

  });

});
