/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const serialPromise = require('promise-serial-exec');

const {
  assertRejects,
  ngsiResponseErrorUtils: {assertErrorsMatch},
  ngsiDatumValidator: {inferTypeForGeneralValue},
  ngsiDateTimeUtils: {fromSeconds, toSeconds, fromMillisecs, toMillisecs, toDate},
  ngsiEntityFormatter: {formatEntity, formatAttribute},
  // ngsiAttributeValueFormatter: {formatAttributeValue},
} = require('@ratatosk/ngsi-utils');

const {
  ORION_ERRORS,
} = require('@ratatosk/ngsi-constants');

const {orionApi, ngsiApi} = require('../../client-api-util');
const mongoApi = require('../../mongo-api');

const entity = formatEntity({
  type: 'Blooey',
  id: 'blerks',
  brrp: 2,
  moop: {value: 3, metadata: {goop: 4}},
});

const isOrionNow = (date, now) => {
  const orionNow = Math.floor(now / 1000);
  return date === orionNow;
};

const isNgsiNow = (date, now) => {
  return date.valueOf() - now <= 20; // millisecs
};

describe('date-time-handling', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      let now;
      before(async () => {
        now = Date.now();
        await mongoApi.assertEntitiesEmpty();
        // and create an entity
        await fiwareApi.createEntity({entity});
        const numDocs = await mongoApi.countEntities();
        assert.strictEqual(numDocs, 1);
      });
      after(async () => mongoApi.wipeEntities());

      it('mongoDoc should have the expected timestamps', async () => {
        const mongoDocs = await mongoApi.findEntities();
        assert.strictEqual(mongoDocs.length, 1, 'should just be the one..');
        const doc = mongoDocs[0];

        // DIVERGE!!
        if (fiwareApi === orionApi) {
          // the entity is timestamped
          assert(isOrionNow(doc.creDate, now));
          assert(isOrionNow(doc.modDate, now));
          // attrs are timestamped
          assert(isOrionNow(doc.attrs.brrp.creDate, now));
          assert(isOrionNow(doc.attrs.brrp.modDate, now));
          assert(isOrionNow(doc.attrs.moop.creDate, now));
          assert(isOrionNow(doc.attrs.moop.modDate, now));
        }
        if (fiwareApi === ngsiApi) {
          // the entity is timestamped
          assert(isNgsiNow(doc.creDate, now));
          assert(isNgsiNow(doc.modDate, now));
          // attrs are timestamped
          assert(isNgsiNow(doc.attrs.brrp.creDate, now));
          assert(isNgsiNow(doc.attrs.brrp.modDate, now));
          assert(isNgsiNow(doc.attrs.moop.creDate, now));
          assert(isNgsiNow(doc.attrs.moop.modDate, now));
        }

        // metadata is not timestamped
        assert(_.has(doc, 'attrs.moop.md.goop.value'));
        assert(!_.has(doc.attrs.moop.md.goop, 'creDate'));
        assert(!_.has(doc.attrs.moop.md.goop, 'modDate'));
      });

      it('the orion doc should also have the expected timestamps', async () => {
        const params = {
          attrs: ['brrp', 'dateCreated', 'dateModified'],
        };
        const {data: doc} = await fiwareApi.findEntity({entityId: 'blerks', params});

        // DIVERGE!!
        if (fiwareApi === orionApi) {
          assert.strictEqual(doc.id, 'blerks');
          // dateCreated
          const msCreated = toSeconds(doc.dateCreated.value);
          assert(isOrionNow(msCreated, now));
          const dt = fromSeconds(msCreated);
          // dt is missing the thousands digit
          assert.notStrictEqual(dt, doc.dateCreated.value);
          const fixedUpDt = dt.slice(0, dt.length - 2) + 'Z';
          assert.strictEqual(fixedUpDt, doc.dateCreated.value);
          // dateModified
          const msMod = toSeconds(doc.dateModified.value);
          assert(isOrionNow(msMod, now));
          const dm = fromSeconds(msMod);
          // dt is missing the thousands digit
          assert.notStrictEqual(dm, doc.dateModified.value);
          const fixedUpDm = dt.slice(0, dm.length - 2) + 'Z';
          assert.strictEqual(fixedUpDm, doc.dateModified.value);
        }
        if (fiwareApi === ngsiApi) {
          assert.strictEqual(doc.id, 'blerks');
          // dateCreated
          const msCreated = toMillisecs(doc.dateCreated.value);
          assert(isNgsiNow(msCreated, now));
          const dt = fromMillisecs(msCreated);
          // dt is NOT missing the thousands digit
          assert.strictEqual(dt, doc.dateCreated.value);
          // dateModified
          const msMod = toMillisecs(doc.dateModified.value);
          assert(isNgsiNow(msMod, now));
          const dm = fromMillisecs(msMod);
          // dt is NOT missing the thousands digit
          assert.strictEqual(dm, doc.dateModified.value);
        }
      });

      it('what happens when we save a single NGSI_V2_FORMAT string as DateTime? (WORKS)', async () => {
        const seconds = 1533333333; // s since 1970
        const dateTime = fromSeconds(seconds);
        const date = toDate(dateTime);
        const attr = formatAttribute({type: 'DateTime', value: dateTime});
        await fiwareApi.upsertEntityAttrs({entityId: 'blerks', attrs: {lupo: attr}});

        // DIVERGE!!
        if (fiwareApi === orionApi) {
          // check the mongo doc
          const [mongoDoc] = await mongoApi.findEntities();
          assert.strictEqual(mongoDoc.attrs.lupo.value, seconds, 'Orion does NOT save the string version!');
          // check the entity
          const {data: doc} = await fiwareApi.findEntity({entityId: 'blerks'});
          const lupo = doc.lupo.value;
          assert.notStrictEqual(lupo, dateTime, 'Ought to be equal, but isnt');
          const fixedLupo = lupo.slice(0, lupo.length - 1) + '0Z';
          assert.strictEqual(fixedLupo, dateTime);
        }
        if (fiwareApi === ngsiApi) {
          // check the mongo doc
          const [mongoDoc] = await mongoApi.findEntities();
          assert.deepStrictEqual(mongoDoc.attrs.lupo.value, date);
          // check the entity
          const {data: doc} = await fiwareApi.findEntity({entityId: 'blerks'});
          const lupo = doc.lupo.value;
          // and the string looks good from the start
          assert.strictEqual(lupo, dateTime);
        }

      });

      it('what happens when we save a single ORION_FORMAT string as DateTime? (ALSO WORKS)', async () => {
        const seconds = 1533333333; // s since 1970
        const dateTime = fromSeconds(seconds);
        const date = toDate(dateTime);
        const mangledDateTime = dateTime.slice(0, dateTime.length - 2) + 'Z';
        const attr = formatAttribute({type: 'DateTime', value: mangledDateTime});
        await fiwareApi.upsertEntityAttrs({entityId: 'blerks', attrs: {crupo: attr}});

        // DIVERGE!!
        if (fiwareApi === orionApi) {
          // check the mongo doc
          const [mongoDoc] = await mongoApi.findEntities();
          assert.strictEqual(mongoDoc.attrs.crupo.value, seconds, 'Orion does NOT save the string version!');
          // check the entity
          const {data: doc} = await fiwareApi.findEntity({entityId: 'blerks'});
          const crupo = doc.crupo.value;
          assert.notStrictEqual(crupo, dateTime, 'Ought to be equal, but isnt');
          const fixedCrupo = crupo.slice(0, crupo.length - 1) + '0Z';
          assert.strictEqual(fixedCrupo, dateTime);
        }
        if (fiwareApi === ngsiApi) {
          // check the mongo doc
          const [mongoDoc] = await mongoApi.findEntities();
          assert.deepStrictEqual(mongoDoc.attrs.crupo.value, date);
          // check the entity
          const {data: doc} = await fiwareApi.findEntity({entityId: 'blerks'});
          const crupo = doc.crupo.value;
          assert.strictEqual(crupo, dateTime);
        }

      });

      it('what happens when we try to save a DateTime with invalid formatting?', async () => {
        const invalids = [
          'eerp',
          null,
        ];
        await serialPromise(_.map(invalids, (invalid) => async () => {
          const nowMs = Date.now();
          const nowSecs = Math.floor(nowMs / 1000);
          await fiwareApi.upsertEntityAttrs({entityId: 'blerks', attrs: {chalupo: invalid}});

          // DIVERGE!!
          if (fiwareApi === orionApi) {
            // check the mongoDoc (type was coerced)
            const [mongoDoc] = await mongoApi.findEntities();
            assert.strictEqual(mongoDoc.attrs.chalupo.value, invalid, 'the invalid value IS saved');
            const expectedMongoChalupo = {
              value: invalid,
              type: inferTypeForGeneralValue(invalid),
              creDate: nowSecs,
              modDate: nowSecs,
              mdNames: [],
            };
            assert.deepStrictEqual(mongoDoc.attrs.chalupo, expectedMongoChalupo);
          }
          if (fiwareApi === ngsiApi) {
            // check the mongoDoc (type was coerced)
            const [mongoDoc] = await mongoApi.findEntities();
            assert.strictEqual(mongoDoc.attrs.chalupo.value, invalid, 'the invalid value IS saved');
            const expectedMongoChalupo = {
              value: invalid,
              type: inferTypeForGeneralValue(invalid),
              md: {},
            };
            const prunedMongoChalupo = _.omit(mongoDoc.attrs.chalupo, ['creDate', 'modDate']);
            assert.deepStrictEqual(prunedMongoChalupo, expectedMongoChalupo);
            assert(isNgsiNow(mongoDoc.attrs.chalupo.creDate.valueOf(), nowMs));
            assert(isNgsiNow(mongoDoc.attrs.chalupo.modDate.valueOf(), nowMs));
          }

          // check the orionDoc (once again -- type was coerced)
          const {data: attrDoc} = await fiwareApi.findEntityAttr({entityId: 'blerks', attrName: 'chalupo'});
          const invalidType = inferTypeForGeneralValue(invalid);
          assert.notStrictEqual(invalidType, 'DateTime');
          const expected = {
            value: invalid,
            type: invalidType,
            metadata: {},
          };
          assert.deepStrictEqual(attrDoc, expected);
        }));
      });

      it('update a pre-existing DateTime using updateEntityAttributeValue to a NON-datetime value, and then back again. see what transpires', async () => {
        const seconds1 = 1533333333;
        const seconds2 = 1533344333;
        const date1 = toDate(fromSeconds(seconds1));
        const date2 = toDate(fromSeconds(seconds2));
        // DIVERGE!! the whole way
        if (fiwareApi === orionApi) {
          // check the attr for lupo at the beginning here
          const [mongoDocInit] = await mongoApi.findEntities();
          assert.strictEqual(mongoDocInit.attrs.lupo.value, seconds1);
          assert.strictEqual(mongoDocInit.attrs.lupo.type, 'DateTime');
          // update the value to an invalid DateTime value
          const reqArg = {
            entityId: 'blerks',
            attrName: 'lupo',
            attrValue: seconds1, // ought to be invalid
          };
          await fiwareApi.updateEntityAttrValue(reqArg);
          // check the mongo attr for lupo again
          const [mongoDocMid] = await mongoApi.findEntities();
          assert.strictEqual(mongoDocMid.attrs.lupo.value, 1533333333);
          assert.strictEqual(mongoDocMid.attrs.lupo.type, 'DateTime'); // NO coercion!!
          // update the value to an OBVIOUSLY invalid DateTime value
          reqArg.attrValue = 'bubbles';
          await fiwareApi.updateEntityAttrValue(reqArg);
          // and check the mongo attr for lupo again
          const [mongoDocMid2] = await mongoApi.findEntities();
          assert.strictEqual(mongoDocMid2.attrs.lupo.value, 'bubbles');
          assert.strictEqual(mongoDocMid2.attrs.lupo.type, 'DateTime'); // NO coercion!!
          // update with a supposedly valid DateTime value
          reqArg.attrValue = fromSeconds(seconds1);
          await fiwareApi.updateEntityAttrValue(reqArg);
          // and check the mongo attr for lupo again
          const [mongoDocMid3] = await mongoApi.findEntities();
          assert.strictEqual(mongoDocMid3.attrs.lupo.value, '2018-08-03T21:55:33.000Z'); // NOT 1533333333
          assert.strictEqual(mongoDocMid3.attrs.lupo.type, 'DateTime'); // NO coercion!!

          // patchEntityAttrs (with an VALID DateTime)
          const attrReqArg = {
            entityId: 'blerks',
            attrs: {
              lupo: {type: 'DateTime', value: fromSeconds(seconds2)},
            },
          };
          await fiwareApi.patchEntityAttrs(attrReqArg);
          // and check the mongo attr for lupo again
          const [mongoDocPostMid] = await mongoApi.findEntities();
          assert.strictEqual(mongoDocPostMid.attrs.lupo.value, 1533344333); // BACK TO ORIGINAL!!
          assert.strictEqual(mongoDocPostMid.attrs.lupo.type, 'DateTime'); // (as expected)
        }
        if (fiwareApi === ngsiApi) {
          // check the attr for lupo at the beginning here
          const [mongoDocInit] = await mongoApi.findEntities();
          assert.deepStrictEqual(mongoDocInit.attrs.lupo.value, date1);
          assert.strictEqual(mongoDocInit.attrs.lupo.type, 'DateTime');
          // update the value to an invalid DateTime value
          const reqArg = {
            entityId: 'blerks',
            attrName: 'lupo',
            attrValue: 1533333333, // ought to be invalid
          };
          await fiwareApi.updateEntityAttrValue(reqArg);
          // check the mongo attr for lupo again
          const [mongoDocMid] = await mongoApi.findEntities();
          assert.strictEqual(mongoDocMid.attrs.lupo.value, 1533333333);
          assert.strictEqual(mongoDocMid.attrs.lupo.type, 'DateTime'); // NO coercion!!
          // update the value to an OBVIOUSLY invalid DateTime value
          reqArg.attrValue = 'bubbles';
          await fiwareApi.updateEntityAttrValue(reqArg);
          // and check the mongo attr for lupo again
          const [mongoDocMid2] = await mongoApi.findEntities();
          assert.strictEqual(mongoDocMid2.attrs.lupo.value, 'bubbles');
          assert.strictEqual(mongoDocMid2.attrs.lupo.type, 'DateTime'); // NO coercion!!
          // update with a supposedly valid DateTime value
          reqArg.attrValue = fromSeconds(seconds1);
          await fiwareApi.updateEntityAttrValue(reqArg);
          // and check the mongo attr for lupo again
          const [mongoDocMid3] = await mongoApi.findEntities();
          assert.deepStrictEqual(mongoDocMid3.attrs.lupo.value, date1); // NOT 1533333333
          assert.strictEqual(mongoDocMid3.attrs.lupo.type, 'DateTime'); // NO coercion!!
          // patchEntityAttrs (with an VALID DateTime)
          const attrReqArg = {
            entityId: 'blerks',
            attrs: {
              lupo: {type: 'DateTime', value: fromSeconds(seconds2)},
            },
          };
          await fiwareApi.patchEntityAttrs(attrReqArg);
          // and check the mongo attr for lupo again
          const [mongoDocPostMid] = await mongoApi.findEntities();
          assert.deepStrictEqual(mongoDocPostMid.attrs.lupo.value, date2); // BACK TO ORIGINAL!!
          assert.strictEqual(mongoDocPostMid.attrs.lupo.type, 'DateTime'); // (as expected)
        }
      });

      it('patchEntityAttrs using an invalid DateTime FAILS (does NOT coerce OR blindly overwrite)', async () => {
        const attrReqArg = {
          entityId: 'blerks',
          attrs: {
            lupo: {type: 'DateTime', value: fromSeconds(1533344333)},
          },
        };
        const config = fiwareApi.patchEntityAttrs.createRequestConfig(attrReqArg);
        const seconds = 1533333333;
        config.data.lupo.value = seconds; // set to an invalid DateTime

        // DIVERGE!!
        if (fiwareApi === orionApi) {
          const err = await assertRejects(fiwareApi.rawRequest(config));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_INVALID_DATE_FORMAT);
        }
        if (fiwareApi === ngsiApi) {
          await fiwareApi.rawRequest(config); // works
          // check the mongoDoc
          const [mongoDoc] = await mongoApi.findEntities();
          assert.deepStrictEqual(mongoDoc.attrs.lupo.value, seconds); // SAVES the value, even when invalid
          assert.strictEqual(mongoDoc.attrs.lupo.type, 'DateTime'); // (as expected)
        }
      });

    });

  });

});
