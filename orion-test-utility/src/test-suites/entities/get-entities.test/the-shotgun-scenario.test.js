/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const serialPromise = require('promise-serial-exec');

const {
  ngsiHeaderUtils: {createServicePathHeader},
  ngsiQueryResponseUtils: {createPaginator},
  ngsiEntityFormatter: {formatEntity},
} = require('@ratatosk/ngsi-utils');

const {orionApi, ngsiApi} = require('../../../client-api-util');
const mongoApi = require('../../../mongo-api');

// What are the different selectors?
// - servicePath (single or multi)
// - id
// - idPattern
// - type
// - typePattern
// - q filter (single & multi)
// - mq filter (single & multi)
// - geo filter

const genPerms = (items, acc = [{}]) => {
  if (!items.length) {
    return acc;
  }
  const attr = items[0];
  const next = _.concat(
    acc,
    _.map(acc, (accItem) => ({
      ...accItem,
      ...attr,
    })),
  );
  return genPerms(items.slice(1), next);
};

const servicePaths = ['/a', '/b', '/c'];
// 3
const ids = ['albert', 'alberta', 'deee'];
// 3 * 3 = 9
const types = ['column', 'columbia', 'geee'];
// 9 * 3 = 27
const attrs = genPerms([{a: 2}, {b: 4}]);
// 27 * 2^2 = 27 * 4 = 108
const mdItems = genPerms([{ma: 3}, {mb: 6}]);
// 108 * 2^2 = 432
const generateTemplates = () => {
  const templates = [];
  _.each(servicePaths, (servicePath) => {
    const headers = createServicePathHeader(servicePath);
    _.each(ids, (id) => {
      _.each(types, (type) => {
        _.each(attrs, (attr) => {
          _.each(mdItems, (mdItem) => {
            const entity = formatEntity({
              id: `${id}${_.concat(_.values(attr), _.values(mdItem)).join('')}`,
              type,
              servicePath, // just for confirmation
              ..._.mapValues(attr, (val) => {
                return {value: val, metadata: mdItem};
              }),
            });
            templates.push({headers, entity});
          });
        });
      });
    });
  });
  return templates;
};


describe('the shotgun scenario', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      before(async function () {
        this.timeout(30000);
        await mongoApi.assertEntitiesEmpty();
        const entityTemplates = generateTemplates();
        await serialPromise(_.map(entityTemplates, ({headers, entity}) => () => {
          return fiwareApi.createEntity({entity, headers});
        }));
      });
      after(async () => mongoApi.wipeEntities());

      it('there should be 432 entities (total)', async () => {
        const params = {limit: 5, count: true, options: 'values'};
        const res = await fiwareApi.getEntities({servicePaths, params});
        const paginator = createPaginator(res);
        assert.strictEqual(paginator.count, 432, 'there should be 432 query docs total');
        assert.strictEqual(paginator.docs.length, 5, 'there should be 5 docs');
      });

      it('there should be 144 in each service path', async () => {
        await serialPromise(_.map(servicePaths, (sp) => async () => {
          const headers = createServicePathHeader(sp);
          const params = {count: true};
          const res = await fiwareApi.getEntities({headers, params});
          const paginator = createPaginator(res);
          assert.strictEqual(paginator.count, 144, 'there should be 144 query docs total');
        }));
      });

      it('there should be 288 in each combo of servicePaths', async () => {
        await serialPromise(_.map(servicePaths, (sp) => async () => {
          const paths = _.without(servicePaths, sp);
          const headers = createServicePathHeader(...paths);
          const params = {count: true};
          const res = await fiwareApi.getEntities({headers, params});
          const paginator = createPaginator(res);
          assert.strictEqual(paginator.count, 288, 'there should be 288 query docs total');
        }));
      });

      it('should be possible to narrow down as desired', async () => {
        const headers = createServicePathHeader('/a', '/c');
        const params = {
          limit: 500,
          count: true,
          idPattern: 'albert',
          typePattern: 'col',
          q: 'a<3;b>0',
          mq: 'a.ma;!not.there',
        };
        const res = await fiwareApi.getEntities({
          params,
          headers,
        });
        const paginator = createPaginator(res);
        assert.strictEqual(paginator.count, 16, 'there should be 16');
        const uniqueServicePaths = _.uniq(_.map(paginator.docs, (doc) => doc.servicePath.value).sort());
        const expectedServicePaths = ['/a', '/c'].sort();
        assert.deepStrictEqual(uniqueServicePaths, expectedServicePaths, 'wrong servicePaths');
      });

    });

  });

});
