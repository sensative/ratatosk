/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');

const {orionApi, ngsiApi} = require('../../../client-api-util');
const mongoApi = require('../../../mongo-api');
const serialPromise = require('promise-serial-exec');

const {
  assertRejects,
  ngsiResponseErrorUtils: {assertErrorsMatch},
} = require('@ratatosk/ngsi-utils');

const {ORION_ERRORS} = require('@ratatosk/ngsi-constants');

const entities = [
  {id: 'derp', type: 'Andy'},
  {id: 'derd', type: 'Mandy'},
  {id: 'perd', type: 'Sandy'},
  {id: 'prd', type: 'Sandy'},
];

describe('check-entity-id-pattern (filtering by idPattern should work as expected)', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      before(async () => {
        await mongoApi.assertEntitiesEmpty();
        // create some entities to check against
        await serialPromise(_.map(entities, (entity) => async () => {
          return fiwareApi.createEntity({entity});
        }));
      });
      after(async () => mongoApi.wipeEntities());

      it('should find all entities without an id filter', async () => {
        const {data: docs} = await fiwareApi.getEntities();
        assert.strictEqual(docs.length, 4, 'wrong number of docs');
      });

      it('should find 3 with ids that match "er"', async () => {
        const params = {idPattern: 'er'};
        const {data: docs} = await fiwareApi.getEntities({params});
        assert.strictEqual(docs.length, 3, 'should find 3 "er"s');
      });

      it('should find 1 with ids that match "erp"', async () => {
        const params = {idPattern: 'erp'};
        const {data: docs} = await fiwareApi.getEntities({params});
        assert.strictEqual(docs.length, 1, 'should find 1 "erp"s');
      });

      it('should find 2 with ids that match "erd"', async () => {
        const params = {idPattern: 'erd'};
        const {data: docs} = await fiwareApi.getEntities({params});
        assert.strictEqual(docs.length, 2, 'should find 2 "erd"s');
      });

      it('check list of idPattern "erd,pr" (UPSHOT: this does NOT take lists)', async () => {
        const config = fiwareApi.getEntities.createRequestConfig();
        _.set(config, 'params.idPattern', 'erd,pr');
        const {data: docs} = await fiwareApi.rawRequest(config);
        assert.strictEqual(docs.length, 0);
      });

      it('an empty "idPattern" arg FAILS', async () => {
        const config = fiwareApi.getEntities.createRequestConfig();
        _.set(config, 'params.idPattern', ''); // mutate!
        const err = await assertRejects(fiwareApi.rawRequest(config));
        assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_EMPTY_IDPATTERN_PARAM);
      });

    });

  });

});
