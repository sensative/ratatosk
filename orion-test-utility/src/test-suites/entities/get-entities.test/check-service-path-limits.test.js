/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const serialPromise = require('promise-serial-exec');

const {
  assertRejects,
  ngsiResponseErrorUtils: {assertErrorsMatch},
  ngsiHeaderUtils: {createServicePathHeader},
} = require('@ratatosk/ngsi-utils');

const {ORION_ERRORS} = require('@ratatosk/ngsi-constants');

const {ngsiApi, orionApi, handleWackyOrionErrors} = require('../../../client-api-util');
const mongoApi = require('../../../mongo-api');

const entityTemplates = [
  {id: 'spiggy', type: 'Jam', servicePath: '/'},
  {id: 'spiggy', type: 'Jam', servicePath: '/a'},
  {id: 'spiggy', type: 'Jam', servicePath: '/b'},
  {id: 'spiggy', type: 'Jam', servicePath: '/c'},
  {id: 'spiggy', type: 'Jam', servicePath: '/d'},
  {id: 'spiggy', type: 'Jam', servicePath: '/a/a'},
  {id: 'spiggy', type: 'Jam', servicePath: '/a/b'},
  {id: 'spiggy', type: 'Jam', servicePath: '/a/c'},
  {id: 'spiggy', type: 'Jam', servicePath: '/a/d'},
  {id: 'spiggy', type: 'Jam', servicePath: '/a/a/a'},
  {id: 'spiggy', type: 'Jam', servicePath: '/a/a/b'},
  {id: 'spiggy', type: 'Jam', servicePath: '/a/a/c'},
  {id: 'spiggy', type: 'Jam', servicePath: '/a/a/d'},
];

describe('check-service-path-limits (find entities)', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      before(async () => {
        await mongoApi.assertEntitiesEmpty();
        await serialPromise(_.map(entityTemplates, (template) => () => {
          const headers = createServicePathHeader(template.servicePath);
          const entity = _.omit(template, 'servicePath');
          return fiwareApi.createEntity({entity, headers});
        }));
      });
      after(async () => mongoApi.wipeEntities());

      it('should find all the docs', async () => {
        const {data: docs} = await fiwareApi.getEntities();
        assert.strictEqual(docs.length, 13);
      });

      it('should find all the docs with servicePath = "" (i.e. empty string just gets ignored)', async () => {
        const headers = createServicePathHeader('');
        const {data: docs} = await fiwareApi.getEntities({headers});
        assert.strictEqual(docs.length, 13);
      });

      it('should find 1 doc with servicePath = "/"', async () => {
        const headers = createServicePathHeader('/');
        const {data: docs} = await fiwareApi.getEntities({headers});
        assert.strictEqual(docs.length, 1, 'should find 1 "/" doc');
      });

      it('should find 1 doc with servicePath = "/a"', async () => {
        const headers = createServicePathHeader('/a');
        const {data: docs} = await fiwareApi.getEntities({headers});
        assert.strictEqual(docs.length, 1, 'should find 1 "/a" doc');
      });

      it('should find 4 doc with servicePaths = "/a,/a/a,/a/a/a,/a/a/d"', async () => {
        const headers = createServicePathHeader('/a,/a/a,/a/a/a,/a/a/d');
        const {data: docs} = await fiwareApi.getEntities({headers});
        assert.strictEqual(docs.length, 4, 'should find 4 "/a,/a/a,/a/a/a,/a/a/d" docs');
      });

      it('should find 10 docs with servicePaths = "/a,/b,/c,/d,/a/a,/a/b,/a/c,/a/d,/a/a/a,/a/a/b"', async () => {
        const headers = createServicePathHeader('/a,/b,/c,/d,/a/a,/a/b,/a/c,/a/d,/a/a/a,/a/a/b');
        const {data: docs} = await fiwareApi.getEntities({headers});
        assert.strictEqual(docs.length, 10, 'should find 10 "/a,/b,/c,/d,/a/a,/a/b,/a/c,/a/d,/a/a/a,/a/a/b" docs');
      });

      it('NOTE: Using 11 (valid) service paths actually SUCCEEDS (status 200), but the data object is an error object.. bla', async () => {
        const config = fiwareApi.getEntities.createRequestConfig();
        _.set(config, ['headers', 'Fiware-ServicePath'], '/a,/b,/c,/d,/a/a,/a/b,/a/c,/a/d,/a/a/a,/a/a/b,/a/a/d'); // mutate

        // DIVERGE!!
        if (fiwareApi === orionApi) {
          const res = await fiwareApi.rawRequest(config);
          assert.strictEqual(res.status, 200, 'THIS GUY SUCCEEDS!!!');
          const expectedDataObject = {
            orionError: {
              code: '400',
              reasonPhrase: 'Bad Request',
              details: 'too many service paths - a maximum of ten service paths is allowed',
            },
          };
          assert.deepStrictEqual(res.data, expectedDataObject, 'We got a messed up messup here');
        }
        if (fiwareApi === ngsiApi) {
          const err = await assertRejects(fiwareApi.rawRequest(config));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_TOO_MANY_SERVICE_PATHS);
        }
      });

      it('using 11 (valid) service paths FAILS (too many disjointed paths) - like above, but with the wack handler', async () => {
        const config = fiwareApi.getEntities.createRequestConfig();
        _.set(config, ['headers', 'Fiware-ServicePath'], '/a,/b,/c,/d,/a/a,/a/b,/a/c,/a/d,/a/a/a,/a/a/b,/a/a/d'); // mutate
        const err = await assertRejects(fiwareApi.rawRequest(config).then(handleWackyOrionErrors));
        assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_TOO_MANY_SERVICE_PATHS);
      });

      it('using service path (with single duplication) "/a/a,/a/a"', async () => {
        const headers = createServicePathHeader('/a/a,/a/a');
        const res = await fiwareApi.getEntities({headers});
        assert.strictEqual(res.data.length, 1, '"/a/a" has just one inhabitant');
      });

      it('using service path (with 10x duplication) "/a/a," X 10', async () => {
        const paths = _.times(10, () => '/a/a').join(',');
        const headers = createServicePathHeader(paths);
        const res = await fiwareApi.getEntities({headers});
        assert.strictEqual(res.data.length, 1, '"/a/a" has just one inhabitant');
      });

      it('using service path (with 11x duplication) "/a/a," X 11 FAILS (too many disjointeds)', async () => {
        const config = fiwareApi.getEntities.createRequestConfig();
        const paths = _.times(11, () => '/a/a').join(',');
        _.set(config, ['headers', 'Fiware-ServicePath'], paths); // mutate
        const err = await assertRejects(fiwareApi.rawRequest(config).then(handleWackyOrionErrors));
        assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_TOO_MANY_SERVICE_PATHS);
      });

      it('let\'s make the longest one possible (10 X 10 X 50)', async () => {
        const chars = 'abcdefghij'; // 10, same as max(numLevels, numDisjoint)
        const numChars = 50;
        const numLevels = 10;
        const numDisjoint = 10;
        const paths = _.times(numDisjoint, (i) => {
          return '/' + _.times(numLevels, (j) => {
            return _.times(numChars, (k) => {
              return chars.charAt((i + j) % 10);
            }).join('');
          }).join('/');
        }).join(',');
        assert.strictEqual(paths.length, 5109, 'paths should be 5109 chars long');
        const headers = createServicePathHeader(paths);
        const {data: docs} = await fiwareApi.getEntities({headers});
        assert.strictEqual(docs.length, 0, 'there are no docs here');
      });

      it('let\'s make the longest one possible (10 X 10 X 50), then add a single char (should FAIL)', async () => {
        const chars = 'abcdefghij'; // 10, same as max(numLevels, numDisjoint)
        const numChars = 50;
        const numLevels = 10;
        const numDisjoint = 10;
        const paths = _.times(numDisjoint, (i) => {
          return '/' + _.times(numLevels, (j) => {
            return _.times(numChars, (k) => {
              return chars.charAt((i + j) % 10);
            }).join('');
          }).join('/');
        }).join(',');
        const modifiedPaths = `${paths.slice(0, 234)}z${paths.slice(234)}`;
        assert.strictEqual(modifiedPaths.length, 5110, 'paths should be 5110 chars long');
        const config = fiwareApi.getEntities.createRequestConfig();
        _.set(config, ['headers', 'Fiware-ServicePath'], modifiedPaths); // mutate
        const err = await assertRejects(fiwareApi.rawRequest(config).then(handleWackyOrionErrors));
        assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_SERVICE_PATH_COMPONENT_NAME_TOO_LONG);
      });

      it('let\'s make the longest one possible (10 X 10 X 50), then add a single extension "/a" to the last path', async () => {
        const chars = 'abcdefghij'; // 10, same as max(numLevels, numDisjoint)
        const numChars = 50;
        const numLevels = 10;
        const numDisjoint = 10;
        const paths = _.times(numDisjoint, (i) => {
          return '/' + _.times(numLevels, (j) => {
            return _.times(numChars, (k) => {
              return chars.charAt((i + j) % 10);
            }).join('');
          }).join('/');
        }).join(',');
        const modifiedPaths = `${paths}/a`;
        assert.strictEqual(modifiedPaths.length, 5111, 'paths should be 5112 chars long');
        const config = fiwareApi.getEntities.createRequestConfig();
        _.set(config, ['headers', 'Fiware-ServicePath'], modifiedPaths); // mutate
        const err = await assertRejects(fiwareApi.rawRequest(config).then(handleWackyOrionErrors));
        assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_SERVICE_PATH_TOO_MANY_COMPONENTS);
      });

      it('let\'s make the longest one possible (10 X 10 X 50), then add a single disjoint path "/a"', async () => {
        const chars = 'abcdefghij'; // 10, same as max(numLevels, numDisjoint)
        const numChars = 50;
        const numLevels = 10;
        const numDisjoint = 10;
        const paths = _.times(numDisjoint, (i) => {
          return '/' + _.times(numLevels, (j) => {
            return _.times(numChars, (k) => {
              return chars.charAt((i + j) % 10);
            }).join('');
          }).join('/');
        }).join(',');
        const modifiedPaths = `${paths},/a`;
        assert.strictEqual(modifiedPaths.length, 5112, 'paths should be 5112 chars long');
        const config = fiwareApi.getEntities.createRequestConfig();
        _.set(config, ['headers', 'Fiware-ServicePath'], modifiedPaths); // mutate
        const err = await assertRejects(fiwareApi.rawRequest(config).then(handleWackyOrionErrors));
        assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_TOO_MANY_SERVICE_PATHS);
      });

      it('what happens with path "/a,,/a/a" (duplicate separators in middle) WORKS (NOTE that the [empty string] between the commas does NOT get expanded into "/")!!', async () => {
        const paths = '/a,,/a/a';
        const config = fiwareApi.getEntities.createRequestConfig();
        _.set(config, ['headers', 'Fiware-ServicePath'], paths); // mutate

        // DIVERGE!!
        if (fiwareApi === orionApi) {
          const {data: docs} = await fiwareApi.rawRequest(config);
          assert.strictEqual(docs.length, 2, 'should be 2 docs');
        }
        if (fiwareApi === ngsiApi) {
          const err = await assertRejects(fiwareApi.rawRequest(config));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_SERVICE_PATH_EMPTY);
        }
      });

      it('what happens with path "/a," (single separator at the end) WORKS!!', async () => {
        const paths = '/a,';
        const config = fiwareApi.getEntities.createRequestConfig();
        _.set(config, ['headers', 'Fiware-ServicePath'], paths); // mutate

        // DIVERGE!!
        if (fiwareApi === orionApi) {
          const {data: docs} = await fiwareApi.rawRequest(config);
          assert.strictEqual(docs.length, 1, 'should be 1 docs');
        }
        if (fiwareApi === ngsiApi) {
          const err = await assertRejects(fiwareApi.rawRequest(config));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_SERVICE_PATH_EMPTY);
        }
      });

      it('what happens with path "/a,,,,,,,,,/a/b" (9 separators in the middle) WORKS!!', async () => {
        const paths = '/a,,,,,,,,,/a/b';
        const config = fiwareApi.getEntities.createRequestConfig();
        _.set(config, ['headers', 'Fiware-ServicePath'], paths); // mutate

        // DIVERGE!!
        if (fiwareApi === orionApi) {
          const {data: docs} = await fiwareApi.rawRequest(config);
          assert.strictEqual(docs.length, 2, 'should be 2 docs');
        }
        if (fiwareApi === ngsiApi) {
          const err = await assertRejects(fiwareApi.rawRequest(config));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_SERVICE_PATH_EMPTY);
        }
      });

      it('what happens with path "/a,,,,,,,,,,/a/b" (10 separators in the middle) FAILS!!', async () => {
        const paths = '/a,,,,,,,,,,/a/b';
        const config = fiwareApi.getEntities.createRequestConfig();
        _.set(config, ['headers', 'Fiware-ServicePath'], paths); // mutate
        const err = await assertRejects(fiwareApi.rawRequest(config).then(handleWackyOrionErrors));
        assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_TOO_MANY_SERVICE_PATHS);
      });

      it('what happens with path "/a,,,,,,,,,/a/b," (9 separators in the middle and then one at the end) FAILS!!', async () => {
        const paths = '/a,,,,,,,,,/a/b,';
        const config = fiwareApi.getEntities.createRequestConfig();
        _.set(config, ['headers', 'Fiware-ServicePath'], paths); // mutate
        const err = await assertRejects(fiwareApi.rawRequest(config).then(handleWackyOrionErrors));
        assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_TOO_MANY_SERVICE_PATHS);
      });

      it('what happens with path ",/a,,,,,,,,,/a/b" (9 separators in the middle and then one at the front) WORKS(?)!!', async () => {
        const paths = ',/a,,,,,,,,,/a/b';
        const config = fiwareApi.getEntities.createRequestConfig();
        _.set(config, ['headers', 'Fiware-ServicePath'], paths); // mutate

        // DIVERGE!!
        if (fiwareApi === orionApi) {
          const {data: docs} = await fiwareApi.rawRequest(config);
          assert.strictEqual(docs.length, 2, 'should be 2 docs');
        }
        if (fiwareApi === ngsiApi) {
          const err = await assertRejects(fiwareApi.rawRequest(config));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_TOO_MANY_SERVICE_PATHS);
        }
      });

      it('what happens with path ",,/a,,,,,,,,,/a/b" (9 separators in the middle and then 2 at the front) WORKS(?)!!', async () => {
        const paths = ',,/a,,,,,,,,,/a/b';
        const config = fiwareApi.getEntities.createRequestConfig();
        _.set(config, ['headers', 'Fiware-ServicePath'], paths); // mutate

        // DIVERGE!!
        if (fiwareApi === orionApi) {
          const {data: docs} = await fiwareApi.rawRequest(config);
          assert.strictEqual(docs.length, 2, 'should be 2 docs');
        }
        if (fiwareApi === ngsiApi) {
          const err = await assertRejects(fiwareApi.rawRequest(config));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_TOO_MANY_SERVICE_PATHS);
        }
      });

      it('what happens with path ",,,,,,,,,,,,,,,,,,,/a,,,,,,,,,/a/b" (9 separators in the middle and then 20 at the front) WORKS(?) -- LEADING COMMAS GET STRIPPED!!', async () => {
        const paths = ',,,,,,,,,,,,,,,,,,,/a,,,,,,,,,/a/b';
        const config = fiwareApi.getEntities.createRequestConfig();
        _.set(config, ['headers', 'Fiware-ServicePath'], paths); // mutate

        // DIVERGE!!
        if (fiwareApi === orionApi) {
          const {data: docs} = await fiwareApi.rawRequest(config);
          assert.strictEqual(docs.length, 2, 'should be 2 docs');
        }
        if (fiwareApi === ngsiApi) {
          const err = await assertRejects(fiwareApi.rawRequest(config));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_TOO_MANY_SERVICE_PATHS);
        }
      });

      it('what happens with path "," (just the comma) -- CAUSES ORION TO HANG!!', async () => {
        const paths = ',';
        const config = fiwareApi.getEntities.createRequestConfig();
        _.set(config, ['headers', 'Fiware-ServicePath'], paths); // mutate

        // DIVERGE!!
        if (fiwareApi === orionApi) {
          // JUST NEVER EVER RUN THIS: const {data: docs} = await fiwareApi.rawRequest(config);
        }
        if (fiwareApi === ngsiApi) {
          const err = await assertRejects(fiwareApi.rawRequest(config));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_SERVICE_PATH_EMPTY);
        }
      });

      it('what happens with path ",,,,,,,,," (just the 9 commas) -- CAUSES ORION TO HANG!!', async () => {
        const paths = _.times(9, () => ',').join('');
        const config = fiwareApi.getEntities.createRequestConfig();
        _.set(config, ['headers', 'Fiware-ServicePath'], paths); // mutate

        // DIVERGE!!
        if (fiwareApi === orionApi) {
          // JUST NEVER EVER RUN THIS: const {data: docs} = await fiwareApi.rawRequest(config);
        }
        if (fiwareApi === ngsiApi) {
          const err = await assertRejects(fiwareApi.rawRequest(config));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_SERVICE_PATH_EMPTY);
        }
      });

      it('what happens with path ",,,,,,,,,," (exactly 10 commas) -- CAUSES ORION TO HANG!!', async () => {
        const paths = _.times(10, () => ',').join('');
        const config = fiwareApi.getEntities.createRequestConfig();
        _.set(config, ['headers', 'Fiware-ServicePath'], paths); // mutate

        // DIVERGE!!
        if (fiwareApi === orionApi) {
          // JUST NEVER EVER RUN THIS: const {data: docs} = await fiwareApi.rawRequest(config);
        }
        if (fiwareApi === ngsiApi) {
          const err = await assertRejects(fiwareApi.rawRequest(config));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_TOO_MANY_SERVICE_PATHS);
        }
      });

      it('what happens with path ",,,,,,,,," (finally 11 (>10) commas) -- CAUSES ORION TO HANG - DO NOT UNSKIP!!', async () => {
        const paths = _.times(11, () => ',').join('');
        const config = fiwareApi.getEntities.createRequestConfig();
        _.set(config, ['headers', 'Fiware-ServicePath'], paths); // mutate

        // DIVERGE!!
        if (fiwareApi === orionApi) {
          // JUST NEVER EVER RUN THIS: const {data: docs} = await fiwareApi.rawRequest(config);
        }
        if (fiwareApi === ngsiApi) {
          const err = await assertRejects(fiwareApi.rawRequest(config));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_TOO_MANY_SERVICE_PATHS);
        }
      });

    });

  });

});
