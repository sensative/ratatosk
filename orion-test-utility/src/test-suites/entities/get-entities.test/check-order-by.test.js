/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const serialPromise = require('promise-serial-exec');

const {
  assertRejects,
  ngsiResponseErrorUtils: {assertErrorsMatch},
  // ngsiHeaderUtils: {createServicePathHeader},
  ngsiEntityFormatter: {formatEntity},
} = require('@ratatosk/ngsi-utils');

const {ORION_ERRORS} = require('@ratatosk/ngsi-constants');

const {orionApi, ngsiApi} = require('../../../client-api-util');
const mongoApi = require('../../../mongo-api');

describe('check-order-by', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      before(async () => {
        await mongoApi.assertEntitiesEmpty();
      });
      after(async () => mongoApi.wipeEntities());

      describe('the basics', () => {

        const entityTemplates = [
          {id: 'arp1', dex: 3},
          {id: 'arp2', dex: -1},
          {id: 'arp3'},
          {id: 'arp4', dex: 2},
        ];

        before(async () => {
          await mongoApi.assertEntitiesEmpty();
          await serialPromise(_.map(entityTemplates, (entity) => async () => {
            return fiwareApi.createEntity({entity: formatEntity(entity)});
          }));
        });

        after(async () => {
          await mongoApi.wipeEntities();
        });

        it('by default, the order is by dateCreated but the dates are probably the same so kinda cheesy', async () => {
          const params = {idPattern: 'arp', attrs: 'dateCreated,dex'};
          const {data: docs} = await fiwareApi.getEntities({params});
          const names = _.map(docs, (doc) => doc.id);
          const expected = ['arp1', 'arp2', 'arp3', 'arp4'];
          assert.deepStrictEqual(names, expected, 'incorrect order');
        });

        it('orderBy "dex" places non-"dex" first', async () => {
          const params = {idPattern: 'arp', orderBy: 'dex'};
          const {data: docs} = await fiwareApi.getEntities({params});
          const names = _.map(docs, (doc) => doc.id);
          const expected = ['arp3', 'arp2', 'arp4', 'arp1'];
          assert.deepStrictEqual(names, expected, 'incorrect order');
        });

        it('orderBy "!dex" places non-"dex" first DOES reverse order for "dex"', async () => {
          const params = {idPattern: 'arp', orderBy: '!dex'};
          const {data: docs} = await fiwareApi.getEntities({params});
          const names = _.map(docs, (doc) => doc.id);
          const expected = ['arp1', 'arp4', 'arp2', 'arp3'];
          assert.deepStrictEqual(names, expected, 'incorrect order');
        });

      });

      describe('check if it is possible to dig into attr.value structure. A: NO THEY DO NOT', () => {

        const entityTemplates = [
          {id: 'arp1', dex: {a: 3}},
          {id: 'arp2', dex: {a: -2}},
          {id: 'arp3'},
          {id: 'arp4', dex: {a: 1}},
        ];

        before(async () => {
          await mongoApi.assertEntitiesEmpty();
          await serialPromise(_.map(entityTemplates, (entity) => async () => {
            return fiwareApi.createEntity({entity: formatEntity(entity)});
          }));
        });

        after(async () => {
          await mongoApi.wipeEntities();
        });

        it('orderBy "dex.a" matches the "dex.a" attr but STILL does not work. IGNORED', async () => {
          const config = fiwareApi.getEntities.createRequestConfig({params: {orderBy: 'placeholder'}});
          // MUTATE !! (dex.a does not validate)
          _.set(config, 'params.orderBy', 'dex.a');

          // DIVERGE!!
          if (fiwareApi === orionApi) {
            const {data: docs} = await fiwareApi.rawRequest(config);
            const names = _.map(docs, (doc) => doc.id);
            const expectedWithFailure = ['arp1', 'arp2', 'arp3', 'arp4'];
            const expectedIfItWorked = ['arp3', 'arp2', 'arp4', 'arp1'];
            assert.deepStrictEqual(names, expectedWithFailure, 'incorrect order');
            assert.notDeepStrictEqual(names, expectedIfItWorked, 'incorrect order');
          }
          if (fiwareApi === ngsiApi) {
            const err = await assertRejects(fiwareApi.rawRequest(config));
            assertErrorsMatch(err.response, ORION_ERRORS.BAD_REQ_ORDERBY_PARAM_INVALID_ITEM);
          }
        });

      });

      describe('order by multiple arguments', () => {

        const entityTemplates = [
          {id: 'be1', dax: 3, den: 'jokko'},
          {id: 'be2', dax: -1, den: 'addy'},
          {id: 'be3', den: 'johnnz'},
          {id: 'be4', dax: 2, den: 'johnny'},
          {id: 'be5', dax: 2, den: 'johnnz'},
          {id: 'be6', dax: 2, den: 'joz'},
          {id: 'be7', dax: 2, den: 'hoz'},
        ];

        before(async () => {
          await mongoApi.assertEntitiesEmpty();
          await serialPromise(_.map(entityTemplates, (entity) => async () => {
            return fiwareApi.createEntity({entity: formatEntity(entity)});
          }));
        });

        after(async () => {
          await mongoApi.wipeEntities();
        });

        it('orderBy "dax,den"', async () => {
          const params = {orderBy: 'dax,den'};
          const {data: docs} = await fiwareApi.getEntities({params});
          const names = _.map(docs, (doc) => doc.id);
          const expected = ['be3', 'be2', 'be7', 'be4', 'be5', 'be6', 'be1'];
          assert.deepStrictEqual(names, expected, 'incorrect order');
        });

        it('orderBy "dax,!den"', async () => {
          const params = {orderBy: 'dax,!den'};
          const {data: docs} = await fiwareApi.getEntities({params});
          const names = _.map(docs, (doc) => doc.id);
          const expected = ['be3', 'be2', 'be6', 'be5', 'be4', 'be7', 'be1'];
          assert.deepStrictEqual(names, expected, 'incorrect order');
        });

        it('orderBy "!dax,!den"', async () => {
          const params = {orderBy: '!dax,!den'};
          const {data: docs} = await fiwareApi.getEntities({params});
          const names = _.map(docs, (doc) => doc.id);
          const expected = ['be1', 'be6', 'be5', 'be4', 'be7', 'be2', 'be3'];
          assert.deepStrictEqual(names, expected, 'incorrect order');
        });

        it('orderBy "den,!dax"', async () => {
          const params = {orderBy: 'den,!dax'};
          const {data: docs} = await fiwareApi.getEntities({params});
          const names = _.map(docs, (doc) => doc.id);
          const expected = ['be2', 'be7', 'be4', 'be5', 'be3', 'be1', 'be6'];
          assert.deepStrictEqual(names, expected, 'incorrect order');
        });

        it('duplicate items are allowed in Orion, but not ngsi-server', async () => {
          const dupes = [
            'den,den',
            'den,!den',
            '!den,dat,!den',
            'den,,dat,',
            ',',
            ',,',
          ];
          const config = fiwareApi.getEntities.createRequestConfig({params: {orderBy: 'placeholder'}});
          await serialPromise(_.map(dupes, (dupe) => async () => {
            _.set(config, 'params.orderBy', dupe); // mutate/!!

            // DIVERGE!!
            if (fiwareApi === orionApi) {
              const {data: docs} = await fiwareApi.rawRequest(config);
              assert.strictEqual(docs.length, 7, 'should have 7 docs');
            }
            if (fiwareApi === ngsiApi) {
              const err = await assertRejects(fiwareApi.rawRequest(config));
              assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_ORDERBY_PARAM_DUPLICATE);
            }
          }));
        });

        it('invalid attrs (that are not forbidden by respective field syntax restrictions)', async () => {
          const dupes = [
            '!',
            ',den',
            'den,',
          ];
          const config = fiwareApi.getEntities.createRequestConfig({params: {orderBy: 'placeholder'}});
          await serialPromise(_.map(dupes, (dupe) => async () => {
            _.set(config, 'params.orderBy', dupe); // mutate/!!

            // DIVERGE!!
            if (fiwareApi === orionApi) {
              const {data: docs} = await fiwareApi.rawRequest(config);
              assert.strictEqual(docs.length, 7, 'should have 7 docs');
            }
            if (fiwareApi === ngsiApi) {
              const err = await assertRejects(fiwareApi.rawRequest(config));
              assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_ORDERBY_PARAM_INVALID_ITEM);
            }
          }));
        });

        it('some varieties of orderBy params that DO work all around', async () => {
          const valids = [
            'id',
            'type',
            'dateCreated',
            'dateModified',
            'den',
            'derp',
            'den,derp',
            '!den,!derp',
            '!!',
            '!!!den',
          ];
          const config = fiwareApi.getEntities.createRequestConfig({params: {orderBy: 'placeholder'}});
          await serialPromise(_.map(valids, (valid) => async () => {
            _.set(config, 'params.orderBy', valid); // mutate!!
            const {data: docs} = await fiwareApi.rawRequest(config);
            assert.strictEqual(docs.length, 7, 'should have 7 docs');
          }));
        });

        it('orderBy "den,!dax,"', async () => {
          const params = {orderBy: 'den,!dax'};
          const {data: docs} = await fiwareApi.getEntities({params});
          const names = _.map(docs, (doc) => doc.id);
          const expected = ['be2', 'be7', 'be4', 'be5', 'be3', 'be1', 'be6'];
          assert.deepStrictEqual(names, expected, 'incorrect order');
        });

        it('orderBy "den,dax,!den" NOTE: compare to just "den,dax". If a value repeats, the FIRST WINS', async () => {
          // DIVERGE!! (this one is obviously ridiculous for ngsi-server (has dupes))
          if (fiwareApi === orionApi) {
            const params = {orderBy: 'placeholder'};
            const config = fiwareApi.getEntities.createRequestConfig({params});
            // MUTATE (to allow otherwise invalid orderBy arg);
            _.set(config, 'params.orderBy', 'den,dax,!den');
            const {data: docs} = await fiwareApi.rawRequest(config);
            const namesDDD = _.map(docs, (doc) => doc.id);
            // MUTATE (to allow otherwise invalid orderBy arg);
            _.set(config, 'params.orderBy', 'dax,!den');
            const {data: docs2} = await fiwareApi.rawRequest(config);
            const namesDaxNDen = _.map(docs2, (doc) => doc.id);
            // MUTATE (to allow otherwise invalid orderBy arg);
            _.set(config, 'params.orderBy', 'den,dax');
            const {data: docs3} = await fiwareApi.rawRequest(config);
            const namesDenDax = _.map(docs3, (doc) => doc.id);
            // MUTATE (to allow otherwise invalid orderBy arg);
            _.set(config, 'params.orderBy', 'dax');
            const {data: docs4} = await fiwareApi.rawRequest(config);
            const namesDax = _.map(docs4, (doc) => doc.id);
            // and check
            assert.deepStrictEqual(namesDDD, namesDenDax, 'should match "den,dax"');
            assert.notDeepStrictEqual(namesDDD, namesDaxNDen, 'should NOT match "dax,!den"');
            assert.notDeepStrictEqual(namesDDD, namesDax, 'should NOT match "dax"');
          }
        });

        it('orderBy "dax,!!!den"', async () => {
          const params = {orderBy: 'placeholder'};
          const config = fiwareApi.getEntities.createRequestConfig({params});
          // MUTATE (to allow otherwise invalid orderBy arg);
          _.set(config, 'params.orderBy', 'dax,!!!den');
          const {data: docs} = await fiwareApi.rawRequest(config);
          const names = _.map(docs, (doc) => doc.id);
          const paramsDax = {orderBy: 'dax'};
          const {data: docsDax} = await fiwareApi.getEntities({params: paramsDax});
          const namesDax = _.map(docsDax, (doc) => doc.id);
          assert.deepStrictEqual(names, namesDax, 'yep, "!!!den" just gets ignored');
        });

        it('orderBy "den,!den"', async () => {
          // DIVERGE!! (this one is obviously ridiculous for ngsi-server (has dupes))
          if (fiwareApi === orionApi) {
            const params = {orderBy: 'placeholder'};
            const config = fiwareApi.getEntities.createRequestConfig({params});
            // MUTATE (to allow otherwise invalid orderBy arg);
            _.set(config, 'params.orderBy', 'den,!den');
            const {data: docs} = await fiwareApi.rawRequest(config);
            const names = _.map(docs, (doc) => doc.id);
            const expected = ['be2', 'be7', 'be4', 'be3', 'be5', 'be1', 'be6'];
            assert.deepStrictEqual(names, expected, 'incorrect order');
            // 'den,!den' should be identical to 'den'
            const paramsDen = {orderBy: 'den'};
            const {data: docsDen} = await fiwareApi.getEntities({params: paramsDen});
            const namesDen = _.map(docsDen, (doc) => doc.id);
            assert.deepStrictEqual(names, namesDen, 'The last den wins');
          }
        });

        it('orderBy "!den,den"', async () => {
          // DIVERGE!! (this one is obviously ridiculous for ngsi-server (has dupes))
          if (fiwareApi === orionApi) {
            const params = {orderBy: 'placeholder'};
            const config = fiwareApi.getEntities.createRequestConfig({params});
            // MUTATE (to allow otherwise invalid orderBy arg);
            _.set(config, 'params.orderBy', '!den,den');
            const {data: docs} = await fiwareApi.rawRequest(config);
            const names = _.map(docs, (doc) => doc.id);
            const expected = ['be6', 'be1', 'be3', 'be5', 'be4', 'be7', 'be2'];
            assert.deepStrictEqual(names, expected, 'incorrect order');
            // '!den,den' should be identical to '!den'
            const paramsDen = {orderBy: '!den'};
            const {data: docsDen} = await fiwareApi.getEntities({params: paramsDen});
            const namesDen = _.map(docsDen, (doc) => doc.id);
            assert.deepStrictEqual(names, namesDen, 'The last den wins');
          }
        });

      });

      describe('some failures', () => {

        it('empty string returns the normal sort of thing', async () => {
          const config = fiwareApi.getEntities.createRequestConfig();
          _.set(config, 'params.orderBy', ''); // mutate!!
          const err = await assertRejects(fiwareApi.rawRequest(config));
          assertErrorsMatch(err.response, ORION_ERRORS.BAD_REQ_EMPTY_ORDERBY_PARAM);
        });

        it('illegal chars throws too (some only in orion)', async () => {
          const invalids = [
            'has_a_;',
            'has_a_"',
          ];
          const config = fiwareApi.getEntities.createRequestConfig();
          await serialPromise(_.map(invalids, (invalid) => async () => {
            _.set(config, 'params.orderBy', invalid); // mutate!!

            // DIVERGE!!
            if (fiwareApi === orionApi) {
              const err = await assertRejects(fiwareApi.rawRequest(config));
              assertErrorsMatch(err.response, ORION_ERRORS.BAD_REQ_INVALID_CHAR_IN_PARAM);
            }
            if (fiwareApi === ngsiApi) {
              const {data: docs} = await fiwareApi.rawRequest(config);
              assert.strictEqual(docs.length, 0);
            }
          }));
        });

      });

    });

  });

});
