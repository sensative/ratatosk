/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const serialPromise = require('promise-serial-exec');

const {
  assertRejects,
  ngsiResponseErrorUtils: {assertErrorsMatch},
} = require('@ratatosk/ngsi-utils');

const {ORION_ERRORS} = require('@ratatosk/ngsi-constants');

const {orionApi, ngsiApi} = require('../../../client-api-util');
const mongoApi = require('../../../mongo-api');

const entities = [
  {id: 'derp', type: 'Andy'},
  {id: 'smerp', type: 'Mandy'},
];

describe('check-entity-type (filtering by type should work as expected)', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      before(async () => {
        await mongoApi.assertEntitiesEmpty();
        // create some entities to check against
        await serialPromise(_.map(entities, (entity) => async () => {
          return fiwareApi.createEntity({entity});
        }));
      });
      after(async () => mongoApi.wipeEntities());

      it('should find all entities without a type filter', async () => {
        const {data: docs} = await fiwareApi.getEntities();
        assert.strictEqual(docs.length, 2, 'wrong number of docs');
      });

      it('should only find one entity of type "Mandy"', async () => {
        const params = {type: 'Mandy'};
        const {data: docs} = await fiwareApi.getEntities({params});
        assert.strictEqual(docs.length, 1, 'should only find the one Mandy');
        const expected = {id: 'smerp', type: 'Mandy'};
        assert.deepStrictEqual(docs[0], expected, 'doc looks incorrect');
      });

      it('should find no entities if the desired type does not exist', async () => {
        const params = {type: 'NoTypeOfThisKind'};
        const {data: docs} = await fiwareApi.getEntities({params});
        assert.strictEqual(docs.length, 0, 'should not find any entities');
      });

      it('an empty "type" arg FAILS', async () => {
        const config = fiwareApi.getEntities.createRequestConfig();
        _.set(config, 'params.type', ''); // mutate!
        const err = await assertRejects(fiwareApi.rawRequest(config));
        assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_EMPTY_TYPE_PARAM);
      });

      it('test lists', async () => {
        const params = {type: 'Andy,Mandy'};
        const {data: docs} = await fiwareApi.getEntities({params});
        assert.strictEqual(docs.length, 2);
      });

      it('using a list of WRAPPED types', async () => {
        const params = {type: '\'Andy\',\'Mandy\''};
        // DIVERGE!! (this really SHOULD work - for the sake of consistency)
        if (fiwareApi === orionApi) {
          const err = await assertRejects(fiwareApi.getEntities({params}));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_INVALID_CHAR_IN_PARAM);
        }
        if (fiwareApi === ngsiApi) {
          const {data: docs} = await fiwareApi.getEntities({params});
          assert.strictEqual(docs.length, 2);
        }
      });

    });

  });

});
