/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');

const {
  assertRejects,
  ngsiResponseErrorUtils: {assertErrorsMatch},
} = require('@ratatosk/ngsi-utils');

const {ORION_ERRORS} = require('@ratatosk/ngsi-constants');

const {orionApi, ngsiApi} = require('../../../client-api-util');
const mongoApi = require('../../../mongo-api');

describe('check entity representations', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      before(async () => {
        await mongoApi.assertEntitiesEmpty();
        // create some entities to check against
        const entity = {
          id: 'corpo',
          type: 'gerpy',
          agda: 'gerbil',
          bo: 73,
          jerks: {type: 'Norbos', value: 73},
          niblets: {type: 'pork', value: null},
          trinkets: 'gerbil',
        };
        await fiwareApi.createEntity({entity});
      });
      after(async () => mongoApi.wipeEntities());

      it('1 - get ["agda", "missing", "bo"] with no option mode', async () => {
        const attrs = ['agda', 'missing', 'bo'];
        const res = await fiwareApi.getEntities({params: {attrs}});
        const docs = res.data;
        assert.strictEqual(docs.length, 1, 'wrong number of entities');
        const doc = docs[0];
        const expected = {
          id: 'corpo',
          type: 'gerpy',
          agda: {type: 'Text', value: 'gerbil', metadata: {}},
          bo: {type: 'Number', value: 73, metadata: {}},
        };
        assert.deepStrictEqual(expected, doc, 'filtered doc looks wrong (no options mode)');
      });

      it('1.1 - get ["agda", "missing", "bo"] with "normalized" option mode (same as no options)', async () => {
        const attrs = ['agda', 'missing', 'bo'];
        const res = await fiwareApi.getEntities({params: {attrs, options: 'normalized'}});
        const docs = res.data;
        assert.strictEqual(docs.length, 1, 'wrong number of entities');
        const doc = docs[0];
        const expected = {
          id: 'corpo',
          type: 'gerpy',
          agda: {type: 'Text', value: 'gerbil', metadata: {}},
          bo: {type: 'Number', value: 73, metadata: {}},
        };
        assert.deepStrictEqual(expected, doc, 'filtered doc looks wrong (no options mode)');

      });

      it('2- get ["agda", "missing", "bo"] with KEY_VALUES option mode', async () => {
        const attrs = ['agda', 'missing', 'bo'];
        const res = await fiwareApi.getEntities({params: {attrs, options: 'keyValues'}});
        const docs = res.data;
        assert.strictEqual(docs.length, 1, 'wrong number of entities');
        const doc = docs[0];
        const expected = {
          id: 'corpo',
          type: 'gerpy',
          agda: 'gerbil',
          bo: 73,
        };
        assert.deepStrictEqual(doc, expected, 'filtered doc looks wrong (KEY_VALUES options mode)');
      });

      it('3 - get ["agda", "missing", "bo"] with VALUES option mode', async () => {
        const attrs = ['agda', 'missing', 'bo'];
        const res = await fiwareApi.getEntities({
          params: {attrs, options: 'values'},
        });
        const docs = res.data;
        assert.strictEqual(docs.length, 1, 'wrong number of entities');
        const doc = docs[0];
        const expected = ['gerbil', 73]; // it just omits that value!! shifts indices!!
        assert.deepStrictEqual(expected, doc, 'filtered doc looks wrong (VALUES options mode)');
      });

      it('4 - get ["agda", "missing", "bo"] with UNIQUE option mode', async () => {
        const attrs = ['agda', 'missing', 'bo', 'jerks'];
        const params = {attrs, options: 'unique'};
        const res = await fiwareApi.getEntities({params});
        const docs = res.data;
        assert.strictEqual(docs.length, 1, 'wrong number of entities');
        const doc = docs[0];
        const expected = ['gerbil', 73]; // it just omits that value!! shifts indices!!
        assert.deepStrictEqual(expected, doc, 'filtered doc looks wrong (UNIQUE options mode)');
      });

      it('5 - getting entities with <<INVALID>> option mode does NOT work', async () => {
        const config = fiwareApi.getEntities.createRequestConfig({params: {options: 'values'}});
        _.set(config, 'params.options', 'eeerp-invalid'); // mutate!!
        const err = await assertRejects(fiwareApi.rawRequest(config));
        assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_INVALID_OPTIONS_PARAM);
      });

      it('6 - get ["agda", "missing", "bo"] with invalid <<INVALID>> option (illegal character) does NOT work', async () => {
        const config = fiwareApi.getEntities.createRequestConfig({params: {options: 'count'}});
        _.set(config, 'params.options', 'illegal;char');

        // DIVERGENCE (but expected)
        if (fiwareApi === orionApi) {
          const err = await assertRejects(fiwareApi.rawRequest(config));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_INVALID_CHAR_IN_PARAM);
        }
        if (fiwareApi === ngsiApi) {
          const err = await assertRejects(fiwareApi.rawRequest(config));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_INVALID_OPTIONS_PARAM);
        }
      });

      it('check extended display', async () => {
        const params = {
          representationType: 'extended',
          attrs: ['niblets', 'fiwareId'],
          metadata: ['remove_timestamps'],
        };

        // DIVERGE!!
        if (fiwareApi === orionApi) {
          const err = await assertRejects(fiwareApi.getEntities({params}));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_INVALID_OPTIONS_PARAM);
        }
        if (fiwareApi === ngsiApi) {
          const {data: [doc]} = await fiwareApi.getEntities({params});
          const [mongoDoc] = await mongoApi.findEntities();
          assert.strictEqual(mongoDoc._id.constructor.name, 'ObjectID');
          const expected = {
            id: 'corpo',
            type: 'gerpy',
            servicePath: '/',
            builtinAttrs: {
              fiwareId: {
                type: 'Text',
                value: mongoDoc._id.toString(),
              },
            },
            attrs: {
              niblets: {
                type: 'pork',
                value: null,
                metadata: {},
                builtinMetadata: {},
              },
            },
          };
          assert.deepStrictEqual(doc, expected);
        }
      });

      it('fiwareId retrieves the objectID for the entity (ngsi only) with keyValues', async () => {
        const params = {
          representationType: 'keyValues',
          attrs: ['niblets', 'fiwareId'],
          metadata: ['remove_timestamps'],
        };
        const {data: [doc]} = await fiwareApi.getEntities({params});
        const expected = {
          id: 'corpo',
          type: 'gerpy',
          niblets: null,
        };

        // DIVERGE!!
        if (fiwareApi === orionApi) {
          assert.deepStrictEqual(doc, expected);
        }
        if (fiwareApi === ngsiApi) {
          const [mongoDoc] = await mongoApi.findEntities();
          assert.strictEqual(mongoDoc._id.constructor.name, 'ObjectID');
          const fullExpected = _.assign(
            {fiwareId: mongoDoc._id.toString()},
            expected,
          );
          assert.deepStrictEqual(doc, fullExpected);
        }
      });

    });

  });

});
