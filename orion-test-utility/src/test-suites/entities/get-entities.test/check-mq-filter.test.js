/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

// This one will primarily aim to investigate how sql relates to
// metadata specifically. It does not aim to specify and validate
// the behavior of the sql implementation in general

// It SHOULD

const _ = require('lodash');
const assert = require('assert');
const serialPromise = require('promise-serial-exec');

const {
  assertRejects,
  ngsiResponseErrorUtils: {assertErrorsMatch},
} = require('@ratatosk/ngsi-utils');

const {ORION_ERRORS} = require('@ratatosk/ngsi-constants');

const {orionApi, ngsiApi} = require('../../../client-api-util');
const mongoApi = require('../../../mongo-api');

const entities = [
  // metaderps
  {
    id: 'oberp',
    type: 'Objecty',
    derp: {
      value: 0,
      metadata: {
        metaderp: {
          subderp: {a: {b: [0, 1, {c: 2.3}, {'': {'': {d: 1.3}}}]}},
          '': {'': 2},
        },
      },
    },
  },
  {id: 'alerp', derp: {value: 0, metadata: {metaderp: 'aaa222_true'}}},
  {id: 'blerp', derp: {value: 0, metadata: {metaderp: 'bbb'}}},
  {id: 'clerp', derp: {value: 0, metadata: {metaderp: 'ccc_null'}}},
  {id: 'querp', derp: {value: 0, metadata: {metaderp: {type: 'Wert', value: null}}}},
  {id: 'verp', derp: {value: 0, metadata: {metaderp: true}}},
  {id: 'zerp', derp: {value: 0, metadata: {metaderp: false}}},
  {id: 'nerp', derp: {value: 0, metadata: {metaderp: 1}}},
  {id: 'derp', derp: {value: 0, metadata: {metaderp: 2}}},
  {id: 'smerp', derp: {value: 0, metadata: {metaderp: 4}}},
  {id: 'knerp', derp: {value: 0, metadata: {metaderp: {'': 2}}}},
  // metanavs!!
  {id: 'lerp', derp: {value: 0, metadata: {metanav: 'car'}}},
  {id: 'sterp', derp: {value: 0, metadata: {metanav: 'groovy'}}},
  {id: 'harp', derp: {value: 0, metadata: {metanav: 'maven'}}},
];

// just some convenience metadata extraction utility functions
// --- just for local usage ----
const getDerpMetadata = (doc) => _.get(doc, 'derp.metadata.metaderp.value');
const getMultiDerpMetadatas = (docs) => _.map(docs, getDerpMetadata);


describe('check-mq-filter', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      before(async () => {
        await mongoApi.assertEntitiesEmpty();
        await serialPromise(_.map(entities, (entity) => async () => {
          return fiwareApi.createEntity({entity});
        }));
      });
      after(async () => mongoApi.wipeEntities());

      describe('basic stuff', () => {

        it('The most simple valid example: "derp.metaderp"', async () => {
          const params = {mq: 'derp.metaderp'};
          const config = {...fiwareApi.getEntities.createRequestConfig(), params};
          const {data: docs} = await fiwareApi.rawRequest(config);
          assert.strictEqual(docs.length, 11, 'should be 11 "derp.metaderp"s');
        });

        it('leading and trailing sql statement separators (;) are allowed ONLY for Orion, else fail', async () => {
          const invalids = [
            'derp.metaderp;',
            ';derp.metaderp',
            ';derp.metaderp;',
          ];
          const config = fiwareApi.getEntities.createRequestConfig();
          await serialPromise(_.map(invalids, (invalid) => async () => {
            _.set(config, 'params.mq', invalid); // mutate!!

            // DIVERGE!!
            if (fiwareApi === orionApi) {
              const {data: docs} = await fiwareApi.rawRequest(config);
              assert.strictEqual(docs.length, 11);
            }
            if (fiwareApi === ngsiApi) {
              const err = await assertRejects(fiwareApi.rawRequest(config));
              assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_EMPTY_Q_ITEM);
            }

          }));
        });

        it('Check how trailing dots get treated (i.e. derp.metaderp..)', async () => {
          const tokensList = [
            {in: 'derp.metaderp.', numOrion: 11, numNgsi: 2},
            {in: 'derp.metaderp..', numOrion: 1, numNgsi: 1},
            {in: 'derp.metaderp.subderp.a.b.3..', numOrion: 1, numNgsi: 1},
          ];
          await serialPromise(_.map(tokensList, (tokens) => async () => {
            const params = {mq: tokens.in};
            const config = {...fiwareApi.getEntities.createRequestConfig(), params};
            // DIVERGE!!
            const {data: docs} = await fiwareApi.rawRequest(config);
            if (fiwareApi === orionApi) {
              assert.strictEqual(docs.length, tokens.numOrion);
            }
            if (fiwareApi === ngsiApi) {
              assert.strictEqual(docs.length, tokens.numNgsi);
            }
          }));
        });

        it('Fails with "metaderp" as it does not have at least two path items (attr & md)', async () => {
          const params = {mq: 'metaderp'};
          assertRejects(() => fiwareApi.getEntities.createRequestConfig({params}));
          const config = fiwareApi.getEntities.createRequestConfig();
          _.set(config, 'params', params);
          const err = await assertRejects(fiwareApi.rawRequest(config));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_NO_METADATA_AT_ALL);
        });

        it('invalid chars not allowed (ngsi) but ALLOWED in Orion', async () => {
          const invalids = [
            'ha#sh.valid',
            'sp ce.valid',
          ];
          const config = fiwareApi.getEntities.createRequestConfig({params: {mq: 'a.placeholder'}});
          await serialPromise(_.map(invalids, (invalid) => async () => {
            const params = {mq: invalid};
            assertRejects(() => fiwareApi.getEntities.createRequestConfig({params}));
            _.set(config, 'params', params); // mutate!!

            // DIVERGE!!
            if (fiwareApi === orionApi) {
              const {data: docs} = await fiwareApi.rawRequest(config);
              assert.strictEqual(docs.length, 0);
            }
            if (fiwareApi === ngsiApi) {
              const err = await assertRejects(fiwareApi.rawRequest(config));
              assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_INVALID_SQL_ATTRIBUTE_TOKEN);
            }

          }));
        });

        it('invalid chars not allowed (ngsi) but ALLOWED in Orion', async () => {
          const invalids = [
            'metaderp.ha#h',
            'metaderp.s  s',
          ];
          const config = fiwareApi.getEntities.createRequestConfig({params: {mq: 'a.placeholder'}});
          await serialPromise(_.map(invalids, (invalid) => async () => {
            const params = {mq: invalid};
            assertRejects(() => fiwareApi.getEntities.createRequestConfig({params}));
            _.set(config, 'params', params); // mutate!!

            // DIVERGE!!
            if (fiwareApi === orionApi) {
              const {data: docs} = await fiwareApi.rawRequest(config);
              assert.strictEqual(docs.length, 0);
            }
            if (fiwareApi === ngsiApi) {
              const err = await assertRejects(fiwareApi.rawRequest(config));
              assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_INVALID_SQL_METADATA_TOKEN);
            }

          }));
        });

        it('Some others that fail with "no metadata in right-hand-side of q-item"', async () => {
          const invalids = [
            {in: '.', ngsiErr: ORION_ERRORS.BAD_REQ_INVALID_SQL_ATTRIBUTE_TOKEN},
            {in: '....', ngsiErr: ORION_ERRORS.BAD_REQ_INVALID_SQL_ATTRIBUTE_TOKEN},
            {in: '.....aa...a.a.', ngsiErr: ORION_ERRORS.BAD_REQ_INVALID_SQL_ATTRIBUTE_TOKEN},
            {in: 'derp..metaderp', ngsiErr: ORION_ERRORS.BAD_REQ_INVALID_SQL_METADATA_TOKEN},
            {in: 'a..a.........', ngsiErr: ORION_ERRORS.BAD_REQ_INVALID_SQL_METADATA_TOKEN},
          ];
          const config = fiwareApi.getEntities.createRequestConfig({params: {mq: 'a.placeholder'}});
          await serialPromise(_.map(invalids, (invalid) => async () => {
            _.set(config, 'params.mq', invalid.in); // mutate!!
            const err = await assertRejects(fiwareApi.rawRequest(config));

            if (fiwareApi === orionApi) {
              assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_NO_METADATA_AT_ALL);
            }
            if (fiwareApi === ngsiApi) {
              assertErrorsMatch(err, invalid.ngsiErr);
            }
          }));
        });

        it('invalids that fail with "empty q-item"', async () => {
          const invalids = [
            'derp.metaderp;;',
            ';;derp.metaderp',
            'derp.metaderp;;derp.etaderp',
          ];
          const config = fiwareApi.getEntities.createRequestConfig({params: {mq: 'a.placeholder'}});
          await serialPromise(_.map(invalids, (invalid) => async () => {
            _.set(config, 'params.mq', invalid); // mutate!!
            const err = await assertRejects(fiwareApi.rawRequest(config));
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_EMPTY_Q_ITEM);
          }));
        });

        it('sigh.. empty space has own err.. (in Orion at least)', async () => {
          const params = {mq: ' '};
          assertRejects(() => fiwareApi.getEntities.createRequestConfig({params}));
          const config = fiwareApi.getEntities.createRequestConfig();
          _.set(config, 'params', params);
          const err = await assertRejects(fiwareApi.rawRequest(config));

          // DIVERGE!!
          if (fiwareApi === orionApi) {
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_EMPTY_Q_ITEM_WHITESPACE);
          }
          if (fiwareApi === ngsiApi) {
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_NO_METADATA_AT_ALL);
          }
        });

        it('Fails if empty', async () => {
          const params = {mq: ''};
          assertRejects(() => fiwareApi.getEntities.createRequestConfig({params}));
          const config = fiwareApi.getEntities.createRequestConfig();
          _.set(config, 'params', params);
          const err = await assertRejects(fiwareApi.rawRequest(config));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_EMPTY_MQ_PARAM);
        });

        it('invalids with illegal Orion chars - they are valid in ngsi!!', async () => {
          const invalids = [
            'valid.in"valid',
            'valid.in)valid',
            'valid.in(valid',
          ];
          await serialPromise(_.map(invalids, (invalid) => async () => {
            const config = fiwareApi.getEntities.createRequestConfig({params: {mq: invalid}});

            // DIVERGE!!
            if (fiwareApi === orionApi) {
              const err = await assertRejects(fiwareApi.rawRequest(config));
              assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_INVALID_CHAR_IN_PARAM_MQ);
            }
            if (fiwareApi === ngsiApi) {
              const {data: docs} = await fiwareApi.rawRequest(config);
              assert.strictEqual(docs.length, 0);
            }

          }));

        });

      });

      describe('EXISTENCE and NON-EXISTENCE', () => {

        it('EXISTENCE: finds 3 entities with "metanav" derp.mdItem', async () => {
          const params = {mq: 'derp.metanav'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert.strictEqual(docs.length, 3, 'did not find 3 "derp.metanav" entities');
        });

        const numNonNavs = entities.length - 3;
        it(`NON-EXISTENCE: finds ${numNonNavs} entities with only non-"derp.metanav" attrs`, async () => {
          const params = {mq: '!derp.metanav'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert.strictEqual(docs.length, numNonNavs, `did not find ${numNonNavs} non-"derp.metanav" entities`);
        });

      });

      describe('strings', () => {

        it('EQUALITY (==string): find 1 with "derp.metaderp==bbb"', async () => {
          const params = {mq: 'derp.metaderp==bbb'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert(docs.length === 1, 'there should only be one "bbb"');
          const value = getDerpMetadata(docs[0]);
          const expected = 'bbb';
          assert.strictEqual(value, expected, 'doc looks wrong');
        });

        it('COLON-EQUALITY (:string): find 1 with "derp.metaderp:bbb"', async () => {
          // This one just here to show that : is synonymous to ==
          const params = {mq: 'derp.metaderp:bbb'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert(docs.length === 1, 'there should only be one "bbb"');
          const value = getDerpMetadata(docs[0]);
          const expected = 'bbb';
          assert.strictEqual(value, expected, 'doc looks wrong');
        });

        it('NOT-EQUAL (!=string): find those with "derp.metaderp!=bbb"', async () => {
          // NOTE that NOT-EQUAL does NOT type-match
          const params = {mq: 'derp.metaderp!=bbb'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert.strictEqual(docs.length, 10, 'there should be 10 non-"bbb"');
        });

        it('INEQUALITY (>string): find with "derp.metaderp>bbb"', async () => {
          const params = {mq: 'derp.metaderp>bbb'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert(docs.length === 1, 'there should only be one "derp.metaderp>bbb"');
          const value = getDerpMetadata(docs[0]);
          const expected = 'ccc_null';
          assert.deepStrictEqual(value, expected, 'doc looks wrong');
        });

        it('INEQUALITY (>=string): find with "derp.metaderp>=bbb"', async () => {
          const params = {mq: 'derp.metaderp>=bbb'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert(docs.length === 2, 'there should be 2 "derp.metaderp>=bbb"');
          const values = getMultiDerpMetadatas(docs);
          const expected = ['bbb', 'ccc_null'];
          assert.deepStrictEqual(values.sort(), expected.sort(), 'docs looks wrong');
        });

        it('INEQUALITY (<string): find with "derp.metaderp<bbb"', async () => {
          const params = {mq: 'derp.metaderp<bbb'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert(docs.length === 1, 'there should only be one "derp.metaderp<bbb"');
          const value = getDerpMetadata(docs[0]);
          const expected = 'aaa222_true';
          assert.strictEqual(value, expected, 'doc looks wrong');
        });

        it('INEQUALITY (<=string): find with "derp.metaderp<=bbb"', async () => {
          const params = {mq: 'derp.metaderp<=bbb'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert(docs.length === 2, 'there should be 2 "derp.metaderp<=bbb"');
          const values = getMultiDerpMetadatas(docs);
          const expected = ['aaa222_true', 'bbb'];
          assert.deepStrictEqual(values.sort(), expected.sort(), 'docs looks wrong');
        });

        it('PATTERN-MATCH (~=pattern): find with derp.metaderp~=b', async () => {
          const params = {mq: 'derp.metaderp~=b'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert(docs.length === 1, 'there should only be one "derp.metaderp~=b"');
          const value = getDerpMetadata(docs[0]);
          const expected = 'bbb';
          assert.deepStrictEqual(value, expected, 'doc looks wrong');
        });

      });

      describe('numbers', () => {

        it('EQUALITY (==number): find 1 with "derp.metaderp==1"', async () => {
          const params = {mq: 'derp.metaderp==1'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert(docs.length === 1, 'there should only be one derp.metaderp==1');
          const value = getDerpMetadata(docs[0]);
          const expected = 1;
          assert.deepStrictEqual(value, expected, 'doc looks wrong');
        });

        it('COLON-EQUALITY (:number): find 1 with "derp.metaderp:1"', async () => {
          const params = {mq: 'derp.metaderp:1'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert(docs.length === 1, 'there should only be one derp.metaderp:1');
          const value = getDerpMetadata(docs[0]);
          const expected = 1;
          assert.deepStrictEqual(value, expected, 'doc looks wrong');
        });

        it('UNEQUAL (!=number): find those with derp.metaderp!=1', async () => {
          const params = {mq: 'derp.metaderp!=1'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert.strictEqual(docs.length, 10);
        });

        it('INEQUALITY (>number) find with "derp.metaderp>2"', async () => {
          const params = {mq: 'derp.metaderp>2'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert(docs.length === 1, 'there should only be one "derp.metaderp>2"');
          const value = getDerpMetadata(docs[0]);
          const expected = 4;
          assert.deepStrictEqual(value, expected, 'doc looks wrong');
        });

        it('INEQUALITY (>=number) find with "derp.metaderp>=2"', async () => {
          const params = {mq: 'derp.metaderp>=2'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert(docs.length === 2, 'there should only be one "derp.metaderp>=2"');
          const values = getMultiDerpMetadatas(docs);
          const expected = [4, 2];
          assert.deepStrictEqual(values.sort(), expected.sort(), 'docs look wrong');
        });

        it('INEQUALITY (<number) find with "derp.metaderp<2"', async () => {
          // Note that the inequality only picks out those entities where
          // derp is a number
          const params = {mq: 'derp.metaderp<2'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert(docs.length === 1, 'there should only be one "derp.metaderp<2"');
          const value = getDerpMetadata(docs[0]);
          const expected = 1;
          assert.deepStrictEqual(value, expected, 'doc looks wrong');
        });

        it('INEQUALITY (<=number) find with "derp.metaderp<=2"', async () => {
          const params = {mq: 'derp.metaderp<=2'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert(docs.length === 2, 'there should only be one "derp.metaderp<=2"');
          const values = getMultiDerpMetadatas(docs);
          const expected = [1, 2];
          assert.deepStrictEqual(values.sort(), expected.sort(), 'docs look wrong');
        });

        it('MATCH_PATTERN (~=number) find with derp.metaderp~=2', async () => {
          const config = fiwareApi.getEntities.createRequestConfig({params: {mq: 'a.placeholder'}});
          _.set(config, 'params.mq', 'derp.metaderp~=2'); // mutate!!

          if (fiwareApi === orionApi) {
            const {data: docs} = await fiwareApi.rawRequest(config);
            assert(docs.length === 1, 'there should only be one "derp.metaderp~=2"');
            const value = getDerpMetadata(docs[0]);
            const expected = 'aaa222_true';
            assert.deepStrictEqual(value, expected);
          }
          if (fiwareApi === ngsiApi) {
            const err = await assertRejects(fiwareApi.rawRequest(config));
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_OP_MATCHPATTERN_NGSI);
          }

        });

      });

      describe('booleans', async () => {

        it('EQUALITY (==true): find 1 with "derp.metaderp==true"', async () => {
          const params = {mq: 'derp.metaderp==true'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert(docs.length === 1, 'there should only be one derp.metaderp==true');
          const value = getDerpMetadata(docs[0]);
          const expected = true;
          assert.deepStrictEqual(value, expected, 'doc looks wrong');
        });

        it('EQUALITY (==false): find some with "derp.metaderp==false"', async () => {
          const params = {mq: 'derp.metaderp==false'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert(docs.length === 1, 'there should only be one derp.metaderp==false');
          const value = getDerpMetadata(docs[0]);
          const expected = false;
          assert.deepStrictEqual(value, expected, 'doc looks wrong');
        });

        it('COLON-EQUALITY (:true): find 1 with "derp.metaderp:true"', async () => {
          const params = {mq: 'derp.metaderp:true'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert(docs.length === 1, 'there should only be one derp.metaderp:true');
          const value = getDerpMetadata(docs[0]);
          const expected = true;
          assert.deepStrictEqual(value, expected, 'doc looks wrong');
        });

        it('COLON-EQUALITY (:false): find some with "derp.metaderp:false"', async () => {
          const params = {mq: 'derp.metaderp:false'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert(docs.length === 1, 'there should only be one derp.metaderp:false');
          const value = getDerpMetadata(docs[0]);
          const expected = false;
          assert.deepStrictEqual(value, expected, 'doc looks wrong');
        });

        it('INEQUALITY (<true/false): find with "derp.metaderp<true" or "derp.metaderp<false" FAILS', async () => {
          const config = fiwareApi.getEntities.createRequestConfig({params: {mq: 'a.placeholder'}});
          _.set(config, 'params.mq', 'derp.metaderp<true'); // mutate!!
          const errT = await assertRejects(fiwareApi.rawRequest(config));
          _.set(config, 'params.mq', 'derp.metaderp<false'); // mutate!!
          const errF = await assertRejects(fiwareApi.rawRequest(config));

          // DIVERGE!!
          if (fiwareApi === orionApi) {
            assertErrorsMatch(errT, ORION_ERRORS.BAD_REQ_OP_LESSTHAN_ON_BOOL);
            assertErrorsMatch(errF, ORION_ERRORS.BAD_REQ_OP_LESSTHAN_ON_BOOL);
          }
          if (fiwareApi === ngsiApi) {
            assertErrorsMatch(errT, ORION_ERRORS.BAD_REQ_OP_LESSTHAN_NGSI);
            assertErrorsMatch(errF, ORION_ERRORS.BAD_REQ_OP_LESSTHAN_NGSI);
          }
        });

        it('INEQUALITY (<=true/false): find with "derp.metaderp<=true" or "derp.metaderp<=false" FAILS', async () => {
          const config = fiwareApi.getEntities.createRequestConfig({params: {mq: 'a.placeholder'}});
          _.set(config, 'params.mq', 'derp.metaderp<=true'); // mutate!!
          const errT = await assertRejects(fiwareApi.rawRequest(config));
          _.set(config, 'params.mq', 'derp.metaderp<=false'); // mutate!!
          const errF = await assertRejects(fiwareApi.rawRequest(config));

          // DIVERGE!!
          if (fiwareApi === orionApi) {
            assertErrorsMatch(errT, ORION_ERRORS.BAD_REQ_OP_LESSTHANOREQUAL_ON_BOOL);
            assertErrorsMatch(errF, ORION_ERRORS.BAD_REQ_OP_LESSTHANOREQUAL_ON_BOOL);
          }
          if (fiwareApi === ngsiApi) {
            assertErrorsMatch(errT, ORION_ERRORS.BAD_REQ_OP_LESSTHANOREQUAL_NGSI);
            assertErrorsMatch(errF, ORION_ERRORS.BAD_REQ_OP_LESSTHANOREQUAL_NGSI);
          }
        });

        it('INEQUALITY (>true/false): find with "derp.metaderp>true" or "derp.metaderp>false" FAILS', async () => {
          const config = fiwareApi.getEntities.createRequestConfig({params: {mq: 'a.placeholder'}});
          _.set(config, 'params.mq', 'derp.metaderp>true'); // mutate!!
          const errT = await assertRejects(fiwareApi.rawRequest(config));
          _.set(config, 'params.mq', 'derp.metaderp>false'); // mutate!!
          const errF = await assertRejects(fiwareApi.rawRequest(config));

          // DIVERGE!!
          if (fiwareApi === orionApi) {
            assertErrorsMatch(errT, ORION_ERRORS.BAD_REQ_OP_GREATERTHAN_ON_BOOL);
            assertErrorsMatch(errF, ORION_ERRORS.BAD_REQ_OP_GREATERTHAN_ON_BOOL);
          }
          if (fiwareApi === ngsiApi) {
            assertErrorsMatch(errT, ORION_ERRORS.BAD_REQ_OP_GREATERTHAN_NGSI);
            assertErrorsMatch(errF, ORION_ERRORS.BAD_REQ_OP_GREATERTHAN_NGSI);
          }
        });

        it('INEQUALITY (>=true/false): find with "derp.metaderp>=true" or "derp.metaderp>=false" FAILS', async () => {
          const config = fiwareApi.getEntities.createRequestConfig({params: {mq: 'a.placeholder'}});
          _.set(config, 'params.mq', 'derp.metaderp>=true'); // mutate!!
          const errT = await assertRejects(fiwareApi.rawRequest(config));
          _.set(config, 'params.mq', 'derp.metaderp>=false'); // mutate!!
          const errF = await assertRejects(fiwareApi.rawRequest(config));

          // DIVERGE!!
          if (fiwareApi === orionApi) {
            assertErrorsMatch(errT, ORION_ERRORS.BAD_REQ_OP_GREATERTHANOREQUAL_ON_BOOL);
            assertErrorsMatch(errF, ORION_ERRORS.BAD_REQ_OP_GREATERTHANOREQUAL_ON_BOOL);
          }
          if (fiwareApi === ngsiApi) {
            assertErrorsMatch(errT, ORION_ERRORS.BAD_REQ_OP_GREATERTHANOREQUAL_NGSI);
            assertErrorsMatch(errF, ORION_ERRORS.BAD_REQ_OP_GREATERTHANOREQUAL_NGSI);
          }
        });

        it('MATCH_PATTERN (~=true) find with derp.metaderp~=true', async () => {
          const config = fiwareApi.getEntities.createRequestConfig({params: {mq: 'a.placeholder'}});
          _.set(config, 'params.mq', 'derp.metaderp~=true'); // mutate!!

          // DIVERGE!!
          if (fiwareApi === orionApi) {
            const {data: docs} = await fiwareApi.rawRequest(config);
            assert(docs.length === 1, 'there should be one derp.metaderp~=true');
            const value = getDerpMetadata(docs[0]);
            const expected = 'aaa222_true';
            assert.deepStrictEqual(value, expected, 'doc looks wrong');
          }
          if (fiwareApi === ngsiApi) {
            const err = await assertRejects(fiwareApi.rawRequest(config));
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_OP_MATCHPATTERN_NGSI);
          }

        });

      });

      describe('nullz', async () => {

        it('EQUALITY (==null): find 1 with "derp.metaderp==null"', async () => {
          const params = {mq: 'derp.metaderp==null'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert(docs.length === 1, 'there should only be one derp.metaderp==null');
          const value = getDerpMetadata(docs[0]);
          const expected = null;
          assert.deepStrictEqual(value, expected, 'doc looks wrong');
        });

        it('COLON-EQUALITY (:null): find 1 with "derp.metaderp:null"', async () => {
          const params = {mq: 'derp.metaderp:null'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert(docs.length === 1, 'there should only be one derp.metaderp:null');
          const value = getDerpMetadata(docs[0]);
          const expected = null;
          assert.deepStrictEqual(value, expected, 'doc looks wrong');
        });

        it('NOT-EQUAL (!=null): find those with "derp.metaderp!=null"', async () => {
          const params = {mq: 'derp.metaderp!=null'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert.strictEqual(docs.length, 10);
        });

        it('INEQUALITY (>null): find those with "derp.metaderp>null FAILS"', async () => {
          const config = fiwareApi.getEntities.createRequestConfig({params: {mq: 'a.placeholder'}});
          _.set(config, 'params.mq', 'derp.metaderp>null'); // mutate!!
          const err = await assertRejects(fiwareApi.rawRequest(config));

          // DIVERGE!!
          if (fiwareApi === orionApi) {
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_OP_GREATERTHAN_ON_NULL);
          }
          if (fiwareApi === ngsiApi) {
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_OP_GREATERTHAN_NGSI);
          }
        });

        it('INEQUALITY (>=null): find those with "derp.metaderp>=null FAILS"', async () => {
          const config = fiwareApi.getEntities.createRequestConfig({params: {mq: 'a.placeholder'}});
          _.set(config, 'params.mq', 'derp.metaderp>=null'); // mutate!!
          const err = await assertRejects(fiwareApi.rawRequest(config));

          // DIVERGE!!
          if (fiwareApi === orionApi) {
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_OP_GREATERTHANOREQUAL_ON_NULL);
          }
          if (fiwareApi === ngsiApi) {
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_OP_GREATERTHANOREQUAL_NGSI);
          }
        });

        it('INEQUALITY (<null): find those with "derp.metaderp<null FAILS"', async () => {
          const config = fiwareApi.getEntities.createRequestConfig({params: {mq: 'a.placeholder'}});
          _.set(config, 'params.mq', 'derp.metaderp<null'); // mutate!!
          const err = await assertRejects(fiwareApi.rawRequest(config));

          // DIVERGE!!
          if (fiwareApi === orionApi) {
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_OP_LESSTHAN_ON_NULL);
          }
          if (fiwareApi === ngsiApi) {
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_OP_LESSTHAN_NGSI);
          }
        });

        it('INEQUALITY (<=null): find those with "derp.metaderp<=null FAILS"', async () => {
          const config = fiwareApi.getEntities.createRequestConfig({params: {mq: 'a.placeholder'}});
          _.set(config, 'params.mq', 'derp.metaderp<=null'); // mutate!!
          const err = await assertRejects(fiwareApi.rawRequest(config));

          // DIVERGE!!
          if (fiwareApi === orionApi) {
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_OP_LESSTHANOREQUAL_ON_NULL);
          }
          if (fiwareApi === ngsiApi) {
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_OP_LESSTHANOREQUAL_NGSI);
          }
        });

        it('MATCH_PATTERN (~=null) find with derp.metaderp~=null', async () => {
          const config = fiwareApi.getEntities.createRequestConfig({params: {mq: 'a.placeholder'}});
          _.set(config, 'params.mq', 'derp.metaderp~=null'); // mutate!!

          // DIVERGE!!
          if (fiwareApi === orionApi) {
            const {data: docs} = await fiwareApi.rawRequest(config);
            assert(docs.length === 1);
            const value = getDerpMetadata(docs[0]);
            const expected = 'ccc_null';
            assert.deepStrictEqual(value, expected, 'doc looks wrong');
          }
          if (fiwareApi === ngsiApi) {
            const err = await assertRejects(fiwareApi.rawRequest(config));
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_OP_MATCHPATTERN_NGSI);
          }
        });

      });

      describe('trying to use multiple tokens to dig into attr.metadata.value', () => {

        it('EQUALITY (multiple tokens): find 1 with "derp.metaderp.subderp.a.b.2.c==2.3" works!', async () => {
          const params = {mq: 'derp.metaderp.subderp.a.b.2.c==2.3'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert(docs.length === 1, 'Should be only 1 doc');
          assert.strictEqual(_.get(docs[0], 'derp.metadata.metaderp.value.subderp.a.b.2.c'), 2.3, 'Could not find doc');
        });

      });

    });

  });


});
