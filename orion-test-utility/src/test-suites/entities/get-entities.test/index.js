/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

// Test all the different versions of patch value

const mongoApi = require('../../../mongo-api');

describe('get-entities', () => {

  before(() => mongoApi.assertEntitiesEmpty());
  after(() => mongoApi.assertEntitiesEmpty());

  require('./check-entity-representations.test');
  require('./check-entity-id.test');
  require('./check-entity-id-pattern.test');
  require('./check-entity-type.test');
  require('./check-entity-type-pattern.test');
  require('./check-attrs-list.test');
  require('./check-metadata-list.test');
  require('./check-q-filter.test');
  require('./check-q-filter-syntax-limits.test');
  require('./check-multi-q-filter.test');
  require('./check-mq-filter.test');
  require('./check-service-path-limits.test');
  require('./check-incompatible-params.test');
  require('./check-order-by.test');
  require('./check-pagination.test');
  require('./the-shotgun-scenario.test');

});
