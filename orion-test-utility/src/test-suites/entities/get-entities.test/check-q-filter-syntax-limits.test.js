/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const serialPromise = require('promise-serial-exec');

const {
  assertRejects,
  ngsiResponseErrorUtils: {assertErrorsMatch},
} = require('@ratatosk/ngsi-utils');

const {ORION_ERRORS} = require('@ratatosk/ngsi-constants');

const {orionApi, ngsiApi} = require('../../../client-api-util');
const mongoApi = require('../../../mongo-api');

// create some entities to check against. ALL of these will be accessible
// to find calls throughout this file
const entityTemplates = [
  {id: 'harp', type: 'Andy', nav: 'maven', derp: 23},
];

describe('check-invalid-q-filter', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      before(async () => {
        await mongoApi.assertEntitiesEmpty();
        await serialPromise(_.map(entityTemplates, (entity) => async () => {
          return fiwareApi.createEntity({entity});
        }));
      });
      after(async () => mongoApi.wipeEntities());

      it('Fails if empty', async () => {
        const config = fiwareApi.getEntities.createRequestConfig();
        _.set(config, 'params.q', ''); // mutate!!
        const err = await assertRejects(fiwareApi.rawRequest(config));
        assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_EMPTY_Q_PARAM);
      });

      it('sigh.. empty space has own err..', async () => {
        const config = fiwareApi.getEntities.createRequestConfig();
        _.set(config, 'params.q', ' '); // mutate!!
        const err = await assertRejects(fiwareApi.rawRequest(config));

        // DIVERGE!!
        if (fiwareApi === orionApi) {
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_EMPTY_Q_ITEM_WHITESPACE);
        }
        if (fiwareApi === ngsiApi) {
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_INVALID_SQL_ATTRIBUTE_TOKEN);
        }
      });

      it('some invalids with empty q-items', async () => {
        const invalids = [
          'derp;;',
          ';;derp',
          'derp;;merp',
        ];
        const config = fiwareApi.getEntities.createRequestConfig();
        await serialPromise(_.map(invalids, (invalid) => async () => {
          _.set(config, 'params.q', invalid); // mutate!!
          const err = await assertRejects(fiwareApi.rawRequest(config));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_EMPTY_Q_ITEM);
        }));
      });

      it('invalids with illegal chars (that are illegal ONLY for Orion)', async () => {
        const invalids = [
          '"',
          '(',
          ')',
        ];
        const config = fiwareApi.getEntities.createRequestConfig();
        await serialPromise(_.map(invalids, (invalid) => async () => {
          _.set(config, 'params.q', invalid); // mutate!!

          // DIVERGE!!
          if (fiwareApi === orionApi) {
            const err = await assertRejects(fiwareApi.rawRequest(config));
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_INVALID_CHAR_IN_PARAM_Q);
          }
          if (fiwareApi === ngsiApi) {
            const {data: docs} = await fiwareApi.rawRequest(config);
            assert.strictEqual(docs.length, 0);
          }
        }));
      });

      it('Some others that also surprisingly work (should NOT throw errors -> dig into attrValue)', async () => {
        const valids = [
          'derp...',
          'derp.asdf....s..s.s.s..s.s................',
          'a.a......b..s.',
        ];
        const config = fiwareApi.getEntities.createRequestConfig();
        await serialPromise(_.map(valids, (valid) => async () => {
          _.set(config, 'params.q', valid); // mutate!!
          const {data: docs} = await fiwareApi.rawRequest(config);
          assert.strictEqual(docs.length, 0, 'should be 0 docs');
        }));
      });

      it('some basic valids', async () => {
        const valids = [
          'nav',
          '!bowp',
          'nav;derp==23',
        ];
        await serialPromise(_.map(valids, (valid) => async () => {
          const params = {q: valid, representationType: 'values'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert.strictEqual(docs.length, 1);
          const expected = ['maven', 23];
          docs[0].sort();
          expected.sort();
          assert.deepStrictEqual(docs[0], expected);
        }));
      });

      it('and some that are only valid in Orion', async () => {
        const valids = [
          ';nav',
          'nav;',
          ';nav;derp==23;',
        ];
        const config = fiwareApi.getEntities.createRequestConfig({params: {representationType: 'values'}});
        await serialPromise(_.map(valids, (valid) => async () => {
          _.set(config, 'params.q', valid); // mutate!!

          // DIVERGE!!
          if (fiwareApi === orionApi) {
            const {data: docs} = await fiwareApi.rawRequest(config);
            assert.strictEqual(docs.length, 1);
            const expected = ['maven', 23];
            docs[0].sort();
            expected.sort();
            assert.deepStrictEqual(docs[0], expected);
          }
          if (fiwareApi === ngsiApi) {
            const err = await assertRejects(fiwareApi.rawRequest(config));
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_EMPTY_Q_ITEM);
          }
        }));
      });

    });

  });


});
