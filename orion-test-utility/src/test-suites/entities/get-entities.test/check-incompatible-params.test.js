/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
// const assert = require('assert');

const {
  assertRejects,
  ngsiResponseErrorUtils: {assertErrorsMatch},
} = require('@ratatosk/ngsi-utils');

const {orionApi, ngsiApi} = require('../../../client-api-util');
const mongoApi = require('../../../mongo-api');

const {ORION_ERRORS} = require('@ratatosk/ngsi-constants');

describe('check incompatible parameters', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      before(async () => {
        await mongoApi.assertEntitiesEmpty();
      });
      after(async () => mongoApi.wipeEntities());

      it('"type" and "typePattern" are incompatible', async () => {
        const params = {type: 'bibbity', typePattern: 'boo'};
        const config = fiwareApi.getEntities.createRequestConfig();
        _.set(config, 'params', params); // mutate!!
        const err = await assertRejects(fiwareApi.rawRequest(config));
        assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_INCOMPATIBLE_PARAMS_TYPE_PATTERN);
      });

      it('"id" and "idPattern" are incompatible', async () => {
        const params = {id: 'bibbity', idPattern: 'boo'};
        const config = fiwareApi.getEntities.createRequestConfig();
        _.set(config, 'params', params); // mutate!!
        const err = await assertRejects(fiwareApi.rawRequest(config));
        assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_INCOMPATIBLE_PARAMS_ID_PATTERN);
      });

    });

  });

});
