/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const serialPromise = require('promise-serial-exec');

const {
  assertRejects,
  ngsiResponseErrorUtils: {assertErrorsMatch},
} = require('@ratatosk/ngsi-utils');

const {ORION_ERRORS} = require('@ratatosk/ngsi-constants');

const {orionApi, ngsiApi} = require('../../../client-api-util');
const mongoApi = require('../../../mongo-api');

const entities = [
  {id: 'derp', type: 'Andy'},
  {id: 'derp', type: 'Mandy'},
  {id: 'perd', type: 'Sandy'},
  {id: 'perp', type: 'Tandy'},
];

describe('check-entity-id (filtering by id should work as expected)', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      before(async () => {
        await mongoApi.assertEntitiesEmpty();
        // create some entities to check against
        await serialPromise(_.map(entities, (entity) => async () => {
          return fiwareApi.createEntity({entity});
        }));
      });
      after(async () => mongoApi.wipeEntities());

      describe('basic functionality', () => {

        it('should find all entities without an id filter', async () => {
          const {data: docs} = await fiwareApi.getEntities();
          assert.strictEqual(docs.length, 4, 'wrong number of docs');
        });

        it('Def: getEntities(arg) === rawRequest(getEntities.createRequestConfig(arg))', async () => {
          const config = fiwareApi.getEntities.createRequestConfig();
          const {data: docs} = await fiwareApi.rawRequest(config);
          assert.strictEqual(docs.length, 4, 'wrong number of docs');
        });

        it('"null" and "undefined" also work', async () => {
          const config = fiwareApi.getEntities.createRequestConfig();
          // mutate
          _.set(config, 'params', null);
          const {data: docsNull} = await fiwareApi.rawRequest(config);
          assert.strictEqual(docsNull.length, 4, 'wrong number of docs');
          // mutate
          _.set(config, 'params', undefined);
          const {data: docsVoid} = await fiwareApi.getEntities({params: undefined});
          assert.strictEqual(docsVoid.length, 4, 'wrong number of docs');
        });

        it('should find 2 derps', async () => {
          const params = {id: 'derp'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert.strictEqual(docs.length, 2, 'should find two derps');
        });

        it('should find 0 perdsssss', async () => {
          const params = {id: 'perdsssss'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert.strictEqual(docs.length, 0, 'should find 0 perdsssss');
        });

      });

      describe('testing what is and is not allowed', () => {

        it('using a list of ids', async () => {
          const config = fiwareApi.getEntities.createRequestConfig();
          _.set(config, 'params.id', 'perd,perp'); // mutate!!
          const {data: docs} = await fiwareApi.rawRequest(config);
          assert.strictEqual(docs.length, 2);
        });

        it('using a list of WRAPPED ids', async () => {
          const params = {id: '\'perd\',\'perp\''};
          // DIVERGE!! (this really SHOULD work - for the sake of consistency)
          if (fiwareApi === orionApi) {
            const err = await assertRejects(fiwareApi.getEntities({params}));
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_INVALID_CHAR_IN_PARAM);
          }
          if (fiwareApi === ngsiApi) {
            const {data: docs} = await fiwareApi.getEntities({params});
            assert.strictEqual(docs.length, 2);
          }
        });

        it('empty param - fails', async () => {
          const config = fiwareApi.getEntities.createRequestConfig();
          _.set(config, 'params.id', ''); // mutate!!
          const err = await assertRejects(fiwareApi.rawRequest(config));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_EMPTY_ID_PARAM);
        });

        it('illegal orion params', async () => {
          const config = fiwareApi.getEntities.createRequestConfig();
          const invalidIds = [
            'aa"aa',
            'ab(ab)ab',
          ];
          await serialPromise(_.map(invalidIds, (invalidId) => async () => {
            _.set(config, 'params.id', invalidId);

            // DIVERGE!! (this should be configurable for ngsi-server)
            if (fiwareApi === orionApi) {
              const err = await assertRejects(fiwareApi.rawRequest(config));
              assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_INVALID_CHAR_IN_PARAM);
            } else {
              const {data: docs} = await fiwareApi.rawRequest(config);
              assert.strictEqual(docs.length, 0);
            }

          }));
        });

        it('the explicitly non-allowed entity-id chars', async () => {
          const config = fiwareApi.getEntities.createRequestConfig();
          const forbiddenish = [
            '&', '?', '/', '#',
          ];
          await serialPromise(_.map(forbiddenish, (wazzit) => async () => {

            _.set(config, 'params.id', wazzit);

            // DIVERGE!! (this should be configurable for ngsi-server)
            // these chars are NOT specifically forbidden for query values
            // but it makes no sense to be able to use them when they are
            // are forbidden to use for the id..
            if (fiwareApi === orionApi) {
              const {data: docs} = await fiwareApi.rawRequest(config);
              assert.strictEqual(docs.length, 0);
            } else {
              const err = await assertRejects(fiwareApi.rawRequest(config));
              assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_INVALID_CHAR_IN_PARAM);
            }
          }));
        });

      });

    });

  });

});
