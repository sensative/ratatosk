/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const serialPromise = require('promise-serial-exec');

const {
  assertRejects,
  ngsiDateTimeUtils: {hasValidDateTimeFormat},
  ngsiResponseErrorUtils: {assertErrorsMatch},
} = require('@ratatosk/ngsi-utils');

const {ORION_ERRORS} = require('@ratatosk/ngsi-constants');

const {
  orionApi,
  ngsiApi,
} = require('../../../client-api-util');
const mongoApi = require('../../../mongo-api');

const entities = [
  {
    id: 'boingo',
    attr0: {value: 5, metadata: {a: 2, b: 3, c: 4}},
    attrX: {value: 5, metadata: {a: 5}},
  },
];

describe('check-metadata-list', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      before(async () => {
        await mongoApi.assertEntitiesEmpty();
        await serialPromise(_.map(entities, (entity) => async () => {
          return fiwareApi.createEntity({entity});
        }));
      });
      after(async () => mongoApi.wipeEntities());

      it('metadata prop nil is ok', async () => {
        const nils = [
          null,
          undefined,
        ];
        const params = {id: 'boingo'};
        const config = fiwareApi.getEntities.createRequestConfig({params});
        await serialPromise(_.map(nils, (nil) => async () => {
          _.set(config, 'params.metadata', nil); // mutate!!
          const {data: [doc]} = await fiwareApi.rawRequest(config);
          const mdKeys = _.keys(doc.attr0.metadata);
          const expected = ['a', 'b', 'c'];
          assert.deepStrictEqual(mdKeys, expected);
        }));
      });

      it('metadata prop is empty string, FAIL', async () => {
        const config = fiwareApi.getEntities.createRequestConfig();
        _.set(config, 'params.metadata', ''); // mutate!!
        const err = await assertRejects(fiwareApi.rawRequest(config));
        assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_EMPTY_METADATA_PARAM);
      });

      it('it is possible to exclude existing custom metadatas (and is ignored md nonexistent)', async () => {
        const params = {id: 'boingo', metadata: 'a,b'};
        const {data: [doc]} = await fiwareApi.getEntities({params});
        const mdKeys0 = _.keys(doc.attr0.metadata);
        const expectedKeys0 = ['a', 'b'];
        assert.deepStrictEqual(mdKeys0, expectedKeys0);
        const mdKeysX = _.keys(doc.attrX.metadata);
        const expectedKeysX = ['a'];
        assert.deepStrictEqual(mdKeysX, expectedKeysX);
      });

      it('it is possible to extract some builtins', async () => {
        const params = {id: 'boingo', metadata: 'dateCreated,dateModified'};
        const {data: [doc]} = await fiwareApi.getEntities({params});
        const mdKeys0 = _.keys(doc.attr0.metadata);
        const expectedKeys0 = ['dateCreated', 'dateModified'];
        assert.deepStrictEqual(mdKeys0, expectedKeys0);
        const mdKeysX = _.keys(doc.attrX.metadata);
        const expectedKeysX = ['dateCreated', 'dateModified'];
        assert.deepStrictEqual(mdKeysX, expectedKeysX);
        assert(hasValidDateTimeFormat(doc.attr0.metadata.dateCreated.value));
        assert(hasValidDateTimeFormat(doc.attr0.metadata.dateModified.value));
      });

      it('it is possible to extract some builtins and ALL customs with "*"', async () => {
        const params = {id: 'boingo', metadata: 'dateCreated,dateModified,*'};
        const {data: [doc]} = await fiwareApi.getEntities({params});
        const mdKeys0 = _.keys(doc.attr0.metadata);
        const expectedKeys0 = ['dateCreated', 'dateModified', 'a', 'b', 'c'];
        mdKeys0.sort();
        expectedKeys0.sort();
        assert.deepStrictEqual(mdKeys0, expectedKeys0);
        const mdKeysX = _.keys(doc.attrX.metadata);
        const expectedKeysX = ['dateCreated', 'dateModified', 'a'];
        mdKeysX.sort();
        expectedKeysX.sort();
        assert.deepStrictEqual(mdKeysX, expectedKeysX);
      });

      it('it is possible to extract some builtins and SOME customs by listing', async () => {
        const params = {id: 'boingo', metadata: 'dateCreated,dateModified,a,b'};
        const {data: [doc]} = await fiwareApi.getEntities({params});
        const mdKeys0 = _.keys(doc.attr0.metadata);
        const expectedKeys0 = ['dateCreated', 'dateModified', 'a', 'b'];
        mdKeys0.sort();
        expectedKeys0.sort();
        assert.deepStrictEqual(mdKeys0, expectedKeys0);
        const mdKeysX = _.keys(doc.attrX.metadata);
        const expectedKeysX = ['dateCreated', 'dateModified', 'a'];
        mdKeysX.sort();
        expectedKeysX.sort();
        assert.deepStrictEqual(mdKeysX, expectedKeysX);
      });

    });

  });

});
