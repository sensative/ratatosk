/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const serialPromise = require('promise-serial-exec');

const {
  assertRejects,
  assertThrows,
  ngsiResponseErrorUtils: {assertErrorsMatch, assertErrorsMatchish, assertErrorsMatchFails},
  ngsiQueryResponseUtils: {createPaginator, getResponseCount, getNumDocs},
  ngsiQueryParamsUtils: {optionsParamParser: {parseOptionsParamOrionStyle}},
  ngsiEntityFormatter: {formatEntity},
} = require('@ratatosk/ngsi-utils');

const {ORION_ERRORS} = require('@ratatosk/ngsi-constants');

const {orionApi, ngsiApi} = require('../../../client-api-util');
const mongoApi = require('../../../mongo-api');

describe('check-pagination', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      before(async () => {
        await mongoApi.assertEntitiesEmpty();
        const entityTemplates = _.times(237, (i) => formatEntity({
          id: `johnny_${i}`,
          index: i,
        }));
        await serialPromise(_.map(entityTemplates, (entity) => () => {
          return fiwareApi.createEntity({entity});
        }));
      });
      after(async () => mongoApi.wipeEntities());

      describe('the basics (and introducing the paginator)', () => {

        it('by default, first 20 are returned', async () => {
          const res = await fiwareApi.getEntities();
          const docs = res.data;
          const indices = _.map(docs, (doc) => doc.index.value);
          const expected = _.times(20, (i) => i);
          assert.deepStrictEqual(indices, expected);
          const totalCount = getResponseCount(res.headers);
          assert.strictEqual(totalCount, null, 'count should be null');
          // and check out the paginator
          const paginator = createPaginator(res);
          assert.strictEqual(paginator.offset, 0, 'offset should be 0');
          assert.strictEqual(paginator.count, null, 'count should be null');
          assert.strictEqual(paginator.limit, 20, 'limit should be 20');
          assert.strictEqual(paginator.isFull, true, 'shoud be "true"');
          assert.strictEqual(paginator.docs, docs, 'docs should be docs');
        });

        it('by adding the count options, we get a totalCount in response', async () => {
          const params = {options: 'count'};
          const res = await fiwareApi.getEntities({params});
          const docs = res.data;
          const indices = _.map(docs, (doc) => doc.index.value);
          const expected = _.times(20, (i) => i);
          assert.deepStrictEqual(indices, expected);
          // can get the totalCount directly from the response
          const headerCount = _.get(res, 'headers.fiware-total-count');
          assert.strictEqual(headerCount, '237', 'totalCount should be "237"');
          // but the utility function will be used henceforth:
          const totalCount = getResponseCount(res.headers);
          assert.strictEqual(totalCount, 237, 'totalCount should be 237');
          // or the paginator
          const paginator = createPaginator(res);
          assert.strictEqual(paginator.count, 237, 'paginator.count should be 237');
        });

        it('by adding the "offset" param we indicate an exact number of docs to advance (NOT number of pages)', async () => {
          const params = {offset: 7};
          const config = fiwareApi.getEntities.createRequestConfig({params});
          const res = await fiwareApi.rawRequest(config);
          const docs = res.data;
          const indices = _.map(docs, (doc) => doc.index.value);
          const expected = _.times(20, (i) => i + 7);
          assert.deepStrictEqual(indices, expected);
          // we can get the "offset" parameter from the response
          const configOffset = _.get(res, 'config.params.offset');
          assert.strictEqual(configOffset, 7);
          // or get it from the paginator
          const paginator = createPaginator(res);
          assert.strictEqual(paginator.count, null, 'paginator.count should be null');
          assert.strictEqual(paginator.offset, 7, 'paginator.offset should be 7');
        });

        it('by adding the "limit" param we indicate the size of the page', async () => {
          const params = {limit: 88};
          const config = fiwareApi.getEntities.createRequestConfig({params});
          const res = await fiwareApi.rawRequest(config);
          const docs = res.data;
          const indices = _.map(docs, (doc) => doc.index.value);
          const expected = _.times(88, (i) => i);
          assert.deepStrictEqual(indices, expected);
          // we can get the "limit" parameter from the response
          const configLimit = _.get(res, 'config.params.limit');
          assert.strictEqual(configLimit, 88, 'limit should be 88..');
          // or get it from the paginator
          const paginator = createPaginator(res);
          assert.strictEqual(paginator.offset, 0, 'paginator.offset should be 0');
          assert.strictEqual(paginator.limit, 88, 'paginator.limit should be 88');
        });

        it('and they can all be combined (i.e. count, limit, offset)', async () => {
          const params = {options: 'count', offset: 33, limit: 4};
          const config = fiwareApi.getEntities.createRequestConfig({params});
          const res = await fiwareApi.rawRequest(config);
          const paginator = createPaginator(res);
          // inspect the paginator
          assert.strictEqual(paginator.offset, 33, 'paginator.offset should be 32');
          assert.strictEqual(paginator.limit, 4, 'paginator.limit should be 4');
          assert.strictEqual(paginator.page, 8, 'paginator.page should be 8');
          assert.strictEqual(paginator.pageOffset, 1, 'paginator.pageOffset should be 1');
          assert.strictEqual(paginator.isFull, true, 'paginator.isFull should be true');
          assert.strictEqual(paginator.docs.length, 4, 'we should have many more pages here');
        });

      });

      describe('both the "count" functionality and the representationType use the same options object. Which combos work?', () => {

        it('some valid combos', async () => {
          const validCombos = [
            // the count flag is always allowed (might not be taken into account)
            'count',
            // all the representation types are allowed
            'values',
            'keyValues',
            'unique',
            // and all combos between the groups
            'count,values',
            'count,keyValues',
            'count,unique',
            'unique,count',
            'keyValues,count',
            'values,count',
          ];
          await Promise.all(_.map(validCombos, async (combo) => {
            assert.doesNotThrow(() => parseOptionsParamOrionStyle(combo));
            const config = fiwareApi.getEntities.createRequestConfig({params: {options: 'count'}}); // placeholder
            _.set(config, 'params.options', combo); // mutate!!
            const {data: docs, headers} = await fiwareApi.rawRequest(config);
            assert.strictEqual(docs.length, 20, 'There should be default 20 docs');
            const totalCount = getResponseCount(headers);
            if (combo.indexOf('count') > -1) {
              assert.strictEqual(totalCount, 237, 'totalCount should be 237');
            } else {
              assert.strictEqual(totalCount, null, 'totalCount should be null');
            }
          }));
        });

        it('and then some of the things that are possible with only Orion', async () => {
          const orionCombos = [
            // the lonely ones
            ',', // just a comma is ignored
            ',,,,,,,,,,,,,,,,,,,,,,,,', // just many are ignored too
            ',,,,,,,,,,,,,count',
            // duplicates
            'count,count',
            'count,count,count,count,count,count,count,count,count,count,count,count,count,count,count,count,count,count,count,count,count,count,count,count,count,count,count,count,count,count,count,count,count,count,count,count,count,count,count,count,count,count,count,count,count,count,count,count,count,count', // cool.
            'values,values',
            'values,values,values,values,values', // etc..
            'keyValues,keyValues',
            'unique,unique',
            // with allowed duplications and other obscenities
            ',,,,,,,,,,,,,count,values',
            ',,,,,,,,,,,,count,values,count,count,values', // yeah. etc.
          ];
          await Promise.all(_.map(orionCombos, async (combo) => {
            assert.doesNotThrow(() => parseOptionsParamOrionStyle(combo));
            const config = fiwareApi.getEntities.createRequestConfig({params: {options: 'count'}}); // placeholder
            _.set(config, 'params.options', combo); // mutate!!

            // DIVERGE!!
            if (fiwareApi === orionApi) {
              const {data: docs, headers} = await fiwareApi.rawRequest(config);
              assert.strictEqual(docs.length, 20, 'There should be default 20 docs');
              const totalCount = getResponseCount(headers);
              if (combo.indexOf('count') > -1) {
                assert.strictEqual(totalCount, 237, 'totalCount should be 237');
              } else {
                assert.strictEqual(totalCount, null, 'totalCount should be null');
              }
            }
            if (fiwareApi === ngsiApi) {
              const err = await assertRejects(fiwareApi.rawRequest(config));
              assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_INVALID_OPTIONS_PARAM);
            }
          }));
        });

        it('check up on the invalid combos', async () => {
          const invalidCombos = [
            // the obvious ones
            'values,keyValues',
            'keyValues, values',
            'values,unique',
            'unique,values',
            'keyValues,unique',
            'unique,keyValues',
            // and for emphasis
            'count,values,keyValues',
            'values,count,keyValues',
            'values,keyValues,count',
            // some malformed
            'count,,values', // no zero-length in the middle
            'count,values,', // no trailing commas
          ];
          await Promise.all(_.map(invalidCombos, async (combo) => {
            assertThrows(() => parseOptionsParamOrionStyle(combo));
            const config = fiwareApi.getEntities.createRequestConfig();
            _.set(config, 'params.options', combo); // mutate!!
            const err = await assertRejects(fiwareApi.rawRequest(config));
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_INVALID_OPTIONS_PARAM);
          }));
        });

        it('An empty string is a special case (failure)', async () => {
          const config = fiwareApi.getEntities.createRequestConfig({params: {options: 'count'}});
          _.set(config, 'params.options', ''); // mutate!!
          const err = await assertRejects(fiwareApi.rawRequest(config));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_EMPTY_OPTIONS_PARAM);
        });

      });

      describe('abuse the "limit" parameter', () => {

        it('"limit" = 10 succeeds', async () => {
          const params = {limit: 10};
          const config = fiwareApi.getEntities.createRequestConfig({params});
          const res = await fiwareApi.rawRequest(config);
          const paginator = createPaginator(res);
          assert.strictEqual(paginator.docs.length, 10, 'paginator.docs.length === 10');
        });

        it('"limit" = "10" succeeds', async () => {
          const params = {limit: '10'};
          const config = fiwareApi.getEntities.createRequestConfig({params});
          const res = await fiwareApi.rawRequest(config);
          const paginator = createPaginator(res);
          assert.strictEqual(paginator.docs.length, 10, 'paginator.docs.length === 10');
        });

        it('"limit" = "" (i.e empty string) FAILS', async () => {
          const config = fiwareApi.getEntities.createRequestConfig();
          _.set(config, 'params.limit', ''); // mutate!!
          const err = await assertRejects(fiwareApi.rawRequest(config));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_EMPTY_LIMIT_PARAM);
        });

        it('"limit" = 1 succeeds', async () => {
          const params = {limit: 1};
          const config = fiwareApi.getEntities.createRequestConfig({params});
          const res = await fiwareApi.rawRequest(config);
          const numDocs = getNumDocs(res);
          assert.strictEqual(numDocs, 1, 'paginator.docs.length === 10');
        });

        it('"limit" = 0 FAILS', async () => {
          const params = {limit: 0};
          const invalidConfig = {
            ...fiwareApi.getEntities.createRequestConfig(),
            params,
          };
          const err = await assertRejects(fiwareApi.rawRequest(invalidConfig));

          // DIVERGE!!
          if (fiwareApi === orionApi) {
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_BAD_PAGINATION_LIMIT_ZERO);
          }
          if (fiwareApi === ngsiApi) {
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_INVALID_PAGE_LIMIT_PARAM);
          }
        });

        it('"limit" = -1 FAILS', async () => {
          const params = {limit: -1};
          const invalidConfig = {
            ...fiwareApi.getEntities.createRequestConfig(),
            params,
          };
          const err = await assertRejects(fiwareApi.rawRequest(invalidConfig));

          // DIVERGE!!
          if (fiwareApi === orionApi) {
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_BAD_PAGINATION_LIMIT_NEG_ONE);
          }
          if (fiwareApi === ngsiApi) {
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_INVALID_PAGE_LIMIT_PARAM);
          }
        });

        // so NOTE that this guy has to be interpolated. derp.
        it('"limit"=/PRETTY MUCH ANY CRAPPY NUMBER E.G. 3.21234566 fails miserably, but MUST be interpolated', async () => {
          const config = fiwareApi.getEntities.createRequestConfig();
          _.set(config, 'params.limit', 3.21234566); // mutate!!
          const err = await assertRejects(fiwareApi.rawRequest(config));

          // DIVERGE!!
          if (fiwareApi === orionApi) {
            assertErrorsMatchish(err, ORION_ERRORS.BAD_REQ_BAD_PAGINATION_LIMIT_NEG_ONE);
          }
          if (fiwareApi === ngsiApi) {
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_INVALID_PAGE_LIMIT_PARAM);
          }
        });

        it('"limit" = 1000 succeeds', async () => {
          const config = fiwareApi.getEntities.createRequestConfig();
          _.set(config, 'params.limit', 1000); // mutate!!
          const res = await fiwareApi.rawRequest(config);
          const numDocs = getNumDocs(res);
          assert.strictEqual(numDocs, 237, 'paginator.docs.length === 237');
        });

        it('"limit" = 1001 FAILS', async () => {
          const params = {limit: 1001};
          const invalidConfig = {
            ...fiwareApi.getEntities.createRequestConfig(),
            params,
          };
          const err = await assertRejects(fiwareApi.rawRequest(invalidConfig));
          assertErrorsMatchFails(err, ORION_ERRORS.BAD_REQ_BAD_PAGINATION_LIMIT_MAX_ONE_K);

          // DIVERGE!!
          if (fiwareApi === orionApi) {
            assertErrorsMatchish(err, ORION_ERRORS.BAD_REQ_BAD_PAGINATION_LIMIT_MAX_ONE_K);
          }
          if (fiwareApi === ngsiApi) {
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_INVALID_PAGE_LIMIT_PARAM);
          }
        });

        it('"limit" = 100000000 FAILS TO FAIL (superwierd) apparantly some numbers are just too big to deny..', async () => {
          const config = fiwareApi.getEntities.createRequestConfig();
          _.set(config, 'params.limit', 101000000000); // mutate!!

          // DIVERGE!!
          if (fiwareApi === orionApi) {
            const res = await fiwareApi.rawRequest(config);
            const numDocs = getNumDocs(res);
            assert.strictEqual(numDocs, 0, 'paginator.docs.length === 0');
          }
          if (fiwareApi === ngsiApi) {
            const err = await assertRejects(fiwareApi.rawRequest(config));
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_INVALID_PAGE_LIMIT_PARAM);
          }
        });

      });

      describe('abuse the "offset" parameter', () => {

        it('"offset" = 10 succeeds', async () => {
          const params = {offset: 10};
          const config = fiwareApi.getEntities.createRequestConfig({params});
          const res = await fiwareApi.rawRequest(config);
          const paginator = createPaginator(res);
          assert.strictEqual(paginator.offset, 10, 'paginator.offset === 10');
        });

        it('"offset" = "10" succeeds', async () => {
          const params = {offset: '10'};
          const config = fiwareApi.getEntities.createRequestConfig({params});
          const res = await fiwareApi.rawRequest(config);
          const paginator = createPaginator(res);
          assert.strictEqual(paginator.offset, 10, 'paginator.offset === 10');
        });

        it('"offset" = "" (i.e empty string) FAILS', async () => {
          const params = {offset: ''};
          const invalidConfig = {
            ...fiwareApi.getEntities.createRequestConfig(),
            params,
          };
          const err = await assertRejects(fiwareApi.rawRequest(invalidConfig));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_EMPTY_OFFSET_PARAM);
        });

        it('"offset" = 0 succeeds', async () => {
          const params = {offset: 0};
          const config = fiwareApi.getEntities.createRequestConfig({params});
          const res = await fiwareApi.rawRequest(config);
          const paginator = createPaginator(res);
          assert.strictEqual(paginator.offset, 0, 'paginator.docs.offset === 0');
        });

        it('"offset" = -1 FAILS', async () => {
          const config = fiwareApi.getEntities.createRequestConfig();
          _.set(config, 'params.offset', -1); // mutate!!
          const err = await assertRejects(fiwareApi.rawRequest(config));

          // DIVERGE!!
          if (fiwareApi === orionApi) {
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_BAD_PAGINATION_OFFSET_GENERAL);
          }
          if (fiwareApi === ngsiApi) {
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_INVALID_PAGE_OFFSET_PARAM);
          }
        });

        it('"offset" = -32233.345 FAILS', async () => {
          const config = fiwareApi.getEntities.createRequestConfig();
          _.set(config, 'params.offset', -32233.345); // mutate!!
          const err = await assertRejects(fiwareApi.rawRequest(config));

          // DIVERGE!!
          if (fiwareApi === orionApi) {
            assertErrorsMatchFails(err, ORION_ERRORS.BAD_REQ_BAD_PAGINATION_OFFSET_GENERAL);
            assertErrorsMatchish(err, ORION_ERRORS.BAD_REQ_BAD_PAGINATION_OFFSET_GENERAL);
          }
          if (fiwareApi === ngsiApi) {
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_INVALID_PAGE_OFFSET_PARAM);
          }
        });

        it('"offset" = 1000 succeeds (but does not find nuthin)', async () => {
          const params = {offset: 1000};
          const config = fiwareApi.getEntities.createRequestConfig({params});
          const res = await fiwareApi.rawRequest(config);
          const numDocs = getNumDocs(res);
          assert.strictEqual(numDocs, 0, 'paginator.docs.length === 0');
        });

        it('"offset" = 100000000000 succeeds (but does not find nuthin)', async () => {
          const params = {offset: 100000000000};
          const config = fiwareApi.getEntities.createRequestConfig({params});
          const res = await fiwareApi.rawRequest(config);
          const numDocs = getNumDocs(res);
          assert.strictEqual(numDocs, 0, 'paginator.docs.length === 0');
        });

        it('"offset" = 200000000000 FAILS', async () => {
          const params = {offset: 200000000000};
          const config = fiwareApi.getEntities.createRequestConfig({params});

          // DIVERGE!!
          if (fiwareApi === orionApi) {
            const err = await assertRejects(fiwareApi.rawRequest(config));
            assertErrorsMatch(err, ORION_ERRORS.INTERNAL_SERVER_ERROR_BAD_MONGO_QUERY);
          }
          if (fiwareApi === ngsiApi) {
            const {data: docs} = await fiwareApi.rawRequest(config);
            assert.strictEqual(docs.length, 0);
          }

        });

      });

      describe('what happens near the edge (just for clarity)', () => {

        it('we have 237 entities. what happens with paginator for "offset"=227, "limit"=15 & !!count', async () => {
          const params = {options: 'count', offset: 227, limit: 15};
          const config = fiwareApi.getEntities.createRequestConfig({params});
          const res = await fiwareApi.rawRequest(config);
          // inspect the paginator
          const paginator = createPaginator(res);
          const expectedPaginator = {
            count: 237,
            nextOffset: 242,
            offset: 227,
            limit: 15,
            isFull: false,
            page: 15,
            pageOffset: 2,
            hasNextPage: false,
            numDocs: 10,
          };
          assert.deepStrictEqual(_.omit(paginator, 'docs'), expectedPaginator, 'paginator looks wrong');
          assert.strictEqual(getNumDocs(res), 10, 'there should be 10 docs');
        });

        it('we have 237 entities. what happens with paginator for "offset"=237, "limit"=15 & !!count', async () => {
          const params = {options: 'count', offset: 237, limit: 15};
          const config = fiwareApi.getEntities.createRequestConfig({params});
          const res = await fiwareApi.rawRequest(config);
          // inspect the paginator
          const paginator = createPaginator(res);
          const expectedPaginator = {
            count: 237,
            nextOffset: 252,
            offset: 237,
            limit: 15,
            isFull: false,
            page: 15,
            pageOffset: 12,
            hasNextPage: false,
            numDocs: 0,
          };
          assert.deepStrictEqual(_.omit(paginator, 'docs'), expectedPaginator, 'paginator looks wrong');
          assert.strictEqual(getNumDocs(res), 0, 'there should be 10 docs');
        });

      });

      describe('when using the getEntities (NOT raw), we can use the {count, representationType} keys, and not just the {options}', () => {

        it('check "count"', async () => {
          // const
          const params = {count: true};
          const config = fiwareApi.getEntities.createRequestConfig({params});
          const res = await fiwareApi.rawRequest(config);
          const paginator = createPaginator(res);
          assert.strictEqual(paginator.count, 237, 'paginator.count should be 237');
        });

        it('check that "append" option is usable, even if ignored', async () => {
          // const
          const params = {count: true, append: true};
          const config = fiwareApi.getEntities.createRequestConfig({params});
          assert.strictEqual(config.params.options, 'append,count');
          const res = await fiwareApi.rawRequest(config);
          const paginator = createPaginator(res);
          assert.strictEqual(paginator.count, 237, 'paginator.count should be 237');
        });

        it('check representationType', async () => {
          // const params = {options: 'values'};
          const params = {representationType: 'values'};
          const config = fiwareApi.getEntities.createRequestConfig({params});
          const res = await fiwareApi.rawRequest(config);
          const paginator = createPaginator(res);
          assert.strictEqual(paginator.docs.length, 20);
          _.each(paginator.docs, (doc, index) => {
            assert.strictEqual(_.get(doc, 0), index, 'should just be the index');
          });
        });

        it('check that {limit, offset, count, representationType} works as expected', async () => {
          // const params = {options: 'values'};
          const params = {representationType: 'values', offset: 189, count: true, limit: 17};
          const config = fiwareApi.getEntities.createRequestConfig({params});
          const res = await fiwareApi.rawRequest(config);
          const paginator = createPaginator(res);
          assert.strictEqual(paginator.docs.length, 17);
          _.each(paginator.docs, (doc, index) => {
            assert.strictEqual(_.get(doc, 0), index + 189, 'should just be the index');
          });
        });

        it('check that {count: false, options:"count"} works (count:false wins)', async () => {
          const params = {count: false, options: 'count'};
          const config = fiwareApi.getEntities.createRequestConfig({params});
          const res = await fiwareApi.rawRequest(config);
          const paginator = createPaginator(res);
          assert.strictEqual(paginator.count, null, 'should not have a count');
        });

        it('check that {representationType: "keyValues", options: "unique"} works ("keyValues" wins)', async () => {
          const params = {representationType: 'keyValues', options: 'unique'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert.strictEqual(docs.length, 20);
          assert.strictEqual(docs[19].id, 'johnny_19');
        });

      });

    });

  });

});
