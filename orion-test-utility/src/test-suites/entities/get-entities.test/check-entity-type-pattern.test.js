/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const serialPromise = require('promise-serial-exec');

const {
  assertRejects,
  ngsiResponseErrorUtils: {assertErrorsMatch},
} = require('@ratatosk/ngsi-utils');

const {ORION_ERRORS} = require('@ratatosk/ngsi-constants');

const {orionApi, ngsiApi} = require('../../../client-api-util');
const mongoApi = require('../../../mongo-api');

const entities = [
  {id: 'derp', type: 'Andy'},
  {id: 'herp', type: 'And'},
  {id: 'smerp', type: 'Mandy'},
  {id: 'erp', type: 'Dandy'},
];

describe('check-entity-type-pattern', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      before(async () => {
        await mongoApi.assertEntitiesEmpty();
        // create some entities to check against
        await serialPromise(_.map(entities, (entity) => async () => {
          return fiwareApi.createEntity({entity});
        }));
      });
      after(async () => mongoApi.wipeEntities());

      it('should find all entities without a type pattern filter', async () => {
        const {data: docs} = await fiwareApi.getEntities();
        assert.strictEqual(docs.length, 4, 'wrong number of docs');
      });

      it('should find 3 entities that match type pattern "ndy"', async () => {
        const params = {typePattern: 'ndy'};
        const {data: docs} = await fiwareApi.getEntities({params});
        assert.strictEqual(docs.length, 3, 'should be 3 "ndy"s');
      });

      it('should find 2 entities that match type pattern "andy"', async () => {
        const params = {typePattern: 'andy'};
        const {data: docs} = await fiwareApi.getEntities({params});
        assert.strictEqual(docs.length, 2, 'should be 2 "andy"s');
      });

      it('an empty "type" arg FAILS', async () => {
        const config = fiwareApi.getEntities.createRequestConfig();
        _.set(config, 'params.typePattern', ''); // mutate!
        const err = await assertRejects(fiwareApi.rawRequest(config));
        assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_EMPTY_TYPEPATTERN_PARAM);
      });

      it('check how a list of typePatterns get interpreted (e.g. NOT as a list)', async () => {
        const {data: docs} = await fiwareApi.getEntities({params: {typePattern: 'An,Dan'}});
        assert.strictEqual(docs.length, 0);
      });

    });

  });

});
