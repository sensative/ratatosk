/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const serialPromise = require('promise-serial-exec');

const {orionApi, ngsiApi} = require('../../../client-api-util');
const mongoApi = require('../../../mongo-api');

const entities = [
  {
    id: 'aaa',
    type: 'Basd',
    a: 1,
    derp: 23,
    smerp: 'asdf',
    toopy: {type: 'Irrp', value: null},
    apr: true,
    ksksks: false,
    drooops: {diggity: 'dog'},
  },
];

describe('check-multi-q-filter.test', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      before(async () => {
        await mongoApi.assertEntitiesEmpty();
        await serialPromise(_.map(entities, (entity) => async () => {
          return fiwareApi.createEntity({entity});
        }));
      });
      after(async () => mongoApi.wipeEntities());

      it('should find the entity using a complex q-query', async () => {
        const attrQuery = 'derp>=2;derp<23.3;smerp>aa;smerp!=null;toopy!=arrgh;apr;!apprr;ksksks==false;drooops';
        const params = {q: attrQuery, options: 'values'};
        const {data: docs} = await fiwareApi.getEntities({params});
        assert.strictEqual(docs.length, 1, 'we did not find it..');
      });

      it('what happens when the same argument "derp>=2" is duplicated?', async () => {
        const attrQuery = 'derp>=2;derp>=2';
        const params = {q: attrQuery, options: 'values'};
        const {data: docs} = await fiwareApi.getEntities({params});
        assert.strictEqual(docs.length, 1, 'we did not find it..');
      });

      it('what happens when the same argument "a>0" is made duplicated 1000 times? yep. works (checking if some arbitrary limit had been imposed)', async () => {
        const attrQuery = _.times(1000, () => 'a>0').join(';');
        const params = {q: attrQuery, options: 'values'};
        const {data: docs} = await fiwareApi.getEntities({params});
        assert.strictEqual(docs.length, 1, 'we did not find it..');
      });

    });

  });

});
