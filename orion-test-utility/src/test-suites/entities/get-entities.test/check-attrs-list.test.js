/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const serialPromise = require('promise-serial-exec');

const {
  assertRejects,
  ngsiResponseErrorUtils: {assertErrorsMatch},
} = require('@ratatosk/ngsi-utils');

const {ORION_ERRORS} = require('@ratatosk/ngsi-constants');

const {orionApi, ngsiApi} = require('../../../client-api-util');
const mongoApi = require('../../../mongo-api');

const entities = [
  {id: 'd0', a0: 5},
  {id: 'd1', a0: 4, a1: 1.1},
  {id: 'd2', a0: 3, a1: 1.2, a2: 2.2},
  {id: 'd3', a0: 2, a1: 1.3, a2: 2.3, a3: 3.3},
  {id: 'd4', a0: 1, a2: null, a4: 'arp'},
];

describe('check-attrs-list', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      before(async () => {
        await mongoApi.assertEntitiesEmpty();
        await serialPromise(_.map(entities, (entity) => async () => {
          return fiwareApi.createEntity({entity});
        }));
      });
      after(async () => mongoApi.wipeEntities());

      it('retrieving the entities by keyValues, filtered attrs list', async () => {
        const params = {options: 'keyValues', attrs: ['a1', 'a2', 'a3', 'a4']}; // <--- a0 omitted!!
        const {data: docs} = await fiwareApi.getEntities({params});
        // check all the docs individually
        const expecteds = _.map(entities, (template) => _.assign(
          _.omit(template, 'a0'),
          {type: 'Thing'}, // <----- Default value
        ));
        // make sure that NO keys were removed or added UNEXPECTEDLY
        _.each(expecteds, (expected) => {
          const doc = _.find(docs, {id: expected.id});
          assert.deepStrictEqual(doc, expected);
        });
      });

      it('retrieving the entities by "values", filtered attrs list. NOTICE that missing props simply get smooshed out. no placeholders', async () => {
        const params = {options: 'values', attrs: ['a1', 'a2', 'a3', 'a4'], orderBy: 'a0'}; // <--- a0 omitted!!
        const {data: docs} = await fiwareApi.getEntities({params});
        const expected = [
          [null, 'arp'],
          [1.3, 2.3, 3.3],
          [1.2, 2.2],
          [1.1],
          [],
        ];
        docs.sort();
        expected.sort();
        assert.deepStrictEqual(docs, expected, 'docs look wrong');
      });


      it('some varieties that are VALID', async () => {
        const validAttrs = [
          'a0',
          'a0,a1',
        ];
        await serialPromise(_.map(validAttrs, (attrs) => async () => {
          const params = {attrs: 'placeholder', id: 'd1', options: 'values'};
          const config = fiwareApi.getEntities.createRequestConfig({params});
          _.set(config, 'params.attrs', attrs); // mutate!!
          const {data: docs} = await fiwareApi.rawRequest(config);
          assert.strictEqual(docs.length, 1, 'should have 1 docs');
        }));

      });

      it('duplicate entries are allowed in Orion but FAIL in ngsi-server', async () => {
        const attrsLists = [
          'a,a', // any dupes
          'a,b,c,d,a',
          ',', // empty strings count as duplicates
          ',,,,,,,,',
          ',,,,,,,,,false,,,,,,,,,,,,10,,,,,,,,,,,,,,,,,,,,,,3.3,,,,,,,,,,,,,,,,,,,,a',
          'a,,', // wherever they occur
          ',a,',
        ];
        await serialPromise(_.map(attrsLists, (attrs) => async () => {
          const params = {attrs: 'placeholder', id: 'd1', options: 'values'};
          const config = fiwareApi.getEntities.createRequestConfig({params});
          _.set(config, 'params.attrs', attrs); // mutate!!

          // DIVERGE!!
          if (fiwareApi === orionApi) {
            const {data: docs} = await fiwareApi.rawRequest(config);
            assert.strictEqual(docs.length, 1, 'should have 1 docs');
          }
          if (fiwareApi === ngsiApi) {
            const err = await assertRejects(fiwareApi.rawRequest(config));
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_ATTRS_PARAM_DUPLICATE);
          }
        }));
      });

      it('asdf', async () => {
        const attrsLists = [
          ',a',
          'a,',
          'has_#',
          'has_a_period.',
          'includes space',
        ];
        await serialPromise(_.map(attrsLists, (attrs) => async () => {
          const params = {attrs: 'placeholder', id: 'd1', options: 'values'};
          const config = fiwareApi.getEntities.createRequestConfig({params});
          _.set(config, 'params.attrs', attrs); // mutate!!

          // DIVERGE!!
          if (fiwareApi === orionApi) {
            const {data: docs} = await fiwareApi.rawRequest(config);
            assert.strictEqual(docs.length, 1);
          }
          if (fiwareApi === ngsiApi) {
            const err = await assertRejects(fiwareApi.rawRequest(config));
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_ATTRS_PARAM_INVALID_ATTRNAME);
          }
        }));
      });

      it('an empty arg FAILS', async () => {
        const emptyAttrs = [
          '',
        ];
        await serialPromise(_.map(emptyAttrs, (attrs) => async () => {
          const params = {attrs: 'placeholder'};
          const config = fiwareApi.getEntities.createRequestConfig({params});
          _.set(config, 'params.attrs', attrs); // mutate!!
          const err = await assertRejects(fiwareApi.rawRequest(config));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_EMPTY_ATTRS_PARAM);
        }));
      });

      it('the empties are allowed all around', async () => {
        const empties = [
          undefined,
          null,
          [],
        ];
        const params = {attrs: 'placeholder', options: 'values', orderBy: 'a0'};
        const config = fiwareApi.getEntities.createRequestConfig({params});
        await serialPromise(_.map(empties, (attrs) => async () => {
          _.set(config, 'params.attrs', attrs); // mutate!!
          const {data: docs} = await fiwareApi.rawRequest(config);
          const expected = [
            [1, null, 'arp'],
            [2, 1.3, 2.3, 3.3],
            [3, 1.2, 2.2],
            [4, 1.1],
            [5],
          ];
          docs.sort();
          expected.sort();
          assert.deepStrictEqual(docs, expected);
        }));
      });

      it('stuff that fails to throw an error (they get stringified by axios)', async () => {
        const wierdOnes = [
          {}, // -> '{}'
          10, // -> '10'
          false, // -> 'false'
          true, // -> true
        ];
        const params = {attrs: 'placeholder', options: 'values', orderBy: 'a0'};
        const config = fiwareApi.getEntities.createRequestConfig({params});
        await serialPromise(_.map(wierdOnes, (attrs) => async () => {
          _.set(config, 'params.attrs', attrs); // mutate!!
          const {data: docs} = await fiwareApi.rawRequest(config);
          const expected = [[], [], [], [], []]; // since the above attrs do not exist
          assert.deepStrictEqual(docs, expected);
        }));
      });

    });

  });

});
