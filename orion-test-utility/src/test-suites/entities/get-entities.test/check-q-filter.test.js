/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const serialPromise = require('promise-serial-exec');

const {
  assertRejects,
  ngsiResponseErrorUtils: {assertErrorsMatch},
} = require('@ratatosk/ngsi-utils');

const {ORION_ERRORS} = require('@ratatosk/ngsi-constants');

const {orionApi, ngsiApi} = require('../../../client-api-util');
const mongoApi = require('../../../mongo-api');

// create some entities to check against. ALL of these will be accessible
// to find calls throughout this file
const entities = [
  {id: 'oberp', type: 'Objecty', derp: {subderp: {a: {b: [0, 1, {c: 2.3}]}}}},
  {id: 'alerp', derp: 'aaa222_true'},
  {id: 'blerp', derp: 'bbb'},
  {id: 'clerp', derp: 'ccc_null'},
  {id: 'querp', derp: {type: 'Wert', value: null}},
  {id: 'verp', derp: true},
  {id: 'zerp', derp: false},
  {id: 'nerp', derp: 1},
  {id: 'derp', derp: 2},
  {id: 'smerp', derp: 4},
  {id: 'cherp', derp: '2'},
  {id: 'lerp', nav: 'car'},
  {id: 'sterp', nav: 'groovy'},
  {id: 'harp', nav: 'maven'},
];

describe('check-q-filter (filtering with a attrs query)', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      before(async () => {
        await mongoApi.assertEntitiesEmpty();
        // create some entities to check against
        await serialPromise(_.map(entities, (entity) => async () => {
          return fiwareApi.createEntity({entity});
        }));
      });
      after(async () => mongoApi.wipeEntities());

      describe('EXISTENCE and NON-EXISTENCE', () => {

        it('EXISTENCE: finds 3 entities with "nav" attrs', async () => {
          const params = {q: 'nav'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert.strictEqual(docs.length, 3, 'did not find 3 "nav" entities');
        });

        const numNonNavs = entities.length - 3;
        it(`NON-EXISTENCE: finds ${numNonNavs} entities with only non-"nav" attrs`, async () => {
          const params = {q: '!nav'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert.strictEqual(docs.length, numNonNavs, `did not find ${numNonNavs} non-"nav" entities`);
        });

      });

      describe('strings', () => {

        it('EQUALITY (==string): find 1 with "derp==bbb"', async () => {
          const params = {q: 'derp==bbb', options: 'values'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert(docs.length === 1, 'there should only be one "bbb"');
          const expected = ['bbb'];
          assert.deepStrictEqual(docs[0], expected, 'doc looks wrong');
        });

        it('COLON-EQUALITY (:string): find 1 with "derp:bbb"', async () => {
          // This one just here to show that : is synonymous to ==
          const params = {q: 'derp:bbb', options: 'values'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert(docs.length === 1, 'there should only be one "bbb"');
          const expected = ['bbb'];
          assert.deepStrictEqual(docs[0], expected, 'doc looks wrong');
        });

        it('NOT-EQUAL (!=string): find those with derp!=bbb', async () => {
          // NOTE that NOT-EQUAL does NOT type-match
          const params = {q: 'derp!=bbb', options: 'values'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert.strictEqual(docs.length, 10, 'there should be 10 non-"bbb"');
          const expected = [
            [true],
            [false],
            [1],
            [2],
            [4],
            [null],
            ['2'],
            ['aaa222_true'],
            ['ccc_null'],
            [{subderp: {a: {b: [0, 1, {c: 2.3}]}}}],
          ];
          assert.deepStrictEqual(docs.sort(), expected.sort(), 'docs look wrong');
        });

        it('INEQUALITY (>string): find with "derp>bbb"', async () => {
          const params = {q: 'derp>bbb', options: 'values'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert(docs.length === 1, 'there should only be one "derp>bbb"');
          const expected = ['ccc_null'];
          assert.deepStrictEqual(docs[0], expected, 'doc looks wrong');
        });

        it('INEQUALITY (>=string): find with "derp>=bbb"', async () => {
          const params = {q: 'derp>=bbb', options: 'values'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert(docs.length === 2, 'there should be 2 "derp>=bbb"');
          const expected = [['bbb'], ['ccc_null']];
          assert.deepStrictEqual(docs.sort(), expected.sort(), 'docs looks wrong');
        });

        it('INEQUALITY (<string): find with "derp<bbb"', async () => {
          const params = {q: 'derp<bbb', options: 'values'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert(docs.length === 2, 'there should only be one "derp<bbb"');
          const expected = [['aaa222_true'], ['2']];
          assert.deepStrictEqual(docs, expected, 'docs look wrong');
        });

        it('INEQUALITY (<=string): find with "derp<=bbb"', async () => {
          const params = {q: 'derp<=bbb', options: 'values'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert.strictEqual(docs.length, 3);
          const expected = [['aaa222_true'], ['bbb'], ['2']];
          assert.deepStrictEqual(docs.sort(), expected.sort());
        });

        it('PATTERN-MATCH (~=pattern): find with derp~=b', async () => {
          const params = {q: 'derp~=b', options: 'values'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert(docs.length === 1, 'there should only be one "derp~=b"');
          const expected = ['bbb'];
          assert.deepStrictEqual(docs[0], expected, 'doc looks wrong');
        });

        it('MATCH_PATTERN: try to find a number value with derp~=\'2\'', async () => {
          // const params = {q: 'derp~=2', options: 'values'};
          const params = {q: 'derp~=\'2\'', options: 'values'};
          // DIVERGE!!
          if (fiwareApi === orionApi) {
            const err = await assertRejects(fiwareApi.getEntities({params}));
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_FORBIDDEN_CHARS);
          }
          if (fiwareApi === ngsiApi) {
            const {data: docs} = await fiwareApi.getEntities({params});
            const expected = [['2'], ['aaa222_true']];
            assert.deepStrictEqual(docs.sort(), expected.sort());
          }
        });

        it('MATCH_PATTERN: try uriencoding derp~=\'2\' (i.e. derp~=%272%27) WORKS(ish) but nothing is found (for both). %272%27 does NOT get decoded', async () => {
          const params = {q: 'derp~=%272%27'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert.strictEqual(docs.length, 0);
        });

      });

      describe('numbers', () => {

        it('EQUALITY (==number): find 1 with "derp==1"', async () => {
          const params = {q: 'derp==1', options: 'values'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert(docs.length === 1, 'there should only be one derp==1');
          const expected = [1];
          assert.deepStrictEqual(docs[0], expected, 'doc looks wrong');
        });

        it('COLON-EQUALITY (:number): find 1 with "derp:1"', async () => {
          const params = {q: 'derp:1', options: 'values'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert(docs.length === 1, 'there should only be one derp:1');
          const expected = [1];
          assert.deepStrictEqual(docs[0], expected, 'doc looks wrong');
        });

        it('UNEQUAL (!=number): find those with derp!=1', async () => {
          const params = {q: 'derp!=1', options: 'values'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert.strictEqual(docs.length, 10, 'there should only be 9 non-"1"s');
          const expected = [[4], [2], [null], ['2'], ['aaa222_true'], ['bbb'], ['ccc_null'], [false], [true], [{subderp: {a: {b: [0, 1, {c: 2.3}]}}}]];
          assert.deepStrictEqual(docs.sort(), expected.sort(), 'docs look wrong');
        });

        it('INEQUALITY (>number) find with "derp>2"', async () => {
          const params = {q: 'derp>2', options: 'values'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert(docs.length === 1, 'there should only be one "derp>2"');
          const expected = [4];
          assert.deepStrictEqual(docs[0], expected, 'doc looks wrong');
        });

        it('INEQUALITY (>=number) find with "derp>=2"', async () => {
          const params = {q: 'derp>=2', options: 'values'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert(docs.length === 2, 'there should only be one "derp>=2"');
          const expected = [[2], [4]];
          assert.deepStrictEqual(docs.sort(), expected.sort(), 'docs look wrong');
        });

        it('INEQUALITY (<number) find with "derp<2"', async () => {
          // Note that the inequality only picks out those entities where
          // derp is a number
          const params = {q: 'derp<2', options: 'values'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert(docs.length === 1, 'there should only be one "derp<2"');
          const expected = [1];
          assert.deepStrictEqual(docs[0], expected, 'doc looks wrong');
        });

        it('INEQUALITY (<=number) find with "derp<=2"', async () => {
          const params = {q: 'derp<=2', options: 'values'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert(docs.length === 2, 'there should only be one "derp<=2"');
          const expected = [[2], [1]];
          assert.deepStrictEqual(docs.sort(), expected.sort(), 'docs look wrong');
        });

        it('MATCH_PATTERN (~=number) find with derp~=2 SHOULD fail', async () => {
          const params = {q: 'derp~=placeholder', options: 'values'};
          const config = fiwareApi.getEntities.createRequestConfig({params});
          _.set(config, 'params.q', 'derp~=2'); // mutate!!

          // DIVERGE!
          if (fiwareApi === orionApi) {
            // Orion accepts values and autoboxes them to strings. this is BS.
            const {data: docs} = await fiwareApi.rawRequest(config);
            assert(docs.length === 2, 'there should be 2 found for "derp~=2"');
            const expected = [['aaa222_true'], ['2']];
            assert.deepStrictEqual(docs, expected, 'doc looks wrong');
          }
          if (fiwareApi === ngsiApi) {
            const err = await assertRejects(ngsiApi.rawRequest(config));
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_OP_MATCHPATTERN_NGSI);
          }
        });

      });

      describe('booleans', async () => {

        it('EQUALITY (==true): find 1 with "derp==true"', async () => {
          const params = {q: 'derp==true', options: 'values'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert(docs.length === 1, 'there should only be one derp==true');
          const expected = [true];
          assert.deepStrictEqual(docs[0], expected, 'doc looks wrong');
        });

        it('EQUALITY (==false): find some with "derp==false"', async () => {
          const params = {q: 'derp==false', options: 'values'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert(docs.length === 1, 'there should only be one derp==false');
          const expected = [false];
          assert.deepStrictEqual(docs[0], expected, 'doc looks wrong');
        });

        it('COLON-EQUALITY (:true): find 1 with "derp:true"', async () => {
          const params = {q: 'derp:true', options: 'values'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert(docs.length === 1, 'there should only be one derp:true');
          const expected = [true];
          assert.deepStrictEqual(docs[0], expected, 'doc looks wrong');
        });

        it('COLON-EQUALITY (:false): find some with "derp:false"', async () => {
          const params = {q: 'derp:false', options: 'values'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert(docs.length === 1, 'there should only be one derp:false');
          const expected = [false];
          assert.deepStrictEqual(docs[0], expected, 'doc looks wrong');
        });

        it('INEQUALITY (<true/false): find with "derp<true" or "derp<false" FAILS', async () => {
          const params = {q: 'derp<boolean_placeholder'};
          const config = fiwareApi.getEntities.createRequestConfig({params});
          // modify for TRUE
          _.set(config, 'params.q', 'derp<true'); // mutate!!
          const errTrue = await assertRejects(fiwareApi.rawRequest(config));
          // DIVERGE!!
          if (fiwareApi === orionApi) {
            assertErrorsMatch(errTrue, ORION_ERRORS.BAD_REQ_OP_LESSTHAN_ON_BOOL);
          }
          if (fiwareApi === ngsiApi) {
            assertErrorsMatch(errTrue, ORION_ERRORS.BAD_REQ_OP_LESSTHAN_NGSI);
          }
          // modify for FALSE
          _.set(config, 'params.q', 'derp<false'); // mutate!!
          const errFalse = await assertRejects(fiwareApi.rawRequest(config));
          // DIVERGE!!
          if (fiwareApi === orionApi) {
            assertErrorsMatch(errFalse, ORION_ERRORS.BAD_REQ_OP_LESSTHAN_ON_BOOL);
          }
          if (fiwareApi === ngsiApi) {
            assertErrorsMatch(errFalse, ORION_ERRORS.BAD_REQ_OP_LESSTHAN_NGSI);
          }
        });

        it('INEQUALITY (<=true/false): find with "derp<=true" or "derp<=false" FAILS', async () => {
          const params = {q: 'derp<=boolean_placeholder'};
          const config = fiwareApi.getEntities.createRequestConfig({params});
          // modify for TRUE
          _.set(config, 'params.q', 'derp<=true'); // mutate!!
          const errTrue = await assertRejects(fiwareApi.rawRequest(config));
          // DIVERGE!!
          if (fiwareApi === orionApi) {
            assertErrorsMatch(errTrue, ORION_ERRORS.BAD_REQ_OP_LESSTHANOREQUAL_ON_BOOL);
          }
          if (fiwareApi === ngsiApi) {
            assertErrorsMatch(errTrue, ORION_ERRORS.BAD_REQ_OP_LESSTHANOREQUAL_NGSI);
          }
          // modify for FALSE
          _.set(config, 'params.q', 'derp<=false'); // mutate!!
          const errFalse = await assertRejects(fiwareApi.rawRequest(config));
          // DIVERGE!!
          if (fiwareApi === orionApi) {
            assertErrorsMatch(errFalse, ORION_ERRORS.BAD_REQ_OP_LESSTHANOREQUAL_ON_BOOL);
          }
          if (fiwareApi === ngsiApi) {
            assertErrorsMatch(errFalse, ORION_ERRORS.BAD_REQ_OP_LESSTHANOREQUAL_NGSI);
          }
        });

        it('INEQUALITY (>true/false): find with "derp>true" or "derp>false" FAILS', async () => {
          const params = {q: 'derp>boolean_placeholder'};
          const config = fiwareApi.getEntities.createRequestConfig({params});
          // modify for TRUE
          _.set(config, 'params.q', 'derp>true'); // mutate!!
          const errTrue = await assertRejects(fiwareApi.rawRequest(config));
          // DIVERGE!!
          if (fiwareApi === orionApi) {
            assertErrorsMatch(errTrue, ORION_ERRORS.BAD_REQ_OP_GREATERTHAN_ON_BOOL);
          }
          if (fiwareApi === ngsiApi) {
            assertErrorsMatch(errTrue, ORION_ERRORS.BAD_REQ_OP_GREATERTHAN_NGSI);
          }
          // modify for FALSE
          _.set(config, 'params.q', 'derp>false'); // mutate!!
          const errFalse = await assertRejects(fiwareApi.rawRequest(config));
          // DIVERGE!!
          if (fiwareApi === orionApi) {
            assertErrorsMatch(errFalse, ORION_ERRORS.BAD_REQ_OP_GREATERTHAN_ON_BOOL);
          }
          if (fiwareApi === ngsiApi) {
            assertErrorsMatch(errFalse, ORION_ERRORS.BAD_REQ_OP_GREATERTHAN_NGSI);
          }
        });

        it('INEQUALITY (>=true/false): find with "derp>=true" or "derp>=false" FAILS', async () => {
          const params = {q: 'derp>=boolean_placeholder'};
          const config = fiwareApi.getEntities.createRequestConfig({params});
          // modify for TRUE
          _.set(config, 'params.q', 'derp>=true'); // mutate!!
          const errTrue = await assertRejects(fiwareApi.rawRequest(config));
          // DIVERGE!!
          if (fiwareApi === orionApi) {
            assertErrorsMatch(errTrue, ORION_ERRORS.BAD_REQ_OP_GREATERTHANOREQUAL_ON_BOOL);
          }
          if (fiwareApi === ngsiApi) {
            assertErrorsMatch(errTrue, ORION_ERRORS.BAD_REQ_OP_GREATERTHANOREQUAL_NGSI);
          }
          // modify for FALSE
          _.set(config, 'params.q', 'derp>=false'); // mutate!!
          const errFalse = await assertRejects(fiwareApi.rawRequest(config));
          // DIVERGE!!
          if (fiwareApi === orionApi) {
            assertErrorsMatch(errFalse, ORION_ERRORS.BAD_REQ_OP_GREATERTHANOREQUAL_ON_BOOL);
          }
          if (fiwareApi === ngsiApi) {
            assertErrorsMatch(errFalse, ORION_ERRORS.BAD_REQ_OP_GREATERTHANOREQUAL_NGSI);
          }
        });

        it('MATCH_PATTERN (~=true) find with derp~=true', async () => {
          const params = {q: 'derp~=true_placeholder', options: 'values'};
          const config = fiwareApi.getEntities.createRequestConfig({params});
          _.set(config, 'params.q', 'derp~=true'); // mutate
          // DIVERGE!!
          if (fiwareApi === orionApi) {
            // NOTE: true is interpreted as a string
            const {data: docs} = await fiwareApi.rawRequest(config);
            assert(docs.length === 1, 'there should be one derp~=true');
            const expected = ['aaa222_true'];
            assert.deepStrictEqual(docs[0], expected, 'doc looks wrong');
          }
          if (fiwareApi === ngsiApi) {
            const err = await assertRejects(fiwareApi.rawRequest(config));
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_OP_MATCHPATTERN_NGSI);
          }
        });

        it('MATCH_PATTERN (~=false) find with derp~=false', async () => {
          const params = {q: 'derp~=false_placeholder', options: 'values'};
          const config = fiwareApi.getEntities.createRequestConfig({params});
          _.set(config, 'params.q', 'derp~=false'); // mutate
          // DIVERGE!!
          if (fiwareApi === orionApi) {
            // NOTE: true is interpreted as a string
            const {data: docs} = await fiwareApi.rawRequest(config);
            assert(docs.length === 0, 'there should be one derp~=false');
          }
          if (fiwareApi === ngsiApi) {
            const err = await assertRejects(fiwareApi.rawRequest(config));
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_OP_MATCHPATTERN_NGSI);
          }
        });

      });

      describe('nullz', async () => {

        it('EQUALITY (==null): find 1 with "derp==null"', async () => {
          const params = {q: 'derp==null', options: 'values'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert(docs.length === 1, 'there should only be one derp==null');
          const expected = [null];
          assert.deepStrictEqual(docs[0], expected, 'doc looks wrong');
        });

        it('COLON-EQUALITY (:null): find 1 with "derp:null"', async () => {
          const params = {q: 'derp:null', options: 'values'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert(docs.length === 1, 'there should only be one derp:null');
          const expected = [null];
          assert.deepStrictEqual(docs[0], expected, 'doc looks wrong');
        });

        it('NOT-EQUAL (!=null): find those with "derp!=null"', async () => {
          const params = {q: 'derp!=null', options: 'values'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert.strictEqual(docs.length, 10, 'there should be 10 derp!=null');
          const expected = [[true], [false], ['aaa222_true'], ['bbb'], ['ccc_null'], [1], [2], [4], ['2'], [{subderp: {a: {b: [0, 1, {c: 2.3}]}}}]];
          assert.deepStrictEqual(docs.sort(), expected.sort(), 'docs look wrong');
        });

        it('INEQUALITY (>null): find those with "derp>null FAILS"', async () => {
          const params = {q: 'derp>null_placeholder'};
          const config = fiwareApi.getEntities.createRequestConfig({params});
          _.set(config, 'params.q', 'derp>null'); // mutate!!
          const err = await assertRejects(fiwareApi.rawRequest(config));
          // DIVERGE!!
          if (fiwareApi === orionApi) {
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_OP_GREATERTHAN_ON_NULL);
          }
          if (fiwareApi === ngsiApi) {
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_OP_GREATERTHAN_NGSI);
          }
        });

        it('INEQUALITY (>=null): find those with "derp>=null FAILS"', async () => {
          const params = {q: 'derp>=null_placeholder'};
          const config = fiwareApi.getEntities.createRequestConfig({params});
          _.set(config, 'params.q', 'derp>=null'); // mutate!!
          const err = await assertRejects(fiwareApi.rawRequest(config));
          // DIVERGE!!
          if (fiwareApi === orionApi) {
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_OP_GREATERTHANOREQUAL_ON_NULL);
          }
          if (fiwareApi === ngsiApi) {
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_OP_GREATERTHANOREQUAL_NGSI);
          }
        });

        it('INEQUALITY (<null): find those with "derp<null FAILS"', async () => {
          const params = {q: 'derp<null_placeholder'};
          const config = fiwareApi.getEntities.createRequestConfig({params});
          _.set(config, 'params.q', 'derp<null'); // mutate!!
          const err = await assertRejects(fiwareApi.rawRequest(config));
          // DIVERGE!!
          if (fiwareApi === orionApi) {
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_OP_LESSTHAN_ON_NULL);
          }
          if (fiwareApi === ngsiApi) {
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_OP_LESSTHAN_NGSI);
          }
        });

        it('INEQUALITY (<=null): find those with "derp<=null FAILS"', async () => {
          const params = {q: 'derp<=null_placeholder'};
          const config = fiwareApi.getEntities.createRequestConfig({params});
          _.set(config, 'params.q', 'derp<=null'); // mutate!!
          const err = await assertRejects(fiwareApi.rawRequest(config));
          // DIVERGE!!
          if (fiwareApi === orionApi) {
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_OP_LESSTHANOREQUAL_ON_NULL);
          }
          if (fiwareApi === ngsiApi) {
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_OP_LESSTHANOREQUAL_NGSI);
          }
        });

        it('MATCH_PATTERN (~=null) find with derp~=null', async () => {

          const params = {q: 'derp~=null_placeholder', options: 'values'};
          const config = fiwareApi.getEntities.createRequestConfig({params});
          _.set(config, 'params.q', 'derp~=null'); // mutate
          // DIVERGE!!
          if (fiwareApi === orionApi) {
            // NOTE: true is interpreted as a string
            const {data: docs} = await fiwareApi.rawRequest(config);
            assert(docs.length === 1, 'there should be one derp~=true');
            const expected = ['ccc_null'];
            assert.deepStrictEqual(docs[0], expected, 'doc looks wrong');
          }
          if (fiwareApi === ngsiApi) {
            const err = await assertRejects(fiwareApi.rawRequest(config));
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_OP_MATCHPATTERN_NGSI);
          }
        });

      });

      describe('trying to use multiple tokens to dig into attr.value', () => {

        it('EQUALITY (multiple tokens): find 1 with "derp.subderp.a.b.2.c==2.3" works!', async () => {
          const params = {q: 'derp.subderp.a.b.2.c==2.3'};
          const {data: docs} = await fiwareApi.getEntities({params});
          assert(docs.length === 1, 'Should be only 1 doc');
          assert.strictEqual(_.get(docs[0], 'derp.value.subderp.a.b.2.c'), 2.3, 'Could not find correct doc');
        });

      });

    });

  });

});
