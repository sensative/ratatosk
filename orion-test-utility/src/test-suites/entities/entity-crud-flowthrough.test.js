/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

// This essentially tests that a simple case CRUD cycle works as expected

const assert = require('assert');
const _ = require('lodash');

const {
  // assertRejects,
  ngsiEntityFormatter: {formatAttribute},
} = require('@ratatosk/ngsi-utils');

const {orionApi, ngsiApi} = require('../../client-api-util');
const mongoApi = require('../../mongo-api');

describe('entity-crud-flowthrough', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      let now;
      before(async () => {
        now = Math.floor(Date.now() / 1000);
        await mongoApi.assertEntitiesEmpty();
      });

      after(async () => {
        await mongoApi.wipeEntities();
      });

      it('create a simple entity', async () => {
        const entity = {type: 'Blooey', id: 'blerks'};
        const res = await fiwareApi.createEntity({entity});
        assert.strictEqual(res.status, 201);
        const numEntities = await mongoApi.countEntities();
        assert.strictEqual(numEntities, 1, 'there should be 1 entity');
      });

      it('mongodb doc for simple entity should look as expected', async () => {
        const docs = await mongoApi.findEntities();
        assert.strictEqual(docs.length, 1, 'should just be the one..');
        const entity = docs[0];
        assert(entity.creDate >= now, 'should have a valid entity.creDate');
        assert(entity.modDate >= now, 'should have a valid entity.modDate');
        // assert(_.isString(entity.lastCorrelator), 'there should be a lastCorrelator (string)');

        // DIVERGE!!
        let expectedEntity;
        if (fiwareApi === orionApi) {
          expectedEntity = {
            _id: {
              id: 'blerks', type: 'Blooey', servicePath: '/',
            },
            attrNames: [],
            attrs: {},
          };
        }
        if (fiwareApi === ngsiApi) {
          assert.deepStrictEqual(entity._id.constructor.name, 'ObjectID');
          expectedEntity = {
            _id: entity._id,
            orionId: {
              id: 'blerks', type: 'Blooey', servicePath: '/',
            },
            attrs: {},
          };
        }
        const prunedDoc = _.omit(entity, ['creDate', 'modDate', 'lastCorrelator']);
        assert.deepStrictEqual(expectedEntity, prunedDoc, 'mongodb doc does not look as expected');
      });

      it('api should find the entity, and it should look correct', async () => {
        const res = await fiwareApi.getEntities();
        assert.strictEqual(res.status, 200);
        const orionDocs = res.data;
        assert.strictEqual(orionDocs.length, 1, 'We should have the 1 entity');
        const expectedEntity = {
          id: 'blerks',
          type: 'Blooey',
        };
        assert.deepStrictEqual(expectedEntity, orionDocs[0], 'the orion doc looks invalid');
      });

      it('api should find the entity by id, and it should look correct', async () => {
        const res = await fiwareApi.findEntity({entityId: 'blerks'});
        assert.strictEqual(res.status, 200);
        const orionDoc = res.data;
        assert(orionDoc, 'We should have the entity');
        const expectedEntity = {
          id: 'blerks',
          type: 'Blooey',
        };
        assert.deepStrictEqual(expectedEntity, orionDoc, 'the orion doc looks invalid');
      });

      it('api replaceEntityAttrs should update the entity, and it should look correct', async () => {
        const reqArg = {entityId: 'blerks', attrs: {spud: 2}, params: {type: 'Blooey'}};
        const res = await fiwareApi.replaceEntityAttrs(reqArg);
        assert.strictEqual(res.status, 204);
        const {data: doc} = await fiwareApi.findEntity(reqArg);
        const expectedEntity = {
          id: 'blerks',
          type: 'Blooey',
          spud: {type: 'Number', value: 2, metadata: {}},
        };
        assert.deepStrictEqual(doc, expectedEntity, 'the orion doc looks invalid');
      });

      it('api replaceEntityAttrs should overwrite all props (undo the previous test)', async () => {
        const reqArg = {entityId: 'blerks', attrs: {derps: 'asdf', merps: 2}, params: {type: 'Blooey'}};
        const res = await fiwareApi.replaceEntityAttrs(reqArg);
        assert.strictEqual(res.status, 204);
        const {data: doc} = await fiwareApi.findEntity(reqArg);
        const expectedEntity = {
          id: 'blerks',
          type: 'Blooey',
          derps: {type: 'Text', value: 'asdf', metadata: {}},
          merps: {type: 'Number', value: 2, metadata: {}},
        };
        assert.deepStrictEqual(doc, expectedEntity, 'the orion doc looks invalid');
      });

      it('api upsertEntityAttrs should overwrite all props too? (difference from replaceEntityAttrs: unmentioned pre-existing attrs get wiped(?))', async () => {
        const attrs = {derps: 'asdffff', merps: 2};
        const reqArg = {entityId: 'blerks', params: {type: 'Blooey'}, attrs};
        const res = await fiwareApi.upsertEntityAttrs(reqArg);
        assert.strictEqual(res.status, 204);
        const {data: doc} = await fiwareApi.findEntity(reqArg);
        const expectedEntity = {
          id: 'blerks',
          type: 'Blooey',
          derps: {type: 'Text', value: 'asdffff', metadata: {}},
          merps: {type: 'Number', value: 2, metadata: {}},
        };
        assert.deepStrictEqual(doc, expectedEntity, 'the orion doc looks invalid');
      });

      it('api getEntityAttrs should return the entity attrs (simplest usage case..)', async () => {
        const res = await fiwareApi.getEntityAttrs({entityId: 'blerks'});
        assert.strictEqual(res.status, 200);
        const docAttrs = res.data;
        const expectedAttrs = {
          derps: {type: 'Text', value: 'asdffff', metadata: {}},
          merps: {type: 'Number', value: 2, metadata: {}},
        };
        assert.deepStrictEqual(expectedAttrs, docAttrs, 'the orion attrs looks invalid');
      });

      it('api findEntityAttr should return the desired attr', async () => {
        const reqArg = {entityId: 'blerks', attrName: 'merps'};
        const res = await fiwareApi.findEntityAttr(reqArg);
        assert.strictEqual(res.status, 200);
        const attr = res.data;
        const expectedAttr = {type: 'Number', value: 2, metadata: {}};
        assert.deepStrictEqual(attr, expectedAttr, 'the orion attr looks invalid');
      });

      it('api updateEntityAttr should update the desired attr', async () => {
        const attr = formatAttribute({
          type: 'Bloowaai',
          value: 45,
          metadata: {dingle: 'Barry'},
        });
        const reqArg = {entityId: 'blerks', attrName: 'merps', attr};
        const res = await fiwareApi.updateEntityAttr(reqArg);
        assert.strictEqual(res.status, 204, 'status should be 204');
        const {data: savedAttr} = await fiwareApi.findEntityAttr(reqArg);
        assert.deepStrictEqual(savedAttr, attr);
      });

      it('api findEntityAttrValue should find what is expected', async () => {
        const reqArg = {entityId: 'blerks', attrName: 'merps'};
        const res = await fiwareApi.findEntityAttrValue(reqArg);
        assert.strictEqual(res.status, 200, 'status should be 200');
        const expected = 45;
        assert.strictEqual(res.data, expected, 'not getting the correct attr value');
      });

      it('api updateEntityAttrValue should overwrite existing attr.value', async () => {
        const attrValue = 'shimtoddy';
        const reqArg = {entityId: 'blerks', attrName: 'derps', attrValue};
        const res = await fiwareApi.updateEntityAttrValue(reqArg);
        assert.strictEqual(res.status, 204);
        const {data: orionDocs} = await fiwareApi.getEntities();
        assert.strictEqual(orionDocs.length, 1, 'We should have the 1 entity');
        const expectedEntity = {
          id: 'blerks',
          type: 'Blooey',
          derps: {type: 'Text', value: 'shimtoddy', metadata: {}},
          merps: {
            type: 'Bloowaai',
            value: 45,
            metadata: {
              dingle: {type: 'Text', value: 'Barry'},
            },
          },
        };
        assert.deepStrictEqual(expectedEntity, orionDocs[0], 'the orion doc looks invalid');
      });

      it('api patchEntityAttrs should update selected attrs, leaving rest alone', async () => {
        const attrs = {merps: {value: 77, metadata: {schmo: 3.5}}};
        const reqArg = {entityId: 'blerks', params: {type: 'Blooey'}, attrs};
        const res = await fiwareApi.patchEntityAttrs(reqArg);
        assert.strictEqual(res.status, 204);
        const {data: doc} = await fiwareApi.findEntity(reqArg);
        const expectedEntity = {
          id: 'blerks',
          type: 'Blooey',
          derps: {type: 'Text', value: 'shimtoddy', metadata: {}},
          merps: {
            type: 'Number',
            value: 77,
            metadata: {
              schmo: {type: 'Number', value: 3.5},
            },
          },
        };
        assert.deepStrictEqual(doc, expectedEntity, 'the orion doc looks invalid');
      });

      it('api deleteEntityAttr should delete selected attr from entity', async () => {
        const reqArg = {entityId: 'blerks', attrName: 'merps'};
        const res = await fiwareApi.deleteEntityAttr(reqArg);
        assert.strictEqual(res.status, 204);
        const {data: orionDocs} = await fiwareApi.getEntities();
        assert.strictEqual(orionDocs.length, 1, 'We should have the 1 entity');
        const expectedEntity = {
          id: 'blerks',
          type: 'Blooey',
          derps: {type: 'Text', value: 'shimtoddy', metadata: {}},
        };
        assert.deepStrictEqual(expectedEntity, orionDocs[0], 'the orion doc looks invalid');
      });

      it('api deleteEntity should delete the entity', async () => {
        const entityId = 'blerks';
        const res = await fiwareApi.deleteEntity({entityId});
        assert.strictEqual(res.status, 204);
        const docs = await mongoApi.findEntities();
        assert.strictEqual(docs.length, 0, 'should be 0 as we just deleted it');
      });

    });

  });

});
