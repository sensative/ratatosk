/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');

const {
  ORION_ERRORS,
} = require('@ratatosk/ngsi-constants');

const {
  assertRejects,
  ngsiResponseErrorUtils: {
    assertErrorsMatch,
    assertErrorsMatchFails,
    assertErrorsMatchesColonTruncated,
  },
} = require('@ratatosk/ngsi-utils');

const {orionApi, ngsiApi} = require('../../client-api-util');
const mongoApi = require('../../mongo-api');

const entity = {
  id: 'sploder',
  type: 'gerpy',
  agda: 'gerbil',
  bo: 73,
  jerks: {type: 'Norbos', value: 74},
};

describe('upsert-entity-attrs', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      beforeEach(async () => {
        await mongoApi.assertEntitiesEmpty();
        await fiwareApi.createEntity({entity});
      });
      afterEach(() => mongoApi.wipeEntities());

      it('upsertEntityAttrs: works in simplest case (1 completely new attr)', async () => {
        const attrs = {what: 'dat'};
        const reqArg = {entityId: 'sploder', attrs};
        const res = await fiwareApi.upsertEntityAttrs(reqArg);
        assert.strictEqual(res.status, 204, 'status should be 204');
        const {data: doc} = await fiwareApi.findEntity(reqArg);
        const expected = {
          id: 'sploder',
          type: 'gerpy',
          agda: {type: 'Text', value: 'gerbil', metadata: {}},
          bo: {type: 'Number', value: 73, metadata: {}},
          jerks: {type: 'Norbos', value: 74, metadata: {}},
          what: {type: 'Text', value: 'dat', metadata: {}},
        };
        assert.deepStrictEqual(doc, expected);
      });

      it('upsertEntityAttrs: testing how metadata is preserved (modifying existing attrs)', async () => {
        const attrs = {bo: {value: 3, metadata: {spo: 4}}};
        const reqArg = {entityId: 'sploder', attrs};
        const res = await fiwareApi.upsertEntityAttrs(reqArg);
        assert.strictEqual(res.status, 204, 'status should be 204');
        const {data: doc} = await fiwareApi.findEntity(reqArg);
        const expected = {
          id: 'sploder',
          type: 'gerpy',
          agda: {type: 'Text', value: 'gerbil', metadata: {}},
          bo: {type: 'Number', value: 3, metadata: {spo: {type: 'Number', value: 4}}}, // metadata does get saved
          jerks: {type: 'Norbos', value: 74, metadata: {}},
        };
        assert.deepStrictEqual(doc, expected);
        const attrsNext = {bo: 'glue'};
        const reqArgNext = {entityId: 'sploder', attrs: attrsNext};
        const resNext = await fiwareApi.upsertEntityAttrs(reqArgNext);
        assert.strictEqual(resNext.status, 204, 'status should be 204');
        const {data: docNext} = await fiwareApi.findEntity(reqArgNext);
        const expectedNext = {
          id: 'sploder',
          type: 'gerpy',
          agda: {type: 'Text', value: 'gerbil', metadata: {}},
          bo: {type: 'Text', value: 'glue', metadata: {}}, // metadata gets wiped if not included
          jerks: {type: 'Norbos', value: 74, metadata: {}},
        };
        assert.deepStrictEqual(docNext, expectedNext);
      });

      it('upsertEntityAttrs: using "append" option, ONLY adding previously NON-existing attrs', async () => {
        const attrs = {what: 'dat'};

        const reqArg = {entityId: 'sploder', attrs, params: {append: true}};
        const config = fiwareApi.upsertEntityAttrs.createRequestConfig(reqArg);
        assert.strictEqual(config.params.options, 'append');

        const res = await fiwareApi.rawRequest(config);
        assert.strictEqual(res.status, 204, 'status should be 204');
        const {data: doc} = await fiwareApi.findEntity(reqArg);
        const expected = {
          id: 'sploder',
          type: 'gerpy',
          agda: {type: 'Text', value: 'gerbil', metadata: {}},
          bo: {type: 'Number', value: 73, metadata: {}},
          jerks: {type: 'Norbos', value: 74, metadata: {}},
          what: {type: 'Text', value: 'dat', metadata: {}},
        };
        assert.deepStrictEqual(doc, expected);
      });

      it('upsertEntityAttrs: using "append" option, changin ONLY existing attrs (FAILS)', async () => {
        const attrs = {bo: 'dat'};

        const reqArg = {entityId: 'sploder', attrs, params: {append: true}};
        const config = fiwareApi.upsertEntityAttrs.createRequestConfig(reqArg);
        assert.strictEqual(config.params.options, 'append');

        const err = await assertRejects(fiwareApi.rawRequest(config));

        // DIVERGE!!
        if (fiwareApi === orionApi) {
          assertErrorsMatchFails(err, ORION_ERRORS.UNPROCESSABLE_ATTRIBUTE_ALREADY_EXISTS);
          assertErrorsMatchesColonTruncated(err, ORION_ERRORS.UNPROCESSABLE_ATTRIBUTE_ALREADY_EXISTS);
        }
        if (fiwareApi === ngsiApi) {
          assertErrorsMatch(err, ORION_ERRORS.UNPROCESSABLE_ATTRIBUTE_ALREADY_EXISTS);
        }

      });


    });

  });

});
