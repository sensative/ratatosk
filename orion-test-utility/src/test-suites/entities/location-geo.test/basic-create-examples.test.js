/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const assert = require('assert');
const _ = require('lodash');

const {
  orionApi,
  ngsiApi,
} = require('../../../client-api-util');
const mongoApi = require('../../../mongo-api');

const stripMongoEntity = (mongoEnt) => {
  const attrs = _.mapValues(mongoEnt.attrs, (attr) => {
    const omitteds = _.concat(
      ['creDate', 'modDate', 'mdNames'],
    );
    return _.omit(attr, omitteds);
  });
  return _.assign(
    _.omit(mongoEnt, ['lastCorrelator', 'creDate', 'modDate', 'attrNames']),
    {attrs},
  );
};


describe('basic-create-examples', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      before(async () => {
        await mongoApi.assertEntitiesEmpty();
      });

      afterEach(async () => {
        await mongoApi.wipeEntities();
      });

      describe('simple location format', () => {

        it('create an entity with geo:point', async () => {
          const entity = {
            type: 'bvvwat',
            id: 'grytta',
            loco: {type: 'geo:point', value: '9.3    , 9.3'},
          };
          await fiwareApi.createEntity({entity});
          const {data: savedEnt} = await fiwareApi.findEntity({entityId: 'grytta'});

          // DIVERGE!!
          let storedValue;
          if (fiwareApi === orionApi) {
            storedValue = '9.3    , 9.3';
          }
          if (fiwareApi === ngsiApi) {
            storedValue = '9.3,9.3';
          }

          const expectedSavedEnt = {
            id: 'grytta',
            type: 'bvvwat',
            loco: {
              type: 'geo:point',
              value: storedValue,
              metadata: {},
            },
          };

          // DIVERGE!!
          let storedAttr;
          if (fiwareApi === orionApi) {
            storedAttr = {
              type: 'geo:point', value: '9.3    , 9.3',
            };
          }
          if (fiwareApi === ngsiApi) {
            storedAttr = {
              type: 'geo:point',
              value: {type: 'Point', coordinates: [9.3, 9.3]},
              md: {},
            };
          }
          assert.deepStrictEqual(savedEnt, expectedSavedEnt);

          // and check the mongo structure
          const [mongoEnt] = await mongoApi.findEntities();
          let expectedMongoEnt = {
            _id: mongoEnt._id,
            orionId: {id: 'grytta', type: 'bvvwat', servicePath: '/'},
            attrs: {
              loco: storedAttr,
            },
            location: {
              attrName: 'loco',
              coords: {
                coordinates: [9.3, 9.3],
                type: 'Point',
              },
            },
          };
          // DIVERGE!!
          if (fiwareApi === orionApi) {
            expectedMongoEnt = _.assign(
              _.omit(expectedMongoEnt, 'orionId', '_id'),
              {_id: expectedMongoEnt.orionId},
            );
          }
          if (fiwareApi === ngsiApi) {
            assert.strictEqual(mongoEnt._id.constructor.name, 'ObjectID');
          }
          assert.deepStrictEqual(stripMongoEntity(mongoEnt), expectedMongoEnt);
        });

        it('create an entity with geo:box', async () => {
          const entity = {
            type: 'bvvwat',
            id: 'grytta',
            loco: {type: 'geo:box', value: ['9.3,9.3', '9.4,9.4']},
          };
          await fiwareApi.createEntity({entity});

          const {data: savedEnt} = await fiwareApi.findEntity({entityId: 'grytta'});
          const expectedSavedEnt = {
            id: 'grytta',
            type: 'bvvwat',
            loco: {
              type: 'geo:box',
              value: ['9.3,9.3', '9.4,9.4'],
              metadata: {},
            },
          };
          assert.deepStrictEqual(savedEnt, expectedSavedEnt);

          // and check the mongo structure

          // DIVERGE!!
          let storedAttr;
          if (fiwareApi === orionApi) {
            storedAttr = {
              type: 'geo:box', value: ['9.3,9.3', '9.4,9.4'],
            };
          }
          if (fiwareApi === ngsiApi) {
            storedAttr = {
              type: 'geo:box',
              value: {
                type: 'Polygon',
                coordinates: [[[9.3, 9.3], [9.3, 9.4], [9.4, 9.4], [9.4, 9.3], [9.3, 9.3]]],
              },
              md: {},
            };
          }

          const [mongoEnt] = await mongoApi.findEntities();
          let expectedMongoEnt = {
            _id: mongoEnt._id,
            orionId: {id: 'grytta', type: 'bvvwat', servicePath: '/'},
            attrs: {
              loco: storedAttr,
            },
            location: {
              attrName: 'loco',
              coords: {
                coordinates: [[[9.3, 9.3], [9.3, 9.4], [9.4, 9.4], [9.4, 9.3], [9.3, 9.3]]],
                type: 'Polygon',
              },
            },
          };
          // DIVERGE!!
          if (fiwareApi === orionApi) {
            expectedMongoEnt = _.assign(
              _.omit(expectedMongoEnt, 'orionId', '_id'),
              {_id: expectedMongoEnt.orionId},
            );
          }
          if (fiwareApi === ngsiApi) {
            assert.strictEqual(mongoEnt._id.constructor.name, 'ObjectID');
          }
          assert.deepStrictEqual(stripMongoEntity(mongoEnt), expectedMongoEnt);
        });

        it('create an entity with geo:line', async () => {
          const entity = {
            type: 'bvvwat',
            id: 'grytta',
            loco: {type: 'geo:line', value: ['9.3,9.3', '9.4,9.4']},
          };
          await fiwareApi.createEntity({entity});

          const {data: savedEnt} = await fiwareApi.findEntity({entityId: 'grytta'});
          const expectedSavedEnt = {
            id: 'grytta',
            type: 'bvvwat',
            loco: {
              type: 'geo:line',
              value: ['9.3,9.3', '9.4,9.4'],
              metadata: {},
            },
          };
          assert.deepStrictEqual(savedEnt, expectedSavedEnt);

          // and check the mongo structure

          // DIVERGE!!
          let storedAttr;
          if (fiwareApi === orionApi) {
            storedAttr = {
              type: 'geo:line', value: ['9.3,9.3', '9.4,9.4'],
            };
          }
          if (fiwareApi === ngsiApi) {
            storedAttr = {
              type: 'geo:line',
              value: {
                type: 'LineString',
                coordinates: [[9.3, 9.3], [9.4, 9.4]],
              },
              md: {},
            };
          }

          const [mongoEnt] = await mongoApi.findEntities();
          let expectedMongoEnt = {
            _id: mongoEnt._id,
            orionId: {id: 'grytta', type: 'bvvwat', servicePath: '/'},
            attrs: {
              loco: storedAttr,
            },
            location: {
              attrName: 'loco',
              coords: {
                coordinates: [[9.3, 9.3], [9.4, 9.4]],
                type: 'LineString',
              },
            },
          };
          // DIVERGE!!
          if (fiwareApi === orionApi) {
            expectedMongoEnt = _.assign(
              _.omit(expectedMongoEnt, 'orionId', '_id'),
              {_id: expectedMongoEnt.orionId},
            );
          }
          if (fiwareApi === ngsiApi) {
            assert.strictEqual(mongoEnt._id.constructor.name, 'ObjectID');
          }
          assert.deepStrictEqual(stripMongoEntity(mongoEnt), expectedMongoEnt);
        });

      });

      describe('geo:json stuff', () => {

        it('create an entity with geo:json Point', async () => {
          const entity = {
            type: 'bvvwat',
            id: 'grytta',
            loco: {
              type: 'geo:json',
              value: {
                type: 'Point',
                coordinates: [2.2, 41.2],
              },
            },
          };
          await fiwareApi.createEntity({entity});

          const {data: savedEnt} = await fiwareApi.findEntity({entityId: 'grytta'});
          const expectedSavedEnt = {
            id: 'grytta',
            type: 'bvvwat',
            loco: {
              type: 'geo:json',
              value: {
                type: 'Point',
                coordinates: [2.2, 41.2],
              },
              metadata: {},
            },
          };
          assert.deepStrictEqual(savedEnt, expectedSavedEnt);
          // and check the mongo structure
          const [mongoEnt] = await mongoApi.findEntities();
          let expectedMongoEnt = {
            _id: mongoEnt._id,
            orionId: {id: 'grytta', type: 'bvvwat', servicePath: '/'},
            attrs: {
              loco: {
                type: 'geo:json',
                value: {type: 'Point', coordinates: [2.2, 41.2]},
              },
            },
            location: {
              attrName: 'loco',
              coords: {
                coordinates: [2.2, 41.2],
                type: 'Point',
              },
            },
          };
          // DIVERGE!!
          if (fiwareApi === orionApi) {
            expectedMongoEnt = _.assign(
              _.omit(expectedMongoEnt, 'orionId', '_id'),
              {_id: expectedMongoEnt.orionId},
            );
          }
          if (fiwareApi === ngsiApi) {
            assert.strictEqual(mongoEnt._id.constructor.name, 'ObjectID');
            _.set(expectedMongoEnt, 'attrs.loco.md', {});
          }
          assert.deepStrictEqual(stripMongoEntity(mongoEnt), expectedMongoEnt);
        });

        it('create an entity with geo:json LineString', async () => {
          const entity = {
            type: 'bvvwat',
            id: 'grytta',
            loco: {
              type: 'geo:json',
              value: {
                type: 'LineString',
                coordinates: [[40, 41], [51, 52]],
              },
            },
          };
          await fiwareApi.createEntity({entity});

          const {data: savedEnt} = await fiwareApi.findEntity({entityId: 'grytta'});
          const expectedSavedEnt = {
            id: 'grytta',
            type: 'bvvwat',
            loco: {
              type: 'geo:json',
              value: {
                type: 'LineString',
                coordinates: [[40, 41], [51, 52]],
              },
              metadata: {},
            },
          };
          assert.deepStrictEqual(savedEnt, expectedSavedEnt);
          // and check the mongo structure
          const [mongoEnt] = await mongoApi.findEntities();
          let expectedMongoEnt = {
            _id: mongoEnt._id,
            orionId: {id: 'grytta', type: 'bvvwat', servicePath: '/'},
            attrs: {
              loco: {
                type: 'geo:json',
                value: {
                  type: 'LineString',
                  coordinates: [[40, 41], [51, 52]],
                },
              },
            },
            location: {
              attrName: 'loco',
              coords: {
                coordinates: [[40, 41], [51, 52]],
                type: 'LineString',
              },
            },
          };
          // DIVERGE!!
          if (fiwareApi === orionApi) {
            expectedMongoEnt = _.assign(
              _.omit(expectedMongoEnt, 'orionId', '_id'),
              {_id: expectedMongoEnt.orionId},
            );
          }
          if (fiwareApi === ngsiApi) {
            assert.strictEqual(mongoEnt._id.constructor.name, 'ObjectID');
            _.set(expectedMongoEnt, 'attrs.loco.md', {});
          }
          assert.deepStrictEqual(stripMongoEntity(mongoEnt), expectedMongoEnt);
        });

        it('create an entity with geo:json Polygon', async () => {
          const entity = {
            type: 'bvvwat',
            id: 'grytta',
            loco: {
              type: 'geo:json',
              value: {
                type: 'Polygon',
                coordinates: [[[40, 41], [51, 52], [43, 43], [40, 41]]],
              },
            },
          };
          await fiwareApi.createEntity({entity});

          const {data: savedEnt} = await fiwareApi.findEntity({entityId: 'grytta'});
          const expectedSavedEnt = {
            id: 'grytta',
            type: 'bvvwat',
            loco: {
              type: 'geo:json',
              value: {
                type: 'Polygon',
                coordinates: [[[40, 41], [51, 52], [43, 43], [40, 41]]],
              },
              metadata: {},
            },
          };
          assert.deepStrictEqual(savedEnt, expectedSavedEnt);
          // and check the mongo structure
          const [mongoEnt] = await mongoApi.findEntities();
          let expectedMongoEnt = {
            _id: mongoEnt._id,
            orionId: {id: 'grytta', type: 'bvvwat', servicePath: '/'},
            attrs: {
              loco: {
                type: 'geo:json',
                value: {
                  type: 'Polygon',
                  coordinates: [[[40, 41], [51, 52], [43, 43], [40, 41]]],
                },
              },
            },
            location: {
              attrName: 'loco',
              coords: {
                coordinates: [[[40, 41], [51, 52], [43, 43], [40, 41]]],
                type: 'Polygon',
              },
            },
          };
          // DIVERGE!!
          if (fiwareApi === orionApi) {
            expectedMongoEnt = _.assign(
              _.omit(expectedMongoEnt, 'orionId', '_id'),
              {_id: expectedMongoEnt.orionId},
            );
          }
          if (fiwareApi === ngsiApi) {
            assert.strictEqual(mongoEnt._id.constructor.name, 'ObjectID');
            _.set(expectedMongoEnt, 'attrs.loco.md', {});
          }
          assert.deepStrictEqual(stripMongoEntity(mongoEnt), expectedMongoEnt);
        });

        it('create an entity with geo:json MultiPolygon', async () => {
          const entity = {
            type: 'bvvwat',
            id: 'grytta',
            loco: {
              type: 'geo:json',
              value: {
                type: 'MultiPolygon',
                coordinates: [[[[40, 41], [51, 52], [43, 43], [40, 41]]]],
              },
            },
          };
          await fiwareApi.createEntity({entity});

          const {data: savedEnt} = await fiwareApi.findEntity({entityId: 'grytta'});
          const expectedSavedEnt = {
            id: 'grytta',
            type: 'bvvwat',
            loco: {
              type: 'geo:json',
              value: {
                type: 'MultiPolygon',
                coordinates: [[[[40, 41], [51, 52], [43, 43], [40, 41]]]],
              },
              metadata: {},
            },
          };
          assert.deepStrictEqual(savedEnt, expectedSavedEnt);
          // and check the mongo structure
          const [mongoEnt] = await mongoApi.findEntities();
          let expectedMongoEnt = {
            _id: mongoEnt._id,
            orionId: {id: 'grytta', type: 'bvvwat', servicePath: '/'},
            attrs: {
              loco: {
                type: 'geo:json',
                value: {
                  type: 'MultiPolygon',
                  coordinates: [[[[40, 41], [51, 52], [43, 43], [40, 41]]]],
                },
              },
            },
            location: {
              attrName: 'loco',
              coords: {
                type: 'MultiPolygon',
                coordinates: [[[[40, 41], [51, 52], [43, 43], [40, 41]]]],
              },
            },
          };
          // DIVERGE!!
          if (fiwareApi === orionApi) {
            expectedMongoEnt = _.assign(
              _.omit(expectedMongoEnt, 'orionId', '_id'),
              {_id: expectedMongoEnt.orionId},
            );
          }
          if (fiwareApi === ngsiApi) {
            assert.strictEqual(mongoEnt._id.constructor.name, 'ObjectID');
            _.set(expectedMongoEnt, 'attrs.loco.md', {});
          }
          assert.deepStrictEqual(stripMongoEntity(mongoEnt), expectedMongoEnt);
        });

      });

    });

  });

});
