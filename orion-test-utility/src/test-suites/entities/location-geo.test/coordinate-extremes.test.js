/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const assert = require('assert');
const _ = require('lodash');
const serialPromise = require('promise-serial-exec');


const {
  assertRejects,
  // ngsiDateTimeUtils: {}
  ngsiResponseErrorUtils: {assertErrorsMatchesColonTruncated},
} = require('@ratatosk/ngsi-utils');

const {
  ORION_ERRORS,
} = require('@ratatosk/ngsi-constants');

const {
  orionApi,
  ngsiApi,
} = require('../../../client-api-util');
const mongoApi = require('../../../mongo-api');


describe('coordinate-extremes', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      before(async () => {
        await mongoApi.assertEntitiesEmpty();
      });

      after(async () => {
        await mongoApi.wipeEntities();
      });

      it('create geo:point with valid coords', async () => {
        const entity = {
          type: 'bvvwat',
          id: 'ubbity',
          loco: {type: 'geo:point', value: '9.3,9.3'},
          other: 234,
        };
        await fiwareApi.createEntity({entity});
        const {data: ent0} = await fiwareApi.findEntity({entityId: 'ubbity'});
        const expectedEnt0 = {
          id: 'ubbity',
          type: 'bvvwat',
          loco: {type: 'geo:point', value: '9.3,9.3', metadata: {}},
          other: {type: 'Number', value: 234, metadata: {}},
        };
        assert.deepStrictEqual(ent0, expectedEnt0);
        const mongoEnts0 = await mongoApi.findEntities();

        // DIVERGE!!
        const idPath = fiwareApi === orionApi ? '_id' : 'orionId';

        const mongoEnt0 = _.find(mongoEnts0, (ent) => ent[idPath].id === 'ubbity');
        const expectedMongoLocation0 = {
          attrName: 'loco',
          coords: {
            type: 'Point',
            coordinates: [9.3, 9.3],
          },
        };
        assert.deepStrictEqual(mongoEnt0.location, expectedMongoLocation0);
      });

      it('check some valid coord values (Simple geo:point)', async () => {
        const valids = [
          '0,0',
          '90,180',
          '-90,-180',
        ];
        await serialPromise(_.map(valids, (valid) => async () => {
          const mods = {
            entityId: 'ubbity',
            attrs: {loco: {type: 'geo:point', value: valid}},
          };
          await fiwareApi.patchEntityAttrs(mods);
          const {data: ent} = await fiwareApi.findEntity({entityId: 'ubbity'});
          const expectedEnt = {
            id: 'ubbity',
            type: 'bvvwat',
            loco: {type: 'geo:point', value: valid, metadata: {}},
            other: {type: 'Number', value: 234, metadata: {}},
          };
          assert.deepStrictEqual(ent, expectedEnt);
        }));
      });

      it('check some invalid coord values (Simple geo:point)', async () => {
        const invalids = [
          '91,0',
          '-91,0',
          '0,181',
          '0,-181',
        ];
        await serialPromise(_.map(invalids, (invalid) => async () => {
          const mods = {
            entityId: 'ubbity',
            attrs: {loco: {type: 'geo:point', value: invalid}},
          };

          // DIVERGE!!
          if (fiwareApi === orionApi) {
            const err = await assertRejects(fiwareApi.patchEntityAttrs(mods));
            assertErrorsMatchesColonTruncated(err, ORION_ERRORS.BAD_REQ_INVALID_GEO_COORDS);
          }
          if (fiwareApi === ngsiApi) {
            await fiwareApi.patchEntityAttrs(mods);
            const [mongoEnt] = await mongoApi.findEntities();
            // the geo thing is invalid, but saved
            assert.strictEqual(mongoEnt.location, undefined);
          }

        }));
      });

      it('check some valid coord values (simple geo:polygon)', async () => {
        const valids = [
          ['0,0', '1,1', '2,2', '0,0'],
          ['0,0', '90,0', '-90,-180', '90,180', '0,0'],
        ];
        await serialPromise(_.map(valids, (valid) => async () => {
          const mods = {
            entityId: 'ubbity',
            attrs: {loco: {type: 'geo:polygon', value: valid}},
          };
          await fiwareApi.patchEntityAttrs(mods);
          const {data: ent} = await fiwareApi.findEntity({entityId: 'ubbity'});
          const expectedEnt = {
            id: 'ubbity',
            type: 'bvvwat',
            loco: {type: 'geo:polygon', value: valid, metadata: {}},
            other: {type: 'Number', value: 234, metadata: {}},
          };
          assert.deepStrictEqual(ent, expectedEnt);
        }));
      });

      it('check some invalid coord values (simple geo:polygon)', async () => {
        const invalids = [
          ['0,0', '91,1', '2,2', '0,0'],
          ['0,0', '-91,1', '2,2', '0,0'],
          ['0,0', '1,181', '2,2', '0,0'],
          ['0,0', '1,-181', '2,2', '0,0'],
        ];
        await serialPromise(_.map(invalids, (invalid) => async () => {
          const mods = {
            entityId: 'ubbity',
            attrs: {loco: {type: 'geo:polygon', value: invalid}},
          };
          // DIVERGE!!
          if (fiwareApi === orionApi) {
            const err = await assertRejects(fiwareApi.patchEntityAttrs(mods));
            assertErrorsMatchesColonTruncated(err, ORION_ERRORS.BAD_REQ_INVALID_GEO_COORDS);
          }
          if (fiwareApi === ngsiApi) {
            await fiwareApi.patchEntityAttrs(mods);
            const [mongoEnt] = await mongoApi.findEntities();
            // the geo thing is invalid, but saved
            assert.strictEqual(mongoEnt.location, undefined);
          }
        }));
      });

      it('check some valid coord values (geo:json "Point")', async () => {
        const valids = [
          [0, 0],
          [180, 90],
          [-180, -90],
        ];
        await serialPromise(_.map(valids, (valid) => async () => {
          const mods = {
            entityId: 'ubbity',
            attrs: {
              loco: {
                type: 'geo:json',
                value: {type: 'Point', coordinates: valid},
              },
            },
          };
          await fiwareApi.patchEntityAttrs(mods);
          const {data: ent} = await fiwareApi.findEntity({entityId: 'ubbity'});
          const expectedEnt = {
            id: 'ubbity',
            type: 'bvvwat',
            loco: {type: 'geo:json', value: {type: 'Point', coordinates: valid}, metadata: {}},
            other: {type: 'Number', value: 234, metadata: {}},
          };
          assert.deepStrictEqual(ent, expectedEnt);
        }));
      });

      it('check some invalid coord values (geo:json "Point")', async () => {
        const invalids = [
          [0, 91],
          [181, 0],
          [-181, 0],
          [0, -91],
        ];
        await serialPromise(_.map(invalids, (invalid) => async () => {
          const mods = {
            entityId: 'ubbity',
            attrs: {loco: {type: 'geo:json', value: {type: 'Point', coordinates: invalid}}},
          };

          // DIVERGE!!
          if (fiwareApi === orionApi) {
            const err = await assertRejects(fiwareApi.patchEntityAttrs(mods));
            assert.strictEqual(err.response.status, 500);
            assert.strictEqual(err.response.data.error, 'InternalServerError');
            assert(err.response.data.description.length > 200); // some true uglies here
          }
          if (fiwareApi === ngsiApi) {
            await fiwareApi.patchEntityAttrs(mods);
            const [mongoEnt] = await mongoApi.findEntities();
            // the geo thing is invalid, but saved
            assert.strictEqual(mongoEnt.location, undefined);
          }

        }));
      });

      it('check some valid coord values (geo:json "Polygon")', async () => {
        const valids = [
          [[[0, 0], [1, 1], [2, 2], [0, 0]]],
        ];
        await serialPromise(_.map(valids, (valid) => async () => {
          const mods = {
            entityId: 'ubbity',
            attrs: {
              loco: {
                type: 'geo:json',
                value: {type: 'Polygon', coordinates: valid},
              },
            },
          };
          await fiwareApi.patchEntityAttrs(mods);
          const {data: ent} = await fiwareApi.findEntity({entityId: 'ubbity'});
          const expectedEnt = {
            id: 'ubbity',
            type: 'bvvwat',
            loco: {type: 'geo:json', value: {type: 'Polygon', coordinates: valid}, metadata: {}},
            other: {type: 'Number', value: 234, metadata: {}},
          };
          assert.deepStrictEqual(ent, expectedEnt);
        }));
      });

      it('check some invalid coord values (geo:json "Polygon")', async () => {
        const invalids = [
          [[[0, 0], [0, 91], [2, 2], [0, 0]]],
          [[[0, 0], [181, 0], [2, 2], [0, 0]]],
          [[[0, 0], [0, -91], [2, 2], [0, 0]]],
          [[[0, 0], [-181, 0], [2, 2], [0, 0]]],
        ];
        await serialPromise(_.map(invalids, (invalid) => async () => {
          const mods = {
            entityId: 'ubbity',
            attrs: {loco: {type: 'geo:json', value: {type: 'Polygon', coordinates: invalid}}},
          };

          // DIVERGE!!
          if (fiwareApi === orionApi) {
            const err = await assertRejects(fiwareApi.patchEntityAttrs(mods));
            assert.strictEqual(err.response.status, 500);
            assert.strictEqual(err.response.data.error, 'InternalServerError');
            assert(err.response.data.description.length > 200); // some true uglies here
          }
          if (fiwareApi === ngsiApi) {
            await fiwareApi.patchEntityAttrs(mods);
            const [mongoEnt] = await mongoApi.findEntities();
            // the geo thing is invalid, but saved
            assert.strictEqual(mongoEnt.location, undefined);
          }
        }));
      });

    });

  });

});
