/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const assert = require('assert');
const _ = require('lodash');

const {
  assertRejects,
  ngsiResponseErrorUtils: {assertErrorsMatch},
} = require('@ratatosk/ngsi-utils');

const {
  ORION_ERRORS,
} = require('@ratatosk/ngsi-constants');

const {
  orionApi,
  ngsiApi,
} = require('../../../client-api-util');
const mongoApi = require('../../../mongo-api');


describe('create-and-update-examples', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      before(async () => {
        await mongoApi.assertEntitiesEmpty();
      });

      after(async () => {
        await mongoApi.wipeEntities();
      });

      it('create an entity with 2 geo:points (no defaultLocation metadata) (FAILS)', async () => {

        const entity = {
          id: 'grytta',
          type: 'bvvwat',
          loco: {type: 'geo:point', value: '1,2'},
          moto: {type: 'geo:point', value: '3,4'},
        };
        const err = await assertRejects(fiwareApi.createEntity({entity}));

        // DIVERGE!!
        if (fiwareApi === orionApi) {
          const expectedErr = ORION_ERRORS.MULTIPLE_LOCATIONS_FORBIDDEN;
          assertErrorsMatch(err, expectedErr);
        }
        if (fiwareApi === ngsiApi) {
          const expectedErr = ORION_ERRORS.UNPROCESSABLE_MULTIPLE_GEO_WITHOUT_DEFAULT;
          assertErrorsMatch(err, expectedErr);
        }
      });

      it('create an entity with 2 geo:points (defaultLocation = true) (ALSO fails for Orion)', async () => {
        const entity = {
          type: 'bvvwat',
          id: 'grytta',
          loco: {type: 'geo:point', value: '9.3,9.3'},
          moto: {type: 'geo:point', value: '3,3', metadata: {defaultLocation: true}},
        };

        // DIVERGE!!
        if (fiwareApi === orionApi) {
          const err = await assertRejects(fiwareApi.createEntity({entity}));
          const expectedErr = ORION_ERRORS.MULTIPLE_LOCATIONS_FORBIDDEN;
          assertErrorsMatch(err, expectedErr);
        }
        if (fiwareApi === ngsiApi) {
          await fiwareApi.createEntity({entity});
          const mongoDocs = await mongoApi.findEntities();
          assert.strictEqual(mongoDocs.length, 1);
          const locationAttrName = _.get(mongoDocs[0], 'location.attrName');
          assert.strictEqual(locationAttrName, 'moto');
          // and delete the entity for the other tests..
          await mongoApi.wipeEntities();
        }
      });

      it('create entity with one location, then mutate THAT attr', async () => {
        const entity = {
          type: 'bvvwat',
          id: 'grytta',
          loco: {type: 'geo:point', value: '9.3,9.3'},
          other: 234,
        };
        await fiwareApi.createEntity({entity});
        const {data: ent0} = await fiwareApi.findEntity({entityId: 'grytta'});
        const expectedEnt0 = {
          id: 'grytta',
          type: 'bvvwat',
          loco: {type: 'geo:point', value: '9.3,9.3', metadata: {}},
          other: {type: 'Number', value: 234, metadata: {}},
        };
        assert.deepStrictEqual(ent0, expectedEnt0);

        // DIVERGE!!
        const idPath = fiwareApi === orionApi ? '_id' : 'orionId';

        const mongoEnts0 = await mongoApi.findEntities();
        const mongoEnt0 = _.find(mongoEnts0, (ent) => ent[idPath].id === 'grytta');
        const expectedMongoLocation0 = {
          attrName: 'loco',
          coords: {
            type: 'Point',
            coordinates: [9.3, 9.3],
          },
        };
        assert.deepStrictEqual(mongoEnt0.location, expectedMongoLocation0);

        const mods1 = {
          entityId: 'grytta',
          attrs: {loco: {type: 'geo:point', value: '3.2,3.2'}},
        };
        await fiwareApi.patchEntityAttrs(mods1);
        const {data: ent1} = await fiwareApi.findEntity({entityId: 'grytta'});
        const expectedEnt1 = {
          id: 'grytta',
          type: 'bvvwat',
          loco: {type: 'geo:point', value: '3.2,3.2', metadata: {}},
          other: {type: 'Number', value: 234, metadata: {}},
        };
        assert.deepStrictEqual(ent1, expectedEnt1);

        const mods2 = {
          entityId: 'grytta',
          attrs: {
            loco: {
              type: 'geo:json',
              value: {
                type: 'Polygon',
                coordinates: [[[40, 41], [51, 52], [43, 43], [40, 41]]],
              },
            },
          },
        };
        await fiwareApi.upsertEntityAttrs(mods2);
        const {data: ent2} = await fiwareApi.findEntity({entityId: 'grytta'});
        const expectedEnt2 = {
          id: 'grytta',
          type: 'bvvwat',
          loco: {
            type: 'geo:json',
            value: {
              type: 'Polygon',
              coordinates: [[[40, 41], [51, 52], [43, 43], [40, 41]]],
            },
            metadata: {},
          },
          other: {type: 'Number', value: 234, metadata: {}},
        };
        assert.deepStrictEqual(ent2, expectedEnt2);
        const mongoEnts2 = await mongoApi.findEntities();
        const mongoEnt2 = _.find(mongoEnts2, (ent) => ent[idPath].id === 'grytta');
        const expectedMongoLocation2 = {
          attrName: 'loco',
          coords: {
            type: 'Polygon',
            coordinates: [[[40, 41], [51, 52], [43, 43], [40, 41]]],
          },
        };
        assert.deepStrictEqual(mongoEnt2.location, expectedMongoLocation2);

      });

    });

  });

});
