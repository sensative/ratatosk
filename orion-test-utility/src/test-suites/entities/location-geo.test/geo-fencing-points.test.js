/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const assert = require('assert');
const _ = require('lodash');
const geolib = require('geolib');


const {
  assertRejects,
  ngsiResponseErrorUtils: {assertErrorsMatch},
} = require('@ratatosk/ngsi-utils');

const {
  ORION_ERRORS,
} = require('@ratatosk/ngsi-constants');

const {
  orionApi,
  ngsiApi,
} = require('../../../client-api-util');
const mongoApi = require('../../../mongo-api');

describe('basic-create-examples', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      before(async () => {
        await mongoApi.assertEntitiesEmpty();
      });

      after(async () => {
        await mongoApi.wipeEntities();
      });

      describe('geo:point georels', () => {

        const pt0 = {longitude: 23, latitude: 24};
        before(async () => {
          const pt1 = geolib.computeDestinationPoint(pt0, 998, 35);
          const pt2 = geolib.computeDestinationPoint(pt0, 1001, 125);
          const ent0 = {
            id: 'berp',
            type: 'al',
            hop: {type: 'geo:point', value: `${pt0.latitude},${pt0.longitude}`},
          };
          const ent1 = {
            id: 'nerp',
            type: 'al',
            hop: {type: 'geo:point', value: `${pt1.latitude},${pt1.longitude}`},
          };
          const ent2 = {
            id: 'herp',
            type: 'al',
            hop: {type: 'geo:point', value: `${pt2.latitude},${pt2.longitude}`},
          };
          await fiwareApi.createEntity({entity: ent0});
          await fiwareApi.createEntity({entity: ent1});
          await fiwareApi.createEntity({entity: ent2});
        });
        after(async () => mongoApi.wipeEntities());

        it('find entities < 1000m from pt0', async () => {
          const params = {
            georel: 'near;maxDistance:1000',
            geometry: 'point',
            coords: `${pt0.latitude},${pt0.longitude}`,
          };
          const {data: ents} = await fiwareApi.getEntities({params});
          const names = _.map(_.sortBy(ents, (ent) => ent.id), (ent) => ent.id);
          assert.deepStrictEqual(names, ['berp', 'nerp']);
        });

        it('find entities >1000 m from pt0', async () => {
          const config = await fiwareApi.getEntities.createRequestConfig();
          // georel=near;maxDistance:1000&geometry=point&coords=-40.4,-3.5
          _.set(config, 'params.georel', 'near;minDistance:1000');
          _.set(config, 'params.geometry', 'point');
          _.set(config, 'params.coords', `${pt0.latitude},${pt0.longitude}`);
          const {data: ents} = await fiwareApi.rawRequest(config);
          const names = _.map(_.sortBy(ents, (ent) => ent.id), (ent) => ent.id);
          assert.deepStrictEqual(names, ['herp']);
        });

        it('find entities COVERED_BY a point', async () => {
          const config = await fiwareApi.getEntities.createRequestConfig();
          _.set(config, 'params.georel', 'coveredBy');
          _.set(config, 'params.geometry', 'point');
          _.set(config, 'params.coords', `${pt0.latitude},${pt0.longitude}`);

          // DIVERGE!!
          const err = await assertRejects(fiwareApi.rawRequest(config));
          if (fiwareApi === orionApi) {
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_INVALID_POINT_GEOMETRY_WITH_GEOREL_COVEREDBY);
          }
          if (fiwareApi === ngsiApi) {
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_INVALID_GEOMETRY_WITH_GEOREL_COVEREDBY);
          }
        });

        it('find entities COVERED_BY a line', async () => {
          const pt1 = {longitude: 24, latitude: 25};
          const config = await fiwareApi.getEntities.createRequestConfig();
          _.set(config, 'params.georel', 'coveredBy');
          _.set(config, 'params.geometry', 'line');
          _.set(config, 'params.coords', `${pt0.latitude},${pt0.longitude};${pt1.latitude},${pt1.longitude}`);
          // DIVERGE!!
          const err = await assertRejects(fiwareApi.rawRequest(config));
          if (fiwareApi === orionApi) {
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_INVALID_LINE_GEOMETRY_WITH_GEOREL_COVEREDBY);
          }
          if (fiwareApi === ngsiApi) {
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_INVALID_GEOMETRY_WITH_GEOREL_COVEREDBY);
          }
        });

        it('find entities INTERSECTS a point', async () => {
          const params = {
            georel: 'intersects',
            geometry: 'point',
            coords: `${pt0.latitude},${pt0.longitude}`,
          };
          const {data: docs} = await fiwareApi.getEntities({params});
          assert.strictEqual(docs.length, 1);
          assert.strictEqual(docs[0].id, 'berp');
        });

        it('find entities DISJOINT from a point', async () => {
          const params = {
            georel: 'disjoint',
            geometry: 'point',
            coords: `${pt0.latitude},${pt0.longitude}`,
          };
          const {data: docs} = await fiwareApi.getEntities({params});
          assert.strictEqual(docs.length, 2);
          const names = _.sortBy(_.map(docs, (doc) => doc.id), (doc) => doc);
          assert.deepStrictEqual(names, ['herp', 'nerp']);
        });

        it('find entities EQUALS a point', async () => {
          const params = {
            georel: 'equals',
            geometry: 'point',
            coords: `${pt0.latitude},${pt0.longitude}`,
          };
          const {data: docs} = await fiwareApi.getEntities({params});
          assert.strictEqual(docs.length, 1);
          assert.strictEqual(docs[0].id, 'berp');
        });

      });

      describe('geo:polygon georels', () => {

        const pt0 = {longitude: 23, latitude: 24};
        const genPoly = (pt, dist) => {
          const mid = geolib.computeDestinationPoint(pt, dist, 0);
          const last = geolib.computeDestinationPoint(mid, dist, 145);
          return [pt, mid, last, pt];
        };
        const serializePoly = (polyArr) => _.map(polyArr, (pt) => {
          return `${pt.latitude},${pt.longitude}`;
        });
        before(async () => {
          const pt1 = geolib.computeDestinationPoint(pt0, 880, 0);
          const pt2 = geolib.computeDestinationPoint(pt0, 950, 0);
          const pt3 = geolib.computeDestinationPoint(pt0, 1010, 0);
          const poly1 = serializePoly(genPoly(pt1, 100));
          const poly2 = serializePoly(genPoly(pt2, 300));
          const poly3 = serializePoly(genPoly(pt3, 100));
          const ent1 = {
            id: 'bppp',
            type: 'al',
            hop: {type: 'geo:polygon', value: poly1},
          };
          const ent2 = {
            id: 'nppp',
            type: 'al',
            hop: {type: 'geo:polygon', value: poly2},
          };
          const ent3 = {
            id: 'hppp',
            type: 'al',
            hop: {type: 'geo:polygon', value: poly3},
          };
          await fiwareApi.createEntity({entity: ent1});
          await fiwareApi.createEntity({entity: ent2});
          await fiwareApi.createEntity({entity: ent3});
        });
        after(async () => mongoApi.wipeEntities());

        it('find entities < 1000m from pt0', async () => {
          const params = {
            georel: 'near;maxDistance:1000',
            geometry: 'point',
            coords: `${pt0.latitude},${pt0.longitude}`,
          };
          const {data: ents} = await fiwareApi.getEntities({params});
          const names = _.map(_.sortBy(ents, (ent) => ent.id), (ent) => ent.id);
          assert.deepStrictEqual(names, ['bppp', 'nppp']);
        });

        it('find entities >1000 m from pt0', async () => {
          const params = {
            georel: 'near;minDistance:1000',
            geometry: 'point',
            coords: `${pt0.latitude},${pt0.longitude}`,
          };
          const {data: ents} = await fiwareApi.getEntities({params});
          const names = _.map(_.sortBy(ents, (ent) => ent.id), (ent) => ent.id);
          assert.deepStrictEqual(names, ['hppp']);
        });

        it('find entities < 1000m from poly0', async () => {
          const poly0 = serializePoly(genPoly(pt0, 30));
          const coords = poly0.join(';');
          const config = await fiwareApi.getEntities.createRequestConfig();
          _.set(config, 'params.georel', 'near;minDistance:1000');
          _.set(config, 'params.geometry', 'polygon');
          _.set(config, 'params.coords', coords);
          const err = await assertRejects(fiwareApi.rawRequest(config));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_INVALID_GEOMETRY_WITH_GEOREL_NEAR);
        });

      });

    });

  });

});
