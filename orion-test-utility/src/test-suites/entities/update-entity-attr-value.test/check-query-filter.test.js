/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');

const {
  assertRejects,
  ngsiResponseErrorUtils: {assertErrorsMatch},
} = require('@ratatosk/ngsi-utils');

const {
  ORION_ERRORS,
} = require('@ratatosk/ngsi-constants');

const {orionApi, ngsiApi} = require('../../../client-api-util');
const mongoApi = require('../../../mongo-api');

const entity = {
  id: 'supanu',
  type: 'Kerchew',
  smokey: {
    type: 'Robinson',
    value: null,
    metadata: {precision: 23},
  },
};

describe('check-query-filter', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      beforeEach(async () => {
        await mongoApi.assertEntitiesEmpty();
        await fiwareApi.createEntity({entity});
      });
      afterEach(() => mongoApi.wipeEntities());

      it('updateEntityAttrValue: works in simplest case', async () => {
        const reqArg = {entityId: 'supanu', attrName: 'smokey', attrValue: 224};
        const res = await fiwareApi.updateEntityAttrValue(reqArg);
        assert.strictEqual(res.status, 204, 'status should be 204');
        const {data: attrValue} = await fiwareApi.findEntityAttrValue(reqArg);
        assert.strictEqual(attrValue, 224);
      });

      it('updateEntityAttrValue: fails if there are 2 entities with same name (as expected - no attempt at disambiguation)', async () => {
        // create a second arg
        const entityX = {id: 'supanu', type: 'keet'};
        await fiwareApi.createEntity({entity: entityX});
        // we should not be able to identify this guy
        const reqArg = {entityId: 'supanu', attrName: 'smokey', attrValue: 232};
        const err = await assertRejects(fiwareApi.updateEntityAttrValue(reqArg));
        assertErrorsMatch(err, ORION_ERRORS.FIND_ERROR_TOO_MANY);
      });

      it('when 2 ents with same name are present, PARAMS WORK (at least type..) to disambiguate', async () => {
        // create a second arg
        const entityX = {id: 'supanu', type: 'keet'};
        await fiwareApi.createEntity({entity: entityX});
        // update attr
        const reqArg = {
          entityId: 'supanu',
          attrName: 'smokey',
          attrValue: 345,
          params: {type: 'Kerchew'},
        };
        await fiwareApi.updateEntityAttrValue(reqArg);
        // and check that it worked
        const {data: attrValue} = await fiwareApi.findEntityAttrValue(reqArg);
        assert.strictEqual(attrValue, 345);
      });

      it('updateEntityAttrValue FAILS when disambiguating with q-query. works for ngsi-server', async () => {
        // create a second arg
        const entityX = {id: 'supanu', type: 'keet'};
        await fiwareApi.createEntity({entity: entityX});
        // update attr
        const reqArg = {
          entityId: 'supanu',
          attrName: 'smokey',
          attrValue: 4567,
          params: {q: 'smokey==null'},
        };

        // DIVERGE!!
        if (fiwareApi === orionApi) {
          const err = await assertRejects(fiwareApi.updateEntityAttrValue(reqArg));
          assertErrorsMatch(err, ORION_ERRORS.FIND_ERROR_TOO_MANY);
        }
        if (fiwareApi === ngsiApi) {
          await fiwareApi.updateEntityAttrValue(reqArg);
          const {data: attrValue} = await fiwareApi.findEntityAttrValue({
            entityId: 'supanu',
            attrName: 'smokey',
            params: {q: 'smokey==4567'}},
          );
          assert.strictEqual(attrValue, 4567);
        }
      });

    });

  });


});
