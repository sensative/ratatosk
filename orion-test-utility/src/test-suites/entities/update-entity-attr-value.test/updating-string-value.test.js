/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const serialPromise = require('promise-serial-exec');

const {
  assertRejects,
  ngsiResponseErrorUtils: {assertErrorsMatch},
} = require('@ratatosk/ngsi-utils');

const {
  ORION_ERRORS,
  ILLEGAL_METADATA_NAMES,
  ILLEGAL_ATTRIBUTE_NAMES,
  FIELD_NAME_EXPLICITLY_INVALID_CHARS,
  ORION_FORBIDDEN_QUERY_CHARS,
} = require('@ratatosk/ngsi-constants');

const {orionApi, ngsiApi} = require('../../../client-api-util');
const mongoApi = require('../../../mongo-api');

const JSON_CONTENT = {'Content-Type': 'application/json'};

const entity = {
  id: 'supanu',
  type: 'Kerchew',
  smokey: {
    type: 'Robinson',
    value: null,
    metadata: {precision: 23},
  },
};

describe('updating STRING value', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      beforeEach(async () => {
        await mongoApi.assertEntitiesEmpty();
        await fiwareApi.createEntity({entity});
      });
      afterEach(() => mongoApi.wipeEntities());

      it('works with TEXT_CONTENT and surrounded by ""\'s', async () => {
        const reqArg = {
          entityId: 'supanu',
          attrName: 'smokey',
          attrValue: 'spodo',
        };
        const config = fiwareApi.updateEntityAttrValue.createRequestConfig(reqArg);
        assert.strictEqual(config.headers['Content-Type'], 'text/plain', 'should be text');
        assert.strictEqual(config.data, '"spodo"');
        await fiwareApi.rawRequest(config);
        const {data: doc} = await fiwareApi.findEntity({entityId: 'supanu'});
        const expectedAttr = {
          type: 'Robinson',
          value: 'spodo',
          metadata: {
            precision: {type: 'Number', value: 23},
          },
        };
        assert.deepStrictEqual(expectedAttr, doc.smokey, 'the attr looks invalid');
      });

      it('urlencoding a string with a forbidden characters works (when surrounded by ""\'s), but it is the ENCODED data that is saved in orion', async () => {
        const attrValue = 'sp"do';
        const encodedAttrValue = encodeURI(attrValue);
        assert.strictEqual(encodedAttrValue, 'sp%22do');
        const reqArg = {
          entityId: 'supanu',
          attrName: 'smokey',
          attrValue: encodedAttrValue,
        };
        const config = fiwareApi.updateEntityAttrValue.createRequestConfig(reqArg);
        assert.strictEqual(config.headers['Content-Type'], 'text/plain', 'should be text');
        assert.strictEqual(config.data, '"sp%22do"');
        await fiwareApi.rawRequest(config);
        const {data: doc} = await fiwareApi.findEntity({entityId: 'supanu'});
        const expectedAttr = {
          type: 'Robinson',
          value: 'sp%22do',
          metadata: {
            precision: {type: 'Number', value: 23},
          },
        };
        assert.deepStrictEqual(expectedAttr, doc.smokey, 'the attr looks invalid');
      });

      it('fails when string is not surrounded by ""\'s', async () => {
        const reqArg = {
          entityId: 'supanu',
          attrName: 'smokey',
          attrValue: 'spodo',
        };
        const config = fiwareApi.updateEntityAttrValue.createRequestConfig(reqArg);
        config.data = 'spodo'; // mutate! gets rid of ""s
        const err = await assertRejects(fiwareApi.rawRequest(config));
        assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_INVALID_VALUE);
      });

      it('fails when string sent as JSON_CONTENT', async () => {
        const reqArg = {
          entityId: 'supanu',
          attrName: 'smokey',
          attrValue: 'spodo',
        };
        const config = fiwareApi.updateEntityAttrValue.createRequestConfig(reqArg);
        config.headers = JSON_CONTENT; // mutate! switch to JSON_CONTENT
        const err = await assertRejects(fiwareApi.rawRequest(config));
        assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_INVALID_JSON);
      });


      it('fails when the string has ANY orion forbidden character (only for orion)', async () => {
        const reqArg = {
          entityId: 'supanu',
          attrName: 'smokey',
          attrValue: 'spodo',
        };
        const config = fiwareApi.updateEntityAttrValue.createRequestConfig(reqArg);
        await serialPromise(_.map(ORION_FORBIDDEN_QUERY_CHARS, (char) => async () => {
          if (char === '"') {
            config.data = `"spo\\${char}do"`; // mutate!
          } else {
            config.data = `"spo${char}do"`; // mutate!
          }

          // DIVERGE!!
          if (fiwareApi === orionApi) {
            const err = await assertRejects(fiwareApi.rawRequest(config));
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_INVALID_CHAR_IN_ATTR_VALUE);
          }
          if (fiwareApi === ngsiApi) {
            await fiwareApi.rawRequest(config); // no problems
            const {data: attrValue} = await fiwareApi.findEntityAttrValue(reqArg);
            assert.strictEqual(attrValue, `spo${char}do`);
          }
        }));
      });

      it('does NOT fail when the string has ANY explicitly forbidden field character ("&", "?", "/", "#")', async () => {
        const reqArg = {
          entityId: 'supanu',
          attrName: 'smokey',
          attrValue: 'spodo',
        };
        const config = fiwareApi.updateEntityAttrValue.createRequestConfig(reqArg);
        await serialPromise(_.map(FIELD_NAME_EXPLICITLY_INVALID_CHARS, (char) => async () => {
          config.data = `"spo${char}do"`; // mutate!
          const {data: doc} = await fiwareApi.findEntity({entityId: 'supanu'});
          assert.strictEqual(doc.smokey.value, `spo${char}do`);
        }));
      });

      it('works with special ngsi strings and surrounded by ""\'s', async () => {
        const specials = _.uniq(_.concat(
          ILLEGAL_METADATA_NAMES,
          ILLEGAL_ATTRIBUTE_NAMES,
        ));
        const reqArg = {
          entityId: 'supanu',
          attrName: 'smokey',
          attrValue: 'spodo',
        };
        const config = fiwareApi.updateEntityAttrValue.createRequestConfig(reqArg);
        await serialPromise(_.map(specials, (name) => async () => {
          config.data = `"${name}"`;
          await fiwareApi.rawRequest(config);
          const {data: doc} = await fiwareApi.findEntity({entityId: 'supanu'});
          const expectedAttr = {
            type: 'Robinson',
            value: name,
            metadata: {
              precision: {type: 'Number', value: 23},
            },
          };
          assert.deepStrictEqual(expectedAttr, doc.smokey, 'the attr looks invalid');
        }));
      });

    });

  });

});
