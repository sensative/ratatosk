/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

// Test all the different versions of patch value

const mongoApi = require('../../../mongo-api');

describe('update-entity-attr-value', () => {

  before(() => mongoApi.assertEntitiesEmpty());
  after(() => mongoApi.assertEntitiesEmpty());

  // // basic value tests
  require('./updating-string-value.test');
  require('./updating-number-value.test');
  require('./updating-boolean-value.test');
  require('./updating-null-value.test');
  require('./updating-object-value.test');
  require('./updating-array-value.test');

  // // general tests
  require('./check-query-filter.test');
});
