/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');

const {
  assertRejects,
  ngsiResponseErrorUtils: {assertErrorsMatch},
} = require('@ratatosk/ngsi-utils');

const {
  ORION_ERRORS,
} = require('@ratatosk/ngsi-constants');

const {orionApi, ngsiApi} = require('../../../client-api-util');
const mongoApi = require('../../../mongo-api');

const JSON_CONTENT = {'Content-Type': 'application/json'};

const entity = {
  id: 'supanu',
  type: 'Kerchew',
  smokey: {
    type: 'Robinson',
    value: null,
    metadata: {precision: 23},
  },
};

describe('updating BOOLEAN value', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      beforeEach(async () => {
        await mongoApi.assertEntitiesEmpty();
        await fiwareApi.createEntity({entity});
      });
      afterEach(() => mongoApi.wipeEntities());

      it('true - works with TEXT_CONTENT', async () => {
        const reqArg = {
          entityId: 'supanu',
          attrName: 'smokey',
          attrValue: true,
        };
        const config = fiwareApi.updateEntityAttrValue.createRequestConfig(reqArg);
        assert.strictEqual(config.headers['Content-Type'], 'text/plain', 'should be text');
        assert.strictEqual(config.data, 'true');
        await fiwareApi.rawRequest(config);
        const {data: doc} = await fiwareApi.findEntity({entityId: 'supanu'});
        const expectedAttr = {
          type: 'Robinson',
          value: true,
          metadata: {
            precision: {type: 'Number', value: 23},
          },
        };
        assert.deepStrictEqual(expectedAttr, doc.smokey, 'the attr looks invalid');
      });

      it('true - fails when sent as JSON_CONTENT', async () => {
        const reqArg = {
          entityId: 'supanu',
          attrName: 'smokey',
          attrValue: true,
        };
        const config = fiwareApi.updateEntityAttrValue.createRequestConfig(reqArg);
        config.headers = JSON_CONTENT; // mutate! switch to JSON_CONTENT
        const err = await assertRejects(fiwareApi.rawRequest(config));
        assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_INVALID_JSON);
      });

      it('false - works with TEXT_CONTENT', async () => {
        const reqArg = {
          entityId: 'supanu',
          attrName: 'smokey',
          attrValue: false,
        };
        const config = fiwareApi.updateEntityAttrValue.createRequestConfig(reqArg);
        assert.strictEqual(config.headers['Content-Type'], 'text/plain', 'should be text');
        assert.strictEqual(config.data, 'false');
        await fiwareApi.rawRequest(config);
        const {data: doc} = await fiwareApi.findEntity({entityId: 'supanu'});
        const expectedAttr = {
          type: 'Robinson',
          value: false,
          metadata: {
            precision: {type: 'Number', value: 23},
          },
        };
        assert.deepStrictEqual(expectedAttr, doc.smokey, 'the attr looks invalid');
      });

      it('false - fails when sent as JSON_CONTENT', async () => {
        const reqArg = {
          entityId: 'supanu',
          attrName: 'smokey',
          attrValue: false,
        };
        const config = fiwareApi.updateEntityAttrValue.createRequestConfig(reqArg);
        config.headers = JSON_CONTENT; // mutate! switch to JSON_CONTENT
        const err = await assertRejects(fiwareApi.rawRequest(config));
        assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_INVALID_JSON);
      });

    });

  });

});
