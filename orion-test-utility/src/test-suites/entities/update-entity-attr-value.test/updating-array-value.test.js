/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');

const {
  assertRejects,
  ngsiResponseErrorUtils: {assertErrorsMatch},
} = require('@ratatosk/ngsi-utils');

const {
  ORION_ERRORS,
} = require('@ratatosk/ngsi-constants');

const {orionApi, ngsiApi} = require('../../../client-api-util');
const mongoApi = require('../../../mongo-api');

const TEXT_CONTENT = {'Content-Type': 'text/plain'};

const entity = {
  id: 'supanu',
  type: 'Kerchew',
  smokey: {
    type: 'Robinson',
    value: null,
    metadata: {precision: 23},
  },
};

describe('updating ARRAY value', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      beforeEach(async () => {
        await mongoApi.assertEntitiesEmpty();
        await fiwareApi.createEntity({entity});
      });
      afterEach(() => mongoApi.wipeEntities());

      it('works with JSON_CONTENT', async () => {
        const reqArg = {
          entityId: 'supanu',
          attrName: 'smokey',
          attrValue: [1, 2, 3, 'four'],
        };
        const config = fiwareApi.updateEntityAttrValue.createRequestConfig(reqArg);
        assert.strictEqual(config.headers['Content-Type'], 'application/json', 'should be json');
        assert.deepStrictEqual(config.data, [1, 2, 3, 'four']);
        await fiwareApi.rawRequest(config);
        const {data: doc} = await fiwareApi.findEntity({entityId: 'supanu'});
        const expectedAttr = {
          type: 'Robinson',
          value: [1, 2, 3, 'four'], // <-- the data
          metadata: {
            precision: {type: 'Number', value: 23},
          },
        };
        assert.deepStrictEqual(expectedAttr, doc.smokey, 'the attr looks invalid');
      });

      it('[] - fails when sent as TEXT_CONTENT', async () => {
        const reqArg = {
          entityId: 'supanu',
          attrName: 'smokey',
          attrValue: [2, 3, 4, 5],
        };
        const config = fiwareApi.updateEntityAttrValue.createRequestConfig(reqArg);
        config.headers = TEXT_CONTENT; // mutate! switch to TEXT_CONTENT

        // DIVERGE!!
        if (fiwareApi === orionApi) {
          const err = await assertRejects(fiwareApi.rawRequest(config));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_INVALID_VALUE);
        }
        if (fiwareApi === ngsiApi) {
          // NOT SURE ABOUT THIS. might be ok?
          // but it seems to just work as if it were JSON_CONTENT..
          await fiwareApi.rawRequest(config);
          const {data: attrValue} = await fiwareApi.findEntityAttrValue({entityId: 'supanu', attrName: 'smokey'});
          assert.deepStrictEqual(attrValue, [2, 3, 4, 5]);
        }

      });

      it('urlencoding a {key: string} with a forbidden characters works, but it is the ENCODED data that is saved in orion', async () => {
        const name = 'sp"do';
        const encodedName = encodeURI(name);
        assert(name !== encodedName, 'encoding name changes nothing');
        const reqArg = {
          entityId: 'supanu',
          attrName: 'smokey',
          attrValue: [encodedName],
        };
        const config = fiwareApi.updateEntityAttrValue.createRequestConfig(reqArg);
        assert.strictEqual(config.headers['Content-Type'], 'application/json', 'should be json');
        assert.deepStrictEqual(config.data, [encodedName]);
        await fiwareApi.rawRequest(config);
        const {data: doc} = await fiwareApi.findEntity({entityId: 'supanu'});
        const expectedAttr = {
          type: 'Robinson',
          value: [encodedName], // <-- the data
          metadata: {
            precision: {type: 'Number', value: 23},
          },
        };
        assert.deepStrictEqual(expectedAttr, doc.smokey, 'the attr looks invalid');
      });

    });

  });

});
