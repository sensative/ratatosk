/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const notificationsApi = require('../../notification-buffer-api');

describe('entities suites', () => {

  before(async () => notificationsApi.resetAll());

  // // test the most basic functionality
  require('./entity-crud-flowthrough.test');

  // // go through each endpoint
  require('./create-entity.test');
  require('./get-entities.test');
  require('./find-entity.test');
  require('./get-entity-attrs.test');
  require('./find-entity-attr.test');
  require('./find-entity-attr-value.test');
  require('./delete-entity.test');
  require('./delete-entity-attr.test');
  require('./update-entity-attr-value.test');
  require('./update-entity-attr.test');
  require('./upsert-entity-attrs.test');
  require('./replace-entity-attrs.test');
  require('./patch-entity-attrs.test');

  // // and some other general test-suites
  require('./location-geo.test');
  require('./date-time-handling.test');
  require('./general-service-path-wildcard.test');

  // // the bulk ops
  require('./bulk-query.test');
  require('./bulk-update.test');

});
