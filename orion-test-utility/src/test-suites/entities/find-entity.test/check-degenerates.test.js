/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

// Test all the different versions of patch value

const _ = require('lodash');
const assert = require('assert');
const serialPromise = require('promise-serial-exec');

const {
  assertRejects,
  ngsiResponseErrorUtils: {assertErrorsMatch},
} = require('@ratatosk/ngsi-utils');

const {
  ORION_ERRORS,
} = require('@ratatosk/ngsi-constants');


const {orionApi, ngsiApi} = require('../../../client-api-util');
const mongoApi = require('../../../mongo-api');

const entities = [
  {id: 'supanu', type: 'Kerchew'},
  {id: 'supanu', type: 'Wakkow'},
  {id: 'doh'},
];


describe('entities with same id, different types', async () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      before(async () => {
        await mongoApi.assertEntitiesEmpty();
        // create some entities to check against
        await serialPromise(_.map(entities, (entity) => async () => {
          return fiwareApi.createEntity({entity});
        }));
      });
      after(async () => mongoApi.wipeEntities());

      it('findEntity (by id) should fail (without type filter)', async () => {
        const err = await assertRejects(fiwareApi.findEntity({entityId: 'supanu'}));
        assertErrorsMatch(err, ORION_ERRORS.FIND_ERROR_TOO_MANY);
      });

      it('findEntity (by id) succeeds (when using VALID type filter), NOTE: this behaviour technically BREAKS the ngsi-v2 specification, but is eminently sensible', async () => {
        const params = {type: 'Kerchew'};
        const {data: doc} = await fiwareApi.findEntity({entityId: 'supanu', params});
        const expected = {id: 'supanu', type: 'Kerchew'};
        assert.deepStrictEqual(doc, expected, 'doc looks invalid');
      });

      it('findEntity (by id) should fail (when using INVALID type filter)', async () => {
        const params = {type: 'invalid_type'};
        const err = await assertRejects(fiwareApi.findEntity({entityId: 'supanu', params}));
        assertErrorsMatch(err, ORION_ERRORS.FIND_ERROR_ENTITY_MISSING);
      });

      it('findEntity (by id) can use either entityId OR entity.id, but entityId is first choice', async () => {
        const {data: doc1} = await fiwareApi.findEntity({entityId: 'doh'});
        const expected = {id: 'doh', type: 'Thing'};
        assert.deepStrictEqual(doc1, expected, 'doc1 looks incorrect');
        const {data: doc2} = await fiwareApi.findEntity({entity: {id: 'doh'}});
        assert.deepStrictEqual(doc2, expected, 'doc2 looks incorrect');
        const {data: doc3} = await fiwareApi.findEntity({entityId: 'doh', entity: {id: 'doh'}});
        assert.deepStrictEqual(doc3, expected, 'doc3 looks incorrect');
      });

    });

  });

});
