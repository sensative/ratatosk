/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

// Test all the different versions of patch value

const _ = require('lodash');
const assert = require('assert');

const {orionApi, ngsiApi} = require('../../../client-api-util');
const mongoApi = require('../../../mongo-api');

const entity = {
  id: 'sploder',
  type: 'gerpy',
  agda: 'gerbil',
  bo: 73,
};

describe('what happens when you try to get an attribute that does not exist?', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      before(async () => {
        await mongoApi.assertEntitiesEmpty();
        await fiwareApi.createEntity({entity});
      });
      after(async () => mongoApi.wipeEntities());

      it('get ["agda", "missing", "bo"] with no option mode', async () => {
        const attrs = ['agda', 'missing', 'bo'];
        const expected = {
          id: 'sploder',
          type: 'gerpy',
          agda: {type: 'Text', value: 'gerbil', metadata: {}},
          bo: {type: 'Number', value: 73, metadata: {}},
        };
        const {data: doc} = await fiwareApi.findEntity({entityId: 'sploder', params: {attrs}});
        assert.deepStrictEqual(expected, doc, 'filtered doc looks wrong (no options mode)');
      });

      it('get ["agda", "missing", "bo"] with KEY_VALUES option mode', async () => {
        const attrs = ['agda', 'missing', 'bo'];
        const params = {attrs, options: 'keyValues'};
        const expected = {
          id: 'sploder',
          type: 'gerpy',
          agda: 'gerbil',
          bo: 73,
        };
        const {data: doc} = await fiwareApi.findEntity({entityId: 'sploder', params});
        assert.deepStrictEqual(expected, doc, 'filtered doc looks wrong (KEY_VALUES options mode)');
      });

      it('get ["agda", "missing", "bo"] with VALUES option mode', async () => {
        const attrs = ['agda', 'missing', 'bo'];
        const params = {attrs, options: 'values'};
        const expected = ['gerbil', 73]; // it just omits that value!! shifts indices!!
        const {data: doc} = await fiwareApi.findEntity({entityId: 'sploder', params});
        assert.deepStrictEqual(expected, doc, 'filtered doc looks wrong (VALUES options mode)');
      });

      it('get ["agda", "missing", "bo"] with UNIQUE option mode (with some missing attrs removals)', async () => {
        const attrs = ['agda', 'missing', 'bo', 'jerks', 'missing2'];
        const params = {attrs, options: 'unique'};
        const expected = ['gerbil', 73]; // it just omits that value!! shifts indices!!
        const {data: doc} = await fiwareApi.findEntity({entityId: 'sploder', params});
        assert.deepStrictEqual(expected, doc, 'filtered doc looks wrong (UNIQUE options mode)');
      });

    });

  });

});
