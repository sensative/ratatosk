/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

// Test all the different versions of patch value

const _ = require('lodash');
const assert = require('assert');

const {
  BUILTIN_ATTRIBUTES,
} = require('@ratatosk/ngsi-constants');


const {orionApi, ngsiApi} = require('../../../client-api-util');
const mongoApi = require('../../../mongo-api');

const entity = {
  id: 'sploder',
  type: 'gerpy',
  agda: 'gerbil',
  bo: 73,
  jerks: {type: 'Norbos', value: 73},
  niblets: {type: 'pork', value: null},
  trinkets: 'gerbil',
};

describe('builtin attributes & "*" etc..', () => {

  const createdAt = BUILTIN_ATTRIBUTES.CREATED_AT.name;
  const modifiedAt = BUILTIN_ATTRIBUTES.MODIFIED_AT.name;
  const expiresAt = BUILTIN_ATTRIBUTES.EXPIRES_AT.name;

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      before(async () => {
        await mongoApi.assertEntitiesEmpty();
        await fiwareApi.createEntity({entity});
      });
      after(async () => mongoApi.wipeEntities());

      it('just get the builtin attrs (no options mode)', async () => {
        const attrs = [createdAt, modifiedAt, expiresAt];
        const params = {attrs};
        const expectedStripped = {
          id: 'sploder',
          type: 'gerpy',
        };
        const {data: doc} = await fiwareApi.findEntity({entityId: 'sploder', params});
        assert.deepStrictEqual(expectedStripped, _.omit(doc, attrs), 'stripped doc looks wrong');
        assert(_.has(doc, [createdAt, 'value']), 'createdAt is missing');
        assert(_.has(doc, [modifiedAt, 'value']), 'modifiedAt is missing');
        assert(!_.has(doc, [expiresAt, 'value']), 'expiresAt SHOULD BE missing');
      });

      it('just get the builtin attrs (all other modes)', async () => {
        const attrs = [createdAt, modifiedAt, expiresAt];
        // keyValues
        const paramsKV = {attrs, options: 'keyValues'};
        const {data: docKV} = await fiwareApi.findEntity({entityId: 'sploder', params: paramsKV});
        assert.strictEqual(_.size(docKV), 4, 'keyValues should have 4 props (id, type, dateCreated, and dateModified)');
        // values
        const paramsV = {attrs, options: 'values'};
        const {data: docV} = await fiwareApi.findEntity({entityId: 'sploder', params: paramsV});
        assert.strictEqual(docV.length, 2, 'there should be two dates');
        // unique
        const paramsU = {attrs, options: 'unique'};
        const {data: docU} = await fiwareApi.findEntity({entityId: 'sploder', params: paramsU});
        assert.strictEqual(docU.length, 1, 'there should be the one date (mod & created are the same)');
      });

      it('get createdAt and * (KEY_VALUES mode - reduce verbosity..)', async () => {
        const attrs = [createdAt, '*'];
        const params = {attrs, options: 'keyValues'};
        const expectedStripped = {
          id: 'sploder',
          type: 'gerpy',
          agda: 'gerbil',
          bo: 73,
          jerks: 73,
          niblets: null,
          trinkets: 'gerbil',
        };
        const {data: doc} = await fiwareApi.findEntity({entityId: 'sploder', params});
        assert.deepStrictEqual(expectedStripped, _.omit(doc, createdAt), 'stripped doc looks wrong');
        assert(_.has(doc, createdAt), 'createdAt is missing');
      });

    });

  });

});
