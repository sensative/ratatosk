/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

// Test all the different versions of patch value
const _ = require('lodash');
const assert = require('assert');

const {orionApi, ngsiApi} = require('../../../client-api-util');
const mongoApi = require('../../../mongo-api');

const entity = {
  id: 'sploder',
  type: 'gerpy',
  trinkets: 'gerbil',
  bo: 73,
  niblets: {type: 'pork', value: null},
  jerks: {type: 'Norbos', value: 73}, // duplicates 73's (for unique check)
  agda: 'gerbil',
};

describe('filter by attribute & set optional representation type', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      before(async () => {
        await mongoApi.assertEntitiesEmpty();
        await fiwareApi.createEntity({entity});
      });
      after(async () => mongoApi.wipeEntities());

      it('full entity (no attr filter, no options) should look as expected', async () => {
        const expected = {
          id: 'sploder',
          type: 'gerpy',
          agda: {type: 'Text', value: 'gerbil', metadata: {}},
          bo: {type: 'Number', value: 73, metadata: {}},
          jerks: {type: 'Norbos', value: 73, metadata: {}},
          niblets: {type: 'pork', value: null, metadata: {}},
          trinkets: {type: 'Text', value: 'gerbil', metadata: {}},
        };
        const {data: doc} = await fiwareApi.findEntity({entityId: 'sploder'});
        assert.deepStrictEqual(expected, doc, 'full doc looks wrong');
      });

      it('full entity, (no attr filter, KEY_VALUES option) should look as expected', async () => {
        const expected = {
          id: 'sploder',
          type: 'gerpy',
          trinkets: 'gerbil',
          jerks: 73,
          bo: 73,
          agda: 'gerbil',
          niblets: null,
        };
        const params = {options: 'keyValues'};
        const {data: doc} = await fiwareApi.findEntity({entityId: 'sploder', params});
        assert.deepStrictEqual(expected, doc, 'full doc looks wrong (KEY_VALUES)');
      });

      it('full entity, (no attr filter, VALUES option) should look as expected', async () => {
        // by default, the order of the attributes is alphabetical by attrName
        const expected = ['gerbil', 73, 73, null, 'gerbil'];
        const params = {options: 'values'};
        const {data: doc} = await fiwareApi.findEntity({entityId: 'sploder', params});
        // the sort order is NOT guaranteed
        doc.sort();
        expected.sort();
        assert.deepStrictEqual(doc, expected, 'full doc looks wrong (VALUES)');
      });

      it('full entity, (no attr filter, UNIQUE mode) should look as expected', async () => {
        const expected = ['gerbil', null, 73];
        const params = {options: 'unique'};
        const {data: doc} = await fiwareApi.findEntity({entityId: 'sploder', params});
        // NOTE that order is not guaranteed when attrs not included, hence the sort()
        assert.deepStrictEqual(expected.sort(), doc.sort(), 'full doc looks wrong (UNIQUE)');
      });

      it('filtered entity (3 custom attrs, no options) should look as expected', async () => {
        const expected = {
          id: 'sploder',
          type: 'gerpy',
          agda: {type: 'Text', value: 'gerbil', metadata: {}},
          bo: {type: 'Number', value: 73, metadata: {}},
          jerks: {type: 'Norbos', value: 73, metadata: {}},
        };
        const params = {attrs: ['jerks', 'agda', 'bo']};
        const {data: doc} = await fiwareApi.findEntity({entityId: 'sploder', params});
        assert.deepStrictEqual(expected, doc, 'filtered doc looks wrong (no options mode)');
      });

      it('filtered entity (3 custom attrs, KEY_VALUES mode) should look as expected', async () => {
        const expected = {
          id: 'sploder',
          type: 'gerpy',
          agda: 'gerbil',
          bo: 73,
          jerks: 73,
        };
        const params = {attrs: ['jerks', 'agda', 'bo'], options: 'keyValues'};
        const {data: doc} = await fiwareApi.findEntity({entityId: 'sploder', params});
        assert.deepStrictEqual(expected, doc, 'filtered doc looks wrong (KEY_VALUES)');
      });

      it('filtered entity (3 custom attrs, VALUES mode) should look as expected', async () => {
        const expected = [73, 'gerbil', 73];
        const params = {attrs: ['jerks', 'agda', 'bo'], options: 'values'};
        const {data: doc} = await fiwareApi.findEntity({entityId: 'sploder', params});
        assert.deepStrictEqual(expected, doc, 'filtered doc looks wrong (VALUES)');
      });

      it('filtered entity (3 custom attrs, UNIQUE mode) should look as expected', async () => {
        const expected = [73, 'gerbil'];
        const params = {attrs: ['jerks', 'agda', 'bo'], options: 'unique'};
        const {data: doc} = await fiwareApi.findEntity({entityId: 'sploder', params});
        assert.deepStrictEqual(expected, doc, 'filtered doc looks wrong (UNIQUE)');
      });

      it('filtered entity (several attrs, compare VALUES to UNIQUE mode) - nail down how redundant values are removed for UNIQUE', async () => {
        const attrs = ['jerks', 'agda', 'niblets', 'bo', 'trinkets'];
        const expectedValues = [73, 'gerbil', null, 73, 'gerbil'];
        const paramsV = {attrs, options: 'values'};
        const {data: docsV} = await fiwareApi.findEntity({entityId: 'sploder', params: paramsV});
        const expectedUniques = [73, 'gerbil', null];
        const paramsU = {attrs, options: 'unique'};
        const {data: docsU} = await fiwareApi.findEntity({entityId: 'sploder', params: paramsU});
        assert.deepStrictEqual(expectedValues, docsV, 'doc looks wrong (VALUES)');
        assert.deepStrictEqual(expectedUniques, docsU, 'doc looks wrong (UNIQUE)');
      });

    });

  });

});
