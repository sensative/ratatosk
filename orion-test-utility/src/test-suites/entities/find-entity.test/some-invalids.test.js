/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

// Test all the different versions of patch value

const _ = require('lodash');
const assert = require('assert');

const {
  assertRejects,
  ngsiResponseErrorUtils: {assertErrorsMatch},
} = require('@ratatosk/ngsi-utils');

const {
  ORION_ERRORS,
} = require('@ratatosk/ngsi-constants');

const {orionApi, ngsiApi} = require('../../../client-api-util');
const mongoApi = require('../../../mongo-api');

describe('invalid id or representation option', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      before(async () => {
        await mongoApi.assertEntitiesEmpty();
      });
      after(async () => mongoApi.wipeEntities());

      it('finding with an invalid id (that does not exist) should fail', async () => {
        const err = await assertRejects(fiwareApi.findEntity({entityId: 'nonexistent-entity'}));
        assertErrorsMatch(err, ORION_ERRORS.FIND_ERROR_ENTITY_MISSING);
      });

      it('findEntity with valid id, but invalid representation option', async () => {
        const entity = {id: 'schpo', type: 'Kerchew', spam: 'muppets'};
        await fiwareApi.createEntity({entity});
        // first make sure that we DO get what we expect with some valid options
        const params = {options: 'unique'};
        const {data: doc} = await fiwareApi.findEntity({entityId: 'schpo', params});
        const expected = ['muppets'];
        assert.deepStrictEqual(doc, expected, 'did not get the expected doc..');
        // then check what happens with some invalid representation option
        const config = fiwareApi.findEntity.createRequestConfig({entityId: 'schpo'});
        _.set(config, 'params.options', 'eerrrp'); // mutate!
        const err = await assertRejects(fiwareApi.rawRequest(config));
        assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_INVALID_OPTIONS_PARAM);
        await mongoApi.wipeEntities();
      });

    });

  });

});
