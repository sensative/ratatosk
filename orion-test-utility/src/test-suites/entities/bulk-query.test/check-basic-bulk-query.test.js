/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const serialPromise = require('promise-serial-exec');

const {
  assertRejects,
  assertThrows,
  ngsiResponseErrorUtils: {assertErrorsMatch},
} = require('@ratatosk/ngsi-utils');

const {ORION_ERRORS} = require('@ratatosk/ngsi-constants');

const {
  orionApi,
  ngsiApi,
} = require('../../../client-api-util');

const mongoApi = require('../../../mongo-api');

const entities = [
  {
    id: 'appy',
    type: 'erp',
    doofie: 'aggy',
    boofie: {value: 2, metadata: {bib: 3, did: 4}},
  },
  {
    id: 'pappy',
    type: 'rptttt',
  },
];

describe('check-basic-bulk-query', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      before(async () => {
        await mongoApi.assertEntitiesEmpty();
        await serialPromise(_.map(entities, (entity) => async () => {
          await fiwareApi.createEntity({entity});
        }));
      });
      after(async () => mongoApi.wipeEntities());

      describe('basics', () => {

        it('payload cannot be empty, even though ALL ELEMENTS ARE OPTIONAL!', async () => {
          const config = fiwareApi.bulkQuery.createRequestConfig({payload: {}});
          assert.deepStrictEqual(config.data, {});

          // DIVERGE!!
          if (fiwareApi === orionApi) {
            const err = await assertRejects(fiwareApi.rawRequest(config));
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_EMPTY_PAYLOAD);
          }
          if (fiwareApi === ngsiApi) {
            const {data: docs} = await fiwareApi.rawRequest(config);
            assert.strictEqual(docs.length, 2);
          }
        });

        it('unknown payload keys are not allowed (for an otherwise empty payload at least)', async () => {
          const config = fiwareApi.bulkQuery.createRequestConfig({payload: {}});
          _.set(config, 'data', {someInvalidProp: 'derp'});
          const err = await assertRejects(fiwareApi.rawRequest(config));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_NO_RELEVANT_FIELDS);
        });

        it('payload cannot be empty', async () => {
          const empties = [
            null,
            undefined,
            false,
            0,
          ];
          const config = fiwareApi.bulkQuery.createRequestConfig({payload: {}});
          await serialPromise(_.map(empties, (empty) => async () => {
            _.set(config, 'data', empty); // mutate!!

            // DIVERGE!! bodyParser creates empty object instead of invalid value..
            if (fiwareApi === orionApi) {
              const err = await assertRejects(fiwareApi.rawRequest(config));
              assertErrorsMatch(err, ORION_ERRORS.CONTENT_LENGTH_REQUIRED);
            }
            if (fiwareApi === ngsiApi) {
              const {data: docs} = await fiwareApi.rawRequest(config);
              assert.strictEqual(docs.length, 2);
            }

          }));
        });

        it('either entity.id or entity.idPattern must be present', async () => {
          const config = fiwareApi.bulkQuery.createRequestConfig({payload: {}});
          _.set(config, 'data.entities', [{type: 'asdf'}]);
          const err = await assertRejects(fiwareApi.rawRequest(config));
          assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_MISSING_ID_OR_PATTERN);
        });

      });

      describe('attrs & metadata', () => {

        it('use attrs projection', async () => {
          const payload = {attrs: ['doofie', 'biff']};
          const {data: docs} = await fiwareApi.bulkQuery({payload});
          const doc = _.find(docs, {id: 'appy'});
          const expected = {
            id: 'appy',
            type: 'erp',
            doofie: {type: 'Text', value: 'aggy', metadata: {}},
          };
          assert.deepStrictEqual(doc, expected);
        });

        it('use attrs projection WITH another irrelevant key', async () => {
          const config = fiwareApi.bulkQuery.createRequestConfig({});
          _.set(config, 'data', {attrs: ['doofie'], uknownKey: 'sbloof'});
          const err = await assertRejects(fiwareApi.rawRequest(config));

          // DIVERGE!! (this is dumb. always have the same error for extraneous keys)
          if (fiwareApi === orionApi) {
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_UNKNOWN_PAYLOAD_KEY);
          }
          if (fiwareApi === ngsiApi) {
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_NO_RELEVANT_FIELDS);
          }

        });

        it('use attrs & metadata projection', async () => {
          const payload = {attrs: 'boofie', metadata: 'bib,derp'};
          const {data: docs} = await fiwareApi.bulkQuery({payload});
          const doc = _.find(docs, {id: 'appy'});
          const expected = {
            id: 'appy',
            type: 'erp',
            boofie: {
              type: 'Number',
              value: 2,
              metadata: {bib: {type: 'Number', value: 3}},
            },
          };
          assert.deepStrictEqual(doc, expected);
        });

      });

      describe('entityId', () => {

        it('use entityId filter (single member)', async () => {
          const payload = {entities: [{id: 'appy'}]};
          const {data: docs} = await fiwareApi.bulkQuery({payload});
          assert.strictEqual(docs.length, 1);
          assert.strictEqual(docs[0].id, 'appy');
        });

        it('an id must be valid (for ngsi server)', async () => {
          const invalids = [
            {id: '', err: ORION_ERRORS.BAD_REQ_TOO_SHORT_ENTITY_ID},
            {id: 'has#', err: ORION_ERRORS.BAD_REQ_INVALID_CHAR_IN_ENTITY_ID},
            {id: 'has\'', err: ORION_ERRORS.BAD_REQ_INVALID_CHAR_IN_ENTITY_ID},
          ];
          await serialPromise(_.map(invalids, (invalid) => async () => {
            const payload = {entities: [{id: invalid.id}]};
            assertThrows(() => fiwareApi.bulkQuery.createRequestConfig({payload}));
            const config = fiwareApi.bulkQuery.createRequestConfig({});
            _.set(config, 'data.entities', payload.entities);
            const err = await assertRejects(() => fiwareApi.rawRequest(config));
            assertErrorsMatch(err, invalid.err);
          }));
        });

        it('use entityId filter (using an array of ids) - still single member', async () => {
          const payload = {entities: [{id: ['appy']}]};

          // DIVERGE!!
          if (fiwareApi === orionApi) {
            const err = await assertRejects(fiwareApi.bulkQuery({payload}));
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_INVALID_ENTITY_ID_TYPE);
          }
          if (fiwareApi === ngsiApi) {
            const {data: docs} = await fiwareApi.bulkQuery({payload});
            assert.strictEqual(docs.length, 1);
            assert.strictEqual(docs[0].id, 'appy');
          }
        });

        it('use entityId filter using a string list of ids (2 members)', async () => {
          const payload = {entities: [{id: 'appy,dappy'}]};
          const {data: docs} = await fiwareApi.bulkQuery({payload});

          // DIVERGE!! (orionApi does NOT split the id into an array)
          if (fiwareApi === orionApi) {
            assert.strictEqual(docs.length, 0);
          }
          if (fiwareApi === ngsiApi) {
            assert.strictEqual(docs.length, 1);
          }
        });

      });

      describe('entityIdPattern', () => {

        it('works with one of these..', async () => {
          const payload = {entities: [{idPattern: 'papp'}]};
          const {data: docs} = await fiwareApi.bulkQuery({payload});
          assert.strictEqual(docs.length, 1);
        });

      });

      describe('entityTypePattern', () => {

        it('this wont break it at least', async () => {
          const payload = {entities: [{id: 'appy', typePattern: 'er'}]};
          const {data: docs} = await fiwareApi.bulkQuery({payload});
          assert.strictEqual(docs.length, 1);
        });

      });

      describe('entityType', () => {

        it('use entityType filter (single member)', async () => {
          const payload = {entities: [{id: 'appy', type: 'erp'}]};
          const {data: docs} = await fiwareApi.bulkQuery({payload});
          assert.strictEqual(docs.length, 1);
        });

        it('a type must be valid (for ngsi server)', async () => {
          const invalids = [
            {type: '', err: ORION_ERRORS.BAD_REQ_TOO_SHORT_ENTITY_TYPE},
            {type: 'has#', err: ORION_ERRORS.BAD_REQ_INVALID_CHAR_IN_ENTITY_TYPE},
            {type: 'has\'', err: ORION_ERRORS.BAD_REQ_INVALID_CHAR_IN_ENTITY_TYPE},
          ];
          await serialPromise(_.map(invalids, (invalid) => async () => {
            const payload = {entities: [{id: 'appy', type: invalid.type}]};
            assertThrows(() => fiwareApi.bulkQuery.createRequestConfig({payload}));
            const config = fiwareApi.bulkQuery.createRequestConfig({});
            _.set(config, 'data.entities', payload.entities);
            const err = await assertRejects(() => fiwareApi.rawRequest(config));
            assertErrorsMatch(err, invalid.err);
          }));
        });

        it('use entityType filter (using an array of type) - still single member', async () => {
          const payload = {entities: [{id: 'appy', type: ['erp']}]};

          // DIVERGE!!
          if (fiwareApi === orionApi) {
            const err = await assertRejects(fiwareApi.bulkQuery({payload}));
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_INVALID_ENTITY_TYPE_TYPE);
          }
          if (fiwareApi === ngsiApi) {
            const {data: docs} = await fiwareApi.bulkQuery({payload});
            assert.strictEqual(docs.length, 1);
            assert.strictEqual(docs[0].id, 'appy');
          }
        });

        it('use entityType filter using a string list of ids (2 members)', async () => {
          const payload = {entities: [{id: 'appy', type: 'erp,rptttt'}]};
          const {data: docs} = await fiwareApi.bulkQuery({payload});

          // DIVERGE!! (orionApi does NOT split the type string into an array)
          if (fiwareApi === orionApi) {
            assert.strictEqual(docs.length, 0);
          }
          if (fiwareApi === ngsiApi) {
            assert.strictEqual(docs.length, 1);
          }
        });

      });

    });

  });

});
