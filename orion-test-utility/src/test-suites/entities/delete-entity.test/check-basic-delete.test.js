/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const serialPromise = require('promise-serial-exec');

const {
  assertRejects,
  ngsiResponseErrorUtils: {assertErrorsMatch},
  ngsiHeaderUtils: {createServicePathHeader},
  ngsiEntityFormatter: {formatEntity},
} = require('@ratatosk/ngsi-utils');

const {ORION_ERRORS} = require('@ratatosk/ngsi-constants');

const mongoApi = require('../../../mongo-api');
const {orionApi, ngsiApi} = require('../../../client-api-util');

const createEntities = async (templates, fiwareApi) => {
  await serialPromise(_.map(templates, (template) => async () => {
    const headers = createServicePathHeader(template.servicePath);
    const entity = formatEntity(_.omit(template, 'servicePath'));
    return fiwareApi.createEntity({entity, headers});
  }));
};

describe('check-basic-delete', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      beforeEach(() => mongoApi.assertEntitiesEmpty());
      afterEach(() => mongoApi.wipeEntities());

      it('create 3 ents (different ids, same type & sp), delete 1 by id', async () => {
        const entities = [
          {id: 'bruno'},
          {id: 'poppers'},
          {id: 'jibby'},
        ];
        await createEntities(entities, fiwareApi);
        const config = fiwareApi.deleteEntity.createRequestConfig({entityId: 'poppers'});
        const res = await fiwareApi.rawRequest(config);
        assert.strictEqual(res.status, 204, 'status should be 204');
        const numDocs = await mongoApi.countEntities();
        assert.strictEqual(numDocs, 2, 'there should be two docs left');
        const {data: docs} = await fiwareApi.getEntities();
        const names = _.map(docs, 'id');
        assert.deepStrictEqual(names.sort(), ['bruno', 'jibby'], 'remaining names do not match');
      });

      it('trying to delete a doc when query finds more than one', async () => {
        const entities = [
          {id: 'bruno', type: 'sooper'},
          {id: 'bruno', type: 'trooper'},
        ];
        await createEntities(entities, fiwareApi);
        const err = await assertRejects(fiwareApi.deleteEntity({entityId: 'bruno'}));
        assertErrorsMatch(err, ORION_ERRORS.FIND_ERROR_TOO_MANY);
      });

      it('create 3 ents (same ids, same type & different sp), delete 1 by id', async () => {
        const entities = [
          {id: 'bruno', servicePath: '/aaa'},
          {id: 'bruno', servicePath: '/bbb'},
          {id: 'bruno', servicePath: '/ccc'},
        ];
        await createEntities(entities, fiwareApi);
        const headers = createServicePathHeader('/bbb');
        await fiwareApi.deleteEntity({entityId: 'bruno', headers});
        const numDocs = await mongoApi.countEntities();
        assert.strictEqual(numDocs, 2, 'there should be two docs left');
      });

      it('delete ALWAYS needs an explicit servicePath -- otherwise EXACTLY "/" is assumed', async () => {
        const entities = [
          {id: 'bruno', servicePath: '/'},
          {id: 'bruno', servicePath: '/bbb'},
        ];
        await createEntities(entities, fiwareApi);
        // deleting without a sp, should delete the '/' one
        await fiwareApi.deleteEntity({entityId: 'bruno'});
        let numDocs = await mongoApi.countEntities();
        assert.strictEqual(numDocs, 1, 'there should be one doc left');
        // deleting another one should create a NOT FOUND ERROR
        const err = await assertRejects(fiwareApi.deleteEntity({entityId: 'bruno'}));
        assertErrorsMatch(err, ORION_ERRORS.FIND_ERROR_ENTITY_MISSING);
        // but deleting with a servicePath gets rid of the other one too
        const headers = createServicePathHeader('/bbb');
        await fiwareApi.deleteEntity({entityId: 'bruno', headers});
        numDocs = await mongoApi.countEntities();
        assert.strictEqual(numDocs, 0, 'there should be 0 docs left');
      });

      it('create 3 ents (same ids, different types, same sp), delete 1 by id', async () => {
        const entities = [
          {id: 'bruno', type: 'jaggy'},
          {id: 'bruno', type: 'kaggy'},
          {id: 'bruno', type: 'laggy'},
        ];
        await createEntities(entities, fiwareApi);
        const params = {type: 'kaggy'};
        await fiwareApi.deleteEntity({entityId: 'bruno', params});
        const numDocs = await mongoApi.countEntities();
        assert.strictEqual(numDocs, 2, 'there should be two docs left');
        const {data: docs} = await fiwareApi.getEntities();
        const types = _.map(docs, 'type');
        assert.deepStrictEqual(types.sort(), ['jaggy', 'laggy'], 'remaining types do not match');
      });

      it('create 2 entities with same id, different type or sp, but separable with a q query, FAILS with orion, but  NOT with ngsi-server', async () => {
        const entities = [
          {id: 'bruno', type: 'arby', doapy: 23},
          {id: 'bruno', type: 'ybra', soapy: 27},
        ];
        await createEntities(entities, fiwareApi);
        // create a requestArg (WITH q-query) that is confirmed to only find 1 doc
        const requestArg = {entityId: 'bruno', params: {q: 'soapy'}};
        const {data: soapies} = await fiwareApi.getEntities({params: {q: 'soapy'}});
        assert.strictEqual(soapies.length, 1, 'there should only be one soapy');
        assert.strictEqual(soapies[0].type, 'ybra', 'and it has typ ybra');

        // DIVERGE!!
        if (fiwareApi === orionApi) {
          // but the same requestArg does NOT work for the fiwareApi.deleteEntity!!!
          const err = await assertRejects(fiwareApi.deleteEntity(requestArg));
          assertErrorsMatch(err, ORION_ERRORS.FIND_ERROR_TOO_MANY);
          // BUT!!!! Once again, the "type" disambiguation DOES work..
          await fiwareApi.deleteEntity({entityId: 'bruno', params: {type: 'ybra'}});
          const {data: leftOvers} = await fiwareApi.getEntities();
          assert.strictEqual(leftOvers.length, 1, 'there should be one left over');
          assert.strictEqual(leftOvers[0].type, 'arby', 'and it should have type "arby"');
        }
        if (fiwareApi === ngsiApi) {
          const initDocs = await mongoApi.findEntities();
          assert.strictEqual(initDocs.length, 2);
          await fiwareApi.deleteEntity(requestArg); // works here
          const finalDocs = await mongoApi.findEntities();
          assert.strictEqual(finalDocs.length, 1);
        }

      });

    });

  });

});
