/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const serialPromise = require('promise-serial-exec');

const {
  assertRejects,
  ngsiResponseErrorUtils: {assertErrorsMatch},
} = require('@ratatosk/ngsi-utils');

const {ORION_ERRORS} = require('@ratatosk/ngsi-constants');

const mongoApi = require('../../../mongo-api');
const {orionApi, ngsiApi} = require('../../../client-api-util');

describe('data-payload-prohibited.test: the orionApi.deleteEntity request does not like having stuff sent in payload', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      beforeEach(() => mongoApi.assertEntitiesEmpty());
      afterEach(() => mongoApi.wipeEntities());

      it('would be valid to begin with, but for this test-suite, no entities exist', async () => {
        const config = fiwareApi.deleteEntity.createRequestConfig({entityId: 'validId'});
        const err0 = await assertRejects(fiwareApi.deleteEntity({entityId: 'validId'}));
        const err1 = await assertRejects(fiwareApi.rawRequest(config));
        assertErrorsMatch(err0, err1, 'they are doing the exact same thing');
        assertErrorsMatch(err0, ORION_ERRORS.FIND_ERROR_ENTITY_MISSING);
      });

      it('seems like there is a special error if data.id is set', async () => {
        await fiwareApi.createEntity({entity: {id: 'validId'}});
        const config = fiwareApi.deleteEntity.createRequestConfig({entityId: 'validId'});
        _.set(config, 'data', {id: 'alsoValidId', type: 'sadf'});
        const err = await assertRejects(fiwareApi.rawRequest(config));
        assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_ENTITY_ID_IN_PAYLOAD);
      });

      it('seems like there is a special error if data.type is set', async () => {
        await fiwareApi.createEntity({entity: {id: 'validId'}});
        const config = fiwareApi.deleteEntity.createRequestConfig({entityId: 'validId'});
        _.set(config, 'data', {type: 'drep'});
        const err = await assertRejects(fiwareApi.rawRequest(config));
        assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_ENTITY_TYPE_IN_PAYLOAD);
      });

      it('if payload params is set but empty then a special error comes up', async () => {
        const config = fiwareApi.deleteEntity.createRequestConfig({entityId: 'validId'});

        const emptyParams = [
          {},
          {rubbbishyParam: undefined},
          {q: undefined},
          {id: undefined},
        ];
        await serialPromise(_.map(emptyParams, (params) => async () => {
          _.set(config, 'data', params);

          // DIVERGE!!
          if (fiwareApi === orionApi) {
            const errEmpty = await assertRejects(fiwareApi.rawRequest(config));
            assertErrorsMatch(errEmpty, ORION_ERRORS.BAD_REQ_EMPTY_PAYLOAD);
          }
          if (fiwareApi === ngsiApi) { // WORKS FOR NGSI-SERVER
            await fiwareApi.createEntity({entity: {id: 'validId'}});
            await fiwareApi.rawRequest(config);
            const mongoDocs = await mongoApi.findEntities();
            assert.strictEqual(mongoDocs.length, 0);
          }
        }));
      });

      it('but for most params it is the same error', async () => {
        const config = fiwareApi.deleteEntity.createRequestConfig({entityId: 'validId'});
        const otherwiseValidParams = [
          {q: '!arg'},
          {mq: '!argy.bargy'},
          {typePattern: 'blues'},
          {idPattern: 'sppoon'},
          {somethingRidiculous: 'rubbish', dimpl: 'shmuzz'},
          {nully: null},
        ];
        await serialPromise(_.map(otherwiseValidParams, (params) => async () => {
          _.set(config, 'data', params);

          // DIVERGE!!
          if (fiwareApi === orionApi) {
            const errStandard = await assertRejects(fiwareApi.rawRequest(config));
            assertErrorsMatch(errStandard, ORION_ERRORS.BAD_REQ_ATTR_MUST_BE_JSON);
          }
          if (fiwareApi === ngsiApi) { // WORKS FOR NGSI-SERVER
            await fiwareApi.createEntity({entity: {id: 'validId'}});
            await fiwareApi.rawRequest(config);
            const mongoDocs = await mongoApi.findEntities();
            assert.strictEqual(mongoDocs.length, 0);
          }
        }));
      });

    });

  });

});
