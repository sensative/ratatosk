/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');

const {orionApi, ngsiApi} = require('../../client-api-util');
const mongoApi = require('../../mongo-api');

const {
  assertRejects,
  ngsiResponseErrorUtils: {assertErrorsMatch},
} = require('@ratatosk/ngsi-utils');

const {
  ORION_ERRORS,
} = require('@ratatosk/ngsi-constants');

describe('delete-entity-attr', () => {

  const fiwareApis = {
    orionApi,
    ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      before(() => mongoApi.assertEntitiesEmpty());
      after(() => mongoApi.wipeEntities());

      describe('the basics', () => {

        beforeEach(async () => {
          await mongoApi.wipeEntities();
          const entity = {
            id: 'sploder',
            type: 'gerpy',
            agda: 'gerbil',
            bo: 73,
            jerks: {type: 'Norbos', value: 73},
          };
          await fiwareApi.createEntity({entity});
          const entityX = {
            id: 'sploder',
            type: 'keet',
          };
          await fiwareApi.createEntity({entity: entityX});
        });
        after(() => mongoApi.wipeEntities());

        it('deleteEntityAttr works when disambiguating with type', async () => {
          const reqArg = {entityId: 'sploder', attrName: 'jerks', params: {type: 'gerpy'}};
          const res = await fiwareApi.deleteEntityAttr(reqArg);
          assert.strictEqual(res.status, 204, 'status should be 204');
          const {data: doc} = await fiwareApi.findEntity(_.omit(reqArg, 'attrName'));
          const expected = {
            id: 'sploder',
            type: 'gerpy',
            agda: {type: 'Text', value: 'gerbil', metadata: {}},
            bo: {type: 'Number', value: 73, metadata: {}},
          };
          assert.deepStrictEqual(doc, expected);
        });

        it('delete-entity-attr CANNOT be used with orion to disambiguate. works for ngsi-server', async () => {
          const reqArg = {entityId: 'sploder', attrName: 'jerks', params: {q: 'bo>49'}};

          // DIVERGE!!
          if (fiwareApi === orionApi) {
            const err = await assertRejects(fiwareApi.deleteEntityAttr(reqArg));
            assertErrorsMatch(err, ORION_ERRORS.FIND_ERROR_TOO_MANY);
          }
          if (fiwareApi === ngsiApi) {
            await fiwareApi.deleteEntityAttr(reqArg); // works
          }
        });

        it('adding payloads that Orion deems invalid', async () => {
          const reqArg = {entityId: 'sploder', attrName: 'jerks', params: {type: 'gerpy'}};
          const config = fiwareApi.deleteEntityAttr.createRequestConfig(reqArg);
          _.set(config, 'data', {derp: 'isvalid'});

          // DIVERGE!!
          if (fiwareApi === orionApi) {
            const err = await assertRejects(fiwareApi.rawRequest(config));
            assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_UNRECOGNIZED_ATTR_PROP);

          }
          if (fiwareApi === ngsiApi) { // works (payload is just ignored)
            await fiwareApi.rawRequest(config);
          }
        });

        it('adding payloads that Orion deems valid, is IGNORED by both orion & ngsi-server..', async () => {
          const reqArg = {entityId: 'sploder', attrName: 'jerks', params: {type: 'gerpy'}};
          const config = fiwareApi.deleteEntityAttr.createRequestConfig(reqArg);
          _.set(config, 'data', {type: 'someothertype'});

          await fiwareApi.rawRequest(config);
          const {data: attrs} = await fiwareApi.getEntityAttrs({entityId: 'sploder', params: {type: 'gerpy'}});
          assert(_.isNil(attrs.sploder));
        });

      });

    });

  });

});
