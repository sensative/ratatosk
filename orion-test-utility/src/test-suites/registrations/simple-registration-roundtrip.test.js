/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const assert = require('assert');
const _ = require('lodash');

const {
  assertRejects,
  ngsiResponseErrorUtils: {assertErrorsMatch},
} = require('@ratatosk/ngsi-utils');

const {
  ORION_ERRORS,
} = require('@ratatosk/ngsi-constants');

const {
  orionApi,
  // ngsiApi,
} = require('../../client-api-util');
const mongoApi = require('../../mongo-api');
const notificationsApi = require('../../notification-buffer-api');

const {notificationBuffer: {host, port}} = require('../../config');

const simpleEntity = {
  id: 'lamni',
  type: 'merp',
  // doo: 'ber',
};

const simpleRegistration = {
  description: 'shoobidoobee',
  dataProvided: {
    entities: [_.pick(simpleEntity, ['id', 'type'])],
    attrs: ['doo'],
  },
  provider: {
    legacyForwarding: true,
    http: {url: `http://${host}:${port}/commands`},
  },
};

const expectedMongoReg = {
  servicePath: '/',
  format: 'JSON',
  description: 'shoobidoobee',
  contextRegistration: [
    {
      attrs: [{name: 'doo', type: ''}],
      entities: [
        {
          id: 'lamni',
          type: 'merp',
        },
      ],
      providingApplication: `http://${host}:${port}/commands`,
    },
  ],
};

const expectedMongoEnt = {
  _id: {id: 'lamni', type: 'merp', servicePath: '/'},
  attrNames: [],
  attrs: {},
};

// convenience functions
const scrubRegistration = (reg) => {
  return _.omit(reg, ['_id', 'expiration']);
};
const scrubEntity = (ent) => {
  const scrubbed = _.omit(ent, ['creDate', 'modDate', 'lastCorrelator']);
  scrubbed.attrs = _.mapValues(scrubbed.attrs, (attr) => _.omit(attr, ['creDate', 'modDate']));
  return scrubbed;
};

describe('create registration', () => {

  const fiwareApis = {
    orionApi,
    // ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      before(async () => {
        await mongoApi.assertRegistrationsEmpty();
        await mongoApi.assertEntitiesEmpty();
        // wipe this guy clean
        await notificationsApi.getCommands();
      });

      after(async () => {
        await mongoApi.wipeRegistrations();
        await mongoApi.wipeEntities();
      });

      it('create an entity and verify', async () => {
        await fiwareApi.createEntity({entity: simpleEntity});
        const [mongoEnt] = await mongoApi.findEntities();
        assert.deepStrictEqual(scrubEntity(mongoEnt), expectedMongoEnt);
      });

      it('create a registration and verify', async () => {
        await fiwareApi.createRegistration({payload: simpleRegistration});
        const [mongoReg] = await mongoApi.findRegistrations();
        assert.deepStrictEqual(scrubRegistration(mongoReg), expectedMongoReg);
      });

      it('upsertEntityAttrs: add "doo" attr, DOES NOT TRIGGER REGISTRATION', async () => {
        await fiwareApi.upsertEntityAttrs({
          entityId: simpleEntity.id,
          attrs: {doo: 'wop'},
        });
        const [mongoEnt] = await mongoApi.findEntities();
        const expectedEnt = {
          ...expectedMongoEnt,
          attrNames: ['doo'],
          attrs: {
            doo: {value: 'wop', type: 'Text', mdNames: []},
          },
        };
        assert.deepStrictEqual(scrubEntity(mongoEnt), expectedEnt);
        const updates = await notificationsApi.waitForNextCommand(500);
        assert.strictEqual(updates.length, 0); // should this not trigger the 'doo' registration????
      });

      it('patchEntityAttrs', async () => {
        await fiwareApi.patchEntityAttrs({
          entityId: simpleEntity.id,
          attrs: {doo: 'poo'},
        });
        const [mongoEnt] = await mongoApi.findEntities();
        const expectedEnt = {
          ...expectedMongoEnt,
          attrNames: ['doo'],
          attrs: {
            doo: {value: 'poo', type: 'Text', mdNames: []},
          },
        };
        assert.deepStrictEqual(scrubEntity(mongoEnt), expectedEnt);
        const updates = await notificationsApi.waitForNextCommand(500);
        assert.strictEqual(updates.length, 0); // should this not trigger the 'doo' registration????
      });

      it('remove the "doo" attr and then repatch', async () => {
        // delete (stored "doo" attr)
        await fiwareApi.deleteEntityAttr({
          entityId: simpleEntity.id,
          attrName: 'doo',
        });
        const [mongoEntOrig] = await mongoApi.findEntities();
        assert.deepStrictEqual(scrubEntity(mongoEntOrig), expectedMongoEnt);

        // try to set a completely unknown attr
        const err0 = await assertRejects(fiwareApi.patchEntityAttrs({
          entityId: simpleEntity.id,
          attrs: {unknownAttr: 'should fail'},
        }));
        assertErrorsMatch(err0, ORION_ERRORS.FIND_ERROR_ATTR_MISSING); // DOES NOT FIND ATTR

        // and then set "doo" again, which does not fail..
        await fiwareApi.patchEntityAttrs({
          entityId: simpleEntity.id,
          attrs: {doo: 'redoo'},
        });
        // check how the entity looks
        const {data: finalEnt} = await fiwareApi.findEntity({entityId: simpleEntity.id});
        assert.deepStrictEqual(finalEnt, simpleEntity);

        // check the updates, ONE IS FOUND!!
        const updates = await notificationsApi.waitForNextCommand(500);
        assert.strictEqual(updates.length, 1); // should this not trigger the 'doo' registration????
        const expectedUpdate = {
          contextElements: [{
            id: 'lamni',
            type: 'merp',
            isPattern: 'false',
            attributes: [{name: 'doo', type: 'Text', value: ''}], // value is "", NOT 'redoo' (??)
          }],
          updateAction: 'UPDATE',
        };
        assert.deepStrictEqual(updates[0], expectedUpdate);
      });

      it('updateEntityAttrValue: what happens when pre-existing "doo", and when not', async () => {
        // make sure that the ent has no attrs
        const {data: entOrig} = await fiwareApi.findEntity({entityId: simpleEntity.id});
        assert.deepStrictEqual(entOrig, simpleEntity);

        const err = await assertRejects(fiwareApi.updateEntityAttrValue({
          entityId: simpleEntity.id,
          attrName: 'doo',
          attrValue: {a: 2},
        }));
        assertErrorsMatch(err, ORION_ERRORS.FIND_ERROR_ENTITY_MISSING);
        const updates = await notificationsApi.waitForNextCommand(500);
        assert.strictEqual(updates.length, 1);
        const attrs = _.get(updates, '0.contextElements.0.attributes');
        assert.deepStrictEqual(attrs, [{name: 'doo', type: '', value: ''}]); // WHY IS value === '' ??

        // replace the attrs & check for entity mutation (!!)
        await fiwareApi.replaceEntityAttrs({
          entityId: simpleEntity.id,
          attrs: {doo: 45},
        });
        const {data: updatedEnt} = await fiwareApi.findEntity({entityId: simpleEntity.id});
        const expectedUpdated = {
          ...simpleEntity,
          doo: {
            value: 45,
            metadata: {},
            type: 'Number',
          },
        };
        assert.deepStrictEqual(updatedEnt, expectedUpdated); // registration NOT triggered

        // and do a re-updateEntityAttrValue, see what shakes loose
        await fiwareApi.updateEntityAttrValue({
          entityId: simpleEntity.id,
          attrName: 'doo',
          attrValue: {a: 2},
        });
        const updatesF = await notificationsApi.waitForNextCommand(400);
        assert.strictEqual(updatesF.length, 0); // none triggered, cause attr exists

        // see what the entity looks like
        const {data: entF} = await fiwareApi.findEntity({entityId: simpleEntity.id});
        const expectedF = {
          ...simpleEntity,
          doo: {
            value: {a: 2},
            metadata: {},
            type: 'Number',
          },
        };
        assert.deepStrictEqual(entF, expectedF); // entity WAS mutated instead
      });

      it('what happens when triggering registration for non-existent entity?', async () => {
        // remove the entity
        await fiwareApi.deleteEntity({entityId: simpleEntity.id});
        // check that none are left
        const {data: ents} = await fiwareApi.getEntities();
        assert.strictEqual(ents.length, 0);
        // check that the registration is still around
        const {data: regs} = await fiwareApi.getRegistrations();
        assert.strictEqual(regs.length, 1);
        // and try to trigger the reg
        const err = await assertRejects(fiwareApi.patchEntityAttrs({
          entityId: simpleEntity.id,
          attrs: {doo: 'foo'},
        }));
        assertErrorsMatch(err, ORION_ERRORS.FIND_ERROR_ENTITY_MISSING); // fails as expected/desired
      });

    });

  });

});
