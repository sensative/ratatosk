/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const assert = require('assert');
const _ = require('lodash');

// const {assertRejects} = require('@ratatosk/ngsi-utils');

const {
  orionApi,
  // ngsiApi,
} = require('../../client-api-util');
const mongoApi = require('../../mongo-api');

const validRegistration = {
  description: 'shoobidoobee',
  dataProvided: {
    entities: [
      {
        id: 'boo',
        type: 'per',
      },
    ],
    attrs: [
      'relativeHumidity',
    ],
  },
  provider: {
    legacyForwarding: true,
    supportedForwardingMode: 'all',
    http: {
      url: 'http://localhost:1234',
    },
  },
};

const expectedReg = {
  servicePath: '/',
  format: 'JSON',
  description: 'shoobidoobee',
  contextRegistration: [
    {
      attrs: [
        {
          name: 'relativeHumidity',
          type: '',
        },
      ],
      entities: [
        {
          id: 'boo',
          type: 'per',
        },
      ],
      providingApplication: 'http://localhost:1234',
    },
  ],
};


// conventience function
const scrubRegistration = (reg) => {
  return _.omit(reg, ['_id', 'expiration']);
};

describe('create registration', () => {

  const fiwareApis = {
    orionApi,
    // ngsiApi,
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      before(async () => {
        // await mongoApi.assertRegistrationsEmpty();
        await mongoApi.wipeRegistrations();
      });

      afterEach(async () => {
        await mongoApi.wipeRegistrations();
      });

      it('create a raw, minimal, registration', async () => {
        const raw = {
          url: '/registrations',
          method: 'post',
          data: validRegistration,
        };
        await fiwareApi.rawRequest(raw);
        const registrations = await mongoApi.findRegistrations();
        assert.strictEqual(registrations.length, 1);
        const [reg] = registrations;
        assert.deepStrictEqual(scrubRegistration(reg), expectedReg);
      });

      it('also works using client requestArg tools', async () => {
        // create
        await fiwareApi.createRegistration({payload: validRegistration});
        // and check
        const registrations = await mongoApi.findRegistrations();
        assert.strictEqual(registrations.length, 1);
        const [reg] = registrations;
        assert.deepStrictEqual(scrubRegistration(reg), expectedReg);
      });

    });

  });

});
