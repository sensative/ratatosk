/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const assert = require('assert');
const _ = require('lodash');

const {
  // assertRejects,
  ngsiDateTimeUtils: {hasValidDateTimeFormat},
} = require('@ratatosk/ngsi-utils');

const {
  ngsiApi,
} = require('../../client-api-util');
const mongoApi = require('../../mongo-api');
const notificationsApi = require('../../notification-buffer-api');

const createTimestamps = (creDate, modDate) => {
  const creStamp = {type: 'DateTime', value: creDate};
  const modStamp = {type: 'DateTime', value: modDate || creDate};
  return {dateCreated: creStamp, dateModified: modStamp};
};

describe('check-global-updates', () => {

  const fiwareApis = {
    // orionApi,
    ngsiApi, // ngsi only
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      before(async () => {
        await mongoApi.assertEntitiesEmpty();
        await notificationsApi.getGlobalUpdates();
      });

      after(async () => {
        await mongoApi.wipeEntities();
      });

      it('create a simple entity', async () => {
        const entity = {type: 'Blooey', id: 'blerks'};
        await fiwareApi.createEntity({entity});
        // check for updates
        const updates = await notificationsApi.waitForNextGlobalUpdate();
        assert.strictEqual(updates.length, 1);
        assert.strictEqual(updates[0].originalEntity, null);
        const date = updates[0].modifiedEntity.builtinAttrs.dateCreated.value;
        assert(hasValidDateTimeFormat(date));
        const fiwareId = _.get(updates, '0.modifiedEntity.builtinAttrs.fiwareId.value');
        const expectedModified = {
          id: 'blerks',
          type: 'Blooey',
          servicePath: '/',
          attrs: {},
          builtinAttrs: {
            ...createTimestamps(date),
            fiwareId: {type: 'Text', value: fiwareId},
          },
        };
        assert.deepStrictEqual(updates[0].modifiedEntity, expectedModified);
      });

      it('api replaceEntityAttrs should update the entity, and it should look correct', async () => {
        const reqArg = {entityId: 'blerks', attrs: {spud: 2}, params: {type: 'Blooey'}};
        await fiwareApi.replaceEntityAttrs(reqArg);
        // check for updates
        const updates = await notificationsApi.waitForNextGlobalUpdate();
        assert.strictEqual(updates.length, 1);
        const creDate = updates[0].modifiedEntity.builtinAttrs.dateCreated.value;
        assert(hasValidDateTimeFormat(creDate));
        const modDate = updates[0].modifiedEntity.builtinAttrs.dateModified.value;
        assert(hasValidDateTimeFormat(modDate));
        const [mongoEnt] = await mongoApi.findEntities();
        // check original
        const expectedOriginal = {
          id: 'blerks',
          type: 'Blooey',
          servicePath: '/',
          attrs: {},
          builtinAttrs: {
            ...createTimestamps(creDate),
            fiwareId: {
              type: 'Text',
              value: mongoEnt._id.toString(),
            },
          },
        };
        assert.deepStrictEqual(updates[0].originalEntity, expectedOriginal);
        // check modified
        const expectedModified = {
          id: 'blerks',
          type: 'Blooey',
          servicePath: '/',
          attrs: {
            spud: {
              value: 2,
              type: 'Number',
              metadata: {},
              builtinMetadata: createTimestamps(modDate),
            },
          },
          builtinAttrs: {
            ...createTimestamps(creDate, modDate),
            fiwareId: {
              type: 'Text',
              value: mongoEnt._id.toString(),
            },
          },
        };
        assert.deepStrictEqual(updates[0].modifiedEntity, expectedModified);
      });

    });

  });

});
