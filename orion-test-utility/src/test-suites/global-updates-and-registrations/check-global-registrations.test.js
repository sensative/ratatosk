/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const assert = require('assert');
const _ = require('lodash');

const {
  assertRejects,
  ngsiDateTimeUtils: {hasValidDateTimeFormat},
  ngsiResponseErrorUtils: {assertErrorsMatch},
} = require('@ratatosk/ngsi-utils');

const {
  ORION_ERRORS,
} = require('@ratatosk/ngsi-constants');

const {
  ngsiApi,
} = require('../../client-api-util');
const mongoApi = require('../../mongo-api');
const notificationsApi = require('../../notification-buffer-api');

const createTimestamps = (creDate, modDate) => {
  const creStamp = {type: 'DateTime', value: creDate};
  const modStamp = {type: 'DateTime', value: modDate || creDate};
  return {dateCreated: creStamp, dateModified: modStamp};
};

describe('check-global-registrations', () => {

  const fiwareApis = {
    // orionApi,
    ngsiApi, // ngsi only
  };
  _.each(fiwareApis, (fiwareApi, apiName) => {

    describe(`USING ${apiName}`, () => {

      before(async () => {
        await mongoApi.assertEntitiesEmpty();
        await notificationsApi.getGlobalUpdates();
        await notificationsApi.getGlobalRegistrations();
      });

      after(async () => {
        await mongoApi.wipeEntities();
      });

      it('create a simple entity', async () => {
        const entity = {type: 'Blooey', id: 'blerks'};
        await fiwareApi.createEntity({entity});
      });

      it('create a simple entity with "yggiocmd" fails', async () => {
        const entity = {type: 'fil', id: 'more', yggiocmd: 'oops'};
        const err = await assertRejects(fiwareApi.createEntity({entity}));
        assertErrorsMatch(err, ORION_ERRORS.UNPROCESSABLE_ATTRIBUTE_COLLIDES_WITH_REGISTRATION);
      });

      it('can update attr value', async () => {
        const reqArg = {
          entityId: 'blerks',
          attrName: 'yggiocmd',
          attrValue: 23,
        };
        await fiwareApi.updateEntityAttrValue(reqArg);
        const regs = await notificationsApi.waitForNextGlobalRegistration();
        assert.strictEqual(regs.length, 1);
        const [mongoEnt] = await mongoApi.findEntities();
        const creDate = regs[0].entity.builtinAttrs.dateCreated.value;
        assert(hasValidDateTimeFormat(creDate));
        const expectedData = {
          metadata: {},
          type: 'Number',
          value: 23,
        };
        assert.deepStrictEqual(regs[0].data, expectedData);
        const expectedEntity = {
          id: 'blerks',
          type: 'Blooey',
          servicePath: '/',
          attrs: {},
          builtinAttrs: {
            ...createTimestamps(creDate),
            fiwareId: {
              type: 'Text',
              value: mongoEnt._id.toString(),
            },
          },
        };
        assert.deepStrictEqual(regs[0].entity, expectedEntity);
      });

      it('we can upsert new attr and send command', async () => {
        const reqArg = {
          entityId: 'blerks',
          attrs: {yggiocmd: 'diggity', derp: 'two'},
        };
        await fiwareApi.upsertEntityAttrs(reqArg);
        const regs = await notificationsApi.waitForNextGlobalRegistration();
        const [mongoEnt] = await mongoApi.findEntities();
        assert.strictEqual(regs.length, 1);
        const creDate = regs[0].entity.builtinAttrs.dateCreated.value;
        assert(hasValidDateTimeFormat(creDate));
        const modDate = regs[0].entity.builtinAttrs.dateModified.value;
        assert(hasValidDateTimeFormat(modDate));
        assert(creDate !== modDate);
        const expectedData = {
          metadata: {},
          type: 'Text',
          value: 'diggity',
        };
        assert.deepStrictEqual(regs[0].data, expectedData);
        const expectedEntity = {
          id: 'blerks',
          type: 'Blooey',
          servicePath: '/',
          attrs: {
            derp: {
              type: 'Text',
              value: 'two',
              metadata: {},
              builtinMetadata: createTimestamps(modDate),
            },
          },
          builtinAttrs: {
            ...createTimestamps(creDate, modDate),
            fiwareId: {
              type: 'Text',
              value: mongoEnt._id.toString(),
            },
          },
        };
        assert.deepStrictEqual(regs[0].entity, expectedEntity);
      });

    });

  });

});
