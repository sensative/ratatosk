/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const assert = require('assert');
const {MongoClient} = require('mongodb');

const {
  mongodb: {url: mongodbUrl, db: dbName, options: dbOptions},
} = require('./config');

const {
  ORION_ENTITITIES_DB_COLLECTION,
  ORION_SUBSCRIPTIONS_DB_COLLECTION,
  ORION_REGISTRATIONS_DB_COLLECTION,
} = require('@ratatosk/ngsi-constants');


// Mutable!!!
let client = null;

const start = async () => {
  return new Promise((resolve, reject) => {
    // Use connect method to connect to the server
    MongoClient.connect(mongodbUrl, dbOptions, (err, cli) => {
      if (err) {
        reject(err);
      } else if (!cli) {
        reject(new Error('mongo client is missing'));
      } else {
        client = cli;
        resolve();
      }
    });
  });
};

const stop = async () => {
  if (!client) {
    throw new Error('mongodb client is null');
  }
  await client.close();
  client = null;
};

const getDb = () => {
  if (!client) {
    throw new Error('mongodb client is null');
  }
  return client.db(dbName);
};

//
// mongo-api Entities
//

const getEntitiesCollection = () => {
  const db = getDb();
  const entities = db.collection(ORION_ENTITITIES_DB_COLLECTION);
  if (!entities) {
    throw new Error('entities collection is missing');
  }
  return entities;
};

const wipeEntities = async () => {
  const collection = getEntitiesCollection();
  return collection.deleteMany()
    .then((res) => res.deletedCount);
};

const findEntities = async () => {
  const collection = getEntitiesCollection();
  const docs = await collection.find().toArray();
  return docs;
};

const countEntities = async () => {
  const docs = await findEntities();
  return docs.length;
};

const assertEntitiesEmpty = async () => {
  const numEntities = await countEntities();
  assert.strictEqual(numEntities, 0, 'there should be 0 entities');
};

//
// mongo-api Subscriptions
//

const getSubscriptionsCollection = () => {
  const db = getDb();
  const subscriptions = db.collection(ORION_SUBSCRIPTIONS_DB_COLLECTION);
  if (!subscriptions) {
    throw new Error('subscriptions collection is missing');
  }
  return subscriptions;
};

const wipeSubscriptions = async () => {
  const collection = getSubscriptionsCollection();
  return collection.deleteMany()
    .then((res) => res.deletedCount);
};

const findSubscriptions = async () => {
  const collection = getSubscriptionsCollection();
  const docs = await collection.find().toArray();
  return docs;
};

const countSubscriptions = async () => {
  const docs = await findSubscriptions();
  return docs.length;
};

const assertSubscriptionsEmpty = async () => {
  const numSubscriptions = await countSubscriptions();
  assert.strictEqual(numSubscriptions, 0, 'there should be 0 subscriptions');
};

//
// mongo-api Registrations
//

const getRegistrationsCollection = () => {
  const db = getDb();
  const registrations = db.collection(ORION_REGISTRATIONS_DB_COLLECTION);
  if (!registrations) {
    throw new Error('registrations collection is missing');
  }
  return registrations;
};

const wipeRegistrations = async () => {
  const collection = getRegistrationsCollection();
  return collection.deleteMany()
    .then((res) => res.deletedCount);
};

const findRegistrations = async () => {
  const collection = getRegistrationsCollection();
  const docs = await collection.find().toArray();
  return docs;
};

const countRegistrations = async () => {
  const docs = await findRegistrations();
  return docs.length;
};

const assertRegistrationsEmpty = async () => {
  const numRegistrations = await countRegistrations();
  assert.strictEqual(numRegistrations, 0, 'there should be 0 registrations');
};

module.exports = {
  start,
  stop,
  wipeEntities,
  findEntities,
  countEntities,
  assertEntitiesEmpty,
  wipeSubscriptions,
  findSubscriptions,
  countSubscriptions,
  assertSubscriptionsEmpty,
  wipeRegistrations,
  findRegistrations,
  countRegistrations,
  assertRegistrationsEmpty,
};
