/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const axios = require('axios');
const delay = require('delay');

const {
  notificationBuffer: {port},
} = require('./config');


const {request} = axios.create({
  baseURL: `http://localhost:${port}/`,
  timeout: 300,
});

// commands

const getCommands = async () => {
  const {data: updates} = await request({
    url: '/commands',
    method: 'get',
  });
  return updates;
};

const waitForNextCommand = async (maxDelay) => {
  const maximum = maxDelay || 1050; // ms
  const start = Date.now();
  let updates = await getCommands();

  while (!updates.length && Date.now() - start < maximum) {
    await delay(100);
    updates = await getCommands();
  }
  const finish = Date.now();
  console.info(`waitForNextCommand: update took ${finish - start} ms`);
  return updates;
};

//
// notifications
//

const getNotifications = async () => {
  const {data: notifications} = await request({
    url: '/notifications',
    method: 'get',
  });
  return notifications;
};

const waitForNextNotification = async (maxDelay) => {
  const maximum = maxDelay || 1050; // ms
  const start = Date.now();
  let notifications = await getNotifications();

  while (!notifications.length && Date.now() - start < maximum) {
    await delay(100);
    notifications = await getNotifications();
  }
  const finish = Date.now();
  console.info(`waitForNextNotification: notification took ${finish - start} ms`);
  return notifications;
};

//
// global updates
//

const getGlobalUpdates = async () => {
  const {data: updates} = await request({
    url: '/globalUpdates',
    method: 'get',
  });
  return updates;
};

const waitForNextGlobalUpdate = async (maxDelay) => {
  const maximum = maxDelay || 1050; // ms
  const start = Date.now();
  let updates = await getGlobalUpdates();

  while (!updates.length && Date.now() - start < maximum) {
    await delay(100);
    updates = await getGlobalUpdates();
  }
  const finish = Date.now();
  console.info(`waitForNextGlobalUpdate: global update took ${finish - start} ms`);
  return updates;
};

//
// global registrations
//

const getGlobalRegistrations = async () => {
  const {data: registrations} = await request({
    url: '/globalRegistrations',
    method: 'get',
  });
  return registrations;
};

const waitForNextGlobalRegistration = async (maxDelay) => {
  const maximum = maxDelay || 1050; // ms
  const start = Date.now();
  let registrations = await getGlobalRegistrations();

  while (!registrations.length && Date.now() - start < maximum) {
    await delay(100);
    registrations = await getGlobalRegistrations();
  }
  const finish = Date.now();
  console.info(`waitForNextGlobalRegistration: global registration took ${finish - start} ms`);
  return registrations;
};

//
// global regs
//

const resetAll = async () => {
  const {total} = await request({
    url: '/reset',
    method: 'post',
  });
  return total;
};

module.exports = {
  getCommands,
  waitForNextCommand,
  getNotifications,
  waitForNextNotification,
  getGlobalUpdates,
  waitForNextGlobalUpdate,
  getGlobalRegistrations,
  waitForNextGlobalRegistration,
  resetAll,
};
