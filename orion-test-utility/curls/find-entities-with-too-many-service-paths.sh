## see the tests findEntities/
## there's some wierd stuff going on here..
## findEntities with too many (>10) service-paths returns a 200 success
## for which the data object is an error object with {code:400} ...

curl localhost:1026/v2/entities -s -S -H 'Fiware-ServicePath: /a,/b,/c,/d,/a/a,/a/b,/a/c,/a/d,/a/a/a,/a/a/b,/a/a/d' -H 'Accept: application/json'
