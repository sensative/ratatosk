Orion Test utility

Orion test utility is an integrated test of how Orion server works and how ngis-server differs, and is intended to be comprehensive, and runs with independent docker-containers for the different components, located in `docker/docker-compose.yml`

To run the tests located in `src/test-suites`, in a dedicated console, stand in `docker/` and run `$ docker-compose up` (assuming you have docker-compose up and running)
