'use strict';

const _ = require('lodash');
const fs = require('fs');
const path = require('path');


const run = async () => {

  const rootEntries = fs.readdirSync(__dirname);
  const packages = {};
  _.each(rootEntries, entry => {

    let hasPackageFile;
    try {
      const packageStat = fs.statSync(path.join(__dirname, entry, 'package.json'));
      hasPackageFile = packageStat.isFile();
    } catch (err) {
      hasPackageFile = false;
    }

    if (hasPackageFile) {
      const packageJson = require(path.join(__dirname, entry, 'package.json'));
      const {name, version} = packageJson;
      packages[name] = {
        entry,
        name,
        version,
        packageJson,
        nextPackageJson: _.cloneDeep(packageJson),
      };
    }

    // mutate
    _.each(packages, ngsiPackage => {
      const {name, version} = ngsiPackage;
      _.each(packages, targetPackage => {
        if (_.get(targetPackage, ['packageJson', 'dependencies', name])) {
          _.set(targetPackage, ['nextPackageJson', 'dependencies', name], `workspace:^${version}`);
        }
        if (_.get(targetPackage, ['packageJson', 'devDependencies', name])) {
          _.set(targetPackage, ['nextPackageJson', 'devDependencies', name], `workspace:^${version}`);
        }
      });
    });

    // save if necessary
    _.each(packages, ngsiPackage => {
      const {name, entry} = ngsiPackage;
      if (!_.isEqual(ngsiPackage.packageJson, ngsiPackage.nextPackageJson)) {
        console.log(`package ${name} package.json was changed`);
        fs.writeFileSync(
          path.join(__dirname, entry, 'package.json'),
          JSON.stringify(ngsiPackage.nextPackageJson, null, 2),
        );
      }
    });

  });

};


run().catch((err) => {
  console.error('uncaught: ', err.message);
});
