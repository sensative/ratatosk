# ngsi-mongo-storage

A list of changes as compared to Orion database schema

// NOW
  - DateTime gets stored as a proper date, NOT seconds (i.e. Integer) since 1970

// LATER
  - attrNames and attr.mdNames REMOVED
  - \_id.id -> nameId (and maps to entity.id)
  - \_id.type -> type (and maps to entity.type)
  - \_id.servicePath -> servicePath (exposed as a builtin "servicePath")
  - \_id is just saved as an ObjectId() (exposed as a builtin "fiwareId")
  - uniqueness index added to {nameId, type, servicePath} trio to enforce Orion compatibility
  - builtins expanded to include {servicePath, fiwareId} (from above)
