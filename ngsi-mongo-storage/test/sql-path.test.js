/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const serialPromise = require('promise-serial-exec');

const {
  ngsiSql: {queryParser: {parseQueryString}},
  ngsiEntityFormatter: {formatEntity},
} = require('@ratatosk/ngsi-utils');

const {connectionManager: {stop, restart, mongoRaw}} = require('../src');
const {getMongoConfig} = require('./test-config');

describe('sql-path tests', () => {

  let api;
  before(async () => {
    api = await restart(getMongoConfig());
  });
  after(async () => {
    await mongoRaw.wipeEntities();
    await stop();
  });

  const entities = [
    formatEntity({
      id: 'agda',
      type: 'beeline',
      cee: {
        value: {'': {a: 2}},
      },
      dee: {
        value: {'': {'': {b: 3, '': 'empty'}}},
      },
      fee: {
        value: {},
        metadata: {
          az: {z: 'asdf', '': {zz: 23}},
        },
      },
    }),
    formatEntity({
      id: 'casey',
      type: 'doorknob',
      cee: {
        value: {doorb: {a: 2}},
      },
      dee: {
        value: {'': {'': {b: 3, '': 'empty'}}},
        metadata: {},
      },
      fee: {
        value: {},
        metadata: {
          az: {z: 'asdf', '': {zz: 23, '': 4}},
        },
      },
    }),
    formatEntity({
      id: 'gordy',
      type: 'poboy',
      cee: 3,
      dee: 4,
      fee: 5,
    }),
  ];

  describe('simple check of projections functionality', () => {

    before(async () => {
      await serialPromise(_.map(entities, (entity) => async () => {
        await api.createEntity({entity});
      }));
    });

    it('default', async () => {
      const {entities: ents} = await api.getEntities({
        params: {representationType: 'keyValues'},
      });
      assert.strictEqual(ents.length, 3);
    });

    it('some q existense queries', async () => {
      const qVariants = [
        {in: 'cee.', num: 1},
        {in: 'cee..', num: 0},
        {in: 'dee..', num: 2},
      ];
      await serialPromise(_.map(qVariants, (query) => async () => {
        const parsedQuery = parseQueryString(query.in);
        const {entities: ents} = await api.getEntities({
          params: {representationType: 'keyValues', q: parsedQuery},
        });
        assert.strictEqual(ents.length, query.num);
      }));
    });

    it('some mq existense queries', async () => {
      const qVariants = [
        {in: 'fee.az', num: 2},
        {in: 'fee.az.', num: 2},
        {in: 'fee.az..', num: 1},
        {in: 'fee.az...', num: 0},
      ];
      await serialPromise(_.map(qVariants, (query) => async () => {
        const parsedQuery = parseQueryString(query.in);
        const {entities: ents} = await api.getEntities({
          params: {representationType: 'keyValues', mq: parsedQuery},
        });
        assert.strictEqual(ents.length, query.num);
      }));
    });

  });

});
