/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const path = require('path');
const {MongoMemoryServer} = require('mongodb-memory-server');
const {lint} = require('@ratatosk/ngsi-linter');

const {setMongoUrl} = require('./test-config');

let mongoServer;
before(() => {
  mongoServer = new MongoMemoryServer();
  return mongoServer.getConnectionString()
    .then((mongoUrl) => setMongoUrl(mongoUrl));
});
after(async () => {
  await mongoServer.stop();
});

describe('testing components in ngsi-mongo-storage', () => {

  it('inside the main describe', () => 0);

  it('linting', async () => {
    await lint(path.join(__dirname, '..'));
  });

  // // // core construction
  require('./proxied-start-stop.test');
  require('./real-start-stop.test');

  // // // entities routes
  require('./crud-flowthrough.test');
  require('./service-path-wildcard.test');
  require('./mongo-api-filter-params.test');
  require('./representation-types.test');
  require('./attrs-projection.test');
  require('./metadata-projection.test');
  require('./peek-representation.test');
  require('./date-handling.test');
  require('./sql-path.test');
  require('./order-by.test.js');
  require('./update-entity-attr-value.test');
  require('./location-geo.test');
  require('./pagination-limit-sort.test');
  require('./orion-import-export.test');

  // // // bulk stuff
  require('./bulk-query.test');
  require('./bulk-update-crud-flowthrough.test');
  //
  // // // subscriptions
  require('./subscription-crud-flowthrough.test');
  require('./generate-notifications.test');
  //
  // // // global registrations
  require('./global-registration-attrs-basics.test');
  require('./global-registration-attrs-triggered.test');
  //
  // // // access-token stuff
  // require('./access-token-pipeline.test');
  //
  // // // api-generator
  require('./api-generator.test');
  // //
  // // // indexing
  require('./entities-custom-indexing.test');
  require('./create-or-update.test');

});
