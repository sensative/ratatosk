/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');

const {assertRejects} = require('@ratatosk/ngsi-utils');

const {
  connectionManager: {start, stop, restart, mongoRaw},
} = require('../src');

const {getMongoConfig} = require('./test-config');

const connClosedMessage = 'Connection has been closed';


describe('real-start-stop tests', () => {

  after(async () => {
    await restart(getMongoConfig());
    await mongoRaw.wipeEntities();
    await stop();
  });

  it('can start and stop', async () => {
    const entity = {
      id: 'booker',
      type: 'snooker',
    };
    const api = await start(getMongoConfig());
    const res = await api.createEntity({entity});
    assert.strictEqual(_.get(res, 'modifiedEntity.id'), 'booker');
    await stop();
    const apiN = await start(getMongoConfig());
    const {entities: entsN} = await apiN.getEntities();
    assert.strictEqual(entsN.length, 1, 'should be the one');
    assert.strictEqual(_.get(entsN, '0.id'), 'booker');
    await stop();
    const apiNN = await start(getMongoConfig());
    await apiNN.deleteEntity({entityId: 'booker'});
    await stop();
    const apiNNN = await start(getMongoConfig());
    const {entities: entsNNN} = await apiNNN.getEntities();
    assert.strictEqual(entsNNN.length, 0, 'should be gone');
    await stop();
    assertRejects(api.createEntity({entity}), connClosedMessage);
  });

  it('can start, start, restart, and stop', async () => {
    const api0 = await start(getMongoConfig());
    const restarter = restart(getMongoConfig());
    // NOTE that next reject happens SYNC (is before the "await");
    assertRejects(api0.getEntities(), connClosedMessage);
    const api1 = await restarter;
    const stopper = stop();
    // and the same here
    assertRejects(api1.getEntities(), connClosedMessage);
    await stopper;
  });

});
