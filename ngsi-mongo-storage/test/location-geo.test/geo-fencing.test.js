/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const geolib = require('geolib');
const serialPromise = require('promise-serial-exec');

const {
  assertRejects,
  ngsiLocationUtils: {parseNgsiGeometry},
  ngsiEntityFormatter: {formatEntity},
  ngsiSql: {queryParser: {parseQueryString}},
} = require('@ratatosk/ngsi-utils');

const {connectionManager: {stop, restart, mongoRaw}} = require('../../src');
const {getMongoConfig} = require('../test-config');

const genSerializedPoint = (pt) => {
  return `${pt.latitude},${pt.longitude}`;
};

const genSerializedPoly = (pt, dist) => {
  const mid = geolib.computeDestinationPoint(pt, dist, 0);
  const last = geolib.computeDestinationPoint(mid, dist, 145);
  const points = [pt, mid, last, pt];
  return _.map(points, genSerializedPoint);
};

const genSerializedLine = (pt, dist) => {
  const mid = geolib.computeDestinationPoint(pt, dist, 0);
  const last = geolib.computeDestinationPoint(mid, dist, 145);
  const points = [pt, mid, last];
  return _.map(points, genSerializedPoint);
};


describe('geo-fencing', () => {

  let api;
  before(async () => {
    api = await restart(getMongoConfig());
    await mongoRaw.assertEntitiesEmpty();
  });
  after(async () => {
    await mongoRaw.wipeEntities();
    await stop();
  });

  describe('geo:point georels', () => {

    const pt0 = {longitude: 23, latitude: 24};
    before(async () => {
      const pt1 = geolib.computeDestinationPoint(pt0, 998, 35);
      const pt2 = geolib.computeDestinationPoint(pt0, 1001, 125);
      const ent0 = formatEntity({
        id: 'berp',
        type: 'al',
        hop: {type: 'geo:point', value: `${pt0.latitude},${pt0.longitude}`},
      });
      const ent1 = formatEntity({
        id: 'nerp',
        type: 'al',
        hop: {type: 'geo:point', value: `${pt1.latitude},${pt1.longitude}`},
      });
      const ent2 = formatEntity({
        id: 'herp',
        type: 'al',
        hop: {type: 'geo:point', value: `${pt2.latitude},${pt2.longitude}`},
      });
      await api.createEntity({entity: ent0});
      await api.createEntity({entity: ent1});
      await api.createEntity({entity: ent2});
    });
    after(async () => mongoRaw.wipeEntities());

    it('find entities < 1000m from pt0', async () => {
      const {entities} = await api.getEntities({
        params: {
          georel: 'near;maxDistance:1000',
          geometry: 'point',
          coords: `${pt0.latitude},${pt0.longitude}`,
        },
      });
      assert.strictEqual(entities.length, 2);
      const names = _.map(_.sortBy(entities, (ent) => ent.id), (ent) => ent.id);
      assert.deepStrictEqual(names, ['berp', 'nerp']);
    });

    it('find entities >1000 m from pt0', async () => {
      const {entities} = await api.getEntities({
        params: {
          georel: 'near;minDistance:1000',
          geometry: 'point',
          coords: `${pt0.latitude},${pt0.longitude}`,
        },
      });
      assert.strictEqual(entities.length, 1);
      assert.strictEqual(entities[0].id, 'herp');
    });

    it('find entities COVERED_BY a point', async () => {
      const params = {
        georel: 'coveredBy',
        geometry: 'point',
        coords: `${pt0.latitude},${pt0.longitude}`,
      };
      const err = 'invalid geometry for georel "coveredBy" (must be a polygon)';
      await assertRejects(api.getEntities({params}), err);
    });

    it('find entities COVERED_BY polygon', async () => {
      const polyOrigin = geolib.computeDestinationPoint(pt0, 10, 190);
      const poly = genSerializedPoly(polyOrigin, 30);
      const params = {
        georel: 'coveredBy',
        geometry: 'polygon',
        coords: poly.join(';'),
      };
      const {entities} = await api.getEntities({params});
      assert.strictEqual(entities.length, 1);
      assert.strictEqual(entities[0].id, 'berp');
    });

  });

  describe('geo:polygon georels', () => {

    const pt0 = {longitude: 23, latitude: 24};
    before(async () => {
      const pt1 = geolib.computeDestinationPoint(pt0, 880, 0);
      const pt2 = geolib.computeDestinationPoint(pt0, 950, 0);
      const pt3 = geolib.computeDestinationPoint(pt0, 1010, 0);
      const poly1 = genSerializedPoly(pt1, 100);
      const poly2 = genSerializedPoly(pt2, 300);
      const poly3 = genSerializedPoly(pt3, 100);
      const ent1 = {
        id: 'bppp',
        type: 'al',
        hop: {type: 'geo:polygon', value: poly1},
      };
      const ent2 = {
        id: 'nppp',
        type: 'al',
        hop: {type: 'geo:polygon', value: poly2},
      };
      const ent3 = {
        id: 'hppp',
        type: 'al',
        hop: {type: 'geo:polygon', value: poly3},
      };
      await api.createEntity({entity: ent1});
      await api.createEntity({entity: ent2});
      await api.createEntity({entity: ent3});
    });
    after(async () => mongoRaw.wipeEntities());

    it('find entities < 1000m from pt0', async () => {
      const params = {
        georel: 'near;maxDistance:1000',
        geometry: 'point',
        coords: `${pt0.latitude},${pt0.longitude}`,
      };
      const {entities} = await api.getEntities({params});
      assert.strictEqual(entities.length, 2);
      const names = _.map(_.sortBy(entities, (ent) => ent.id), (ent) => ent.id);
      assert.deepStrictEqual(names, ['bppp', 'nppp']);
    });

    it('find entities >1000 m from pt0', async () => {
      const params = {
        georel: 'near;minDistance:1000',
        geometry: 'point',
        coords: `${pt0.latitude},${pt0.longitude}`,
      };
      const {entities} = await api.getEntities({params});
      assert.strictEqual(entities.length, 1);
      assert.strictEqual(entities[0].id, 'hppp');
    });

  });

  describe('georel "equals"', () => {
    const pt0 = {longitude: 0, latitude: 0};
    const pt1 = {longitude: 5, latitude: 5};

    const point0 = genSerializedPoint(pt0);
    const point1 = genSerializedPoint(pt1);
    const line0 = genSerializedLine(pt0, 10);
    const line1 = genSerializedLine(pt1, 10);
    const poly0 = genSerializedPoly(pt0, 20);
    const poly1 = genSerializedPoly(pt1, 20);
    const geoms = [
      {value: point0, type: 'geo:point'},
      {value: point1, type: 'geo:point'},
      {value: line0, type: 'geo:line'},
      {value: line1, type: 'geo:line'},
      {value: poly0, type: 'geo:polygon'},
      {value: poly1, type: 'geo:polygon'},
    ];
    // also add some that match but are geo:json and NOT simple geo
    geoms.push({ // point
      type: 'geo:json',
      value: parseNgsiGeometry(geoms[0].type, geoms[0].value),
    });
    geoms.push({ // line
      type: 'geo:json',
      value: parseNgsiGeometry(geoms[2].type, geoms[2].value),
    });
    geoms.push({ // polygon
      type: 'geo:json',
      value: parseNgsiGeometry(geoms[4].type, geoms[4].value),
    });
    before(async () => {
      await serialPromise(_.map(geoms, (geom, index) => async () => {
        const entity = formatEntity({
          id: `peeep_${index}`,
          loop: geom,
        });
        await api.createEntity({entity});
      }));
    });
    after(async () => mongoRaw.wipeEntities());

    it('equals pt0', async () => {
      const params = {
        georel: 'equals',
        geometry: 'point',
        coords: point0,
      };
      const {entities} = await api.getEntities({params});
      assert.strictEqual(entities.length, 2);
      const names = _.map(_.sortBy(entities, (ent) => ent.id), (ent) => ent.id);
      assert.deepStrictEqual(names, ['peeep_0', 'peeep_6']);
    });

    it('equals, line0', async () => {
      const params = {
        georel: 'equals',
        geometry: 'line',
        coords: line0.join(';'),
      };
      const {entities} = await api.getEntities({params});
      assert.strictEqual(entities.length, 2);
      const names = _.map(_.sortBy(entities, (ent) => ent.id), (ent) => ent.id);
      assert.deepStrictEqual(names, ['peeep_2', 'peeep_7']);
    });

    it('equals, poly0', async () => {
      const params = {
        georel: 'equals',
        geometry: 'polygon',
        coords: poly0.join(';'),
      };
      const {entities} = await api.getEntities({params});
      assert.strictEqual(entities.length, 2);
      const names = _.map(_.sortBy(entities, (ent) => ent.id), (ent) => ent.id);
      assert.deepStrictEqual(names, ['peeep_4', 'peeep_8']);
    });

  });

  describe('georel intersects', () => {
    const pt0 = {longitude: 0, latitude: 0};
    const pt1 = {longitude: 20, latitude: 20}; // far away

    const point0 = genSerializedPoint(pt0);
    const line0 = genSerializedLine(pt0, 10);
    const poly0 = genSerializedPoly(pt0, 10);
    const point1 = genSerializedPoint(pt1);
    const line1 = genSerializedLine(pt1, 10);
    const poly1 = genSerializedPoly(pt1, 10);
    const geoms = [
      {value: point0, type: 'geo:point'},
      {value: line0, type: 'geo:line'},
      {value: poly0, type: 'geo:polygon'},
      {value: point1, type: 'geo:point'},
      {value: line1, type: 'geo:line'},
      {value: poly1, type: 'geo:polygon'},
    ];

    before(async () => {
      await serialPromise(_.map(geoms, (geom, index) => async () => {
        const entity = formatEntity({
          id: `rrrp_${index}`,
          loop: geom,
        });
        await api.createEntity({entity});
      }));
    });
    after(async () => mongoRaw.wipeEntities());

    it('intersect polygon - intersecting borders', async () => {
      // this poly should intersect line0 and poly0, and contain point0
      const polyOrigin = geolib.computeDestinationPoint(pt0, 8, 190);
      const poly = genSerializedPoly(polyOrigin, 10);
      const params = {
        georel: 'intersects',
        geometry: 'polygon',
        coords: poly.join(';'),
      };
      const {entities} = await api.getEntities({params});
      assert.strictEqual(entities.length, 3);
      const names = _.map(_.sortBy(entities, (ent) => ent.id), (ent) => ent.id);
      assert.deepStrictEqual(names, ['rrrp_0', 'rrrp_1', 'rrrp_2']);
    });

    it('intersecting polygon - should contain all three with no intersecting borders', async () => {
      const polyOrigin = geolib.computeDestinationPoint(pt0, 2, 225);
      const poly = genSerializedPoly(polyOrigin, 14);
      const params = {
        georel: 'intersects',
        geometry: 'polygon',
        coords: poly.join(';'),
      };
      const {entities} = await api.getEntities({params});
      assert.strictEqual(entities.length, 3);
      const names = _.map(_.sortBy(entities, (ent) => ent.id), (ent) => ent.id);
      assert.deepStrictEqual(names, ['rrrp_0', 'rrrp_1', 'rrrp_2']);
    });

    it('intersecting lineString - intersecting borders (same as poly case 1)', async () => {
      // this line should intersect line0 and poly0, and contain point0
      const lineOrigin = geolib.computeDestinationPoint(pt0, 8, 190);
      const line = genSerializedLine(lineOrigin, 10);
      const params = {
        georel: 'intersects',
        geometry: 'line',
        coords: line.join(';'),
      };
      const {entities} = await api.getEntities({params});
      assert.strictEqual(entities.length, 2);
      const names = _.map(_.sortBy(entities, (ent) => ent.id), (ent) => ent.id);
      assert.deepStrictEqual(names, ['rrrp_1', 'rrrp_2']);
    });

    it('(not) intersecting line - intersecting borders (same as poly case 2)', async () => {
      // this line, if closed to a polygon, would contain the first three
      const lineOrigin = geolib.computeDestinationPoint(pt0, 2, 225);
      const line = genSerializedLine(lineOrigin, 14);
      const params = {
        georel: 'intersects',
        geometry: 'line',
        coords: line.join(';'),
      };
      const {entities} = await api.getEntities({params});
      assert.strictEqual(entities.length, 0);
    });

    it('intersecting point - at the origin (i.e. pt0)', async () => {
      const params = {
        georel: 'intersects',
        geometry: 'point',
        coords: genSerializedPoint(pt0),
      };
      const {entities} = await api.getEntities({params});
      assert.strictEqual(entities.length, 3);
      const names = _.map(_.sortBy(entities, (ent) => ent.id), (ent) => ent.id);
      assert.deepStrictEqual(names, ['rrrp_0', 'rrrp_1', 'rrrp_2']);
    });

    it('intersecting point - along x-axis offset slightly from the origin (misses ALL)', async () => {
      const pt = {longitude: 1, latitude: 0};
      const params = {
        georel: 'intersects',
        geometry: 'point',
        coords: genSerializedPoint(pt),
      };
      const {entities} = await api.getEntities({params});
      assert.strictEqual(entities.length, 0);
    });

    it('intersecting point - on the inside of poly1', async () => {
      const pt = {longitude: 5, latitude: 1};
      const params = {
        georel: 'intersects',
        geometry: 'point',
        coords: genSerializedPoint(pt),
      };
      const {entities} = await api.getEntities({params});
      assert.strictEqual(entities.length, 0);
    });

  });

  describe('georel disjoint', () => {
    const pt0 = {longitude: 0, latitude: 0};
    const pt1 = {longitude: 20, latitude: 20}; // far away

    const point0 = genSerializedPoint(pt0);
    const line0 = genSerializedLine(pt0, 10);
    const poly0 = genSerializedPoly(pt0, 10);
    const point1 = genSerializedPoint(pt1);
    const line1 = genSerializedLine(pt1, 10);
    const poly1 = genSerializedPoly(pt1, 10);
    const geoms = [
      {value: point0, type: 'geo:point'},
      {value: line0, type: 'geo:line'},
      {value: poly0, type: 'geo:polygon'},
      {value: point1, type: 'geo:point'},
      {value: line1, type: 'geo:line'},
      {value: poly1, type: 'geo:polygon'},
    ];

    before(async () => {
      await serialPromise(_.map(geoms, (geom, index) => async () => {
        const entity = formatEntity({
          id: `rrrp_${index}`,
          loop: geom,
        });
        await api.createEntity({entity});
      }));
    });
    after(async () => mongoRaw.wipeEntities());

    it('intersect polygon - intersecting borders', async () => {
      // this poly should intersect line0 and poly0, and contain point0
      const polyOrigin = geolib.computeDestinationPoint(pt0, 8, 190);
      const poly = genSerializedPoly(polyOrigin, 10);
      const params = {
        georel: 'disjoint',
        geometry: 'polygon',
        coords: poly.join(';'),
      };
      const {entities} = await api.getEntities({params});
      assert.strictEqual(entities.length, 3);
      const names = _.map(_.sortBy(entities, (ent) => ent.id), (ent) => ent.id);
      assert.deepStrictEqual(names, ['rrrp_3', 'rrrp_4', 'rrrp_5']);
    });

    it('intersecting polygon - should contain all three with no intersecting borders', async () => {
      const polyOrigin = geolib.computeDestinationPoint(pt0, 2, 225);
      const poly = genSerializedPoly(polyOrigin, 14);
      const params = {
        georel: 'disjoint',
        geometry: 'polygon',
        coords: poly.join(';'),
      };
      const {entities} = await api.getEntities({params});
      assert.strictEqual(entities.length, 3);
      const names = _.map(_.sortBy(entities, (ent) => ent.id), (ent) => ent.id);
      assert.deepStrictEqual(names, ['rrrp_3', 'rrrp_4', 'rrrp_5']);
    });

    it('intersecting lineString - intersecting borders (same as poly case 1)', async () => {
      // this line should intersect line0 and poly0, and contain point0
      const lineOrigin = geolib.computeDestinationPoint(pt0, 8, 190);
      const line = genSerializedLine(lineOrigin, 10);
      const params = {
        georel: 'disjoint',
        geometry: 'line',
        coords: line.join(';'),
      };
      const {entities} = await api.getEntities({params});
      assert.strictEqual(entities.length, 4);
      const names = _.map(_.sortBy(entities, (ent) => ent.id), (ent) => ent.id);
      assert.deepStrictEqual(names, ['rrrp_0', 'rrrp_3', 'rrrp_4', 'rrrp_5']);
    });

    it('(not) intersecting line - intersecting borders (same as poly case 2)', async () => {
      // this line, if closed to a polygon, would contain the first three
      const lineOrigin = geolib.computeDestinationPoint(pt0, 2, 225);
      const line = genSerializedLine(lineOrigin, 14);
      const params = {
        georel: 'disjoint',
        geometry: 'line',
        coords: line.join(';'),
      };
      const {entities} = await api.getEntities({params});
      assert.strictEqual(entities.length, 6);
      const names = _.map(_.sortBy(entities, (ent) => ent.id), (ent) => ent.id);
      assert.deepStrictEqual(names, ['rrrp_0', 'rrrp_1', 'rrrp_2', 'rrrp_3', 'rrrp_4', 'rrrp_5']);
    });

    it('intersecting point - at the origin (i.e. pt0)', async () => {
      const params = {
        georel: 'disjoint',
        geometry: 'point',
        coords: genSerializedPoint(pt0),
      };
      const {entities} = await api.getEntities({params});
      assert.strictEqual(entities.length, 3);
      const names = _.map(_.sortBy(entities, (ent) => ent.id), (ent) => ent.id);
      assert.deepStrictEqual(names, ['rrrp_3', 'rrrp_4', 'rrrp_5']);
    });

    it('intersecting point - along x-axis offset slightly from the origin (misses ALL)', async () => {
      const pt = {longitude: 1, latitude: 0};
      const params = {
        georel: 'intersects',
        geometry: 'point',
        coords: genSerializedPoint(pt),
      };
      const {entities} = await api.getEntities({params});
      assert.strictEqual(entities.length, 0);
    });

    it('intersecting point - on the inside of poly1', async () => {
      const pt = {longitude: 5, latitude: 1};
      const params = {
        georel: 'disjoint',
        geometry: 'point',
        coords: genSerializedPoint(pt),
      };
      const {entities} = await api.getEntities({params});
      assert.strictEqual(entities.length, 6);
      const names = _.map(_.sortBy(entities, (ent) => ent.id), (ent) => ent.id);
      assert.deepStrictEqual(names, ['rrrp_0', 'rrrp_1', 'rrrp_2', 'rrrp_3', 'rrrp_4', 'rrrp_5']);
    });

  });

  describe('geo:point, with ordering and sql', () => {

    const pt0 = {longitude: 7, latitude: 5};
    before(async () => {
      await Promise.all(_.times(200, (index) => {
        const pt = geolib.computeDestinationPoint(pt0, 50 * index, 0);
        const entity = formatEntity({
          id: `smerp_${index}`,
          index,
          loc: {type: 'geo:point', value: `${pt.latitude},${pt.longitude}`},
        });
        return api.createEntity({entity});
      }));
    });
    after(async () => mongoRaw.wipeEntities());

    it('find the default at 5k (halfway) with radius 10k (i.e. all of them qualify - but no sql, no count, no sort, no paging)', async () => {
      const pt = geolib.computeDestinationPoint(pt0, 5003, 0);
      const params = {
        georel: 'near;maxDistance:10000',
        geometry: 'point',
        coords: `${pt.latitude},${pt.longitude}`,
      };
      const {entities, count} = await api.getEntities({params});
      assert.strictEqual(count, null);
      assert.strictEqual(entities.length, 20);
      const indices = _.map(entities, (ent) => ent.index.value);
      // note that these guys are AUTOMATICALLY ordered by nearness to pt
      const expected = [100, 101, 99, 102, 98, 103, 97, 104, 96, 105, 95, 106, 94, 107, 93, 108, 92, 109, 91, 110];
      assert.deepStrictEqual(indices, expected);
    });

    it('find the default at 5k (halfway) with radius 10k (i.e. all of them qualify - but no sql, no sort, but limit 10, skip 10)', async () => {
      const pt = geolib.computeDestinationPoint(pt0, 5003, 0);
      const params = {
        georel: 'near;maxDistance:10000',
        geometry: 'point',
        coords: `${pt.latitude},${pt.longitude}`,
        limit: 10,
        offset: 10,
      };
      const {entities, count} = await api.getEntities({params});
      assert.strictEqual(count, null);
      assert.strictEqual(entities.length, 10);
      const indices = _.map(entities, (ent) => ent.index.value);
      // note that these guys are AUTOMATICALLY ordered by nearness to pt
      const expected = [95, 106, 94, 107, 93, 108, 92, 109, 91, 110];
      assert.deepStrictEqual(indices, expected);
    });

    it('find the default at 5k (halfway) with radius 10k (i.e. all of them qualify -  but limit 5, no skip, then skip 3)', async () => {
      const pt = geolib.computeDestinationPoint(pt0, 5003, 0);
      const params = {
        georel: 'near;maxDistance:10000',
        geometry: 'point',
        coords: `${pt.latitude},${pt.longitude}`,
        limit: 5,
      };
      const {entities, count} = await api.getEntities({params});
      assert.strictEqual(count, null);
      assert.strictEqual(entities.length, 5);
      const indices = _.map(entities, (ent) => ent.index.value);
      const expected = [100, 101, 99, 102, 98];
      assert.deepStrictEqual(indices, expected);
      // do same but skip 3
      const params2 = {
        georel: 'near;maxDistance:10000',
        geometry: 'point',
        coords: `${pt.latitude},${pt.longitude}`,
        limit: 5,
        offset: 3,
      };
      const {entities: entities2} = await api.getEntities({params: params2});
      assert.strictEqual(entities2.length, 5);
      const indices2 = _.map(entities2, (ent) => ent.index.value);
      const expected2 = [102, 98, 103, 97, 104];
      assert.deepStrictEqual(indices2, expected2);
    });

    it('find the default at 5k (halfway) with radius 10k (i.e. all of them qualify - match q=index<20, but limit 10, no skip, then skip 3)', async () => {
      const pt = geolib.computeDestinationPoint(pt0, 5003, 0);
      const params = {
        georel: 'near;maxDistance:10000',
        geometry: 'point',
        coords: `${pt.latitude},${pt.longitude}`,
        q: parseQueryString('index<20'),
        limit: 5,
      };
      const {entities, count} = await api.getEntities({params});
      assert.strictEqual(count, null);
      assert.strictEqual(entities.length, 5);
      const indices = _.map(entities, (ent) => ent.index.value);
      const expected = [19, 18, 17, 16, 15];
      assert.deepStrictEqual(indices, expected);
      // skip 3
      params.offset = 3; // mutate
      const {entities: ents2} = await api.getEntities({params});
      assert.strictEqual(ents2.length, 5);
      const indices2 = _.map(ents2, (ent) => ent.index.value);
      const expected2 = [16, 15, 14, 13, 12];
      assert.deepStrictEqual(indices2, expected2);
    });

    it('find the default at 5k (halfway) OUTSIDE radius 4900 (i.e. all of them qualify - sort by index, but limit 10, no skip)', async () => {
      const pt = geolib.computeDestinationPoint(pt0, 5003, 0);
      const params = {
        georel: 'near;minDistance:4800',
        geometry: 'point',
        coords: `${pt.latitude},${pt.longitude}`,
        limit: 10,
      };
      const {entities, count} = await api.getEntities({params});
      assert.strictEqual(count, null);
      assert.strictEqual(entities.length, 9);
      const indices = _.map(entities, (ent) => ent.index.value);
      const expected = [196, 4, 197, 3, 198, 2, 199, 1, 0];
      assert.deepStrictEqual(indices, expected);
    });

    it('we should be able to get a full count but ONLY get 100 (default geonear limit)', async () => {
      const pt = geolib.computeDestinationPoint(pt0, 5003, 0);
      const params = {
        georel: 'near;maxDistance:10000',
        geometry: 'point',
        coords: `${pt.latitude},${pt.longitude}`,
        count: true,
        limit: 4,
      };
      const {count, entities} = await api.getEntities({params});
      assert.strictEqual(entities.length, 4);
      assert.strictEqual(count, 100); // 100!!!! (default limit for geonear query..)
    });

    it('we do get a full count if we specify a custom limit high enough though', async () => {
      const pt = geolib.computeDestinationPoint(pt0, 5003, 0);
      const params = {
        georel: 'near;maxDistance:10000',
        geometry: 'point',
        coords: `${pt.latitude},${pt.longitude}`,
        count: true,
        limit: 1000,
      };
      const {count, entities} = await api.getEntities({params});
      assert.strictEqual(entities.length, 200);
      assert.strictEqual(count, 200);
    });

  });

});
