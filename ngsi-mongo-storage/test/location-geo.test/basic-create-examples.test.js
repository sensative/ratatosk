/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const {
  assertRejects,
  ngsiEntityFormatter: {formatEntity},
  ngsiStorageErrorUtils: {errors: storageErrors},
} = require('@ratatosk/ngsi-utils');

const {connectionManager: {stop, restart, mongoRaw}} = require('../../src');
const {getMongoConfig} = require('../test-config');

const stripMongoEntity = (mongoEnt) => {
  return _.assign(
    _.omit(mongoEnt, ['creDate', 'modDate', '_id']),
    {attrs: _.mapValues(mongoEnt.attrs, (attr) => _.omit(attr, ['creDate', 'modDate']))},
  );
};

describe('basic-create-examples', () => {

  let api;
  before(async () => {
    api = await restart(getMongoConfig());
    await mongoRaw.assertEntitiesEmpty();
  });
  after(async () => {
    await mongoRaw.wipeEntities();
    await stop();
  });

  describe('simple location format', () => {

    afterEach(async () => {
      await mongoRaw.wipeEntities();
    });

    it('create an entity with geo:point', async () => {
      const entity = formatEntity({
        type: 'bvvwat',
        id: 'grytta',
        loco: {
          type: 'geo:point',
          value: '  3 ,   9  ',
        },
      });
      await api.createEntity({entity});
      const savedEnt = await api.findEntity({entityId: 'grytta'});
      const expectedSavedEnt = {
        id: 'grytta',
        type: 'bvvwat',
        loco: {
          type: 'geo:point',
          value: '3,9',
          metadata: {},
        },
      };
      assert.deepStrictEqual(savedEnt, expectedSavedEnt);
      // and check the mongo structure
      const [mongoEnt] = await mongoRaw.getEntities();
      const expectedMongoEnt = {
        orionId: {id: 'grytta', type: 'bvvwat', servicePath: '/'},
        attrs: {
          loco: {
            type: 'geo:point',
            value: {type: 'Point', coordinates: [9, 3]},
            md: {},
          },
        },
        location: {
          attrName: 'loco',
          coords: {
            coordinates: [9, 3],
            type: 'Point',
          },
        },
      };
      assert.deepStrictEqual(stripMongoEntity(mongoEnt), expectedMongoEnt);
    });

    it('create an entity with 2 geo:points, 1 with defaultLocation=true', async () => {
      const entity = formatEntity({
        type: 'bvvwat',
        id: 'grytta',
        loco: {
          type: 'geo:point',
          value: '  3 ,   9  ',
          metadata: {defaultLocation: false},
        },
        noco: {
          type: 'geo:point',
          value: '2,3',
          metadata: {defaultLocation: true},
        },
      });
      await api.createEntity({entity});
      const savedEnt = await api.findEntity({entityId: 'grytta'});
      const expectedSavedEnt = {
        id: 'grytta',
        type: 'bvvwat',
        loco: {
          type: 'geo:point',
          value: '3,9',
          metadata: {defaultLocation: {type: 'Boolean', value: false}},
        },
        noco: {
          type: 'geo:point',
          value: '2,3',
          metadata: {defaultLocation: {type: 'Boolean', value: true}},
        },
      };
      assert.deepStrictEqual(savedEnt, expectedSavedEnt);
      // and check the mongo structure
      const [mongoEnt] = await mongoRaw.getEntities();
      const expectedMongoEnt = {
        orionId: {id: 'grytta', type: 'bvvwat', servicePath: '/'},
        attrs: {
          loco: {
            type: 'geo:point',
            value: {type: 'Point', coordinates: [9, 3]},
            md: {defaultLocation: {type: 'Boolean', value: false}},
          },
          noco: {
            type: 'geo:point',
            value: {type: 'Point', coordinates: [3, 2]},
            md: {defaultLocation: {type: 'Boolean', value: true}},
          },
        },
        location: {
          attrName: 'noco',
          coords: {
            coordinates: [3, 2],
            type: 'Point',
          },
        },
      };
      assert.deepStrictEqual(stripMongoEntity(mongoEnt), expectedMongoEnt);
    });

    it('create an entity with 2 geo:points, none with defaultLocation set at all', async () => {
      const entity = formatEntity({
        type: 'bvvwat',
        id: 'grytta',
        loco: {
          type: 'geo:point',
          value: '  3 ,   9  ',
        },
        noco: {
          type: 'geo:point',
          value: '2,3',
        },
      });
      const err = storageErrors.MULTIPLE_GEOATTRS_BUT_NO_DEFAULT();
      await assertRejects(() => api.createEntity({entity}), err);
    });

    it('create an entity with 2 geo:points, both with defaultLocation=false', async () => {
      const entity = formatEntity({
        type: 'bvvwat',
        id: 'grytta',
        loco: {
          type: 'geo:point',
          value: '  3 ,   9  ',
          metadata: {defaultLocation: false},
        },
        noco: {
          type: 'geo:point',
          value: '2,3',
          metadata: {defaultLocation: false},
        },
      });
      const err = storageErrors.MULTIPLE_GEOATTRS_BUT_NO_DEFAULT();
      await assertRejects(() => api.createEntity({entity}), err);
    });

    it('create an entity with 2 geo:points, both with defaultLocation=true', async () => {
      const entity = formatEntity({
        type: 'bvvwat',
        id: 'grytta',
        loco: {
          type: 'geo:point',
          value: '  3 ,   9  ',
          metadata: {defaultLocation: true},
        },
        noco: {
          type: 'geo:point',
          value: '2,3',
          metadata: {defaultLocation: true},
        },
      });
      const err = storageErrors.MULTIPLE_GEOATTRS_AND_MULTIPLE_DEFAULTS();
      await assertRejects(() => api.createEntity({entity}), err);
    });

    it('create an entity with geo:box', async () => {
      const entity = formatEntity({
        type: 'bvvwat',
        id: 'grytta',
        loco: {
          type: 'geo:box',
          value: ['0,0', '1,1'],
        },
      });
      await api.createEntity({entity});
      const savedEnt = await api.findEntity({entityId: 'grytta'});
      const expectedSavedEnt = {
        id: 'grytta',
        type: 'bvvwat',
        loco: {
          type: 'geo:box',
          value: ['0,0', '1,1'],
          metadata: {},
        },
      };
      assert.deepStrictEqual(savedEnt, expectedSavedEnt);
      // and check the mongo structure
      const [mongoEnt] = await mongoRaw.getEntities();
      const expectedMongoEnt = {
        orionId: {id: 'grytta', type: 'bvvwat', servicePath: '/'},
        attrs: {
          loco: {
            type: 'geo:box',
            value: {
              type: 'Polygon',
              coordinates: [[[0, 0], [0, 1], [1, 1], [1, 0], [0, 0]]],
            },
            md: {},
          },
        },
        location: {
          attrName: 'loco',
          coords: {
            type: 'Polygon',
            coordinates: [[[0, 0], [0, 1], [1, 1], [1, 0], [0, 0]]],
          },
        },
      };
      assert.deepStrictEqual(stripMongoEntity(mongoEnt), expectedMongoEnt);
    });

    it('create an entity with geo:line', async () => {
      const entity = formatEntity({
        type: 'bvvwat',
        id: 'grytta',
        loco: {
          type: 'geo:line',
          value: ['0,0', '1,1'],
        },
      });
      await api.createEntity({entity});
      const savedEnt = await api.findEntity({entityId: 'grytta'});
      const expectedSavedEnt = {
        id: 'grytta',
        type: 'bvvwat',
        loco: {
          type: 'geo:line',
          value: ['0,0', '1,1'],
          metadata: {},
        },
      };
      assert.deepStrictEqual(savedEnt, expectedSavedEnt);
      // and check the mongo structure
      const [mongoEnt] = await mongoRaw.getEntities();
      const expectedMongoEnt = {
        orionId: {id: 'grytta', type: 'bvvwat', servicePath: '/'},
        attrs: {
          loco: {
            type: 'geo:line',
            value: {
              type: 'LineString',
              coordinates: [[0, 0], [1, 1]],
            },
            md: {},
          },
        },
        location: {
          attrName: 'loco',
          coords: {
            type: 'LineString',
            coordinates: [[0, 0], [1, 1]],
          },
        },
      };
      assert.deepStrictEqual(stripMongoEntity(mongoEnt), expectedMongoEnt);
    });

    it('create an entity with geo:polygon', async () => {
      const entity = formatEntity({
        type: 'bvvwat',
        id: 'grytta',
        loco: {
          type: 'geo:polygon',
          value: ['0,0', '1,0', '1,1', '0,0'],
        },
      });
      await api.createEntity({entity});
      const savedEnt = await api.findEntity({entityId: 'grytta'});
      const expectedSavedEnt = {
        id: 'grytta',
        type: 'bvvwat',
        loco: {
          type: 'geo:polygon',
          value: ['0,0', '1,0', '1,1', '0,0'],
          metadata: {},
        },
      };
      assert.deepStrictEqual(savedEnt, expectedSavedEnt);
      // and check the mongo structure
      const [mongoEnt] = await mongoRaw.getEntities();
      const expectedMongoEnt = {
        orionId: {id: 'grytta', type: 'bvvwat', servicePath: '/'},
        attrs: {
          loco: {
            type: 'geo:polygon',
            value: {
              type: 'Polygon',
              coordinates: [[[0, 0], [0, 1], [1, 1], [0, 0]]],
            },
            md: {},
          },
        },
        location: {
          attrName: 'loco',
          coords: {
            type: 'Polygon',
            coordinates: [[[0, 0], [0, 1], [1, 1], [0, 0]]],
          },
        },
      };
      assert.deepStrictEqual(stripMongoEntity(mongoEnt), expectedMongoEnt);
    });

  });

  describe('geo:json stuff', () => {

    afterEach(async () => {
      await mongoRaw.wipeEntities();
    });

    it('create an entity with geo:json Point', async () => {
      const entity = formatEntity({
        type: 'bvvwat',
        id: 'grytta',
        loco: {
          type: 'geo:json',
          value: {
            type: 'Point',
            coordinates: [2.2, 41.2],
          },
        },
      });
      await api.createEntity({entity});
      const savedEnt = await api.findEntity({entityId: 'grytta'});
      const expectedSavedEnt = {
        id: 'grytta',
        type: 'bvvwat',
        loco: {
          type: 'geo:json',
          value: {
            type: 'Point',
            coordinates: [2.2, 41.2],
          },
          metadata: {},
        },
      };
      assert.deepStrictEqual(savedEnt, expectedSavedEnt);
      // and check the mongo structure
      const [mongoEnt] = await mongoRaw.getEntities();
      const expectedMongoEnt = {
        orionId: {id: 'grytta', type: 'bvvwat', servicePath: '/'},
        attrs: {
          loco: {
            type: 'geo:json',
            value: {
              type: 'Point',
              coordinates: [2.2, 41.2],
            },
            md: {},
          },
        },
        location: {
          attrName: 'loco',
          coords: {
            type: 'Point',
            coordinates: [2.2, 41.2],
          },
        },
      };
      assert.deepStrictEqual(stripMongoEntity(mongoEnt), expectedMongoEnt);
    });

    it('create an entity with geo:json MultiPoint', async () => {
      const entity = formatEntity({
        type: 'bvvwat',
        id: 'grytta',
        loco: {
          type: 'geo:json',
          value: {
            type: 'MultiPoint',
            coordinates: [[1, 2], [3, 4]],
          },
        },
      });
      await api.createEntity({entity});
      const savedEnt = await api.findEntity({entityId: 'grytta'});
      const expectedSavedEnt = {
        id: 'grytta',
        type: 'bvvwat',
        loco: {
          type: 'geo:json',
          value: {
            type: 'MultiPoint',
            coordinates: [[1, 2], [3, 4]],
          },
          metadata: {},
        },
      };
      assert.deepStrictEqual(savedEnt, expectedSavedEnt);
      // and check the mongo structure
      const [mongoEnt] = await mongoRaw.getEntities();
      const expectedMongoEnt = {
        orionId: {id: 'grytta', type: 'bvvwat', servicePath: '/'},
        attrs: {
          loco: {
            type: 'geo:json',
            value: {
              type: 'MultiPoint',
              coordinates: [[1, 2], [3, 4]],
            },
            md: {},
          },
        },
        location: {
          attrName: 'loco',
          coords: {
            type: 'MultiPoint',
            coordinates: [[1, 2], [3, 4]],
          },
        },
      };
      assert.deepStrictEqual(stripMongoEntity(mongoEnt), expectedMongoEnt);
    });

    it('create an entity with geo:json LineString', async () => {
      const entity = formatEntity({
        type: 'bvvwat',
        id: 'grytta',
        loco: {
          type: 'geo:json',
          value: {
            type: 'LineString',
            coordinates: [[1, 2], [3, 4]],
          },
        },
      });
      await api.createEntity({entity});
      const savedEnt = await api.findEntity({entityId: 'grytta'});
      const expectedSavedEnt = {
        id: 'grytta',
        type: 'bvvwat',
        loco: {
          type: 'geo:json',
          value: {
            type: 'LineString',
            coordinates: [[1, 2], [3, 4]],
          },
          metadata: {},
        },
      };
      assert.deepStrictEqual(savedEnt, expectedSavedEnt);
      // and check the mongo structure
      const [mongoEnt] = await mongoRaw.getEntities();
      const expectedMongoEnt = {
        orionId: {id: 'grytta', type: 'bvvwat', servicePath: '/'},
        attrs: {
          loco: {
            type: 'geo:json',
            value: {
              type: 'LineString',
              coordinates: [[1, 2], [3, 4]],
            },
            md: {},
          },
        },
        location: {
          attrName: 'loco',
          coords: {
            type: 'LineString',
            coordinates: [[1, 2], [3, 4]],
          },
        },
      };
      assert.deepStrictEqual(stripMongoEntity(mongoEnt), expectedMongoEnt);
    });

    it('create an entity with geo:json  INVALID  MultiLineString', async () => {
      const entity = formatEntity({
        type: 'bvvwat',
        id: 'grytta',
        loco: {
          type: 'geo:json',
          value: {
            type: 'MultiLineString',
            coordinates: [[1, 2], [3, 4]], // should be nested one more layer
          },
        },
      });
      await api.createEntity({entity});
      const savedEnt = await api.findEntity({entityId: 'grytta'});
      const expectedSavedEnt = {
        id: 'grytta',
        type: 'bvvwat',
        loco: {
          type: 'geo:json',
          value: {
            type: 'MultiLineString',
            coordinates: [[1, 2], [3, 4]],
          },
          metadata: {},
        },
      };
      assert.deepStrictEqual(savedEnt, expectedSavedEnt);
      // and check the mongo structure
      const [mongoEnt] = await mongoRaw.getEntities();
      const expectedMongoEnt = {
        orionId: {id: 'grytta', type: 'bvvwat', servicePath: '/'},
        attrs: {
          loco: {
            type: 'geo:json',
            value: {
              type: 'MultiLineString',
              coordinates: [[1, 2], [3, 4]],
            },
            md: {},
          },
        },
      };
      // note that location is missing (since the geojson is invalid)
      assert.deepStrictEqual(stripMongoEntity(mongoEnt), expectedMongoEnt);
    });

    it('create an entity with geo:json MultiLineString', async () => {
      const entity = formatEntity({
        type: 'bvvwat',
        id: 'grytta',
        loco: {
          type: 'geo:json',
          value: {
            type: 'MultiLineString',
            coordinates: [[[1, 2], [3, 4]]],
          },
        },
      });
      await api.createEntity({entity});
      const savedEnt = await api.findEntity({entityId: 'grytta'});
      const expectedSavedEnt = {
        id: 'grytta',
        type: 'bvvwat',
        loco: {
          type: 'geo:json',
          value: {
            type: 'MultiLineString',
            coordinates: [[[1, 2], [3, 4]]],
          },
          metadata: {},
        },
      };
      assert.deepStrictEqual(savedEnt, expectedSavedEnt);
      // and check the mongo structure
      const [mongoEnt] = await mongoRaw.getEntities();
      const expectedMongoEnt = {
        orionId: {id: 'grytta', type: 'bvvwat', servicePath: '/'},
        attrs: {
          loco: {
            type: 'geo:json',
            value: {
              type: 'MultiLineString',
              coordinates: [[[1, 2], [3, 4]]],
            },
            md: {},
          },
        },
        location: {
          attrName: 'loco',
          coords: {
            type: 'MultiLineString',
            coordinates: [[[1, 2], [3, 4]]],
          },
        },
      };
      assert.deepStrictEqual(stripMongoEntity(mongoEnt), expectedMongoEnt);
    });

    it('create an entity with geo:json Polygon', async () => {
      const entity = formatEntity({
        type: 'bvvwat',
        id: 'grytta',
        loco: {
          type: 'geo:json',
          value: {
            type: 'Polygon',
            coordinates: [[[1, 2], [3, 4], [5, 6], [1, 2]]],
          },
        },
      });
      await api.createEntity({entity});
      const savedEnt = await api.findEntity({entityId: 'grytta'});
      const expectedSavedEnt = {
        id: 'grytta',
        type: 'bvvwat',
        loco: {
          type: 'geo:json',
          value: {
            type: 'Polygon',
            coordinates: [[[1, 2], [3, 4], [5, 6], [1, 2]]],
          },
          metadata: {},
        },
      };
      assert.deepStrictEqual(savedEnt, expectedSavedEnt);
      // and check the mongo structure
      const [mongoEnt] = await mongoRaw.getEntities();
      const expectedMongoEnt = {
        orionId: {id: 'grytta', type: 'bvvwat', servicePath: '/'},
        attrs: {
          loco: {
            type: 'geo:json',
            value: {
              type: 'Polygon',
              coordinates: [[[1, 2], [3, 4], [5, 6], [1, 2]]],
            },
            md: {},
          },
        },
        location: {
          attrName: 'loco',
          coords: {
            type: 'Polygon',
            coordinates: [[[1, 2], [3, 4], [5, 6], [1, 2]]],
          },
        },
      };
      assert.deepStrictEqual(stripMongoEntity(mongoEnt), expectedMongoEnt);
    });

    it('create an entity with geo:json MultiPolygon', async () => {
      const entity = formatEntity({
        type: 'bvvwat',
        id: 'grytta',
        loco: {
          type: 'geo:json',
          value: {
            type: 'MultiPolygon',
            coordinates: [[[[1, 2], [3, 4], [5, 6], [1, 2]]]],
          },
        },
      });
      await api.createEntity({entity});
      const savedEnt = await api.findEntity({entityId: 'grytta'});
      const expectedSavedEnt = {
        id: 'grytta',
        type: 'bvvwat',
        loco: {
          type: 'geo:json',
          value: {
            type: 'MultiPolygon',
            coordinates: [[[[1, 2], [3, 4], [5, 6], [1, 2]]]],
          },
          metadata: {},
        },
      };
      assert.deepStrictEqual(savedEnt, expectedSavedEnt);
      // and check the mongo structure
      const [mongoEnt] = await mongoRaw.getEntities();
      const expectedMongoEnt = {
        orionId: {id: 'grytta', type: 'bvvwat', servicePath: '/'},
        attrs: {
          loco: {
            type: 'geo:json',
            value: {
              type: 'MultiPolygon',
              coordinates: [[[[1, 2], [3, 4], [5, 6], [1, 2]]]],
            },
            md: {},
          },
        },
        location: {
          attrName: 'loco',
          coords: {
            type: 'MultiPolygon',
            coordinates: [[[[1, 2], [3, 4], [5, 6], [1, 2]]]],
          },
        },
      };
      assert.deepStrictEqual(stripMongoEntity(mongoEnt), expectedMongoEnt);
    });

  });

});
