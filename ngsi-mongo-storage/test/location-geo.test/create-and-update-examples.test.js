/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const {
  assertRejects,
  ngsiEntityFormatter: {formatEntity, formatAttribute},
  ngsiStorageErrorUtils: {errors: storageErrors},
} = require('@ratatosk/ngsi-utils');

const {connectionManager: {stop, restart, mongoRaw}} = require('../../src');
const {getMongoConfig} = require('../test-config');

const stripMongoEntity = (mongoEnt) => {
  return _.assign(
    _.omit(mongoEnt, ['creDate', 'modDate', '_id']),
    {attrs: _.mapValues(mongoEnt.attrs, (attr) => _.omit(attr, ['creDate', 'modDate']))},
  );
};

describe('basic-create-examples', () => {

  let api;
  before(async () => {
    api = await restart(getMongoConfig());
    await mongoRaw.assertEntitiesEmpty();
  });
  after(async () => {
    await mongoRaw.wipeEntities();
    await stop();
  });

  describe('simple location format', () => {

    it('create an entity with 2 geo attrs (but 0 defaultLocation=true) (FAILS)', async () => {
      const entity = formatEntity({
        type: 'bvvwat',
        id: 'grytta',
        loco: {
          type: 'geo:point',
          value: '1,2',
        },
        noco: {
          type: 'geo:line',
          value: ['1,2', '3,4'],
        },
      });
      const err = storageErrors.MULTIPLE_GEOATTRS_BUT_NO_DEFAULT();
      await assertRejects(api.createEntity({entity}), err);
    });

    it('create an entity with 2 geo attrs (AND 2 defaultLocation=true) (FAILS)', async () => {
      const entity = formatEntity({
        type: 'bvvwat',
        id: 'grytta',
        loco: {
          type: 'geo:point',
          value: '1,2',
          metadata: {defaultLocation: true},
        },
        noco: {
          type: 'geo:line',
          value: ['1,2', '3,4'],
          metadata: {defaultLocation: true},
        },
      });
      const err = storageErrors.MULTIPLE_GEOATTRS_AND_MULTIPLE_DEFAULTS();
      await assertRejects(api.createEntity({entity}), err);
    });

    it('create an entity with 2 geo:point(s) (and 1 defaultLocation=true)', async () => {
      const entity = formatEntity({
        type: 'bvvwat',
        id: 'grytta',
        loco: {
          type: 'geo:point',
          value: '1,2',
          metadata: {defaultLocation: true},
        },
        noco: {
          type: 'geo:line',
          value: ['1,2', '3,4'],
        },
      });
      await api.createEntity({entity});
      const savedEnt = await api.findEntity({entityId: 'grytta'});
      const expectedSavedEnt = {
        id: 'grytta',
        type: 'bvvwat',
        loco: {
          type: 'geo:point',
          value: '1,2',
          metadata: {defaultLocation: {type: 'Boolean', value: true}},
        },
        noco: {
          type: 'geo:line',
          value: ['1,2', '3,4'],
          metadata: {},
        },
      };
      assert.deepStrictEqual(savedEnt, expectedSavedEnt);
      // and check the mongo structure
      const [mongoEnt] = await mongoRaw.getEntities();
      const expectedMongoEnt = {
        orionId: {id: 'grytta', type: 'bvvwat', servicePath: '/'},
        attrs: {
          loco: {
            type: 'geo:point',
            value: {type: 'Point', coordinates: [2, 1]},
            md: {
              defaultLocation: {type: 'Boolean', value: true},
            },
          },
          noco: {
            type: 'geo:line',
            value: {type: 'LineString', coordinates: [[2, 1], [4, 3]]},
            md: {},
          },
        },
        location: {
          attrName: 'loco',
          coords: {
            coordinates: [2, 1],
            type: 'Point',
          },
        },
      };
      assert.deepStrictEqual(stripMongoEntity(mongoEnt), expectedMongoEnt);
    });

    it('update the entity (to BOTH defaultLocation=true) (FAILS)', async () => {
      const attrs = {
        loco: formatAttribute({
          type: 'geo:point',
          value: '1,2',
          metadata: {defaultLocation: true},
        }),
        noco: formatAttribute({
          type: 'geo:line',
          value: ['1,2', '3,4'],
          metadata: {defaultLocation: true},
        }),
      };
      const err = storageErrors.MULTIPLE_GEOATTRS_AND_MULTIPLE_DEFAULTS();
      await assertRejects(api.patchEntityAttrs({entityId: 'grytta', attrs}), err);
    });

    it('switching which attr has the defaultLocation should work fine', async () => {
      const attrs = {
        loco: formatAttribute({
          type: 'geo:point',
          value: '1,2',
          metadata: {defaultLocation: false},
        }),
        noco: formatAttribute({
          type: 'geo:line',
          value: ['1,2', '3,4'],
          metadata: {defaultLocation: true},
        }),
      };
      await api.patchEntityAttrs({entityId: 'grytta', attrs});
      const savedEnt = await api.findEntity({entityId: 'grytta'});
      const expectedSavedEnt = {
        id: 'grytta',
        type: 'bvvwat',
        loco: {
          type: 'geo:point',
          value: '1,2',
          metadata: {defaultLocation: {type: 'Boolean', value: false}},
        },
        noco: {
          type: 'geo:line',
          value: ['1,2', '3,4'],
          metadata: {defaultLocation: {type: 'Boolean', value: true}},
        },
      };
      assert.deepStrictEqual(savedEnt, expectedSavedEnt);
      // and check the mongo structure
      const [mongoEnt] = await mongoRaw.getEntities();
      const expectedMongoEnt = {
        orionId: {id: 'grytta', type: 'bvvwat', servicePath: '/'},
        attrs: {
          loco: {
            type: 'geo:point',
            value: {type: 'Point', coordinates: [2, 1]},
            md: {
              defaultLocation: {type: 'Boolean', value: false},
            },
          },
          noco: {
            type: 'geo:line',
            value: {type: 'LineString', coordinates: [[2, 1], [4, 3]]},
            md: {
              defaultLocation: {type: 'Boolean', value: true},
            },
          },
        },
        location: {
          attrName: 'noco',
          coords: {
            coordinates: [[2, 1], [4, 3]],
            type: 'LineString',
          },
        },
      };
      assert.deepStrictEqual(stripMongoEntity(mongoEnt), expectedMongoEnt);
    });

  });

});
