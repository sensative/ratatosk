/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const assert = require('assert');
const geolib = require('geolib');

const {
  assertRejects,
  ngsiEntityFormatter: {formatEntity},
  ngsiSql: {queryParser: {parseQueryString}},
} = require('@ratatosk/ngsi-utils');

const {connectionManager: {stop, restart, mongoRaw}} = require('../../src');
const {getMongoConfig} = require('../test-config');

describe('geo-fencing-singles', () => {

  let api;
  before(async () => {
    api = await restart(getMongoConfig());
    await mongoRaw.assertEntitiesEmpty();
  });
  after(async () => {
    await mongoRaw.wipeEntities();
    await stop();
  });

  describe('findEntity should geofence correctly', () => {

    const pt0 = {longitude: 23, latitude: 24};
    before(async () => {
      const pt1 = geolib.computeDestinationPoint(pt0, 998, 35);
      const pt2 = geolib.computeDestinationPoint(pt0, 1003, 125);
      const pt3 = geolib.computeDestinationPoint(pt0, 1003, 324);
      const ent0 = formatEntity({
        id: 'herp',
        type: 'aal',
        hop: {type: 'geo:point', value: `${pt1.latitude},${pt1.longitude}`},
        udder: 2,
      });
      const ent1 = formatEntity({
        id: 'herp',
        type: 'bal',
        hop: {type: 'geo:point', value: `${pt2.latitude},${pt2.longitude}`},
        udder: 4,
      });
      const ent2 = formatEntity({
        id: 'herp',
        type: 'cal',
        hop: {type: 'geo:point', value: `${pt3.latitude},${pt3.longitude}`},
        udder: 6,
      });
      await api.createEntity({entity: ent0});
      await api.createEntity({entity: ent1});
      await api.createEntity({entity: ent2});
    });
    after(async () => mongoRaw.wipeEntities());

    it('find SINGLE entity <1000 m from pt0 (quick check for different endpoint)', async () => {
      const entity = await api.findEntity({
        entityId: 'herp',
        params: {
          georel: 'near;maxDistance:1000',
          geometry: 'point',
          coords: `${pt0.latitude},${pt0.longitude}`,
        },
      });
      assert.strictEqual(entity.id, 'herp');
      assert.strictEqual(entity.type, 'aal');
    });

    it('find SINGLE entity <1000 m from pt0 (FAILS cause 2 were found)', async () => {
      const dbArg = {
        entityId: 'herp',
        params: {
          georel: 'near;minDistance:1000',
          geometry: 'point',
          coords: `${pt0.latitude},${pt0.longitude}`,
        },
      };
      const err = 'More than 1 entity was found that matches the query';
      await assertRejects(api.findEntity(dbArg), err);
    });

    it('find entity < 1000 m from pt0, with udder > 2 (FAILS)', async () => {
      const dbArg = {
        entityId: 'herp',
        params: {
          georel: 'near;maxDistance:1000',
          geometry: 'point',
          coords: `${pt0.latitude},${pt0.longitude}`,
          q: parseQueryString('udder>2'),
        },
      };
      const err = 'No matching entity to the query was found';
      await assertRejects(api.findEntity(dbArg), err);
    });

    it('find entity > 1000 m from pt0, with udder < 5', async () => {
      const entity = await api.findEntity({
        entityId: 'herp',
        params: {
          georel: 'near;minDistance:1000',
          geometry: 'point',
          coords: `${pt0.latitude},${pt0.longitude}`,
          q: parseQueryString('udder<5'),
        },
      });
      assert.strictEqual(entity.id, 'herp');
      assert.strictEqual(entity.type, 'bal');
    });

  });

});
