/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');

const {connectionManager: {stop, restart, mongoRaw}} = require('../src');
const {getMongoConfig} = require('./test-config');

describe('subscription-crud-flowthrough', () => {

  let api;
  before(async () => {
    api = await restart(getMongoConfig());
  });
  after(async () => {
    await mongoRaw.wipeSubscriptions();
    await stop();
  });

  it('create some fresh entities (append)', async () => {
    const subscriptionArg = {
      servicePaths: ['/'],
      payload: {
        description: 'asdf;lkj',
        subject: {
          entities: [{id: 'blue'}],
        },
        notification: {
          http: {
            url: 'asdf',
          },
        },
      },
    };
    await api.createSubscription(subscriptionArg);
    const [mongoSub] = await mongoRaw.getSubscriptions();
    const expectedMongoSub = {
      reference: 'asdf',
      custom: false,
      throttling: 0,
      servicePath: '/',
      description: 'asdf;lkj',
      entities: [{id: 'blue', isPattern: 'false'}],
      format: 'normalized',
      conditions: [],
      expression: {q: '', mq: '', geometry: '', coords: '', georel: ''},
    };
    assert.deepStrictEqual(_.omit(mongoSub, '_id'), expectedMongoSub);
  });

  it('getSubscriptions', async () => {
    const subs = await api.getSubscriptions({});
    assert.strictEqual(subs.length, 1);
    const sub = subs[0];
    const expectedSub = {
      description: 'asdf;lkj',
      subject: {
        entities: [{id: 'blue'}],
      },
      status: 'active',
      notification: {
        http: {
          url: 'asdf',
        },
        timesSent: 0,
      },
    };
    assert.deepStrictEqual(_.omit(sub, 'id'), expectedSub);
  });

  it('findSubscription', async () => {
    const [sub] = await api.getSubscriptions();
    const subscriptionId = sub.id;
    const singleSub = await api.findSubscription({subscriptionId});
    const expectedSub = {
      id: subscriptionId,
      description: 'asdf;lkj',
      status: 'active',
      subject: {
        entities: [{id: 'blue'}],
      },
      notification: {
        http: {
          url: 'asdf',
        },
        timesSent: 0,
      },
    };
    assert.deepStrictEqual(singleSub, expectedSub);
  });

  it('deleteSubscription', async () => {
    const [sub] = await api.getSubscriptions();
    const subscriptionId = sub.id;
    await api.deleteSubscription({subscriptionId});
    const postSubs = await api.getSubscriptions();
    assert.strictEqual(postSubs.length, 0);
  });

});
