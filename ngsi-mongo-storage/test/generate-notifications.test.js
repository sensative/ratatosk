/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');

const {connectionManager: {stop, restart, mongoRaw}} = require('../src');
const {getMongoConfig} = require('./test-config');

describe('generate-notifications (and acknowledgement)', () => {

  let api;
  before(async () => {
    api = await restart(getMongoConfig());
    await mongoRaw.assertSubscriptionsEmpty();
    await mongoRaw.assertEntitiesEmpty();
  });
  afterEach(async () => {
    await mongoRaw.wipeSubscriptions();
    await mongoRaw.wipeEntities();
  });
  after(async () => {
    await stop();
  });

  it('create a subscription and an entity, then generate a notification', async () => {
    // create an entity
    const entity = {
      id: 'aa',
      type: 'bb',
      cee: {type: 'Number', value: 23},
    };
    const docPair = await api.createEntity({entity});
    // create a subscription
    const subscriptionArg = {
      servicePaths: ['/'],
      payload: {
        description: 'asdf;lkj',
        subject: {entities: [{id: 'aa'}]},
        notification: {http: {url: 'asdf'}},
      },
    };
    await api.createSubscription(subscriptionArg);
    // and then generate the notifications
    const res = await api.generateNotifications(docPair);
    assert.strictEqual(res.length, 1);
    const subscription = res[0].subscription;
    const notification = res[0].notification;
    assert.strictEqual(subscription.id, notification.subscriptionId);
    const timesSent = _.get(subscription, 'notification.timesSent');
    assert.strictEqual(timesSent, 0);
    // and then send an acknowledgement
    await api.acknowledgeNotification({subscription, succeeded: true});
    const finalSubscription = await api.findSubscription({subscriptionId: subscription.id});
    const finalTimesSent = _.get(finalSubscription, 'notification.timesSent');
    assert.strictEqual(finalTimesSent, 1);
  });

});
