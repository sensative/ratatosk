/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const {
  assertRejects,
  ngsiEntityFormatter: {formatEntity, formatAttrs, formatAttribute},
  ngsiStorageErrorUtils: {errorTemplates},
} = require('@ratatosk/ngsi-utils');

const {connectionManager: {stop, restart, mongoRaw}} = require('../src');
const {getMongoConfig} = require('./test-config');

describe('global-registration-attrs-triggered', () => {

  let api;
  beforeEach(async () => {
    const config = _.assign(getMongoConfig(), {globalRegistrationAttrs: ['cmd']});
    api = await restart(config);
    await mongoRaw.assertEntitiesEmpty();
    const entity = {id: 'buggo', type: 'dice'};
    await api.createEntity({entity});
  });

  afterEach(async () => {
    await mongoRaw.wipeEntities();
    await stop();
  });

  it('create entity with "cmd" and one other attr FAILS', async () => {
    const entity = formatEntity({id: 'jigs', type: 'fortoo', cmd: 25, stacc: 'asdf'});
    await assertRejects(api.createEntity({entity}), errorTemplates.CANNOT_CREATE_ATTR_COLLIDES_WITH_REGISTRATION);
  });

  it('replaceEntityAttrs', async () => {
    const attrs = formatAttrs({cmd: 23, derp: 24});
    const res = await api.replaceEntityAttrs({entityId: 'buggo', attrs});
    // has one attr
    assert.deepStrictEqual(_.keys(res.modifiedEntity.attrs), ['derp']);
    // and the registration looks as expected
    assert.deepStrictEqual(res.registrations, {cmd: {value: 23, metadata: {}, type: 'Number'}});
  });

  it('upsertEntityAttrs', async () => {
    // add an attr
    const initAttrs = formatAttrs({doop: 'loo'});
    await api.replaceEntityAttrs({entityId: 'buggo', attrs: initAttrs});
    // and then upsert
    const attrs = formatAttrs({cmd: 34, doop: 43});
    const res = await api.upsertEntityAttrs({entityId: 'buggo', attrs});
    // has one attr
    assert.deepStrictEqual(_.keys(res.modifiedEntity.attrs), ['doop']);
    assert.strictEqual(res.modifiedEntity.attrs.doop.value, 43);
    // and the registration looks as expected
    assert.deepStrictEqual(res.registrations, {cmd: {value: 34, metadata: {}, type: 'Number'}});
  });

  it('upsertEntityAttrs with APPEND_STRICT fails to trigger registration', async () => {
    // and then upsert
    const attrs = formatAttrs({cmd: 'beep'});
    const dbArg = {entityId: 'buggo', attrs, params: {append: true}};
    await assertRejects(api.upsertEntityAttrs(dbArg), errorTemplates.CANNOT_CREATE_ATTR_COLLIDES_WITH_REGISTRATION);
  });

  it('patchEntityAttrs', async () => {
    // add an attr
    const initAttrs = formatAttrs({doop: 'loo'});
    await api.replaceEntityAttrs({entityId: 'buggo', attrs: initAttrs});
    // and then upsert
    const attrs = formatAttrs({cmd: 345, doop: 44});
    const res = await api.patchEntityAttrs({entityId: 'buggo', attrs});
    // has one attr
    assert.deepStrictEqual(_.keys(res.modifiedEntity.attrs), ['doop']);
    assert.strictEqual(res.modifiedEntity.attrs.doop.value, 44);
    // and the registration looks as expected
    assert.deepStrictEqual(res.registrations, {cmd: {value: 345, metadata: {}, type: 'Number'}});
  });

  it('updateEntityAttr', async () => {
    const attr = formatAttribute(5.5);
    const dbArg = {entityId: 'buggo', attrName: 'cmd', attr};
    const res = await api.updateEntityAttr(dbArg);
    // the docs look the same
    assert.deepStrictEqual(res.modifiedEntity, res.originalEntity);
    // and the registration looks as expected
    assert.deepStrictEqual(res.registrations, {cmd: {value: 5.5, metadata: {}, type: 'Number'}});
  });

  it('updateEntityAttrValue', async () => {
    const dbArg = {entityId: 'buggo', attrName: 'cmd', attrValue: 1234};
    const res = await api.updateEntityAttrValue(dbArg);
    // the docs look the same
    assert.deepStrictEqual(res.modifiedEntity, res.originalEntity);
    // and the registration looks as expected
    assert.deepStrictEqual(res.registrations, {cmd: {value: 1234, metadata: {}, type: 'Number'}});
  });

});
