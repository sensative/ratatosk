/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');

const {
  assertRejects,
  ngsiDateTimeUtils: {fromDate, toDate, fromSeconds, toSeconds},
  ngsiEntityFormatter: {formatEntity},
} = require('@ratatosk/ngsi-utils');

const {
  connectionManager: {
    stop,
    restart,
    mongoRaw,
    importOrion,
    exportOrion,
  },
} = require('../src');

const {getMongoConfig} = require('./test-config');

const orionConfig = (mongoConfig) => ({
  ..._.pick(mongoConfig, ['url', 'options']),
  dbName: 'veryunspecialngsidb',
  orionDbName: 'superspecialoriondb',
  overwrite: false,
});

const apiConfig = (mongoConfig) => ({
  ...mongoConfig,
  dbName: orionConfig().dbName,
});

describe('orion import/export tests', () => {

  it('populate ngsi-db with some entities', async () => {
    // set up the api
    const mongoConfig = getMongoConfig();
    const api = await restart(apiConfig(mongoConfig));
    await mongoRaw.assertEntitiesEmpty();
    // create a bunch of thin entities
    await Promise.all(_.times(1200, (index) => {
      const entity = {
        id: `numero_${index}`,
        type: 'thingy',
      };
      return api.createEntity({entity});
    }));
    // create one complex entity
    const complexEntity = formatEntity({
      id: 'complex_entity',
      type: 'complex_type',
      loco: {type: 'geo:point', value: '2,3', metadata: {defaultLocation: true}},
      polly: {type: 'geo:json', value: {type: 'Polygon', coordinates: [[[1, 2], [2, 3], [3, 4], [1, 2]]]}},
      dater: {type: 'DateTime', value: '2020-02-10T14:47:16.882Z'},
    });
    await api.createEntity({entity: complexEntity});
    // and shut it down
    const numEnts = await mongoRaw.countEntities();
    assert.strictEqual(numEnts, 1201);
    await stop();
    await assertRejects(mongoRaw.countEntities(), 'mongoRaw db is not connected');
  });

  it('should be able to restart and refind those entities...', async () => {
    // set up the api
    const mongoConfig = getMongoConfig();
    await restart(apiConfig(mongoConfig));
    const numEnts = await mongoRaw.countEntities();
    assert.strictEqual(numEnts, 1201);
    await stop();
    await assertRejects(mongoRaw.countEntities(), 'mongoRaw db is not connected');
  });

  it('should be able to copy the entire collection to the orion', async () => {
    // set up the copy connection
    const mongoConfig = getMongoConfig();
    const {orionEntities, ngsiEntities, closeClient} = await exportOrion(orionConfig(mongoConfig));
    const numOrionDocs = await orionEntities.countDocuments();
    assert.strictEqual(numOrionDocs, 1201);

    // get a date value from the original simple entity
    const [nsgiSimpleEntity] = await ngsiEntities.find({'orionId.id': 'numero_259'}).toArray();
    assert(_.isDate(nsgiSimpleEntity.creDate));
    const simpleOrionEntityDate = toSeconds(fromDate(nsgiSimpleEntity.creDate));

    // check one of the simple entities
    const [numero259] = await orionEntities.find({'_id.id': 'numero_259'}).toArray();
    const expectedNumero259 = {
      _id: {id: 'numero_259', type: 'thingy', servicePath: '/'},
      creDate: simpleOrionEntityDate,
      modDate: simpleOrionEntityDate,
      attrNames: [],
      attrs: {},
      location: null,
    };
    assert.deepStrictEqual(numero259, expectedNumero259);

    // get a date value from the original complex entity
    const [ngsiComplexEntity] = await ngsiEntities.find({'orionId.id': 'complex_entity'}).toArray();
    assert(_.isDate(ngsiComplexEntity.creDate));
    const complexOrionEntityDate = toSeconds(fromDate(ngsiComplexEntity.creDate));

    // and check the copied complex entity
    const [complexEntity] = await orionEntities.find({'_id.id': 'complex_entity'}).toArray();
    complexEntity.attrNames.sort(); // sometimes needed
    const expectedComplexEntity = {
      _id: {id: 'complex_entity', type: 'complex_type', servicePath: '/'},
      creDate: complexOrionEntityDate,
      modDate: complexOrionEntityDate,
      attrNames: ['dater', 'loco', 'polly'],
      attrs: {
        loco: {
          type: 'geo:point',
          value: '2,3',
          creDate: complexOrionEntityDate,
          modDate: complexOrionEntityDate,
          mdNames: ['defaultLocation'],
          md: {
            defaultLocation: {
              type: 'Boolean',
              value: true,
            },
          },
        },
        polly: {
          type: 'geo:json',
          value: {type: 'Polygon', coordinates: [[[1, 2], [2, 3], [3, 4], [1, 2]]]},
          creDate: complexOrionEntityDate,
          modDate: complexOrionEntityDate,
          mdNames: [],
          md: {},
        },
        dater: {
          type: 'DateTime',
          value: 1581346036,
          creDate: complexOrionEntityDate,
          modDate: complexOrionEntityDate,
          mdNames: [],
          md: {},
        },
      },
      location: {
        attrName: 'loco',
        coords: {
          type: 'Point',
          coordinates: [3, 2],
        },
      },
    };
    assert.deepStrictEqual(complexEntity, expectedComplexEntity);
    // and close the connections
    await closeClient();
  });

  it('should be possible to copy the copied orion entities BACK to ngsi mongo', async () => {
    // check the originals (i.e. the original originals..)
    const mongoConfig = getMongoConfig();
    await restart(apiConfig(mongoConfig));
    const numOrignal = await mongoRaw.countEntities();
    assert.strictEqual(numOrignal, 1201);
    const [originalComplexNgsiEntity] = await mongoRaw.getEntities({'orionId.id': 'complex_entity'});
    await stop();

    // since there are already entities in the ngsi collection, we need overwrite
    const err = 'ngsi entities collection already contains documents (must use "overwrite" option to force overwrite)';
    await assertRejects(importOrion(orionConfig(mongoConfig)), err);

    // use overwrite to fix this (and copy orion -> ngsi)
    const overwrite = true;
    const {orionEntities, ngsiEntities, closeClient} = await importOrion({...orionConfig(mongoConfig), overwrite});
    const numNgsiEntities = await ngsiEntities.countDocuments();
    assert.strictEqual(numNgsiEntities, 1201);

    // get a date value from the original simple entity
    const [orionSimpleEntity] = await orionEntities.find({'_id.id': 'numero_259'}).toArray();
    assert(_.isInteger(orionSimpleEntity.creDate));
    const simpleNgsiEntityDate = toDate(fromSeconds(orionSimpleEntity.creDate));

    // check one of the simple entities
    const [numero259] = await ngsiEntities.find({'orionId.id': 'numero_259'}).toArray();
    const expectedNumero259 = {
      orionId: {id: 'numero_259', type: 'thingy', servicePath: '/'},
      creDate: simpleNgsiEntityDate,
      modDate: simpleNgsiEntityDate,
      attrs: {},
      location: null,
    };
    assert.deepStrictEqual(_.omit(numero259, '_id'), expectedNumero259);

    // get a date value from the original complex entity
    const [orionComplexEntity] = await orionEntities.find({'_id.id': 'complex_entity'}).toArray();
    assert(_.isInteger(orionComplexEntity.creDate));
    const complexNgsiEntityDate = toDate(fromSeconds(orionComplexEntity.creDate));

    // and check the copied complex entity
    const [complexEntity] = await ngsiEntities.find({'orionId.id': 'complex_entity'}).toArray();
    const expectedComplexEntity = {
      orionId: {id: 'complex_entity', type: 'complex_type', servicePath: '/'},
      creDate: complexNgsiEntityDate,
      modDate: complexNgsiEntityDate,
      attrs: {
        loco: {
          type: 'geo:point',
          value: {type: 'Point', coordinates: [3, 2]},
          creDate: complexNgsiEntityDate,
          modDate: complexNgsiEntityDate,
          md: {
            defaultLocation: {
              type: 'Boolean',
              value: true,
            },
          },
        },
        polly: {
          type: 'geo:json',
          value: {type: 'Polygon', coordinates: [[[1, 2], [2, 3], [3, 4], [1, 2]]]},
          creDate: complexNgsiEntityDate,
          modDate: complexNgsiEntityDate,
          md: {},
        },
        dater: {
          type: 'DateTime',
          value: toDate(fromSeconds(1581346036)),
          creDate: complexNgsiEntityDate,
          modDate: complexNgsiEntityDate,
          md: {},
        },
      },
      location: {
        attrName: 'loco',
        coords: {
          type: 'Point',
          coordinates: [3, 2],
        },
      },
    };
    assert.deepStrictEqual(_.omit(complexEntity, '_id'), expectedComplexEntity);

    // and finally let's assess the difference to the original entity
    const stripDatesAndId = (ngsiEnt) => {
      return _.assign(
        _.omit(ngsiEnt, ['_id', 'modDate', 'creDate']),
        {attrs: _.mapValues(ngsiEnt.attrs, (attr) => {
          return _.assign(
            _.omit(attr, ['modDate', 'creDate']),
            {value: attr.type !== 'DateTime' ? attr.value : toDate(fromSeconds(toSeconds(fromDate(attr.value))))},
          );
        })},
      );
    };
    assert.deepStrictEqual(
      stripDatesAndId(complexEntity),
      stripDatesAndId(originalComplexNgsiEntity),
    );

    // and close the connections
    await closeClient();
  });

});
