/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');

const {
  assertRejects,
  ngsiStorageErrorUtils: {errorTemplates},
} = require('@ratatosk/ngsi-utils');

const {connectionManager: {stop, restart, mongoRaw}} = require('../src');
const {getMongoConfig} = require('./test-config');

describe('bulk-update-crud-flowthrough', () => {

  let api;
  before(async () => {
    api = await restart(getMongoConfig());
    await mongoRaw.assertEntitiesEmpty();
  });
  after(async () => {
    await mongoRaw.wipeEntities();
    await stop();
  });

  it('create some fresh entities (append)', async () => {
    const entities = [
      {id: 'aa', type: 'AA', derp: true},
      {id: 'bb', type: 'BB', derp: {value: 3.4, metadata: {merp: 'erp'}}},
    ];
    const actionType = 'append';
    await api.bulkUpdate({payload: {actionType, entities}});
    const docs = await mongoRaw.getEntities();
    assert.strictEqual(docs.length, 2);
    const trueDerp = _.find(docs, (doc) => (doc.orionId.id === 'aa')).attrs.derp.value;
    assert.strictEqual(trueDerp, true);
    const merpDerp = _.find(docs, (doc) => (doc.orionId.id === 'bb')).attrs.derp.md.merp.value;
    assert.strictEqual(merpDerp, 'erp');
  });

  it('create one fresh and update one old (append)', async () => {
    const entities = [
      {id: 'aa', type: 'AA', derp: false, lerp: 23}, // changes old attr, adds new attr
      {id: 'cc', type: 'CC', derp: null},
    ];
    const actionType = 'append';
    await api.bulkUpdate({payload: {actionType, entities}});
    const docs = await mongoRaw.getEntities();
    assert.strictEqual(docs.length, 3);
    const trueDerp = _.find(docs, (doc) => (doc.orionId.id === 'aa')).attrs.derp.value;
    assert.strictEqual(trueDerp, false);
    const numLerp = _.find(docs, (doc) => (doc.orionId.id === 'aa')).attrs.lerp.value;
    assert.strictEqual(numLerp, 23);
    const merpDerp = _.find(docs, (doc) => (doc.orionId.id === 'cc')).attrs.derp.value;
    assert.strictEqual(merpDerp, null);
  });

  it('create a fresh entity and add a fresh attr to one old entity (appendStrict)', async () => {
    const entities = [
      {id: 'aa', type: 'AA', sherp: 'dibbs'}, // changes old attr, adds new attr
      {id: 'dd', type: 'DD', derp: 'asdf'},
    ];
    const actionType = 'appendStrict';
    await api.bulkUpdate({payload: {actionType, entities}});
    const docs = await mongoRaw.getEntities();
    assert.strictEqual(docs.length, 4);
    const trueDerp = _.find(docs, (doc) => (doc.orionId.id === 'aa')).attrs.derp.value;
    assert.strictEqual(trueDerp, false);
    const sherp = _.find(docs, (doc) => (doc.orionId.id === 'aa')).attrs.sherp.value;
    assert.strictEqual(sherp, 'dibbs');
    const merpDerp = _.find(docs, (doc) => (doc.orionId.id === 'dd')).attrs.derp.value;
    assert.strictEqual(merpDerp, 'asdf');
  });

  it('update existing attrs (update)', async () => {
    const entities = [
      {id: 'aa', type: 'AA', derp: 'thisshouldwork'},
    ];
    const actionType = 'update';
    await api.bulkUpdate({payload: {actionType, entities}});
    const docs = await mongoRaw.getEntities();
    assert.strictEqual(docs.length, 4);
    const derp = _.find(docs, (doc) => (doc.orionId.id === 'aa')).attrs.derp.value;
    assert.strictEqual(derp, 'thisshouldwork');
    const numAttrs = _.size(_.find(docs, (doc) => (doc.orionId.id === 'aa')).attrs);
    assert.strictEqual(numAttrs, 3); // other attrs not affected
  });

  it('cannot add new attrs with update (update)', async () => {
    const entities = [
      {id: 'aa', type: 'AA', croderp: 'thiswillnotwork'},
    ];
    const payload = {actionType: 'update', entities};
    await assertRejects(api.bulkUpdate({payload}), errorTemplates.ATTR_DOES_NOT_EXIST);
  });

  it('cannot create fresh entity with update (update)', async () => {
    const entities = [
      {id: 'mmmmmuppet', type: 'AA', croderp: 'thiswillnotwork'},
    ];
    const payload = {actionType: 'update', entities};
    await assertRejects(api.bulkUpdate({payload}), errorTemplates.MATCH_NOT_FOUND);
  });

  it('replace all attrs for existing entity (replace)', async () => {
    const entities = [
      {id: 'aa', type: 'AA', derp: 'shouldbeonlysurvivingattr'},
    ];
    const actionType = 'replace';
    await api.bulkUpdate({payload: {actionType, entities}});
    const docs = await mongoRaw.getEntities();
    assert.strictEqual(docs.length, 4);
    const derp = _.find(docs, (doc) => (doc.orionId.id === 'aa')).attrs.derp.value;
    assert.strictEqual(derp, 'shouldbeonlysurvivingattr');
    const numAttrs = _.size(_.find(docs, (doc) => (doc.orionId.id === 'aa')).attrs);
    assert.strictEqual(numAttrs, 1); // other attrs not affected
  });

  it('cannot create fresh entity with replace (replace)', async () => {
    const entities = [
      {id: 'mmmmmuppet', type: 'AA', croderp: 'thiswillnotwork'},
    ];
    const payload = {actionType: 'replace', entities};
    await assertRejects(api.bulkUpdate({payload}), errorTemplates.MATCH_NOT_FOUND);
  });

  it('delete an attr from an existing entity (delete)', async () => {
    const entities = [
      {id: 'aa', type: 'AA', derp: 'thiswilldisappear'},
    ];
    const actionType = 'delete';
    await api.bulkUpdate({payload: {actionType, entities}});
    const docs = await mongoRaw.getEntities();
    assert.strictEqual(docs.length, 4);
    const numAttrs = _.size(_.find(docs, (doc) => (doc.orionId.id === 'aa')).attrs);
    assert.strictEqual(numAttrs, 0); // all gone
  });

  it('delete a non-existing attr from an existing entity fails (delete)', async () => {
    const entities = [
      {id: 'aa', type: 'AA', croderp: 'thiswillnotwork'},
    ];
    const payload = {actionType: 'delete', entities};
    await assertRejects(api.bulkUpdate({payload}), errorTemplates.ATTR_DOES_NOT_EXIST);
  });

  it('delete an existing entity (delete)', async () => {
    const entities = [
      {id: 'aa', type: 'AA'},
    ];
    const actionType = 'delete';
    await api.bulkUpdate({payload: {actionType, entities}});
    const docs = await mongoRaw.getEntities();
    assert.strictEqual(docs.length, 3);
    const aa = _.find(docs, (doc) => doc.orionId.id === 'aa');
    assert.strictEqual(aa, undefined); // was disappeared
  });

  it('delete a non-existing attr from a non-existing entity does not work (delete)', async () => {
    const entities = [
      {id: 'mmmmmuppet', type: 'AA', croderp: 'thiswillnotwork'},
    ];
    const payload = {actionType: 'delete', entities};
    await assertRejects(api.bulkUpdate({payload}), errorTemplates.MATCH_NOT_FOUND);
  });

  it('delete a non-existing attr from a non-existing entity does not work (delete)', async () => {
    const entities = [
      {id: 'mmmmmuppet', type: 'AA'},
    ];
    const payload = {actionType: 'delete', entities};
    await assertRejects(api.bulkUpdate({payload}), errorTemplates.MATCH_NOT_FOUND);
  });


});
