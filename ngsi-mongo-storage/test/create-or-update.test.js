/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

// const _ = require('lodash');
const assert = require('assert');
const {
  assertRejects,
} = require('@ratatosk/ngsi-utils');

const {connectionManager: {stop, restart, mongoRaw}} = require('../src');
const {getMongoConfig} = require('./test-config');

describe('create-or-update', () => {

  let api;
  before(async () => {
    api = await restart(getMongoConfig());
  });
  after(async () => {
    await mongoRaw.wipeEntities();
    await stop();
  });

  it('create-entity X 2', async () => {
    const entity = {
      id: 'asdf',
      type: 'blob',
      mape: {value: 23, type: 'Number'},
    };
    await api.createEntity({entity});
    const {entities} = await api.getEntities();
    assert.strictEqual(entities.length, 1);
    assert.strictEqual(entities[0].mape.value, 23);
  });

  it('should not (NORMALLY) be possible to create an entity with same id and type', async () => {
    const entity = {
      id: 'asdf',
      type: 'blob',
    };
    const err = 'Cannot create entity as it already exists for the given {id, type, servicePath} combination';
    await assertRejects(api.createEntity({entity}), err);
  });

  it('with upsert option, the device instead gets updated', async () => {
    const entity = {
      id: 'asdf',
      type: 'blob',
      slerp: {value: 'slerp', type: 'Text'},
    };
    const upsert = true;
    await api.createEntity({entity, upsert});
    const {entities} = await api.getEntities();
    assert.strictEqual(entities.length, 1);
    assert.strictEqual(entities[0].slerp.value, 'slerp');
    assert.strictEqual(entities[0].mape.value, 23);
  });

});
