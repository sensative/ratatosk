/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const {ObjectId} = require('mongodb');

const {
  ORION_ENTITITIES_DB_COLLECTION,
} = require('@ratatosk/ngsi-constants');

const {
  assertRejects,
  ngsiEntityFormatter: {formatEntity},
  ngsiSql: {queryParser: {parseQueryString}},
} = require('@ratatosk/ngsi-utils');

const {
  stop: stopTokens,
  restart: restartTokens,
} = require('./access-token-api');
const {connectionManager: {stop, restart, mongoRaw}} = require('../../src');
const {getMongoConfig} = require('../test-config');

const templates = [
  {name: 'joe', ber: 2, num: 4},
  {name: 'sandy', num: 5},
  {name: 'skippers', num: 200},
];

const users = {
  joe: new ObjectId().toString(),
  sandy: new ObjectId().toString(),
};

const ACCESS_TYPES = {
  ADMIN: 'admin',
  WRITE: 'write',
  READ: 'read',
};

const createAccessStages = (user, accessType) => {
  const stages = [
    {
      $lookup: {
        from: 'tokens',
        let: {fiwareId: '$_id'},
        pipeline: [{
          $match: {
            $expr: {
              $and: [
                {$eq: ['$entityId', '$$fiwareId']},
                {$eq: ['$subjectId', new ObjectId(user)]},
                {$eq: ['$accessType', accessType]},
              ],
            },
          },
        }],
        as: 'token',
      },
    },
    {
      $unwind: {path: '$token'},
    },
  ];
  return stages;
};

const createPipelineGenerator = (tokensApi, user, accessType) => {
  const accessStages = createAccessStages(user, accessType);
  const generator = ({match, sort, paging}) => {
    const collection = tokensApi.getCollection(ORION_ENTITITIES_DB_COLLECTION);
    const pipeline = _.concat(
      match,
      accessStages,
      sort,
      paging,
    );
    const query = collection.aggregate(pipeline);
    return query;
  };
  return generator;
};

describe('access-token-pipeline', () => {

  let api;
  let tokensApi;
  before(async () => {
    api = await restart(getMongoConfig());
    // create the entities
    const groups = {};
    await Promise.all(_.map(templates, async (template) => {
      const creators = _.times(template.num, async (index) => {
        const entity = formatEntity({
          id: `${template.name}_${index}`,
          index,
          key: template.name,
          ..._.omit(template, 'num'),
        });
        const res = await api.createEntity({entity});
        return res.modifiedEntity;
      });
      groups[template.name] = await Promise.all(creators);
    }));
    // and fill in the tokens
    tokensApi = await restartTokens(getMongoConfig());
    await Promise.all(_.map(groups, async (entities, groupKey) => {
      if (groupKey === 'joe') {
        await Promise.all(_.map(entities, async (entity) => {
          await tokensApi.createToken({
            entityId: new ObjectId(entity.builtinAttrs.fiwareId.value),
            subjectId: new ObjectId(users.joe),
            accessType: ACCESS_TYPES.ADMIN,
          });
        }));
      }
      if (groupKey === 'sandy') {
        await Promise.all(_.map(entities, async (entity) => {
          await tokensApi.createToken({
            entityId: new ObjectId(entity.builtinAttrs.fiwareId.value),
            subjectId: new ObjectId(users.sandy),
            accessType: ACCESS_TYPES.ADMIN,
          });
        }));
      }
      if (groupKey === 'skippers') {
        await Promise.all(_.map(entities, async (entity, index) => {
          if (index % 2 === 0) {
            await tokensApi.createToken({
              entityId: new ObjectId(entity.builtinAttrs.fiwareId.value),
              subjectId: new ObjectId(users.sandy),
              accessType: ACCESS_TYPES.READ,
            });
          }
        }));
      }
    }));
    const numTokens = await tokensApi.countTokens();
    assert.strictEqual(numTokens, 109);
    api = await restart(getMongoConfig());
  });

  after(async () => {
    // api = await restart(getMongoConfig());
    await mongoRaw.wipeEntities();
    await stop();
    // and wipe the tokens
    tokensApi = await restartTokens(getMongoConfig());
    // await tokensApi.wipeTokens();
    await stopTokens();
  });

  describe('without custom', () => {

    it('get all entities with index < 2', async () => {
      const params = {
        q: parseQueryString('index<2'),
        count: true,
      };
      const {count, entities} = await api.getEntities({params});
      assert.strictEqual(count, 6);
      assert.strictEqual(entities.length, 6);
    });

  });

  describe('custom pipeline injection', () => {

    it('get all entities with index < 2 that joe has admin access to', async () => {
      const params = {
        q: parseQueryString('index<2'),
        count: true,
      };
      const accessStages = createAccessStages(users.joe, 'admin');
      const {count, entities} = await api.getEntities({params, custom: accessStages});
      assert.strictEqual(count, 2);
      assert.strictEqual(entities.length, 2);
    });

    it('find "joe_0" with joe as admin (should find it), (should not find with read)', async () => {
      const entityId = 'joe_0';
      const accessStages = createAccessStages(users.joe, 'admin');
      const entity = await api.findEntity({entityId, custom: accessStages});
      assert.strictEqual(entity.id, 'joe_0');
      // try again with read access (should fail)
      const readStages = createAccessStages(users.joe, 'read');
      const err = 'No matching entity to the query was found';
      await assertRejects(api.findEntity({entityId, custom: readStages}), err);
    });

    it('check in to how paging works together with access stuff', async () => {
      const params = {
        count: true,
        limit: 10,
        offset: 10,
        orderBy: [{name: 'index', isNegatory: false}],
      };
      const accessStages = createAccessStages(users.sandy, 'read');
      const {count, entities} = await api.getEntities({params, custom: accessStages});
      assert.strictEqual(count, 100);
      assert.strictEqual(entities.length, 10);
      const indices = _.map(entities, (ent) => ent.index.value);
      const expected = [20, 22, 24, 26, 28, 30, 32, 34, 36, 38];
      assert.deepStrictEqual(indices, expected);
    });

  });

  describe('custom pipeline generator function', () => {

    it('get all entities with index < 2 that joe has admin access to', async () => {
      const params = {
        q: parseQueryString('index<2'),
        count: true,
      };
      const queryGenerator = createPipelineGenerator(tokensApi, users.joe, 'admin');
      const {count, entities} = await api.getEntities({params, custom: queryGenerator});
      assert.strictEqual(count, null); // COUNT IS NOT IMPLEMENTED FOR THIS GUY
      assert.strictEqual(entities.length, 2);
    });

    it('find "joe_0" with joe as admin (should find it), (should not find with read)', async () => {
      const entityId = 'joe_0';
      const queryGenerator = createPipelineGenerator(tokensApi, users.joe, 'admin');
      const entity = await api.findEntity({entityId, custom: queryGenerator});
      assert.strictEqual(entity.id, 'joe_0');
      // try again with read access (should fail)
      const readStages = createAccessStages(users.joe, 'read');
      const err = 'No matching entity to the query was found';
      await assertRejects(api.findEntity({entityId, custom: readStages}), err);
    });

    it('check in to how paging works together with access stuff', async () => {
      const params = {
        count: true,
        limit: 10,
        offset: 10,
        orderBy: [{name: 'index', isNegatory: false}],
      };
      const queryGenerator = createPipelineGenerator(tokensApi, users.sandy, 'read');
      const {count, entities} = await api.getEntities({params, custom: queryGenerator});
      assert.strictEqual(count, null); // COUNT IS NOT IMPLEMENTED FOR THIS GUY
      assert.strictEqual(entities.length, 10);
      const indices = _.map(entities, (ent) => ent.index.value);
      const expected = [20, 22, 24, 26, 28, 30, 32, 34, 36, 38];
      assert.deepStrictEqual(indices, expected);
    });

  });

});
