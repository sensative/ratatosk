/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const {MongoClient} = require('mongodb');

// MUTABLE STATE!!
const state = {
  current: null,
  isStopping: null,
  isStarting: null,
};

// this is just used as a sanity check
const assertValidState = () => {
  // at most 1 can be non-null at a time
  const hasValidSize = _.size(_.compact(state)) < 2;
  if (!hasValidSize) {
    throw new Error('DevErr: invalid mongo connection state');
  }
};

// local utility function
const connect = async (url, opts) => {
  const options = _.assign(
    {
      useNewUrlParser: true, // due to deprecation warning
      useUnifiedTopology: true, // due to deprecation warning
    },
    opts,
  );
  return new Promise((resolve, reject) => {
    // Use connect method to connect to the server
    MongoClient.connect(url, options, (err, cli) => {
      if (err) {
        reject(err);
      } else if (!cli) {
        reject(new Error('mongo client is missing'));
      } else {
        resolve(cli);
      }
    });
  });
};

const ACCESS_TOKEN_COLLECTION = 'tokens';

// local utility function
const createApi = async (db, isAlive) => {

  // check if we need to create a new collection
  const tokensCollection = db.collection(ACCESS_TOKEN_COLLECTION);

  const endpoints = {
    createToken,
    countTokens,
    getTokens,
    wipeTokens,
  };

  const api = _.mapValues(endpoints, (endpoint) => {
    // we want to unwrap the endpoint just once
    const unwrapped = endpoint(tokensCollection);
    // and return a function that evaluates each time (including an isAlive check)
    return async (dbArg) => isAlive()
      .then(() => unwrapped(dbArg));
  });
  // for convenience
  api.getCollection = (name) => db.collection(name);

  // and done
  return api;
};

// EXPORT - closes any existing (or about-to-exist) mongo connection
const stop = async () => {
  assertValidState();
  if (state.isStopping) {
    return state.isStopping;
  }
  if (state.isStarting) {
    return state.isStarting
      .then(stop);
  }
  if (!state.current) {
    return; // already stopped
  }
  // and stop the client
  const client = state.current.client;
  state.current = null;
  state.isStopping = client.close()
    .then(() => {
      state.isStopping = null;
      assertValidState();
    });
  // and done
  assertValidState();
  return state.isStopping;
};


// EXPORT - creates a new connection if one does not exist
const start = async ({url, options, dbName, globalRegistrationAttrs = []}) => {
  assertValidState();
  if (state.isStopping) {
    return state.isStopping
      .then(() => start({url, options, dbName, globalRegistrationAttrs}));
  }
  if (state.isStarting) {
    return state.isStarting;
  }
  if (state.current) {
    return state.current.api;
  }
  // start the connection and return the api
  state.isStarting = connect(url, options, dbName)
    .then((client) => {
      const connErr = () => new Error('Connection has been closed');
      const isAlive = async () => {
        if (client !== _.get(state, 'current.client')) {
          throw connErr();
        }
      };
      const db = client.db(dbName);
      const api = createApi(db, isAlive);
      state.current = {client, db, api};
      state.isStarting = null;
      assertValidState();
      return state.current.api;
    });
  // and done
  assertValidState();
  return state.isStarting;
};

// EXPORT - stops any existing mongo connection, then starts a new one
const restart = async ({url, options, dbName, globalRegistrationAttrs}) => {
  await stop();
  return start({url, options, dbName, globalRegistrationAttrs});
};

// the endpoints

const createToken = (collection) => async (token) => {
  const res = await collection.insertOne(token);
  return res;
};

const getTokens = (collection) => async (filter) => {
  const docs = await collection.find(filter).toArray();
  return docs;
};

const wipeTokens = (collection) => async () => {
  const res = collection.deleteMany();
  return res;
};

const countTokens = (collection) => async (filter) => {
  const num = await collection.countDocuments(filter);
  return num;
};


module.exports = {
  start,
  stop,
  restart,
};
