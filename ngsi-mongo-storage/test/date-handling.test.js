/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const assert = require('assert');

const {
  ngsiDateTimeUtils: {fromDate},
} = require('@ratatosk/ngsi-utils');

const {connectionManager: {stop, restart, mongoRaw}} = require('../src');
const {getMongoConfig} = require('./test-config');

describe('date-handling', () => {

  let api;
  before(async () => {
    api = await restart(getMongoConfig());
  });
  after(async () => {
    await mongoRaw.wipeEntities();
    await stop();
  });

  describe('create an entity with custom DateTime attr', () => {

    let nowish;
    before(() => {
      nowish = new Date();
    });
    after(() => mongoRaw.wipeEntities());

    it('should be possible to save an entity with a custom date-time value', async () => {
      const entity = {
        id: 'a',
        type: 'b',
        cee: {
          type: 'DateTime',
          value: fromDate(nowish),
        },
      };
      await api.createEntity({entity});
    });

    it('the database should be storing the date as a date', async () => {
      const mongoDocs = await mongoRaw.getEntities();
      assert.strictEqual(mongoDocs.length, 1);
      const mongoAttr = mongoDocs[0].attrs.cee;
      assert.deepStrictEqual(mongoAttr.value, nowish);
    });

    it('should be possible to retrieve the entity value', async () => {
      const value = await api.findEntityAttrValue({entityId: 'a', attrName: 'cee'});
      assert.strictEqual(value, fromDate(nowish));
    });

  });


});
