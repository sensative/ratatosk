/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const assert = require('assert');
const {
  assertRejects,
} = require('@ratatosk/ngsi-utils');
const {
  ORION_ENTITITIES_DB_COLLECTION,
} = require('@ratatosk/ngsi-constants');

const {
  connectionManager: {stop, restart, mongoRaw, getCurrentDatabase},
} = require('../src');
const {getMongoConfig} = require('./test-config');

// ///////
//
// This test-suite goes through a flow of creating unique indexes, then using
// those indexes to enforce uniqueness, and finally removing the indexes
//
// ///////

describe('entities-custom-filtering', () => {

  let api;
  let entitiesCollection;
  let numInitIndexes;
  before(async () => {
    api = await restart(getMongoConfig());
    const db = getCurrentDatabase();
    entitiesCollection = await db.collection(ORION_ENTITITIES_DB_COLLECTION);
    assert.strictEqual(entitiesCollection.collectionName, 'entities');
    const indexes = await entitiesCollection.indexes();
    numInitIndexes = indexes.length;
    // create the new index
    await entitiesCollection.createIndex(
      {
        'attrs.asdf.value': 1,
      },
      {
        name: 'customIndex',
        unique: true,
        partialFilterExpression: {'attrs.asdf.value': {$exists: true}},
      },
    );
  });

  after(async () => {
    // and remove the custom index
    await entitiesCollection.dropIndex('customIndex');
    // and check
    const indexes = await entitiesCollection.indexes();
    assert.strictEqual(indexes.length, numInitIndexes);
    await mongoRaw.wipeEntities();
    await stop();
  });

  it('temp filtering test', () => 0);

  it('creating 2 entities that break custom unique requirement fails', async () => {
    // first entity should work
    const entity = {
      id: 'iggy',
      type: 'pop',
      asdf: {type: 'Number', value: 1.32},
    };
    await api.createEntity({entity});
    const entDocs = await mongoRaw.getEntities();
    assert.strictEqual(entDocs[0].attrs.asdf.value, 1.32);
    // second entity with 1.32 should NOT work
    const ent2 = {
      id: 'aaa',
      type: 'bbb',
      asdf: {type: 'Number', value: 1.32},
    };
    const message = /E11000 duplicate key error/;
    const err = await assertRejects(api.createEntity({entity: ent2}));
    assert(err.message.match(message));
  });

});
