/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');

const {
  ngsiEntityFormatter: {formatEntity},
  ngsiDateTimeUtils: {hasValidDateTimeFormat},
} = require('@ratatosk/ngsi-utils');

const {connectionManager: {stop, restart, mongoRaw}} = require('../src');
const {getMongoConfig} = require('./test-config');

describe('attrs-projection', () => {

  let api;
  before(async () => {
    api = await restart(getMongoConfig());
  });
  after(async () => {
    await mongoRaw.wipeEntities();
    await stop();
  });

  describe('simple check of projections functionality', () => {

    before(async () => {
      const entity = formatEntity({
        id: 'a',
        type: 'b',
        cee: 'tttt',
        dee: 234,
        fee: '234',
      });
      await api.createEntity({entity});
    });

    it('default projection (i.e. none)', async () => {
      const ent = await api.findEntity({entityId: 'a'});
      const expected = {
        id: 'a',
        type: 'b',
        cee: {value: 'tttt', type: 'Text', metadata: {}},
        dee: {value: 234, type: 'Number', metadata: {}},
        fee: {value: '234', type: 'Text', metadata: {}},
      };
      assert.deepStrictEqual(ent, expected);
    });

    it('using only standard, user-defined attrs', async () => {
      const ent = await api.findEntity({entityId: 'a', params: {attrs: ['cee', 'dee']}});
      const expected = {
        id: 'a',
        type: 'b',
        cee: {value: 'tttt', type: 'Text', metadata: {}},
        dee: {value: 234, type: 'Number', metadata: {}},
      };
      assert.deepStrictEqual(ent, expected);
    });

    it('using only wildcard (*) (should be same as default)', async () => {
      const ent = await api.findEntity({entityId: 'a', params: {attrs: ['*']}});
      const expected = {
        id: 'a',
        type: 'b',
        cee: {value: 'tttt', type: 'Text', metadata: {}},
        dee: {value: 234, type: 'Number', metadata: {}},
        fee: {value: '234', type: 'Text', metadata: {}},
      };
      assert.deepStrictEqual(ent, expected);
    });

    it('should be possible to get the builtin dates', async () => {
      const params = {attrs: ['dateCreated', 'dateModified']};
      const ent = await api.findEntity({entityId: 'a', params});
      const now = _.get(ent, 'dateCreated.value');
      assert(hasValidDateTimeFormat(now));
      const expected = {
        id: 'a',
        type: 'b',
        dateCreated: {type: 'DateTime', value: now},
        dateModified: {type: 'DateTime', value: now},
      };
      assert.deepStrictEqual(ent, expected);
    });

    it('combining builtin with standard attr', async () => {
      const params = {attrs: ['dateCreated', 'fee']};
      const ent = await api.findEntity({entityId: 'a', params});
      const now = _.get(ent, 'dateCreated.value');
      assert(hasValidDateTimeFormat(now));
      const expected = {
        id: 'a',
        type: 'b',
        dateCreated: {type: 'DateTime', value: now},
        fee: {type: 'Text', value: '234', metadata: {}},
      };
      assert.deepStrictEqual(ent, expected);
    });

    it('combining builtin with wildcard (and standard attrs should not matter)', async () => {
      const params = {attrs: ['dateCreated', '*']};
      const ent = await api.findEntity({entityId: 'a', params});
      const now = _.get(ent, 'dateCreated.value');
      assert(hasValidDateTimeFormat(now));
      const expected = {
        id: 'a',
        type: 'b',
        dateCreated: {type: 'DateTime', value: now},
        cee: {value: 'tttt', type: 'Text', metadata: {}},
        dee: {value: 234, type: 'Number', metadata: {}},
        fee: {value: '234', type: 'Text', metadata: {}},
      };
      assert.deepStrictEqual(ent, expected);
      const paramsExtra = {attrs: ['dateCreated', '*', 'fee']};
      const entExtra = await api.findEntity({entityId: 'a', params: paramsExtra});
      assert.deepStrictEqual(ent, entExtra);
    });

    it('extended representation, together with a builtin', async () => {
      const params = {attrs: ['dateCreated', 'dee'], representationType: 'extended'};
      const ent = await api.findEntity({entityId: 'a', params});
      const now = ent.builtinAttrs.dateCreated.value;
      const stamp = {type: 'DateTime', value: now};
      assert(hasValidDateTimeFormat(now));
      const expected = {
        id: 'a',
        type: 'b',
        servicePath: '/',
        builtinAttrs: {
          dateCreated: stamp,
        },
        attrs: {
          dee: {
            value: 234,
            type: 'Number',
            metadata: {},
            builtinMetadata: {dateCreated: stamp, dateModified: stamp},
          },
        },
      };
      assert.deepStrictEqual(ent, expected);
    });

    it('peek representation, together with a builtin', async () => {
      const params = {attrs: ['dateCreated', 'dee'], representationType: 'peek'};
      const ent = await api.findEntity({entityId: 'a', params});
      const expected = {
        id: 'a',
        type: 'b',
        servicePath: '/',
      };
      assert(_.isString(ent.fiwareId));
      assert.deepStrictEqual(_.omit(ent, 'fiwareId'), expected);
    });

  });

});
