/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const delay = require('delay');
const serialPromise = require('promise-serial-exec');

const {
  ngsiEntityFormatter: {formatEntity},
} = require('@ratatosk/ngsi-utils');

const {connectionManager: {stop, restart, mongoRaw}} = require('../src');
const {getMongoConfig} = require('./test-config');

const entities = [
  // for numbers
  {id: 'aa', type: 'diggy', arp: 0},
  {id: 'dd', type: 'diggy', arp: 2},
  {id: 'cc', type: 'diggy', arp: 1},
  {id: 'cc', type: 'aggy'},
  {id: 'cc', type: 'zaggy'},
];

describe('order-by.test', () => {

  let api;
  before(async () => {
    api = await restart(getMongoConfig());
  });
  after(async () => {
    await mongoRaw.wipeEntities();
    await stop();
  });

  describe('simple check of projections functionality', () => {

    before(async () => {
      await serialPromise(_.map(entities, (entity) => async () => {
        await delay(10); // some delay between creates so dates are different
        await api.createEntity({entity: formatEntity(entity)});
      }));
    });

    it('unordered, (default id)', async () => {
      const params = {type: ['diggy']};
      const {entities: ents} = await api.getEntities({params});
      assert.strictEqual(ents.length, 3);
      const expected = ['aa', 'cc', 'dd'];
      const names = _.map(ents, (ent) => ent.id);
      assert.deepStrictEqual(names, expected);
    });

    it('ordered by numbers (orderBy = "arp")', async () => {
      const params = {type: ['diggy'], orderBy: [{name: 'arp', isNegatory: false}]};
      const {entities: ents} = await api.getEntities({params});
      assert.strictEqual(ents.length, 3);
      const expected = ['aa', 'cc', 'dd'];
      const names = _.map(ents, (ent) => ent.id);
      assert.deepStrictEqual(names, expected);
    });

    it('ordered by id', async () => {
      const params = {type: ['diggy'], orderBy: [{name: 'id', isNegatory: true}]};
      const {entities: ents} = await api.getEntities({params});
      assert.strictEqual(ents.length, 3);
      const expected = ['dd', 'cc', 'aa'];
      const names = _.map(ents, (ent) => ent.id);
      assert.deepStrictEqual(names, expected);
    });

    it('ordered by type', async () => {
      const params = {id: ['cc'], orderBy: [{name: 'type', isNegatory: false}]};
      const {entities: ents} = await api.getEntities({params});
      assert.strictEqual(ents.length, 3);
      const expected = ['aggy', 'diggy', 'zaggy'];
      const types = _.map(ents, (ent) => ent.type);
      assert.deepStrictEqual(types, expected);
    });

    it('ordered by dateCreated', async () => {
      const params = {type: ['diggy'], orderBy: [{name: 'dateCreated', isNegatory: false}]};
      const {entities: ents} = await api.getEntities({params});
      assert.strictEqual(ents.length, 3);
      const expected = ['aa', 'dd', 'cc'];
      const types = _.map(ents, (ent) => ent.id);
      assert.deepStrictEqual(types, expected);
    });

    it('ordered by dateModified', async () => {
      const params = {type: ['diggy'], orderBy: [{name: 'dateModified', isNegatory: true}]};
      const {entities: ents} = await api.getEntities({params});
      assert.strictEqual(ents.length, 3);
      const expected = ['cc', 'dd', 'aa'];
      const types = _.map(ents, (ent) => ent.id);
      assert.deepStrictEqual(types, expected);
    });

  });

});
