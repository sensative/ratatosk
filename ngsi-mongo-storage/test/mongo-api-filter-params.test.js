/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const {
  assertRejects,
  ngsiEntityFormatter: {formatEntity},
  ngsiSql: {queryParser: {parseQueryString}},
} = require('@ratatosk/ngsi-utils');

const {connectionManager: {stop, restart, mongoRaw}} = require('../src');
const {getMongoConfig} = require('./test-config');

describe('mongo-api-filter-params', () => {

  let api;
  before(async () => {
    api = await restart(getMongoConfig());
  });
  after(async () => {
    await mongoRaw.wipeEntities();
    await stop();
  });

  describe('some basic tests', () => {

    afterEach(() => mongoRaw.wipeEntities());

    it('should be able to create a basic entity', async () => {
      const entity = {
        id: 'iggy',
        type: 'pop',
        johnny: {type: 'Number', value: 2, metadata: {begood: {value: false, type: 'Boolean'}}},
      };
      await api.createEntity({entity});
      const mongoDocs = await mongoRaw.getEntities();
      // const now = new Date();
      // const now = Math.floor(Date.now() / 1000);
      assert.strictEqual(mongoDocs.length, 1, 'should just be the one');
      const now = _.get(mongoDocs, '0.creDate');
      assert(_.isDate(now));
      const expected = {
        orionId: {id: 'iggy', type: 'pop', servicePath: '/'},
        attrs: {
          johnny: {
            type: 'Number',
            value: 2,
            md: {
              begood: {type: 'Boolean', value: false},
            },
            creDate: now,
            modDate: now,
          },
        },
        creDate: now,
        modDate: now,
      };
      assert.deepStrictEqual(_.omit(mongoDocs[0], '_id'), expected);
    });

    it('can differentiate by type', async () => {
      const entA = formatEntity({id: 'a', type: 'aaaz'});
      await api.createEntity({entity: entA});
      const entB = formatEntity({id: 'a', type: 'bb'});
      await api.createEntity({entity: entB});
      const {entities: ents} = await api.getEntities({params: {type: ['bb']}});
      assert.strictEqual(ents.length, 1);
      assert.deepStrictEqual(ents[0], entB);
    });

    it('servicePaths must be a non-empty array if included', async () => {
      const entity = formatEntity({id: 'a', type: 'b'});
      const message = 'validateMultipleServicePathsStrict argument must be an array with at least 1 servicePath';
      await assertRejects(api.createEntity({entity, servicePaths: '/a'}), message);
    });

    it('can differentiate by servicePath', async () => {
      const entity = formatEntity({id: 'a', type: 'b'});
      await api.createEntity({entity, servicePaths: ['/r']});
      await api.createEntity({entity, servicePaths: ['/s']});
      const {entities: ents} = await api.getEntities({servicePaths: ['/r']});
      assert.strictEqual(ents.length, 1);
      assert.deepStrictEqual(ents[0], entity);
    });

    it('can delete (no id collision)', async () => {
      await api.createEntity({entity: {id: 'a', type: 'b'}});
      await api.createEntity({entity: {id: 'c', type: 'b'}});
      const numInit = await mongoRaw.countEntities();
      assert.strictEqual(numInit, 2);
      await api.deleteEntity({entityId: 'a'});
      const numFin = await mongoRaw.countEntities();
      assert.strictEqual(numFin, 1);
    });

  });

  describe('idPattern and typePattern', () => {

    afterEach(() => mongoRaw.wipeEntities());

    it('find by idPattern', async () => {
      await api.createEntity({entity: {id: 'a', type: 'A'}});
      await api.createEntity({entity: {id: 'aa', type: 'A'}});
      await api.createEntity({entity: {id: 'aaa', type: 'A'}});
      await api.createEntity({entity: {id: 'aaaa', type: 'A'}});
      await api.createEntity({entity: {id: 'aaaaa', type: 'A'}});
      const {entities: ents1} = await api.getEntities({params: {idPattern: 'a'}});
      assert.strictEqual(ents1.length, 5);
      const {entities: ents2} = await api.getEntities({params: {idPattern: 'aa'}});
      assert.strictEqual(ents2.length, 4);
      const {entities: ents3} = await api.getEntities({params: {idPattern: 'aaa'}});
      assert.strictEqual(ents3.length, 3);
      const {entities: ents4} = await api.getEntities({params: {idPattern: 'aaaa'}});
      assert.strictEqual(ents4.length, 2);
      const {entities: ents5} = await api.getEntities({params: {idPattern: 'aaaaa'}});
      assert.strictEqual(ents5.length, 1);
    });

    it('find by typePattern', async () => {
      await api.createEntity({entity: {id: 'a', type: 'A'}});
      await api.createEntity({entity: {id: 'a', type: 'AA'}});
      await api.createEntity({entity: {id: 'a', type: 'AAA'}});
      await api.createEntity({entity: {id: 'a', type: 'AAAA'}});
      await api.createEntity({entity: {id: 'a', type: 'AAAAA'}});
      const {entities: ents1} = await api.getEntities({params: {typePattern: 'A'}});
      assert.strictEqual(ents1.length, 5);
      const {entities: ents2} = await api.getEntities({params: {typePattern: 'AA'}});
      assert.strictEqual(ents2.length, 4);
      const {entities: ents3} = await api.getEntities({params: {typePattern: 'AAA'}});
      assert.strictEqual(ents3.length, 3);
      const {entities: ents4} = await api.getEntities({params: {typePattern: 'AAAA'}});
      assert.strictEqual(ents4.length, 2);
      const {entities: ents5} = await api.getEntities({params: {typePattern: 'AAAAA'}});
      assert.strictEqual(ents5.length, 1);
    });

  });

  describe('simplest case sql: test comparators', () => {

    afterEach(() => mongoRaw.wipeEntities());

    it('can differentiate using $gt', async () => {
      await api.createEntity({entity: formatEntity({id: 'a', type: 'B', aki: 1, boop: 'prrp'})});
      await api.createEntity({entity: formatEntity({id: 'b', type: 'B', aki: 5, boop: 'aaaz'})});
      await api.createEntity({entity: formatEntity({id: 'c', type: 'B', boop: null})});
      const query1 = parseQueryString('aki>1');
      const {entities: ents1} = await api.getEntities({params: {q: query1}});
      assert.strictEqual(ents1.length, 1);
      assert.strictEqual(ents1[0].aki.value, 5);
      const query2 = parseQueryString('boop>beep');
      const {entities: ents2} = await api.getEntities({params: {q: query2}});
      assert.strictEqual(ents2.length, 1);
      assert.strictEqual(ents2[0].boop.value, 'prrp');
    });

    it('can differentiate using $gte', async () => {
      await api.createEntity({entity: formatEntity({id: 'a', type: 'B', aki: 1, boop: 'prrp'})});
      await api.createEntity({entity: formatEntity({id: 'b', type: 'B', aki: 5, boop: 'aaaz'})});
      await api.createEntity({entity: formatEntity({id: 'c', type: 'B', boop: null})});
      const query1 = parseQueryString('aki>=5');
      const {entities: ents1} = await api.getEntities({params: {q: query1}});
      assert.strictEqual(ents1.length, 1);
      assert.strictEqual(ents1[0].aki.value, 5);
      const query2 = parseQueryString('boop>=pnnp');
      const {entities: ents2} = await api.getEntities({params: {q: query2}});
      assert.strictEqual(ents2.length, 1);
      assert.strictEqual(ents2[0].boop.value, 'prrp');
      const query3 = parseQueryString('boop>=prrp');
      const {entities: ents3} = await api.getEntities({params: {q: query3}});
      assert.strictEqual(ents3.length, 1);
      assert.strictEqual(ents3[0].boop.value, 'prrp');
    });

    it('can differentiate using $lt', async () => {
      await api.createEntity({entity: formatEntity({id: 'a', type: 'B', aki: 1, boop: 'prrp'})});
      await api.createEntity({entity: formatEntity({id: 'b', type: 'B', aki: 5, boop: 'aaaz'})});
      await api.createEntity({entity: formatEntity({id: 'c', type: 'B', boop: null})});
      const query1 = parseQueryString('aki<5');
      const {entities: ents1} = await api.getEntities({params: {q: query1}});
      assert.strictEqual(ents1.length, 1);
      assert.strictEqual(ents1[0].aki.value, 1);
      const query2 = parseQueryString('boop<oops');
      const {entities: ents2} = await api.getEntities({params: {q: query2}});
      assert.strictEqual(ents2.length, 1);
      assert.strictEqual(ents2[0].boop.value, 'aaaz');
    });

    it('can differentiate using $lte', async () => {
      await api.createEntity({entity: formatEntity({id: 'a', type: 'B', aki: 1, boop: 'prrp'})});
      await api.createEntity({entity: formatEntity({id: 'b', type: 'B', aki: 5, boop: 'aaaz'})});
      await api.createEntity({entity: formatEntity({id: 'c', type: 'B', boop: null})});
      const query1 = parseQueryString('aki<=1');
      const {entities: ents1} = await api.getEntities({params: {q: query1}});
      assert.strictEqual(ents1.length, 1);
      assert.strictEqual(ents1[0].aki.value, 1);
      const query2 = parseQueryString('boop<=pnnp');
      const {entities: ents2} = await api.getEntities({params: {q: query2}});
      assert.strictEqual(ents2.length, 1);
      assert.strictEqual(ents2[0].boop.value, 'aaaz');
      const query3 = parseQueryString('boop<=aaaz');
      const {entities: ents3} = await api.getEntities({params: {q: query3}});
      assert.strictEqual(ents3.length, 1);
      assert.strictEqual(ents3[0].boop.value, 'aaaz');
    });

    it('can differentiate using == (and :)', async () => {
      await api.createEntity({entity: formatEntity({id: 'a', type: 'B', aki: 1, boop: 'prrp'})});
      await api.createEntity({entity: formatEntity({id: 'b', type: 'B', aki: 5, boop: 'aaaz'})});
      await api.createEntity({entity: formatEntity({id: 'c', type: 'B', boop: null})});
      const query1 = parseQueryString('boop==null');
      const {entities: ents1} = await api.getEntities({params: {q: query1}});
      assert.strictEqual(ents1.length, 1);
      assert.strictEqual(ents1[0].boop.value, null);
      const query2 = parseQueryString('boop:null');
      const {entities: ents2} = await api.getEntities({params: {q: query2}});
      assert.strictEqual(ents2.length, 1);
      assert.strictEqual(ents2[0].boop.value, null);
      const query3 = parseQueryString('boop:prrp');
      const {entities: ents3} = await api.getEntities({params: {q: query3}});
      assert.strictEqual(ents3.length, 1);
      assert.strictEqual(ents3[0].boop.value, 'prrp');
      const query4 = parseQueryString('aki==5');
      const {entities: ents4} = await api.getEntities({params: {q: query4}});
      assert.strictEqual(ents4.length, 1);
      assert.strictEqual(ents4[0].aki.value, 5);
    });

    it('can differentiate using !=', async () => {
      await api.createEntity({entity: formatEntity({id: 'a', type: 'B', aki: 1, boop: 'prrp'})});
      await api.createEntity({entity: formatEntity({id: 'b', type: 'B', aki: 5, boop: 'aaaz'})});
      await api.createEntity({entity: formatEntity({id: 'c', type: 'B', aki: 1, boop: null})});
      const query1 = parseQueryString('aki!=1');
      const {entities: ents1} = await api.getEntities({params: {q: query1}});
      assert.strictEqual(ents1.length, 1);
      assert.strictEqual(ents1[0].aki.value, 5);
      const query2 = parseQueryString('boop!=null');
      const {entities: ents2} = await api.getEntities({params: {q: query2}});
      assert.strictEqual(ents2.length, 2, 'there should be 2 non-nulls');
    });

    it('can differentiate using ~=', async () => {
      await api.createEntity({entity: formatEntity({id: 'a', type: 'B', boop: 'prrp'})});
      await api.createEntity({entity: formatEntity({id: 'b', type: 'B', boop: 'aaaz'})});
      await api.createEntity({entity: formatEntity({id: 'c', type: 'B', boop: 'aa'})});
      const query1 = parseQueryString('boop~=aaaz');
      const {entities: ents1} = await api.getEntities({params: {q: query1}});
      assert.strictEqual(ents1.length, 1);
      assert.strictEqual(ents1[0].boop.value, 'aaaz');
      const query2 = parseQueryString('boop~=aa');
      const {entities: ents2} = await api.getEntities({params: {q: query2}});
      assert.strictEqual(ents2.length, 2, 'there should be 2 non-nulls');
    });

  });

  describe('compound sql', () => {

    afterEach(() => mongoRaw.wipeEntities());

    it('can find using q query', async () => {
      await api.createEntity({entity: formatEntity({id: 'a', type: 'B', obi: 2, wan: 2})});
      await api.createEntity({entity: formatEntity({id: 'b', type: 'B', obi: 4, wan: 2})});
      await api.createEntity({entity: formatEntity({id: 'c', type: 'B', obi: 2, wan: 4})});
      await api.createEntity({entity: formatEntity({id: 'd', type: 'B', obi: 4, wan: 4})});
      const query = parseQueryString('obi>3;wan>=4');
      const {entities: ents} = await api.getEntities({params: {q: query}});
      assert.strictEqual(ents.length, 1);
      assert.strictEqual(ents[0].id, 'd');
    });

  });

  describe('query explain (the mongodb winning strategy and other info exposed)', () => {

    afterEach(() => mongoRaw.wipeEntities());

    it('just opt in by using the metaOpts (optional) 2nd param for getEntities', async () => {
      await api.createEntity({entity: formatEntity({id: 'a', type: 'B', obi: 2, wan: 2})});
      await api.createEntity({entity: formatEntity({id: 'b', type: 'B', obi: 4, wan: 2})});
      await api.createEntity({entity: formatEntity({id: 'c', type: 'B', obi: 2, wan: 4})});
      await api.createEntity({entity: formatEntity({id: 'd', type: 'B', obi: 4, wan: 4})});
      const query = parseQueryString('obi>3;wan>=4');
      const explanation = await api.explain({params: {q: query}});
      const expectedKeys = ['stages', 'serverInfo', 'ok'].sort();
      assert.deepStrictEqual(_.keys(explanation).sort(), expectedKeys);
    });

  });

});
