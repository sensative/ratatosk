/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const assert = require('assert');
const connectionManager = require('../src/connection-manager');
const {generateApi} = require('../src/api-generator');

const {MongoClient} = require('mongodb');
const {getMongoConfig} = require('./test-config');

describe('api-generator.test', () => {

  describe('using db from connection-manager', () => {

    let api = null;
    before(async () => {
      const config = getMongoConfig();
      await connectionManager.start(config);
      const db = connectionManager.getCurrentDatabase();
      api = await generateApi(db);
    });
    after(async () => {
      await connectionManager.stop();
    });

    it('should be possible to create an entity', async () => {
      const entity = {id: 'a', type: 'b', cee: {type: 'Number', value: 2}};
      await api.createEntity({entity});
      const {entities} = await api.getEntities();
      assert.strictEqual(entities.length, 1);
    });

    it('should be possible to remove the entity', async () => {
      await api.deleteEntity({entityId: 'a'});
      const {entities} = await api.getEntities();
      assert.strictEqual(entities.length, 0);
    });

  });

  describe('using db from an independent connection', () => {

    let client = null;
    let api = null;
    before(async () => {
      const {url, dbName} = getMongoConfig();
      const mongodbOptions = {
        useNewUrlParser: true, // due to deprecation warning
        useUnifiedTopology: true, // due to deprecation warning
      };
      client = await new Promise((resolve, reject) => {
        // Use connect method to connect to the server
        MongoClient.connect(url, mongodbOptions, (err, cli) => {
          if (err) {
            reject(err);
          } else {
            resolve(cli);
          }
        });
      });
      const db = client.db(dbName);
      api = await generateApi(db);
    });
    after(async () => {
      await client.close();
    });

    it('should be possible to create an entity', async () => {
      const entity = {id: 'a', type: 'b', cee: {type: 'Number', value: 2}};
      await api.createEntity({entity});
      const {entities} = await api.getEntities();
      assert.strictEqual(entities.length, 1);
    });

    it('should be possible to remove the entity', async () => {
      await api.deleteEntity({entityId: 'a'});
      const {entities} = await api.getEntities();
      assert.strictEqual(entities.length, 0);
    });
  });

});
