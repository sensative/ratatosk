/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const delay = require('delay');
const serialPromise = require('promise-serial-exec');

const {
  ngsiSql: {queryParser: {parseQueryString}},
  ngsiEntityFormatter: {formatEntity},
} = require('@ratatosk/ngsi-utils');

const {connectionManager: {stop, restart, mongoRaw}} = require('../src');
const {getMongoConfig} = require('./test-config');

const entities = [
  // for numbers
  {id: 'aa', type: 'diggy', arp: 0},
  {id: 'dd', type: 'diggy', arp: 2},
  {id: 'cc', type: 'diggy', arp: 1, marp: true},
  {id: 'cc', type: 'aggy'},
  {id: 'cc', type: 'zaggy', arp: {value: 2, metadata: {boo: 4, zoo: 'nu'}}},
];

describe('bulk-query', () => {

  let api;
  before(async () => {
    api = await restart(getMongoConfig());
    await serialPromise(_.map(entities, (entity) => async () => {
      await delay(10); // some delay between creates so dates are different
      await api.createEntity({entity: formatEntity(entity)});
    }));
  });
  after(async () => {
    await mongoRaw.wipeEntities();
    await stop();
  });

  describe('entities only', () => {

    it('bulk-query tests (single id)', async () => {
      const payload = {entities: [{id: ['aa']}]};
      const entities = await api.bulkQuery({payload});
      assert.strictEqual(entities.length, 1);
      assert.strictEqual(entities[0].id, 'aa');
    });

    it('bulk-query tests (compound ids)', async () => {
      const payload = {entities: [{id: ['aa', 'cc']}]};
      const entities = await api.bulkQuery({payload});
      assert.strictEqual(entities.length, 4);
    });

    it('bulk-query tests (multiple ids)', async () => {
      const payload = {entities: [{id: ['aa']}, {id: ['cc']}]};
      const entities = await api.bulkQuery({payload});
      assert.strictEqual(entities.length, 4);
    });

    it('bulk-query tests (mix of compound & multiple ids)', async () => {
      const payload = {entities: [{id: ['aa', 'cc']}, {id: ['cc']}]};
      const entities = await api.bulkQuery({payload});
      assert.strictEqual(entities.length, 7, 'duplicates are not removed');
    });

    it('bulk-query tests (mix of compounds ids and type)', async () => {
      const payload = {entities: [{id: ['aa', 'cc'], type: ['diggy']}]};
      const entities = await api.bulkQuery({payload});
      assert.strictEqual(entities.length, 2, 'duplicates are not removed');
    });

    it('bulk-query tests (mix of compounds ids and type)', async () => {
      const payload = {entities: [{id: ['cc'], type: ['diggy']}], attrs: ['marp']};
      const entities = await api.bulkQuery({payload});
      assert.strictEqual(entities.length, 1);
      const attrs = _.omit(entities[0], ['id', 'type']);
      assert.deepStrictEqual(attrs, {marp: {metadata: {}, type: 'Boolean', value: true}});
    });

  });

  describe('attrs only', () => {

    it('only non-existent custom attrs', async () => {
      const payload = {attrs: ['asdf', 'doh']};
      const entities = await api.bulkQuery({payload});
      assert.strictEqual(entities.length, 5);
      const allKeys = _.uniq(_.flatten(_.map(entities, (entity) => _.keys(entity))));
      assert.deepStrictEqual(allKeys, ['id', 'type']);
    });

    it('one existing custom attr', async () => {
      const payload = {attrs: ['arp']};
      const entities = await api.bulkQuery({payload});
      assert.strictEqual(entities.length, 5);
      const allKeys = _.uniq(_.flatten(_.map(entities, (entity) => _.keys(entity))));
      assert.deepStrictEqual(allKeys, ['id', 'type', 'arp']);
    });

    it('built-in dateCreated & dateModified', async () => {
      const payload = {attrs: ['dateCreated', 'dateModified']};
      const entities = await api.bulkQuery({payload});
      assert.strictEqual(entities.length, 5);
      const allKeys = _.uniq(_.flatten(_.map(entities, (entity) => _.keys(entity))));
      assert.deepStrictEqual(allKeys, ['id', 'type', 'dateCreated', 'dateModified']);
    });

    it('a mix', async () => {
      const payload = {attrs: ['dateCreated', 'marp', 'doesNotExist']};
      const entities = await api.bulkQuery({payload});
      assert.strictEqual(entities.length, 5);
      const allKeys = _.uniq(_.flatten(_.map(entities, (entity) => _.keys(entity))));
      assert.deepStrictEqual(allKeys, ['id', 'type', 'dateCreated', 'marp']);
    });

  });

  describe('metadata only', () => {

    it('only non-existent custom attrs', async () => {
      const payload = {metadata: ['asdf', 'doh']};
      const entities = await api.bulkQuery({payload});
      assert.strictEqual(entities.length, 5);
      const allMds = _(entities)
        .map((entity) => _.omit(entity, ['id', 'type']))
        .map((entity) => _.map(entity, (attr) => attr.metadata))
        .flatten()
        .valueOf();
      const allKeys = _.flatten(_.map(allMds, (md) => _.keys(md)));
      assert.deepStrictEqual(allKeys, []);
    });

    it('one existing custom metadata', async () => {
      const payload = {metadata: ['boo']};
      const entities = await api.bulkQuery({payload});
      assert.strictEqual(entities.length, 5);
      const allMds = _(entities)
        .map((entity) => _.omit(entity, ['id', 'type']))
        .map((entity) => _.map(entity, (attr) => attr.metadata))
        .flatten()
        .valueOf();
      const allKeys = _.flatten(_.map(allMds, (md) => _.keys(md)));
      assert.deepStrictEqual(allKeys, ['boo']);
    });

    it('built-in dateCreated & dateModified', async () => {
      const payload = {metadata: ['dateCreated', 'dateModified']};
      const entities = await api.bulkQuery({payload});
      assert.strictEqual(entities.length, 5);
      const allMds = _(entities)
        .map((entity) => _.omit(entity, ['id', 'type']))
        .map((entity) => _.map(entity, (attr) => attr.metadata))
        .flatten()
        .valueOf();
      const allKeys = _.uniq(_.flatten(_.map(allMds, (md) => _.keys(md))));
      assert.deepStrictEqual(allKeys, ['dateCreated', 'dateModified']);
    });

    it('a mix', async () => {
      const payload = {metadata: ['dateCreated', 'boo', 'doesNotExist']};
      const entities = await api.bulkQuery({payload});
      assert.strictEqual(entities.length, 5);
      const allMds = _(entities)
        .map((entity) => _.omit(entity, ['id', 'type']))
        .map((entity) => _.map(entity, (attr) => attr.metadata))
        .flatten()
        .valueOf();
      const allKeys = _.uniq(_.flatten(_.map(allMds, (md) => _.keys(md))));
      assert.deepStrictEqual(allKeys, ['dateCreated', 'boo']);
    });

  });

  describe('expression only', () => {

    it('simple q statement', async () => {
      const query = parseQueryString('arp==2');
      const payload = {expression: {q: query}};
      const entities = await api.bulkQuery({payload});
      assert.strictEqual(entities.length, 2);
    });

    it('simple mq statement', async () => {
      const query = parseQueryString('arp.boo>2');
      const payload = {expression: {mq: query}};
      const entities = await api.bulkQuery({payload});
      assert.strictEqual(entities.length, 1);
    });

  });

});
