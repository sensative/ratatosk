/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const assert = require('assert');

const {
  ngsiDateTimeUtils: {hasValidDateTimeFormat},
  ngsiEntityFormatter: {formatEntity},
} = require('@ratatosk/ngsi-utils');

const {connectionManager: {stop, restart, mongoRaw}} = require('../src');
const {getMongoConfig} = require('./test-config');

describe('representation-types', () => {

  let api;
  before(async () => {
    api = await restart(getMongoConfig());
  });
  after(async () => {
    await mongoRaw.wipeEntities();
    await stop();
  });

  describe('display options tests', () => {

    before(async () => {
      const entity = formatEntity({
        id: 'a',
        type: 'b',
        cee: {type: 'Number', value: 2},
        dee: 'aargh',
        fee: 2,
      });
      await api.createEntity({entity});
    });

    it('default display (is normalized)', async () => {
      const ent = await api.findEntity({entityId: 'a'});
      const expected = {
        id: 'a',
        type: 'b',
        cee: {value: 2, type: 'Number', metadata: {}},
        dee: {value: 'aargh', type: 'Text', metadata: {}},
        fee: {value: 2, type: 'Number', metadata: {}},
      };
      assert.deepStrictEqual(ent, expected);
    });

    it('normalized display', async () => {
      const ent = await api.findEntity({entityId: 'a', params: {representationType: 'normalized'}});
      const expected = {
        id: 'a',
        type: 'b',
        cee: {value: 2, type: 'Number', metadata: {}},
        dee: {value: 'aargh', type: 'Text', metadata: {}},
        fee: {value: 2, type: 'Number', metadata: {}},
      };
      assert.deepStrictEqual(ent, expected);
    });

    it('extended display', async () => {
      const ent = await api.findEntity({entityId: 'a', params: {representationType: 'extended'}});
      const dateCreated = ent.builtinAttrs.dateCreated.value;
      assert(hasValidDateTimeFormat(dateCreated));
      const stamp = {type: 'DateTime', value: dateCreated};
      const stamps = {dateCreated: stamp, dateModified: stamp};
      const [mongoEnt] = await mongoRaw.getEntities();
      const expected = {
        id: 'a',
        type: 'b',
        servicePath: '/',
        attrs: {
          cee: {value: 2, type: 'Number', metadata: {}, builtinMetadata: stamps},
          dee: {value: 'aargh', type: 'Text', metadata: {}, builtinMetadata: stamps},
          fee: {value: 2, type: 'Number', metadata: {}, builtinMetadata: stamps},
        },
        builtinAttrs: {
          ...stamps,
          fiwareId: {
            type: 'Text',
            value: mongoEnt._id.toString(),
          },
        },
      };
      assert.deepStrictEqual(ent, expected);
    });

    it('keyValues representation', async () => {
      const ent = await api.findEntity({entityId: 'a', params: {representationType: 'keyValues'}});
      const expected = {
        id: 'a',
        type: 'b',
        cee: 2,
        dee: 'aargh',
        fee: 2,
      };
      assert.deepStrictEqual(ent, expected);
    });

    it('values representation', async () => {
      const ent = await api.findEntity({entityId: 'a', params: {representationType: 'values'}});
      const expected = [2, 'aargh', 2];
      assert.deepStrictEqual(ent, expected);
    });

    it('unique representation', async () => {
      const ent = await api.findEntity({entityId: 'a', params: {representationType: 'unique'}});
      const expected = [2, 'aargh'];
      assert.deepStrictEqual(ent, expected);
    });

  });

});
