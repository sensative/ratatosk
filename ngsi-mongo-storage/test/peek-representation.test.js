/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const serialPromise = require('promise-serial-exec');

const {
  ngsiEntityFormatter: {formatEntity},
} = require('@ratatosk/ngsi-utils');

const {connectionManager: {stop, restart, mongoRaw}} = require('../src');
const {getMongoConfig} = require('./test-config');


describe('peek-representation', () => {

  let api;
  before(async () => {
    api = await restart(getMongoConfig());
  });
  after(async () => {
    await mongoRaw.wipeEntities();
    await stop();
  });

  describe('simple check of projections functionality', () => {

    const entitiesTemplates = [
      {
        id: 'asdf',
        type: 'derp',
      },
      {
        id: 'spud',
        type: 'flerp',
        name: 'joe',
        downlinkQueue: 'berry',
        description: 'smo',
      },
    ];

    before(async () => {
      const entities = _.map(entitiesTemplates, formatEntity);
      await serialPromise(_.map(entities, (entity) => async () => {
        await api.createEntity({entity});
      }));
    });


    it('peek representation of minimal "asdf"', async () => {
      const params = {representationType: 'peek'};
      const ent = await api.findEntity({entityId: 'asdf', params});
      const expected = {
        id: 'asdf',
        type: 'derp',
        servicePath: '/',
      };
      assert(_.isString(ent.fiwareId));
      assert.deepStrictEqual(_.omit(ent, 'fiwareId'), expected);
    });

    it('peek representation of maximal "spud"', async () => {
      const params = {representationType: 'peek'};
      const ent = await api.findEntity({entityId: 'spud', params});
      const expected = {
        id: 'spud',
        type: 'flerp',
        servicePath: '/',
        name: 'joe',
        downlinkQueue: 'berry',
        description: 'smo',
      };
      assert(_.isString(ent.fiwareId));
      assert.deepStrictEqual(_.omit(ent, 'fiwareId'), expected);
    });

  });

});
