/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const delay = require('delay');

const {
  ngsiEntityFormatter: {formatEntity},
} = require('@ratatosk/ngsi-utils');

const {connectionManager: {stop, restart, mongoRaw}} = require('../src');
const {getMongoConfig} = require('./test-config');

describe('update-entity-attr-value', () => {

  let api;
  before(async () => {
    api = await restart(getMongoConfig());
  });
  after(async () => {
    await mongoRaw.wipeEntities();
    await stop();
  });

  describe('text attr', () => {

    before(async () => {
      const entity = formatEntity({
        id: 'blue',
        type: 'stoops',
        crazt: {value: 'nietzsche', type: 'Text', metadata: {spoon: {value: 'sooop', type: 'derp'}}},
      });
      await api.createEntity({entity});
    });
    after(() => mongoRaw.wipeEntities());

    it('we have an text attr that looks as expected', async () => {
      const attr = await api.findEntityAttr({entityId: 'blue', attrName: 'crazt'});
      const expected = {
        value: 'nietzsche',
        type: 'Text',
        metadata: {spoon: {value: 'sooop', type: 'derp'}},
      };
      assert.deepStrictEqual(attr, expected);
    });

    it('update to another string', async () => {
      await delay(10); // do a little wait to ensure diffs in create & modify times
      await api.updateEntityAttrValue({entityId: 'blue', attrName: 'crazt', attrValue: 'splatch'});
      const attr = await api.findEntityAttr({entityId: 'blue', attrName: 'crazt'});
      const expected = {
        value: 'splatch',
        type: 'Text',
        metadata: {spoon: {value: 'sooop', type: 'derp'}},
      };
      assert.deepStrictEqual(attr, expected);
      // check that the attr dates change
      const [mongoDoc] = await mongoRaw.getEntities();
      const mongoAttr = mongoDoc.attrs.crazt;
      const creDate = mongoAttr.creDate.valueOf();
      const modDate = mongoAttr.modDate.valueOf();
      assert(modDate - creDate > 10);
    });

    it('changing to a DateTime consistent string should NOT affect the type', async () => {
      await api.updateEntityAttrValue({entityId: 'blue', attrName: 'crazt', attrValue: '2019-09-24T15:11:11.111Z'});
      const attr = await api.findEntityAttr({entityId: 'blue', attrName: 'crazt'});
      const expected = {
        value: '2019-09-24T15:11:11.111Z',
        type: 'Text',
        metadata: {spoon: {value: 'sooop', type: 'derp'}},
      };
      assert.deepStrictEqual(attr, expected);
      // check that the attr dates change
      const [mongoDoc] = await mongoRaw.getEntities();
      const mongoAttr = mongoDoc.attrs.crazt;
      assert(_.isString(mongoAttr.value), 'Should definitely not be a DateTime date even though it looks like one');
    });

    it('should be possible to update to an object as well', async () => {
      await api.updateEntityAttrValue({entityId: 'blue', attrName: 'crazt', attrValue: {do: 'not'}});
      const attr = await api.findEntityAttr({entityId: 'blue', attrName: 'crazt'});
      const expected = {
        value: {do: 'not'},
        type: 'Text',
        metadata: {spoon: {value: 'sooop', type: 'derp'}},
      };
      assert.deepStrictEqual(attr, expected);
    });

  });

  describe('date attr & metadata date', () => {

    before(async () => {
      const entity = formatEntity({
        id: 'red',
        type: 'lint',
        spizzy: {value: '2019-09-24T15:11:11.111Z', type: 'DateTime', metadata: {trinkets: {value: '2019-09-24T15:33:33.333Z', type: 'DateTime'}}},
      });
      await api.createEntity({entity});
    });
    after(() => mongoRaw.wipeEntities());

    it('we have an text attr that looks as expected', async () => {
      const attr = await api.findEntityAttr({entityId: 'red', attrName: 'spizzy'});
      const expected = {
        value: '2019-09-24T15:11:11.111Z',
        type: 'DateTime',
        metadata: {trinkets: {value: '2019-09-24T15:33:33.333Z', type: 'DateTime'}},
      };
      assert.deepStrictEqual(attr, expected);
      // and check the mongodoc for Dates
      const [mongoDoc] = await mongoRaw.getEntities();
      const mongoAttr = mongoDoc.attrs.spizzy;
      assert(mongoAttr.value instanceof Date);
      assert(mongoAttr.md.trinkets.value instanceof Date);
    });

    it('update to another date', async () => {
      await api.updateEntityAttrValue({entityId: 'red', attrName: 'spizzy', attrValue: '2019-09-24T15:33:33.777Z'});
      const attr = await api.findEntityAttr({entityId: 'red', attrName: 'spizzy'});
      const expected = {
        value: '2019-09-24T15:33:33.777Z',
        type: 'DateTime',
        metadata: {trinkets: {value: '2019-09-24T15:33:33.333Z', type: 'DateTime'}},
      };
      assert.deepStrictEqual(attr, expected);
      // check that the modified mongoDoc value is a date
      const [mongoDoc] = await mongoRaw.getEntities();
      const mongoAttr = mongoDoc.attrs.spizzy;
      assert(!_.isString(mongoAttr.value), 'should not be a string..');
      assert(mongoAttr.value instanceof Date, 'should be a date internally');
    });

  });

});
