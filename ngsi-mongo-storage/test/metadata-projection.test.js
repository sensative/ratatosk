/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');

const {
  ngsiEntityFormatter: {formatEntity},
  ngsiDateTimeUtils: {hasValidDateTimeFormat},
} = require('@ratatosk/ngsi-utils');

const {connectionManager: {stop, restart, mongoRaw}} = require('../src');
const {getMongoConfig} = require('./test-config');

describe('metadata-projection', () => {

  let api;
  before(async () => {
    api = await restart(getMongoConfig());
  });
  after(async () => {
    await mongoRaw.wipeEntities();
    await stop();
  });

  describe('simple check of projections functionality', () => {

    before(async () => {
      const entity = formatEntity({
        id: 'arp',
        type: 'barp',
        attr0: {value: 5, metadata: {a: 1, b: 2, c: 3}},
        attrX: {value: 'five', metadata: {a: 'deeedum'}},
        attrDate: {value: 'date', metadata: {b: '2019-09-27T16:05:35.523Z'}},
      });
      await api.createEntity({entity});
    });

    it('using no metadata list at all', async () => {
      const ent = await api.findEntity({entityId: 'arp'});
      const mdKeys0 = _.keys(ent.attr0.metadata);
      assert.deepStrictEqual(mdKeys0, ['a', 'b', 'c']);
      const mdKeysX = _.keys(ent.attrX.metadata);
      assert.deepStrictEqual(mdKeysX, ['a']);
      const mdKeysDate = _.keys(ent.attrDate.metadata);
      assert.deepStrictEqual(mdKeysDate, ['b']);
      // make sure the attrDate md looks right
      assert.deepStrictEqual(ent.attrDate.metadata.b.value, '2019-09-27T16:05:35.523Z');
      // and make sure that it is stored as a Date in mongo
      const [mongoDoc] = await mongoRaw.getEntities();
      const dateVal = _.get(mongoDoc, 'attrs.attrDate.md.b.value');
      assert(dateVal instanceof Date);
    });

    it('we can use the metadata list to filter (and items are ignored if appropriate)', async () => {
      const params = {metadata: ['a', 'b']};
      const ent = await api.findEntity({entityId: 'arp', params});
      const mdKeys0 = _.keys(ent.attr0.metadata);
      assert.deepStrictEqual(mdKeys0, ['a', 'b']);
      const mdKeysX = _.keys(ent.attrX.metadata);
      assert.deepStrictEqual(mdKeysX, ['a']);
      const mdKeysDate = _.keys(ent.attrDate.metadata);
      assert.deepStrictEqual(mdKeysDate, ['b']);
    });

    it('we can combine attrList and metadataList', async () => {
      const params = {attrs: ['attr0'], metadata: ['a', 'b']};
      const ent = await api.findEntity({entityId: 'arp', params});
      const expected = {
        id: 'arp',
        type: 'barp',
        attr0: {
          value: 5,
          type: 'Number',
          metadata: {
            a: {type: 'Number', value: 1},
            b: {type: 'Number', value: 2},
          },
        },
      };
      assert.deepStrictEqual(ent, expected);
    });

    it('we can get the builtin metadata', async () => {
      const params = {attrs: ['attr0'], metadata: ['dateCreated', 'dateModified', 'a']};
      const ent = await api.findEntity({entityId: 'arp', params});
      const mdKeys0 = _.keys(ent.attr0.metadata);
      assert.deepStrictEqual(mdKeys0, ['a', 'dateCreated', 'dateModified']);
      assert(hasValidDateTimeFormat(ent.attr0.metadata.dateCreated.value));
      assert(hasValidDateTimeFormat(ent.attr0.metadata.dateModified.value));
    });

    it('we can get the builtin metadata and all custom metadata with the "*" wildcard', async () => {
      const params = {attrs: ['attr0'], metadata: ['dateCreated', 'dateModified', '*']};
      const ent = await api.findEntity({entityId: 'arp', params});
      const mdKeys0 = _.keys(ent.attr0.metadata);
      assert.deepStrictEqual(mdKeys0, ['a', 'b', 'c', 'dateCreated', 'dateModified']);
    });

    it('using "*" wildcard and specifying additional custom mds does notthing extra', async () => {
      const params = {attrs: ['attr0'], metadata: ['dateCreated', 'dateModified', '*', 'a', 'b', 'ccc']};
      const ent = await api.findEntity({entityId: 'arp', params});
      const mdKeys0 = _.keys(ent.attr0.metadata);
      assert.deepStrictEqual(mdKeys0, ['a', 'b', 'c', 'dateCreated', 'dateModified']);
    });

    it('extended entity: combining attrs & metadata lists, with metadata wildcard action', async () => {
      const params = {attrs: ['dateCreated', '*'], metadata: ['dateModified', 'b', 'ccc'], representationType: 'extended'};

      const ent = await api.findEntity({entityId: 'arp', params});
      const now = ent.builtinAttrs.dateCreated.value;
      const stamp = {type: 'DateTime', value: now};
      assert(hasValidDateTimeFormat(now));
      const expected = {
        id: 'arp',
        type: 'barp',
        servicePath: '/',
        builtinAttrs: {
          dateCreated: stamp,
        },
        attrs: {
          attr0: {
            type: 'Number',
            value: 5,
            metadata: {b: {type: 'Number', value: 2}},
            builtinMetadata: {dateModified: {type: 'DateTime', value: now}},
          },
          attrX: {
            type: 'Text',
            value: 'five',
            metadata: {},
            builtinMetadata: {dateModified: {type: 'DateTime', value: now}},
          },
          attrDate: {
            type: 'Text',
            value: 'date',
            metadata: {b: {type: 'DateTime', value: '2019-09-27T16:05:35.523Z'}},
            builtinMetadata: {dateModified: {type: 'DateTime', value: now}},
          },
        },
      };
      assert.deepStrictEqual(ent, expected);
    });

  });

});
