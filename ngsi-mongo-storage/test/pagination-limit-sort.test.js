/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const {
  assertRejects,
} = require('@ratatosk/ngsi-utils');

const {REST_PAGINATION_LIMIT_MAXIMUM} = require('@ratatosk/ngsi-constants');

const {connectionManager: {stop, restart, mongoRaw}} = require('../src');
const {getMongoConfig} = require('./test-config');

const generateId = (index) => {
  const numPad = 4 - String(index).length;
  const padding = _.times(numPad, () => '0').join('');
  const paddedIndex = `${padding}${index}`;
  const smerpId = `smerp_${paddedIndex}`;
  return smerpId;
};

describe('pagination-limit-sort.test', () => {

  let api;
  before(async () => {
    api = await restart(getMongoConfig());
    await mongoRaw.assertEntitiesEmpty();
  });
  after(async () => {
    await mongoRaw.wipeEntities();
    await stop();
  });

  const TOTAL_ENTITIES = REST_PAGINATION_LIMIT_MAXIMUM + 1;
  it(`setup - create ${REST_PAGINATION_LIMIT_MAXIMUM + 1} entities`, async () => {
    // randomize templates so that default sorting gets properly tested
    const templates = _.shuffle(_.times(TOTAL_ENTITIES, (index) => ({
      type: 'derp',
      id: generateId(index),
      attr1: {
        value: index % (3000 / 6),
        type: 'Number',
      },
      attr2: {
        value: index % 3,
        type: 'Number',
      },
    })));
    await Promise.all(_.map(templates, async (template) => {
      const entity = await api.createEntity({entity: template});
      return entity;
    }));
    const numEnts = await mongoRaw.countEntities();
    assert.strictEqual(numEnts, TOTAL_ENTITIES);
  }).timeout(10000);

  describe('limit (simple)', () => {

    it('we should get default 20 docs', async () => {
      const {entities} = await api.getEntities();
      assert.strictEqual(entities.length, 20);
      const expectedIds = _.times(20, (index) => generateId(index));
      const actualIds = _.map(entities, (entity) => entity.id);
      assert.deepStrictEqual(actualIds, expectedIds);
    });

    it('we should be able to specify 38 docs', async () => {
      const params = {limit: 38};
      const {entities} = await api.getEntities({params});
      assert.strictEqual(entities.length, 38);
    });

    it('we should be able to specify 538 docs', async () => {
      const params = {limit: 538};
      const {entities} = await api.getEntities({params});
      assert.strictEqual(entities.length, 538);
    });

    it(`we should be able to get ${REST_PAGINATION_LIMIT_MAXIMUM} docs`, async () => {
      const params = {limit: REST_PAGINATION_LIMIT_MAXIMUM};
      const {entities} = await api.getEntities({params});
      assert.strictEqual(entities.length, REST_PAGINATION_LIMIT_MAXIMUM);
    });

    it(`we should NOT be able to get ${REST_PAGINATION_LIMIT_MAXIMUM + 1} docs`, async () => {
      const params = {limit: REST_PAGINATION_LIMIT_MAXIMUM + 1};
      const err = 'Invalid page limit: must be a positive integer (strictly non-zero), or a corresponding string';
      await assertRejects(api.getEntities({params}), err);
    });

    it('omitting the count leads to null value for count', async () => {
      const params = {limit: 2};
      const {count} = await api.getEntities({params});
      assert.strictEqual(count, null);
    });

    it('we should be able to get a count', async () => {
      const params = {limit: 3, count: true};
      const {entities, count} = await api.getEntities({params});
      assert.strictEqual(entities.length, 3);
      assert.strictEqual(count, REST_PAGINATION_LIMIT_MAXIMUM + 1);
    });

  });

  describe('offset', () => {

    it('simple - no sort', async () => {
      const {entities: firstEntities} = await api.getEntities();
      assert.strictEqual(firstEntities.length, 20);
      assert.strictEqual(firstEntities[0].id, 'smerp_0000'); // default sort (orionId.id decreasing)
      assert.strictEqual(firstEntities[19].id, 'smerp_0019');
      const {entities: offsetEntities} = await api.getEntities({params: {offset: 5}});
      assert.strictEqual(offsetEntities.length, 20);
      assert.deepStrictEqual(offsetEntities[0], firstEntities[5]);
      assert.deepStrictEqual(offsetEntities[14], firstEntities[19]);
    });

    it('with sort (inverse id)', async () => {
      const params = {
        offset: 0,
        orderBy: [{name: 'id', isNegatory: true}],
      };
      const {entities: firstEntities} = await api.getEntities({params});
      assert.strictEqual(firstEntities.length, 20);
      assert.strictEqual(firstEntities[0].id, 'smerp_3000'); // default sort (orionId.id decreasing)
      assert.strictEqual(firstEntities[19].id, 'smerp_2981');
      params.offset = 5; // mutate!!
      const {entities: offsetEntities} = await api.getEntities({params});
      assert.strictEqual(offsetEntities.length, 20);
      assert.deepStrictEqual(offsetEntities[0], firstEntities[5]);
      assert.deepStrictEqual(offsetEntities[14], firstEntities[19]);
    });

    it('with multi-sort', async () => {
      const params = {
        offset: 0,
        orderBy: [
          {name: 'attr1', isNegatory: true},
          {name: 'attr2', isNegatory: false},
          {name: 'id', isNegatory: true},
        ],
        limit: 12,
      };
      const {entities: firstEntities} = await api.getEntities({params});
      _.times(12, (index) => {
        const ent = firstEntities[index];
        // primary
        assert.strictEqual(ent.attr1.value, index < 6 ? 499 : 498); // six of each
        // secondary
        assert.strictEqual(ent.attr2.value, Math.floor((index % 6) / 2));
      });
      // tertiary
      _.times(6, (index) => {
        const ent1 = firstEntities[2 * index];
        const ent2 = firstEntities[2 * index + 1];
        assert.strictEqual(ent1.attr1.value, ent2.attr1.value);
        assert.strictEqual(ent1.attr2.value, ent2.attr2.value);
        assert(ent1.id > ent2.id);
      });
      // test teh offset
      params.offset = 5; // mutate!!
      const {entities: offsetEntities} = await api.getEntities({params});
      assert.strictEqual(offsetEntities.length, 12);
      assert.deepStrictEqual(offsetEntities[0], firstEntities[5]);
      assert.deepStrictEqual(offsetEntities[14], firstEntities[19]);
    });

  });

  describe('pageItem', () => {

    it('pageItem with multi-sort', async function () {
      this.timeout(10000);
      // use offset to create entities list to check against
      const params = {
        offset: 557,
        orderBy: [
          {name: 'attr1', isNegatory: true},
          {name: 'attr2', isNegatory: false},
          {name: 'id', isNegatory: true},
        ],
        limit: 11,
      };
      const {entities} = await api.getEntities({params});
      assert.strictEqual(entities.length, 11);
      const expectedIds = [
        'smerp_0407',
        'smerp_2406',
        'smerp_0906',
        'smerp_1906',
        'smerp_0406',
        'smerp_2906',
        'smerp_1406',
        'smerp_1905',
        'smerp_0405',
        'smerp_2905',
        'smerp_1405',
      ];
      assert.deepStrictEqual(_.map(entities, (ent) => ent.id), expectedIds);
      // set up for pageItem
      const pageItem = entities[5];
      assert.strictEqual(pageItem.id, 'smerp_2906');
      const itemParams = {
        orderBy: [
          {name: 'attr1', isNegatory: true},
          {name: 'attr2', isNegatory: false},
          {name: 'id', isNegatory: true},
        ],
        limit: 5,
        pageItemId: pageItem.id,
        pageDirection: 'next',
      };
      const {entities: nextEntities} = await api.getEntities({params: itemParams});
      assert.strictEqual(nextEntities.length, 5);
      const nextIds = _.map(nextEntities, (ent) => ent.id);
      assert.deepStrictEqual(nextIds, expectedIds.slice(6, 11)); // last five
      // previous pageItem
      itemParams.pageDirection = 'prev';
      const {entities: prevEntities} = await api.getEntities({params: itemParams});
      assert.strictEqual(prevEntities.length, 5);
      const prevIds = _.map(prevEntities, (ent) => ent.id);
      assert.deepStrictEqual(prevIds, _.reverse(expectedIds.slice(0, 5))); // first five
    });

  });

});
