/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const {
  assertRejects,
  ngsiDateTimeUtils: {hasValidDateTimeFormat},
  ngsiSql: {queryParser: {parseQueryString}},
} = require('@ratatosk/ngsi-utils');

const {connectionManager: {stop, restart, mongoRaw}} = require('../src');
const {getMongoConfig} = require('./test-config');

const generateTimestamps = (now) => {
  const stamp = {value: now, type: 'DateTime'};
  const stamps = {dateCreated: stamp, dateModified: stamp};
  return stamps;
};

describe('crud-flowthrough', () => {

  let api;
  before(async () => {
    api = await restart(getMongoConfig());
  });
  after(async () => {
    await mongoRaw.wipeEntities();
    await stop();
  });

  it('create-entity X 2', async () => {
    await api.createEntity({entity: {id: 'a', type: 'b', cee: {type: 'Number', value: 2}}});
    const res = await api.createEntity({entity: {id: 'bo', type: 'do'}});
    const mongoDocs = await mongoRaw.getEntities();
    assert.strictEqual(mongoDocs.length, 2);
    assert.strictEqual(res.originalEntity, null);
    const now = _.get(res, 'modifiedEntity.builtinAttrs.dateCreated.value');
    assert(hasValidDateTimeFormat(now));
    const timestamps = generateTimestamps(now);
    const fiwareId = _.get(res.modifiedEntity, 'builtinAttrs.fiwareId.value');
    const expectedModified = {
      id: 'bo',
      type: 'do',
      servicePath: '/',
      attrs: {},
      builtinAttrs: {
        ...timestamps,
        fiwareId: {type: 'Text', value: fiwareId},
      },
    };
    assert.deepStrictEqual(res.modifiedEntity, expectedModified);
  });

  it('should not be possible to create an entity with same id and type', async () => {
    const copy = {id: 'a', type: 'b'};
    const err = 'Cannot create entity as it already exists for the given {id, type, servicePath} combination';
    await assertRejects(api.createEntity({entity: copy}), err);
  });

  it('get-entities', async () => {
    const {entities: ents0} = await api.getEntities();
    assert.strictEqual(ents0.length, 2);
    const {entities: ents1} = await api.getEntities({params: {id: ['bo']}});
    assert.strictEqual(ents1.length, 1);
    const {entities: ents2} = await api.getEntities({params: {type: ['b']}});
    assert.strictEqual(ents2.length, 1);
    const {entities: ents3} = await api.getEntities({params: {q: parseQueryString('!cee')}});
    assert.strictEqual(ents3.length, 1);
    const {entities: ents4} = await api.getEntities({params: {q: parseQueryString('cee>1')}});
    assert.strictEqual(ents4.length, 1);
  });

  it('find-entity', async () => {
    const ent0 = await api.findEntity({entityId: 'bo'});
    const expected0 = {id: 'bo', type: 'do'};
    assert.deepStrictEqual(ent0, expected0);
    const ent1 = await api.findEntity({entityId: 'a'});
    const expected1 = {id: 'a', type: 'b', cee: {metadata: {}, type: 'Number', value: 2}};
    assert.deepStrictEqual(ent1, expected1);
  });

  it('delete-entity', async () => {
    await api.deleteEntity({entityId: 'bo'});
    const {entities: ents} = await api.getEntities();
    assert.strictEqual(ents.length, 1);
    assert.strictEqual(ents[0].id, 'a');
  });

  it('get-entity-attrs', async () => {
    const attrs = await api.getEntityAttrs({entityId: 'a'});
    const expected = {cee: {metadata: {}, type: 'Number', value: 2}};
    assert.deepStrictEqual(attrs, expected);
  });

  it('replace-entity-attrs', async () => {
    const attrs = {dee: {type: 'Derp', value: true}};
    await api.replaceEntityAttrs({entityId: 'a', attrs});
    const savedAttrs = await api.getEntityAttrs({entityId: 'a'});
    const expected = {dee: {type: 'Derp', value: true, metadata: {}}};
    assert.deepStrictEqual(savedAttrs, expected, '');
  });

  it('upsert-entity-attrs', async () => {
    const attrs = {fee: {type: 'Merp', value: null}};
    await api.upsertEntityAttrs({entityId: 'a', attrs});
    const savedAttrs = await api.getEntityAttrs({entityId: 'a'});
    const expected = {
      dee: {type: 'Derp', value: true, metadata: {}},
      fee: {type: 'Merp', value: null, metadata: {}},
    };
    assert.deepStrictEqual(savedAttrs, expected);
    const nextAttrs = {dee: {type: 'Oopsy', value: 'doops'}};
    await api.upsertEntityAttrs({entityId: 'a', attrs: nextAttrs});
    const savedFinalAttrs = await api.getEntityAttrs({entityId: 'a'});
    const expectedFinal = {
      dee: {type: 'Oopsy', value: 'doops', metadata: {}},
      fee: {type: 'Merp', value: null, metadata: {}},
    };
    assert.deepStrictEqual(savedFinalAttrs, expectedFinal);
  });

  it('upsert-entity-attrs-atomicity', async () => {
    const attrs1 = {fee: {type: 'Merp', value: null}};
    const attrs2 = {abc: {type: 'Blafs', value: null}};
    const updates = [
      api.upsertEntityAttrs({entityId: 'a', attrs: attrs1}),
      api.upsertEntityAttrs({entityId: 'a', attrs: attrs2}),
    ];

    await Promise.all(updates);

    const savedAttrs = await api.getEntityAttrs({entityId: 'a'});

    const expected = {
      dee: {type: 'Oopsy', value: 'doops', metadata: {}},
      fee: {value: null, type: 'Merp', metadata: {}},
      abc: {value: null, type: 'Blafs', metadata: {}},
    };

    assert.deepStrictEqual(savedAttrs, expected);
  });

  it('patch-entity-attrs', async () => {
    // fails if the attr does not already exist
    const invalidAttrs = {floops: {type: 'FloopsIsMissing', value: 'argh'}};
    const errMsg = 'Desired attribute does not exist';
    const prom = api.patchEntityAttrs({entityId: 'a', attrs: invalidAttrs});

    await assertRejects(prom, errMsg);
    // but updating existing attr does work
    const attrs = {dee: {type: 'sllop', value: 'koops'}};
    await api.patchEntityAttrs({entityId: 'a', attrs});
    const savedAttrs = await api.getEntityAttrs({entityId: 'a'});
    const expected = {
      dee: {type: 'sllop', value: 'koops', metadata: {}},
      fee: {type: 'Merp', value: null, metadata: {}},
      abc: {value: null, type: 'Blafs', metadata: {}},
    };
    assert.deepStrictEqual(savedAttrs, expected, '');
  });

  it('find-entity-attr', async () => {
    const attr = await api.findEntityAttr({entityId: 'a', attrName: 'dee'});
    const expected = {value: 'koops', type: 'sllop', metadata: {}};
    assert.deepStrictEqual(attr, expected, '');
  });

  it('update-entity-attr', async () => {
    const attr = {type: 'doowop', value: 99999};
    await api.updateEntityAttr({entityId: 'a', attrName: 'dee', attr});
    const savedAttr = await api.findEntityAttr({entityId: 'a', attrName: 'dee'});
    const expected = {value: 99999, type: 'doowop', metadata: {}};
    assert.deepStrictEqual(savedAttr, expected);
  });

  it('delete-entity-attr', async () => {
    const initialAttrs = await api.getEntityAttrs({entityId: 'a'});
    assert.deepStrictEqual(_.sortBy(_.keys(initialAttrs)), ['abc', 'dee', 'fee']);
    await api.deleteEntityAttr({entityId: 'a', attrName: 'dee'});
    const errMsg = 'Desired attribute does not exist';
    await assertRejects(api.findEntityAttr({entityId: 'a', attrName: 'dee'}), errMsg);
    const finalAttrs = await api.getEntityAttrs({entityId: 'a'});
    assert.deepStrictEqual(_.sortBy(_.keys(finalAttrs)), ['abc', 'fee']);
  });

  it('find-entity-attr-value', async () => {
    const attrVal = await api.findEntityAttrValue({entityId: 'a', attrName: 'fee'});
    const expected = null;
    assert.deepStrictEqual(attrVal, expected);
  });

  it('update-entity-attr-value', async () => {
    await api.updateEntityAttrValue({entityId: 'a', attrName: 'fee', attrValue: 'spork'});
    const attrVal = await api.findEntityAttrValue({entityId: 'a', attrName: 'fee'});
    assert.strictEqual(attrVal, 'spork');
  });

});
