/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const proxyquire = require('proxyquire');

const {assertRejects} = require('@ratatosk/ngsi-utils');

const apiGenerator = proxyquire('../src/api-generator', {
  './mongo-api': {
    boppety: (collection) => async (requestArg) => {
      return `boppety ${requestArg.entityId}`;
    },
  },
});

const {start, stop, restart} = proxyquire('../src/connection-manager', {
  './api-generator': apiGenerator,
});

const {getMongoConfig} = require('./test-config');


describe('proxied-start-stop tests', () => {

  it('can start and stop', async () => {
    const api = await start(getMongoConfig());
    assert(_.has(api, 'boppety'), 'boppety should be there..');
    const res = await api.boppety({entityId: 'boo'});
    assert.strictEqual(res, 'boppety boo');
    await stop();
    assertRejects(api.boppety(), 'Connection has been closed');
  });

  it('can start, start, restart, and stop', async () => {
    const api0 = await start(getMongoConfig());
    const res0 = await api0.boppety({entityId: 'booo'});
    assert.strictEqual(res0, 'boppety booo');
    const restarter = restart(getMongoConfig());
    // NOTE that next reject happens SYNC (is before the "await");
    assertRejects(api0.boppety(), 'Connection has been closed');
    const api1 = await restarter;
    const res1 = await api1.boppety({entityId: 'choo'});
    assert.strictEqual(res1, 'boppety choo');
    const stopper = stop();
    // and the same here
    assertRejects(api1.boppety(), 'Connection has been closed');
    await stopper;
  });

});
