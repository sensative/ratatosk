/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const {
  assertRejects,
  ngsiEntityFormatter: {formatAttrs},
} = require('@ratatosk/ngsi-utils');

const {connectionManager: {stop, restart, mongoRaw}} = require('../src');
const {getMongoConfig} = require('./test-config');

const entityTemplates = [
  {entity: {id: 'spiggy0', type: 'Jam'}, servicePath: '/'},
  {entity: {id: 'spiggy1', type: 'Jam'}, servicePath: '/a'},
  {entity: {id: 'spiggy2', type: 'Jam'}, servicePath: '/b'},
  {entity: {id: 'spiggy3', type: 'Jam'}, servicePath: '/c'},
  {entity: {id: 'spiggy4', type: 'Jam'}, servicePath: '/d'},
  {entity: {id: 'spiggy5', type: 'Jam'}, servicePath: '/a/a'},
  {entity: {id: 'spiggy6', type: 'Jam'}, servicePath: '/a/b'},
  {entity: {id: 'spiggy7', type: 'Jam'}, servicePath: '/a/c'},
  {entity: {id: 'spiggy8', type: 'Jam'}, servicePath: '/a/d'},
  {entity: {id: 'spiggy9', type: 'Jam'}, servicePath: '/a/a/a'},
  {entity: {id: 'spiggyA', type: 'Jam'}, servicePath: '/a/a/b'},
  {entity: {id: 'spiggyB', type: 'Jam'}, servicePath: '/a/a/c'},
  {entity: {id: 'spiggyC', type: 'Jam'}, servicePath: '/a/a/d'},
];

describe('service-path-wildcard.test', () => {

  let api;
  before(async () => {
    api = await restart(getMongoConfig());
    await Promise.all(_.map(entityTemplates, async (template) => {
      const {entity, servicePath} = template;
      await api.createEntity({entity, servicePaths: [servicePath]});
    }));
  });
  after(async () => {
    await mongoRaw.wipeEntities();
    await stop();
  });

  it('cannot use a wildcard (OR Multiple paths) in createEntity', async () => {
    const entity = {id: 'willNotBeSaved', type: 'irrelevantType'};
    const mess = 'illegal char in servicePath component';
    const servicePath = '/a/#';
    await assertRejects(api.createEntity({entity, servicePaths: [servicePath]}), mess);
  });

  it('getEntities: "/#" finds all entities', async () => {
    const servicePath = '/#';
    const {entities} = await api.getEntities({servicePaths: [servicePath]});
    const numTotal = await mongoRaw.countEntities();
    assert.strictEqual(entities.length, numTotal);
  });

  it('getEntities: "/d/#,/c/#" finds two (i.e. those with "/d" and "/c" -- kinda strange)', async () => {
    const servicePaths = ['/d/#', '/c/#'];
    const {entities} = await api.getEntities({servicePaths});
    assert.strictEqual(entities.length, 2);
  });

  it('getEntities: "/a/#" should find most of them, including the one at "/a"', async () => {
    const servicePaths = ['/a/#'];
    const {entities} = await api.getEntities({servicePaths});
    assert.strictEqual(entities.length, 9);
  });

  it('should be able to handle combination', async () => {
    const servicePaths = ['/a/#', '/d/#', '/c'];
    const {entities} = await api.getEntities({servicePaths});
    assert.strictEqual(entities.length, 11);
  });

  it('upsertEntityAttrs handles wildcard as expected', async () => {
    const entityId = 'spiggy4';
    const attrs = formatAttrs({billy: 'joe'});

    // "/d/#" fails
    const mess0 = 'illegal char in servicePath component';
    await assertRejects(api.upsertEntityAttrs({entityId, attrs, servicePaths: ['/d/#']}), mess0);

    // "/#" also fails
    const mess1 = 'illegal char in servicePath component';
    await assertRejects(api.upsertEntityAttrs({entityId, attrs, servicePaths: ['/#']}), mess1);

    // "/d,/c" also fails
    const mess2 = 'Update operations can specify maximum 1 servicePaths';
    await assertRejects(api.upsertEntityAttrs({entityId, attrs, servicePaths: ['/d', '/c']}), mess2);

    // just as a quick check, "/ddd" FAILS as expected
    const mess3 = 'No matching entity to the query was found';
    await assertRejects(api.upsertEntityAttrs({entityId, attrs, servicePaths: ['/ddd']}), mess3);

    // "/d" WORKS
    await api.upsertEntityAttrs({entityId, attrs, servicePaths: ['/d']});
    const entity = await api.findEntity({entityId});
    const expected = {
      id: 'spiggy4',
      type: 'Jam',
      billy: {value: 'joe', type: 'Text', metadata: {}},
    };
    assert.deepStrictEqual(entity, expected);
  });

  it('updateEntityAttrValue handles wildcard as expected', async () => {
    const entityId = 'spiggy4';
    const attrName = 'billy';
    const attrValue = 'skull';

    // "/d/#" fails
    const mess0 = 'illegal char in servicePath component';
    await assertRejects(api.updateEntityAttrValue({entityId, attrName, attrValue, servicePaths: ['/d/#']}), mess0);

    // "/#" also fails
    const mess1 = 'illegal char in servicePath component';
    await assertRejects(api.updateEntityAttrValue({entityId, attrName, attrValue, servicePaths: ['/#']}), mess1);

    // "/d,/c" also fails
    const mess2 = 'Update operations can specify maximum 1 servicePaths';
    await assertRejects(api.updateEntityAttrValue({entityId, attrName, attrValue, servicePaths: ['/d', '/c']}), mess2);

    // just as a quick check, "/ddd" FAILS as expected
    const mess3 = 'No matching entity to the query was found';
    await assertRejects(api.updateEntityAttrValue({entityId, attrName, attrValue, servicePaths: ['/ddd']}), mess3);

    // "/d" WORKS
    await api.updateEntityAttrValue({entityId, attrName, attrValue, servicePaths: ['/d']});
    const entity = await api.findEntity({entityId});
    const expected = {
      id: 'spiggy4',
      type: 'Jam',
      billy: {value: 'skull', type: 'Text', metadata: {}},
    };
    assert.deepStrictEqual(entity, expected);
  });

  it('deleteEntity: what works here as well?? - more wierdnesses abound', async () => {
    const entityId = 'spiggy4';

    // "/d/#" fails
    const mess0 = 'illegal char in servicePath component';
    await assertRejects(api.deleteEntity({entityId, servicePaths: ['/d/#']}), mess0);

    // "/#" also fails
    const mess1 = 'illegal char in servicePath component';
    await assertRejects(api.deleteEntity({entityId, servicePaths: ['/#']}), mess1);

    // "/d,/c" also fails
    const mess2 = 'Update operations can specify maximum 1 servicePaths';
    await assertRejects(api.deleteEntity({entityId, servicePaths: ['/d', '/c']}), mess2);

    // just as a quick check, "/ddd" FAILS as expected
    const mess3 = 'No matching entity to the query was found';
    await assertRejects(api.deleteEntity({entityId, servicePaths: ['/ddd']}), mess3);

    // "/d" WORKS
    await api.deleteEntity({entityId, servicePaths: ['/d']});
    const messFinal = 'No matching entity to the query was found';
    await assertRejects(api.findEntity({entityId}), messFinal);
  });


});
