/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
// const assert = require('assert');
const {
  assertRejects,
} = require('@ratatosk/ngsi-utils');

const {connectionManager: {stop, restart, mongoRaw}} = require('../src');
const {getMongoConfig: getDefaultConfig} = require('./test-config');

const getConfig = (attrs) => _.assign(
  getDefaultConfig(),
  {globalRegistrationAttrs: attrs},
);

describe('global-registration-attrs-basics', () => {

  afterEach(async () => {
    await mongoRaw.wipeEntities()
      .then(stop)
      .catch(() => 0); // not all tests set up a connection..
  });

  it('empty attrs is ok', async () => {
    const attrs = null;
    const config = getConfig(attrs);
    await restart(config);
  });

  it('non-array attrs is not ok', async () => {
    const invalidAttrs = {not: 'an array'};
    const config = getConfig(invalidAttrs);
    const mess = 'globalRegistrationAttrs must be an array when included';
    await assertRejects(restart(config), mess);
  });

  it('invalid attrs do not work either', async () => {
    const invalidAttrs = ['##'];
    const config = getConfig(invalidAttrs);
    const mess = 'Invalid globalRegistrationAttrs item: field contains an illegal char';
    await assertRejects(restart(config), mess);
  });

  it('otherwise valid attrs are ok', async () => {
    const attrs = ['cmd', 'somethingElseEntirely'];
    const config = getConfig(attrs);
    await restart(config);
  });

});
