/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const ATTRS_UPDATE_TYPES = {
  UPSERT_ATTRS: 'UPSERT_ATTRS',
  REPLACE_ATTRS: 'REPLACE_ATTRS',
  STRICT_APPEND_ATTRS: 'STRICT_APPEND_ATTRS',
  PATCH_ATTRS: 'PATCH_ATTRS',
  DELETE_ATTRS: 'DELETE_ATTRS',
  UPDATE_ATTR_VALUES: 'UPDATE_ATTR_VALUES',
};

const MONGO_KEYS = {
  CRE_DATE: 'creDate',
  MOD_DATE: 'modDate',
  EXP_DATE: 'expDate',
  MD_CRE_DATE: 'creDate',
  MD_MOD_DATE: 'modDate',
  MD_ACTION_TYPE: 'someActionTypeKeyThisIsNotIt',
  MD_PREVIOUS_VALUE: 'somePreviousValueKeyThisIsNotIt',
};

const DEFAULT_GEONEAR_LIMIT = 100;

module.exports = {
  ATTRS_UPDATE_TYPES,
  MONGO_KEYS,
  DEFAULT_GEONEAR_LIMIT,
};
