/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const {
  ORION_ENTITITIES_DB_COLLECTION,
  ORION_SUBSCRIPTIONS_DB_COLLECTION,
} = require('@ratatosk/ngsi-constants');

const endpoints = require('./mongo-api');

const generateApi = (mongoDb, isAlive = null, globalRegistrationAttrs = []) => {

  const entitiesCollection = mongoDb.collection(ORION_ENTITITIES_DB_COLLECTION);
  const subscriptionsCollection = mongoDb.collection(ORION_SUBSCRIPTIONS_DB_COLLECTION);
  const collections = {
    entities: entitiesCollection,
    subscriptions: subscriptionsCollection,
  };

  const api = _.mapValues(endpoints, (endpoint) => {
    // we want to unwrap the endpoint just once
    const unwrapped = endpoint(collections, globalRegistrationAttrs);
    // and return a function that evaluates each time (including an isAlive check)
    if (isAlive) {
      return async (dbArg) => isAlive()
        .then(() => unwrapped(dbArg));
    } else {
      return async (dbArg) => unwrapped(dbArg);
    }
  });

  return api;
};

module.exports = {
  generateApi,
};
