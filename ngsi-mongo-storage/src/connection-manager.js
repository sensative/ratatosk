/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const {MongoClient} = require('mongodb');

const {
  ngsiDatumValidator: {validateAttributeName},
} = require('@ratatosk/ngsi-utils');

const {generateApi} = require('./api-generator');
const {
  convertEntityFromOrion,
  convertEntityToOrion,
} = require('./utils/mongo-entity-utils');
const {
  ORION_ENTITITIES_DB_COLLECTION,
  ORION_SUBSCRIPTIONS_DB_COLLECTION,
} = require('@ratatosk/ngsi-constants');

// MUTABLE STATE!!
const state = {
  current: null,
  isStopping: null,
  isStarting: null,
};

// this is just used as a sanity check
const assertValidState = () => {
  // at most 1 can be non-null at a time
  const hasValidSize = _.size(_.compact(state)) < 2;
  if (!hasValidSize) {
    throw new Error('DevErr: invalid mongo connection state');
  }
};

// local utility function
const connect = async (url, opts) => {
  const options = _.assign(
    {
      useNewUrlParser: true, // due to deprecation warning
      useUnifiedTopology: true, // due to deprecation warning
    },
    opts,
  );
  return new Promise((resolve, reject) => {
    // Use connect method to connect to the server
    MongoClient.connect(url, options, (err, cli) => {
      if (err) {
        reject(err);
      } else if (!cli) {
        reject(new Error('mongo client is missing'));
      } else {
        resolve(cli);
      }
    });
  });
};

// extracts existing collection
const extractCollection = (collections, collectionName) => {
  const targetCollection = _.find(collections, (collection) => {
    const name = _.get(
      collection,
      's.namespace.collection',
      _.get(collection, 's.name', null),
    );
    return name === ORION_ENTITITIES_DB_COLLECTION;
  });
  return targetCollection;
};

// local utility function
const createApi = async (db, isAlive, globalRegistrationAttrs) => {

  // check if we need to create a new collection
  const collections = await db.collections();

  // entities
  const entitiesCollection = extractCollection(collections, ORION_ENTITITIES_DB_COLLECTION);
  if (!entitiesCollection) {
    await db.createCollection(ORION_ENTITITIES_DB_COLLECTION);
    const ents = db.collection(ORION_ENTITITIES_DB_COLLECTION);
    // create indexes
    await ents.createIndex({'location.coords': '2dsphere'});
    await ents.createIndex({expDate: 1}, {expireAfterSeconds: 0});
  }

  // subscriptions
  const subscriptionsCollection = extractCollection(collections, ORION_SUBSCRIPTIONS_DB_COLLECTION);
  if (!subscriptionsCollection) {
    // no indices needed for this guy
    await db.createCollection(ORION_SUBSCRIPTIONS_DB_COLLECTION);
  }

  // and done
  return generateApi(db, isAlive, globalRegistrationAttrs);
};

// EXPORT - closes any existing (or about-to-exist) mongo connection
const stop = async () => {
  assertValidState();
  if (state.isStopping) {
    return state.isStopping;
  }
  if (state.isStarting) {
    return state.isStarting
      .then(stop);
  }
  if (!state.current) {
    return; // already stopped
  }
  // and stop the client
  const client = state.current.client;
  state.current = null;
  state.isStopping = client.close()
    .then(() => {
      state.isStopping = null;
      assertValidState();
    });
  // and done
  assertValidState();
  return state.isStopping;
};

const validateGlobalRegistrationAttrs = (globalRegistrationAttrs) => {
  if (!_.isArray(globalRegistrationAttrs) && !_.isNil(globalRegistrationAttrs)) {
    throw new Error('globalRegistrationAttrs must be an array when included');
  }
  _.each(globalRegistrationAttrs, (attr) => {
    try {
      validateAttributeName(attr);
    } catch (err) {
      throw new Error(`Invalid globalRegistrationAttrs item: ${err.message}`);
    }
  });
  return globalRegistrationAttrs;
};

// EXPORT - creates a new connection if one does not exist
const start = async ({url, options, dbName, globalRegistrationAttrs = []}) => {
  assertValidState();
  if (state.isStopping) {
    return state.isStopping
      .then(() => start({url, options, dbName, globalRegistrationAttrs}));
  }
  if (state.isStarting) {
    return state.isStarting;
  }
  if (state.current) {
    return state.current.api;
  }
  // start the connection and return the api
  validateGlobalRegistrationAttrs(globalRegistrationAttrs); // validate BEFORE starting
  state.isStarting = connect(url, options, dbName)
    .then((client) => {
      const connErr = () => new Error('Connection has been closed');
      const isAlive = async () => {
        if (client !== _.get(state, 'current.client')) {
          throw connErr();
        }
      };
      const db = client.db(dbName);
      const api = createApi(db, isAlive, globalRegistrationAttrs);
      state.current = {client, db, api};
      state.isStarting = null;
      assertValidState();
      return state.current.api;
    });
  // and done
  assertValidState();
  return state.isStarting;
};

// EXPORT - stops any existing mongo connection, then starts a new one
const restart = async ({url, options, dbName, globalRegistrationAttrs}) => {
  await stop();
  return start({url, options, dbName, globalRegistrationAttrs});
};

// mongoRaw
const mongoRawRoutes = require('./mongo-raw');

const mongoRaw = _.mapValues(mongoRawRoutes, (route) => async (...args) => {
  const db = _.get(state, 'current.db');
  if (!db) {
    throw new Error('mongoRaw db is not connected');
  }
  return route(db)(...args);
});

const importOrion = async ({url, options, dbName, orionDbName, overwrite = false}) => {

  // connect to a client
  if (dbName === orionDbName) {
    throw new Error('db names must be different');
  }
  const client = await connect(url, options);
  const closeClient = () => new Promise((resolve, reject) => {
    client.close((err, res) => {
      resolve(err || res);
    });
  });

  // check orion compatibility
  const orionDb = client.db(orionDbName);
  const orionCollections = await orionDb.collections();
  const orionEntities = extractCollection(orionCollections, ORION_ENTITITIES_DB_COLLECTION);
  if (!orionEntities) {
    await closeClient();
    throw new Error('cannot import orion enities to ngsi format: orion entities collection not found');
  }

  // check ngsi compatibility
  const ngsiDb = client.db(dbName);
  const ngsiEntities = ngsiDb.collection(ORION_ENTITITIES_DB_COLLECTION);
  const numNgsiEntities = await ngsiEntities.countDocuments();
  if (numNgsiEntities && !overwrite) {
    await closeClient();
    throw new Error('ngsi entities collection already contains documents (must use "overwrite" option to force overwrite)');
  }
  if (numNgsiEntities && overwrite) {
    console.info(`Deleting ${numNgsiEntities} entities from ngsi entities collection`);
    await ngsiEntities.deleteMany();
  }

  // and convert
  const query = orionEntities.find({}).batchSize(100);
  let hasNext = await query.hasNext();
  while (hasNext) {
    const entity = await query.next();
    const convertedEntity = convertEntityFromOrion(entity);
    await ngsiEntities.insertOne(convertedEntity);
    hasNext = await query.hasNext();
  }

  // and done - return the collections for access purposes
  return {orionEntities, ngsiEntities, closeClient};
};

const exportOrion = async ({url, options, dbName, orionDbName, overwrite = false}) => {
  // connect to a client
  if (dbName === orionDbName) {
    throw new Error('db names must be different');
  }
  const client = await connect(url, options);
  const closeClient = () => new Promise((resolve, reject) => {
    client.close((err, res) => {
      resolve(err || res);
    });
  });

  // check ngsi compatibility
  const ngsiDb = client.db(dbName);
  const ngsiCollections = await ngsiDb.collections();
  const ngsiEntities = extractCollection(ngsiCollections, ORION_ENTITITIES_DB_COLLECTION);
  if (!ngsiEntities) {
    await closeClient();
    throw new Error('cannot export ngsi enities to orion format: ngsi entities collection not found');
  }

  // check orion compatibility
  const orionDb = client.db(orionDbName);
  const orionEntities = orionDb.collection(ORION_ENTITITIES_DB_COLLECTION);
  const numOrionEntities = await orionEntities.countDocuments();
  if (numOrionEntities && !overwrite) {
    await closeClient();
    throw new Error('orion entities collection already contains documents');
  }
  if (numOrionEntities && overwrite) {
    console.info(`Deleting ${numOrionEntities} entities from Orion entities collection`);
    await orionEntities.deleteMany();
  }

  // and convert
  const query = ngsiEntities.find({}).batchSize(100);
  let hasNext = await query.hasNext();
  while (hasNext) {
    const entity = await query.next();
    const convertedEntity = convertEntityToOrion(entity);
    await orionEntities.insertOne(convertedEntity);
    hasNext = await query.hasNext();
  }

  // and done - return the collections for access purposes
  return {orionEntities, ngsiEntities, closeClient};
};

const getCurrentDatabase = () => {
  return _.get(state, 'current.db');
};


module.exports = {
  start,
  stop,
  restart,
  mongoRaw,
  importOrion,
  exportOrion,
  getCurrentDatabase,
};
