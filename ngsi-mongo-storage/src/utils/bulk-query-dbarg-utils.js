/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {
  ngsiServicePathUtils: {validateMultipleServicePathsStrict},
  ngsiQueryParamsUtils: {
    bulkQueryPayloadParser: {
      validateBulkQueryPayload,
    },
    bulkUpdatePayloadParser: {
      validateBulkUpdatePayload,
    },
  },
  ngsiStorageErrorUtils: {
    errors: storageErrors,
  },
  constants: {
    DB_ENTITY_ARG_KEYS,
    DB_BULK_QUERY_ARG_KEYS,
    DB_BULK_UPDATE_ARG_KEYS,
    DB_ARG_PARAMS_KEYS,
  },
} = require('@ratatosk/ngsi-utils');

const {
  BULK_QUERY_PAYLOAD_KEYS,
} = require('@ratatosk/ngsi-constants');


const validateBulkUpdateDbarg = (dbBulkUpdateArg) => {
  if (!_.isUndefined(dbBulkUpdateArg) && !_.isObject(dbBulkUpdateArg)) {
    throw storageErrors.INVALID_BULK_UPDATE_DBARG_FORMAT();
  }
  const diff = _.difference(_.keys(dbBulkUpdateArg), _.values(DB_BULK_UPDATE_ARG_KEYS));
  if (_.size(diff)) {
    throw storageErrors.INVALID_BULK_UPDATE_DBARG_KEYS();
  }
  if (_.has(dbBulkUpdateArg, DB_BULK_UPDATE_ARG_KEYS.SERVICE_PATHS)) {
    const isUpdate = true;
    validateMultipleServicePathsStrict(dbBulkUpdateArg[DB_BULK_UPDATE_ARG_KEYS.SERVICE_PATHS], isUpdate);
  }
  if (_.has(dbBulkUpdateArg, DB_BULK_UPDATE_ARG_KEYS.PAYLOAD)) {
    validateBulkUpdatePayload(dbBulkUpdateArg[DB_BULK_UPDATE_ARG_KEYS.PAYLOAD]);
  }
};

const validateBulkQueryDbarg = (dbBulkQueryArg) => {
  if (!_.isUndefined(dbBulkQueryArg) && !_.isObject(dbBulkQueryArg)) {
    throw storageErrors.INVALID_BULK_QUERY_DBARG_FORMAT();
  }
  const diff = _.difference(_.keys(dbBulkQueryArg), _.values(DB_BULK_QUERY_ARG_KEYS));
  if (_.size(diff)) {
    throw storageErrors.INVALID_BULK_QUERY_DBARG_KEYS();
  }

  if (_.has(dbBulkQueryArg, DB_BULK_QUERY_ARG_KEYS.SERVICE_PATHS)) {
    const isUpdate = false;
    validateMultipleServicePathsStrict(dbBulkQueryArg[DB_BULK_QUERY_ARG_KEYS.SERVICE_PATHS], isUpdate);
  }
  if (_.has(dbBulkQueryArg, DB_BULK_QUERY_ARG_KEYS.PAYLOAD)) {
    validateBulkQueryPayload(dbBulkQueryArg[DB_BULK_QUERY_ARG_KEYS.PAYLOAD]);
  }
};

const partitionBulkQueryDbarg = (dbBulkQueryArg) => {
  validateBulkQueryDbarg(dbBulkQueryArg);
  const payload = dbBulkQueryArg[DB_BULK_QUERY_ARG_KEYS.PAYLOAD];
  const servicePaths = dbBulkQueryArg[DB_BULK_QUERY_ARG_KEYS.SERVICE_PATHS];

  const entities = _.get(payload, BULK_QUERY_PAYLOAD_KEYS.ENTITIES, [{}]);
  const attrs = _.get(payload, BULK_QUERY_PAYLOAD_KEYS.ATTRS_LIST);
  const metadata = _.get(payload, BULK_QUERY_PAYLOAD_KEYS.METADATA_LIST);
  const expression = _.get(payload, BULK_QUERY_PAYLOAD_KEYS.EXPRESSION);

  const dbArgs = _.map(entities, (entity) => {
    const params = _.assign(
      attrs ? {[DB_ARG_PARAMS_KEYS.ATTRS_LIST]: attrs} : {},
      metadata ? {[DB_ARG_PARAMS_KEYS.METADATA_LIST]: metadata} : {},
      entity,
      expression,
    );
    const dbArg = {
      [DB_ENTITY_ARG_KEYS.SERVICE_PATHS]: servicePaths,
      [DB_ENTITY_ARG_KEYS.PARAMS]: params,
    };
    return dbArg;
  });
  return dbArgs;
};

module.exports = {
  validateBulkUpdateDbarg,
  validateBulkQueryDbarg,
  partitionBulkQueryDbarg,
};
