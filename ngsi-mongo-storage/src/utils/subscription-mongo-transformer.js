/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {
  SUBSCRIPTION_PAYLOAD_KEYS,
  SUBSCRIPTION_PAYLOAD_SUBJECT_KEYS,
  // SUBSCRIPTION_PAYLOAD_SUBJECT_CONDITION_KEYS,
  SUBSCRIPTION_PAYLOAD_NOTIFICATION_KEYS,
  SUBSCRIPTION_PAYLOAD_NOTIFICATION_HTTP_KEYS,
  // SUBSCRIPTION_PAYLOAD_NOTIFICATION_HTTP_CUSTOM_KEYS,
  // SUBSCRIPTION_NOTIFICATION_STATE_PROPS,
  // SUBSCRIPTION_STATUS_VALUES,
  // these too..
  BULK_QUERY_PAYLOAD_ENTITIES_KEYS,
} = require('@ratatosk/ngsi-constants');


const transformSubjectEntitiesToMongo = (subjectEntities) => {
  const {
    ENTITY_TYPE,
    ENTITY_TYPE_PATTERN,
    ENTITY_ID,
    ENTITY_ID_PATTERN,
  } = BULK_QUERY_PAYLOAD_ENTITIES_KEYS;
  const entities = _.map(subjectEntities, (ent) => {
    const mongoEnt = {
      id: _.get(ent, ENTITY_ID, _.get(ent, ENTITY_ID_PATTERN)),
      isPattern: _.has(ent, ENTITY_ID_PATTERN) ? 'true' : 'false', // this one is a string for some reason
    };
    if (_.has(ent, ENTITY_TYPE) || _.has(ent, ENTITY_TYPE_PATTERN)) {
      mongoEnt.type = _.get(ent, ENTITY_TYPE, _.get(ent, ENTITY_TYPE_PATTERN));
      mongoEnt.isTypePattern = _.has(ent, ENTITY_TYPE_PATTERN); // this one should be boolean
    }
    return mongoEnt;
  });
  return entities;
};

const transformSubjectEntitiesFromMongo = (mongoEntities) => {
  const {
    ENTITY_TYPE,
    ENTITY_TYPE_PATTERN,
    ENTITY_ID,
    ENTITY_ID_PATTERN,
  } = BULK_QUERY_PAYLOAD_ENTITIES_KEYS;
  const entities = _.map(mongoEntities, (mongoEntity) => {
    const entity = {}; // will be mutated
    if (mongoEntity.isPattern === 'true') {
      entity[ENTITY_ID_PATTERN] = mongoEntity.id;
    } else {
      entity[ENTITY_ID] = mongoEntity.id;
    }
    if (mongoEntity.type) {
      if (mongoEntity.isTypePattern) {
        entity[ENTITY_TYPE_PATTERN] = mongoEntity.type;
      } else {
        entity[ENTITY_TYPE] = mongoEntity.type;
      }
    }
    return entity;
  });
  return entities;
};

const transformSubscriptionToMongo = (subscription, servicePath) => {

  // top level
  const description = _.get(subscription, SUBSCRIPTION_PAYLOAD_KEYS.DESCRIPTION, '');
  const subject = _.get(subscription, SUBSCRIPTION_PAYLOAD_KEYS.SUBJECT);
  const notification = _.get(subscription, SUBSCRIPTION_PAYLOAD_KEYS.NOTIFICATION);

  // subject derivatives
  const entities = _.get(subject, SUBSCRIPTION_PAYLOAD_SUBJECT_KEYS.ENTITIES);

  // notification derivatives
  const http = _.get(notification, SUBSCRIPTION_PAYLOAD_NOTIFICATION_KEYS.HTTP);
  const url = _.get(http, SUBSCRIPTION_PAYLOAD_NOTIFICATION_HTTP_KEYS.URL);

  const mongoSubscription = {
    reference: url,
    custom: false, // no custom allowed for now
    throttling: 0, // ignored for now
    servicePath,
    description,
    entities: transformSubjectEntitiesToMongo(entities),
    format: 'normalized', // set hard for now
    conditions: [], // not allowed for now
    expression: {q: '', mq: '', geometry: '', coords: '', georel: ''}, // not allowed for now
  };

  return mongoSubscription;
};

const transformSubscriptionFromMongo = (mongoSubscription) => {

  // create the timeStamps
  const lastNotification = _.get(mongoSubscription, 'notification.lastNotification');
  const lastSuccess = _.get(mongoSubscription, 'notification.lastSuccess');
  const lastFailure = _.get(mongoSubscription, 'notification.lastFailure');
  const timeStamps = _.assign(
    lastNotification ? {lastNotification} : {},
    lastSuccess ? {lastSuccess} : {},
    lastFailure ? {lastFailure} : {},
  );
  // and the full object
  const subscription = {
    id: mongoSubscription._id.toString(),
    description: mongoSubscription.description,
    status: mongoSubscription.status || 'active',
    subject: {
      entities: transformSubjectEntitiesFromMongo(mongoSubscription.entities),
    },
    notification: {
      http: {
        url: mongoSubscription.reference,
      },
      timesSent: _.get(mongoSubscription, 'notification.timesSent', 0),
      ...timeStamps,
    },
  };
  return subscription;
};

module.exports = {
  transformSubscriptionToMongo,
  transformSubscriptionFromMongo,
};
