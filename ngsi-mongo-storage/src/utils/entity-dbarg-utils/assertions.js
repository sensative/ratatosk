/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {
  constants: {
    DB_ENTITY_ARG_KEYS,
    DB_ARG_PARAMS_KEYS,
  },
  ngsiStorageErrorUtils: {errors: storageErrors},
} = require('@ratatosk/ngsi-utils');


const requiresDbArgEntity = (dbArg) => {
  if (!_.get(dbArg, DB_ENTITY_ARG_KEYS.ENTITY)) {
    throw storageErrors.INVALID_ENTITY_DBARG_ENTITY_MISSING();
  }
};

const requiresDbArgEntityId = (dbArg) => {
  if (!_.get(dbArg, DB_ENTITY_ARG_KEYS.ENTITY_ID)) {
    throw storageErrors.INVALID_ENTITY_DBARG_ENTITY_ID_MISSING();
  }
  const hasId = _.has(dbArg, [DB_ENTITY_ARG_KEYS.PARAMS, DB_ARG_PARAMS_KEYS.ENTITY_ID]);
  const hasIdPattern = _.has(dbArg, [DB_ENTITY_ARG_KEYS.PARAMS, DB_ARG_PARAMS_KEYS.ENTITY_ID_PATTERN]);
  if (hasId || hasIdPattern) {
    throw storageErrors.INVALID_ENTITY_DBARG_FAULTY_ID_INFO();
  }
};

const requiresDbArgEntityAttrs = (dbArg) => {
  if (!_.get(dbArg, DB_ENTITY_ARG_KEYS.ATTRS)) {
    throw storageErrors.INVALID_ENTITY_DBARG_ATTRS_MISSING();
  }
};

const requiresDbArgAttrName = (dbArg) => {
  if (!_.get(dbArg, DB_ENTITY_ARG_KEYS.ATTR_NAME)) {
    throw storageErrors.INVALID_ENTITY_DBARG_ATTR_NAME_MISSING();
  }
};

const requiresDbArgEntityAttr = (dbArg) => {
  if (!_.get(dbArg, DB_ENTITY_ARG_KEYS.ATTR)) {
    throw storageErrors.INVALID_ENTITY_DBARG_ATTR_MISSING();
  }
};

const requiresDbArgEntityAttrValue = (dbArg) => {
  // We cannot use _.get for this one, as it can contain falsey values legitimately
  if (!_.has(dbArg, DB_ENTITY_ARG_KEYS.ATTR_VALUE)) {
    throw storageErrors.INVALID_ENTITY_DBARG_ATTR_VALUE_MISSING();
  }
};

module.exports = {
  requiresDbArgEntity,
  requiresDbArgEntityId,
  requiresDbArgEntityAttrs,
  requiresDbArgAttrName,
  requiresDbArgEntityAttr,
  requiresDbArgEntityAttrValue,
};
