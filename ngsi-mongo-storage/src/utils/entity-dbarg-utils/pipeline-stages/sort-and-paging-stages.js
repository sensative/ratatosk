/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {
  ENTITY_ID_KEY,
  ENTITY_TYPE_KEY,
  GEO_DISTANCE_KEY,
  BUILTIN_ATTRIBUTES,
  PAGING_DIRECTIONS,
} = require('@ratatosk/ngsi-constants');

const {
  MONGO_KEYS,
} = require('../../../constants');


const {
  constants: {
    DB_ENTITY_ARG_KEYS,
    DB_ARG_PARAMS_KEYS,
  },
} = require('@ratatosk/ngsi-utils');


const BUILTINS_FIWARE_TO_MONGO = {
  [BUILTIN_ATTRIBUTES.CREATED_AT.name]: MONGO_KEYS.CRE_DATE,
  [BUILTIN_ATTRIBUTES.MODIFIED_AT.name]: MONGO_KEYS.MOD_DATE,
  [BUILTIN_ATTRIBUTES.EXPIRES_AT.name]: MONGO_KEYS.EXP_DATE,
};

// ////
// utils
// ////

const determineFinalOrderByList = (orderByList) => {
  // add default id sorting
  const fullList = _.concat(orderByList || [], {name: ENTITY_ID_KEY, isNegatory: false});
  // remove duplicates (due to mongo sort formatting a duplicate would overwrite previous sort entry)
  const dedupedList = _.uniqBy(fullList, (item) => item.name);
  // and done
  return dedupedList;
};

const determineValuePath = (name) => {
  let path;
  if (name === ENTITY_ID_KEY) {
    path = 'orionId.id';
  } else if (name === ENTITY_TYPE_KEY) {
    path = 'orionId.type';
  } else if (name === GEO_DISTANCE_KEY) {
    throw new Error('geo:distance ordering has not yet been implemented');
  } else if (BUILTINS_FIWARE_TO_MONGO[name]) {
    // and check the builtins
    path = BUILTINS_FIWARE_TO_MONGO[name];
  } else {
    // otherwise we have a normal attr
    path = `attrs.${name}.value`;
  }
  return path;
};


const generateItemPagingStage = (finalOrderByList, isPrevious) => {
  const directionMultiplier = isPrevious ? -1 : 1;
  const fullMatchExpression = {}; // gets mutated!!
  let accBottom = fullMatchExpression;
  _.each(finalOrderByList, (item, index) => {
    const attrPath = determineValuePath(item.name);
    const value = (item.isNegatory ? -1 : 1) * directionMultiplier;
    const direction = value > 0 ? '$gt' : '$lt';
    const isLastItem = index === finalOrderByList.length - 1;
    if (isLastItem) {
      accBottom[direction] = [`$${attrPath}`, `$pageItem.${attrPath}`];
    } else {
      accBottom.$or = [
        {[direction]: [`$${attrPath}`, `$pageItem.${attrPath}`]},
        {$and: [
          {$eq: [`$${attrPath}`, `$pageItem.${attrPath}`]},
          {}, // next accBottom
        ]},
      ];
      accBottom = accBottom.$or[1].$and[1];
    }
  });
  // and done
  const itemPagingStage = {$match: {$expr: fullMatchExpression}};

  // EXAMPLE RESULT:
  // {$match: {$expr: {
  //   $or: [
  //     {$gt: ['$length', $pageItem.length]},
  //     {$and: [
  //       {$eq: ['$length', $pageItem.length]},
  //       {$or: [
  //         {$lt: ['$height', $pageItem.height]},
  //         {$and: [
  //           {$eq: ['$height', $pageItem.height]},
  //           {$lt: ['$orionId.id', '$pageItem.orionId.id']},
  //         ]},
  //       ]},
  //     ]},
  //   ],
  // }}},

  return itemPagingStage;
};

// ////
// stages
// ////


const extractSortStages = (dbArg) => {
  // geostuff doesn't work well with other sorting..
  const geometry = _.get(dbArg, [DB_ENTITY_ARG_KEYS.PARAMS, DB_ARG_PARAMS_KEYS.GEOMETRY]);
  if (geometry) {
    return [];
  }
  // otherwise evaluate from extractions
  const orderByList = _.get(dbArg, [DB_ENTITY_ARG_KEYS.PARAMS, DB_ARG_PARAMS_KEYS.ORDER_BY]);
  const pageDirection = _.get(dbArg, [DB_ENTITY_ARG_KEYS.PARAMS, DB_ARG_PARAMS_KEYS.PAGE_DIRECTION]);
  // process
  const finalOrderByList = determineFinalOrderByList(orderByList);
  const directionMultiplier = pageDirection === PAGING_DIRECTIONS.PREVIOUS ? -1 : 1;
  // eval
  const sortOption = {}; // is mutated!!
  _.each(finalOrderByList, (item) => {
    // the value order-wise
    const value = (item.isNegatory ? -1 : 1) * directionMultiplier;
    const path = determineValuePath(item.name);
    sortOption[path] = value; // mutate!!
  });
  const sortStage = {$sort: sortOption};
  return [sortStage];
};


const extractPagingStages = (dbArg, collectionName) => {
  // get the values if they're there
  const skip = _.get(dbArg, [DB_ENTITY_ARG_KEYS.PARAMS, DB_ARG_PARAMS_KEYS.PAGE_OFFSET]);
  const pageItemId = _.get(dbArg, [DB_ENTITY_ARG_KEYS.PARAMS, DB_ARG_PARAMS_KEYS.PAGE_ITEM_ID]);
  const pageDirection = _.get(dbArg, [DB_ENTITY_ARG_KEYS.PARAMS, DB_ARG_PARAMS_KEYS.PAGE_DIRECTION]);
  const orderByList = _.get(dbArg, [DB_ENTITY_ARG_KEYS.PARAMS, DB_ARG_PARAMS_KEYS.ORDER_BY]);
  // process
  const finalOrderByList = determineFinalOrderByList(orderByList);
  const isPrevious = pageDirection === PAGING_DIRECTIONS.PREVIOUS;
  // if skip offset is used then use it
  if (skip) {
    const skipStages = skip ? [{$skip: skip}] : [];
    return skipStages;
  }

  // create the pipeline stages
  if (pageItemId) {

    const itemStages = [

      // FETCH lastItem
      // fetch the entity to be compared to. OPTIONS: this could be done in a separate
      // operation if lastItem paging needs to be included in initial match (probably a good idea)
      {$lookup: {
        from: collectionName,
        let: {
          pageItemId,
        },
        pipeline: [{
          $match: {$expr:
            {$eq: ['$orionId.id', '$$pageItemId']},
          },
        }],
        as: 'pageItem',
      }},
      {$unwind: {path: '$pageItem'}},

      // GENERATE stage
      // this guy generates a valid resulting stage that passes tests
      // any replacement implementations should be able to pass those (or modified) tests
      generateItemPagingStage(finalOrderByList, isPrevious),
    ];
    return itemStages;
  }

  // default - no paging
  return [];
};


const extractLimitStages = (dbArg) => {
  // get the values if they're there
  const limit = _.get(dbArg, [DB_ENTITY_ARG_KEYS.PARAMS, DB_ARG_PARAMS_KEYS.PAGE_LIMIT]);
  // create the pipeline stage
  const limitStages = limit ? [{$limit: limit}] : [];
  return limitStages;
};


module.exports = {
  extractSortStages,
  extractPagingStages,
  extractLimitStages,
};
