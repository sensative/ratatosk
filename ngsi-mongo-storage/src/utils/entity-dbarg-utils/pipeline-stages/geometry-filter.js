/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const {
  GEOMETRY_QUERY_GEOREL_TYPES,
} = require('@ratatosk/ngsi-constants');

const {
  DEFAULT_GEONEAR_LIMIT,
} = require('../../../constants');

const {
  ngsiLocationUtils: {parseNgsiGeometry},
} = require('@ratatosk/ngsi-utils');

// available GEOMETRY_QUERY_GEOREL_TYPES:
//   NEAR: 'near',
//   COVERED_BY: 'coveredBy',
//   INTERSECTS: 'intersects',
//   EQUALS: 'equals',
//   DISJOINT: 'disjoint',

const generateCoveredByFilter = (geometry) => {
  return [{
    'location.coords': {
      $geoWithin: {
        $geometry: geometry,
      },
    },
  }];
};

const generateEqualsFilter = (geometry) => {
  return [{
    'location.coords': {
      $eq: geometry,
    },
  }];
};

const generateIntersectsFilter = (geometry) => {
  return [{
    'location.coords': {
      $geoIntersects: {
        $geometry: geometry,
      },
    },
  }];
};

const generateDisjointFilter = (geometry) => {
  return [{
    'location.coords': {
      $not: {
        $geoIntersects: {
          $geometry: geometry,
        },
      },
    },
  }];
};

const generateGeometryFilter = (simpleGeo, parsedGeorel) => {
  const geometry = parseNgsiGeometry(simpleGeo.type, simpleGeo.value);

  switch (parsedGeorel.georel) {
    case GEOMETRY_QUERY_GEOREL_TYPES.NEAR: {
      return []; // This one is not allowed in the match stage
    }
    case GEOMETRY_QUERY_GEOREL_TYPES.COVERED_BY: {
      return generateCoveredByFilter(geometry);
    }
    case GEOMETRY_QUERY_GEOREL_TYPES.EQUALS: {
      return generateEqualsFilter(geometry);
    }
    case GEOMETRY_QUERY_GEOREL_TYPES.INTERSECTS: {
      return generateIntersectsFilter(geometry);
    }
    case GEOMETRY_QUERY_GEOREL_TYPES.DISJOINT: {
      return generateDisjointFilter(geometry);
    }
    default: {
      throw new Error('DevErr: the georel list should be exhaustive');
    }
  }
};

const generateGeoNearPipeline = (simpleGeo, comparator, query, customLimit) => {
  const geometry = parseNgsiGeometry(simpleGeo.type, simpleGeo.value);
  const limit = Math.max(customLimit || DEFAULT_GEONEAR_LIMIT, DEFAULT_GEONEAR_LIMIT);
  return {
    $geoNear: {
      near: geometry,
      ...comparator,
      distanceField: 'calculatedDistance',
      spherical: true,
      query,
      limit,
    },
  };
};

module.exports = {
  generateGeometryFilter,
  generateGeoNearPipeline,
};
