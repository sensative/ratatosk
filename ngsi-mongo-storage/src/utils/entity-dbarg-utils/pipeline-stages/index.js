/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {
  GEOMETRY_QUERY_GEOREL_TYPES,
} = require('@ratatosk/ngsi-constants');

const {
  ngsiQueryParamsUtils: {
    geometryParamsParser: {
      parseGeometryAndCoordsParam,
      parseGeorelParam,
    },
  },
  constants: {
    DB_ENTITY_ARG_KEYS,
    DB_ARG_PARAMS_KEYS,
  },
  ngsiStorageErrorUtils: {errors: storageErrors},
} = require('@ratatosk/ngsi-utils');

const {
  generateGeoNearPipeline,
} = require('./geometry-filter');

const {
  generateDbargFilter,
} = require('./dbarg-filter');

const {
  extractSortStages,
  extractPagingStages,
  extractLimitStages,
} = require('./sort-and-paging-stages');

const extractMatchStage = (dbArg) => {
  // construct the general filter
  const matchFilter = generateDbargFilter(dbArg);

  // $geoNear is special. all this is **in case** $geoNear
  const geometry = _.get(dbArg, [DB_ENTITY_ARG_KEYS.PARAMS, DB_ARG_PARAMS_KEYS.GEOMETRY]);
  if (geometry) {
    // the other params
    const coords = _.get(dbArg, [DB_ENTITY_ARG_KEYS.PARAMS, DB_ARG_PARAMS_KEYS.COORDS]);
    const georelArg = _.get(dbArg, [DB_ENTITY_ARG_KEYS.PARAMS, DB_ARG_PARAMS_KEYS.GEOREL]);
    const customLimit = _.get(dbArg, [DB_ENTITY_ARG_KEYS.PARAMS, DB_ARG_PARAMS_KEYS.PAGE_LIMIT]);
    // massage the values a bit
    const simpleGeo = parseGeometryAndCoordsParam(geometry, coords);
    const georelObj = parseGeorelParam(georelArg, geometry);
    // if geonear, return the geonearStage
    if (georelObj.georel === GEOMETRY_QUERY_GEOREL_TYPES.NEAR) {
      const comparator = _.omit(georelObj, 'georel');
      const geoNearStage = generateGeoNearPipeline(
        simpleGeo,
        comparator,
        matchFilter,
        customLimit,
      );
      return geoNearStage;
    }
  }
  // otherwise just return the normal stage
  return {$match: matchFilter};
};

const extractAccessStage = (dbArg) => {
  const accessStage = _.get(dbArg, DB_ENTITY_ARG_KEYS.CUSTOM);
  if (accessStage && !_.isArray(accessStage) && !_.isFunction(accessStage)) {
    throw storageErrors.INVALID_CUSTOM_ARG();
  }
  return _.isArray(accessStage) ? accessStage : [];
};

const extractCustomQueryGenerator = (dbArg) => {
  const queryGenerator = _.get(dbArg, DB_ENTITY_ARG_KEYS.CUSTOM);
  if (queryGenerator && !_.isArray(queryGenerator) && !_.isFunction(queryGenerator)) {
    throw storageErrors.INVALID_CUSTOM_ARG();
  }
  return _.isFunction(queryGenerator) ? queryGenerator : null;
};


module.exports = {
  extractMatchStage,
  extractAccessStage,
  extractCustomQueryGenerator,

  extractSortStages,
  extractPagingStages,
  extractLimitStages,
};
