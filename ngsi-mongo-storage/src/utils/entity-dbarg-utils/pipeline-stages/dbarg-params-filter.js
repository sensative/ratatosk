/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

'use strict';

const _ = require('lodash');

const {
  ngsiQueryParamsUtils: {
    basicParamParser: {
      parseEntityIdParam,
      parseEntityTypeParam,
    },
    geometryParamsParser: {
      parseGeometryAndCoordsParam,
      parseGeorelParam,
    },
  },
  constants: {
    DB_ARG_PARAMS_KEYS,
    NGSI_SQL_QUERY_TYPES,
  },
} = require('@ratatosk/ngsi-utils');

const {
  generateSqlFilterStack,
} = require('./fiware-sql-filter');

const {
  generateGeometryFilter,
} = require('./geometry-filter');


const generateDbargParamsFilterStack = (params) => {
  // this guy gets mutated throughout
  let filterStack = [];
  const entityId = _.get(params, DB_ARG_PARAMS_KEYS.ENTITY_ID);
  if (entityId) {
    const entityIdList = _.isString(entityId) ? parseEntityIdParam(entityId) : entityId;
    filterStack.push({'orionId.id': {$in: entityIdList}});
  }
  const entityIdPattern = _.get(params, DB_ARG_PARAMS_KEYS.ENTITY_ID_PATTERN);
  if (entityIdPattern) {
    filterStack.push({'orionId.id': new RegExp(entityIdPattern)});
  }
  const entityType = _.get(params, DB_ARG_PARAMS_KEYS.ENTITY_TYPE);
  if (entityType) {
    const entityTypeList = _.isString(entityType) ? parseEntityTypeParam(entityType) : entityType;
    filterStack.push({'orionId.type': {$in: entityTypeList}});
  }
  const typePattern = _.get(params, DB_ARG_PARAMS_KEYS.ENTITY_TYPE_PATTERN);
  if (typePattern) {
    filterStack.push({'orionId.type': new RegExp(typePattern)});
  }
  const attrQuery = _.get(params, DB_ARG_PARAMS_KEYS.ATTRIBUTE_QUERY);
  if (attrQuery) {
    const attrQueryFilterStack = generateSqlFilterStack(attrQuery, NGSI_SQL_QUERY_TYPES.ATTRIBUTE_QUERY);
    filterStack = _.concat(filterStack, attrQueryFilterStack);
  }
  const mdQuery = _.get(params, DB_ARG_PARAMS_KEYS.METADATA_QUERY);
  if (mdQuery) {
    const mdQueryFilterStack = generateSqlFilterStack(mdQuery, NGSI_SQL_QUERY_TYPES.METADATA_QUERY);
    filterStack = _.concat(filterStack, mdQueryFilterStack);
  }
  const geometry = _.get(params, DB_ARG_PARAMS_KEYS.GEOMETRY);
  if (geometry) {
    const coords = _.get(params, DB_ARG_PARAMS_KEYS.COORDS);
    const georel = _.get(params, DB_ARG_PARAMS_KEYS.GEOREL);
    const simpleGeo = parseGeometryAndCoordsParam(geometry, coords);
    const geometryFilterStack = generateGeometryFilter(
      simpleGeo,
      parseGeorelParam(georel, geometry),
    );
    filterStack = _.concat(filterStack, geometryFilterStack);
  }
  return filterStack;
};

module.exports = {
  generateDbargParamsFilterStack,
};
