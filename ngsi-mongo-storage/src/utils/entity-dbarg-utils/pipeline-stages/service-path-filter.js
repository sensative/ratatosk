/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {
  SERVICE_PATH_WILDCARD,
} = require('@ratatosk/ngsi-constants');

const generateServicePathFilter = (servicePaths) => {
  const simplePaths = [];
  const wildcardPaths = [];
  _.each(servicePaths, (path) => {
    if (path.charAt(path.length - 1) === SERVICE_PATH_WILDCARD) {
      const matcher = path.substring(0, path.length - 2); // get rid of "/" also
      const pathRegex = new RegExp(`^${matcher}`);
      wildcardPaths.push(pathRegex);
    } else {
      simplePaths.push(path);
    }
  });
  const filter = {
    $or: [
      {'orionId.servicePath': {$in: simplePaths}},
      {'orionId.servicePath': {$in: wildcardPaths}},
    ],
  };
  return filter;
};

module.exports = {
  generateServicePathFilter,
};
