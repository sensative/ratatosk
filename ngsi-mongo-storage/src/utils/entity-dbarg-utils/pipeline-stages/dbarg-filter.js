/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {
  constants: {
    DB_ENTITY_ARG_KEYS,
    DB_ARG_PARAMS_KEYS,
  },
} = require('@ratatosk/ngsi-utils');

const {
  generateServicePathFilter,
} = require('./service-path-filter');

const {
  generateDbargParamsFilterStack,
} = require('./dbarg-params-filter');


const generateDbargFilter = (dbArg) => {
  // this guy gets mutated throughout & is returned
  let filterStack = [];
  // first go through non-params
  const entityId = _.get(
    dbArg,
    DB_ENTITY_ARG_KEYS.ENTITY_ID,
    _.get(dbArg, [DB_ENTITY_ARG_KEYS.ENTITY, DB_ARG_PARAMS_KEYS.ENTITY_ID]),
  );
  if (entityId) {
    filterStack.push({'orionId.id': entityId});
  }
  const type = _.get(dbArg, [DB_ENTITY_ARG_KEYS.ENTITY, DB_ARG_PARAMS_KEYS.ENTITY_TYPE]);
  if (type) {
    filterStack.push({'orionId.type': type});
  }
  const servicePaths = _.get(dbArg, DB_ENTITY_ARG_KEYS.SERVICE_PATHS);
  if (servicePaths) {
    filterStack.push(generateServicePathFilter(servicePaths));
  }
  // then the params
  const params = _.get(dbArg, DB_ENTITY_ARG_KEYS.PARAMS);
  if (params) {
    filterStack = _.concat(filterStack, generateDbargParamsFilterStack(params));
  }
  const filter = {$and: filterStack};
  return filter;
};

module.exports = {
  generateDbargFilter,
};
