/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {
  constants: {QUERY_PREDICATE_TYPES, NGSI_SQL_QUERY_TYPES},
} = require('@ratatosk/ngsi-utils');

const {
  QUERY_BINARY_OPERATORS,
} = require('@ratatosk/ngsi-constants');


const generateBinaryFilterItem = (op, value, path) => {
  switch (op) {
    case QUERY_BINARY_OPERATORS.EQUALS:
    case QUERY_BINARY_OPERATORS.COLON_EQUALS: {
      if (_.isPlainObject(value)) {
        // range
        return {
          $and: [
            {[path]: {$lt: value.max}},
            {[path]: {$gte: value.min}},
          ],
        };
      } else if (_.isArray(value)) {
        // list
        return {[path]: {$in: value}};
      } else {
        // single element
        return {
          $and: [
            {[path]: {$exists: true}}, // null makes this necessary
            {[path]: {$eq: value}},
          ],
        };
      }
    }
    case QUERY_BINARY_OPERATORS.UNEQUAL: {
      if (_.isObject(value)) {
        // range
        return {
          $or: [
            {[path]: {$gte: value.max}},
            {[path]: {$lt: value.min}},
          ],
        };
      } else if (_.isArray(value)) {
        // list
        return {[path]: {$nin: value}};
      } else {
        // single element
        return {
          $and: [
            {[path]: {$exists: true}},
            {[path]: {$ne: value}},
          ],
        };
      }
    }
    case QUERY_BINARY_OPERATORS.GREATER_THAN: {
      return {[path]: {$gt: value}};
    }
    case QUERY_BINARY_OPERATORS.GREATER_THAN_OR_EQUALS: {
      return {[path]: {$gte: value}};
    }
    case QUERY_BINARY_OPERATORS.LESS_THAN: {
      return {[path]: {$lt: value}};
    }
    case QUERY_BINARY_OPERATORS.LESS_THAN_OR_EQUALS: {
      return {[path]: {$lte: value}};
    }
    case QUERY_BINARY_OPERATORS.MATCH_PATTERN: {
      return {[path]: {$regex: new RegExp(value)}};
    }
    default: {
      throw new Error('DevErr: Invalid sql binary op. This should have been caught by validation');
    }
  }
};

const generateUnaryFilterItem = (path, isNegatory) => {
  return {[path]: {$exists: !isNegatory}};
};

const createPropPath = (sqlItem, queryType) => {
  const {tokens} = sqlItem;
  const isAttrQuery = (queryType === NGSI_SQL_QUERY_TYPES.ATTRIBUTE_QUERY);
  const pathIntro = ['attrs', tokens[0]];
  if (!isAttrQuery) {
    pathIntro.push('md');
    pathIntro.push(tokens[1]);
  }
  pathIntro.push('value');
  const pathOutro = tokens.slice(isAttrQuery ? 1 : 2);
  const pathItems = _.concat(pathIntro, pathOutro);
  const path = _.join(pathItems, '.');
  return path;
};

const generateSqlFilterStack = (queryList, queryType) => {
  let filterStack = [];
  _.each(queryList, (sqlItem) => {
    const path = createPropPath(sqlItem, queryType);
    switch (sqlItem.predicateType) {
      case QUERY_PREDICATE_TYPES.BINARY: {
        filterStack = _.concat(filterStack, generateBinaryFilterItem(sqlItem.op, sqlItem.value, path));
        break;
      }
      case QUERY_PREDICATE_TYPES.UNARY: {
        filterStack = _.concat(filterStack, generateUnaryFilterItem(path, sqlItem.isNegatory));
        break;
      }
      default: {
        throw new Error('DevErr: Invalid predicate type. This should have been caught already by validation');
      }
    }
  });
  return filterStack;
};

module.exports = {
  generateSqlFilterStack,
};
