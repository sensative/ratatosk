/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {
  ngsiServicePathUtils: {validateMultipleServicePathsStrict},
  ngsiDatumValidator: {validateEntityId, validateGeneralValue, validateAttributeName},
  ngsiEntityValidator: {validateEntity, validateAttribute, validateAttrs},
  ngsiQueryParamsUtils: {
    queryParamsParser: {
      assertQueryParamsAllCompatible,

    },
    basicParamParser: {
      validateEntityIdList,
      validateEntityIdParam,
      validateEntityTypeList,
      validateEntityTypeParam,
      validateIdPatternParam,
      validateTypePatternParam,
      validatePageLimitParam,
      validatePageOffsetParam,
      validatePageItemIdParam,
      validatePageDirectionParam,
    },
    geometryParamsParser: {
      parseGeometryAndCoordsParam,
      parseGeorelParam,
    },
    attrsParamParser: {
      validateAttrsList,
    },
    metadataParamParser: {
      validateMetadataList,
    },
    orderbyParamParser: {
      validateOrderbyList,
    },
  },
  ngsiSql: {queryParser: {validateQueryList}},
  constants: {
    DB_ENTITY_ARG_KEYS,
    DB_ARG_PARAMS_KEYS,
    NGSI_SQL_QUERY_TYPES,
  },
} = require('@ratatosk/ngsi-utils');

const {
  ENTITY_REPRESENTATION_OPTIONS,
  DEFAULT_PAGE_SIZE,
  SERVICE_PATH_DEFAULT_UPDATE_PATH,
  SERVICE_PATH_DEFAULT_QUERY_PATH,
} = require('@ratatosk/ngsi-constants');

const invalidDbArg = () => new Error('dbArg must be an object if included');
const invalidDbArgKeys = () => new Error('Invalid db arg keys');
const invalidDbArgParams = () => new Error('dbArg.params must be an object if included');
const invalidDbArgParamsKeys = (invalidKeys, allowedKeys) => new Error(`Invalid dbArg params keys [${invalidKeys.join(', ')}], allowedKeys = [${allowedKeys.join(', ')}]`);

const invalidTotalCountParam = () => new Error('Invalid total count param');
const invalidStrictAppendParam = () => new Error('Invalid strict append param');
const invalidRepresentationTypeParam = () => new Error('Invalid representation type param');

const validateDbArgParams = (params) => {

  if (!_.isUndefined(params) && !_.isObject(params)) {
    throw invalidDbArgParams();
  }
  const diff = _.difference(_.keys(params), _.values(DB_ARG_PARAMS_KEYS));
  if (_.size(diff)) {
    throw invalidDbArgParamsKeys(diff, _.values(DB_ARG_PARAMS_KEYS));
  }
  assertQueryParamsAllCompatible(params);

  if (_.has(params, DB_ARG_PARAMS_KEYS.ENTITY_ID_PATTERN)) {
    const idPatternParam = params[DB_ARG_PARAMS_KEYS.ENTITY_ID_PATTERN];
    validateIdPatternParam(idPatternParam);
  }

  if (_.has(params, DB_ARG_PARAMS_KEYS.ENTITY_TYPE_PATTERN)) {
    const typePatternParam = params[DB_ARG_PARAMS_KEYS.ENTITY_TYPE_PATTERN];
    validateTypePatternParam(typePatternParam);
  }

  if (_.has(params, DB_ARG_PARAMS_KEYS.ENTITY_ID)) {
    const entityId = params[DB_ARG_PARAMS_KEYS.ENTITY_ID];
    if (_.isString(entityId)) {
      validateEntityIdParam(entityId);
    } else {
      validateEntityIdList(entityId);
    }
  }

  if (_.has(params, DB_ARG_PARAMS_KEYS.ENTITY_TYPE)) {
    const entityType = params[DB_ARG_PARAMS_KEYS.ENTITY_TYPE];
    if (_.isString(entityType)) {
      validateEntityTypeParam(entityType);
    } else {
      validateEntityTypeList(entityType);
    }
  }

  if (_.has(params, DB_ARG_PARAMS_KEYS.ATTRS_LIST)) {
    const attrsList = params[DB_ARG_PARAMS_KEYS.ATTRS_LIST];
    validateAttrsList(attrsList);
  }

  if (_.has(params, DB_ARG_PARAMS_KEYS.METADATA_LIST)) {
    const metadataList = params[DB_ARG_PARAMS_KEYS.METADATA_LIST];
    validateMetadataList(metadataList);
  }

  if (_.has(params, DB_ARG_PARAMS_KEYS.ATTRIBUTE_QUERY)) {
    const attrQuery = params[DB_ARG_PARAMS_KEYS.ATTRIBUTE_QUERY];
    validateQueryList(attrQuery, NGSI_SQL_QUERY_TYPES.ATTRIBUTE_QUERY);
  }

  if (_.has(params, DB_ARG_PARAMS_KEYS.METADATA_QUERY)) {
    const mdQuery = params[DB_ARG_PARAMS_KEYS.METADATA_QUERY];
    validateQueryList(mdQuery, NGSI_SQL_QUERY_TYPES.METADATA_QUERY);
  }

  if (_.has(params, DB_ARG_PARAMS_KEYS.ORDER_BY)) {
    const orderBy = params[DB_ARG_PARAMS_KEYS.ORDER_BY];
    validateOrderbyList(orderBy);
  }

  if (_.has(params, DB_ARG_PARAMS_KEYS.PAGE_LIMIT)) {
    const pageLimit = params[DB_ARG_PARAMS_KEYS.PAGE_LIMIT];
    validatePageLimitParam(pageLimit);
  }

  if (_.has(params, DB_ARG_PARAMS_KEYS.PAGE_OFFSET)) {
    const pageOffset = params[DB_ARG_PARAMS_KEYS.PAGE_OFFSET];
    validatePageOffsetParam(pageOffset);
  }

  if (_.has(params, DB_ARG_PARAMS_KEYS.TOTAL_COUNT)) {
    const totalCount = params[DB_ARG_PARAMS_KEYS.TOTAL_COUNT];
    if (!_.isBoolean(totalCount)) {
      throw invalidTotalCountParam();
    }
  }

  if (_.has(params, DB_ARG_PARAMS_KEYS.STRICT_APPEND)) {
    const strictAppend = params[DB_ARG_PARAMS_KEYS.STRICT_APPEND];
    if (!_.isBoolean(strictAppend)) {
      throw invalidStrictAppendParam();
    }
  }

  if (_.has(params, DB_ARG_PARAMS_KEYS.REPRESENTATION_TYPE)) {
    const representationType = params[DB_ARG_PARAMS_KEYS.REPRESENTATION_TYPE];
    if (!_.includes(ENTITY_REPRESENTATION_OPTIONS, representationType)) {
      throw invalidRepresentationTypeParam();
    }
  }

  if (_.has(params, DB_ARG_PARAMS_KEYS.GEOMETRY)) {
    const georel = _.get(params, DB_ARG_PARAMS_KEYS.GEOREL);
    const geometry = _.get(params, DB_ARG_PARAMS_KEYS.GEOMETRY);
    const coords = _.get(params, DB_ARG_PARAMS_KEYS.COORDS);
    parseGeometryAndCoordsParam(geometry, coords);
    parseGeorelParam(georel, geometry);
  }

  if (_.has(params, DB_ARG_PARAMS_KEYS.PAGE_ITEM_ID)) {
    const itemId = _.get(params, DB_ARG_PARAMS_KEYS.PAGE_ITEM_ID);
    validatePageItemIdParam(itemId);
  }

  if (_.has(params, DB_ARG_PARAMS_KEYS.PAGE_DIRECTION)) {
    const pageDirection = _.get(params, DB_ARG_PARAMS_KEYS.PAGE_DIRECTION);
    validatePageDirectionParam(pageDirection);
  }
};

const validateDbArg = (dbArg, isUpdate) => {

  if (!_.isUndefined(dbArg) && !_.isObject(dbArg)) {
    throw invalidDbArg();
  }
  const diff = _.difference(_.keys(dbArg), _.values(DB_ENTITY_ARG_KEYS));
  if (_.size(diff)) {
    throw invalidDbArgKeys();
  }

  if (_.has(dbArg, DB_ENTITY_ARG_KEYS.SERVICE_PATHS)) {
    // THIS IS WHERE isUpdate is really needed
    validateMultipleServicePathsStrict(dbArg[DB_ENTITY_ARG_KEYS.SERVICE_PATHS], isUpdate);
  }

  if (_.has(dbArg, DB_ENTITY_ARG_KEYS.ENTITY_ID)) {
    validateEntityId(dbArg[DB_ENTITY_ARG_KEYS.ENTITY_ID]);
  }

  if (_.has(dbArg, DB_ENTITY_ARG_KEYS.ENTITY)) {
    validateEntity(dbArg[DB_ENTITY_ARG_KEYS.ENTITY]);
  }

  if (_.has(dbArg, DB_ENTITY_ARG_KEYS.ATTR_NAME)) {
    validateAttributeName(dbArg[DB_ENTITY_ARG_KEYS.ATTR_NAME]);
  }

  if (_.has(dbArg, DB_ENTITY_ARG_KEYS.ATTR)) {
    validateAttribute(dbArg[DB_ENTITY_ARG_KEYS.ATTR]);
  }

  if (_.has(dbArg, DB_ENTITY_ARG_KEYS.ATTRS)) {
    validateAttrs(dbArg[DB_ENTITY_ARG_KEYS.ATTRS]);
  }

  if (_.has(dbArg, DB_ENTITY_ARG_KEYS.ATTR_VALUE)) {
    validateGeneralValue(dbArg[DB_ENTITY_ARG_KEYS.ATTR_VALUE]);
  }

  if (_.has(dbArg, DB_ENTITY_ARG_KEYS.PARAMS)) {
    validateDbArgParams(dbArg[DB_ENTITY_ARG_KEYS.PARAMS]);
  }

  if (_.has(dbArg, DB_ENTITY_ARG_KEYS.UPSERT)) {
    // used as bool - not validated
  }

  if (_.has(dbArg, DB_ENTITY_ARG_KEYS.CUSTOM)) {
    // this is not validated..
  }

};

const assignDbArgDefaults = (dbArgRaw, isUpdate, requiresSingleMatch) => {
  const dbArg = {...dbArgRaw};
  if (_.isNil(dbArg[DB_ENTITY_ARG_KEYS.SERVICE_PATHS])) {
    const servicePath = isUpdate ? SERVICE_PATH_DEFAULT_UPDATE_PATH : SERVICE_PATH_DEFAULT_QUERY_PATH;
    _.set(dbArg, DB_ENTITY_ARG_KEYS.SERVICE_PATHS, [servicePath]);
  }
  if (!_.has(dbArg, [DB_ENTITY_ARG_KEYS.PARAMS, DB_ARG_PARAMS_KEYS.PAGE_LIMIT])) {
    _.set(dbArg, [DB_ENTITY_ARG_KEYS.PARAMS, DB_ARG_PARAMS_KEYS.PAGE_LIMIT], DEFAULT_PAGE_SIZE);
  }
  if (!_.has(dbArg, [DB_ENTITY_ARG_KEYS.PARAMS, DB_ARG_PARAMS_KEYS.PAGE_OFFSET])) {
    _.set(dbArg, [DB_ENTITY_ARG_KEYS.PARAMS, DB_ARG_PARAMS_KEYS.PAGE_OFFSET], 0);
  }
  if (requiresSingleMatch) {
    // if it requiresSingleMatch then there must be exactly 1. This rides roughshod.
    _.set(dbArg, [DB_ENTITY_ARG_KEYS.PARAMS, DB_ARG_PARAMS_KEYS.PAGE_LIMIT], 2);
    _.set(dbArg, [DB_ENTITY_ARG_KEYS.PARAMS, DB_ARG_PARAMS_KEYS.PAGE_OFFSET], 0);
  }
  return dbArg;
};

const extractValidatedEntityDbarg = (dbArg, options = {}) => {
  const {isUpdate, requiresSingleMatch} = options;
  const validatedDbArg = assignDbArgDefaults(dbArg, isUpdate, requiresSingleMatch);
  validateDbArg(validatedDbArg, isUpdate);
  return validatedDbArg;
};


module.exports = {
  extractValidatedEntityDbarg,
};
