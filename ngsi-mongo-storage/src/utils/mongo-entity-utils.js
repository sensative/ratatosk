/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {
  ngsiDateTimeUtils: {
    toDate,
    fromDate,
    toSeconds,
    fromSeconds,
    hasValidDateTimeFormat,
  },
  ngsiLocationUtils: {
    parseNgsiGeometry,
    serializeNgsiGeometry,
    isNgsiGeometryType,
  },
  ngsiStorageErrorUtils: {
    errors: storageErrors,
  },
} = require('@ratatosk/ngsi-utils');

const {
  DEFAULT_VALUE_TYPES,
  SPECIAL_VALUE_TYPES,
  // GEO_JSON_TYPES,
  BUILTIN_ATTRIBUTES,
  BUILTIN_METADATA,
  DEFAULT_LOCATION_METADATUM_NAME,
  DEFAULT_LOCATION_METADATUM,
} = require('@ratatosk/ngsi-constants');

const MONGO_KEYS = {
  CRE_DATE: 'creDate',
  MOD_DATE: 'modDate',
  EXP_DATE: 'expDate',
};

const transformValueToMongo = (value, type) => {
  if (type === SPECIAL_VALUE_TYPES.DATE_TIME && hasValidDateTimeFormat(value)) {
    return toDate(value);
  }
  if (isNgsiGeometryType(type)) {
    try {
      return parseNgsiGeometry(type, value);
    } catch (err) {
      return value;
    }
  }
  // otherwise does not qualify
  return value;
};

const transformValueFromMongo = (value, type) => {
  if (type === SPECIAL_VALUE_TYPES.DATE_TIME && _.isDate(value)) {
    return fromDate(new Date(value));
  }
  if (isNgsiGeometryType(type)) {
    try {
      return serializeNgsiGeometry(type, value);
    } catch (err) {
      return value;
    }
  }
  return value;
};

const transformBuiltinAttrsToMongo = (timestamps = {}) => {
  const entityTimestamps = {}; // mutate this guy
  if (timestamps[BUILTIN_ATTRIBUTES.CREATED_AT.name]) {
    const createdAt = timestamps[BUILTIN_ATTRIBUTES.CREATED_AT.name].value;
    entityTimestamps[MONGO_KEYS.CRE_DATE] = toDate(createdAt);
  }
  if (timestamps[BUILTIN_ATTRIBUTES.MODIFIED_AT.name]) {
    const modifiedAt = timestamps[BUILTIN_ATTRIBUTES.MODIFIED_AT.name].value;
    entityTimestamps[MONGO_KEYS.MOD_DATE] = toDate(modifiedAt);
  }
  if (timestamps[BUILTIN_ATTRIBUTES.EXPIRES_AT.name]) {
    const expiresAt = timestamps[BUILTIN_ATTRIBUTES.EXPIRES_AT.name].value;
    entityTimestamps[MONGO_KEYS.EXP_DATE] = toDate(expiresAt);
  }
  return entityTimestamps;
};

const extractBuiltinAttrsFromMongo = (mongoEntity) => {
  const builtinAttrs = {};
  if (mongoEntity[MONGO_KEYS.CRE_DATE]) {
    builtinAttrs[BUILTIN_ATTRIBUTES.CREATED_AT.name] = {
      value: fromDate(mongoEntity[MONGO_KEYS.CRE_DATE]),
      type: SPECIAL_VALUE_TYPES.DATE_TIME,
    };
  }
  if (mongoEntity[MONGO_KEYS.MOD_DATE]) {
    builtinAttrs[BUILTIN_ATTRIBUTES.MODIFIED_AT.name] = {
      value: fromDate(mongoEntity[MONGO_KEYS.MOD_DATE]),
      type: SPECIAL_VALUE_TYPES.DATE_TIME,
    };
  }
  if (mongoEntity[MONGO_KEYS.EXP_DATE]) {
    builtinAttrs[BUILTIN_ATTRIBUTES.EXPIRES_AT.name] = {
      value: fromDate(mongoEntity[MONGO_KEYS.EXP_DATE]),
      type: SPECIAL_VALUE_TYPES.DATE_TIME,
    };
  }
  builtinAttrs[BUILTIN_ATTRIBUTES.FIWARE_ID.name] = {
    value: mongoEntity._id.toString(),
    type: DEFAULT_VALUE_TYPES.TEXT,
  };
  return builtinAttrs;
};

const transformBuiltinMetadataToMongo = (timestamps = {}) => {
  const attrTimestamps = {}; // mutate this guy
  if (timestamps[BUILTIN_METADATA.CREATED_AT.name]) {
    const createdAt = timestamps[BUILTIN_METADATA.CREATED_AT.name].value || new Date().toISOString();
    attrTimestamps[MONGO_KEYS.CRE_DATE] = toDate(createdAt);
  }
  if (timestamps[BUILTIN_METADATA.MODIFIED_AT.name]) {
    const modifiedAt = timestamps[BUILTIN_METADATA.MODIFIED_AT.name].value || new Date().toISOString();
    attrTimestamps[MONGO_KEYS.MOD_DATE] = toDate(modifiedAt);
  }
  return attrTimestamps;
};

const extractBuiltinMetadataFromMongo = (attr) => {
  const builtinMetadata = {
    [BUILTIN_METADATA.CREATED_AT.name]: {
      value: attr[MONGO_KEYS.CRE_DATE] && fromDate(attr[MONGO_KEYS.CRE_DATE]),
      type: SPECIAL_VALUE_TYPES.DATE_TIME,
    },
    [BUILTIN_METADATA.MODIFIED_AT.name]: {
      value: attr[MONGO_KEYS.MOD_DATE] && fromDate(attr[MONGO_KEYS.MOD_DATE]),
      type: SPECIAL_VALUE_TYPES.DATE_TIME,
    },
  };
  return builtinMetadata;
};

const transformAttrsToMongo = (extendedAttrs) => {
  const attrs = _.mapValues(extendedAttrs, (attr) => ({
    type: attr.type,
    value: transformValueToMongo(attr.value, attr.type),
    ...transformBuiltinMetadataToMongo(attr.builtinMetadata),
    md: _.mapValues(attr.metadata, (datum) => ({
      value: transformValueToMongo(datum.value, datum.type),
      type: datum.type,
    })),
  }));
  return attrs;
};

const parseMongoLocationItem = (attrName, attr) => {
  try {
    const location = {
      attrName,
      coords: parseNgsiGeometry(attr.type, attr.value),
    };
    return {location};
  } catch (err) {
    return {};
  }
};

const extractMongoLocation = (extendedAttrs) => {
  // determine which attrs are contenders
  // this gets a bit messy, so will do it janky
  let numContenders = 0;
  let firstContender = null;
  let firstContenderName = null;
  let numAssignedContenders = 0;
  let firstAssignedContender = null;
  let firstAssignedContenderName = null;
  _.each(extendedAttrs, (attr, attrName) => {
    if (isNgsiGeometryType(attr.type)) {
      // check if geo attr
      if (!numContenders) {
        firstContender = attr;
        firstContenderName = attrName;
      }
      numContenders += 1;
      // check if it assigns defaultLocation
      const flag = _.get(attr, ['metadata', DEFAULT_LOCATION_METADATUM_NAME]);
      const flagEnables = _.isEqual(flag, DEFAULT_LOCATION_METADATUM);
      if (flagEnables) {
        if (!numAssignedContenders) {
          firstAssignedContender = attr;
          firstAssignedContenderName = attrName;
        }
        numAssignedContenders += 1;
      }
    }
  });
  // if no contenders then we are done
  if (!numContenders) {
    return {};
  }
  // if there is only one contender then it is it
  if (numContenders === 1) {
    return parseMongoLocationItem(firstContenderName, firstContender);
  }
  // otherwise we have more. check if any have been assigned
  if (!numAssignedContenders) {
    throw storageErrors.MULTIPLE_GEOATTRS_BUT_NO_DEFAULT();
  }
  // if there is just the one..
  if (numAssignedContenders === 1) {
    return parseMongoLocationItem(firstAssignedContenderName, firstAssignedContender);
  }
  // otherwise we have too many
  throw storageErrors.MULTIPLE_GEOATTRS_AND_MULTIPLE_DEFAULTS();
};

const transformEntityToMongo = (extendedEntity) => {
  const mongoAttrs = transformAttrsToMongo(extendedEntity.attrs);
  const mongoEntity = {
    orionId: {
      id: extendedEntity.id,
      type: extendedEntity.type,
      servicePath: extendedEntity.servicePath,
    },
    ...transformBuiltinAttrsToMongo(extendedEntity.builtinAttrs),
    ...extractMongoLocation(extendedEntity.attrs),
    attrs: mongoAttrs,
  };
  return mongoEntity;
};

const transformEntityFromMongo = (mongoEntity) => {
  const extendedEntity = {
    id: mongoEntity.orionId.id,
    type: mongoEntity.orionId.type,
    servicePath: mongoEntity.orionId.servicePath,
    attrs: _.mapValues(mongoEntity.attrs, (attr) => ({
      type: attr.type,
      value: transformValueFromMongo(attr.value, attr.type),
      builtinMetadata: extractBuiltinMetadataFromMongo(attr),
      metadata: _.mapValues(attr.md, (datum) => ({
        type: datum.type,
        value: transformValueFromMongo(datum.value, datum.type),
      })),
    })),
    builtinAttrs: extractBuiltinAttrsFromMongo(mongoEntity),
  };
  return extendedEntity;
};

//
// Convert TO orion
//

const convertValueToOrion = (value, type) => {
  if (type === SPECIAL_VALUE_TYPES.DATE_TIME && _.isDate(value)) {
    return toSeconds(fromDate(new Date(value)));
  }
  if (isNgsiGeometryType(type)) {
    try {
      return serializeNgsiGeometry(type, value);
    } catch (err) {
      return value;
    }
  }
  return value;
};

const convertMetadataToOrion = (mongoMetadata) => {
  const orionMetadata = {
    type: mongoMetadata.type,
    value: convertValueToOrion(mongoMetadata.value, mongoMetadata.type),
  };
  return orionMetadata;
};

const convertAttrToOrion = (mongoAttr) => {
  const orionAttr = {
    mdNames: _.keys(mongoAttr.md),
    md: _.mapValues(mongoAttr.md, convertMetadataToOrion),
    creDate: toSeconds(fromDate(mongoAttr.creDate)),
    modDate: toSeconds(fromDate(mongoAttr.modDate)),
    type: mongoAttr.type,
    value: convertValueToOrion(mongoAttr.value, mongoAttr.type),
  };
  return orionAttr;
};

const convertEntityToOrion = (mongoEntity) => {
  const orionEntity = {
    _id: mongoEntity.orionId,
    creDate: toSeconds(fromDate(mongoEntity.creDate)),
    modDate: toSeconds(fromDate(mongoEntity.modDate)),
    attrNames: _.keys(mongoEntity.attrs),
    attrs: _.mapValues(mongoEntity.attrs, convertAttrToOrion),
    location: mongoEntity.location, // can be nil (gets removed by compact if so)
  };
  return orionEntity;
};

//
// Convert FROM Orion
//

const convertValueFromOrion = (value, type) => {
  if (type === SPECIAL_VALUE_TYPES.DATE_TIME && _.isInteger(value)) {
    return toDate(fromSeconds(value));
  }
  if (isNgsiGeometryType(type)) {
    try {
      return parseNgsiGeometry(type, value);
    } catch (err) {
      return value;
    }
  }
  return value;
};

const convertMetadataFromOrion = (orionMetadata) => {
  const mongoMetadata = {
    type: orionMetadata.type,
    value: convertValueFromOrion(orionMetadata.value, orionMetadata.type),
  };
  return mongoMetadata;
};

const convertAttrFromOrion = (orionAttr) => {
  const mongoAttr = {
    type: orionAttr.type,
    value: convertValueFromOrion(orionAttr.value, orionAttr.type),
    creDate: toDate(fromSeconds(orionAttr.creDate)),
    modDate: toDate(fromSeconds(orionAttr.modDate)),
    md: _.mapValues(orionAttr.md, convertMetadataFromOrion),
  };
  return mongoAttr;
};

const convertEntityFromOrion = (orionEntity) => {
  const mongoEntity = {
    orionId: orionEntity._id,
    creDate: toDate(fromSeconds(orionEntity.creDate)),
    modDate: toDate(fromSeconds(orionEntity.modDate)),
    location: orionEntity.location,
    attrs: _.mapValues(orionEntity.attrs, convertAttrFromOrion),
  };
  return mongoEntity;
};


//
// generate update operators from a mongo entity
//

const generateEntityUpdateOperators = (mongoEntity, oldDoc) => {
  // basics will always be present
  const basics = {
    orionId: mongoEntity.orionId,
    modDate: mongoEntity.modDate,
    creDate: oldDoc ? oldDoc.creDate : mongoEntity.creDate,
  };
  // location MIGHT be included
  const locationValue = mongoEntity.location || _.get(oldDoc, 'location');
  const location = locationValue ? {location: locationValue} : {};
  // and attrs are variable
  const attrs = _.reduce(mongoEntity.attrs, (acc, attr, name) => {
    const oldAttr = _.get(oldDoc, ['attrs', name]);
    // if we do not have an old attr, just add it in wholesale
    if (!oldAttr) {
      acc[`attrs.${name}`] = attr;
      return acc;
    }
    // otherwise combine the new with the old
    const attrUpdates = {
      [`attrs.${name}.type`]: attr.type,
      [`attrs.${name}.value`]: attr.value,
      [`attrs.${name}.creDate`]: oldAttr.creDate,
      [`attrs.${name}.modDate`]: attr.modDate,
      [`attrs.${name}.md`]: attr.md,
    };
    return _.assign(acc, attrUpdates);
  }, {});
  const updateOperators = {
    $set: _.assign(
      basics,
      location,
      attrs,
    ),
  };
  return updateOperators;
};

//
// exports
//

module.exports = {
  transformEntityToMongo,
  transformEntityFromMongo,
  convertEntityFromOrion,
  convertEntityToOrion,
  generateEntityUpdateOperators,
};
