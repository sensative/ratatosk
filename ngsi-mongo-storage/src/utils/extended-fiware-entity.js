/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {
  ngsiDateTimeUtils: {fromDate},
  ngsiStorageErrorUtils: {errors: ngsiStorageErrors},
} = require('@ratatosk/ngsi-utils');

const {
  SERVICE_PATH_DEFAULT_UPDATE_PATH,
  SPECIAL_VALUE_TYPES,
  BUILTIN_ATTRIBUTES,
  // representiation & ordering
  QUERY_ATTRIBUTE_LIST_WILDCARD,
  ENTITY_REPRESENTATION_OPTIONS,
  DEFAULT_ENTITY_REPRESENTATION_TYPE,
} = require('@ratatosk/ngsi-constants');

const {
  ATTRS_UPDATE_TYPES,
} = require('../constants');

const getTimestamps = () => {
  const now = fromDate(new Date());
  const timestamp = {
    value: now,
    type: SPECIAL_VALUE_TYPES.DATE_TIME,
  };
  return {
    [BUILTIN_ATTRIBUTES.CREATED_AT.name]: timestamp,
    [BUILTIN_ATTRIBUTES.MODIFIED_AT.name]: timestamp,
  };
};

const extendFiwareEntity = (normalizedEntity, options = {}) => {
  const {servicePath, initEntity, initAttrs} = options;
  const timestamps = getTimestamps();
  const rawAttrs = _.omit(normalizedEntity, ['id', 'type']);
  const getAttrTimestamps = (attrName) => {
    if (_.isArray(initAttrs)) {
      return _.includes(initAttrs, attrName) ? timestamps : {};
    } else {
      return initAttrs ? timestamps : {};
    }
  };
  const expandedEntity = {
    id: normalizedEntity.id,
    type: normalizedEntity.type,
    servicePath: servicePath || SERVICE_PATH_DEFAULT_UPDATE_PATH,
    attrs: _.mapValues(rawAttrs, (attr, attrName) => ({
      value: attr.value,
      type: attr.type,
      metadata: attr.metadata,
      builtinMetadata: getAttrTimestamps(attrName),
    })),
    builtinAttrs: _.assign({}, initEntity ? timestamps : {}),
  };
  return expandedEntity;
};

const assertFiwareAttrsExist = (extendedEntity, attrs) => {
  parseAndValidateAttrs(extendedEntity, attrs); // no return value
};

const deleteFiwareAttrs = (extendedEntity, attrs) => {
  const timestamps = getTimestamps();
  const modifiedTimestamp = _.pick(timestamps, BUILTIN_ATTRIBUTES.MODIFIED_AT.name);
  const attrNames = parseAndValidateAttrs(extendedEntity, attrs);
  const modifiedEntity = {
    ...extendedEntity,
    attrs: _.omit(extendedEntity.attrs, attrNames),
    builtinAttrs: {
      ...timestamps,
      ...extendedEntity.builtinAttrs,
      ...modifiedTimestamp,
    },
  };
  return modifiedEntity;
};

const updateFiwareAttrs = (extendedEntity, attrs, attrsUpdateType) => {
  const {
    // UPSERT_ATTRS, // unused - is the most permissive (i.e. default case)
    REPLACE_ATTRS,
    STRICT_APPEND_ATTRS,
    PATCH_ATTRS,
    DELETE_ATTRS, // the special special case
    UPDATE_ATTR_VALUES,
  } = ATTRS_UPDATE_TYPES;
  // check for the DELETE_ATTRS special case
  if (attrsUpdateType === DELETE_ATTRS) {
    return deleteFiwareAttrs(extendedEntity, attrs);
  }
  // otherwise proceed with mutations
  const timestamps = getTimestamps();
  const modifiedTimestamp = _.pick(timestamps, BUILTIN_ATTRIBUTES.MODIFIED_AT.name);
  const attrsAreEqual = (attr1, attr2) => {
    return _.isEqual(_.omit(attr1, 'builtinMetadata'), _.omit(attr2, 'builtinMetadata'));
  };
  let entityChanged = false; // if we should update the entity timestamp
  const replacementAttrs = _.mapValues(attrs, (attr, attrName) => {
    const existingAttr = extendedEntity.attrs[attrName];
    if (attrsUpdateType === STRICT_APPEND_ATTRS && !!existingAttr) {
      throw ngsiStorageErrors.CANNOT_CREATE_ATTR_ALREADY_EXISTS();
    }
    if (_.includes([PATCH_ATTRS, UPDATE_ATTR_VALUES], attrsUpdateType) && !existingAttr) {
      throw ngsiStorageErrors.ATTR_DOES_NOT_EXIST();
    }

    const replacementAttr = (attrsUpdateType !== UPDATE_ATTR_VALUES) ? attr : {
      ...existingAttr,
      value: attr.value,
    };
    const attrChanged = !attrsAreEqual(replacementAttr, existingAttr);
    const builtinMetadata = {
      ...timestamps,
      ..._.get(existingAttr, 'builtinMetadata', {}), // will be undefined for new attrs
      ...(attrChanged ? modifiedTimestamp : {}),
    };
    entityChanged = entityChanged || attrChanged;
    const modifiedAttr = {...replacementAttr, builtinMetadata};
    return modifiedAttr;
  });
  if (attrsUpdateType === REPLACE_ATTRS) {
    const willRemoveAttrs = _.size(replacementAttrs) < _.size(extendedEntity.attrs);
    entityChanged = entityChanged || willRemoveAttrs;
  }
  const preExistingAttrs = (attrsUpdateType === REPLACE_ATTRS) ? {} : extendedEntity.attrs;
  const modifiedEntity = {
    ...extendedEntity,
    attrs: {
      ...preExistingAttrs,
      ...replacementAttrs,
    },
    builtinAttrs: {
      ...timestamps,
      ...extendedEntity.builtinAttrs,
      ...(entityChanged ? modifiedTimestamp : {}),
    },
  };
  return modifiedEntity;
};

const parseAndValidateAttrs = (extendedEntity, attrs) => {
  let attrNames = attrs; // array with strings
  if (_.isArray(attrs)) {
    attrNames = attrs;
  } else if (_.isObject(attrs)) {
    attrNames = _.keys(attrs);
  } else if (_.isString(attrs)) {
    attrNames = [attrs];
  } else {
    throw new Error('DevErr: invalid attrs sanity check');
  }
  const currentAttrNames = _.keys(extendedEntity.attrs);
  if (_.size(_.without(attrNames, ...currentAttrNames))) {
    throw ngsiStorageErrors.ATTR_DOES_NOT_EXIST();
  }
  return attrNames;
};

// internal utility function for filtering builtin attrs & md
const filterMembers = (members, list, wildcard) => {
  if (!_.size(list)) {
    return wildcard ? members : {};
  }
  if (_.includes(list, wildcard)) {
    return members;
  }
  return _.reduce(members, (acc, member, name) => {
    if (_.includes(list, name)) {
      acc[name] = member;
    }
    return acc;
  }, {});
};

// internal utility function
const filterMetadata = (attr, metadataList = [], autoIncludeBuiltins = false) => {
  const filteredAttr = {
    ...attr,
    metadata: filterMembers(attr.metadata, metadataList, QUERY_ATTRIBUTE_LIST_WILDCARD),
    builtinMetadata: filterMembers(attr.builtinMetadata, metadataList, autoIncludeBuiltins),
  };
  return filteredAttr;
};

// utility function for applyEntityDisplayOptions
const filterAttrsAndMetadata = (entity, attrsList = [], metadataList = [], autoIncludeBuiltins = false) => {
  const filteredAttrs = filterMembers(entity.attrs, attrsList, QUERY_ATTRIBUTE_LIST_WILDCARD);
  const filteredEntity = {
    ..._.pick(entity, ['id', 'type', 'servicePath']),
    builtinAttrs: filterMembers(entity.builtinAttrs, attrsList, autoIncludeBuiltins),
    attrs: _.mapValues(filteredAttrs, (attr) => filterMetadata(attr, metadataList, autoIncludeBuiltins)),
  };
  return filteredEntity;
};

// utility function for applyEntityDisplayOptions
const extractOrderedAttrValues = (entity, attrsList = []) => {
  const attrs = {
    ...entity.attrs,
    ...entity.builtinAttrs,
  };
  const orderedAttrValues = [];
  const namesFound = [];
  _.each(attrsList, (attrName) => {
    if (_.has(attrs, attrName)) {
      orderedAttrValues.push(attrs[attrName].value);
      namesFound.push(attrName);
    }
  });
  if (!_.size(attrsList) || _.includes(attrsList, QUERY_ATTRIBUTE_LIST_WILDCARD)) {
    const unclaimed = _.omit(entity.attrs, namesFound);
    _.each(unclaimed, (attr) => {
      orderedAttrValues.push(attr.value);
    });
  }
  return orderedAttrValues;
};

const applyEntityDisplayOptions = (entity, representationType = DEFAULT_ENTITY_REPRESENTATION_TYPE, attrsList = [], metadataList = []) => {
  const autoIncludeBuiltins = (representationType === ENTITY_REPRESENTATION_OPTIONS.EXTENDED);
  const filteredEntity = filterAttrsAndMetadata(entity, attrsList, metadataList, autoIncludeBuiltins);
  switch (representationType) {
    case ENTITY_REPRESENTATION_OPTIONS.EXTENDED: {
      return filteredEntity;
    }
    case ENTITY_REPRESENTATION_OPTIONS.NORMALIZED: {
      const normalizedEntity = {
        ..._.pick(filteredEntity, ['id', 'type']),
        ..._.mapValues(filteredEntity.attrs, (attr) => ({
          ..._.pick(attr, ['value', 'type']),
          metadata: {
            ...attr.metadata,
            ...attr.builtinMetadata,
          },
        })),
        ...filteredEntity.builtinAttrs,
      };
      return normalizedEntity;
    }
    case ENTITY_REPRESENTATION_OPTIONS.KEY_VALUES: {
      const keyValueEntity = {
        ..._.pick(filteredEntity, ['id', 'type']),
        ..._.mapValues(filteredEntity.attrs, (attr) => attr.value),
        ..._.mapValues(filteredEntity.builtinAttrs, (attr) => attr.value),
      };
      return keyValueEntity;
    }
    case ENTITY_REPRESENTATION_OPTIONS.VALUES: {
      return extractOrderedAttrValues(filteredEntity, attrsList);
    }
    case ENTITY_REPRESENTATION_OPTIONS.UNIQUE: {
      return _.uniq(extractOrderedAttrValues(filteredEntity, attrsList));
    }
    case ENTITY_REPRESENTATION_OPTIONS.PEEK: {
      const peek = {
        fiwareId: entity.builtinAttrs.fiwareId.value, // mongodb _id
        id: entity.id,
        type: entity.type,
        ..._.omitBy({
          name: _.get(entity, 'attrs.name.value'),
          description: _.get(entity, 'attrs.description.value'),
          downlinkQueue: _.get(entity, 'attrs.downlinkQueue.value'),
          servicePath: entity.servicePath || null, // kanske skicka '/' till null?
        }, (value) => _.isNil(value)),
      };
      return peek;
    }
    default: {
      throw new Error('Invalid representation type. This should already have been validated.');
    }
  }
};

const applyAttrDisplayOptions = (attr, representationType = DEFAULT_ENTITY_REPRESENTATION_TYPE, metadataList = []) => {
  const autoIncludeBuiltins = (representationType === ENTITY_REPRESENTATION_OPTIONS.EXTENDED);
  const filteredAttr = filterMetadata(attr, metadataList, autoIncludeBuiltins);
  switch (representationType) {
    case ENTITY_REPRESENTATION_OPTIONS.EXTENDED: {
      return filteredAttr;
    }
    case ENTITY_REPRESENTATION_OPTIONS.NORMALIZED: {
      const normalizedAttr = {
        ..._.pick(filteredAttr, ['value', 'type']),
        metadata: filteredAttr.metadata,
      };
      return normalizedAttr;
    }
    case ENTITY_REPRESENTATION_OPTIONS.PEEK: {
      return null; // should this be an error?
    }
    case ENTITY_REPRESENTATION_OPTIONS.KEY_VALUES:
    case ENTITY_REPRESENTATION_OPTIONS.VALUES:
    case ENTITY_REPRESENTATION_OPTIONS.UNIQUE: {
      return filteredAttr.value;
    }
    default: {
      throw new Error('Invalid representation type. This should already have been validated.');
    }
  }

};

module.exports = {
  extendFiwareEntity,
  assertFiwareAttrsExist,
  updateFiwareAttrs,
  applyEntityDisplayOptions,
  applyAttrDisplayOptions,
};
