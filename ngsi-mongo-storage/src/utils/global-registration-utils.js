/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {
  ATTRS_UPDATE_TYPES,
} = require('../constants');

const {
  ngsiStorageErrorUtils: {errors: storageErrors},
} = require('@ratatosk/ngsi-utils');

const untangleGlobalRegistrations = (fiwareInput, globalRegistrationAttrs, attrUpdateType) => {
  const registrations = _.pick(fiwareInput, globalRegistrationAttrs);
  const fiware = _.omit(fiwareInput, globalRegistrationAttrs);
  if (attrUpdateType === ATTRS_UPDATE_TYPES.STRICT_APPEND_ATTRS && _.size(registrations)) {
    throw storageErrors.CANNOT_CREATE_ATTR_COLLIDES_WITH_REGISTRATION();
  }
  return {fiware, registrations};
};

module.exports = {
  untangleGlobalRegistrations,
};
