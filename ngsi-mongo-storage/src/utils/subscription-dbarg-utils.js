/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {
  ngsiServicePathUtils: {validateMultipleServicePathsStrict},
  ngsiQueryParamsUtils: {
    subscriptionPayloadParser: {
      validateSubscriptionPayload,
    },
  },
  ngsiStorageErrorUtils: {
    errors: storagErrors,
  },
  constants: {
    DB_SUBSCRIPTION_ARG_KEYS,
  },
} = require('@ratatosk/ngsi-utils');


const validateSubscriptionDbarg = (subscriptionQueryArg) => {
  if (!_.isUndefined(subscriptionQueryArg) && !_.isObject(subscriptionQueryArg)) {
    throw storagErrors.INVALID_SUBSCRIPTION_DBARG_FORMAT();
  }
  const diff = _.difference(_.keys(subscriptionQueryArg), _.values(DB_SUBSCRIPTION_ARG_KEYS));
  if (_.size(diff)) {
    throw storagErrors.INVALID_SUBSCRIPTION_DBARG_KEYS();
  }

  if (_.has(subscriptionQueryArg, DB_SUBSCRIPTION_ARG_KEYS.SERVICE_PATHS)) {
    const isUpdate = true;
    validateMultipleServicePathsStrict(subscriptionQueryArg[DB_SUBSCRIPTION_ARG_KEYS.SERVICE_PATHS], isUpdate);
  }
  if (_.has(subscriptionQueryArg, DB_SUBSCRIPTION_ARG_KEYS.PAYLOAD)) {
    validateSubscriptionPayload(subscriptionQueryArg[DB_SUBSCRIPTION_ARG_KEYS.PAYLOAD]);
  }
};

module.exports = {
  validateSubscriptionDbarg,
};
