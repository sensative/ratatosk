/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const {
  ORION_ENTITITIES_DB_COLLECTION,
  ORION_SUBSCRIPTIONS_DB_COLLECTION,
} = require('@ratatosk/ngsi-constants');

//
// entities
//

const wipeEntities = (db) => async () => {
  if (!db) {
    throw new Error('not connected. cannot wipe entities');
  }
  const collection = db.collection(ORION_ENTITITIES_DB_COLLECTION);
  return collection.deleteMany()
    .then((res) => res.deletedCount);
};

const getEntities = (db) => async (filter) => {
  if (!db) {
    throw new Error('not connected. cannot find entities');
  }
  const collection = db.collection(ORION_ENTITITIES_DB_COLLECTION);
  const docs = await collection.find(filter).toArray();
  return docs;
};

const countEntities = (db) => async () => {
  if (!db) {
    throw new Error('not connected. cannot find entities');
  }
  const collection = db.collection(ORION_ENTITITIES_DB_COLLECTION);
  const numDocs = await collection.countDocuments();
  return numDocs;
};

const assertEntitiesEmpty = (db) => async () => {
  const numEntities = await countEntities(db)();
  if (numEntities !== 0) {
    throw new Error(`Entities is not empty. Count = ${numEntities}`);
  }
};

//
// subscriptions
//

const wipeSubscriptions = (db) => async () => {
  if (!db) {
    throw new Error('not connected. cannot wipe subscriptions');
  }
  const collection = db.collection(ORION_SUBSCRIPTIONS_DB_COLLECTION);
  return collection.deleteMany()
    .then((res) => res.deletedCount);
};

const getSubscriptions = (db) => async (filter) => {
  if (!db) {
    throw new Error('not connected. cannot find subscriptions');
  }
  const collection = db.collection(ORION_SUBSCRIPTIONS_DB_COLLECTION);
  const docs = await collection.find(filter).toArray();
  return docs;
};

const countSubscriptions = (db) => async () => {
  if (!db) {
    throw new Error('not connected. cannot find subscriptions');
  }
  const collection = db.collection(ORION_SUBSCRIPTIONS_DB_COLLECTION);
  const numDocs = await collection.countDocuments();
  return numDocs;
};

const assertSubscriptionsEmpty = (db) => async () => {
  const numSubscriptions = await countSubscriptions(db)();
  if (numSubscriptions !== 0) {
    throw new Error(`Subscriptions is not empty. Count = ${numSubscriptions}`);
  }
};

module.exports = {
  // entities
  getEntities,
  wipeEntities,
  countEntities,
  assertEntitiesEmpty,
  // subscriptions
  getSubscriptions,
  wipeSubscriptions,
  countSubscriptions,
  assertSubscriptionsEmpty,
};
