/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {ObjectId} = require('mongodb');

const {
  constants: {
    DB_SUBSCRIPTION_ARG_KEYS,
  },
} = require('@ratatosk/ngsi-utils');

const {
  transformSubscriptionToMongo,
  transformSubscriptionFromMongo,
} = require('../utils/subscription-mongo-transformer');

const {
  validateSubscriptionDbarg,
} = require('../utils/subscription-dbarg-utils');

const {
  extractConfirmedUniqueDoc,
  fetchDocs,
} = require('../utils/mongo-driver-utils');

//
// endpoints
//

const createSubscription = (collections) => async (dbSubscriptionArg) => {
  validateSubscriptionDbarg(dbSubscriptionArg);
  // just do it raw in place
  const servicePath = dbSubscriptionArg[DB_SUBSCRIPTION_ARG_KEYS.SERVICE_PATHS][0];
  const payload = dbSubscriptionArg[DB_SUBSCRIPTION_ARG_KEYS.PAYLOAD];
  const mongoSubscription = transformSubscriptionToMongo(payload, servicePath);
  return collections.subscriptions.insertOne(mongoSubscription);
};

const getSubscriptions = (collections) => async (dbSubscriptionArg) => {
  const filter = {}; // should use service path
  const options = {}; // should have paging?
  const query = collections.subscriptions.find(filter, options);
  const docs = await fetchDocs(query);
  const subscriptions = _.map(docs, (doc) => transformSubscriptionFromMongo(doc));
  return subscriptions;
};

const findSubscription = (collections) => async (dbSubscriptionArg) => {
  const subscriptionId = dbSubscriptionArg[DB_SUBSCRIPTION_ARG_KEYS.SUBSCRIPTION_ID];
  const filter = {_id: new ObjectId(subscriptionId)};
  const options = {};
  const query = collections.subscriptions.find(filter, options);
  const docs = await fetchDocs(query);
  const doc = extractConfirmedUniqueDoc(docs);
  const subscription = transformSubscriptionFromMongo(doc);
  return subscription;
};

const deleteSubscription = (collections) => async (dbSubscriptionArg) => {
  const subscriptionId = dbSubscriptionArg[DB_SUBSCRIPTION_ARG_KEYS.SUBSCRIPTION_ID];
  const filter = {_id: new ObjectId(subscriptionId)};
  const options = {};
  const query = collections.subscriptions.find(filter, options);
  const docs = await fetchDocs(query);
  const doc = extractConfirmedUniqueDoc(docs);
  return collections.subscriptions.deleteOne(doc);
};

const updateSubscription = (collections) => async (dbSubscriptionArg) => {
  throw new Error('mongo storage updateSubscription not yet implemented');
};

module.exports = {
  // subscriptions
  createSubscription,
  getSubscriptions,
  findSubscription,
  deleteSubscription,
  updateSubscription,
};
