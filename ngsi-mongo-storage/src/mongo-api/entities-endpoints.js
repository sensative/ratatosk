/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const {flatten} = require('flat');

const {
  ngsiEntityFormatter: {formatAttrs},
  ngsiStorageErrorUtils: {errors: ngsiStorageErrors},
  constants: {
    DB_ENTITY_ARG_KEYS,
    DB_ARG_PARAMS_KEYS,
  },
} = require('@ratatosk/ngsi-utils');

const {
  ENTITY_REPRESENTATION_OPTIONS,
} = require('@ratatosk/ngsi-constants');

const {
  ATTRS_UPDATE_TYPES,
} = require('../constants');

const {
  untangleGlobalRegistrations,
} = require('../utils/global-registration-utils');

const {
  extendFiwareEntity,
  assertFiwareAttrsExist,
  updateFiwareAttrs,
  applyEntityDisplayOptions,
  applyAttrDisplayOptions,
} = require('../utils/extended-fiware-entity');

const {
  transformEntityToMongo,
  transformEntityFromMongo,
  generateEntityUpdateOperators,
} = require('../utils/mongo-entity-utils');


const {
  validator: {
    extractValidatedEntityDbarg,
  },
  assertions: {
    requiresDbArgEntity,
    requiresDbArgEntityId,
    requiresDbArgEntityAttrs,
    requiresDbArgAttrName,
    requiresDbArgEntityAttr,
    requiresDbArgEntityAttrValue,
  },
  pipelineStages: {
    extractMatchStage,
    extractAccessStage,
    extractCustomQueryGenerator,
    extractSortStages,
    extractPagingStages,
    extractLimitStages,
  },
} = require('../utils/entity-dbarg-utils');

const {
  extractConfirmedUniqueDoc,
} = require('../utils/mongo-driver-utils');

//
// some validation checks for individual/multiple endpoints
//

const assertCreateEntityOperationPossible = (docs, isUpsert) => {
  if (docs.length > (isUpsert ? 1 : 0)) {
    throw ngsiStorageErrors.CANNOT_CREATE_ALREADY_EXISTS();
  }
};

//
// ENDPOINTS
//

const explain = (collections) => async (dbArgRaw) => {
  const dbArg = extractValidatedEntityDbarg(
    dbArgRaw,
    {requiresSingleMatch: false, isUpdate: false},
  );

  const customQueryGenerator = extractCustomQueryGenerator(dbArg);
  const customAccessStage = extractAccessStage(dbArg);
  const matchStage = extractMatchStage(dbArg);
  const sortStages = extractSortStages(dbArg);
  const pagingStages = extractPagingStages(dbArg);
  const limitStages = extractLimitStages(dbArg);

  let query = null;
  if (customQueryGenerator) {
    query = customQueryGenerator({
      match: matchStage,
      sort: sortStages,
      paging: pagingStages,
    });
    if (_.get(query, ['constructor', 'name']) !== 'AggregationCursor') {
      throw ngsiStorageErrors.INVALID_CUSTOM_ARG();
    }
  } else {
    const pipeline = _.concat(
      matchStage,
      customAccessStage, // can be empty
      sortStages,
      pagingStages,
      limitStages,
    );
    query = collections.entities.aggregate(pipeline);
  }

  // return a mongodb query explanation instead of the actual results of the query
  return query.explain();
};

const createEntity = (collections, globalRegistrationAttrs) => async (dbArgRaw) => {
  // fetch
  requiresDbArgEntity(dbArgRaw);
  const dbArg = extractValidatedEntityDbarg(
    dbArgRaw,
    {requiresSingleMatch: true, isUpdate: true},
  );
  const isUpsert = !!dbArg[DB_ENTITY_ARG_KEYS.UPSERT];
  const matchStage = extractMatchStage(dbArg); // need this even for the match filter (2 places..)
  const pipeline = _.concat(matchStage);
  const query = await collections.entities.aggregate(pipeline);
  const docs = await query.toArray();
  assertCreateEntityOperationPossible(docs, isUpsert);

  // separate all registrations
  const attrUpdateType = ATTRS_UPDATE_TYPES.STRICT_APPEND_ATTRS;
  const {fiware: normalizedEntity} = untangleGlobalRegistrations(dbArg[DB_ENTITY_ARG_KEYS.ENTITY], globalRegistrationAttrs, attrUpdateType);
  const servicePath = dbArg[DB_ENTITY_ARG_KEYS.SERVICE_PATHS][0];
  // format the entity
  const unsavedEntity = extendFiwareEntity(
    normalizedEntity,
    {initEntity: true, initAttrs: true, servicePath},
  );

  const unsavedMongoEntity = transformEntityToMongo(unsavedEntity);
  const updateOperators = generateEntityUpdateOperators(unsavedMongoEntity, docs[0]);
  const {value: savedMongoEntity} = await collections.entities.findOneAndUpdate(
    matchStage.$match,
    updateOperators,
    {upsert: true, returnOriginal: false},
  );

  const modifiedEntity = transformEntityFromMongo(savedMongoEntity);
  return {originalEntity: null, modifiedEntity};
};


const getEntities = (collections) => async (dbArgRaw) => {
  const collectionName = collections.entities.s.namespace.collection;
  const dbArg = extractValidatedEntityDbarg(
    dbArgRaw,
    {requiresSingleMatch: false, isUpdate: false},
  );

  // only one customXxxx is non-nil (both come from custom)
  const customQueryGenerator = extractCustomQueryGenerator(dbArg);
  const customAccessStage = extractAccessStage(dbArg);
  // other stages
  const matchStage = extractMatchStage(dbArg);
  const sortStages = extractSortStages(dbArg);
  const pagingStages = extractPagingStages(dbArg, collectionName);
  const limitStages = extractLimitStages(dbArg);

  const needsCount = _.get(dbArg, [DB_ENTITY_ARG_KEYS.PARAMS, DB_ARG_PARAMS_KEYS.TOTAL_COUNT]);
  let docs;
  let count = null;
  if (customQueryGenerator) {
    const query = await customQueryGenerator({
      collections,
      entitiesCollection: collections.entities,
      stages: {
        match: matchStage,
        sort: sortStages,
        paging: pagingStages,
        limit: limitStages,
      },
    });

    if (_.get(query, ['constructor', 'name']) !== 'AggregationCursor') {
      throw ngsiStorageErrors.INVALID_CUSTOM_ARG();
    }

    const data = await query.toArray();
    docs = _.get(data[0], 'nodes', data);
    // this should be coming from the query. this is a hack
    if (needsCount) {
      count = _.get(data[0], 'countTotal', 0);
    }
  } else {
    const pipeline = _.concat(
      matchStage,
      customAccessStage, // can be nil
      sortStages,
      pagingStages,
      limitStages,
    );
    // console.dir({pipeline}, {depth: null});

    const query = await collections.entities.aggregate(pipeline);
    docs = await query.toArray();

    if (needsCount) {
      // this should be incorporated into main pipeline. this is an ugly hack
      const countPipeline = _.concat(
        matchStage,
        customAccessStage,
        {$count: 'count'},
      );
      const countQuery = collections.entities.aggregate(countPipeline);
      const [countDoc] = await countQuery.toArray();
      count = _.get(countDoc, 'count');
    }
  }

  // transform to extended
  const extendedEntities = _.map(docs, (doc) => transformEntityFromMongo(doc));
  // extract the desired representation etc.
  const queryParams = _.get(dbArg, [DB_ENTITY_ARG_KEYS.PARAMS]);
  const representationType = _.get(queryParams, DB_ARG_PARAMS_KEYS.REPRESENTATION_TYPE);
  const attrsList = _.get(queryParams, DB_ARG_PARAMS_KEYS.ATTRS_LIST);
  const metadataList = _.get(queryParams, DB_ARG_PARAMS_KEYS.METADATA_LIST);
  const entities = _.map(extendedEntities, (entity) => applyEntityDisplayOptions(
    entity,
    representationType,
    attrsList,
    metadataList,
  ));

  // and done
  return {count, entities};
};

//
// FIND SINGLE ENTITY
//


const privateFindEntity = async (collections, dbArgRaw, isUpdate = false) => {
  // fetch
  requiresDbArgEntityId(dbArgRaw);
  const dbArg = extractValidatedEntityDbarg(
    dbArgRaw,
    {requiresSingleMatch: true, isUpdate},
  );

  const customQueryGenerator = extractCustomQueryGenerator(dbArg);
  const customAccessStage = extractAccessStage(dbArg);
  const matchStage = extractMatchStage(dbArg);

  let query = null;
  if (customQueryGenerator) {
    query = customQueryGenerator({
      entitiesCollection: collections.entities,
      match: matchStage,
      paging: {},
    });
    if (_.get(query, ['constructor', 'name']) !== 'AggregationCursor') {
      throw ngsiStorageErrors.INVALID_CUSTOM_ARG();
    }
  } else {
    const pipeline = _.concat(
      matchStage,
      customAccessStage, // can be empty
    );
    query = collections.entities.aggregate(pipeline);
  }

  const data = await query.toArray();
  const docs = _.get(data[0], 'nodes', data);
  const doc = extractConfirmedUniqueDoc(docs);

  // transform to extended
  const extendedEntity = transformEntityFromMongo(doc);
  // extract display options
  const representationType = _.get(dbArg, [DB_ENTITY_ARG_KEYS.PARAMS, DB_ARG_PARAMS_KEYS.REPRESENTATION_TYPE]);
  const attrsList = _.get(dbArg, [DB_ENTITY_ARG_KEYS.PARAMS, DB_ARG_PARAMS_KEYS.ATTRS_LIST]);
  const metadataList = _.get(dbArg, [DB_ENTITY_ARG_KEYS.PARAMS, DB_ARG_PARAMS_KEYS.METADATA_LIST]);
  // and done
  return {
    fiwareId: doc._id,
    extendedEntity,
    representationType,
    attrsList,
    metadataList,
  };
};

const privateExtractEntityAttr = (dbArg, extendedEntity) => {
  requiresDbArgAttrName(dbArg);
  const attrName = dbArg[DB_ENTITY_ARG_KEYS.ATTR_NAME];
  assertFiwareAttrsExist(extendedEntity, attrName);
  const attr = extendedEntity.attrs[attrName];
  return attr;
};

//
// SINGLE ENTITY ROUTES
//

const deleteEntity = (collections) => async (dbArgRaw) => {
  // extract the entity
  const isUpdate = true;
  const {
    extendedEntity: originalEntity,
    fiwareId,
  } = await privateFindEntity(collections, dbArgRaw, isUpdate);
  await collections.entities.deleteOne({_id: fiwareId});
  return {originalEntity, modifiedEntity: null, entity: null};
};

const findEntity = (collections) => async (dbArgRaw) => {
  // find entity
  const {
    extendedEntity,
    representationType,
    attrsList,
    metadataList,
  } = await privateFindEntity(collections, dbArgRaw);
  // mutilate
  const entity = applyEntityDisplayOptions(
    extendedEntity,
    representationType,
    attrsList,
    metadataList,
  );
  // and done
  return entity;
};

const getEntityAttrs = (collections) => async (dbArgRaw) => {
  // find entity
  const {
    extendedEntity,
    representationType,
    attrsList,
    metadataList,
  } = await privateFindEntity(collections, dbArgRaw);
  // mutilate
  const entity = applyEntityDisplayOptions(
    extendedEntity,
    representationType,
    attrsList,
    metadataList,
  );
  // and get rid of "id" and "type" if representation-type is relevant
  if (_.has(entity, 'id')) {
    return _.omit(entity, ['id', 'type', 'servicePath']);
  }
  return entity;
};

const findEntityAttr = (collections) => async (dbArgRaw) => {
  // find entity
  const {
    extendedEntity,
    representationType,
    metadataList,
  } = await privateFindEntity(collections, dbArgRaw);
  // extract the attr
  const attr = privateExtractEntityAttr(dbArgRaw, extendedEntity);
  // mutilate
  const displayedAttr = applyAttrDisplayOptions(
    attr,
    representationType,
    metadataList,
  );
  // and done
  return displayedAttr;
};

const findEntityAttrValue = (collections) => async (dbArgRaw) => {
  // find entity
  const {
    extendedEntity,
    metadataList,
  } = await privateFindEntity(collections, dbArgRaw);
  // extract the attr
  const attr = privateExtractEntityAttr(dbArgRaw, extendedEntity);
  // mutilate
  const representationType = ENTITY_REPRESENTATION_OPTIONS.VALUES; // only difference..
  const displayedAttr = applyAttrDisplayOptions(
    attr,
    representationType,
    metadataList,
  );
  // and done
  return displayedAttr;
};

const getQueryUpdateOptions = (fiwareId, attrs, attrUpdateType, dbArgRaw) => {
  const query = {_id: fiwareId};
  let update = {};
  const options = {upsert: true, returnOriginal: false};

  if (attrUpdateType === ATTRS_UPDATE_TYPES.REPLACE_ATTRS) {
    update = {$set: {attrs}};
  }

  if (attrUpdateType === ATTRS_UPDATE_TYPES.UPSERT_ATTRS) {
    update = {$set: flatten({attrs}, {maxDepth: 2})};
  }

  if (attrUpdateType === ATTRS_UPDATE_TYPES.PATCH_ATTRS) {
    const attrKeys = _.keys({attrs});
    _.map(attrKeys, (attrKey) => {
      query[attrKey] = {$exists: true};
    });

    update = {$set: flatten({attrs}, {maxDepth: 2})};
    options.upsert = false;
  }

  if (attrUpdateType === ATTRS_UPDATE_TYPES.DELETE_ATTRS) {
    const [attrKey] = _.keys(dbArgRaw.attrs);
    update = {$unset: {[`attrs.${attrKey}`]: ''}};
    options.upsert = false;
  }

  if (attrUpdateType === ATTRS_UPDATE_TYPES.UPDATE_ATTR_VALUES) {
    const attrKeys = _.keys({attrs});
    _.map(attrKeys, (attrKey) => {
      if (attrKey !== 'attrs') {
        query[attrKey] = {$exists: true};
      }
    });

    update = {$set: flatten({attrs}, {maxDepth: 3})};
  }

  if (attrUpdateType === ATTRS_UPDATE_TYPES.STRICT_APPEND_ATTRS) {
    const [attrKey] = _.keys(dbArgRaw.attrs);
    update = {$set: {[`attrs.${attrKey}`]: attrs[attrKey]}};
    options.upsert = false;
  }

  return {query, update, options};
};

const getUpdateAttrValues = (attrs, valueObject) => {
  const rootAttrObj = {};
  const rootAttrs = _.keys(attrs);

  _.map(rootAttrs, (attr) => {
    const attrsKey = `attrs.${attr}`;
    const attrValue = _.get(valueObject, attrsKey);
    if (!_.isUndefined(attrValue)) {
      _.set(rootAttrObj, attrsKey, attrValue);
    }
  });

  return _.get(rootAttrObj, 'attrs', {});
};

//
// UPDATE SINGLE ENTITY
//
const privateUpdateAttrs = async (collections, globalRegistrationAttrs, dbArgRaw, attrUpdateType) => {
  const isUpdate = true;
  const {
    extendedEntity: originalEntity,
    fiwareId,
  } = await privateFindEntity(collections, dbArgRaw, isUpdate);
  // extract the attrs changes
  requiresDbArgEntityAttrs(dbArgRaw);
  const {fiware: attrs, registrations} = untangleGlobalRegistrations(dbArgRaw[DB_ENTITY_ARG_KEYS.ATTRS], globalRegistrationAttrs, attrUpdateType);
  // only update if necessary
  let modifiedEntity = originalEntity;
  if (_.size(attrs)) {
    modifiedEntity = updateFiwareAttrs(originalEntity, attrs, attrUpdateType);
    const modifiedMongoDoc = transformEntityToMongo(modifiedEntity);
    const updateAttrs = getUpdateAttrValues(attrs, modifiedMongoDoc);
    const {query, update, options} = getQueryUpdateOptions(fiwareId, updateAttrs, attrUpdateType, dbArgRaw);

    if (_.get(modifiedMongoDoc, 'location')) {
      update.$set = {...update.$set, location: modifiedMongoDoc.location};
    }
    await collections.entities.findOneAndUpdate(query, _.pickBy(update), options);
  }
  return {originalEntity, modifiedEntity, registrations};
};

const replaceEntityAttrs = (collections, globalRegistrationAttrs) => async (dbArg) => {
  const attrUpdateType = ATTRS_UPDATE_TYPES.REPLACE_ATTRS;
  return privateUpdateAttrs(collections, globalRegistrationAttrs, dbArg, attrUpdateType);
};

const upsertEntityAttrs = (collections, globalRegistrationAttrs) => async (dbArg) => {
  const isStrictAppend = _.get(dbArg, [DB_ENTITY_ARG_KEYS.PARAMS, DB_ARG_PARAMS_KEYS.STRICT_APPEND], false);
  const attrUpdateType = isStrictAppend ? ATTRS_UPDATE_TYPES.STRICT_APPEND_ATTRS : ATTRS_UPDATE_TYPES.UPSERT_ATTRS;
  return privateUpdateAttrs(collections, globalRegistrationAttrs, dbArg, attrUpdateType);
};

const patchEntityAttrs = (collections, globalRegistrationAttrs) => async (dbArg) => {
  const attrUpdateType = ATTRS_UPDATE_TYPES.PATCH_ATTRS;
  return privateUpdateAttrs(collections, globalRegistrationAttrs, dbArg, attrUpdateType);
};

const updateEntityAttr = (collections, globalRegistrationAttrs) => async (dbArgRaw) => {
  requiresDbArgEntityId(dbArgRaw);
  requiresDbArgAttrName(dbArgRaw);
  requiresDbArgEntityAttr(dbArgRaw);
  const attrName = dbArgRaw[DB_ENTITY_ARG_KEYS.ATTR_NAME];
  const attr = dbArgRaw[DB_ENTITY_ARG_KEYS.ATTR];
  const dbArg = {
    ..._.omit(dbArgRaw, [DB_ENTITY_ARG_KEYS.ATTR_NAME, DB_ENTITY_ARG_KEYS.ATTR]),
    [DB_ENTITY_ARG_KEYS.ATTRS]: {[attrName]: attr},
  };
  const attrUpdateType = ATTRS_UPDATE_TYPES.PATCH_ATTRS;
  return privateUpdateAttrs(collections, globalRegistrationAttrs, dbArg, attrUpdateType);
};

const deleteEntityAttr = (collections, globalRegistrationAttrs) => async (dbArgRaw) => {
  requiresDbArgEntityId(dbArgRaw);
  requiresDbArgAttrName(dbArgRaw);
  const attrName = dbArgRaw[DB_ENTITY_ARG_KEYS.ATTR_NAME];
  const attr = {value: null, type: 'placeholder'};
  const dbArg = {
    ...dbArgRaw,
    [DB_ENTITY_ARG_KEYS.ATTRS]: {[attrName]: attr},
  };
  const attrUpdateType = ATTRS_UPDATE_TYPES.DELETE_ATTRS;
  return privateUpdateAttrs(collections, globalRegistrationAttrs, dbArg, attrUpdateType);
};

const updateEntityAttrValue = (collections, globalRegistrationAttrs) => async (dbArgRaw) => {
  requiresDbArgEntityId(dbArgRaw);
  requiresDbArgAttrName(dbArgRaw);
  requiresDbArgEntityAttrValue(dbArgRaw);
  const attrName = dbArgRaw[DB_ENTITY_ARG_KEYS.ATTR_NAME];
  const attrValue = dbArgRaw[DB_ENTITY_ARG_KEYS.ATTR_VALUE];
  const attrs = formatAttrs({[attrName]: attrValue});
  const dbArg = {
    ..._.omit(dbArgRaw, [DB_ENTITY_ARG_KEYS.ATTR_NAME, DB_ENTITY_ARG_KEYS.ATTR_VALUE]),
    [DB_ENTITY_ARG_KEYS.ATTRS]: attrs,
  };
  const attrUpdateType = ATTRS_UPDATE_TYPES.UPDATE_ATTR_VALUES;
  return privateUpdateAttrs(collections, globalRegistrationAttrs, dbArg, attrUpdateType);
};

module.exports = {
  // top level
  explain,
  createEntity,
  deleteEntity,
  getEntities,
  // find single entity
  findEntity,
  getEntityAttrs,
  findEntityAttr,
  findEntityAttrValue,
  // update single entity
  replaceEntityAttrs,
  upsertEntityAttrs,
  patchEntityAttrs,
  updateEntityAttr,
  deleteEntityAttr,
  updateEntityAttrValue,
};
