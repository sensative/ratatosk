/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const {ObjectId} = require('mongodb');

const {
  applyEntityDisplayOptions,
} = require('../utils/extended-fiware-entity');

const {
  transformSubscriptionFromMongo,
} = require('../utils/subscription-mongo-transformer');

const {
  extractConfirmedUniqueDoc,
  fetchDocs,
} = require('../utils/mongo-driver-utils');

//
// endpoints
//

const generateNotifications = (collections) => async (entityPair) => {
  if (!_.isObject(entityPair)) {
    throw new Error('entityPair must be an object');
  }
  if (!entityPair.originalEntity && !entityPair.modifiedEntity) {
    return []; // no changes, no notifications..
  }
  const idInfo = {
    ..._.pick(entityPair.originalEntity, ['id', 'type', 'servicePath']),
    ..._.pick(entityPair.modifiedEntity, ['id', 'type', 'servicePath']),
  };
  const filter = {
    servicePath: '/',
    $or: [
      {
        // id only (no patterns)
        $and: [
          {'entities.isPattern': {$eq: 'false'}},
          {'entities.id': {$eq: idInfo.id}},
          {'entities.type': {$exists: false}},
        ],
      },
      {
        // id and type (no patterns)
        $and: [
          {'entities.isPattern': {$eq: 'false'}},
          {'entities.id': {$eq: idInfo.id}},
          {'entities.isTypePattern': {$eq: false}},
          {'entities.type': {$eq: idInfo.type}},
        ],
      },
    ],
  };
  const query = collections.subscriptions.find(filter);
  const subscriptionDocs = await fetchDocs(query);

  const entity = entityPair.modifiedEntity ? applyEntityDisplayOptions(entityPair.modifiedEntity) : null;
  const notifications = _.map(subscriptionDocs, (subscription) => {
    const notification = {
      subscriptionId: subscription._id.toString(),
      data: [entity],
    };
    return {
      subscription: transformSubscriptionFromMongo(subscription),
      notification,
    };
  });
  return notifications;
};

const acknowledgeNotification = (collections) => async ({subscription, succeeded}) => {
  const filter = {_id: new ObjectId(subscription.id)};
  const query = collections.subscriptions.find(filter);
  const docs = await fetchDocs(query);
  const doc = extractConfirmedUniqueDoc(docs);
  const timesSent = _.get(doc, 'notification.timesSent', 0);
  if (succeeded) {
    _.set(doc, 'notification.timesSent', timesSent + 1);
    _.set(doc, 'notification.lastNotification', new Date());
    _.set(doc, 'notification.lastSuccess', new Date());
    doc.status = 'active';
  } else {
    _.set(doc, 'notification.timesSent', timesSent + 1);
    _.set(doc, 'notification.lastNotification', new Date());
    _.set(doc, 'notification.lastFailure', new Date());
    doc.status = 'failed';
  }
  await collections.subscriptions.findOneAndReplace(filter, doc);
};

module.exports = {
  // notifications
  generateNotifications,
  acknowledgeNotification,
};
