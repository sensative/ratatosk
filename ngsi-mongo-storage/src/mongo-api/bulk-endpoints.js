/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const serialPromise = require('promise-serial-exec');

const {
  ngsiEntityFormatter: {formatEntity, formatAttrs},
  ngsiStorageErrorUtils: {errors: ngsiStorageErrors},
  constants: {
    DB_ENTITY_ARG_KEYS,
    DB_ARG_PARAMS_KEYS,
    DB_BULK_UPDATE_ARG_KEYS,
  },
} = require('@ratatosk/ngsi-utils');

const {
  BULK_UPDATE_PAYLOAD_KEYS,
  BULK_UPDATE_ACTION_TYPES,
} = require('@ratatosk/ngsi-constants');

const {
  validateBulkUpdateDbarg,
  validateBulkQueryDbarg,
  partitionBulkQueryDbarg,
} = require('../utils/bulk-query-dbarg-utils');

//
// the entity endpoints subcontractors
//

const {
  getEntities,
  createEntity,
  upsertEntityAttrs,
  patchEntityAttrs,
  replaceEntityAttrs,
  deleteEntity,
  deleteEntityAttr,
} = require('./entities-endpoints');

//
// endpoints
//

const bulkQuery = (collections) => async (dbBulkQueryArg) => {
  validateBulkQueryDbarg(dbBulkQueryArg);
  const dbArgs = partitionBulkQueryDbarg(dbBulkQueryArg);
  const get = getEntities(collections);
  const resList = await Promise.all(_.map(dbArgs, (dbArg) => get(dbArg)));
  const entities = _.flatten(_.map(resList, (res) => res.entities));
  return entities;
};

const bulkUpdate = (collections, globalRegistrationAttrs) => async (dbBulkUpdateArg) => {
  validateBulkUpdateDbarg(dbBulkUpdateArg);
  // This is gonna be dirty. A dirty monster.
  // For now, to develop and pass some tests, just do the roughest
  // remapping of the bulk update as possible
  // This WILL be redone with a single atomic pipeline
  const servicePaths = dbBulkUpdateArg[DB_BULK_UPDATE_ARG_KEYS.SERVICE_PATHS];
  const actionType = dbBulkUpdateArg[DB_BULK_UPDATE_ARG_KEYS.PAYLOAD][BULK_UPDATE_PAYLOAD_KEYS.ACTION_TYPE];
  const entities = dbBulkUpdateArg[DB_BULK_UPDATE_ARG_KEYS.PAYLOAD][BULK_UPDATE_PAYLOAD_KEYS.ENTITIES];
  // const dbArgs = partitionDbBulkUpdateArg(dbBulkUpdateArg);
  switch (actionType) {
    case BULK_UPDATE_ACTION_TYPES.APPEND: {
      return serialPromise(_.map(entities, (entity) => async () => {
        const dbArg = {
          [DB_ENTITY_ARG_KEYS.SERVICE_PATHS]: servicePaths,
          [DB_ENTITY_ARG_KEYS.ENTITY]: formatEntity(entity),
        };
        return createEntity(collections)(dbArg)
          .catch(async (err) => {
            if (ngsiStorageErrors.CANNOT_CREATE_ALREADY_EXISTS.matches(err)) {
              const subDbArg = {
                [DB_ENTITY_ARG_KEYS.SERVICE_PATHS]: servicePaths,
                [DB_ENTITY_ARG_KEYS.ENTITY_ID]: entity.id,
                [DB_ENTITY_ARG_KEYS.ATTRS]: formatAttrs(_.omit(entity, ['id', 'type'])),
              };
              return upsertEntityAttrs(collections)(subDbArg);
            } else {
              throw err;
            }
          });
      }));
    }
    case BULK_UPDATE_ACTION_TYPES.APPEND_STRICT: {
      return serialPromise(_.map(entities, (entity) => async () => {
        const dbArg = {
          [DB_ENTITY_ARG_KEYS.SERVICE_PATHS]: servicePaths,
          [DB_ENTITY_ARG_KEYS.ENTITY]: formatEntity(entity),
        };
        return createEntity(collections)(dbArg)
          .catch(async (err) => {
            if (ngsiStorageErrors.CANNOT_CREATE_ALREADY_EXISTS.matches(err)) {
              const subDbArg = {
                [DB_ENTITY_ARG_KEYS.SERVICE_PATHS]: servicePaths,
                [DB_ENTITY_ARG_KEYS.ENTITY_ID]: entity.id,
                [DB_ENTITY_ARG_KEYS.ATTRS]: formatAttrs(_.omit(entity, ['id', 'type'])),
                [DB_ENTITY_ARG_KEYS.PARAMS]: {
                  [DB_ARG_PARAMS_KEYS.STRICT_APPEND]: true,
                },
              };
              return upsertEntityAttrs(collections)(subDbArg);
            } else {
              throw err;
            }
          });
      }));
    }
    case BULK_UPDATE_ACTION_TYPES.UPDATE: {
      return serialPromise(_.map(entities, (entity) => async () => {
        const dbArg = {
          [DB_ENTITY_ARG_KEYS.SERVICE_PATHS]: servicePaths,
          [DB_ENTITY_ARG_KEYS.ENTITY_ID]: entity.id,
          [DB_ENTITY_ARG_KEYS.ATTRS]: formatAttrs(_.omit(entity, ['id', 'type'])),
        };
        return patchEntityAttrs(collections)(dbArg);
      }));
    }
    case BULK_UPDATE_ACTION_TYPES.REPLACE: {
      return serialPromise(_.map(entities, (entity) => async () => {
        const dbArg = {
          [DB_ENTITY_ARG_KEYS.SERVICE_PATHS]: servicePaths,
          [DB_ENTITY_ARG_KEYS.ENTITY_ID]: entity.id,
          [DB_ENTITY_ARG_KEYS.ATTRS]: formatAttrs(_.omit(entity, ['id', 'type'])),
        };
        return replaceEntityAttrs(collections)(dbArg);
      }));
    }
    case BULK_UPDATE_ACTION_TYPES.DELETE: {
      return serialPromise(_.map(entities, (entity) => async () => {
        const attrs = formatAttrs(_.omit(entity, ['id', 'type']));
        if (_.size(attrs) === 0) {
          const dbArg = {
            [DB_ENTITY_ARG_KEYS.SERVICE_PATHS]: servicePaths,
            [DB_ENTITY_ARG_KEYS.ENTITY_ID]: entity.id,
          };
          return deleteEntity(collections)(dbArg);
        } else {
          const entityPairs = await serialPromise(_.map(attrs, (attr, attrName) => async () => {
            const subDbArg = {
              [DB_ENTITY_ARG_KEYS.SERVICE_PATHS]: servicePaths,
              [DB_ENTITY_ARG_KEYS.ENTITY_ID]: entity.id,
              [DB_ENTITY_ARG_KEYS.ATTR_NAME]: attrName,
            };
            return deleteEntityAttr(collections)(subDbArg);
          }));
          const firstPair = entityPairs.shift();
          const lastPair = entityPairs.pop() || firstPair;
          return {
            originalEntity: firstPair.originalEntity,
            modifiedEntity: lastPair.modifiedEntity,
          };
        }
      }));
    }
    default: {
      throw new Error(`DevErr: Invalid actionType ${actionType} should have been accounted for`);
    }
  }
};

module.exports = {
  // bulk
  bulkQuery,
  bulkUpdate,
};
