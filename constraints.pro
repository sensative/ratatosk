% Enable this when we have cleaned up packages
% Enforces that no workspaces have different versions of the same package
% gen_enforced_dependency(WorkspaceCwd, DependencyIdent, DependencyRange2, DependencyType) :-
%   workspace_has_dependency(WorkspaceCwd, DependencyIdent, DependencyRange, DependencyType),
%   workspace_has_dependency(OtherWorkspaceCwd, DependencyIdent, DependencyRange2, DependencyType2),
%   DependencyRange \= DependencyRange2.

% Enforces that all workspaces depend on other workspaces using `workspace:*`
gen_enforced_dependency(WorkspaceCwd, DependencyIdent, 'workspace:*', DependencyType) :-
  workspace_has_dependency(WorkspaceCwd, DependencyIdent, DependencyRange, DependencyType),
  % Only consider dependency ranges that start with 'workspace:'
  atom_concat('workspace:', _, DependencyRange).

% Enforce that the workspace has a name that corresponds with its dirname
gen_enforced_field(WorkspaceCwd, 'name', WorkspaceName) :-
  atomic_list_concat(List, '/', WorkspaceCwd),
  last(List, DirName),
  atom_concat('@ratatosk/', DirName, WorkspaceName),
  WorkspaceCwd \= '.'. % Do force the root workspace to have name '.'

% Enforces that a dependency doesn't appear in both `dependencies` and `devDependencies`
gen_enforced_dependency(WorkspaceCwd, DependencyIdent, null, 'devDependencies') :-
  workspace_has_dependency(WorkspaceCwd, DependencyIdent, _, 'devDependencies'),
  workspace_has_dependency(WorkspaceCwd, DependencyIdent, _, 'dependencies').

% Forbid is-number
gen_enforced_dependency(WorkspaceCwd, 'is-number', null, DependencyType) :-
  workspace_has_dependency(WorkspaceCwd, 'is-number', _, DependencyType).

%%% Enforced package versions %%%
gen_enforced_dependency(WorkspaceCwd, 'lodash', '^4.17.21', DependencyType) :-
  workspace_has_dependency(WorkspaceCwd, 'lodash', DependencyRange, DependencyType).

gen_enforced_dependency(WorkspaceCwd, 'mongoose', '5.13.13', DependencyType) :-
  workspace_has_dependency(WorkspaceCwd, 'mongoose', DependencyRange, DependencyType).

gen_enforced_dependency(WorkspaceCwd, 'next', '12.0.4', DependencyType) :-
  workspace_has_dependency(WorkspaceCwd, 'next', DependencyRange, DependencyType).
