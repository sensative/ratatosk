## ngsi-linter

helps enforce linting in all other ngsi modules and is only used for testing

---------
#### the roadmap to the entities api

inside the test file

```js

const {lint} = require('@ratatosk/ngsi-linter');


const folderToLintAbsolutePath = path.join(__dirname, '..');
it('linting', async () => {
  await lint(folderToLintAbsolutePath);
});

```
