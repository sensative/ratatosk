/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

const path = require('path');
const _ = require('lodash');
const eslint = require('eslint');
const Linter = require('standard-engine').linter;

const configFile = path.join(__dirname, '../eslintrc.ratatosk.json');

const linter = new Linter({
  cmd: 'ratatosk-standard',
  eslint,
  eslintConfig: {configFile},
});

const formatFileRes = (name, res) => {
  if (!_.get(res, 'messages.length')) {
    return ''; // no errors
  }
  const pre = `\n\t ${name}: `;
  const intro = `${pre} (${res.errorCount} errors, ${res.warningCount} warnings)`;
  return _.reduce(res.messages, (acc, m) => `${acc} ${pre} ${m.line}:${m.column} - ${m.message} (${m.ruleId})`, intro);
};

const lintFiles = async (rootPath) => {
  // console.log({rootPath});
  const files = `${rootPath}/**/**.js`;
  await new Promise((resolve, reject) => {
    linter.lintFiles(files, (err, res) => {
      if (err) {
        return reject(err);
      }
      const message = _.reduce(res.results, (acc, fileResult) => {
        const name = path.relative(rootPath, fileResult.filePath);
        const fileMessage = formatFileRes(name, fileResult);
        if (fileMessage) {
          return `${acc}\n ${fileMessage}`;
        }
        return acc;
      }, '');
      if (message) {
        reject(new Error(message));
      } else {
        resolve();
      }
    });
  });
};

module.exports = {
  lintFiles,
};
