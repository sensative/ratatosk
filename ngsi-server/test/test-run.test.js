/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const assert = require('assert');
const delay = require('delay');
const sinon = require('sinon');

const {
  startNgsiV2Server,
  stopNgsiV2Server,
} = require('../src');

const {
  getMongoConfig,
} = require('./test-config');

describe('test-run', () => {
  after(() => {
    if (process.on.restore) {
      process.on.restore();
    }
  });

  it('should be able to start and stop the server using', async () => {
    const mongoConfig = getMongoConfig();
    const customConfig = {mongo: mongoConfig};
    await startNgsiV2Server(customConfig);
    await stopNgsiV2Server();
    await startNgsiV2Server(customConfig);
    await stopNgsiV2Server();
    await startNgsiV2Server(customConfig);
    await stopNgsiV2Server();
    await startNgsiV2Server(customConfig);
    await stopNgsiV2Server();
  });

  it('should be able to start the server with a customConfig', async () => {
    const mongoConfig = getMongoConfig();
    const customConfig = {mongo: mongoConfig};
    await startNgsiV2Server(customConfig);
    await delay(500);
    await stopNgsiV2Server();
  });

  it('should be able to disable process event listeners through config', async () => {
    const mongoConfig = getMongoConfig();
    const customConfig = {
      disableProcessEventListeners: true,
      mongo: mongoConfig,
    };
    sinon.stub(process, 'on');
    await startNgsiV2Server(customConfig);
    await delay(500);
    await stopNgsiV2Server();
    assert(!process.on.called, 'Process event listeners initialized');
  });

});
