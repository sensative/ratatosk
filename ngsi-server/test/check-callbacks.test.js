/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const sinon = require('sinon');

const {
  startNgsiV2Server,
  stopNgsiV2Server,
} = require('../src');

const {createApi} = require('@ratatosk/ngsi-client-api');

const {
  getMongoConfig,
} = require('./test-config');


const globalUpdateCallback = sinon.spy();
const globalRegistrationCallback = sinon.spy();
const registrationAttr = 'diggity';
const serverPort = 8999;

const ngsiApi = createApi({
  host: 'localhost',
  port: serverPort,
});

const wipeAllDates = (entity) => {
  if (!entity) {
    return null;
  }
  const wipedEntity = {
    ...entity,
    builtinAttrs: _.omit(entity.builtinAttrs, ['dateCreated', 'dateModified']),
    attrs: _.mapValues(entity.attrs, (attr) => ({
      ...attr,
      builtinMetadata: _.omit(entity.builtinMetadata, ['dateCreated', 'dateModified']),
    })),
  };
  return wipedEntity;
};

describe('test-run', () => {

  before(async () => {
    const mongoConfig = getMongoConfig();
    const publisherConfig = {
      globalUpdateCallback,
      globalRegistrationCallback,
      globalRegistrationAttrs: [registrationAttr],
    };
    const serverConfig = {port: serverPort};
    const customConfig = {
      mongo: mongoConfig,
      publisher: publisherConfig,
      server: serverConfig,
    };
    await startNgsiV2Server(customConfig);
  });

  after(async () => {
    await stopNgsiV2Server();
  });

  let fiwareId;
  it('create a new device', async () => {
    const entity = {id: 'derp'};
    await ngsiApi.createEntity({entity});
    const {data: savedEntity} = await ngsiApi.findEntity({entityId: 'derp'});
    const expectedEntity = {
      id: 'derp',
      type: 'Thing',
    };
    assert.deepStrictEqual(savedEntity, expectedEntity);
    const numUpdates = globalUpdateCallback.callCount;
    assert.strictEqual(numUpdates, 1);
    const firstArg = globalUpdateCallback.firstCall.firstArg;
    fiwareId = _.get(firstArg, 'modifiedEntity.builtinAttrs.fiwareId.value');
    const expectedFirstArg = {
      // originalEntity: null,
      modifiedEntity: {
        id: 'derp',
        type: 'Thing',
        servicePath: '/',
        attrs: {},
        builtinAttrs: {
          fiwareId: {type: 'Text', value: fiwareId},
        },
      },
    };
    assert.strictEqual(firstArg.originalEntity, null);
    assert.deepStrictEqual(wipeAllDates(firstArg.modifiedEntity), expectedFirstArg.modifiedEntity);
    // check & reset callbacks
    globalUpdateCallback.resetHistory();
    const numRegs = globalRegistrationCallback.callCount;
    assert.strictEqual(numRegs, 0);
    globalRegistrationCallback.resetHistory();
  });

  it('update a device (no reg)', async () => {
    const attrs = {smerp: 2};
    await ngsiApi.upsertEntityAttrs({entityId: 'derp', attrs});
    const {data: savedEntity} = await ngsiApi.findEntity({entityId: 'derp'});
    const expectedEntity = {
      id: 'derp',
      type: 'Thing',
      smerp: {metadata: {}, type: 'Number', value: 2},
    };
    assert.deepStrictEqual(savedEntity, expectedEntity);
    const numUpdates = globalUpdateCallback.callCount;
    assert.strictEqual(numUpdates, 1);
    const firstArg = globalUpdateCallback.firstCall.firstArg;
    const expectedFirstArg = {
      originalEntity: {
        id: 'derp',
        type: 'Thing',
        servicePath: '/',
        attrs: {},
        builtinAttrs: {
          fiwareId: {type: 'Text', value: fiwareId},
        },
      },
      modifiedEntity: {
        id: 'derp',
        type: 'Thing',
        servicePath: '/',
        attrs: {
          smerp: {
            metadata: {},
            type: 'Number',
            value: 2,
            builtinMetadata: {},
          },
        },
        builtinAttrs: {
          fiwareId: {type: 'Text', value: fiwareId},
        },
      },
    };
    assert.deepStrictEqual(wipeAllDates(firstArg.originalEntity), expectedFirstArg.originalEntity);
    assert.deepStrictEqual(wipeAllDates(firstArg.modifiedEntity), expectedFirstArg.modifiedEntity);
    // check & reset callbacks
    globalUpdateCallback.resetHistory();
    const numRegs = globalRegistrationCallback.callCount;
    assert.strictEqual(numRegs, 0);
    globalRegistrationCallback.resetHistory();
  });

  it('upsert same attr (smerp)', async () => {
    const attrs = {smerp: 'bloop'};
    await ngsiApi.upsertEntityAttrs({entityId: 'derp', attrs});
    const {data: savedEntity} = await ngsiApi.findEntity({entityId: 'derp'});
    const expectedEntity = {
      id: 'derp',
      type: 'Thing',
      smerp: {metadata: {}, type: 'Text', value: 'bloop'},
    };
    assert.deepStrictEqual(savedEntity, expectedEntity);
    const numUpdates = globalUpdateCallback.callCount;
    assert.strictEqual(numUpdates, 1);
    const firstArg = globalUpdateCallback.firstCall.firstArg;
    const expectedFirstArg = {
      originalEntity: {
        id: 'derp',
        type: 'Thing',
        servicePath: '/',
        attrs: {
          smerp: {
            metadata: {},
            type: 'Number',
            value: 2,
            builtinMetadata: {},
          },
        },
        builtinAttrs: {
          fiwareId: {type: 'Text', value: fiwareId},
        },
      },
      modifiedEntity: {
        id: 'derp',
        type: 'Thing',
        servicePath: '/',
        attrs: {
          smerp: {
            metadata: {},
            type: 'Text',
            value: 'bloop',
            builtinMetadata: {},
          },
        },
        builtinAttrs: {
          fiwareId: {type: 'Text', value: fiwareId},
        },
      },
    };
    assert.deepStrictEqual(wipeAllDates(firstArg.originalEntity), expectedFirstArg.originalEntity);
    assert.deepStrictEqual(wipeAllDates(firstArg.modifiedEntity), expectedFirstArg.modifiedEntity);
    // check & reset callbacks
    globalUpdateCallback.resetHistory();
    const numRegs = globalRegistrationCallback.callCount;
    assert.strictEqual(numRegs, 0);
    globalRegistrationCallback.resetHistory();
  });

  it('upsert different attr (knerp - and only knerp)', async () => {
    const attrs = {knerp: false};
    await ngsiApi.upsertEntityAttrs({entityId: 'derp', attrs});
    const {data: savedEntity} = await ngsiApi.findEntity({entityId: 'derp'});
    const expectedEntity = {
      id: 'derp',
      type: 'Thing',
      smerp: {metadata: {}, type: 'Text', value: 'bloop'},
      knerp: {metadata: {}, type: 'Boolean', value: false},
    };
    assert.deepStrictEqual(savedEntity, expectedEntity);
    const numUpdates = globalUpdateCallback.callCount;
    assert.strictEqual(numUpdates, 1);
    const firstArg = globalUpdateCallback.firstCall.firstArg;
    const expectedFirstArg = {
      originalEntity: {
        id: 'derp',
        type: 'Thing',
        servicePath: '/',
        attrs: {
          smerp: {
            metadata: {},
            type: 'Text',
            value: 'bloop',
            builtinMetadata: {},
          },
        },
        builtinAttrs: {
          fiwareId: {type: 'Text', value: fiwareId},
        },
      },
      modifiedEntity: {
        id: 'derp',
        type: 'Thing',
        servicePath: '/',
        attrs: {
          smerp: {
            metadata: {},
            type: 'Text',
            value: 'bloop',
            builtinMetadata: {},
          },
          knerp: {
            metadata: {},
            type: 'Boolean',
            value: false,
            builtinMetadata: {},
          },
        },
        builtinAttrs: {
          fiwareId: {type: 'Text', value: fiwareId},
        },
      },
    };
    assert.deepStrictEqual(wipeAllDates(firstArg.originalEntity), expectedFirstArg.originalEntity);
    assert.deepStrictEqual(wipeAllDates(firstArg.modifiedEntity), expectedFirstArg.modifiedEntity);
    // check & reset callbacks
    globalUpdateCallback.resetHistory();
    const numRegs = globalRegistrationCallback.callCount;
    assert.strictEqual(numRegs, 0);
    globalRegistrationCallback.resetHistory();
  });

  it('update the registration attr ONLY', async () => {
    // initial find
    const {data: initialEntity} = await ngsiApi.findEntity({entityId: 'derp'});
    const expectedInitialEntity = {
      id: 'derp',
      type: 'Thing',
      smerp: {metadata: {}, type: 'Text', value: 'bloop'},
      knerp: {metadata: {}, type: 'Boolean', value: false},
    };
    assert.deepStrictEqual(initialEntity, expectedInitialEntity);
    // upsert registrationAttr
    const attrs = {[registrationAttr]: 777};
    await ngsiApi.upsertEntityAttrs({entityId: 'derp', attrs});
    // and check for no changes
    const {data: savedEntity} = await ngsiApi.findEntity({entityId: 'derp'});
    assert.deepStrictEqual(savedEntity, expectedInitialEntity);
    // check globalUpdateCallback (NO UPDATE)
    const numUpdates = globalUpdateCallback.callCount;
    assert.strictEqual(numUpdates, 0);
    globalUpdateCallback.resetHistory();
    // check globalRegistrationCallback
    const numRegs = globalRegistrationCallback.callCount;
    assert.strictEqual(numRegs, 1);
    const firstCallArgs = globalRegistrationCallback.firstCall.args;

    const expectedFirstCallArgs = [
      'diggity',
      {
        metadata: {},
        type: 'Number',
        value: 777,
      },
      {
        id: 'derp',
        type: 'Thing',
        servicePath: '/',
        attrs: {
          smerp: {
            metadata: {},
            type: 'Text',
            value: 'bloop',
            builtinMetadata: {},
          },
          knerp: {
            metadata: {},
            type: 'Boolean',
            value: false,
            builtinMetadata: {},
          },
        },
        builtinAttrs: {
          fiwareId: {type: 'Text', value: fiwareId},
        },
      },
    ];
    assert.strictEqual(firstCallArgs.length, 3);
    assert.strictEqual(firstCallArgs[0], expectedFirstCallArgs[0]);
    assert.deepStrictEqual(firstCallArgs[1], expectedFirstCallArgs[1]);
    assert.deepStrictEqual(wipeAllDates(firstCallArgs[2]), expectedFirstCallArgs[2]);
    globalRegistrationCallback.resetHistory();
  });

  it('delete the device', async () => {
    await ngsiApi.deleteEntity({entityId: 'derp'});
    const {data: docs} = await ngsiApi.getEntities();
    assert.strictEqual(docs.length, 0);
    const numUpdates = globalUpdateCallback.callCount;
    assert.strictEqual(numUpdates, 1);
    const firstArg = globalUpdateCallback.firstCall.firstArg;
    const expectedFirstArg = {
      // modifiedEntity: null,
      originalEntity: {
        id: 'derp',
        type: 'Thing',
        servicePath: '/',
        attrs: {
          smerp: {
            metadata: {},
            type: 'Text',
            value: 'bloop',
            builtinMetadata: {},
          },
          knerp: {
            metadata: {},
            type: 'Boolean',
            value: false,
            builtinMetadata: {},
          },
        },
        builtinAttrs: {
          fiwareId: {type: 'Text', value: fiwareId},
        },
      },
    };
    assert.deepStrictEqual(firstArg.modifiedEntity, null);
    assert.deepStrictEqual(wipeAllDates(firstArg.originalEntity), expectedFirstArg.originalEntity);

  });

});
