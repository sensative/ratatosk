/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const sinon = require('sinon');
const {ObjectId} = require('mongodb');

const {
  assertRejects,
  ngsiResponseErrorUtils: {assertErrorsMatch},
} = require('@ratatosk/ngsi-utils');

const {
  ORION_ERRORS,
} = require('@ratatosk/ngsi-constants');

const {
  startNgsiV2Server,
  stopNgsiV2Server,
} = require('../src');

const {
  createApi,
} = require('@ratatosk/ngsi-client-api');

const {
  getMongoConfig,
} = require('./test-config');

const {
  connectionManager: {getCurrentDatabase},
} = require('@ratatosk/ngsi-mongo-storage');

const {
  generateApi: generateAccessApi,
} = require('@ratatosk/ngsi-mongo-access-manager');

const globalUpdateCallback = sinon.spy();
const serverPort = 8999;

const ngsiApi = createApi({
  host: 'localhost',
  port: serverPort,
});

const userId = (new ObjectId()).toString();

describe('access-control', () => {

  let accessApi = null;
  let fiwareId = null;
  before(async () => {
    const mongoConfig = getMongoConfig();
    const publisherConfig = {
      globalUpdateCallback,
      userAccess: true,
    };
    const serverConfig = {port: serverPort};
    const customConfig = {
      mongo: mongoConfig,
      publisher: publisherConfig,
      server: serverConfig,
    };
    await startNgsiV2Server(customConfig);
    // and start up the accessToken api
    const db = getCurrentDatabase();
    accessApi = await generateAccessApi(db);
  });

  after(async () => {
    await accessApi.deleteTokens();
    await stopNgsiV2Server();
  });

  it('create a new device', async () => {
    const entity = {id: 'rederp'};
    // without userId - create does not work
    const createErr = await assertRejects(ngsiApi.createEntity({entity}));
    assertErrorsMatch(createErr, ORION_ERRORS.BAD_REQ_INVALID_USER_ID);
    // but should work with userId
    await ngsiApi.createEntity({entity, userId});
    // check up on the update stuff
    const numUpdates = globalUpdateCallback.callCount;
    assert.strictEqual(numUpdates, 1);
    const firstArg = globalUpdateCallback.firstCall.firstArg;
    const date = _.get(firstArg, 'modifiedEntity.builtinAttrs.dateCreated.value');
    fiwareId = _.get(firstArg, 'modifiedEntity.builtinAttrs.fiwareId.value'); // gets reused
    const expectedFirstArg = {
      originalEntity: null,
      modifiedEntity: {
        id: 'rederp',
        type: 'Thing',
        servicePath: '/',
        attrs: {},
        builtinAttrs: {
          dateCreated: {type: 'DateTime', value: date},
          dateModified: {type: 'DateTime', value: date},
          fiwareId: {type: 'Text', value: fiwareId},
        },
      },
      userId, // GETS INCLUDED
    };
    assert.deepStrictEqual(firstArg, expectedFirstArg);
    // without the access set, entity is not found (using the api)
    const err = await assertRejects(ngsiApi.findEntity({entityId: 'rederp', userId}));
    assertErrorsMatch(err, ORION_ERRORS.FIND_ERROR_ENTITY_MISSING);
    // set the access (now it should work as expected)
    await accessApi.createToken({
      userId,
      entityId: fiwareId,
      accessType: 'READ',
    });
    const {data: savedEntity} = await ngsiApi.findEntity({entityId: 'rederp', userId});
    const expectedEntity = {
      id: 'rederp',
      type: 'Thing',
    };
    assert.deepStrictEqual(savedEntity, expectedEntity);
    // and reset
    globalUpdateCallback.resetHistory();
  });

  it('delete the device', async () => {
    // deleting device without userId will not work
    const err = await assertRejects(ngsiApi.deleteEntity({entityId: 'rederp'}));
    assertErrorsMatch(err, ORION_ERRORS.BAD_REQ_INVALID_USER_ID);
    // even WITH userId it does not work, since we have not given write access yet
    const err2 = await assertRejects(ngsiApi.deleteEntity({entityId: 'rederp', userId}));
    assertErrorsMatch(err2, ORION_ERRORS.FIND_ERROR_ENTITY_MISSING);
    // add write accessToken - and it works
    await accessApi.createToken({
      userId,
      entityId: fiwareId,
      accessType: 'WRITE',
    });
    await ngsiApi.deleteEntity({entityId: 'rederp', userId});
    // check that it's gone
    const {data: docs} = await ngsiApi.getEntities({userId});
    assert.strictEqual(docs.length, 0);
    const numUpdates = globalUpdateCallback.callCount;
    assert.strictEqual(numUpdates, 1);
    const firstArg = globalUpdateCallback.firstCall.firstArg;
    const creDate = _.get(firstArg, 'originalEntity.builtinAttrs.dateCreated.value');
    const expectedFirstArg = {
      modifiedEntity: null,
      originalEntity: {
        id: 'rederp',
        type: 'Thing',
        servicePath: '/',
        attrs: {},
        builtinAttrs: {
          dateCreated: {type: 'DateTime', value: creDate},
          dateModified: {type: 'DateTime', value: creDate},
          fiwareId: {type: 'Text', value: fiwareId},
        },
      },
      userId, // GETS INCLUDED
    };
    assert.deepStrictEqual(firstArg, expectedFirstArg);
  });

});
