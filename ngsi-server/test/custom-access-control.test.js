/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const assert = require('assert');
const sinon = require('sinon');

const {
  startNgsiV2Server,
  stopNgsiV2Server,
} = require('../src');

const {
  createApi,
} = require('@ratatosk/ngsi-client-api');

const {
  getMongoConfig,
} = require('./test-config');

const {
  connectionManager: {mongoRaw},
} = require('@ratatosk/ngsi-mongo-storage');


const globalUpdateCallback = sinon.spy();
const serverPort = 8999;

const ngsiApi = createApi({
  host: 'localhost',
  port: serverPort,
});

const users = {
  bob: 'bob_id',
  sue: 'sue_id',
};

//
//
// This is the guy that is being flexed

// const customAggregationGenerator = sinon.spy((userId, storageApiKey, representationType) => {
// });

const customAggregationGenerator = sinon.spy((...args) => {
  const aggregationStages = [
    {$match: {'attrs.joh.value': {$lt: 10}}},
  ];
  return aggregationStages;
});

describe('access-control', () => {

  before(async () => {
    const mongoConfig = getMongoConfig();
    const publisherConfig = {
      userAccess: true,
      globalUpdateCallback,
      customAggregationGenerator,
    };
    const serverConfig = {port: serverPort};
    const customConfig = {
      mongo: mongoConfig,
      publisher: publisherConfig,
      server: serverConfig,
    };
    await startNgsiV2Server(customConfig);
    assert.strictEqual(customAggregationGenerator.callCount, 0);
  });

  after(async () => {
    await stopNgsiV2Server();
  });

  it('create some entities', async () => {
    const valids = [
      {entity: {id: 'aarb', joh: 2}, userId: users.bob},
      {entity: {id: 'barb', joh: 3}, userId: users.bob},
      {entity: {id: 'carb', joh: 12}, userId: users.bob},
      {entity: {id: 'darb', joh: 13}, userId: users.bob},
    ];
    await Promise.all(_.map(valids, async ({userId, entity}) => {
      await ngsiApi.createEntity({userId, entity});
    }));
    const num = await mongoRaw.countEntities();
    assert.strictEqual(num, 4);
    assert.strictEqual(customAggregationGenerator.callCount, 4);
    customAggregationGenerator.resetHistory();
  });

  it('get some entities (using the customAggregationGenerator)', async () => {
    const {data: docs} = await ngsiApi.getEntities({userId: users.bob});
    assert.strictEqual(docs.length, 2);
    _.each(docs, (doc) => {
      assert(doc.joh.value < 10);
    });
    assert.strictEqual(customAggregationGenerator.callCount, 1);
    customAggregationGenerator.resetHistory();
  });

});
