/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');

const shutDown = async (preShutdown, message, exitCode) => {
  try {
    console.info('Trying to do preShutdown');
    await preShutdown();
    console.info(`Shutting down:\n${message}`);
    process.exit(exitCode);
  } catch (err) {
    console.error(`Clean shutdown failed: ${err.stack}`);
    process.exit(1);
  }
};

const registerProcessEventListeners = (cleanShutdown) => {
  console.info('registering process event listeners');
  process.on('SIGTERM', () => {
    shutDown(cleanShutdown, 'SIGTERM received', 0);
  });
  process.on('unhandledRejection', (reason) => {
    shutDown(cleanShutdown, `Promise generated an unhandled rejection, reason: ${_.get(reason, 'stack', reason)}`, 1);
  });
  process.on('uncaughtException', (err) => {
    shutDown(cleanShutdown, `Uncaught exception: ${err.stack}`, 1);
  });
};

const unregisterAllEventListeners = () => {
  process.removeAllListeners('SIGTERM');
  process.removeAllListeners('unhandledRejection');
  process.removeAllListeners('uncaughtException');
};

module.exports = {
  registerProcessEventListeners,
  unregisterAllEventListeners,
};
