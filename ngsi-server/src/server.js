/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const http = require('http');
const express = require('express');
// const compression = require('compression');

const {
  createLogger,
  createCors,
  notFoundHandler,
  errorHandler,
} = require('./server-middleware');

// MUTABLE STATE!!
const state = {
  server: null,
  isStopping: null,
  isStarting: null,
};

// this is just used as a sanity check
const assertStateIsValid = () => {
  // at most 1 can be non-null at a time
  const isValid = _.size(_.compact(state)) < 2;
  if (!isValid) {
    throw new Error('sanity check: state should always be valid (DEV MESSAGE)');
  }
};

const initExpress = async (ngsiRouter, options) => {
  // included in options: {port, cors (opt), logger (opt)}
  // load the middleware
  const app = express();
  // app.use(compression);
  if (options.logger) {
    const logger = createLogger(_.pick(options, ['logger', 'env']));
    app.use(logger);
  }
  if (options.cors) {
    const cors = createCors(options.cors);
    app.options('*', cors);
    app.use(cors);
  }
  app.use(ngsiRouter);
  app.use(notFoundHandler);
  app.use(errorHandler);
  // and start listening
  const server = http.createServer(app);
  return new Promise((resolve) => {
    server.listen(options.port, () => {
      console.info('Listening on port', options.port);
      console.info('%%% Express HTTP Server CONNECTED %%%');
      resolve(server);
    });
    server.on('error', (err) => {
      console.error('Express HTTP Server error:', err.stack);
      process.exit(1);
    });
  });
};

const start = async (ngsiRouter, options) => {
  assertStateIsValid();
  if (state.isStopping) {
    return state.isStopping
      .then(() => start(ngsiRouter, options));
  }
  if (state.isStarting) {
    return state.isStarting;
  }
  if (state.server) {
    return state.server;
  }
  // start express
  state.isStarting = initExpress(ngsiRouter, options)
    .then((server) => {
      state.server = server;
      state.isStarting = null;
      assertStateIsValid();
      return state.server;
    });
  // and done
  assertStateIsValid();
  return state.isStarting;
};

const stop = async () => {
  assertStateIsValid();
  if (state.isStopping) {
    return state.isStopping;
  }
  if (state.isStarting) {
    return state.isStarting
      .then(stop);
  }
  if (!state.server) {
    return; // already stopped
  }
  // and stop server
  const server = state.server;
  state.server = null;
  state.isStopping = new Promise((resolve) => server.close(resolve))
    .then(() => {
      state.isStopping = null;
      assertStateIsValid();
    });
  // and done
  assertStateIsValid();
  return state.isStopping;
};

const restart = async (ngsiRouter, options) => {
  await stop();
  return start(ngsiRouter, options);
};

module.exports = {
  restart,
  stop,
};
