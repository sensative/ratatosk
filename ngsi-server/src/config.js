/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const {validateProps, PropTypes} = require('vanilla-prop-types');
const {
  ngsiQueryParamsUtils: {attrsParamParser: {parseAttrsParam}},
} = require('@ratatosk/ngsi-utils');

const {ArgumentParser} = require('argparse');

// Any config options coming from the command line arguments get the highest priority
const parseXArgs = () => {
  const parser = new ArgumentParser({
    version: '0.0.1',
    addHelp: true,
    description: 'a javascript implementation of ngsi v2',
  });
  parser.addArgument(
    ['-e', '--disableprocesseventlisteners'],
    {
      nargs: 0,
      help: 'if included, will not register process event listeners catching unhandled errors and SIGTERM',
    },
  );
  parser.addArgument(
    ['-p', '--port'],
    {help: 'port for ngsi-server (default 1026)'},
  );
  parser.addArgument(
    ['-f', '--pathprefix'],
    {help: 'Path prefix to add before NGSI routes'},
  );
  parser.addArgument(
    ['-l', '--logformat'],
    {help: 'morgan logger format to use (default "dev")'},
  );
  parser.addArgument(
    ['-r', '--dbhost'],
    {help: 'the db host ip address (default "localhost")'},
  );
  parser.addArgument(
    ['-q', '--dbport'],
    {help: 'the db port (default 27017)'},
  );
  parser.addArgument(
    ['-n', '--dbname'],
    {help: 'the mongo db name (default "ngsi-server")'},
  );

  // callbacks to the mothership
  parser.addArgument(
    ['-u', '--globalupdateurl'],
    {help: 'opt-in feature: url where update docPairs are published (over http)'},
  );
  parser.addArgument(
    ['-z', '--globalregistrationurl'],
    {help: 'opt-in feature: url where global registrations events are published (over http)'},
  );
  parser.addArgument(
    ['-w', '--globalregistrationattrs'],
    {help: 'opt-in feature: commaseparated list of attrs that are interpreted as the global command attrs (probably should just be the one)'},
  );
  // if user access control is applied
  parser.addArgument(
    ['-a', '--useraccess'],
    {
      nargs: 0,
      help: 'if included, then all (entity-related) requests will be expected to include the {Fiware-UserId: xxxxx} header',
    },
  );

  const args = parser.parseKnownArgs();
  return {
    disableProcessEventListeners: !!args.disableprocesseventlisteners || false,
    port: _.toSafeInteger(args.port) || undefined,
    pathPrefix: args.pathprefix || undefined,
    logFormat: args.logformat || undefined,
    dbHost: args.dbhost || undefined,
    dbPort: _.toSafeInteger(args.dbport) || undefined,
    dbName: args.dbname || undefined,
    globalUpdateUrl: args.globalupdateurl || undefined,
    globalRegistrationUrl: args.globalregistrationurl || undefined,
    globalRegistrationAttrs: args.globalregistrationurl || undefined,
    userAccess: !!args.useraccess || false,
    customAggregationGenerator: undefined, // always & unabmiguously
  };
};

// Config options set using the overall env get second highest priority
const parseEnvArgs = () => {
  const args = {
    disableProcessEventListeners: process.env.DISABLE_PROCESS_EVENT_LISTENERS === 'false' ? false : !!process.env.DISABLE_PROCESS_EVENT_LISTENERS,
    port: process.env.NGSI_SERVER_PORT || undefined,
    pathPrefix: process.env.NGSI_SERVER_PATH_PREFIX || undefined,
    logFormat: process.env.LOG_FORMAT || undefined,
    dbHost: process.env.DB_HOST || undefined,
    dbPort: process.env.DB_PORT || undefined,
    dbName: process.env.DB_NAME || undefined,
    globalUpdateUrl: process.env.GLOBAL_UPDATE_URL || undefined,
    globalRegistrationUrl: process.env.GLOBAL_REGISTRATION_URL || undefined,
    globalRegistrationAttrs: process.env.GLOBAL_REGISTRATION_ATTRS || undefined,
    userAccess: process.env.USER_ACCESS === 'false' ? false : !!process.env.USER_ACCESS,
    customAggregationGenerator: undefined, // always & unabmiguously
  };
  return args;
};

// and the defaults (i.e. the lowest priority)
const defaultConfigArgs = {
  disableProcessEventListeners: false,
  port: 1026,
  pathPrefix: '',
  logFormat: 'dev', // morgan predefined formats: 'combined', 'common', 'dev', 'short', 'tiny'
  dbHost: 'localhost',
  dbPort: 27017,
  dbName: 'ngsi-server',
  globalUpdateUrl: undefined,
  globalRegistrationUrl: undefined,
  globalRegistrationAttrs: undefined,
  userAccess: false,
  customAggregationGenerator: undefined, // always & unabmiguously
};

const calculateDefaultConfig = (configOptions) => {
  const xArgs = configOptions.usesXArgs ? parseXArgs() : {};
  const envArgs = configOptions.usesEnvArgs ? parseEnvArgs() : {};
  // smash em together
  const resolvedConfigArgs = _.merge({}, defaultConfigArgs, envArgs, xArgs);

  // construct the default config
  const defaultConfig = {
    disableProcessEventListeners: resolvedConfigArgs.disableProcessEventListeners,
    logFormat: resolvedConfigArgs.logFormat,
    server: {
      port: resolvedConfigArgs.port,
      pathPrefix: resolvedConfigArgs.pathPrefix,
      cors: true,
      logger: true,
    },
    mongo: {
      url: `mongodb://${resolvedConfigArgs.dbHost}:${resolvedConfigArgs.dbPort}`,
      dbName: resolvedConfigArgs.dbName,
      options: {},
    },
    publisher: {
      globalUpdateCallback: resolvedConfigArgs.globalUpdateUrl ? {url: resolvedConfigArgs.globalUpdateUrl} : undefined,
      globalRegistrationCallback: resolvedConfigArgs.globalRegistrationUrl ? {url: resolvedConfigArgs.globalRegistrationUrl} : undefined,
      globalRegistrationAttrs: parseAttrsParam(resolvedConfigArgs.globalRegistrationAttrs),
      userAccess: resolvedConfigArgs.userAccess,
      customAggregationGenerator: resolvedConfigArgs.customAggregationGenerator,
    },
  };

  return defaultConfig;
};

const getCustomizedConfig = (customConfig, configOptions) => {
  const defaultConfig = calculateDefaultConfig(configOptions);
  const config = {
    disableProcessEventListeners: _.get(customConfig, 'disableProcessEventListeners', defaultConfig.disableProcessEventListeners),
    logFormat: _.get(customConfig, 'logFormat', defaultConfig.logFormat),
    server: {
      ...defaultConfig.server,
      ..._.get(customConfig, 'server', {}),
    },
    mongo: {
      ...defaultConfig.mongo,
      ..._.get(customConfig, 'mongo', {}),
    },
    publisher: {
      ...defaultConfig.publisher,
      ..._.get(customConfig, 'publisher', {}),
    },
  };
  return config;
};

const validateConfigOptions = validateProps({
  usesXArgs: PropTypes.bool,
  usesEnvArgs: PropTypes.bool,
}, {
  isExact: true,
});

// and done
module.exports = {
  getCustomizedConfig,
  validateConfigOptions,
};
