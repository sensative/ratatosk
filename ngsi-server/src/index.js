/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const delay = require('delay');

const {
  validateConfigOptions,
  getCustomizedConfig,
} = require('./config');
const {createRoutes} = require('@ratatosk/ngsi-routes');
const {
  connectionManager: {
    restart: startMongo,
    stop: stopMongo,
  },
} = require('@ratatosk/ngsi-mongo-storage');
const {
  restart: startServer,
  stop: stopServer,
} = require('./server');

const {
  registerProcessEventListeners,
  unregisterAllEventListeners,
} = require('./process-event-listeners');

const startMongoRecursive = (mongoConfig) => {
  return startMongo(mongoConfig)
    .catch(async () => {
      await delay(1000);
      return startMongoRecursive(mongoConfig);
    });
};

const preShutdown = async () => {
  await stopServer();
  await stopMongo();
};

const startNgsiV2Server = async (customConfig, configOptions = {}) => {
  validateConfigOptions(configOptions);
  const config = getCustomizedConfig(customConfig, configOptions);

  if (!config.disableProcessEventListeners) {
    // prepare the shutdown stuff
    registerProcessEventListeners(preShutdown);
  }

  // start mongo
  const mongoConfig = {
    ...config.mongo,
    globalRegistrationAttrs: config.publisher.globalRegistrationAttrs,
  };
  const mongoStorage = await startMongoRecursive(mongoConfig);

  // and start the express server
  const options = _.assign(
    {env: config.env},
    config.server,
    {logger: _.isUndefined(config.server.logger) ? true : config.server.logger},
  );

  // create the router
  const router = createRoutes(mongoStorage, config.publisher, options);
  await startServer(router, options);
};

const stopNgsiV2Server = async () => {
  await preShutdown();
  unregisterAllEventListeners();
};

module.exports = {
  startNgsiV2Server,
  stopNgsiV2Server,
};
