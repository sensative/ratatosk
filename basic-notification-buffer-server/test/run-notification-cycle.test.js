/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const assert = require('assert');
const axios = require('axios');

// start up the server
const {stop, restart} = require('../src');

// collect some info
const {server: {port}} = require('../src/config');

const {request} = axios.create({
  baseURL: `http://localhost:${port}/`,
  timeout: 1000,
});

const getNotifications = async () => {
  const {data: notifications} = await request({
    url: '/notifications',
    method: 'get',
  });
  return notifications;
};

const notify = async (notification) => {
  await request({
    url: '/notify',
    method: 'post',
    data: notification,
  });
};

describe('run-notification-cycle', () => {

  before(async () => {
    await restart();
  });

  after(async () => {
    await stop();
  });

  it('testing some notifies', async () => {
    const notesInit = await getNotifications();
    assert.deepStrictEqual(notesInit, []);
    await notify({a: 1});
    await notify({b: 2});
    const notesMid = await getNotifications();
    assert.deepStrictEqual(notesMid, [{a: 1}, {b: 2}]);
    const notesEnd = await getNotifications();
    assert.deepStrictEqual(notesEnd, []);
  });

});
