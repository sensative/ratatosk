/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const {ArgumentParser} = require('argparse');

// Any config options coming from the command line arguments get the highest priority
const parseXArgs = () => {
  const parser = new ArgumentParser({
    version: '0.0.1',
    addHelp: true,
    description: 'listens for notifications - a testing aid',
  });
  parser.addArgument(
    ['-p', '--port'],
    {help: 'port for basic-notification-buffer-server (default 8765)'},
  );
  parser.addArgument(
    ['-l', '--logformat'],
    {help: 'morgan logger format to use (default "dev")'},
  );
  const args = parser.parseKnownArgs(); // craps out during testing if only using parseArgs
  return {
    port: _.toSafeInteger(args.port) || undefined,
    logFormat: args.logFormat || undefined,
  };
};
const xArgs = parseXArgs();

// Config options set using the overall env get second highest priority
const parseEnvArgs = () => {
  const args = {
    port: process.env.NOTIFICATION_BUFFER_PORT || undefined,
    logFormat: process.env.LOG_FORMAT || undefined,
  };
  return args;
};
const envArgs = parseEnvArgs();

// and the defaults (i.e. the lowest priority)
const defaultConfigArgs = {
  port: 8765,
  logFormat: 'dev', // morgan predefined formats: 'combined', 'common', 'dev', 'short', 'tiny'
};

// smash em together
const resolvedConfigArgs = _.merge({}, defaultConfigArgs, envArgs, xArgs);

// construct the config
const config = {
  logFormat: resolvedConfigArgs.logFormat,
  server: {
    port: resolvedConfigArgs.port,
    cors: true,
    logger: true,
  },
};

// and done
module.exports = config;
