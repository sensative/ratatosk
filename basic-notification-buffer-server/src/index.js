/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const config = require('./config');

const {
  restart: startServer,
  stop: stopServer,
} = require('./server');


const stop = async () => stopServer();

const restart = async () => {
  const options = _.assign(
    {env: config.env},
    config.server,
    {logger: _.isUndefined(config.server.logger) ? true : config.server.logger},
  );
  await startServer(options)
    .catch((err) => {
      console.error('Express HTTP Server error:', err.stack);
      process.exit(1);
    });
};


// for the testing
module.exports = {
  stop,
  restart,
};


// and kick it off
restart();
