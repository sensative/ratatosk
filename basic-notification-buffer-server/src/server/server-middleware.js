/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const morgan = require('morgan');
const cors = require('cors');

const DEFAULT_ENV = 'dev';

const createLogger = (options) => {
  if (options.logger === true) {
    const logger = morgan(options.env || DEFAULT_ENV);
    return logger;
  } else if (_.isFunction(options.logger)) {
    return options.logger;
  } else if (!options.logger) {
    return null;
  }
  throw new Error('Invalid logger included in options');
};

const createCors = () => {
  return cors({
    credentials: true,
    origin: true,
  });
};

const errorHandler = (err, req, res, next) => {
  const status = err.status || 500;
  res.status(status).send(err.message);
};

const notFoundHandler = (req, res, next) => {
  // 404 handler
  res.status(404).json('Page not found');
};

module.exports = {
  createLogger,
  createCors,
  notFoundHandler,
  errorHandler,
};
