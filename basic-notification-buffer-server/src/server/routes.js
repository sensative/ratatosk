/*
 * Copyright 2021 Sensative AB
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const _ = require('lodash');
const bodyParser = require('body-parser');
const {Router: createRouter} = require('express');

const getInitialState = () => ({
  notifications: [],
  updates: [],
  // special for ngsi-server
  globalRegistrations: [],
  globalUpdates: [],
});
// this guy is mutable
const state = getInitialState();

//
// ngsi registrations
//

const getCommands = (req, res, next) => {
  const updates = state.updates;
  state.updates = [];
  return res.json(updates).status(200);
};

const addCommand = (req, res, next) => {
  const {body: update} = req;
  state.updates.push(update);
  const contextElements = _.get(update, 'contextElements', []);
  const contextResponses = _.map(contextElements, (contextElement) => ({
    contextElement: {
      ...contextElement,
      isPattern: contextElement.isPattern === 'true',
    },
    statusCode: {
      code: 200,
      reasonPhrase: 'OK',
      // code: success ? 200 : 400,
      // reasonPhrase: success ? 'OK' : 'Failed command!'
    },
  }));
  return res.status(200).json({contextResponses});
};

const queryContext = (req, res, next) => {
  const {body: update} = req;
  const contextElements = _.get(update, 'entities', []);
  const contextResponses = _.map(contextElements, (contextElement) => ({
    contextElement: {
      ...contextElement,
      isPattern: contextElement.isPattern === 'true',
    },
    statusCode: {
      code: 200,
      reasonPhrase: 'OK',
      attributes: [],
    },
  }));
  // return res.status(200).json({});
  return res.status(200).json({contextResponses});
};

//
// ngsi notifications
//

const getNotifications = (req, res, next) => {
  const notifications = state.notifications;
  state.notifications = [];
  return res.json(notifications).status(200);
};

const addNotification = (req, res, next) => {
  const {body: notification} = req;
  state.notifications.push(notification); // mutate!
  return res.status(204).send();
};

//
// global updates
//

const getGlobalUpdates = (req, res, next) => {
  const {globalUpdates} = state;
  state.globalUpdates = [];
  return res.json(globalUpdates).status(200);
};

const addGlobalUpdate = (req, res, next) => {
  const {body: update} = req;
  state.globalUpdates.push(update); // mutate!
  return res.status(204).send();
};

//
// global registrations
//

const getGlobalRegistrations = (req, res, next) => {
  const {globalRegistrations} = state;
  state.globalRegistrations = [];
  return res.json(globalRegistrations).status(200);
};

const addGlobalRegistration = (req, res, next) => {
  const {body: registration} = req;
  state.globalRegistrations.push(registration); // mutate!
  return res.status(204).send();
};

//
// utility to wipe all
//

const reset = (req, res, next) => {
  const sizes = _.mapValues(state, (buffer) => _.size(buffer));
  const total = _.sum(_.values(sizes));
  _.assign(state, getInitialState()); // mutate!!
  return res.json({sizes, total}).status(200);
};

const createRoutes = () => {
  const router = createRouter();
  router.use(bodyParser.json());

  router.get('/notifications', getNotifications);
  router.post('/notify', addNotification);

  router.get('/commands', getCommands);
  router.post('/commands/updateContext', addCommand);
  router.post('/commands/queryContext', queryContext);

  router.get('/globalUpdates', getGlobalUpdates);
  router.post('/globalUpdates', addGlobalUpdate);

  router.get('/globalRegistrations', getGlobalRegistrations);
  router.post('/globalRegistrations', addGlobalRegistration);

  router.post('/reset', reset);

  // and done
  return router;
};


module.exports = createRoutes();
